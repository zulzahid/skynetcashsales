﻿using SkynetCashSales.General;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;

namespace SkynetCashSales
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            string ProcName = System.AppDomain.CurrentDomain.FriendlyName;
            if (ProcName == "SkynetCashSales.exe" && e.Args.Length == 0)
            {
                var myFile = new DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory).GetFiles().ToList().Where(p => p.Name.StartsWith("CSS") && p.Extension == ".exe").OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                if (myFile != null && File.Exists(myFile.Name))
                {
                    Process.Start(myFile.Name, "CSSVersion");
                    Application.Current.Shutdown();
                }               
            }
            else
            {
                if (e.Args.Length != 1 || e.Args[0] != "CSSVersion")
                {
                    MessageBox.Show("Use SkynetCashSales.exe to launch CSS", "Cannot Run Directly", MessageBoxButton.OK, MessageBoxImage.Information);
                    Application.Current.Shutdown();
                }
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            UserActivities.UserExitEvent();

            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName == "SkynetCashSales") p.Kill();
            }
        }
    }
}
