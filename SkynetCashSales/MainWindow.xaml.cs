﻿using SkynetCashSales.General;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Closing += this.OnWindowClosing;
        }
        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            UserActivities.UserWindowEvent("Closed");
            
            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName == "SkynetCashSales") p.Kill();
            }
        }

        private void NewGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
