﻿using SkynetCashSales.ViewModel;
using System.Windows;

namespace SkynetCashSales.View.Help
{
    /// <summary>
    /// Interaction logic for frmLogin.xaml
    /// </summary>
    public partial class FrmLogin : Window
    {
        public FrmLogin()
        {
            InitializeComponent();
            DataContext = new LoginVM(this);
        }
    }
}
