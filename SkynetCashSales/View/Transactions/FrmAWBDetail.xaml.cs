﻿using SkynetCashSales.ViewModel.Transactions;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.View.Transactions
{
    /// <summary>
    /// Interaction logic for FrmCashSales.xaml
    /// </summary>
    public partial class FrmAWBDetail : Window
    {
        public FrmAWBDetail()
        {
            InitializeComponent();
        }

        private void txtDestCode_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            var ctx = (AWBDetailVM)this.DataContext;
            ctx.DestinationDetails();
        }

        private void txtStrOtherCharges_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
            e.Handled = !regex.IsMatch((sender as System.Windows.Controls.TextBox).Text.Insert((sender as System.Windows.Controls.TextBox).SelectionStart, e.Text));
            char ch = e.Text[0];
        }

        private void txtVLength_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (txtVLength.Text == "")
            {
                txtVLength.Text = "0";
                txtVLength.SelectionStart = txtVLength.MaxLength;
            }

            string str = txtVLength.Text;
            int index = txtVLength.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtVLength.Text = str.Substring(0, index);
                txtVLength.SelectionStart = txtVLength.MaxLength;
            }
        }

        private void txtVBreadth_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (txtVBreadth.Text == "")
            {
                txtVBreadth.Text = "0";
                txtVBreadth.SelectionStart = txtVBreadth.MaxLength;
            }

            string str = txtVBreadth.Text;
            int index = txtVBreadth.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtVBreadth.Text = str.Substring(0, index);
                txtVBreadth.SelectionStart = txtVBreadth.MaxLength;
            }

        }

        private void txtVHeight_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (txtVHeight.Text == "")
            {
                txtVHeight.Text = "0";
                txtVHeight.SelectionStart = txtVHeight.MaxLength;
            }

            string str = txtVHeight.Text;
            int index = txtVHeight.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtVHeight.Text = str.Substring(0, index);
                txtVHeight.SelectionStart = txtVHeight.MaxLength;
            }
        }

        private void txtInsShpmntValue_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (txtInsShpmntValue.Text == "")
            {
                txtInsShpmntValue.Text = "0";
                txtInsShpmntValue.SelectionStart = txtInsShpmntValue.MaxLength;
            }
        }

        private void TextBox_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            if (e.KeyboardDevice.IsKeyDown(Key.Tab) || e.KeyboardDevice.IsKeyDown(Key.Enter))
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtbox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            if (e.MouseDevice.LeftButton == MouseButtonState.Pressed)
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtStrOtherCharges_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtStrOtherCharges.Text == "")
            {
                txtStrOtherCharges.Text = "0";
                txtStrOtherCharges.SelectionStart = txtStrOtherCharges.MaxLength;
            }

            string str = txtStrOtherCharges.Text;
            int index = txtStrOtherCharges.Text.LastIndexOf(".");
            int count = 0;
            bool doubleDot = false;

            if (index > str.IndexOf("."))
            {                
                txtStrOtherCharges.Text = str.Substring(0, index);
                txtStrOtherCharges.SelectionStart = txtStrOtherCharges.MaxLength;
                doubleDot = true;
            }

            //To prevent entering more than 2 decimal places
            if (!doubleDot) { count = BitConverter.GetBytes(decimal.GetBits(Convert.ToDecimal(str))[3])[2]; }
            if(count > 2) { txtStrOtherCharges.Text = Math.Round(Convert.ToDecimal(str), 2, MidpointRounding.ToEven).ToString(); }
        }
    }
}
