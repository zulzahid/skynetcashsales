﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.View.Transactions
{
    /// <summary>
    /// Interaction logic for FrmCashSales.xaml
    /// </summary>
    public partial class FrmLodgeIn : Window
    {
        public FrmLodgeIn()
        {
            InitializeComponent();
        }

        private void TextBox_GotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            if (e.KeyboardDevice.IsKeyDown(Key.Tab) || e.KeyboardDevice.IsKeyDown(Key.Enter))
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtbox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            if (e.MouseDevice.LeftButton == MouseButtonState.Pressed)
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtWeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtWeight.Text == "")
            {
                txtWeight.Text = "0";
                txtWeight.SelectionStart = txtWeight.MaxLength;
            }

            string str = txtWeight.Text;
            int index = txtWeight.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtWeight.Text = str.Substring(0, index);
                txtWeight.SelectionStart = txtWeight.MaxLength;
            }
        }

        private void txtLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtLength.Text == "")
            {
                txtLength.Text = "0";
                txtLength.SelectionStart = txtLength.MaxLength;
            }

            string str = txtLength.Text;
            int index = txtLength.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtLength.Text = str.Substring(0, index);
                txtLength.SelectionStart = txtLength.MaxLength;
            }
        }

        private void txtWidth_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtWidth.Text == "")
            {
                txtWidth.Text = "0";
                txtWidth.SelectionStart = txtWidth.MaxLength;
            }

            string str = txtWidth.Text;
            int index = txtWidth.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtWidth.Text = str.Substring(0, index);
                txtWidth.SelectionStart = txtWidth.MaxLength;
            }
        }

        private void txtHeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtHeight.Text == "")
            {
                txtHeight.Text = "0";
                txtHeight.SelectionStart = txtHeight.MaxLength;
            }

            string str = txtHeight.Text;
            int index = txtHeight.Text.LastIndexOf(".");
            if (index > str.IndexOf("."))
            {
                txtHeight.Text = str.Substring(0, index);
                txtHeight.SelectionStart = txtHeight.MaxLength;
            }
        }
    }
}
