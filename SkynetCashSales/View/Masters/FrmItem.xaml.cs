﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkynetCashSales.View.Masters
{
    /// <summary>
    /// Interaction logic for frmItem.xaml
    /// </summary>
    public partial class FrmItem : Window
    {
        public FrmItem()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = Regex.Replace(((System.Windows.Controls.TextBox)sender).Text, @"[^\d.]", "");
            ((System.Windows.Controls.TextBox)sender).Text = s;
        }

    }
}
