﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Masters;
using SkynetCashSales.ViewModel.Help;
using SkynetCashSales.ViewModel.Masters;
using SkynetCashSales.ViewModel.Transactions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;

namespace SkynetCashSales.ViewModel
{
    public class LoginVM : AMGenFunction
    {
        private FrmLogin MyWind;
        public LoginVM(FrmLogin wind)
        {
            try
            {
                MyWind = wind;
                ServerConfig(); SqlDatabseConection(); APISettings();
                MyWind.Title = $"{MyCompanyCode.Trim()} - Cash Sales System";
                FrontVersionNo = CurrentVersion;

                SqlDataReader dr = GetDataReader($"SELECT TOP 1 CSalesDate FROM tblCashSales WHERE Status = 1 ORDER BY CSalesID DESC");
                if (dr.Read())
                {
                    if(Convert.ToDateTime(dr["CSalesDate"]) >= DateTime.Now)
                    {
                        System.Windows.Forms.DialogResult dialogResult3 = System.Windows.Forms.MessageBox.Show("Your system date is less than your last transaction date, You want to continue?", "Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo);
                        if (dialogResult3 == System.Windows.Forms.DialogResult.No)
                        {
                            Application.Current.Shutdown();
                        }
                    }
                }
                dr.Close();

                MyWind.Title = $"{MyCompanyCode.Trim()} - Cash Sales System";

                dr = GetDataReader($"SELECT CompName, SubName, HQCode, State FROM tblStationProfile WHERE RTRIM(CompCode) = '{MyCompanyCode.Trim()}'");
                if (dr.Read())
                {
                    MyCompanyName = dr["CompName"].ToString();
                    MyCompanySubName = dr["SubName"].ToString();
                    MyBranchCode = dr["HQCode"].ToString().Trim();
                    MyStateName = dr["State"].ToString().Trim();
                }
                else
                {
                    dr.Close();
                    FrmSelectStation stnw = new FrmSelectStation();
                    stnw.DataContext = new SelectStationVM(stnw);
                    stnw.ShowDialog();
                    if (!string.IsNullOrEmpty(CodeChoosen))
                    {
                        MyCompanyCode = CodeChoosen;
                        FrmCompProfile sw = new FrmCompProfile();
                        sw.DataContext = new CompProfileVM(sw);
                        sw.ShowDialog();
                    }
                }
                dr.Close();
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); CloseWind(); }
        }

        public int UserId { get { return GetValue(() => UserId); } set { SetValue(() => UserId, value); OnPropertyChanged("UserId"); } }
        public string UserName { get { return GetValue(() => UserName); } set { SetValue(() => UserName, value); OnPropertyChanged("UserName"); } }
        public string RippleMessage { get { return GetValue(() => RippleMessage); } set { SetValue(() => RippleMessage, value); OnPropertyChanged("RippleMessage"); } }
        public Visibility UpdateVisibility { get { return GetValue(() => UpdateVisibility); } set { SetValue(() => UpdateVisibility, value); OnPropertyChanged("UpdateVisibility"); } }
        public string FrontVersionNo { get { return GetValue(() => FrontVersionNo); } set { SetValue(() => FrontVersionNo, value); OnPropertyChanged("FrontVersionNo"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "System.Windows.Controls.PasswordBox": ExecuteLogin(Obj); break;
                    case "Exit": 
                        { 
                            Application.Current.Shutdown();
                            foreach (Process p in Process.GetProcesses())
                            {
                                if (p.ProcessName == "SkynetCashSales") p.Kill();
                            }
                        } break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        void ExecuteLogin(object parameter)
        {
            try
            {
                VersionCheck();
                FrontVersionNo = CurrentVersion;
            }
            catch (Exception ex)
            {
                error_log.errorlog("Executelogin get new version:" + ex.ToString());
            }

            using (new WaitCursor())
            {
               
                string LoginUser = UserName;
                var passwordBox = parameter as PasswordBox;
                var Password = passwordBox.Password;

                SqlDataReader dr = GetDataReader($"SELECT UserId, UserName, Authentication FROM tblUserMaster WHERE UserName = '{LoginUser}' AND Password = '{Password}' AND Status = 1");
                if (dr.Read())
                {
                    LoginUserId = Convert.ToInt32(dr["UserId"]); 
                    LoginUserName = dr["UserName"].ToString(); 
                    LoginUserAuthentication = dr["Authentication"].ToString();
                    dr.Close();

                    dr = GetDataReader($"SELECT CompName, SubName, HQCode, State FROM tblStationProfile WHERE RTRIM(CompCode) = '{MyCompanyCode.Trim()}'");
                    if (dr.Read())
                    {
                        MyCompanyName = dr["CompName"].ToString();
                        MyCompanySubName = dr["SubName"].ToString();
                        MyBranchCode = dr["HQCode"].ToString().Trim();
                        MyStateName = dr["State"].ToString().Trim();
                    }
                    dr.Close();

                    CSalesNoPrefix = $"CS{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";

                    //  ILANGO  -   COMMON  -----> START
                    CSInvenOBPrefix = $"OB{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";

                    CSInvenPRPrefix = $"PR{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";

                    CSInvenPOPrefix = $"PO{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";

                    CSInvenGRNPrefix = $"GRN{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";

                    CSInvenSAPrefix = $"SS{MyCompanyCode.Trim()}" +
                        $"{(MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim()))}" +
                        $"{new string('0', 5 - (MyCompanySubName.Trim().Length > 5 ? MyCompanySubName.Trim().Substring(0, 5) : (MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())).Length)}";
                    //  ILANGO  -   COMMON  -----> END

                    MyCompanySubName = string.IsNullOrEmpty(MyCompanySubName) ? "" : MyCompanySubName;

                    if (!string.IsNullOrEmpty(MyCompanyName))
                    {
                        CompAddress();
                    }
                    else
                    {
                        dr = GetDataReader($"SELECT StationId, a.StationName, a.StationCode, a.StationType, a.RegNum, a.IATACode, a.BranchCode, b.Address1, b.Address2, b.City, b.State, b.Country, b.ZipCode, b.PhoneNum, b.Fax, b.EMail FROM tblNetworkStation a" +
                            $" LEFT JOIN tblAddressStation b ON a.StationId = b.StationId" +
                            $" WHERE RTRIM(a.StationCode) = '{MyCompanyCode.Trim()}'");
                        if (dr.Read())
                        {
                            ExecuteQuery($"INSERT INTO tblStationProfile(CompName, SubName, CompCode, CompType, RegNum, IATACode, HQCode, Address1, Address2, City, State, Country, ZipCode, PhoneNum, Fax, EMail, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                                $" VALUES('{dr["StationName"].ToString()}', '{dr["StationCode"].ToString().Trim()}', '{dr["StationCode"].ToString().Trim()}', '{dr["StationType"].ToString()}', '{dr["RegNum"].ToString()}', '{dr["IATACode"].ToString()}', '{dr["BranchCode"].ToString().Trim()}', '{dr["Address1"].ToString()}', '{dr["Address2"].ToString()}', '{dr["City"].ToString()}', '{dr["State"].ToString()}', '{dr["Country"].ToString()}', '{dr["ZipCode"].ToString()}', '{dr["PhoneNum"].ToString()}', '{dr["Fax"].ToString()}', '{dr["EMail"].ToString()}', '{Convert.ToInt32(dr["LoginUserId"])}', GETDATE(), 0, NULL, 1)");
                            CompAddress();
                        }
                        dr.Close();
                    }

                    if (LoginUserId > 1)
                    {
                        UserActivities.UserEvents("LOGIN", MyCompanyCode.Trim() + " user:" + LoginUser.Trim() + " successfully logged in");
                        MainWind = new MainWindow();
                        MainWind.DataContext = new MainWindowVM(MainWind);
                        App.Current.MainWindow = MainWind;
                        MainWindStat = true;
                        MainWind.Show();

                        CloseWind();
                    }
                    else
                    {
                        UserActivities.UserEvents("LOGIN", MyCompanyCode.Trim() + " user:" + LoginUser.Trim() + " successfully logged in");
                        MainWind = new MainWindow();
                        MainWind.DataContext = new MainWindowVM(MainWind);
                        App.Current.MainWindow = MainWind;
                        MainWindStat = true;
                        MainWind.Show();

                        CloseWind();
                    }
                }
                else
                {
                    passwordBox.Password = "";
                    passwordBox.Focus();
                    MessageBox.Show("Please check the User Name and Password", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                dr.Close();
            }
        }

        private void ServerConfig()
        {
            if (System.IO.File.Exists(@System.Windows.Forms.Application.StartupPath + "\\" + "ServerConfig.xml"))
            {
                using (var reader = XmlReader.Create(System.Windows.Forms.Application.StartupPath + "\\" + "ServerConfig.xml"))
                {
                    var nodeName = String.Empty;
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                nodeName = reader.Name; break;
                            case XmlNodeType.Text:
                                var trimmed = reader.Value.Trim();
                                if (trimmed.Length == 0) continue;
                                switch (nodeName)
                                {
                                    case "Station": MyCompanyCode = trimmed; break;
                                    case "ServerName": sqlServer = trimmed; break;
                                    case "InstanceName": sqlInstance = trimmed; break;
                                    case "sqlDbName": sqlDbName = trimmed; break;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("ServerConfig.xml is Missing");
            }
        }

        private void APISettings()
        {
            if (System.IO.File.Exists(@System.Windows.Forms.Application.StartupPath + "\\" + "APISettings.xml"))
            {
                using (var reader = XmlReader.Create(System.Windows.Forms.Application.StartupPath + "\\" + "APISettings.xml"))
                {
                    var nodeName = String.Empty;
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                nodeName = reader.Name; break;
                            case XmlNodeType.Text:
                                var trimmed = reader.Value.Trim();
                                if (trimmed.Length == 0) continue;
                                switch (nodeName)
                                {
                                    case "MainURI": APIMainURI = trimmed; break;
                                    case "Token": APIToken = trimmed; break;
                                    case "CheckAWB": APICheckAWB = trimmed; break;
                                    case "MRefBase": APIMobileReferenceBase = trimmed; break;
                                    case "MRef": APIMobileReferenceToken = trimmed; break;
                                    case "MRefBGetShipment": APIMobileReferenceGet = trimmed; break;
                                    case "MRefBUpdateShipment": APIMobileReferenceUpdate = trimmed; break;
                                    case "MRefTrackingBase": APITrackingBase = trimmed; break;
                                    case "MRefTrackingCreate": APICreateTracking = trimmed; break;
                                    case "insertTblRcvCashSaleBase": APICreatetblRcvCashSaleBase = trimmed; break;
                                    case "insertTblRcvCashSale": APICreatetblRcvCashSale = trimmed; break;                                    
                                }
                                break;
                        }
                    }
                }
            }
        }

        private void VersionCheck()
        {
            List<CSSUpgradeDto> CSSUpgradelist = GetCSSUpgradebyAPI();         
            if (CSSUpgradelist != null && CSSUpgradelist.Count != 0)
            {
                foreach (var row in CSSUpgradelist)
                {
                    cssupgrade.VersionNo = row.VersionNo.ToString(); ;
                    cssupgrade.IsCumpolsory = Convert.ToBoolean(row.IsCumpolsory);
                    cssupgrade.ReleaseDate = Convert.ToDateTime(row.ReleaseDate);
                    cssupgrade.FileName = row.FileName.ToString();
                    cssupgrade.Features = row.Features.ToString();
                    cssupgrade.Ftp_URL = row.Ftp_URL.ToString();
                    cssupgrade.Ftp_Password = row.Ftp_Password.ToString();
                    cssupgrade.Ftp_UserName = row.Ftp_Username.ToString();

                    SqlDataReader dr = GetDataReader($"SELECT TOP 1 VersionNo, FileName FROM tblCSSUpgrade WHERE VersionNo = '{cssupgrade.VersionNo}'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"INSERT INTO tblCSSUpgrade ( ReleaseDate, VersionNo, FileName, IsCumpolsory, UpdatedDate, Features, Ftp_URL, Ftp_UserName,Ftp_Password)" +
                            $" VALUES ('{cssupgrade.ReleaseDate.ToString(sqlDateLongFormat)}', '{cssupgrade.VersionNo}', '{cssupgrade.FileName}','{cssupgrade.IsCumpolsory}', GETDATE(), '{cssupgrade.Features}','{cssupgrade.Ftp_URL}','{cssupgrade.Ftp_UserName}','{cssupgrade.Ftp_Password}')");
                    }
                    dr.Close();
                }

                var items = (from s in CSSUpgradelist select s).OrderByDescending(r => r.ReleaseDate).FirstOrDefault();

                cssupgrade.VersionNo = items.VersionNo.ToString(); ;
                cssupgrade.IsCumpolsory = Convert.ToBoolean(items.IsCumpolsory);
                cssupgrade.ReleaseDate = Convert.ToDateTime(items.ReleaseDate);
                cssupgrade.FileName = items.FileName.ToString();
                cssupgrade.Features = items.Features.ToString();
                cssupgrade.Ftp_URL = items.Ftp_URL.ToString();
                cssupgrade.Ftp_Password = items.Ftp_Password.ToString();
                cssupgrade.Ftp_UserName = items.Ftp_Username.ToString();

                if (cssupgrade != null)
                {
                    if (IsNewVersion(cssupgrade))
                    {
                        FrmUpgradePopUp UpdateFrm = new FrmUpgradePopUp();
                        UpdateFrm.DataContext = new UpgradePopUpVM(UpdateFrm);
                        UpdateFrm.ShowDialog();
                    }
                    else { UpdateVisibility = Visibility.Collapsed; }
                }
            }
        }

    }
}
