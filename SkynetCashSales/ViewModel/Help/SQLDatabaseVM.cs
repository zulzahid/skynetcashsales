﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Windows;
//using System.Windows.Forms;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class SQLDatabaseVM : AMGenFunction
    {
        FrmSQLDatabase MyWind;
        public SQLDatabaseVM(FrmSQLDatabase frm)
        {
            MyWind = frm;
            ResetData();
        }

        public string QueryString { get { return GetValue(() => QueryString); } set { SetValue(() => QueryString, value); OnPropertyChanged("QueryString"); } }
        public string QueryResult { get { return GetValue(() => QueryResult); } set { SetValue(() => QueryResult, value); OnPropertyChanged("QueryResult"); } }

        private DataView _QueryOutputList = new DataView();
        public DataView QueryOutputList { get { return _QueryOutputList; } set { _QueryOutputList = value; OnPropertyChanged("QueryOutputList"); } }

        private ObservableCollection<DBFile> _TabelList = new ObservableCollection<DBFile>();
        public ObservableCollection<DBFile> TabelList { get { return _TabelList; } set { _TabelList = value; } }

        private DBFile _SelectedItemGen1 = new DBFile();
        public DBFile SelectedItemGen1 { get { return _SelectedItemGen1; } set { _SelectedItemGen1 = value; OnPropertyChanged("SelectedItemGen1"); } }

        public string ChooseSQLFile { get { return GetValue(() => ChooseSQLFile); } set { SetValue(() => ChooseSQLFile, value); OnPropertyChanged("ChooseSQLFile"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Select table":
                        if (SelectedItemGen1 != null)
                        {
                            QueryString += QueryString != null && QueryString.Trim() != "" ? SelectedItemGen1.TableName : ("SELECT * FROM " + SelectedItemGen1.TableName);
                        }
                        break;
                    case "Go": Go(); break;
                    case "Submit": SubmitData(); break;
                    case "Database Backup": using (new WaitCursor()) { DbBackup(); } break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
        private RelayCommand browsecommand;
        public RelayCommand BrowseCommand
        {
            get
            {
                if (browsecommand == null)
                    browsecommand = new RelayCommand(Browse, CanBrowse);
                return browsecommand;
            }
            set { browsecommand = value; }
        }
        private void Browse(object parameter)
        {
            try
            {
                string type = parameter as string;
                System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse SQL Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;

                openFileDialog1.DefaultExt = "xls";
                openFileDialog1.Filter = "SQL Files|*.sql";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                { ChooseSQLFile = openFileDialog1.FileName; }
            }
            catch (Exception Ex) { error_log.errorlog("SQL Conversion form browse Function: " + Ex.ToString()); }
        }
        private bool CanBrowse(object parameter) { return true; }
        private void Go()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChooseSQLFile))
                {
                    string s = File.ReadAllText(ChooseSQLFile);
                    ExecuteQuery(File.ReadAllText(ChooseSQLFile));
                    MessageBox.Show("Successfully completed", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    ChooseSQLFile = QueryString = string.Empty;
                }
            }
            catch (Exception Ex) { error_log.errorlog("SQLDataBase Go Function:" + Ex.Message); }
        }
        private void SubmitData()
        {
            try
            {
                QueryOutputList = new DataView();
                if (!string.IsNullOrEmpty(QueryString))
                {
                    if (QueryString.Trim().ToUpper().StartsWith("INSERT") || QueryString.Trim().ToUpper().StartsWith("UPDATE") || QueryString.Trim().ToUpper().StartsWith("DELETE") || QueryString.Trim().ToUpper().StartsWith("TRUNCATE"))
                    {
                        int cnt = ExecuteQuery(QueryString);
                        QueryResult = cnt.ToString() + " Records affected";
                        MessageBox.Show("Successfully completed", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        ChooseSQLFile = QueryString = string.Empty;
                    }
                    else if (QueryString.Trim().ToUpper().StartsWith("SELECT") && !QueryString.Trim().ToUpper().Contains("TBLUSERMASTER"))
                    {
                        DataTable dt;
                        using (dt = new DataTable())
                        {
                            dt = GetDataTable(QueryString);
                            QueryResult = dt.Rows.Count.ToString() + " Records selected";
                            QueryOutputList = dt.DefaultView;
                        }
                    }
                    else
                    {
                        QueryResult = "Query is not valid";
                    }
                }
            }
            catch (Exception Ex)
            {
                QueryResult = Ex.Message;
                error_log.errorlog("SQLDataBase Submit Function:" + Ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                TabelList.Clear();
                DataTable dt = new DataTable();
                dt = GetDataTable($"SELECT name FROM sys.tables");

                foreach (DataRow row in dt.Rows)
                {
                    TabelList.Add(new DBFile { TableName = row["name"].ToString() });
                }
            }
            catch (Exception Ex) { error_log.errorlog("SQLDataBase Database Table Load Function:" + Ex.Message); }
        }

        private void DbBackup()
        {
            try
            {
                System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
                sfd.FileName = $"{sqlDbName}_{DateTime.Now.ToString("ddMMyyyy_hhmmtt")}.bak";
                sfd.Filter = "Backup files|*.bak";
                sfd.Title = "Save as backup file";
                sfd.ShowDialog();

                if (sfd.FileName != "")
                {
                    ExecuteQuery($"BACKUP DATABASE {sqlDbName} TO DISK = '{sfd.FileName}'");
                    MessageBox.Show("Backup completed successfully", sqlDbName, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Information", MessageBoxButton.OK, MessageBoxImage.Information); }
        }

        private void ResetData()
        {
            QueryString = QueryResult = string.Empty;
            QueryOutputList = new DataView();

            if (sqlServer == GetIPAddress() || sqlServer == Environment.MachineName)
                MyWind.btnDbBackup.Visibility = Visibility.Visible;
            else
                MyWind.btnDbBackup.Visibility = Visibility.Hidden;

            LoadData();
        }

    }

    public class DBFile : AMGenFunction
    {
        public string TableName { get { return GetValue(() => TableName); } set { SetValue(() => TableName, value); OnPropertyChanged("TableName"); } }
    }
}
