﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class MenuVM : AMGenFunction
    {
        public FrmMenu MyWind;
        public MenuVM(FrmMenu Wind, long mUserId, string mUserName)
        {
            MyWind = Wind;
            UserId =  Convert.ToInt32(mUserId); UserName = mUserName;
            LoadTreeview();
        }

        public int UserId { get { return GetValue(() => UserId); } set { SetValue(() => UserId, value); OnPropertyChanged("UserId"); } }
        public string UserName { get { return GetValue(() => UserName); } set { SetValue(() => UserName, value); OnPropertyChanged("UserName"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                if (Obj.ToString() == "Save")
                {
                    
                }
                else if (Obj.ToString() == "Clear")
                {
                    
                }
                else if (Obj.ToString() == "Close")
                {
                    CloseWind();
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void LoadTreeview()
        {
            int level = 0;
            TreeViewItem[] parent = new TreeViewItem[100];
            try
            {
                List<TreeViewData> treeviewData = LoadTreeviewData();
                for (int i = 0; i < treeviewData.Count; i++)
                {
                    TreeViewData data = (TreeViewData)treeviewData[i];
                    TreeViewItem item = new TreeViewItem();
                    item.Header = data.NodeText;
                    item.IsExpanded = data.Expanded;
                    item.Tag = data;

                    parent[data.ChildId] = item;
                    
                    if (level != data.ParentId)
                        MyWind.tvGen.Items.Add(item);
                    else
                        parent[data.ParentId].Items.Add(item);
                    //level = data.ParentId;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private List<TreeViewData> LoadTreeviewData()
        {
            List<TreeViewData> treeviewData = new List<TreeViewData>();
            DataTable dt = new DataTable();

            int MnuUnder = 1000;
            dt = GetDataTable($"SELECT a.GenDetId, a.GDDesc, a.GenId, b.GenDesc FROM tblGeneralDet a" +
                $" join tblGeneralMaster b on a.GenId = b.GenId" +
                $" where b.GenDesc = 'CSS MENU' order by a.GenId");
            
            MnuUnder = 1;
            for (int i = 0; i < dt.Rows.Count; ++i)
            {
                if (MnuUnder != Convert.ToInt32(dt.Rows[i]["GenId"]))
                {
                    treeviewData.Add(new TreeViewData(Convert.ToInt32(dt.Rows[i]["GenDetId"]), Convert.ToInt32(dt.Rows[i]["GenId"]), true, dt.Rows[i]["GenDesc"].ToString()));
                }
                else
                {
                    treeviewData.Add(new TreeViewData(Convert.ToInt32(dt.Rows[i]["GenDetId"]), Convert.ToInt32(dt.Rows[i]["GenId"]), false, dt.Rows[i]["GenDesc"].ToString()));
                }
                MnuUnder = Convert.ToInt32(dt.Rows[i]["GenId"]);
            }
            return (treeviewData);
        }

        public class TreeViewData
        {
            public int ChildId { get; set; }
            public int ParentId { get; set; }
            public bool Expanded { get; set; }
            //public string _NodeText;
            internal string NodeText { get; set; }

            public TreeViewData(int ChildId, int ParentId, bool Expanded, string NodeText)
            {
                this.ChildId = ChildId;
                this.ParentId = ParentId;
                this.Expanded = Expanded;
                this.NodeText = NodeText;
            }
        }
    }
}
