﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Masters;
using SkynetCashSales.View.Reports;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Masters;
using SkynetCashSales.ViewModel.Reports;
using SkynetCashSales.ViewModel.Transactions;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class MainWindowVM : AMGenFunction
    {
        public Window MyWind;
        MainWindow MyMainWind;
        public MainWindowVM(MainWindow wind)
        {
            try
            {
                MyMainWind = wind;
                MyMainWind.Title = $"{MyCompanyCode.Trim()} - Cash Sales System";
                StnCode = MyCompanyCode; UserName = LoginUserName;
                OnPropertyChanged("UserName");
                rangechecker();
                FrontVersionNo = CurrentVersion;

                if (LoginUserAuthentication.Trim() != "User")
                {
                    MotorBikeV = GeneralV = SQLSetupV = PCSettingV = XMLSettingV = "Visible";
                }
                else
                {
                    MotorBikeV = GeneralV = SQLSetupV = PCSettingV = XMLSettingV = "Hidden";
                }
                Timer();

                string LastCSalesNo = CSalesNoPrefix + "000001";
                SqlDataReader dr = GetDataReader($"SELECT TOP 1 ISNULL(CSalesNo, '{(CSalesNoPrefix + "000001")}') as LastCSalesNo FROM tblCashSales  ORDER BY CDate DESC");
                if (dr.Read())
                {
                    LastCSalesNo = dr["LastCSalesNo"].ToString().Substring(dr["LastCSalesNo"].ToString().Length - 6, 6);
                }
                dr.Close();
                if (LastCSalesNo.Trim() != "")
                {
                    LastCSalesNo = LastCSalesNo.Substring(LastCSalesNo.Length - 6, 6);
                    int RunningNo = Convert.ToInt32(LastCSalesNo) + 1;
                    if (RunningNo > 0)
                    {
                        ExecuteQuery($"IF NOT EXISTS (SELECT name FROM sys.sequences WHERE name = 'CSalesNo')" +
                            $" BEGIN" +
                            $" EXEC('CREATE SEQUENCE CSalesNo AS INT START WITH {RunningNo} INCREMENT BY 1')" +
                            $" END");
                    }
                }

                InitializedDashboard();
                MenuAccessSetup();
            }
            catch (Exception Ex) { General.error_log.errorlog("Main MainWindowVM: " + Ex.ToString()); }
        }

        private string _MotorBikeV, _GeneralV, _SQLSetupV, _PCSettingV, _XMLSettingV, _FrontVersionNo;
        public string MotorBikeV { get { return _MotorBikeV; } set { _MotorBikeV = value; OnPropertyChanged("MotorBikeV"); } }
        public string GeneralV { get { return _GeneralV; } set { _GeneralV = value; OnPropertyChanged("GeneralV"); } }
        public string SQLSetupV { get { return _SQLSetupV; } set { _SQLSetupV = value; OnPropertyChanged("SQLSetupV"); } }
        public string PCSettingV { get { return _PCSettingV; } set { _PCSettingV = value; OnPropertyChanged("PCSettingV"); } }
        public string XMLSettingV { get { return _XMLSettingV; } set { _XMLSettingV = value; OnPropertyChanged("XMLSettingV"); } }
        public string FrontVersionNo { get { return _FrontVersionNo; } set { _FrontVersionNo = value; OnPropertyChanged("FrontVersionNo"); } }
       
        private System.Windows.Threading.DispatcherTimer dispatcherTimertime = new System.Windows.Threading.DispatcherTimer();
        private System.Windows.Threading.DispatcherTimer dispatcherTimersend = new System.Windows.Threading.DispatcherTimer();
        private System.Windows.Threading.DispatcherTimer dispatcherTimerreceive = new System.Windows.Threading.DispatcherTimer();
        public void Timer()
        {
            dispatcherTimertime.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimertime.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimertime.Start();
        }

        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime lDt = DateTime.Now;
                //SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales WHERE Status = 1 ORDER BY CSalesDate DESC");
                SqlDataReader dr = GetDataReader($"SELECT GETDATE() AS ServerDate");
                if (dr.Read())
                {
                    //lDt = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    lDt = Convert.ToDateTime(dr["ServerDate"]).Date;
                }
                dr.Close();

                if (lDt.Date <= DateTime.Now.Date)
                {
                    ServerDataTime = "Server Time : " + DateTime.Now.ToString();
                    UserName = "User : " + LoginUserName;
                    OnPropertyChanged("UserName");
                }
                else
                {
                    MessageBox.Show("Kindly check your system Date and Time?", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                    //System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("The Date outside was changed. Kindly set to Current Date and Time?", "Information", System.Windows.Forms.MessageBoxButtons.OK);
                    //if (dialogResult1 == System.Windows.Forms.DialogResult.OK)
                    //{
                    //    Application.Current.Shutdown();
                    //}
                }
            }
            catch { }
        }

        public string ServerDataTime { get { return GetValue(() => ServerDataTime); } set { SetValue(() => ServerDataTime, value); OnPropertyChanged("ServerDataTime"); } }

        private void rangechecker()
        {
            try
            {
                long available = rangecheckernumber();
                if (available > 200) { }
                else
                {
                    MessageBox.Show("AWB Allocation range less than 200, Available only " + available.ToString(), "Information", MessageBoxButton.OK, MessageBoxImage.Information); //showWindow = false;
                }
                UserName = "User : " + LoginUserName;
                OnPropertyChanged("UserName");
            }
            catch (Exception Ex) { General.error_log.errorlog("Main Range checker: " + Ex.ToString()); }
        }

        private long rangecheckernumber()
        {
            long lascn = 0, toawbnum = 0, remain = 0;
            try
            {
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblLastCNNum");
                if (dr.HasRows)
                {
                    dr = GetDataReader("SELECT CNNum FROM tblLastCNNum WHERE CNNum IS NOT NULL AND CNNum != ''");
                    if (dr.Read())
                    {
                        lascn = Convert.ToInt64(dr["CNNum"]);
                    }
                }
                dr.Close();

                dr = GetDataReader($"SELECT * FROM tblAWBAllocation");
                if (dr.HasRows)
                {
                    dr = GetDataReader($"SELECT ToAWBRunNum FROM tblAWBAllocation WHERE Status = 'ACTIVE' AND IsDelete = 0 ORDER BY ToAWBRunNum DESC");
                    if (dr.Read())
                    {
                        toawbnum = Convert.ToInt64(dr["ToAWBRunNum"]);
                        remain += lascn.ToString().Length == 12 ? ((toawbnum / 10) - (lascn / 10)) : (toawbnum - lascn);
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT ISNULL(SUM(NoOfAWB), 0) AS NoOfAWB FROM tblAWBAllocation WHERE Status = 'NEXT' AND IsDelete = 0");
                    if (dr.Read())
                    {
                        remain += Convert.ToInt64(dr["NoOfAWB"]);
                    }
                }
                dr.Close();
            }
            catch (Exception Ex) { General.error_log.errorlog("Main Range checker: " + Ex.ToString()); }
            return remain;
        }

        private string _StnCode, _AccYear, _UserName;
        public string StnCode { get { return _StnCode; } set { _StnCode = value; OnPropertyChanged("StnCode"); } }
        public string AccYear { get { return _AccYear; } set { _AccYear = value; OnPropertyChanged("AccYear"); } }
        public string UserName { get { return _UserName; } set { _UserName = value; OnPropertyChanged("UserName"); } }

        private ICommand _CommandMenu;
        public ICommand CommandMenu { get { _CommandMenu = new RelayCommand(Parameter => ExecuteMenu(Parameter)); return _CommandMenu; } }

        private void ExecuteMenu(object Obj)
        {
            MyWind = new Window();
            bool showWindow = true;
            try
            {
                NoRange = false;
                switch (Obj.ToString())
                {
                    //Master Start
                    case "Consignor":
                        {
                            FrmConsignor cw = new FrmConsignor();
                            cw.DataContext = new ConsignorVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Region":
                        {
                            FrmRegion cw = new FrmRegion();
                            cw.DataContext = new RegionVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Zone":
                        {
                            FrmZone cw = new FrmZone();
                            cw.DataContext = new ZoneVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "DestCode":
                        {
                            FrmDestination cw = new FrmDestination();
                            cw.DataContext = new DestinationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Item":
                        {
                            FrmItem cw = new FrmItem();
                            cw.DataContext = new ItemVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Staff":
                        {
                            FrmStaff cw = new FrmStaff();
                            cw.DataContext = new StaffVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Staff Discount":
                        {
                            FrmStaffDiscount cw = new FrmStaffDiscount();
                            cw.DataContext = new StaffDiscountVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Post Code":
                        {
                            FrmPostCode cw = new FrmPostCode();
                            cw.DataContext = new PostCodeVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "User":
                        {
                            FrmUser cw = new FrmUser();
                            cw.DataContext = new UserVM(cw);
                            MyWind = cw;
                        }
                        break;
                    //Master End
                    case "MRef Cash Sales":
                        {
                            if (rangecheckernumber() > 0)
                            {
                                FrmMobileRefCashSales cw = new FrmMobileRefCashSales();
                                cw.DataContext = new MobileRefCashSalesVM(cw);
                                MyWind = cw;
                            }
                            else
                            {
                                NoRange = true;
                                MessageBox.Show("AWB range finished, Can't create new. Get new allocation range to continue", "Information", MessageBoxButton.OK, MessageBoxImage.Information); //showWindow = false;
                                FrmMobileRefCashSales cw = new FrmMobileRefCashSales();
                                cw.DataContext = new MobileRefCashSalesVM(cw);
                                MyWind = cw;
                            }
                        }
                        break;
                    case "MRef Cash Sales Update":
                        {
                            FrmUpdateTransaction cw = new FrmUpdateTransaction();
                            cw.DataContext = new UpdateTransactionVM(cw);
                            MyWind = cw;                          
                        }
                        break;
                    case "LodgeIn AWB":
                        {
                            FrmLodgeIn cw = new FrmLodgeIn();
                            cw.DataContext = new LodgeInVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Print AWBNo":
                        {
                            FrmCashSaleList cw = new FrmCashSaleList();
                            cw.DataContext = new CashSalesListVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Checkin and POD":
                        {
                            FrmCheckinAndPOD cw = new FrmCheckinAndPOD();
                            cw.DataContext = new CheckinAndPODVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Promotion List":
                        {
                            FrmPromotionList cw = new FrmPromotionList();
                            cw.DataContext = new PromotionListVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Opinion Poll":
                        {
                            FrmSuggestion cw = new FrmSuggestion();
                            cw.DataContext = new SuggestionVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Latest Updates":
                        {
                            FrmAnnouncements cw = new FrmAnnouncements();
                            cw.DataContext = new AnnouncementsVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "General Master":
                        {
                            FrmGeneral cw = new FrmGeneral();
                            cw.DataContext = new GeneralVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "PostCode Finder":
                        {
                            FrmPostcodeFinder cw = new FrmPostcodeFinder();
                            cw.DataContext = new PostcodeFinderVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Company Profile":
                        {
                            FrmCompProfile cw = new FrmCompProfile();
                            cw.DataContext = new CompProfileVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "View Quotation":
                        {
                            FrmQuotation cw = new FrmQuotation();
                            cw.DataContext = new QuotationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Manage Stationery":
                        {
                            FrmManageStationery cw = new FrmManageStationery();
                            cw.DataContext = new ManageStationeryVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "MotorBike Zone":
                        {
                            FrmMotorBikeZone cw = new FrmMotorBikeZone();
                            cw.DataContext = new MotorBikeZoneVM(cw);
                            MyWind = cw;
                        }
                        break;
                    //Transaction End
                    //Report Start 
                    case "Report":
                        {
                            FrmReportSearch cw = new FrmReportSearch();
                            cw.DataContext = new ReportSearchVM(cw);
                            MyWind = cw;
                        }
                        break;
                    //Report End
                    //System Start
                    case "Today's Report":
                        {
                            FrmTodaysSalesReport cw = new FrmTodaysSalesReport();
                            cw.DataContext = new TodaysSalesReportVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "User Authentication":
                        {
                            FrmUserAuthentication cw = new FrmUserAuthentication();
                            cw.DataContext = new UserAuthenticationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Menu Setting":
                        {
                            FrmMenu cw = new FrmMenu();
                            cw.DataContext = new MenuVM(cw, LoginUserId, LoginUserName);
                            MyWind = cw;
                        }
                        break;
                    case "AWB Allocation":
                        {
                            FrmAWBAllocation cw = new FrmAWBAllocation();
                            cw.DataContext = new AWBAllocationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Stationary Allocation":
                        {
                            FrmStationaryAllocation cw = new FrmStationaryAllocation();
                            cw.DataContext = new StationaryAllocationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Manual AWB Allocation":
                        {
                            FrmManualAWBAllocation cw = new FrmManualAWBAllocation();
                            cw.DataContext = new ManualAWBAllocationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "SQLSetup":
                        {
                            FrmSQLDatabase cw = new FrmSQLDatabase();
                            cw.DataContext = new SQLDatabaseVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Quotation":
                        {
                            FrmQuotation cw = new FrmQuotation();
                            cw.DataContext = new QuotationVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "XMLSetting":
                        {
                            FrmXMLSettings cw = new FrmXMLSettings();
                            cw.DataContext = new XMLSettingsVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Version":
                        {
                            FrmVersionHistory cw = new FrmVersionHistory();
                            cw.DataContext = new VersionHistoryVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "SQLite to Sql Conversion":
                        {
                            frmDbConversion cw = new frmDbConversion();
                            cw.DataContext = new DbConversionVM(cw);
                            MyWind = cw;
                        }
                        break;
                    //case "Auto Backup":
                    //    {
                    //        FrmAutoBackup cw = new FrmAutoBackup();
                    //        cw.DataContext = new AutoBackupVM(cw);
                    //        MyWind = cw;
                    //    }
                    //    break;
                    case "Logout":
                        {
                            UserActivities.UserEvents("LOGOUT", MyCompanyCode.Trim() + " user:" + LoginUserName.Trim() + " logged out");
                            FrmLogout cw = new FrmLogout();
                            cw.DataContext = new LogoutVM(cw);
                            MyWind = cw;
                        }
                        break;
                    case "Exit":
                        {
                            if (MessageBox.Show("Are you sure you want to Close?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                foreach (Process p in Process.GetProcesses())
                                {
                                    if (p.ProcessName == "SkynetCashSales") p.Kill();
                                }
                                Environment.Exit(0);
                            }
                        }
                        break;
                    //System End
                    default: throw new Exception("Unexpected property being validated on Service");
                }
            }
            catch (Exception Ex) { General.error_log.errorlog(Ex.Message); }
            if (Obj.ToString() != "Exit" && MyWind != null && showWindow == true)
            {
                MyWind.ShowInTaskbar = false;
                MyWind.Owner = Application.Current.MainWindow;
                if (MyWind.ToString() == "SkynetCashSales.View.Help.FrmLogout")
                    MyWind.ShowDialog();
                else
                    if (LoginUserId == 1 && (MyWind.ToString() == "SkynetCashSales.View.Masters.FrmCompProfile" || MyWind.ToString() == "SkynetCashSales.Masters.View.FrmUser"))
                    MyWind.ShowDialog();
                else if (LoginUserId > 1)
                    MyWind.Show();
                MyWind = new Window();
            }
        }
    }
}