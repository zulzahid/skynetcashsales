﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class GetCommentVM : AMGenFunction
    {
        public event Action<CommentInfo> Closed;
        public FrmGetComment MyWind;
        public GetCommentVM(FrmGetComment Wind)
        {
            MyWind = Wind;
            Comment = "";
            MyWind.txtComment.Focus();
        }
        public string Comment { get { return GetValue(() => Comment); } set { SetValue(() => Comment, value); OnPropertyChanged("Comment"); } }
        
        private ObservableCollection<AMGenFunction> _Comments = new ObservableCollection<AMGenFunction>();
        public ObservableCollection<AMGenFunction> Comments { get { return _Comments; } set { _Comments = value; } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Ok":
                        {
                            CloseWind();
                            if (Closed != null && Comment != null)
                            {
                                var CommentVal = new CommentInfo() { Comment = Comment };
                                Closed(CommentVal);
                            }
                        } break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Command");
                }
            }
            catch (Exception Ex) { System.Windows.MessageBox.Show(Ex.Message); }
        }

        public class CommentInfo : AMGenFunction
        {
            public string Comment { get { return GetValue(() => Comment); } set { SetValue(() => Comment, value); OnPropertyChanged("Comment"); } }
        }
    }
}
