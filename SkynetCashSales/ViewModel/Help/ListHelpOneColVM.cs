﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class ListHelpOneColVM : AMGenFunction
    {
        public event Action<ListInfo> Closed;
        public FrmListHelpOneCol MyWind;
        private DataTable dtMain;
        public ListHelpOneColVM(DataTable dt, FrmListHelpOneCol Wind, string WinHedr)
        {
            SearchText = string.Empty;

            dtMain = GetDataTable($"SELECT ColOneId = '', ColOneText = ''");
            dtMain.Rows.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dtMain.NewRow();
                dr["ColOneId"] = dt.Rows[i][0].ToString();
                dr["ColOneText"] = dt.Rows[i][1].ToString();
                dtMain.Rows.Add(dr);
            }

            MyWind = Wind; WindHdr = WinHedr;
            foreach (DataRow row in dtMain.Rows)
            {
                SingleCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString() });
            }
        }

        public string WindHdr { get { return GetValue(() => WindHdr); } set { SetValue(() => WindHdr, value); OnPropertyChanged("WindHdr"); } }
        public string ColHeader1 { get { return GetValue(() => ColHeader1); } set { SetValue(() => ColHeader1, value); OnPropertyChanged("ColHeader1"); } }
        public string SearchText { get { return GetValue(() => SearchText); } set { SetValue(() => SearchText, value); OnPropertyChanged("SearchText"); if (SearchText != null) FilterData(); } }

        private ObservableCollection<ListInfo> _SingleCol = new ObservableCollection<ListInfo>();
        public ObservableCollection<ListInfo> SingleCol { get { return _SingleCol; } set { _SingleCol = value; } }

        private ListInfo _SelectedItem = new ListInfo();
        public ListInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            switch (Obj.ToString())
            {
                case "Focus DataGrid":
                    {
                        if (SingleCol.Count > 0)
                        {
                            if (SelectedIndex == SingleCol.Count - 1)
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                            else
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                        }
                    }
                    break;
                case "Ok":
                    {
                        CloseWind();
                        if (Closed != null & SelectedItem != null)
                        {
                            var SelectedItems = new ListInfo() { Id = SelectedItem.Id, ColOneText = SelectedItem.ColOneText };
                            Closed(SelectedItems);
                        }
                    }
                    break;
                case "Move Up":
                    {
                        if (SelectedIndex == 0)
                            MyWind.txtSearchText.Focus();
                        else
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex - 1, 0);
                    }
                    break;
                case "Move Down":
                    {
                        if (SelectedIndex == SingleCol.Count - 1)
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                        else
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                    }
                    break;
                default: throw new Exception("Unexpected Command Parameter");
            }
        }

        private void FilterData()
        {
            SingleCol.Clear();
            if (dtMain != null && dtMain.Rows.Count > 0)
            {
                using (DataView view = new DataView(dtMain, "ColOneText LIKE '" + SearchText + "%'", "", DataViewRowState.CurrentRows))
                {
                    using (DataTable dtFilterData = view.ToTable())
                    {
                        foreach (DataRow row in dtFilterData.Rows)
                        {
                            SingleCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString() });
                        }
                    }
                }
            }
        }

        public class ListInfo
        {
            public string Id { get; set; }
            public string ColOneText { get; set; }
        }

    }
}
