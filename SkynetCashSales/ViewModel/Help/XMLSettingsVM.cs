﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class XMLSettingsVM : AMGenFunction
    {
        public FrmXMLSettings MyWind;

        public XMLSettingsVM(FrmXMLSettings Wind)
        {
            MyWind = Wind;
            WPConfigTabV = "Visible";
            ResetData();

            LoadDataWPConfig(); LoadDataFTP();
            StnCode = MyCompanyCode;
            PaymentModeLoad();

            FTPConfigTabV = "Hidden";

            if (LoginUserAuthentication.Trim() != "User")
            {
                //FTPConfigTabV = SeverConfigTabV = ErrorLogTab = RunServiceTab = "Visible";
                SeverConfigTabV = ErrorLogTab = RunServiceTab = "Visible";
                LoadDataFTP();
            }
        }

        private ObservableCollection<PaymentModeUpdtModel> _paymentModelistitems = new ObservableCollection<PaymentModeUpdtModel>();
        public ObservableCollection<PaymentModeUpdtModel> PaymentModeListItem
        {
            get { return _paymentModelistitems; }
            set { _paymentModelistitems = value; OnPropertyChanged("PaymentModeList"); }
        }
        
        public int PaymentId { get { return GetValue(() => PaymentId); } set { SetValue(() => PaymentId, value); OnPropertyChanged("PaymentId"); } }
        public string PaymentNameTxt { get { return GetValue(() => PaymentNameTxt); } set { SetValue(() => PaymentNameTxt, value); OnPropertyChanged("PaymentNameTxt"); } }
        public string PaymentDetailsTxt { get { return GetValue(() => PaymentDetailsTxt); } set { SetValue(() => PaymentDetailsTxt, value); OnPropertyChanged("PaymentDetailsTxt"); } }
        public bool IsActive { get { return GetValue(() => IsActive); } set { SetValue(() => IsActive, value); OnPropertyChanged("IsActive"); } }

        private PaymentModeUpdtModel _SelectedItemPayment = new PaymentModeUpdtModel();
        public PaymentModeUpdtModel SelectedItemPayment { get { return _SelectedItemPayment; } set { _SelectedItemPayment = value; OnPropertyChanged("SelectedItemPayment"); } }

        public string WeighMachineName { get { return GetValue(() => WeighMachineName); } set { SetValue(() => WeighMachineName, value); OnPropertyChanged("WeighMachineName"); } }
        public string RptPrinter { get { return GetValue(() => RptPrinter); } set { SetValue(() => RptPrinter, value); OnPropertyChanged("RptPrinter"); } }
        public string CNPrinter { get { return GetValue(() => CNPrinter); } set { SetValue(() => CNPrinter, value); OnPropertyChanged("CNPrinter"); } }
        public bool CNPrinterIsActive { get { return GetValue(() => CNPrinterIsActive); } set { SetValue(() => CNPrinterIsActive, value); OnPropertyChanged("CNPrinterIsActive"); } }
        public string StickerPrinter { get { return GetValue(() => StickerPrinter); } set { SetValue(() => StickerPrinter, value); OnPropertyChanged("StickerPrinter"); } }
        public bool StickerPrinterIsActive { get { return GetValue(() => StickerPrinterIsActive); } set { SetValue(() => StickerPrinterIsActive, value); OnPropertyChanged("StickerPrinterIsActive"); } }

        private string _WPConfigTabV, _FTPConfigTabV, _SeverConfigTabV, _ErrorLogTab, _RunServiceTab;
        public string WPConfigTabV { get { return _WPConfigTabV; } set { _WPConfigTabV = value; OnPropertyChanged("WPConfigTabV"); } }
        public string FTPConfigTabV { get { return _FTPConfigTabV; } set { _FTPConfigTabV = value; OnPropertyChanged("FTPConfigTabV"); } }
        public string SeverConfigTabV { get { return _SeverConfigTabV; } set { _SeverConfigTabV = value; OnPropertyChanged("SeverConfigTabV"); } }
        public string ErrorLogTab { get { return _ErrorLogTab; } set { _ErrorLogTab = value; OnPropertyChanged("_ErrorLogTab"); } }
        public string RunServiceTab { get { return _RunServiceTab; } set { _RunServiceTab = value; OnPropertyChanged("RunServiceTab"); } }

        private long _ServerConfigId;
        private string _StnCode, _AccYear, _ServerPath, _DBName, _UserName;
        public long ServerConfigId { get { return _ServerConfigId; } set { _ServerConfigId = value; OnPropertyChanged("ServerConfigId"); } }
        public string StnCode { get { return _StnCode; } set { _StnCode = value; OnPropertyChanged("StnCode"); } }
        public string AccYear { get { return _AccYear; } set { _AccYear = value; OnPropertyChanged("AccYear"); } }
        public string ServerPath { get { return _ServerPath; } set { _ServerPath = value; OnPropertyChanged("ServerPath"); } }
        public string DBName { get { return _DBName; } set { _DBName = value; OnPropertyChanged("DBName"); } }
        public string UserName { get { return _UserName; } set { _UserName = value; OnPropertyChanged("UserName"); } }

        private long _ServerIdFTP;
        private string _ServerFTP, _LocationFTP, _FileNameMainFTP, _UserNameFTP;
        public long ServerIdFTP { get { return _ServerIdFTP; } set { _ServerIdFTP = value; OnPropertyChanged("ServerIdFTP"); } }
        public string ServerFTP { get { return _ServerFTP; } set { _ServerFTP = value; OnPropertyChanged("ServerFTP"); } }
        public string LocationFTP { get { return _LocationFTP; } set { _LocationFTP = value; OnPropertyChanged("LocationFTP"); } }
        public string FileNameMainFTP { get { return _FileNameMainFTP; } set { _FileNameMainFTP = value; OnPropertyChanged("FileNameMainFTP"); } }
        public string UserNameFTP { get { return _UserNameFTP; } set { _UserNameFTP = value; OnPropertyChanged("UserNameFTP"); } }

        private long _WPID;
        private int _WTLocation, _WTDividend, _WTBaudRate, _WTDataBits, _SleepTime;
        private string _WTSerialPort;
        private bool _YesXML, _NoXML;
        public long WPID { get { return _WPID; } set { _WPID = value; OnPropertyChanged("WPID"); } }
        public string WTSerialPort { get { return _WTSerialPort; } set { _WTSerialPort = value; OnPropertyChanged("WTSerialPort"); } }
        public int WTBaudRate { get { return _WTBaudRate; } set { _WTBaudRate = value; OnPropertyChanged("WTBaudRate"); } }
        public int WTDataBits { get { return _WTDataBits; } set { _WTDataBits = value; OnPropertyChanged("WTDataBits"); } }
        public int WTLocation { get { return _WTLocation; } set { _WTLocation = value; OnPropertyChanged("WTLocation"); } }
        public int WTDividend { get { return _WTDividend; } set { _WTDividend = value; OnPropertyChanged("WTDividend"); } }
        public int SleepTime { get { return _SleepTime; } set { _SleepTime = value; OnPropertyChanged("SleepTime"); } }

        private ObservableCollection<LogInfo> _LogInfoDetails = new ObservableCollection<LogInfo>();
        public ObservableCollection<LogInfo> LogInfoDetails { get { return _LogInfoDetails; } set { _LogInfoDetails = value; } }
        private LogInfo _SelectedItem = new LogInfo();
        public LogInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen, _CommandHelp, _CommandGenSaveFTP;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }
        public ICommand CommandHelp { get { if (_CommandHelp == null) _CommandHelp = new RelayCommand(Parameter => ExecuteCommandHelp(Parameter)); return _CommandHelp; } }
        public ICommand CommandGenSaveFTP { get { if (_CommandGenSaveFTP == null) _CommandGenSaveFTP = new RelayCommand(Parameter => ExecuteCommandGenSaveFTP(Parameter)); return _CommandGenSaveFTP; } }

        private void ExecuteCommandGen(object Obj)
        {
            string CaseName = "", ControlName = "";
            string[] StrParameter = Obj.ToString().Split('-');
            if (StrParameter.Length > 1) { CaseName = StrParameter[0]; ControlName = StrParameter[1]; } else { CaseName = StrParameter[0]; }
            if (CaseName == "Save")
            {
                if (ControlName == "WPConfig")
                {
                    SaveDataWPConfig();
                }
                else if (ControlName == "Printer Setting")
                {

                }
            }
            else if (CaseName == "Delete")
            {
                if (ControlName == "FTP")
                {
                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Delete this FTP Settings?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        DeleteDataFTP();
                    }
                }
                else if (ControlName == "WPConfig")
                {
                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Delete this Weight and Printer Settings?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        DeleteDataWPConfig();
                    }
                }
                else { }
            }
            else if (CaseName == "Check")
            {
                if (ControlName == "AvailablePorts")
                {
                    WeightAvailablePorts();
                }
                else if (ControlName == "PortData")
                {
                    WeightPortDataDetailsAll(); WeightPortDataDetails();
                }
                else if (ControlName == "Weight")
                {
                    WeightWeightDetails();
                }
                else { }
            }
            else if (CaseName == "ErrorLog")
            {
                if (ControlName == "View")
                {
                    ViewLogDetails();
                }
                else if (ControlName == "Clear")
                {
                    ClearLogDetails();
                }
                else { }
            }
            else if (CaseName == "RunService")
            {
                if (ControlName == "LastAWB")
                {
                    string number = "";
                    SqlDataReader dr = GetDataReader("SELECT CNNum FROM tblLastCNNum WHERE IsDeleted = 0");
                    if (dr.Read())
                    {
                        number = dr["CNNum"].ToString();
                    }
                    dr.Close();
                    MessageBox.Show("Last used number was " + number);
                }
                else { }
            }
            else if (CaseName == "Clear")
            {
                if (ControlName == "Server")
                {
                    ResetDataServer();
                }
                else if (ControlName == "FTP")
                {
                    ResetDataFTP();
                }
                else if (ControlName == "WPConfig")
                {
                    ResetDataWPConfig();
                }
                else { }
            }
            else if (CaseName == "Close")
            {
                CloseWind();
            }
            else if (CaseName == "SavePaymentMode")
            {
                if (!string.IsNullOrEmpty(PaymentNameTxt) && !string.IsNullOrEmpty(PaymentDetailsTxt))
                {
                    SaveData();
                }
                else { MessageBox.Show("Please fill in Payment Mode Name"); }
            }
            else if (CaseName == "DeletePaymentMode")
            {
                var MessageResult = System.Windows.Forms.MessageBox.Show("Are you sure you want to delete this Payment Mode", "Warning", System.Windows.Forms.MessageBoxButtons.YesNo);
                if (MessageResult == System.Windows.Forms.DialogResult.Yes)
                {
                    int i = Delete();
                    if (i > 0) { MessageBox.Show("Payment Mode Deleted"); PaymentModeLoad(); }
                }
            }
            else if (CaseName == "Load PaymentMode")
            {
                SqlDataReader dr = GetDataReader($"SELECT GenDetId, GDDesc, GDSDesc, Status FROM tblGeneraldet WHERE GDDesc = '{SelectedItemPayment.PaymentCode}' ");
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        PaymentId = Convert.ToInt32(dr["GenDetId"]);
                        PaymentNameTxt = dr["GDDesc"].ToString();
                        PaymentDetailsTxt = dr["GDSDesc"].ToString();
                        IsActive = Convert.ToBoolean(dr["Status"]);
                    }
                }
                else
                {
                    PaymentId = 0; PaymentNameTxt = ""; PaymentDetailsTxt = "";
                }
                dr.Close();
            }
        }

        private void ExecuteCommandGenSaveFTP(object Obj)
        {
            try {
                using (new WaitCursor())
                {
                    string LoginUser = _UserName;
                    var passwordBox = Obj as PasswordBox;
                    var PasswordFTP = passwordBox.Password;
                    if (ServerIdFTP == 0 && !string.IsNullOrEmpty(ServerFTP) && !string.IsNullOrEmpty(UserNameFTP) && !string.IsNullOrEmpty(PasswordFTP))
                    {
                        SqlDataReader dr = GetDataReader($"Select * from tblFTPConfig Where FTPConfigID = {ServerIdFTP}");
                        if (dr.HasRows)
                        {
                            dr.Close();
                            MsgRes = MessageBox.Show("Confirm Modify this FTP Settings?", "Confirmation", MessageBoxButton.YesNo);
                            if (MsgRes == MessageBoxResult.Yes)
                            {
                                ExecuteQuery($"UPDATE tblFTPConfig SET" +
                                    $" StnCode = '{StnCode}'," +
                                    $" SubName = '{MyCompanySubName}'," +
                                    $" FTPServer = '{ServerFTP}'," +
                                    $" FTPUser = '{UserNameFTP}'," +
                                    $" FTPPassword = '{PasswordFTP}'," +
                                    $" FTPLocation = '{LocationFTP}'," +
                                    $" FTPMainFile = '{FileNameMainFTP}'," +
                                    $" Status = 1" +
                                    $" Where FTPConfigID = {ServerIdFTP}");
                                MessageBox.Show("FTP Settings Modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            dr.Close();
                            MsgRes = MessageBox.Show("Confirm Create this FTP Settings?", "Confirmation", MessageBoxButton.YesNo);
                            if (MsgRes == MessageBoxResult.Yes)
                            {
                                ExecuteQuery($"INSERT INTO tblFTPConfig(StnCode, SubName, FTPServer, FTPUser, FTPPassword, FTPLocation, FTPMainFile, CUserID, CDate, Status)" +
                                    $" VALUE ('{StnCode}', '{MyCompanySubName}', '{ServerFTP}', '{UserNameFTP}', '{PasswordFTP}', '{LocationFTP}', '{FileNameMainFTP}', {LoginUserId}, GETDATE(), 1");
                                MessageBox.Show("FTP Settings Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }

                        FTPHost = ServerFTP;
                        FTPUser = UserNameFTP;
                        FTPPassword = PasswordFTP;
                        FTPFilePath = LocationFTP;
                        FTPFinalFileDirectory = LocationFTP;
                        FTPMainFileName = FileNameMainFTP;
                    }
                    else
                        MessageBox.Show("Invalid Data to Save", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex) { error_log.errorlog("FTP data save function " + ex.ToString()); }
        }
        private void SaveDataWPConfig()
        {
            try
            {
                using (new WaitCursor())
                {
                    SqlDataReader dr = GetDataReader($"Select * from tblWPConfig where PCNAME = HOST_NAME()");
                    if(dr.HasRows)
                    {
                        dr.Close();
                        MessageBoxResult MsgRes = MessageBox.Show("Confirm Create this Weight and Printer Settings?", "Confirmation", MessageBoxButton.YesNo);
                        if (MsgRes == MessageBoxResult.Yes)
                        {
                            ExecuteQuery($"UPDATE tblWPConfig SET StnCode = '{StnCode}', SubName = '{MyCompanySubName}', WTMachineName = '{WeighMachineName}', SerialPort = '{WTSerialPort}', BaudRate = '{WTBaudRate}', DataBits = '{WTDataBits}', WTLocation = '{WTLocation}', WTDividend = '{WTDividend}', SleepTime = {SleepTime}, CNPrinter = '{CNPrinter}', RptPrinter = '', IsGenXML = 0, CUserID = {LoginUserId}, CDate = GETDATE(), Status = 1, IsExported = 1, StickerPrinter = '{StickerPrinter}', CNPrinterIsActive = '{CNPrinterIsActive}', StickerPrinterIsActive = '{StickerPrinterIsActive}' Where PCNAME = HOST_NAME()");
                            MessageBox.Show("Weight and Printer Settings updated", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        dr.Close();
                        ExecuteQuery($"INSERT INTO tblWPConfig(StnCode, SubName, WTMachineName, SerialPort, BaudRate, DataBits, WTLocation, WTDividend, SleepTime, CNPrinter, RptPrinter, IsGenXML, PCNAME, CUserID, CDate, Status, IsExported, ExportedDate, StickerPrinter, CNPrinterIsActive, StickerPrinterIsActive)" +
                            $" VALUES('{StnCode}', '{MyCompanySubName}', '{WeighMachineName}', '{WTSerialPort}', '{WTBaudRate}', '{WTDataBits}', '{WTLocation}', '{WTDividend}', {SleepTime}, '{CNPrinter}', '', 0, HOST_NAME(), {LoginUserId}, GETDATE(), 1, 1, GETDATE(), '{StickerPrinter}', '{CNPrinterIsActive}', '{StickerPrinterIsActive}')");
                        
                        MessageBox.Show("Weight and Printer Settings saved", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex) { error_log.errorlog("Weight and Printer data save function " + ex.ToString()); }
        }

        private void ExecuteCommandHelp(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            if (Obj.ToString() == "WPID")
            {
                dt = GetDataTable($"SELECT Distinct ColOneId = a.WPID, ColOneText = a.SerialPort, ColTwoText = a.WTLocation, ColThreeText = a.WTDividend FROM tblWPConfig a where a.Status = 1");
                ChildWnd.FrmListCol3_Closed += (r => { WPID = Convert.ToInt32(r.Id); LoadDataWPConfig(WPID); });
                ChildWnd.ThreeColWindShow(dt, "Weight Selection", "Serail Port", "Location", "Dividend");
            }
            else if (Obj.ToString() == "ServerIDFTP")
            {
                dt = GetDataTable($"SELECT Distinct ColOneId = a.FTPConfigID, ColOneText = a.FTPServer, ColTwoText = a.FTPUser, ColThreeText = a.FTPLocation FROM tblFTPConfig a where a.Status = 1");
                ChildWnd.FrmListCol3_Closed += (r => { ServerIdFTP = Convert.ToInt32(r.Id); LoadDataFTP(ServerIdFTP); });
                ChildWnd.ThreeColWindShow(dt, "Destination Selection", "Server", "User", "Location");
            }
            else if (Obj.ToString() == "SelectPath")
            {
                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                // Show the FolderBrowserDialog.  
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    ServerPath = folderDlg.SelectedPath;
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                }
            }
        }

        private static SerialPort spComPort = new SerialPort();
        public string Portweight, weight;
        public void WeightAvailablePorts()
        {
            try
            {
                string[] s = SerialPort.GetPortNames();
                string ports = "";
                for (int i = 0; i < s.Length; i++)
                {
                    ports = ports + s[i] + ",";
                }
                MessageBox.Show("Available Ports are " + ports);
            }
            catch (Exception Ex) { spComPort.Close(); error_log.errorlog(Ex.Message); }
        }
        public void WeightPortDataDetails()
        {
            try
            {
                var portExists = SerialPort.GetPortNames().Any(x => x == WTSerialPort);
                if (portExists == true)
                {
                    if (spComPort.IsOpen) { spComPort.Close(); }
                    if (!(spComPort.IsOpen == true))
                    {
                        spComPort.PortName = WTSerialPort;
                        spComPort.BaudRate = WTBaudRate;
                        spComPort.Open();
                    }
                    Thread.Sleep(100);

                    spComPort.Handshake = Handshake.RequestToSendXOnXOff;
                    spComPort.BreakState = false;
                    Portweight = spComPort.ReadExisting();
                    spComPort.Close();
                    MessageBox.Show("Port received data is " + Portweight + ". After Splitups : " + errorlogforweighingmachine(Portweight.Split(null)));
                }
            }
            catch (Exception Ex) { spComPort.Close(); error_log.errorlog(Ex.Message); }
        }
        public void WeightPortDataDetailsAll()
        {
            try
            {
                Portweight = "";
                string[] s = SerialPort.GetPortNames();
                for (int i = 0; i < s.Length; i++)
                {
                    string port = s[i];
                    var portExists = SerialPort.GetPortNames().Any(x => x == port);
                    if (portExists == true)
                    {
                        if (spComPort.IsOpen) { spComPort.Close(); }
                        if (!(spComPort.IsOpen == true))
                        {
                            spComPort.PortName = port;
                            spComPort.Open();
                        }
                        Thread.Sleep(100);

                        spComPort.Handshake = Handshake.RequestToSendXOnXOff;
                        spComPort.BreakState = false;
                        Portweight = spComPort.ReadExisting();
                        spComPort.Close();
                        MessageBox.Show(port+"Port received data is " + Portweight + ". After Splitups : " + errorlogforweighingmachine(Portweight.Split(null)));
                    }
                }
            }
            catch (Exception Ex) { spComPort.Close(); error_log.errorlog(Ex.Message); }
        }
        public void WeightWeightDetails()
        {
            try
            {
                var portExists = SerialPort.GetPortNames().Any(x => x == WTSerialPort);
                if (portExists == true)
                {
                    if (spComPort.IsOpen) { spComPort.Close(); }
                    if (!(spComPort.IsOpen == true))
                    {
                        spComPort.PortName = WTSerialPort;
                        spComPort.BaudRate = WTBaudRate;
                        spComPort.Open();
                    }
                    Thread.Sleep(100);

                    spComPort.Handshake = Handshake.RequestToSendXOnXOff;
                    spComPort.BreakState = false;
                    Portweight = spComPort.ReadExisting();
                    if (Portweight == "")
                    {
                        spComPort.Close();
                        if (!(spComPort.IsOpen == true))
                        {
                            spComPort.PortName = WTSerialPort;
                            spComPort.BaudRate = WTBaudRate;
                            spComPort.Open();
                        }
                        Portweight = spComPort.ReadExisting();
                        string[] strar = Portweight.Split(null);
                        weight = strar[WTLocation];
                    }
                    else
                    {
                        string[] strar = Portweight.Split(null);
                        weight = strar[WTLocation];
                    }
                    spComPort.Close(); //ST,GS,+0002.72kgat
                    weight = weight.Replace("ST", "").Replace("GS", "").Replace(",", "").Replace("kg", "").Replace("at", "").Replace("+", "");
                    MessageBox.Show("Weight is " + weight);
                }
            }
            catch (Exception Ex) { spComPort.Close(); error_log.errorlog(Ex.Message); }
        }
        public string errorlogforweighingmachine(string[] strar)
        {
            string result = "";
            if (strar.Length >= 9)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6] + "at 7:" + strar[7] + "at 8:" + strar[8]; }
            else if (strar.Length >= 8)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6] + "at 7:" + strar[7]; }
            else if (strar.Length >= 7)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6]; }
            else if (strar.Length >= 6)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5]; }
            else if (strar.Length >= 5)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4]; }
            else if (strar.Length >= 4)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3]; }
            else if (strar.Length >= 3)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2]; }
            else if (strar.Length >= 2)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1]; }
            else if (strar.Length >= 1)
            { result = "at 0:" + strar[0]; }
            else { result = "length:" + strar.Length; }
            return result;
        }
        public void ViewLogDetails()
        {
            try
            {
                string line; 
                StreamReader sr = new StreamReader(@"C:\SkynetICT\CasSalesERRORLOG.txt");
                line = sr.ReadLine();
                while (line != null)
                {
                    LogInfoDetails.Add(new LogInfo { LogDet = line });
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception Ex) { error_log.errorlog("View log details:" + Ex.Message); }
        }
        public void ClearLogDetails()
        {
            try
            {
                File.WriteAllText(@"C:\SkynetICT\CasSalesERRORLOG.txt", String.Empty);
                LogInfoDetails.Clear();
            }
            catch (Exception Ex) { error_log.errorlog("Clear log details:" + Ex.Message); }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("Internet Checking : " + ex.ToString());
                return false;
            }
        }

        private void DeleteDataFTP()
        {
            if (ServerIdFTP > 0)
            {
                ExecuteQuery($"DELETE FROM tblFTPConfig WHERE FTPConfigID = {ServerIdFTP}");
            }
            else { MessageBox.Show("There is no data to delete", "Warning"); }
        }

        private void DeleteDataWPConfig()
        {
            if (WPID > 0)
            {
                ExecuteQuery($"DELETE FROM tblWPConfig WHERE WPID = {WPID}");
            }
            else { MessageBox.Show("There is no data to delete", "Warning"); }
        }

        private void LoadDataFTP()
        {
            try
            {
                SqlDataReader dr = GetDataReader("Select * from tblFTPConfig where Status = 1");
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        ServerIdFTP = Convert.ToInt64(dr["FTPConfigID"]);
                        ServerFTP = dr["FTPServer"].ToString();
                        LocationFTP = dr["FTPLocation"].ToString();
                        FileNameMainFTP = dr["FTPMainFile"].ToString();
                        UserNameFTP = dr["FTPUser"].ToString();
                        MyWind.pwdUserPwdFTP.Password = dr["FTPPassword"].ToString();
                    }
                }
                else
                {
                    ServerFTP = AMGenFunction.FTPHost;
                    LocationFTP = AMGenFunction.FTPFilePath;
                    FileNameMainFTP = AMGenFunction.FTPMainFileName;
                    UserNameFTP = AMGenFunction.FTPUser;
                    MyWind.pwdUserPwdFTP.Password = AMGenFunction.FTPPassword;
                }
                dr.Close();
            }
            catch (Exception ex) { error_log.errorlog("LoadDataFTP at XMLSettingVM:" + ex.ToString()); }
        }

        private void LoadDataWPConfig()
        {
            try
            {
                WPID = 0; WTSerialPort = ""; WTBaudRate = 9600; WTDataBits = 8; WTLocation = 0; WTDividend = 0; SleepTime = 250; RptPrinter = ""; CNPrinter = ""; StickerPrinter = ""; CNPrinterIsActive = false; StickerPrinterIsActive = false;
                SqlDataReader dr = GetDataReader("Select * from tblWPConfig where PCNAME = HOST_NAME()");
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        WPID = Convert.ToInt64(dr["WPID"]);
                        WeighMachineName = dr["WTMachineName"].ToString();
                        WTSerialPort = dr["SerialPort"].ToString();
                        WTBaudRate = dr["BaudRate"] != DBNull.Value ? Convert.ToInt32(dr["BaudRate"]) : WTBaudRate;
                        WTDataBits = dr["DataBits"] != DBNull.Value ? Convert.ToInt32(dr["DataBits"]) : WTDataBits;
                        WTLocation = dr["WTLocation"] != DBNull.Value ? Convert.ToInt32(dr["WTLocation"]) : WTLocation;
                        WTDividend = dr["WTDividend"] != DBNull.Value ? Convert.ToInt32(dr["WTDividend"]) : WTDividend;
                        SleepTime = dr["SleepTime"] != DBNull.Value ? Convert.ToInt32(dr["SleepTime"]) : SleepTime;
                        CNPrinter = dr["CNPrinter"] != DBNull.Value ? dr["CNPrinter"].ToString() : "";
                        RptPrinter = dr["RptPrinter"] != DBNull.Value ? dr["RptPrinter"].ToString() : "";
                        StickerPrinter = dr["StickerPrinter"] != DBNull.Value ? dr["StickerPrinter"].ToString() : "";
                        CNPrinterIsActive = dr["CNPrinterIsActive"] != DBNull.Value ? Convert.ToBoolean(dr["CNPrinterIsActive"]) : false;
                        StickerPrinterIsActive = dr["StickerPrinterIsActive"] != DBNull.Value ? Convert.ToBoolean(dr["StickerPrinterIsActive"]) : false;
                    }
                }
                dr.Close();
            }
            catch (Exception ex) { error_log.errorlog("LoadDataWPConfig at XMLSettingVM:" + ex.ToString()); }
        }

        private void PaymentModeLoad()
        {
            try
            {
                PaymentId = 0; PaymentNameTxt = ""; PaymentDetailsTxt = ""; IsActive = false;
                PaymentModeListItem.Clear();

                SqlDataReader dr = GetDataReader("SELECT gen.GenDetId,gen.GDDesc, gen.GDSDesc, gen.Status  FROM tblGeneralDet gen JOIN tblGeneralMaster mas ON mas.genid = mas.GenId WHERE mas.GenId = '14' AND GEN.GenId = '14'  ");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PaymentModeListItem.Add(new PaymentModeUpdtModel() { PaymentId = Convert.ToInt32(dr["GenDetId"]), PaymentCode = dr["GDDesc"].ToString(), PaymentDesc = dr["GDSDesc"].ToString(), IsActive = Convert.ToBoolean(dr["Status"]) == true ? "Active" : "InActive" });
                    }
                }
                dr.Close();
            }
            catch (Exception ex) { error_log.errorlog("PaymentModeLoad at XMLSettingVM:" + ex.ToString()); }
        }

        private int Delete()
        {
            int i = 0;

            i = ExecuteQuery($"UPDATE tblGeneralDet set Status = '0' WHERE GenDetId = {PaymentId}");

            return i;
        }

        private void SaveData()
        {
            try
            {
                MessageBoxResult MsgRes;
                if (PaymentId > 0)
                {
                    MsgRes = MessageBox.Show("Confirm modify this PaymentMode?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    ExecuteQuery($"UPDATE tblGeneraldet SET GDDesc = '{PaymentNameTxt}', GDSDesc = '{PaymentDetailsTxt}', Status = '{IsActive}' WHERE GendetId = {PaymentId}");
                    MessageBox.Show("PaymentMode modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Create this PaymentMode?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    ExecuteQuery($"INSERT INTO tblGeneralDet (GenId, GDDesc,GDSDesc, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate ) VALUES ( '14', '{PaymentNameTxt}', '{PaymentDetailsTxt}', '1', GETDATE(), null, null, '{IsActive}' , 0, null)");

                    SqlDataReader dr = GetDataReader($"SELECT MAX(GendetId) As LastId FROM tblGeneralDet");
                    PaymentId = dr.Read() ? Convert.ToInt32(dr["LastId"]) : 1; dr.Close();

                    MessageBox.Show("PaymentMode Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                PaymentModeLoad();
            }
            catch (Exception ex)
            {
                MessageBox.Show("savedata:" + ex.ToString());
            }
        }

        private int updatedata()
        {
            int i = 0;
            try
            {
                return i;
            }
            catch (Exception ex)
            {

                error_log.errorlog("Updatedata:" + ex.ToString());
                MessageBox.Show("Updatedata:" + ex.ToString());
                return i;
            }
        }

        private void LoadDataFTP(long id)
        {
            //var checkquery = (from a in CSalesEntity.tblFTPConfigs where a.Status == true && a.FTPConfigID == id select a).FirstOrDefault();
            //if (checkquery != null)
            //{
            //    ServerIdFTP = checkquery.FTPConfigID;
            //    ServerFTP = checkquery.FTPServer; LocationFTP = checkquery.FTPLocation; FileNameMainFTP = checkquery.FTPMainFile; UserNameFTP = checkquery.FTPUser; MyWind.pwdUserPwdFTP.Password = checkquery.FTPPassword;
            //}
        }

        private void LoadDataWPConfig(long id)
        {
            WPID = 0; WTSerialPort = ""; WTBaudRate = 9600; WTDataBits = 8; WTLocation = 0; WTDividend = 0; SleepTime = 250; RptPrinter = ""; CNPrinter = ""; StickerPrinter = ""; CNPrinterIsActive = false; StickerPrinterIsActive = false;
            SqlDataReader dr = GetDataReader("SELECT * FROM tblWPConfig WHERE Status = 1 AND WPID = {id}");
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    WPID = Convert.ToInt64(dr["WPID"]);
                    WeighMachineName = dr["WTMachineName"].ToString();
                    WTSerialPort = dr["SerialPort"].ToString();
                    WTBaudRate = dr["BaudRate"] != DBNull.Value ? Convert.ToInt32(dr["BaudRate"]) : WTBaudRate;
                    WTDataBits = dr["DataBits"] != DBNull.Value ? Convert.ToInt32(dr["DataBits"]) : WTDataBits;
                    WTLocation = dr["WTLocation"] != DBNull.Value ? Convert.ToInt32(dr["WTLocation"]) : WTLocation;
                    WTDividend = dr["WTDividend"] != DBNull.Value ? Convert.ToInt32(dr["WTDividend"]) : WTDividend;
                    SleepTime = dr["SleepTime"] != DBNull.Value ? Convert.ToInt32(dr["SleepTime"]) : SleepTime;
                    CNPrinter = dr["CNPrinter"] != DBNull.Value ? dr["CNPrinter"].ToString() : "";
                    RptPrinter = dr["RptPrinter"] != DBNull.Value ? dr["RptPrinter"].ToString() : "";
                    StickerPrinter = dr["StickerPrinter"] != DBNull.Value ? dr["StickerPrinter"].ToString() : "";
                    CNPrinterIsActive = dr["CNPrinterIsActive"] != DBNull.Value ? Convert.ToBoolean(dr["CNPrinterIsActive"]) : false;
                    StickerPrinterIsActive = dr["StickerPrinterIsActive"] != DBNull.Value ? Convert.ToBoolean(dr["StickerPrinterIsActive"]) : false;

                }
            }
            dr.Close();
        }

        private void ResetDataServer()
        {
            ServerConfigId = 0;
            StnCode = ""; AccYear = ""; ServerPath = ""; DBName = ""; UserName = "";
        }
        private void ResetDataFTP()
        {
            ServerIdFTP = 0;
            ServerFTP = ""; LocationFTP = ""; FileNameMainFTP = ""; UserNameFTP = "";
        }
        private void ResetDataWPConfig()
        {
            WPID = 0;
            WTSerialPort = ""; WTBaudRate = 0; WTDataBits = 0;
            WTLocation = 0; WTDividend = 0;
            CNPrinter = ""; StickerPrinter = ""; StickerPrinterIsActive = false;
        }

        private void ResetData()
        {
            try
            {
                var printerQuery = new System.Management.ManagementObjectSearcher("SELECT * from Win32_Printer");
                foreach (var printer in printerQuery.Get())
                {
                    MyWind.cmbCNPrinters.Items.Add(printer.GetPropertyValue("Name"));
                    MyWind.cmbStickerPrinters.Items.Add(printer.GetPropertyValue("Name"));
                }

                WPID = 0; WeighMachineName = ""; WTSerialPort = ""; WTBaudRate = 9600; WTDataBits = 0; WTLocation = 0; WTDividend = 0; SleepTime = 250;
                RptPrinter = ""; CNPrinter = ""; StickerPrinter = ""; CNPrinterIsActive = false; StickerPrinterIsActive = false;

                MyWind.tiWPConfig.IsSelected = true;
            }
            catch (Exception ex) { error_log.errorlog("ResetData at XMLSettingVM:" + ex.ToString()); }
        }
    }

    public class LogInfo : AMGenFunction
    {
        private string _LogDet;
        public string LogDet { get { return _LogDet; } set { _LogDet = value; OnPropertyChanged("LogDet"); } }
    }
}
