﻿using SkynetCashSales.General;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Linq;

namespace SkynetCashSales.ViewModel.Help
{
    public class ServerConfigVM : AMGenFunction
    {
        public ServerConfigVM()
        {

        }
        private string _ServerName, _InstanceName, _DBName, _LoginName;
        private SecureString _LoginPassword;

        public string ServerName { get { return _ServerName; } set { _ServerName = value; OnPropertyChanged("ServerName"); } }
        public string InstanceName { get { return _InstanceName; } set { _InstanceName = value; OnPropertyChanged("InstanceName"); } }
        public string DBName { get { return _DBName; } set { _DBName = value; OnPropertyChanged("DBName"); } }
        public string LoginName { get { return _LoginName; } set { _LoginName = value; OnPropertyChanged("LoginName"); } }
        public SecureString LoginPassword { get { return _LoginPassword; } set { _LoginPassword = value; OnPropertyChanged("LoginPassword"); } }

        private ICommand _Save, _Cancel;
        public ICommand Save { get { if (_Save == null) _Save = new RelayCommand(Parameter => Savedata()); return _Save; } }
        public ICommand Cancel { get { if (_Cancel == null) _Cancel = new RelayCommand(Parameter => System.Windows.Application.Current.Shutdown()); return _Cancel; } }

        private void Savedata()
        {
            string ConStr = string.Format("Server={0};", ServerName + "\\" + InstanceName);
            string password = new System.Net.NetworkCredential(string.Empty, LoginPassword).Password;
            ConStr += string.Format("UId={0};PWD={1};", LoginName, password);
            ConStr += "Connect Timeout=1;Max Pool Size=1;Connection Lifetime=10;";

            SqlConnection connection = new SqlConnection(ConStr);

            try
            {
                connection.Open();
            }
            catch (SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString(), "SQL Server Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    XElement XMLFile = new XElement("ServerConfig", new XElement("ServerName", ServerName), new XElement("InstanceName", InstanceName), new XElement("DatabaseName", DBName), new XElement("LoginName", LoginName), new XElement("LoginPassword", password));
                    XMLFile.Save(Application.StartupPath + "\\" + "ServerConfig.xml");

                    connection.Close(); connection.Dispose();

                    CloseWind();
                }
            }
        }

    }
}
