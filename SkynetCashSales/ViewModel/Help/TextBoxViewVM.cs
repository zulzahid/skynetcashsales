﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkynetCashSales.ViewModel.Help
{
    class TextBoxViewVM : AMGenFunction
    {
        private FrmTextBoxView MyWind;

        private String _Content, _WindHdr;
        public string Content { get { return _Content; } set { _Content = value; OnPropertyChanged("Content"); } }
        public string WindHdr { get { return _WindHdr; } set { _WindHdr = value; OnPropertyChanged("WindHdr"); } }
        public TextBoxViewVM(string features, FrmTextBoxView wind, string windTitle)
        {
            Content = features;
            MyWind = wind;
            WindHdr = windTitle;
        }
    }
}
