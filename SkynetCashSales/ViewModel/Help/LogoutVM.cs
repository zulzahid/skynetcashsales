﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.ViewModel.Help;
using System;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel
{
    public class LogoutVM : AMGenFunction
    {
        public FrmLogout MyWind;
        public LogoutVM(FrmLogout Wind)
        {
            MyWind = Wind;
            CompCode = MyCompanyCode; CompName = MyCompanyName;
            MyWind.txtUserName.Focus();
            //CurrentVersion = (from a in CSalesEntity.tblStnSetupControls where a.IsActive == true select a.SetupVersion).FirstOrDefault();

            CurrentVersion = AMGenFunction.CurrentVersion;
        }

        private int  _UserId;
        private string _CompCode, _CompName, _AccYear, _UserName, _CurrentVersion;
        public string CompCode { get { return _CompCode; } set { _CompCode = value; OnPropertyChanged("CompCode"); } }
        public string CompName { get { return _CompName; } set { _CompName = value; OnPropertyChanged("CompName"); } }
        public string AccYear { get { return _AccYear; } set { _AccYear = value; OnPropertyChanged("AccYear"); } }
        public int UserId { get { return _UserId; } set { _UserId = value; OnPropertyChanged("UserId"); } }
        public string UserName { get { return _UserName; } set { _UserName = value; OnPropertyChanged("UserName"); } }
        public string CurrentVersion { get { return _CurrentVersion; } set { _CurrentVersion = value; OnPropertyChanged("CurrentVersion"); } }


        private ICommand _FocusPwd, _Login, _Cancel;
        public ICommand FocusPwd { get { if (_FocusPwd == null) _FocusPwd = new RelayCommand(Parameter => Password(Parameter)); return _FocusPwd; } }
        public ICommand Login { get { if (_Login == null) _Login = new RelayCommand(Parameter => ExecuteLogin(Parameter)); return _Login; } }
        public ICommand Cancel { get { if (_Cancel == null) _Cancel = new RelayCommand(NoParameter => CloseLogin()); return _Cancel; } }

        public void Password(object Obj)
        {
            var MyPwd = Obj as PasswordBox;
            if (UserName != "") MyPwd.Focus();
        }

        public void CloseLogin()
        {
            foreach (System.Windows.Window window in System.Windows.Application.Current.Windows)
            {
                if (window.DataContext == this)
                {
                    MsgRes = MessageBox.Show("You Want to Shutdown?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes) Application.Current.Shutdown();
                }
            }
        }

        void ExecuteLogin(object parameter)
        {
            string LoginUser = _UserName;
            var passwordBox = parameter as PasswordBox;
            var Password = passwordBox.Password;

            SqlDataReader dr = GetDataReader($"SELECT UserId, UserName, Authentication FROM tblUserMaster WHERE UserName = '{LoginUser}' AND Password = '{Password}' AND Status = 1");
            if (dr.Read())
            {
                
                //////  Added by Ilango  --->    START
                if (LoginUserName != dr["UserName"].ToString())
                {
                    if (MainWind.Visibility == Visibility.Visible)
                    {
                        string name = "", typeName = "";
                        var allWindows = Application.Current.Windows;
                        foreach (var window in allWindows)
                        {
                            Window win = window as Window;
                            name = win.Name; //mywindow
                            typeName = win.GetType().Name; //MainWindow
                            if (typeName != "MainWindow" && typeName != "FrmLogout")
                            {
                                win.Close();
                            }
                        }
                    }
                }
                //////  Added by Ilango  --->    END

                LoginUserId = Convert.ToInt32(dr["UserId"]);
                LoginUserName = dr["UserName"].ToString();
                LoginUserAuthentication = dr["Authentication"].ToString();
                dr.Close();

                MyCompanyCode = CompCode; MyCompanyName = CompName;
                MyCompanySubName = string.IsNullOrEmpty(MyCompanySubName) ? "" : MyCompanySubName;
                MainWindowVM vm = new MainWindowVM(MainWind);

                //UserActivities.UserEvents("LOGIN", MyCompanyCode.Trim() + " user:" + LoginUser.Trim() + " successfully logged in");
                MainWind.Activate();
                CloseWind();
            }
            else 
            { 
                passwordBox.Password = ""; 
                passwordBox.Focus(); 
                MessageBox.Show("Please check the User Name and Password", "Information", MessageBoxButton.OK, MessageBoxImage.Information); 
            }
        }
    }
}
