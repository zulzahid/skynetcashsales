﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;

namespace SkynetCashSales.ViewModel.Help
{
    public class DbConversionVM : AMGenFunction
    {
        private SQLiteConnection SQLiteCon;
        private SQLiteDataAdapter SQLiteAdapter;
        private frmDbConversion MyWind;
        public DbConversionVM(frmDbConversion wind)
        {
            MyWind = wind;
            TableList = new ObservableCollection<ItemInfo>();
            ResetData();
        }

        public string SQLiteDb { get { return GetValue(() => SQLiteDb); } set { SetValue(() => SQLiteDb, value); OnPropertyChanged("SQLiteDb"); } }
        public int TotalProgress { get { return GetValue(() => TotalProgress); } set { SetValue(() => TotalProgress, value); OnPropertyChanged("TotalProgress"); } }
        public int CurrentProgress { get { return GetValue(() => CurrentProgress); } set { SetValue(() => CurrentProgress, value); OnPropertyChanged("CurrentProgress"); } }

        private ObservableCollection<ItemInfo> _TableList = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> TableList { get { return _TableList; } set { _TableList = value; OnPropertyChanged("TableList"); } }

        private ObservableCollection<ItemInfo> _SelectedItem = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }
        private int SelectedIndex;

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "Select SQLite Database":
                        {
                            OpenFileDialog OFD = new OpenFileDialog();
                            OFD.Title = "Browse SQLite Db Files";
                            OFD.DefaultExt = "db";
                            OFD.Filter = "SQLite Files|*.db";

                            if (OFD.FileName != null && OFD.ShowDialog() == DialogResult.OK)
                            { 
                                SQLiteDb = OFD.FileName; 
                            }
                        }
                        break;
                    case "Check SQLite Connection":
                        {
                            if (SQLiteDb.Trim() != "")
                            {
                                if (System.IO.File.Exists(@SQLiteDb))
                                {
                                    string SQLiteConStr = @"Data Source=" + SQLiteDb + "; Version=3; New=False; Compress=True; Pooling=True; Max Pool Size=100;";

                                    try
                                    {
                                        SQLiteCon = new SQLiteConnection(SQLiteConStr);
                                        SQLiteCon.Open();
                                        System.Windows.MessageBox.Show("SQLiteConnection successfully opened", "Information", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                                    }
                                    catch (SQLiteException SQLiteEx)
                                    {
                                        System.Windows.MessageBox.Show(SQLiteEx.Message, "SQLiteException", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                                    }
                                }
                                else System.Windows.MessageBox.Show("Invalid db file selection", "Information", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                            }
                        }
                        break;
                    case "Start Conversion":
                        {
                            for (int i = 0; i < TableList.Count; i++)
                            {
                                if(TableList[i].TableName != null && TableList[i].TableName.Trim() != "" && TableList[i].Status == false)
                                {
                                    SelectedIndex = i;
                                    try
                                    {
                                        DoConversion(TableList[i].TableDesc);
                                    }
                                    catch (Exception Ex1) { System.Windows.MessageBox.Show(Ex1.Message, "Exception", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error); }
                                }                                
                            }
                        }
                        break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                System.Windows.MessageBox.Show(Ex.Message, "Exception", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                SelectedIndex = 0;
            }
        }

        private void DoConversion(string tblName)
        {                         
            if (SQLiteCon.State == ConnectionState.Closed) SQLiteCon.Open();

            if (SQLiteCon.State == ConnectionState.Open)
            {
                DataTable dt, dt1, dt2;
                DataView view;
                switch (tblName)
                {
                    case "StationProfile":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT CompName, CompCode, CompType, SubName, CashAccNum, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, Fax, EMail, TaxTitle, TaxNo, Status FROM tblStationProfile WHERE Status = 1 AND ToDate IS NULL", SQLiteCon);
                            //SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT CompName, CompCode, CompType, SubName, CashAccNum, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, Fax, EMail, TaxTitle, TaxNo, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, Status FROM tblStationProfile WHERE Status = 1 AND ToDate IS NULL", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblStationProfile");
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblStationProfile (CompName, CompCode, CompType, SubName, CashAccNum, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, Fax, EMail, TaxTitle, TaxNo, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate)" +
                                        $" VALUES('{row["CompName"].ToString()}', '{row["CompCode"].ToString()}', '{row["CompType"].ToString()}', '{row["SubName"].ToString()}', '{row["CashAccNum"].ToString()}', '{row["RegNum"].ToString()}', '{row["IATACode"].ToString()}', '{row["HQCode"].ToString()}', '{row["Address1"].ToString()}', '{row["Address2"].ToString()}', '{row["Address3"].ToString()}', '{row["City"].ToString()}', '{row["State"].ToString()}', '{row["Country"].ToString()}', '{row["ZipCode"].ToString()}', '{row["PhoneNum"].ToString()}', '{row["Fax"].ToString()}', '{row["EMail"].ToString()}', '{row["TaxTitle"].ToString()}', '{row["TaxNo"].ToString()}'," +
                                        $" GETDATE(), NULL, 0, GETDATE(), 0, NULL, 1, 0, NULL)");

                                    //ExecuteQuery($"INSERT INTO tblStationProfile (CompName, CompCode, CompType, SubName, CashAccNum, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, Fax, EMail, TaxTitle, TaxNo, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate)" +
                                    //    $" VALUES('{row["CompName"].ToString()}', '{row["CompCode"].ToString()}', '{row["CompType"].ToString()}', '{row["SubName"].ToString()}', '{row["CashAccNum"].ToString()}', '{row["RegNum"].ToString()}', '{row["IATACode"].ToString()}', '{row["HQCode"].ToString()}', '{row["Address1"].ToString()}', '{row["Address2"].ToString()}', '{row["Address3"].ToString()}', '{row["City"].ToString()}', '{row["State"].ToString()}', '{row["Country"].ToString()}', '{row["ZipCode"].ToString()}', '{row["PhoneNum"].ToString()}', '{row["Fax"].ToString()}', '{row["EMail"].ToString()}', '{row["TaxTitle"].ToString()}', '{row["TaxNo"].ToString()}'," +
                                    //    $" '{Convert.ToDateTime(row["FromDate"]).ToString(sqlDateLongFormat)}', NULL, {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, 1, 0, NULL)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "AWBAllocation":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblAWBAllocation where IsDelete = 0", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblAWBAllocation");
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblAWBAllocation(AllocateNum, AllocateDate, CompanyName, StnCode, NoOfAWB, AWBPrefix, AWBSuffix, FromAWB, FromAWBRunNum, ToAWB, ToAWBRunNum, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, IsDelete)" +
                                        $" VALUES('{row["AllocateNum"].ToString()}', '{Convert.ToDateTime(row["AllocateDate"]).ToString(sqlDateLongFormat)}', '{row["CompanyName"].ToString()}', '{row["StnCode"].ToString()}', {Convert.ToInt32(row["NoOfAWB"])}, '', '', '{row["FromAWB"].ToString()}', {Convert.ToInt64(row["FromAWBRunNum"])}, '{row["ToAWB"].ToString()}', {Convert.ToInt64(row["ToAWBRunNum"])}, {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{row["Status"].ToString()}', 0, 0)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "SystemSetting":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblWPConfig WHERE Status = 1", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"IF EXISTS(SELECT NAME FROM sys.tables WHERE NAME = 'tblWPConfig') DROP TABLE tblWPConfig");

                                ExecuteQuery("CREATE TABLE [dbo].[tblWPConfig](" +
                                        " [WPID] [int] IDENTITY(1,1) NOT NULL," +
                                        " [StnCode] [varchar](10) NOT NULL," +
                                        " [SubName] [varchar](10) NOT NULL," +
                                        " [WTMachineName] [varchar](120) NOT NULL," +
                                        " [SerialPort] [varchar](10) NOT NULL," +
                                        " [BaudRate] [int] NOT NULL," +
                                        " [DataBits] [int] NOT NULL," +
                                        " [WTLocation] [int] NOT NULL," +
                                        " [WTDividend] [int] NOT NULL," +
                                        " [SleepTime] [int] NOT NULL," +
                                        " [CNPrinter] [varchar](120) NOT NULL," +
                                        " [RptPrinter] [varchar](120) NOT NULL," +
                                        " [IsGenXML] [bit] NOT NULL," +
                                        " [PCName] [varchar](50) NOT NULL," +
                                        " [CUserID] [int] NOT NULL," +
                                        " [CDate] [datetime] NOT NULL," +
                                        " [Status] [bit] NOT NULL," +
                                        " [IsExported] [bit] NOT NULL," +
                                        " [ExportedDate] [datetime] NULL," +
                                        " CONSTRAINT [PK_tblWPConfig] PRIMARY KEY CLUSTERED ([WPID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblWPConfig (StnCode, SubName, WTMachineName, SerialPort, BaudRate, DataBits, WTLocation, WTDividend, SleepTime, CNPrinter, RptPrinter, IsGenXML, PCName, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                        $" VALUES('{row["StnCode"].ToString()}', '{row["SubName"].ToString()}', '', '{row["SerialPort"].ToString()}', {Convert.ToInt32(row["BaudRate"])}, {Convert.ToInt32(row["DataBits"])}, {Convert.ToInt32(row["WTLocation"])}, {Convert.ToInt32(row["WTDividend"])}, {Convert.ToInt32(row["SleepTime"])}, '{row["CNPrinter"].ToString()}', '{row["RptPrinter"].ToString()}', 0, HOST_NAME(), 0, GETDATE(), 1, 0, NULL)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "UserMaster":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT UserId, UserName, Password, Authentication, CreateBy, CreateDate, Status FROM tblUserMaster WHERE Status = 1", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblUserMaster");
                                ExecuteQuery("IF(IDENT_CURRENT('tblUserMaster') IS NOT NULL) SET IDENTITY_INSERT tblUserMaster ON");
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblUserMaster (UserId, UserName, Password, Authentication, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate)" +
                                        $" VALUES({Convert.ToInt32(row["UserId"])}, '{row["UserName"].ToString()}', '{row["Password"].ToString()}', '{row["Authentication"].ToString()}', {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, 1, 0, NULL)");
                                }
                                ExecuteQuery("IF(IDENT_CURRENT('tblUserMaster') IS NOT NULL) SET IDENTITY_INSERT tblUserMaster OFF");
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "Destination":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted FROM tblDestination WHERE IsDeleted = 0", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblDestination");
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate)" +
                                        $" VALUES('{row["DestCode"].ToString()}', '{row["DestName"].ToString().Replace("'", "''")}', '{row["StationCode"].ToString()}', '{row["State"].ToString().Replace("'", "''")}', '{row["Country"].ToString().Replace("'", "''")}', '{row["Region"].ToString()}', '{Convert.ToBoolean(row["IsODA"])}', 0, 0, NULL)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "Zone":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT ZoneCode, ZoneDesc, ZoneType FROM tblZone", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblZone");
                                ExecuteQuery($"TRUNCATE TABLE tblZoneDetail");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblZone (ZoneCode, ZoneDesc, ZoneType, IsExported, ExportedDate) VALUES('{row["ZoneCode"].ToString()}', '{row["ZoneDesc"].ToString()}', '{row["ZoneType"].ToString()}', 0, NULL)");

                                    dt1 = new DataTable();
                                    SQLiteAdapter = new SQLiteDataAdapter($"SELECT ZoneCode, DestCode FROM tblZoneDetail WHERE ZoneCode = '{row["ZoneCode"].ToString()}'", SQLiteCon);
                                    SQLiteAdapter.Fill(dt1);
                                    if (dt1.Rows.Count > 0)
                                    {
                                        foreach (DataRow item in dt1.Rows)
                                        {
                                            ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('{item["ZoneCode"].ToString()}', '{item["DestCode"].ToString()}', 0, NULL)");
                                        }
                                    }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "MotorBikeZone":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT ZoneCode, ShipmentType, IsActive, CreateBy, CreateDate FROM tblMotorBikeZone", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblMotorBikeZone");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblMotorBikeZone (ZoneCode, ShipmentType, IsActive, CreateBy, CreateDate, ModifyBy, ModifyDate, IsExported, ExportedDate)" +
                                        $" VALUES('{row["ZoneCode"].ToString()}', '{row["ShipmentType"].ToString()}', '{Convert.ToBoolean(row["IsActive"])}', {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, 0, NULL)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "Quotation":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT QuotationId, QuotationNum, QuotationType, RefQuotationId, SurCharge, TaxTitle, TaxPercentage, InsuranceNormal, InsuranceInternational, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, IsDeleted, IsExported, ExportedDate FROM tblQuotation WHERE IsDeleted = 0 AND ToDate IS NULL", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblQuotation");
                                ExecuteQuery($"TRUNCATE TABLE tblQuotationDet");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblQuotation (QuotationId, QuotationNum, QuotationType, RefQuotationId, SurCharge, TaxTitle, TaxPercentage, InsuranceNormal, InsuranceInternational, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, IsDeleted, IsExported, ExportedDate)" +
                                        $" VALUES({Convert.ToInt32(row["QuotationId"])}, '{row["QuotationNum"].ToString()}', '{row["QuotationType"].ToString()}', {Convert.ToInt32(row["RefQuotationId"])}, {Convert.ToDecimal(row["SurCharge"])}, '{row["TaxTitle"].ToString()}', {row["TaxPercentage"]}, {Convert.ToDecimal(row["InsuranceNormal"])}, {Convert.ToDecimal(row["InsuranceInternational"])}, '{Convert.ToDateTime(row["FromDate"]).ToString(sqlDateLongFormat)}', NULL, {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, 0, 0, NULL)");

                                    dt1 = new DataTable();
                                    SQLiteAdapter = new SQLiteDataAdapter($"SELECT * FROM tblQuotationDet WHERE QuotationId = {Convert.ToInt32(row["QuotationId"])} AND ToDate IS NULL", SQLiteCon);
                                    SQLiteAdapter.Fill(dt1);
                                    if (dt1.Rows.Count > 0)
                                    {
                                        int i = 0;
                                        foreach (DataRow item in dt1.Rows)
                                        {
                                            i += 1;
                                            ExecuteQuery($"INSERT INTO tblQuotationDet (QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstRate, FirstWeight, AddRate, AddWeight, SurCharge, FromDate, ToDate, IsExported, ExportedDate, StnCode, SubName)" +
                                                $" VALUES({Convert.ToInt32(row["QuotationId"])}, {i}, '{item["FromZone"].ToString()}', '{item["ToZone"].ToString()}', '{item["ShipmentType"].ToString()}', {Convert.ToDecimal(item["FirstRate"])}, {Convert.ToDecimal(item["FirstWeight"])}, {Convert.ToDecimal(item["AddRate"])}, {Convert.ToDecimal(item["AddWeight"])}, {Convert.ToDecimal(item["SurCharge"])}, '{Convert.ToDateTime(item["FromDate"]).ToString(sqlDateLongFormat)}', NULL, 0, NULL, '{item["StnCode"].ToString()}', '{item["SubName"].ToString()}')");
                                        }
                                    }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "PromoCode":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter($"SELECT DISTINCT AgentCode, StnCode, SubName, PromoCode, PromoTitle, PromoDesc, Percentage, IsPercentage, ShipmentType, FromDate, ToDate, CUserID, Status FROM tblAllPromotion", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblAllPromotion");
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblAllPromotion (AgentCode, StnCode, SubName, PromoCode, PromoTitle, PromoDesc, Percentage, IsPercentage, ShipmentType, FromDate, ToDate, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                        $" VALUES('{row["AgentCode"].ToString()}', '{row["StnCode"].ToString()}', '{row["SubName"].ToString()}', '{row["PromoCode"].ToString()}', '{row["PromoTitle"].ToString().Replace("'","''")}', '{row["PromoDesc"].ToString().Replace("'", "''")}', {Convert.ToDecimal(row["Percentage"])}, '{Convert.ToBoolean(row["IsPercentage"])}', '{row["ShipmentType"].ToString()}', '{Convert.ToDateTime(row["FromDate"]).ToString(sqlDateLongFormat)}', '{Convert.ToDateTime(row["ToDate"]).ToString(sqlDateLongFormat)}', {Convert.ToInt32(row["CUserID"])}, GETDATE(), '{Convert.ToBoolean(row["Status"])}', 0, NULL)");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "StaffDiscount":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter($"SELECT * FROM tblStaff", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblStaff");
                                ExecuteQuery($"TRUNCATE TABLE tblStaffDiscount");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblStaff (StaffName, StaffCode, StaffTypeId, SupervisorId, ICNum, SOCSO, EPFNum, TaxFileNum, Salary, JoinedDate, LeftDate, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, MobNum, PagerNum, Fax, EMail, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate)" +
                                        $" VALUES('{row["StaffName"].ToString().Replace("'", "''")}', '{row["StaffCode"].ToString()}', {Convert.ToInt32(row["StaffTypeId"])}, {Convert.ToInt32(row["SupervisorId"])}, '{row["ICNum"].ToString()}', '{row["SOCSO"].ToString()}', '{row["EPFNum"].ToString()}', '{row["TaxFileNum"].ToString()}', {Convert.ToDecimal(row["Salary"])}, '{Convert.ToDateTime(row["JoinedDate"]).ToString(sqlDateLongFormat)}', NULL, '{row["Address1"].ToString().Replace("'","''")}', '{row["Address2"].ToString().Replace("'", "''")}', '{row["Address3"].ToString().Replace("'", "''")}', '{row["City"].ToString().Replace("'", "''")}', '{row["State"].ToString().Replace("'", "''")}', '{row["Country"].ToString().Replace("'", "''")}', '{row["ZipCode"].ToString()}', '{row["PhoneNum"].ToString()}', '{row["MobNum"].ToString()}', '{row["PagerNum"].ToString()}', '{row["Fax"].ToString()}', '{row["EMail"].ToString()}', '{Convert.ToInt32(row["CreateBy"])}', '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{Convert.ToBoolean(row["Status"])}', 0, NULL)");
                                }

                                dt1 = new DataTable();
                                SQLiteAdapter = new SQLiteDataAdapter($"SELECT * FROM tblStaffDiscount", SQLiteCon);
                                SQLiteAdapter.Fill(dt1);
                                if (dt1.Rows.Count > 0)
                                {
                                    foreach (DataRow item in dt1.Rows)
                                    {
                                        ExecuteQuery($"INSERT INTO tblStaffDiscount (IsZone, Code, IsPercentage, DiscountValue, MonthlyValid, FromDate, ToDate, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsExported, ExportedDate)" +
                                            $" VALUES('{Convert.ToBoolean(item["IsZone"])}', '{item["Code"].ToString()}', '{Convert.ToBoolean(item["IsPercentage"])}', {Convert.ToDecimal(item["DiscountValue"])}, {Convert.ToInt32(item["MonthlyValid"])}, '{Convert.ToDateTime(item["FromDate"]).ToString(sqlDateLongFormat)}', NULL, '{item["CreatedBy"].ToString()}', '{Convert.ToDateTime(item["CreatedDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{Convert.ToBoolean(item["Status"])}', 0, NULL)");
                                    }
                                }

                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "StationeryItems":
                        {
                            // 1. tblItemMaster
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT ItemId, ItemCode, ItemDesc, ItemType, SerialNo, Brand, Manufacturer, Unit1Id, Unit1, Unit1Cost, Unit1SalRateToStation, Unit1SalRateToBranch, Unit1SalRateToDept, Unit2Id, Unit2, UnitFactor, Unit2SalRateToStation, Unit2SalRateToBranch, Unit2SalRateToDept, OpeningStk, MinStk, MaxStk, ReOrder, CreateBy, CreateDate, ModifyBy, ModifyDate, Status FROM tblItemMaster", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                ExecuteQuery($"TRUNCATE TABLE tblItemMaster");
                                ExecuteQuery("IF(IDENT_CURRENT('tblItemMaster') IS NOT NULL) SET IDENTITY_INSERT tblItemMaster ON");

                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    ExecuteQuery($"INSERT INTO tblItemMaster(ItemId, ItemCode, ItemDesc, ItemType, SerialNo, Brand, Manufacturer, Unit1Id, Unit1, Unit1Cost, Unit1SalRateToStation, Unit1SalRateToBranch, Unit1SalRateToDept, Unit2Id, Unit2, UnitFactor, Unit2SalRateToStation, Unit2SalRateToBranch, Unit2SalRateToDept, OpeningStk, MinStk, MaxStk, ReOrder, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                                        $" VALUES({Convert.ToInt32(row["ItemId"])}, '{row["ItemCode"].ToString()}', '{row["ItemDesc"].ToString()}', {Convert.ToInt32(row["ItemType"])}, '{row["SerialNo"].ToString()}', '{row["Brand"].ToString()}', '{row["Manufacturer"].ToString()}'," +
                                        $" {Convert.ToInt32(row["Unit1Id"])}, '{row["Unit1"].ToString()}', {Convert.ToDecimal(row["Unit1Cost"])}, {Convert.ToDecimal(row["Unit1SalRateToStation"])}, {Convert.ToDecimal(row["Unit1SalRateToBranch"])}, {Convert.ToDecimal(row["Unit1SalRateToDept"])}," +
                                        $" {Convert.ToInt32(row["Unit2Id"])}, '{row["Unit2"].ToString()}', {Convert.ToDecimal(row["UnitFactor"])}, {Convert.ToDecimal(row["Unit2SalRateToStation"])}, {Convert.ToDecimal(row["Unit2SalRateToBranch"])}, {Convert.ToDecimal(row["Unit2SalRateToDept"])}," +
                                        $" {Convert.ToDecimal(row["OpeningStk"])}, {Convert.ToDecimal(row["MinStk"])}, {Convert.ToDecimal(row["MaxStk"])}, {Convert.ToDecimal(row["ReOrder"])}, {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, null, '{Convert.ToBoolean(row["Status"])}')");
                                }

                                ExecuteQuery("IF(IDENT_CURRENT('tblItemMaster') IS NOT NULL) SET IDENTITY_INSERT tblItemMaster OFF");

                                // 2. tblStock
                                dt = new DataTable();
                                SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT StockId, ItemId, Opening, StockAdj, Purchase, PurchaseRet, Receive, Issue, Sales, SalesRet, Excess, Short, Closing FROM tblStock", SQLiteCon);
                                SQLiteAdapter.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                    ExecuteQuery($"TRUNCATE TABLE tblStock");
                                    ExecuteQuery("IF(IDENT_CURRENT('tblStock') IS NOT NULL) SET IDENTITY_INSERT tblStock ON");

                                    foreach (DataRow row in dt.Rows)
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));

                                        ExecuteQuery($"INSERT INTO tblStock(StockId, ItemId, Opening, StockAdj, Purchase, PurchaseRet, Receive, Issue, Sales, SalesRet, Excess, Short, Closing)" +
                                            $" VALUES({Convert.ToInt32(row["StockId"])}, {Convert.ToInt32(row["ItemId"])}, {Convert.ToDecimal(row["Opening"])}, {Convert.ToDecimal(row["StockAdj"])}, {Convert.ToDecimal(row["Purchase"])}, {Convert.ToDecimal(row["PurchaseRet"])}, {Convert.ToDecimal(row["Receive"])}, {Convert.ToDecimal(row["Issue"])}, {Convert.ToDecimal(row["Sales"])}, {Convert.ToDecimal(row["SalesRet"])}, {Convert.ToDecimal(row["Excess"])}, {Convert.ToDecimal(row["Short"])}, {Convert.ToDecimal(row["Closing"])})");
                                    }

                                    ExecuteQuery("IF(IDENT_CURRENT('tblStock') IS NOT NULL) SET IDENTITY_INSERT tblStock OFF");
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "LodgeIn":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT Date, AWBNum, ShipmentType, NoofPcs, RecFromName, RecFromMobile, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate, Weight FROM tblLodginAWB", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;
                                ExecuteQuery($"TRUNCATE TABLE tblLodginAWB");
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                        TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                        ExecuteQuery($"INSERT INTO tblLodginAWB (Date, AWBNum, ShipmentType, NoofPcs, RecFromName, RecFromMobile, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate, Weight)" +
                                            $" VALUES('{Convert.ToDateTime(row["Date"]).ToString(sqlDateLongFormat)}', '{row["AWBNum"].ToString()}', '{row["ShipmentType"].ToString()}', {Convert.ToInt32(row["NoofPcs"])}, '{row["RecFromName"].ToString()}', '{row["RecFromMobile"].ToString()}', {Convert.ToInt32(row["CreateBy"])}, '{Convert.ToDateTime(row["CreateDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{Convert.ToBoolean(row["Status"])}', '{Convert.ToBoolean(row["IsExported"])}', NULL, '{Convert.ToDecimal(row["Weight"])}')");
                                    }
                                    catch { }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "CashSales":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT CNRunningNum, CNNum, CSNo, CSDate FROM tblLastCNNum", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    ExecuteQuery($"UPDATE tblLastCNNum SET CNRunningNum = {Convert.ToInt64(row["CNRunningNum"])}, CNNum = '{row["CNNum"].ToString()}', CSNo = '{row["CSNo"].ToString()}', CSDate = '{Convert.ToDateTime(row["CSDate"]).ToString(sqlDateLongFormat)}'");
                                }
                            }

                            ExecuteQuery($"IF EXISTS (SELECT name FROM sys.sequences WHERE name = 'CSalesNo')" +
                                $" BEGIN" +
                                $" EXEC('DROP SEQUENCE CSalesNo')" +
                                $" END");

                            //DateTime ToSaleDate = DateTime.MinValue;
                            //dt = new DataTable();
                            //dt = GetDataTable("SELECT ParamName, ParamValue FROM SystemParams WHERE ParamName = 'DbConversion' and ParamValue = 'CashSales'");
                            //if (dt.Rows.Count > 0)
                            //{
                            //    dt = new DataTable();
                            //    dt = GetDataTable("SELECT ISNULL(MAX(CDate), GETDATE()) As CDate FROM tblCashSales");
                            //    if (dt.Rows.Count > 0) ToSaleDate = Convert.ToDateTime(dt.Rows[0]["CDate"]);
                            //}
                            //else
                            //{
                            //    ExecuteQuery($"Insert into SystemParams (ParamName, ParamValue, TimeTransmitted) values ('DbConversion','CashSales', GETDATE())");
                            //    ExecuteQuery($"TRUNCATE TABLE tblCashSales");
                            //    ExecuteQuery($"TRUNCATE TABLE tblCSaleCNNum");
                            //}

                            ExecuteQuery($"TRUNCATE TABLE tblCashSales");
                            ExecuteQuery($"TRUNCATE TABLE tblCSaleCNNum");

                            dt = new DataTable();
                            //SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT CSalesNo, CSalesDate, PostCodeId, DestCode, DestStation, ShipmentType, Pieces, Weight, VLength,VBreadth, VHeight, VolWeight , Price, ShipmentTotal, ShipmentGSTValue , StationaryTotal , IsInsuranced , InsShipmentValue , InsShipmentDesc , InsValue, InsGSTValue, InsuranceTotal, CSalesTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, OtherCharges, IsManual, DiscCode, DiscPercentage, DiscAmount, TaxTitle, TaxPercentage, RoundingAmt, RefNum ,Status, PaymentMode, PaymentRefNo, PaymentRemarks FROM tblCashSales" +
                            //    $" WHERE CDate > '{ToSaleDate.ToString("yyyy-MM-dd HH:mm:ss")}' ORDER BY CSalesDate", SQLiteCon);

                            SQLiteAdapter = new SQLiteDataAdapter("SELECT DISTINCT CSalesNo, CSalesDate, PostCodeId, DestCode, DestStation, ShipmentType, Pieces, Weight, VLength,VBreadth, VHeight, VolWeight , Price, ShipmentTotal, ShipmentGSTValue , StationaryTotal , IsInsuranced , InsShipmentValue , InsShipmentDesc , InsValue, InsGSTValue, InsuranceTotal, CSalesTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, OtherCharges, IsManual, DiscCode, DiscPercentage, DiscAmount, TaxTitle, TaxPercentage, RoundingAmt, RefNum ,Status, PaymentMode, PaymentRefNo, PaymentRemarks FROM tblCashSales" +
                                $" ORDER BY CSalesDate", SQLiteCon);

                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;

                                dt2 = new DataTable();
                                SQLiteAdapter = new SQLiteDataAdapter($"SELECT CSalesNo, Num, CNNum, ConsignorCode, ConsigneeCode, CDate, CUserID, Status, IsExported, ExportedDate FROM tblCSaleCNNum", SQLiteCon);
                                SQLiteAdapter.Fill(dt2);

                                string CloseDate = "NULL";
                                foreach (DataRow row in dt.Rows)
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                    TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                    CloseDate = "NULL";
                                    try
                                    {
                                        //using (SQLiteCommand Cmd = new SQLiteCommand($"SELECT LogID, CreatedDate FROM tblDailyLog WHERE CreatedDate >= '{Convert.ToDateTime(row["CDate"]).Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND LogTitle = 'Closed' ORDER BY CreatedDate DESC LIMIT 1", SQLiteCon))
                                        //{
                                        //    SQLiteDataReader dr = Cmd.ExecuteReader();
                                        //    if (dr.HasRows)
                                        //    {
                                        //        if (dr.Read())
                                        //        {
                                        //            CloseDate = "'" + Convert.ToDateTime(dr["CreatedDate"]).ToString(sqlDateLongFormat) + "'";
                                        //        }
                                        //    }
                                        //    dr.Close();
                                        //}

                                        ExecuteQuery($"INSERT INTO tblCashSales (CSalesNo, CSalesDate, CSalesTotal, Price, ShipmentTotal, ShipmentGSTValue, StationaryTotal, IsInsuranced, InsShipmentValue, InsValue, InsGSTValue, InsuranceTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, Status, IsExported, ExportedDate, IsManual, OtherCharges, DiscCode, DiscPercentage, DiscAmount, PaymentMode, PaymentRefNo, PaymentRemarks, TaxTitle, TaxPercentage, RoundingAmt, ClosedSalesTime)" +
                                            $" VALUES('{row["CSalesNo"].ToString()}', '{Convert.ToDateTime(row["CSalesDate"]).ToString(sqlDateLongFormat)}', {Convert.ToDecimal(row["CSalesTotal"])}, {Convert.ToDecimal(row["Price"])}, {Convert.ToDecimal(row["ShipmentTotal"])}, {Convert.ToDecimal(row["ShipmentGSTValue"])}, {Convert.ToDecimal(row["StationaryTotal"])}, '{Convert.ToBoolean(row["IsInsuranced"])}', {Convert.ToDecimal(row["InsShipmentValue"])}, {Convert.ToDecimal(row["InsValue"])}, {Convert.ToDecimal(row["InsGSTValue"])}, {Convert.ToDecimal(row["InsuranceTotal"])}, '{row["StaffCode"].ToString()}', '{row["StaffName"].ToString()}', {Convert.ToDecimal(row["StaffDisAmt"])}," +
                                            $" {Convert.ToInt32(row["CUserID"])}, '{Convert.ToDateTime(row["CDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{Convert.ToBoolean(row["Status"])}', 0, NULL, '{Convert.ToBoolean(row["IsManual"])}', {Convert.ToDecimal(row["OtherCharges"])}, '{row["DiscCode"].ToString()}', {Convert.ToDecimal(row["DiscPercentage"])}, {Convert.ToDecimal(row["DiscAmount"])}, '{row["PaymentMode"].ToString()}', '{row["PaymentRefNo"].ToString()}', '{row["PaymentRemarks"].ToString()}', '{row["TaxTitle"].ToString()}', {Convert.ToDecimal(row["TaxPercentage"])}, {Convert.ToDecimal(row["RoundingAmt"])}, {CloseDate})");

                                        //ExecuteQuery($"INSERT INTO tblCashSales (CSalesNo, CSalesDate, CSalesTotal, DestCode, DestStation, ShipmentType, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, Price, ShipmentTotal, ShipmentGSTValue, StationaryTotal, IsInsuranced, InsShipmentValue, InsShipmentDesc, InsValue, InsGSTValue, InsuranceTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, Status, IsExported, ExportedDate, IsManual, OtherCharges, DiscCode, DiscPercentage, DiscAmount, RefNum, PaymentMode, PaymentRefNo, PaymentRemarks, TaxTitle, TaxPercentage, RoundingAmt, ClosedSalesTime, PostCodeId)" +
                                        //    $" VALUES('{row["CSalesNo"].ToString()}', '{Convert.ToDateTime(row["CSalesDate"]).ToString(sqlDateLongFormat)}', {Convert.ToDecimal(row["CSalesTotal"])}, '{row["DestCode"].ToString()}', '{row["DestStation"].ToString()}', '{row["ShipmentType"].ToString()}', {Convert.ToInt32(row["Pieces"])}, " +
                                        //    $" {Convert.ToDecimal(row["Weight"])}, {Convert.ToDecimal(row["VLength"])}, {Convert.ToDecimal(row["VBreadth"])}, {Convert.ToDecimal(row["VHeight"])}, {Convert.ToDecimal(row["VolWeight"])}, {Convert.ToDecimal(row["Price"])}, {Convert.ToDecimal(row["ShipmentTotal"])}, {Convert.ToDecimal(row["ShipmentGSTValue"])}, {Convert.ToDecimal(row["StationaryTotal"])}, '{Convert.ToBoolean(row["IsInsuranced"])}', {Convert.ToDecimal(row["InsShipmentValue"])}, '{row["InsShipmentDesc"].ToString()}', {Convert.ToDecimal(row["InsValue"])}, {Convert.ToDecimal(row["InsGSTValue"])}, {Convert.ToDecimal(row["InsuranceTotal"])}, '{row["StaffCode"].ToString()}', '{row["StaffName"].ToString()}', {Convert.ToDecimal(row["StaffDisAmt"])}," +
                                        //    $" {Convert.ToInt32(row["CUserID"])}, '{Convert.ToDateTime(row["CDate"]).ToString(sqlDateLongFormat)}', 0, NULL, '{Convert.ToBoolean(row["Status"])}', 0, NULL, '{Convert.ToBoolean(row["IsManual"])}', {Convert.ToDecimal(row["OtherCharges"])}, '{row["DiscCode"].ToString()}', {Convert.ToDecimal(row["DiscPercentage"])}, {Convert.ToDecimal(row["DiscAmount"])}, '{row["RefNum"].ToString()}', '{row["PaymentMode"].ToString()}', '{row["PaymentRefNo"].ToString()}', '{row["PaymentRemarks"].ToString()}', '{row["TaxTitle"].ToString()}', {Convert.ToDecimal(row["TaxPercentage"])}, {Convert.ToDecimal(row["RoundingAmt"])}, {CloseDate}, {Convert.ToInt32(row["PostCodeId"])})");

                                        //dt2 = new DataTable();
                                        //SQLiteAdapter = new SQLiteDataAdapter($"SELECT CSalesNo, Num, CNNum, ConsignorCode, ConsigneeCode, CDate, CUserID, Status FROM tblCSaleCNNum WHERE CSalesNo = '{row["CSalesNo"].ToString()}'", SQLiteCon);
                                        //SQLiteAdapter.Fill(dt2);

                                        view = new DataView(dt2, $"CSalesNo = '{row["CSalesNo"].ToString()}'", "", DataViewRowState.CurrentRows);
                                        dt1 = new DataTable(); dt1 = view.ToTable();

                                        if (dt1.Rows.Count > 0)
                                        {
                                            foreach (DataRow item in dt1.Rows)
                                            {
                                                ExecuteQuery($"INSERT INTO tblCSaleCNNum (CSalesNo, Num, CNNum, MobRefNum, OriginCode, ConsignorCode, PostCodeId, DestCode, ConsigneeCode, ShipmentType, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, HighestWeight, Price, OtherCharges, TotalAmt, InsShipmentValue, InsShipmentDesc, InsRate, InsValue, StaffDiscRate, StaffDiscAmt, PromoCode, PromoRate, PromoDiscAmt, TaxCode, TaxRate, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                                    $" VALUES('{item["CSalesNo"].ToString()}', '{item["Num"].ToString()}', '{item["CNNum"].ToString()}', '{row["RefNum"].ToString()}', '{MyCompanyCode}', '{item["ConsignorCode"].ToString().Replace("'", "")}', {Convert.ToInt32(row["PostCodeId"])}, '{row["DestCode"].ToString()}', '{item["ConsigneeCode"].ToString().Replace("'", "")}', '{row["ShipmentType"].ToString()}', {Convert.ToInt32(row["Pieces"])}, {Convert.ToDecimal(row["Weight"])}, {Convert.ToDecimal(row["VLength"])}, {Convert.ToDecimal(row["VBreadth"])}, {Convert.ToDecimal(row["VHeight"])}, {Convert.ToDecimal(row["VolWeight"])}, {(Convert.ToDecimal(row["Weight"]) > Convert.ToDecimal(row["VolWeight"]) ? Convert.ToDecimal(row["Weight"]) : Convert.ToDecimal(row["VolWeight"]))}, {Convert.ToDecimal(row["Price"])}, {Convert.ToDecimal(row["OtherCharges"])}, {(Convert.ToDecimal(row["Price"]) + Convert.ToDecimal(row["OtherCharges"]))}, {Convert.ToDecimal(row["InsShipmentValue"])}, '{row["InsShipmentDesc"].ToString()}', 0, {Convert.ToDecimal(row["InsValue"])}, 0, {Convert.ToDecimal(row["StaffDisAmt"])}, '{row["DiscCode"].ToString()}', {Convert.ToDecimal(row["DiscPercentage"])}, {Convert.ToDecimal(row["DiscAmount"])}, '{row["TaxTitle"].ToString()}', {Convert.ToDecimal(row["TaxPercentage"])}, {Convert.ToInt32(row["CUserID"])}, '{Convert.ToDateTime(row["CDate"]).ToString(sqlDateLongFormat)}', '{Convert.ToBoolean(row["Status"])}', '{Convert.ToBoolean(item["IsExported"])}', {(item["ExportedDate"] != DBNull.Value ? "'" + Convert.ToDateTime(item["ExportedDate"]).ToString(sqlDateLongFormat) + "'" : "NULL")})");
                                            }
                                        }
                                    }
                                    catch (Exception ex) { error_log.errorlog("DbConversion:" + ex.ToString()); }
                                }
                                ExecuteQuery($"UPDATE tblCashSales SET ClosedSalesTime = B.CSalesDate FROM tblCashSales A" +
                                    $" LEFT JOIN (SELECT MAX(CSalesDate) AS CSalesDate FROM tblCashSales GROUP by FORMAT(CSalesDate, 'dd/MMM/yyyy')) B ON FORMAT(A.CSalesDate, 'dd/MMM/yyyy') = FORMAT(B.CSalesDate, 'dd/MMM/yyyy')");

                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "CNPrint":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblCSPrintDate", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;
                                ExecuteQuery($"TRUNCATE TABLE tblCSPrintDate");
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                        TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                        ExecuteQuery($"INSERT INTO tblCSPrintDate (AWBNum, CreatedBy, CreatedDate, Status, IsExported, ExportedDate)" +
                                            $" VALUES('{row["AWBNum"].ToString()}', {Convert.ToInt32(row["CreatedBy"])}, '{Convert.ToDateTime(row["CreatedDate"]).ToString(sqlDateLongFormat)}', '{Convert.ToBoolean(row["Status"])}', 0, NULL)");
                                    }
                                    catch { }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "StationerySales":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblCSStationary", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;
                                ExecuteQuery($"TRUNCATE TABLE tblCSStationary");
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                        TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                        ExecuteQuery($"INSERT INTO tblCSStationary(CSalesNo, SlNo, ItemCode, ItemName, Quantity, UOM, Rate, Amount, Status)" +
                                            $" VALUES('{row["CSalesNo"].ToString()}', {Convert.ToInt32(row["SlNo"])}, '{row["ItemCode"].ToString()}', '{row["ItemName"].ToString()}', {Convert.ToInt32(row["Quantity"])}, '{row["UOM"].ToString()}', {Convert.ToDecimal(row["Rate"])}, {Convert.ToDecimal(row["Amount"])}, '{Convert.ToBoolean(row["Status"])}')");
                                    }
                                    catch { }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "MobileRef":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblRefMobileApp", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;
                                ExecuteQuery($"TRUNCATE TABLE tblRefMobileApp");
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                        TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                        ExecuteQuery($"INSERT INTO tblRefMobileApp (RefNum, ShipperName, ShipperAdd1, ShipperAdd2, ShipperAdd3, ShipperAdd4, ShipperTelNo, SendTo, CompName, Address1, Address2, City, State, ZipCode, Phone, Createdate, ExpiredDate, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                            $" VALUES('{row["RefNum"].ToString()}', '{row["ShipperName"].ToString().Replace("'", "''")}', '{row["ShipperAdd1"].ToString().Replace("'", "''")}', '{row["ShipperAdd2"].ToString().Replace("'", "''")}', '{row["ShipperAdd3"].ToString().Replace("'", "''")}', '{row["ShipperAdd4"].ToString().Replace("'", "''")}', '{row["ShipperTelNo"].ToString()}', '{row["SendTo"].ToString().Replace("'", "''")}', '{row["CompName"].ToString().Replace("'", "''")}', '{row["Address1"].ToString().Replace("'", "''")}', '{row["Address2"].ToString().Replace("'", "''")}', '{row["City"].ToString().Replace("'", "''")}', '{row["State"].ToString().Replace("'", "''")}', '{row["ZipCode"].ToString()}', '{row["Phone"].ToString()}', '{Convert.ToDateTime(row["Createdate"]).ToString(sqlDateLongFormat)}', '{Convert.ToDateTime(row["ExpiredDate"]).ToString(sqlDateLongFormat)}', {Convert.ToInt32(row["CUserID"])}, '{Convert.ToDateTime(row["CDate"]).ToString(sqlDateLongFormat)}', '{Convert.ToBoolean(row["Status"])}', '{Convert.ToBoolean(row["IsExported"])}', NULL)");
                                    }
                                    catch { }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                    case "UpdatedMobileRef":
                        {
                            dt = new DataTable();
                            SQLiteAdapter = new SQLiteDataAdapter("SELECT * FROM tblRefMobileAppUpdate", SQLiteCon);
                            SQLiteAdapter.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                TotalProgress = 0; CurrentProgress = 0; TotalProgress = dt.Rows.Count;
                                ExecuteQuery($"TRUNCATE TABLE tblRefMobileAppUpdate");
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() => { CurrentProgress++; }));
                                        TableList[SelectedIndex].TableName = tblName + " - " + CurrentProgress + "/" + dt.Rows.Count;

                                        ExecuteQuery($"INSERT INTO tblRefMobileAppUpdate (RefNum, AWBNum, OriginStn, DestStn, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                            $" VALUES('{row["RefNum"].ToString()}', '{row["AWBNum"].ToString()}', '{row["OriginStn"].ToString()}', '{row["DestStn"].ToString()}', {Convert.ToInt32(row["CUserID"])}, '{Convert.ToDateTime(row["CDate"]).ToString(sqlDateLongFormat)}', '{Convert.ToBoolean(row["Status"])}', '{Convert.ToBoolean(row["IsExported"])}', NULL)");
                                    }
                                    catch { }
                                }
                                TableList[SelectedIndex].Status = true;
                            }
                        }
                        break;
                }

            }
        }

        private void ResetData()
        {
            SQLiteDb = ""; TableList.Clear();

            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "StationProfile", TableDesc = "StationProfile", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "AWBAllocation", TableDesc = "AWBAllocation", Status = false });            
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "SystemSetting", TableDesc = "SystemSetting", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "UserMaster", TableDesc = "UserMaster", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "Destination", TableDesc = "Destination", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "Zone", TableDesc = "Zone", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "MotorBikeZone", TableDesc = "MotorBikeZone", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "Quotation", TableDesc = "Quotation", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "PromoCode", TableDesc = "PromoCode", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "StaffDiscount", TableDesc = "StaffDiscount", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "StationeryItems", TableDesc = "StationeryItems", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "LodgeIn", TableDesc = "LodgeIn", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "CashSales", TableDesc = "CashSales", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "CNPrint", TableDesc = "CNPrint", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "StationerySales", TableDesc = "StationerySales", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "MobileRef", TableDesc = "MobileRef", Status = false });
            TableList.Add(new ItemInfo { Item = TableList.Count + 1, TableName = "UpdatedMobileRef", TableDesc = "UpdatedMobileRef", Status = false });

            MyWind.txtSQLiteDb.Focus();
        }

        public class ItemInfo : AMGenFunction
        {
            public int Item { get { return GetValue(() => Item); } set { SetValue(() => Item, value); OnPropertyChanged("Item"); } }
            public string TableName { get { return GetValue(() => TableName); } set { SetValue(() => TableName, value); OnPropertyChanged("TableName"); } }
            public string TableDesc { get { return GetValue(() => TableDesc); } set { SetValue(() => TableDesc, value); OnPropertyChanged("TableDesc"); } }
            public bool Status { get { return GetValue(() => Status); } set { SetValue(() => Status, value); OnPropertyChanged("Status"); } }
        }
    }
}
