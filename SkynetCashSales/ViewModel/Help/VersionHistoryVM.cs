﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    class VersionHistoryVM : AMGenFunction
    {
        public FrmVersionHistory MyWind;
        private ObservableCollection<VersionDetail> _VersionCollection;
        public ObservableCollection<VersionDetail> VersionCollection { get { return _VersionCollection; } set { _VersionCollection = value; OnPropertyChanged("VersionCollection"); } }
        private VersionDetail _VersionCollectionSelected;
        public VersionDetail VersionCollectionSelected { get { return _VersionCollectionSelected; } set { _VersionCollectionSelected = value; OnPropertyChanged("VersionCollectionSelected"); } }

        private string _RippleMessage, _UpdateMessage, _SearchString,_VersionNo, _FrontVersionNo;
        public string RippleMessage { get { return _RippleMessage; } set { _RippleMessage = value; OnPropertyChanged("RippleMessage"); } }
        public string SearchString { get { return _SearchString; } set { _SearchString = value; OnPropertyChanged("SearchString"); } }
        private Visibility _UpdateVisibility;
        public Visibility UpdateVisibility { get { return _UpdateVisibility; } set { _UpdateVisibility = value; OnPropertyChanged("UpdateVisibility"); } }

        public string UpdateMessage { get { return _UpdateMessage; } set { _UpdateMessage = value; OnPropertyChanged("UpdateMessage"); } }

        private ICommand _Buttons;
        public ICommand Buttons { get { _Buttons = new RelayCommand(Parameter => ExecuteButtons(Parameter)); return _Buttons; } }

        public string VersionNo { get { return _VersionNo; } set { _VersionNo = value; OnPropertyChanged("VersionNo"); } }
        public string FrontVersionNo { get { return _FrontVersionNo; } set { _FrontVersionNo = value; OnPropertyChanged("FrontVersionNo"); } }

        DataTable dt = new DataTable();

        public VersionHistoryVM(FrmVersionHistory wind)
        {
            MyWind = wind;
            VersionNo = cssupgrade.VersionNo;
            FrontVersionNo = CurrentVersion;
            LoadIniData();
        }

        private void LoadIniData()
        {
            VersionCollection = GetVersionCollection();
          
            if (IsNewVersion(cssupgrade))
            {
                RippleMessage = RippleMessage = "New " + cssupgrade.VersionNo.ToString() + " Available";
                UpdateVisibility = Visibility.Visible;
                UpdateMessage = "Old Version";
            }
            else { UpdateVisibility = Visibility.Hidden; UpdateMessage = "Latest Version"; }
            //}
        }

        private void ExecuteButtons(object obj)
        {

            switch (obj.ToString())
            {
                //Alert button parameter - Imtiyaz
                case "update":
                    {
                        FrmUpgradePopUp UPfrm = new FrmUpgradePopUp();
                        UPfrm.DataContext = new Masters.UpgradePopUpVM(UPfrm);
                        UPfrm.ShowDialog();
                    }
                    break;
                case "view":
                    {
                        FrmTextBoxView ftv = new FrmTextBoxView();
                        string _windTitle = "Release Notes For : " + VersionCollectionSelected.VersionNo + " " + VersionCollectionSelected.ReleaseDate;
                        ftv.DataContext = new TextBoxViewVM(VersionCollectionSelected.Features, ftv, _windTitle);
                        ftv.ShowDialog();
                    }
                    break;
                case "Search":
                    {
                        if (String.IsNullOrEmpty(SearchString))
                        {
                            MessageBox.Show("Enter the string to search");
                        }
                        else { SearchWithString(); }
                    }
                    break;

                default: throw new Exception("Unexpected MenuButton");
            }
        }

        private void SearchWithString()
        {
            if (dt.Rows.Count > 0)
            {
                DataTable tempdata = dt;
                EnumerableRowCollection<DataRow> query = from myRow in tempdata.AsEnumerable()
                                                         where myRow.Field<string>("Features").ToLower().Contains(SearchString.ToLower())
                                                         select myRow;
                VersionCollection = new ObservableCollection<VersionDetail>();
                if (query.AsDataView().Count > 0) { tempdata = query.CopyToDataTable(); }
                else { tempdata = dt; MessageBox.Show("No matching data found"); }

                if (tempdata.Rows.Count > 0)
                {

                    foreach (DataRow row in tempdata.Rows)
                    {
                        DateTime relDate = (DateTime)row["ReleaseDate"];
                        string vNo = (string)row["VersionNo"];
                        string ftr = (string)row["Features"];

                        VersionCollection.Add(new VersionDetail()
                        {
                            ReleaseDate = relDate.Date.ToString("yy-MMM-dd"),
                            VersionNo = vNo,
                            Features = ftr
                        });

                    }

                }
            }
            else
            {

                MessageBox.Show("No data to search");
            }
        }

        private ObservableCollection<VersionDetail> GetVersionCollection()
        {
            var VersionDetails = new ObservableCollection<VersionDetail>();


            string Query = "select ReleaseDate,VersionNo,Features from tblCSSUpgrade";
            dt = GetDataTable(Query);

            foreach (DataRow row in dt.Rows)
            {
                DateTime relDate = Convert.ToDateTime(row["ReleaseDate"]);
                string vNo = row["VersionNo"].ToString();
                string ftr = row["Features"].ToString();

                VersionDetails.Add(new VersionDetail()
                {
                    ReleaseDate = relDate.Date.ToString("dd-MMM-yyyy"),
                    VersionNo = vNo,
                    Features = ftr
                });

            }
            return VersionDetails;
        }
    }

    internal class VersionDetail
    {
        public string VersionNo { get; set; }
        public string ReleaseDate { get; set; }
        public string Features { get; set; }
    }
}
