﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class ListHelpSingleColVM : AMGenFunction
    {
        public event Action<ListInfo> Closed;
        public static DataTable MyDT;
        public FrmListHelpSingleCol MyWind;

        public ListHelpSingleColVM(DataTable DT, FrmListHelpSingleCol Wind, string WinHedr)
        {
            MyDT = DT; MyWind = Wind; WindHdr = WinHedr;
            try
            {
                foreach (DataRow row in MyDT.Rows)
                {
                    SingleCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString() });
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public string WindHdr { get { return GetValue(() => WindHdr); } set { SetValue(() => WindHdr, value); OnPropertyChanged("WindHdr"); } }
        public string ColHeader1 { get { return GetValue(() => ColHeader1); } set { SetValue(() => ColHeader1, value); OnPropertyChanged("ColHeader1"); } }
        public string SearchText { get { return GetValue(() => SearchText); } set { SetValue(() => SearchText, value); OnPropertyChanged("SearchText"); if (SearchText != null) SelectItem(SearchText); } }

        private ObservableCollection<ListInfo> _SingleCol = new ObservableCollection<ListInfo>();
        public ObservableCollection<ListInfo> SingleCol { get { return _SingleCol; } set { _SingleCol = value; } }

        private ListInfo _SelectedItem = new ListInfo();
        public ListInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "Focus DataGrid":
                        {
                            if (SingleCol.Count > 0)
                            {
                                if (SelectedIndex == SingleCol.Count - 1)
                                    FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                                else
                                    FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                            }
                        }
                        break;
                    case "Ok":
                        {
                            CloseWind();
                            if (Closed != null & SelectedItem != null)
                            {
                                var SelectedItems = new ListInfo() { Id = SelectedItem.Id, ColOneText = SelectedItem.ColOneText };
                                Closed(SelectedItems);
                            }
                        }
                        break;
                    case "Move Up":
                        {
                            if (SelectedIndex == 0)
                                MyWind.txtSearchText.Focus();
                            else
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex - 1, 0);
                        }
                        break;
                    case "Move Down":
                        {
                            if (SelectedIndex == SingleCol.Count - 1)
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                            else
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                        }
                        break;
                    default: throw new Exception("Unexpected Command Parameter");
                }
            }
            catch (Exception Ex) { System.Windows.MessageBox.Show(Ex.Message); }
        }

        private void SelectItem(string SearchTxt)
        {
            SingleCol.Clear();

            if (!string.IsNullOrEmpty(SearchText))
            {
                foreach (DataRow row in MyDT.Rows)
                {
                    if (SearchTxt.Length <= (row["ColOneText"].ToString().Length))
                    {
                        if ((row["ColOneText"].ToString()).Substring(0, SearchTxt.Length).ToUpper() == SearchText)
                        {
                            SingleCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString() });
                        }
                    }
                }
            }
            else if (string.IsNullOrEmpty(SearchText))
            {
                foreach (DataRow row in MyDT.Rows)
                {
                    SingleCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString() });
                }
            }
        }

        public class ListInfo : AMGenFunction
        {
            public string Id { get; set; }
            public string ColOneText { get; set; }
        }

    }
}