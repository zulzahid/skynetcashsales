﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Help
{
    public class ListHelpTwoColVM : AMGenFunction
    {
        public event Action<ListInfo> Closed;
        public FrmListHelpTwoCol MyWind;
        private DataTable dtMain;

        public ListHelpTwoColVM(DataTable dt, FrmListHelpTwoCol Wind, string WinHedr, string Col1Txt, string Col2Txt)
        {
            SearchTextOne = string.Empty; SearchTextTwo = string.Empty;

            dtMain = GetDataTable($"SELECT ColOneId = '', ColOneText = '', ColTwoText = ''");
            dtMain.Rows.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dtMain.NewRow();
                dr["ColOneId"] = dt.Rows[i][0].ToString();
                dr["ColOneText"] = dt.Rows[i][1].ToString();
                dr["ColTwoText"] = dt.Rows[i][2].ToString();
                dtMain.Rows.Add(dr);
            }

            MyWind = Wind; WindHdr = WinHedr;

            ColHeader1 = Col1Txt;
            ColHeader2 = Col2Txt;

            foreach (DataRow row in dtMain.Rows)
            {
                TwoCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString(), ColTwoText = row["ColTwoText"].ToString() });
            }
        }

        public string WindHdr { get { return GetValue(() => WindHdr); } set { SetValue(() => WindHdr, value); OnPropertyChanged("WindHdr"); } }
        public string ColHeader1 { get { return GetValue(() => ColHeader1); } set { SetValue(() => ColHeader1, value); OnPropertyChanged("ColHeader1"); } }
        public string ColHeader2 { get { return GetValue(() => ColHeader2); } set { SetValue(() => ColHeader2, value); OnPropertyChanged("ColHeader2"); } }
        public string SearchTextOne { get { return GetValue(() => SearchTextOne); } set { SetValue(() => SearchTextOne, value); OnPropertyChanged("SearchTextOne"); if (SearchTextOne != null) FilterData(); } }
        public string SearchTextTwo { get { return GetValue(() => SearchTextTwo); } set { SetValue(() => SearchTextTwo, value); OnPropertyChanged("SearchTextTwo"); if (SearchTextTwo != null) FilterData(); } }

        private ObservableCollection<ListInfo> _TwoCol = new ObservableCollection<ListInfo>();
        public ObservableCollection<ListInfo> TwoCol { get { return _TwoCol; } set { _TwoCol = value; } }

        private ListInfo _SelectedItem = new ListInfo();
        public ListInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            switch (Obj.ToString())
            {
                case "Focus DataGrid":
                    {
                        if (TwoCol.Count > 0)
                        {
                            if (SelectedIndex == TwoCol.Count - 1)
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                            else
                                FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                        }
                    }
                    break;
                case "Ok":
                    {
                        CloseWind();
                        if (Closed != null & SelectedItem != null)
                        {
                            var SelectedItems = new ListInfo() { Id = SelectedItem.Id, ColOneText = SelectedItem.ColOneText, ColTwoText = SelectedItem.ColTwoText };
                            Closed(SelectedItems);
                        }
                    }
                    break;
                case "Move Up":
                    {
                        if (SelectedIndex == 0)
                            MyWind.txtSearchTextOne.Focus();
                        else
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex - 1, 0);
                    }
                    break;
                case "Move Down":
                    {
                        if (SelectedIndex == TwoCol.Count - 1)
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex, 0);
                        else
                            FocusMyGrid(MyWind.DGrdGen, SelectedIndex + 1, 0);
                    }
                    break;
                default: throw new Exception("Unexpected Command Parameter");
            }
        }
        private void FilterData()
        {
            TwoCol.Clear();
            if (dtMain != null && dtMain.Rows.Count > 0)
            {
                using (DataView view = new DataView(dtMain, "ColOneText LIKE '" + SearchTextOne + "%' AND ColTwoText LIKE '" + SearchTextTwo + "%'", "", DataViewRowState.CurrentRows))
                {
                    using (DataTable dtFilterData = view.ToTable())
                    {
                        foreach (DataRow row in dtFilterData.Rows)
                        {
                            TwoCol.Add(new ListInfo { Id = Convert.ToString(row["ColOneId"]), ColOneText = row["ColOneText"].ToString(), ColTwoText = row["ColTwoText"].ToString() });
                        }
                    }
                }
            }
        }

        public class ListInfo
        {
            public string Id { get; set; }
            public string ColOneText { get; set; }
            public string ColTwoText { get; set; }
        }
    }
}
