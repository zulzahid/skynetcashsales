﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class UpdateTransactionVM : AMGenFunction
    {
        FrmUpdateTransaction Wind;

        public UpdateTransactionVM(FrmUpdateTransaction Frm)
        {
            Wind = Frm;
            transactionload();
            PaymentModeComboboxload();
            reset();
        }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        public string CashSalesNo
        {
            get { return GetValue(() => CashSalesNo); }
            set { SetValue(() => CashSalesNo, value); OnPropertyChanged("CashSalesNo"); }
        }
        public string ShipmentTypelblValue
        {
            get { return GetValue(() => ShipmentTypelblValue); }
            set { SetValue(() => ShipmentTypelblValue, value); OnPropertyChanged("ShipmentTypelblValue"); }
        }

        public string DestlblValue
        {
            get { return GetValue(() => DestlblValue); }
            set { SetValue(() => DestlblValue, value); OnPropertyChanged("DestlblValue"); }
        }

        public string WeightlblValue
        {
            get { return GetValue(() => WeightlblValue); }
            set { SetValue(() => WeightlblValue, value); OnPropertyChanged("WeightlblValue"); }
        }
        public string VWlblValue
        {
            get { return GetValue(() => VWlblValue); }
            set { SetValue(() => VWlblValue, value); OnPropertyChanged("VWlblValue"); }
        }

        public string ShipmentAmtlblValue
        {
            get { return GetValue(() => ShipmentAmtlblValue); }
            set { SetValue(() => ShipmentAmtlblValue, value); OnPropertyChanged("ShipmentAmtlblValue"); }
        }

        public string PricelblValue
        {
            get { return GetValue(() => PricelblValue); }
            set { SetValue(() => PricelblValue, value); OnPropertyChanged("PricelblValue"); }
        }

        public string ServiceAmtlblValue
        {
            get { return GetValue(() => ServiceAmtlblValue); }
            set { SetValue(() => ServiceAmtlblValue, value); OnPropertyChanged("ServiceAmtlblValue"); }
        }

        public string OtherChargeslblValue
        {
            get { return GetValue(() => OtherChargeslblValue); }
            set { SetValue(() => OtherChargeslblValue, value); OnPropertyChanged("OtherChargeslblValue"); }
        }

        public string StationarylblValue
        {
            get { return GetValue(() => StationarylblValue); }
            set { SetValue(() => StationarylblValue, value); OnPropertyChanged("StationarylblValue"); }
        }

        public string InsurancelblValue
        {
            get { return GetValue(() => InsurancelblValue); }
            set { SetValue(() => InsurancelblValue, value); OnPropertyChanged("InsurancelblValue"); }
        }
        public string ValuelblValue
        {
            get { return GetValue(() => ValuelblValue); }
            set { SetValue(() => ValuelblValue, value); OnPropertyChanged("ValuelblValue"); }
        }
        public string ServiceTaxlblValue
        {
            get { return GetValue(() => ServiceTaxlblValue); }
            set { SetValue(() => ServiceTaxlblValue, value); OnPropertyChanged("WeightlblValue"); }
        }

        private int _selectedPaymentModeIndex;
        public int SelectedPaymentModeIndex { get { return _selectedPaymentModeIndex; } set { _selectedPaymentModeIndex = value; OnPropertyChanged("SelectedPaymentModeIndex"); } }


        private Transactiondetails _SelectedTransaction = new Transactiondetails();
        public Transactiondetails SelectedTransaction { get { return _SelectedTransaction; } set { _SelectedTransaction = value; OnPropertyChanged("SelectedTransaction"); } }
        
        private PaymentModeModel _selectedPaymentMode = new PaymentModeModel();
        public PaymentModeModel SelectedValuePaymentMode { get { return _selectedPaymentMode; } set { _selectedPaymentMode = value; OnPropertyChanged("SelectedValuePaymentMode"); } }
        public string PaymentReference { get { return GetValue(() => PaymentReference); } set { SetValue(() => PaymentReference, value); OnPropertyChanged("PaymentReference"); } }

        public static ObservableCollection<Transactiondetails> _transactiondetails = new ObservableCollection<Transactiondetails>();
        public ObservableCollection<Transactiondetails> transactiondetails
        {
            get { return _transactiondetails; }
            set { _transactiondetails = value; OnPropertyChanged("transactiondetails"); }
        }

        private ObservableCollection<PaymentModeModel> _PaymentMode = new ObservableCollection<PaymentModeModel>();
        public ObservableCollection<PaymentModeModel> PaymentMode { get { return _PaymentMode; } set { _PaymentMode = value; OnPropertyChanged("PaymentMode"); } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {

                    case "Update": { if (!string.IsNullOrEmpty(SelectedTransaction.cashsalesno)) { UpdatePaymentMethod(SelectedTransaction); } else { System.Windows.MessageBox.Show("Please select Transaction"); } } break;
                    //case "Clear": ResetData(); break;
                    case "LoadItem":
                        {
                            transactionLoadItem(SelectedTransaction);
                        }
                        break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        public void transactionLoadItem(Transactiondetails selectedItem)
        {
            try
            {
                SqlDataReader dr = GetDataReader($"select top 1 CSalesNo,ShipmentType, DestCode, Weight, VolWeight,ShipmentTotal,Price,OtherCharges,StationaryTotal,InsuranceTotal,InsValue,InsGSTValue, ShipmentGSTValue,PaymentMode,PaymentRefNo from tblCashSales where CSalesNo = '{SelectedTransaction.cashsalesno}'");
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        CashSalesNo = dr["CSalesNo"].ToString();
                        ShipmentTypelblValue = dr["ShipmentType"].ToString();
                        DestlblValue = dr["DestCode"].ToString();
                        WeightlblValue = dr["Weight"].ToString();
                        VWlblValue = dr["VolWeight"].ToString();
                        ShipmentAmtlblValue = dr["ShipmentTotal"].ToString();
                        PricelblValue = dr["Price"].ToString();
                        ServiceAmtlblValue = dr["ShipmentGSTValue"].ToString();
                        OtherChargeslblValue = dr["OtherCharges"].ToString();
                        StationarylblValue = dr["StationaryTotal"].ToString();
                        InsurancelblValue = dr["InsuranceTotal"].ToString();
                        ValuelblValue = dr["InsValue"].ToString();
                        ServiceTaxlblValue = dr["InsGSTValue"].ToString();
                        SelectedValuePaymentMode.PaymentModeName = dr["PaymentMode"].ToString();
                        //Wind.ddlPaymentMode.SelectedValue = dr["PaymentMode"].ToString();
                        PaymentReference = dr["PaymentRefNo"].ToString();             
                    }
                    dr.Close();
                }
            
            }
            catch (Exception ex)
            {
                error_log.errorlog("transactionloaditem :" + ex.ToString());
            }

        }

        public int UpdatePaymentMethod(Transactiondetails selectedItem)
        {
            int i = 0;

            try
            {
                var messageresult = System.Windows.MessageBox.Show("Are you sure you want to update the Payment Method","Confirmation",MessageBoxButton.YesNo);

                if (messageresult == MessageBoxResult.Yes)
                {
                    i = ExecuteQuery($"UPDATE tblCashSales SET PaymentMode = '{Wind.ddlPaymentMode.SelectedValue}',PaymentRefNo = '{PaymentReference}' WHERE CSalesNo = '{selectedItem.cashsalesno}'");

                    if (i > 0)
                    {
                        System.Windows.MessageBox.Show("Payment Mode Successfully Updated");
                        transactionLoadItem(selectedItem);
                        reset();
                        
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Payment Mode update failed");
                    }
                }
               
                return i;
            }
            catch (Exception ex)
            {
                error_log.errorlog("UpdatePaymentMethod:" + ex.ToString());
                return 0;
            }
            
        }

        public void PaymentModeComboboxload()
        {
            try
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GDDesc FROM tblGeneralDet A" +
                        $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                        $" WHERE RTRIM(B.GenDesc) = 'PAYMENT MODE' AND A.Status != '0' ORDER BY A.GDDesc");

                    if (dt.Rows.Count > 0)
                    {
                        PaymentMode.Clear();
                        for (int i = 0; i < dt.Rows.Count; ++i)
                        {
                            string abc = dt.Rows[i]["GDDesc"].ToString();
                            PaymentMode.Add(new PaymentModeModel { PaymentModeId = abc, PaymentModeName = abc });
                        }
                        SelectedPaymentModeIndex = dt.Rows.Count - 1;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("PaymentModee Combobox load function:" + Ex.ToString()); }
        }

        public void transactionload()
        {
            try
            {
                SqlDataReader dr = GetDataReader($"SELECT CSalesNo, Cast(CSalesDate as nvarchar(20))  as CSalesDate FROM tblCashSales ORDER BY CSalesDate desc");
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        transactiondetails.Add(new Transactiondetails() { cashsalesno = dr["CSalesNo"].ToString(),date = dr["CSalesDate"].ToString()});
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("transactionload:" + ex.ToString());
            }
        }

        public class Transactiondetails : AMGenFunction
        {
            public string cashsalesno { get; set; }
            public string date { get; set; }
            public string awbnumber { get; set; }
        }

        public void reset()
        {

            InitializedDashboard();
        }


    }
}
