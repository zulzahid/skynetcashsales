﻿using BarcodeLib;
using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Reports;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class CNPrintVM : AMGenFunction
    {
        private static DataTable DTCNList = new DataTable();
        public CNPrintVM() { }
        public void PrintORPreview(string csno, int csid, string sendercode, string receivercode,DataTable dtcnlist,string option)
        {
            CSaleNo = csno;
            CSalesID = csid;
            ConsignorCode = sendercode;
            ConsigneeCode = receivercode;
            if (dtcnlist.Rows.Count > 0)
            {
                SqlDataReader dr = GetDataReader($"SELECT ISNULL(CNPrinter, '') AS CNPrinter FROM tblWPConfig WHERE CNPrinter != '' AND Status = 1");
                if (dr.Read())
                {
                    CNPrinter = dr["CNPrinter"].ToString();
                }
                dr.Close();

                DTCNList = dtcnlist;
                Print(option, DTCNList);
            }
        }

        public void PrintORPreview(string csno, int csid, DataTable dtcnlist, string option)
        {
            try
            {
                CSaleNo = csno;
                CSalesID = csid;

                SqlDataReader dr = GetDataReader($"SELECT CSalesID FROM tblCashSales WHERE CSalesNo = '{CSaleNo}'");
                if (dr.Read())
                {
                    CSalesID = Convert.ToInt32(dr["CSalesID"]);
                }
                dr.Close();

                if (dtcnlist.Rows.Count > 0)
                {
                    dr = GetDataReader($"SELECT ISNULL(CNPrinter, '') AS CNPrinter FROM tblWPConfig WHERE PCNAME = HOST_NAME() AND CNPrinter != '' AND Status = 1");
                    if (dr.Read())
                    {
                        CNPrinter = dr["CNPrinter"].ToString();
                    }
                    dr.Close();

                    DTCNList = dtcnlist;
                    Print(option, DTCNList);
                }
            }
            catch (Exception Ex) { error_log.errorlog("CN PrintORPreview Function : " + Ex.ToString()); }
        }

        public void PrintORPreview(string option)
        {
            SqlDataReader dr = GetDataReader($"SELECT ISNULL(CNPrinter, '') AS CNPrinter FROM tblWPConfig WHERE PCNAME = HOST_NAME() AND CNPrinter != '' AND Status = 1");
            if (dr.Read())
            {
                CNPrinter = dr["CNPrinter"].ToString();
            }
            dr.Close();

            TestPrint(option);
        }

        public string AWBNumber { get { return GetValue(() => AWBNumber); } set { SetValue(() => AWBNumber, value); OnPropertyChanged("AWBNumber"); } }
        public string CSaleNo { get { return GetValue(() => CSaleNo); } set { SetValue(() => CSaleNo, value); OnPropertyChanged("CSaleNo"); } }
        public int CSalesID { get { return GetValue(() => CSalesID); } set { SetValue(() => CSalesID, value); OnPropertyChanged("CSalesID"); } }
        public string ConsignorCode { get { return GetValue(() => ConsignorCode); } set { SetValue(() => ConsignorCode, value); OnPropertyChanged("ConsignorCode"); } }
        public string ConsigneeCode { get { return GetValue(() => ConsigneeCode); } set { SetValue(() => ConsigneeCode, value); OnPropertyChanged("ConsigneeCode"); } }
        public string ConsignmentNote { get { return GetValue(() => ConsignmentNote); } set { SetValue(() => ConsignmentNote, value); OnPropertyChanged("ConsignmentNote"); } }

        private Bitmap GetBarCodeBitMap(String data)
        {
            Bitmap barcode = new Bitmap(1, 1);
            Font ThreeofNine = new Font("Free 3 of 9", 60, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            Graphics graphics = Graphics.FromImage(barcode);
            SizeF dataSize = graphics.MeasureString(data,ThreeofNine);
            barcode = new Bitmap(barcode, dataSize.ToSize());
            graphics = Graphics.FromImage(barcode);
            graphics.Clear(System.Drawing.Color.White);
            graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;
            graphics.DrawString(data, ThreeofNine, new SolidBrush(System.Drawing.Color.Black), 0, 0);
            graphics.Flush();
            ThreeofNine.Dispose();
            graphics.Dispose();
            return barcode;
        }

        private BitmapImage GetBarCodeBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public Image GetBarCodeImage(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(System.Drawing.Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(System.Drawing.Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();

                    Convert.ToBase64String(byteImage);
                    return bitMap;
                }             
            }
        }

        private System.Windows.Controls.Image barImage = new System.Windows.Controls.Image();
        public byte[] FillBarCode(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();                     
                    return byteImage;
                    //barImage.Source = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                }
                //plBarCode.Controls.Add(imgBarCode);
            }
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public String FillBarCodeStr(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();

                    Convert.ToBase64String(byteImage);
                    return Convert.ToBase64String(byteImage);
                }
                //plBarCode.Controls.Add(imgBarCode);
            }
        }

        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        private void PrintData(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 8);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);

                if (!string.IsNullOrEmpty(ConsignorCode) && string.IsNullOrEmpty(ConsigneeCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile FROM tblConsignor a Where a.ConsignorCode = '{ConsignorCode}'");
                    if (dr.Read())
                    {
                        // LINE. 0
                        startX = 350; startY = 5; Offset = 5;
                        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                        int width = 400; int height = 35;
                        string cnn = "*" + ConsignmentNote.Trim() + "*";
                        Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                        graphics.DrawImage(CNImages1, startX, 0, width, height);
                        startX = 550; startY = 42; Offset = 5;
                        graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                        // LINE. 1
                        startX = 1; startY = 30; Offset = 40;
                        _Font1 = new Font("Arial", 10);
                        graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);
                        startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString().Trim(), _Font1, _SolidBrush, startX, startY + Offset);

                        // LINE. 2
                        Offset = Offset + 15; startX = 1; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        startX = 250; graphics.DrawString("", _Font1, _SolidBrush, startX, startY + Offset);

                        // LINE. 3
                        Offset = Offset + 12; startX = 1; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 4
                        Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 5
                        Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 6
                        Offset = Offset + 24; startX = 50; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    }
                    dr.Close();
                }
                else if (string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile FROM tblConsignee a Where a.ConsigneeCode = '{ConsigneeCode}'");
                    if (dr.Read())
                    {
                        // LINE. 0
                        startX = 350; startY = 5; Offset = 5;
                        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                        int width = 400; int height = 35;
                        string cnn = "*" + ConsignmentNote.Trim() + "*";
                        Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                        graphics.DrawImage(CNImages1, startX, 0, width, height);
                        startX = 550; startY = 42; Offset = 5;
                        graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);   

                        // LINE. 1
                        startY = 30; 
                        _Font1 = new Font("Arial", 10);
                        Offset = 70; startX = 420; graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                        // LINE. 2
                        Offset = Offset + 15; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 3
                        Offset = Offset + 12; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 4
                        Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 5
                        Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                        // LINE. 6
                        Offset = Offset + 55; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                        startX = 670; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    }
                    dr.Close();
                }
                else if (!string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode)) 
                {
                    SqlDataReader dr = GetDataReader($"SELECT ConsignorName = b.Name, ConsignorAddress1 = b.Add1, ConsignorAddress2 = b.Add2, ConsignorAddress3 = b.City + '-' + b.PostalCode, ConsignorAddress4 = b.State + ',' + b.Country, ConsignorContact = b.Phone + '/' + b.Mobile, ConsignorDeptCode = '', ConsignorCode = b.ConsignorCode," +
                        $" ConsigneeName = a.Name, ConsigneeAddress1 = a.Add1, ConsigneeAddress2 = a.Add2, ConsigneeAddress3 = a.City + '-' + a.PostalCode, ConsigneeAddress4 = a.State + ',' + a.Country, ConsigneeContact = a.Phone + '/' + a.Mobile, ConsigneeDeptCode = '', ConsigneeCode = a.ConsigneeCode, ConsigneeAttention = '' FROM tblConsignee a" +
                        $" join tblConsignors b on a.ConsignorCode = b.ConsignorCode" +
                        $" where a.ConsigneeCode = '{ConsigneeCode}' and b.ConsignorCode = '{ConsignorCode}'");

                    if (dr.Read())
                    {
                        // LINE. 0
                        startX = 350; startY = 5; Offset = 5;
                        Bitmap bp=GetBarCodeBitMap(ConsignmentNote);
                        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                        Image CNImage = Image.FromFile(@System.Windows.Forms.Application.StartupPath + "\\tempCN.jpg");
                        // Working width for A4 fine is int width = 600; int height = 100;
                        int width = 400; int height = 35;
                        string cnn = "*" + ConsignmentNote.Trim() + "*";
                        Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                        graphics.DrawImage(CNImages1, startX, 0, width, height);
                        startX = 550; startY = 42; Offset = 5;
                        graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);   

                        // LINE. 1
                        startX = 1; startY = 30; Offset = 40;
                        _Font1 = new Font("Arial", 10);
                        graphics.DrawString(dr["ConsignorName"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                                             // SHIPPER NAME
                        startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                              // SHIPPER ACCNO

                        // LINE. 2
                        startX = 1; Offset = Offset + 15; graphics.DrawString(dr["ConsignorAddress1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD1
                        startX = 250; graphics.DrawString(dr["ConsignorDeptCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // SHIPPER DEPTCODE

                        // LINE. 3
                        startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD2
                        startX = 420; graphics.DrawString(dr["ConsigneeName"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // CONSIGNEE NAME

                        // LINE. 4
                        startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);           // SHIPPER ADD3
                        startX = 420; graphics.DrawString(dr["ConsigneeAddress1"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD1

                        // LINE. 5
                        startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);          // SHIPPER ADD4
                        startX = 420; graphics.DrawString(dr["ConsigneeAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD2

                        // LINE. 6
                        Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);         // CONSIGNEE ADD3

                        // LINE. 7
                        Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);        // CONSIGNEE ADD4
                        startX = 50; graphics.DrawString(dr["ConsignorContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // SHIPPER PHONE

                        // LINE. 8
                        Offset = Offset + 55; startX = 470; graphics.DrawString(dr["ConsigneeAttention"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                    // CONSIGNEE ATTENTION
                        startX = 670; graphics.DrawString(dr["ConsigneeContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                              // CONSIGNEE PHONE
                    }
                    dr.Close();
                }
            }
        }

        public void Print(string OutputLocation)
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);

            PaperSize psize = new PaperSize("Custom", 200, 100);
            ps.DefaultPageSettings.PaperSize = psize;

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;

            pdoc.DefaultPageSettings.PaperSize.Height = 320;
            pdoc.DefaultPageSettings.PaperSize.Width = 820;
            pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;
            pdoc.PrintPage += new PrintPageEventHandler(PrintData);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            pp.Document = pdoc;

            if (OutputLocation == "Print")
            {
                System.Windows.Forms.DialogResult result = pd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                    pdoc.Print();
            }
            else
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }
        }

        //private int CentimeterToPixel(double Centimeter)
        //{
        //    double pixel = -1;
        //    using (Graphics g = this.CreateGraphics())
        //    {
        //        pixel = Centimeter * g.DpiY / 2.54d;
        //    }
        //    return (int)pixel;
        //}

        public void Print(string OutputLocation,DataTable dtcn)
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            int numberofrows = dtcn.Rows.Count;
            if (numberofrows > 0)
            {                
                    PaperSize psize = new PaperSize("Custom", 950, 400 * numberofrows);                    
                    ps.DefaultPageSettings.PaperSize = psize;

                    pd.Document = pdoc;
                    pd.Document.DefaultPageSettings.PaperSize = psize;
                    Margins margins = new Margins(50, 0, 0, 0);
                    pd.Document.DefaultPageSettings.Margins = margins;
                    //pdoc.DefaultPageSettings.PaperSize.Height = 400 * numberofrows;//320
                    //pdoc.DefaultPageSettings.PaperSize.Width = 850;
                    pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;

                    if (OutputLocation == "TestPrint")
                    {
                        pdoc.PrintPage += new PrintPageEventHandler(TestPDTBC);
                    }
                    else { pdoc.PrintPage += new PrintPageEventHandler(PrintDataTableSR); }
                    pp.Document = pdoc;
            }
            if (OutputLocation == "Print")
            {
                try
                {
                    if (!string.IsNullOrEmpty(CNPrinter))
                    { 
                        pdoc.PrinterSettings.PrinterName = CNPrinter; pdoc.Print(); 
                    }
                    else
                    {
                        System.Windows.Forms.DialogResult result = pd.ShowDialog();
                        if (result == System.Windows.Forms.DialogResult.OK)
                            pdoc.Print();
                    }
                }
                catch (Exception Ex)
                {
                    System.Windows.Forms.DialogResult result = pd.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                        pdoc.Print();
                }
            }
            else if (OutputLocation == "Preview")
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }
            //else
            //{
            //    System.Windows.Forms.DialogResult result = pp.ShowDialog();
            //}
        }

        private void PrintDataTable(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                SqlDataReader dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile Where RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");
                string CashAccNum = dr.Read() ? dr["CashAccNum"].ToString() : ""; dr.Close();

                CashAccNum = !string.IsNullOrEmpty(CashAccNum) ? CashAccNum : " ";
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                Font _Font8 = new Font("Arial", 8);
                Font _Font9 = new Font("Arial", 9);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;
                for (int i = 0; i < DTCNList.Rows.Count; i++)
                {
                    string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                    ConsignmentNote = DTCNList.Rows[i][0].ToString();
                    ShipmentType = DTCNList.Rows[i][1].ToString();
                    DestCode = DTCNList.Rows[i][2].ToString();
                    Pieces = DTCNList.Rows[i][3].ToString();
                    Weight = DTCNList.Rows[i][4].ToString();
                    VL = DTCNList.Rows[i][5].ToString();
                    VB = DTCNList.Rows[i][6].ToString();
                    VH = DTCNList.Rows[i][7].ToString();
                    VolW = DTCNList.Rows[i][8].ToString();
                    ReceivedBy = DTCNList.Rows[i][9].ToString();
                    Date = DTCNList.Rows[i][10].ToString();
                    IsIns = DTCNList.Rows[i][11].ToString();
                    DestStation = DTCNList.Rows[i][12].ToString();
                    InsShipmntvalue = DTCNList.Rows[i][13].ToString();
                    InsChrgs = DTCNList.Rows[i][14].ToString();
                    CourierChrgs = DTCNList.Rows[i][15].ToString();
                    TotalChrgs = DTCNList.Rows[i][16].ToString();
                    DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                    decimal len, brd, hgt, vol;
                    len = Math.Round(Convert.ToDecimal(VL), 0);
                    brd = Math.Round(Convert.ToDecimal(VB), 0);
                    hgt = Math.Round(Convert.ToDecimal(VH), 0);
                    vol = Math.Round(Convert.ToDecimal(VolW), 0);

                    //graphics.DrawString("*", _Font1, _SolidBrush, 0, 0);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 840, 0);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 0, 390);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 840, 390);
                    
                    // LINE. 0
                    startX = 400; startY = Y + 15;
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                    int width = 280; int height = 30; //int width = 350; int height = 30;
                    string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                    Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                    graphics.DrawImage(CNImages1, startX, startY, width, height);
                    graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 300, startY + 7);
                    startX = 500; startY = Y + 45; Offset = 1;
                    graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                    //LINE DEST Code
                    startX = 720; startY = Y + 45; Offset = 30;
                    Font _Font10 = new Font("Arial", 18);
                    graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);
                    
                    //Cash Account Number
                    startX = 250; graphics.DrawString(CashAccNum, _Font9, _SolidBrush, startX, startY + Offset);

                    if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                    {
                        startX = 1; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }
                    else
                    {
                        startX = 76; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }

                    //LINE Vol Weight
                    if (!string.IsNullOrEmpty(vol.ToString().Trim()))
                    {
                        if (Convert.ToDecimal(vol) > 0)
                        {
                            startX = 200; startY = Y + 45; Offset = 1;
                            graphics.DrawString("Vol. Weight : "+vol.ToString().Trim()+" kg", _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        else
                        {
                            startX = 200; startY = Y + 45; Offset = 1;
                            graphics.DrawString("Vol. Weight : ", _Font2, _SolidBrush, startX, startY + Offset);
                        }
                    } 
                    startY = Y; Offset = 234;
                    //startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 90; graphics.DrawString(Weight.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Pieces
                    startX = 24; startY = Y; Offset = 267;
                    graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Weight
                    //startY = Y; Offset = 271;
                    //startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
                    startY = Y; Offset = 267;
                    startX = 230; graphics.DrawString("RM:" + TotalChrgs, _Font1, _SolidBrush, startX, startY + 264);
                    startX = 320; graphics.DrawString(CSDT.ToShortTimeString(), _Font8, _SolidBrush, startX, startY + Offset);
                    startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font8, _SolidBrush, startX, startY + Offset);

                    if (IsIns == "True" || IsIns == "1")
                    {
                        //LINE Insurance Yes
                        startX = 21; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double inshpamt = Convert.ToDouble(InsShipmntvalue);
                        double inschrgs = Convert.ToDouble(InsChrgs);
                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 124; startY = Y; Offset = 299;
                        graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 308;
                        graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 317;
                        graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 326;
                        graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        //LINE Insurance No
                        startX = 201; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 215; startY = Y; Offset = 315;
                        graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 215; startY = Y; Offset = 324;
                        graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    Y += 400;

                    ExecuteQuery($"INSERT INTO tblCSPrintDate(AWBNum, CreatedBy, CreatedDate, Status) VALUES('{ConsignmentNote}', {LoginUserId}, GETDATE(), 1)");
                }
            }
        }

        Bytescout.BarCode.Barcode bb = new Bytescout.BarCode.Barcode();
        private void PrintDataTableSR(object sender, PrintPageEventArgs e)
        {
            SqlDataReader dr;

            if (CSalesID > 0)
            {
                dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile Where RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");
                string CashAccNum = dr.Read() ? dr["CashAccNum"].ToString() : ""; dr.Close();

                CashAccNum = !string.IsNullOrEmpty(CashAccNum) ? CashAccNum : " ";
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                Font _Font8 = new Font("Arial", 8);
                Font _Font9 = new Font("Arial", 9);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;
                for (int i = 0; i < DTCNList.Rows.Count; i++)
                {
                    string refnum = DTCNList.Rows[i]["MobRefNum"].ToString();                    
                    int PostCodeId = Convert.ToInt32(DTCNList.Rows[i]["PostCodeId"]);

                    ////refnum = (from a in CSalesEntity.tblCashSales where a.CSalesID == CSalesID select a.RefNum).FirstOrDefault();
                    //dr = GetDataReader($"SELECT RefNum, PostCodeId FROM tblCashSales WHERE CSalesID = {CSalesID}");
                    //if (dr.Read())
                    //{
                    //    refnum = dr["RefNum"].ToString();
                    //    PostCodeId = Convert.ToInt32(dr["PostCodeId"]);
                    //}
                    //dr.Close();

                    if (!string.IsNullOrEmpty(refnum))
                    {
                        dr = GetDataReader($"SELECT * FROM tblRefMobileApp a Where a.RefNum = '{refnum}'");
                        if (dr.Read())
                        {
                            string RefNum = dr["RefNum"].ToString();
                            string ShipperName = dr["ShipperName"].ToString();
                            string ShipperAdd1 = dr["ShipperAdd1"].ToString();
                            string ShipperAdd2 = dr["ShipperAdd2"].ToString();
                            string ShipperAdd3 = dr["ShipperAdd3"].ToString();
                            string ShipperAdd4 = dr["ShipperAdd4"].ToString();
                            string ShipperTelNo = dr["ShipperTelNo"].ToString();
                            string SendTo = dr["SendTo"].ToString();
                            string CompName = dr["CompName"].ToString();
                            string Address1 = dr["Address1"].ToString();
                            string Address2 = dr["Address2"].ToString();
                            string State = dr["State"].ToString();
                            string City = dr["City"].ToString();
                            string ZipCode = dr["ZipCode"].ToString();
                            string Phone = dr["Phone"].ToString();

                            startX = 5; startY = 90;
                            graphics.DrawString(ShipperName, _Font9, _SolidBrush, startX, startY);
                            startY = 105;
                            graphics.DrawString(ShipperAdd1+", "+ShipperAdd2, _Font9, _SolidBrush, startX, startY);
                            startY = 120;
                            graphics.DrawString(ShipperAdd3 + ", " + ShipperAdd4, _Font9, _SolidBrush, startX, startY);
                            startX = 250;
                            graphics.DrawString("Ph: " + ShipperTelNo, _Font9, _SolidBrush, startX, startY);

                            startX = 420; startY = 105;
                            graphics.DrawString(SendTo, _Font9, _SolidBrush, startX, startY);
                            startY = 120;
                            graphics.DrawString(CompName, _Font9, _SolidBrush, startX, startY);
                            startY = 135;
                            graphics.DrawString(Address1, _Font9, _SolidBrush, startX, startY);
                            startY = 150;
                            graphics.DrawString(Address2, _Font9, _SolidBrush, startX, startY);
                            startY = 165;
                            graphics.DrawString(City, _Font9, _SolidBrush, startX, startY);
                            startY = 180;
                            graphics.DrawString(State+" - "+ZipCode, _Font9, _SolidBrush, startX, startY);
                            startY = 195;
                            graphics.DrawString("Ph:"+Phone, _Font9, _SolidBrush, startX, startY);
                        }
                        dr.Close();
                    }
                    else if (PostCodeId > 0)
                    {
                        DataTable dt;
                        using (dt = new DataTable())
                        {
                            try
                            {
                                dt = GetDataTable($"SELECT top 1 PostCode, Town, City, State, StnCode, deliveryFrequency FROM tblPostcode2020 WHERE PostCodeId = {PostCodeId} AND isdelete = 0");
                                for (int p = 0; p < dt.Rows.Count; p++)
                                {
                                    string SendTo = "";
                                    string CompName = "";
                                    string Address1 = "";
                                    string Address2 = "";
                                    string State = dt.Rows[p]["State"].ToString();
                                    string City = dt.Rows[p]["City"].ToString();
                                    string ZipCode = dt.Rows[p]["PostCode"].ToString();
                                    string Phone = "";

                                    startX = 420; startY = 105;
                                    graphics.DrawString(SendTo, _Font9, _SolidBrush, startX, startY);
                                    startY = 120;
                                    graphics.DrawString(CompName, _Font9, _SolidBrush, startX, startY);
                                    startY = 135;
                                    graphics.DrawString(Address1, _Font9, _SolidBrush, startX, startY);
                                    startY = 150;
                                    graphics.DrawString(Address2, _Font9, _SolidBrush, startX, startY);
                                    startY = 165;
                                    graphics.DrawString(City, _Font9, _SolidBrush, startX, startY);
                                    startY = 180;
                                    graphics.DrawString(State + " - " + ZipCode, _Font9, _SolidBrush, startX, startY);
                                    startY = 195;
                                    graphics.DrawString("Ph:" + Phone, _Font9, _SolidBrush, startX, startY);

                                    break;
                                }
                            }
                            catch
                            {

                            }
                        }
                    }

                    string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                    ConsignmentNote = DTCNList.Rows[i][0].ToString();
                    ShipmentType = DTCNList.Rows[i][1].ToString();
                    DestCode = DTCNList.Rows[i][2].ToString();
                    Pieces = DTCNList.Rows[i][3].ToString();
                    Weight = DTCNList.Rows[i][4].ToString();
                    VL = DTCNList.Rows[i][5].ToString();
                    VB = DTCNList.Rows[i][6].ToString();
                    VH = DTCNList.Rows[i][7].ToString();
                    VolW = DTCNList.Rows[i][8].ToString();
                    ReceivedBy = DTCNList.Rows[i][9].ToString();
                    Date = DTCNList.Rows[i][10].ToString();
                    IsIns = DTCNList.Rows[i][11].ToString();
                    DestStation = DTCNList.Rows[i][12].ToString();
                    InsShipmntvalue = DTCNList.Rows[i][13].ToString();
                    InsChrgs = DTCNList.Rows[i][14].ToString();
                    CourierChrgs = DTCNList.Rows[i][15].ToString();
                    TotalChrgs = DTCNList.Rows[i][16].ToString();
                    DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                    decimal len, brd, hgt, vol;
                    len = Math.Round(Convert.ToDecimal(VL), 0);
                    brd = Math.Round(Convert.ToDecimal(VB), 0);
                    hgt = Math.Round(Convert.ToDecimal(VH), 0);
                    vol = Math.Round(Convert.ToDecimal(VolW), 0);

                    //graphics.DrawString("*", _Font1, _SolidBrush, 0, 0);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 840, 0);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 0, 390);
                    //graphics.DrawString("*", _Font1, _SolidBrush, 840, 390);

                    // LINE. 0
                    //startX = 400; startY = Y + 15;    // Before 19-May-2020
                    //startX = 400; startY = Y + 25;      // 19-May-2020
                    startX = 400; startY = Y + 31;      // 09-Jun-2020
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                    int width = 280; int height = 30; //int width = 350; int height = 30;
                    string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                    Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                    
                    graphics.DrawImage(CNImages1, startX, startY, width, height);   // Barcode
                    graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 300, startY + 7); // AWBNumber

                    //// AWBNumber bellow Barcode
                    //startX = 500; startY = Y + 45; Offset = 1;
                    //graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                    //LINE DEST Code
                    startX = 720; startY = Y + 45; Offset = 30;
                    Font _Font10 = new Font("Arial", 18);
                    graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);

                    //Cash Account Number
                    startX = 250; graphics.DrawString(CashAccNum, _Font9, _SolidBrush, startX, startY + Offset);

                    if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                    {
                        startX = 1; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }
                    else
                    {
                        startX = 76; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }

                    //LINE Vol Weight
                    if (!string.IsNullOrEmpty(vol.ToString().Trim()))
                    {
                        if (Convert.ToDecimal(vol) > 0)
                        {
                            startX = 200; startY = Y + 45; Offset = 1;
                            graphics.DrawString("Vol. Weight : " + vol.ToString().Trim() + " kg", _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        else
                        {
                            startX = 200; startY = Y + 45; Offset = 1;
                            graphics.DrawString("Vol. Weight : ", _Font2, _SolidBrush, startX, startY + Offset);
                        }
                    }
                    startY = Y; Offset = 234;
                    //startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 90; graphics.DrawString(Weight.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Pieces
                    startX = 24; startY = Y; Offset = 267;
                    graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Weight
                    //startY = Y; Offset = 271;
                    //startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
                    startY = Y; Offset = 267;
                    startX = 230; graphics.DrawString("RM:" + string.Format("{0:0.00}", Convert.ToDecimal(TotalChrgs)), _Font1, _SolidBrush, startX, startY + 264);
                    startX = 320; graphics.DrawString(CSDT.ToString("HH:mm"), _Font8, _SolidBrush, startX, startY + Offset);
                    startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font8, _SolidBrush, startX, startY + Offset);

                    if (IsIns == "True" || IsIns == "1")
                    {
                        //LINE Insurance Yes
                        startX = 21; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double inshpamt = Convert.ToDouble(InsShipmntvalue);
                        double inschrgs = Convert.ToDouble(InsChrgs);
                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 124; startY = Y; Offset = 299;
                        graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 308;
                        graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 317;
                        graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 326;
                        graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        //LINE Insurance No
                        startX = 201; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 215; startY = Y; Offset = 315;
                        graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 215; startY = Y; Offset = 324;
                        graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    Y += 400;

                    ExecuteQuery($"INSERT INTO tblCSPrintDate(AWBNum, CreatedBy, CreatedDate, Status) VALUES ('{ConsignmentNote}', {LoginUserId}, GETDATE(), 1)");
                }
            }
        }

        public void TestPrint(string OutputLocation)
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            int numberofrows = 1;
            if (numberofrows > 0)
            {
                PaperSize psize = new PaperSize("Custom", 850, 400 * numberofrows);
                ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                Margins margins = new Margins(50, 0, 0, 0);
                pd.Document.DefaultPageSettings.Margins = margins;
                //pdoc.DefaultPageSettings.PaperSize.Height = 400 * numberofrows;//320
                //pdoc.DefaultPageSettings.PaperSize.Width = 850;
                pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;

                pdoc.PrintPage += new PrintPageEventHandler(TestPDTBC);
                pp.Document = pdoc;
            }
            System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("Do you want to preview?", "Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo);
            if (dialogResult1 == System.Windows.Forms.DialogResult.Yes)
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }
            else
            {
                if (!string.IsNullOrEmpty(CNPrinter)) { pdoc.PrinterSettings.PrinterName = CNPrinter; pdoc.Print(); }
                else
                {
                    System.Windows.Forms.DialogResult result = pd.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                        pdoc.Print();
                }
            }
        }

        private void TestPDTBC(object sender, PrintPageEventArgs e)
        {
            SqlDataReader dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile Where RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");
            string CashAccNum = dr.Read() ? dr["CashAccNum"].ToString() : ""; dr.Close();

            Graphics graphics = e.Graphics;
            //Font font = new Font("Courier New", 10);
            Font font = new Font("Arial", 10);
            float fontHeight = font.GetHeight();
            int startX = 1;
            int startY = 30;
            int Offset = 40;

            Font _Font0 = new Font("Arial", 12);
            Font _Font1 = new Font("Arial", 10);
            Font _Font2 = new Font("Arial", 6);
            Font _Font3 = new Font("Arial", 4);
            Font _Font7 = new Font("Arial", 7);
            Font _Font8 = new Font("Arial", 8);
            Font _Font9 = new Font("Arial", 9);
            SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
            int Y = 0;
            DateTime CSDT = DateTime.Now;

            // LINE. 0
            startX = 400; startY = Y + 15;
            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
            int width = 280; int height = 30; //int width = 350; int height = 30;
            string cnn = "012345678900";// "*" + ConsignmentNote.Trim() + "*";
            Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
            graphics.DrawImage(CNImages1, startX, startY, width, height);
            graphics.DrawString(cnn, _Font0, _SolidBrush, 300, startY + 7);
            startX = 500; startY = Y + 45; Offset = 1;
            graphics.DrawString(cnn, _Font1, _SolidBrush, startX, startY + Offset);

            //LINE DEST Code
            startX = 720; startY = Y + 45; Offset = 30;
            Font _Font10 = new Font("Arial", 18);
            graphics.DrawString("DEST", _Font10, _SolidBrush, startX, startY + Offset);


            //Cash Account Number
            startX = 250; graphics.DrawString(CashAccNum, _Font9, _SolidBrush, startX, startY + Offset);

            startX = 1; startY = Y; Offset = 203;
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

            startX = 76; startY = Y; Offset = 203;
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

            //LINE Vol Weight

            startX = 200; startY = Y + 45; Offset = 1;
            graphics.DrawString("Vol. Weight : ", _Font2, _SolidBrush, startX, startY + Offset);

            startY = Y; Offset = 234;
            //startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
            startX = 90; graphics.DrawString("0.00".ToString(), _Font2, _SolidBrush, startX, startY + Offset);
            startX = 225; graphics.DrawString("0".ToString(), _Font2, _SolidBrush, startX, startY + Offset);
            startX = 255; graphics.DrawString("0".ToString(), _Font2, _SolidBrush, startX, startY + Offset);
            startX = 290; graphics.DrawString("0".ToString(), _Font2, _SolidBrush, startX, startY + Offset);
            startX = 330; graphics.DrawString("TEST", _Font2, _SolidBrush, startX, startY + Offset);

            //LINE Pieces
            startX = 24; startY = Y; Offset = 267;
            graphics.DrawString("1", _Font2, _SolidBrush, startX, startY + Offset);

            //LINE Weight
            //startY = Y; Offset = 271;
            //startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
            startY = Y; Offset = 267;
            startX = 230; graphics.DrawString("RM: 0.00", _Font1, _SolidBrush, startX, startY + 264);
            startX = 320; graphics.DrawString(CSDT.ToShortTimeString(), _Font7, _SolidBrush, startX, startY + Offset);
            startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font7, _SolidBrush, startX, startY + Offset);

            //LINE Insurance Yes
            startX = 21; startY = Y; Offset = 280;
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

            startX = 124; startY = Y; Offset = 299;
            graphics.DrawString("0.00", _Font3, _SolidBrush, startX, startY + Offset);
            startX = 110; startY = Y; Offset = 308;
            graphics.DrawString("0.00", _Font3, _SolidBrush, startX, startY + Offset);
            startX = 110; startY = Y; Offset = 317;
            graphics.DrawString("0.00", _Font3, _SolidBrush, startX, startY + Offset);
            startX = 110; startY = Y; Offset = 326;
            graphics.DrawString("0.00", _Font3, _SolidBrush, startX, startY + Offset);

            //LINE Insurance No
            startX = 201; startY = Y; Offset = 280;
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
            graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

            startX = 215; startY = Y; Offset = 315;
            graphics.DrawString("Courier Charges RM:0.00", _Font3, _SolidBrush, startX, startY + Offset);
            startX = 215; startY = Y; Offset = 324;
            graphics.DrawString("Total Charges   RM: 0.00", _Font3, _SolidBrush, startX, startY + Offset);
        }

        private void PrintDataTablewWORKED(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                SqlDataReader dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile Where RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");
                string CashAccNum = dr.Read() ? dr["CashAccNum"].ToString() : ""; dr.Close();

                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font9 = new Font("Arial", 9);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;
                for (int i = 0; i < DTCNList.Rows.Count; i++)
                {
                    string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                    ConsignmentNote = DTCNList.Rows[i][0].ToString();
                    ShipmentType = DTCNList.Rows[i][1].ToString();
                    DestCode = DTCNList.Rows[i][2].ToString();
                    Pieces = DTCNList.Rows[i][3].ToString();
                    Weight = DTCNList.Rows[i][4].ToString();
                    VL = DTCNList.Rows[i][5].ToString();
                    VB = DTCNList.Rows[i][6].ToString();
                    VH = DTCNList.Rows[i][7].ToString();
                    VolW = DTCNList.Rows[i][8].ToString();
                    ReceivedBy = DTCNList.Rows[i][9].ToString();
                    Date = DTCNList.Rows[i][10].ToString();
                    IsIns = DTCNList.Rows[i][11].ToString();
                    DestStation = DTCNList.Rows[i][12].ToString();
                    InsShipmntvalue = DTCNList.Rows[i][13].ToString();
                    InsChrgs = DTCNList.Rows[i][14].ToString();
                    CourierChrgs = DTCNList.Rows[i][15].ToString();
                    TotalChrgs = DTCNList.Rows[i][16].ToString();
                    DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                    decimal len, brd, hgt, vol;
                    len = Math.Round(Convert.ToDecimal(VL), 0);
                    brd = Math.Round(Convert.ToDecimal(VB), 0);
                    hgt = Math.Round(Convert.ToDecimal(VH), 0);
                    vol = Math.Round(Convert.ToDecimal(VolW), 0);
                    // LINE. 0
                    startX = 430; startY = Y + 15; 
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                    int width = 280; int height = 30; //int width = 350; int height = 30;
                    string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                    Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                    graphics.DrawImage(CNImages1, startX, startY, width, height);
                    graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 350, startY+7);
                    startX = 500; startY = Y + 45; Offset = 1;
                    graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                    //LINE DEST Code
                    startX = 720; startY = Y + 45; Offset = 30;
                    Font _Font10 = new Font("Arial", 18);
                    graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);
                    //Cash Account Number
                    startX = 250; graphics.DrawString(CashAccNum, _Font9, _SolidBrush, startX, startY + Offset);
                    if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                    {
                        startX = 1; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }
                    else
                    {
                        startX = 76; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }

                    //LINE Vol Weight
                    startY = Y; Offset = 234;
                    startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);
                    
                    //LINE Pieces
                    startX =24; startY = Y; Offset = 267;
                    graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Weight
                    startY = Y; Offset = 271;
                    startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
                    startY = Y; Offset = 267;
                    startX = 230; graphics.DrawString("RM:"+TotalChrgs, _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 320; graphics.DrawString(CSDT.ToShortTimeString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font2, _SolidBrush, startX, startY + Offset);

                    if (IsIns == "True" || IsIns == "1")
                    {
                        //LINE Insurance Yes
                        startX = 21; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double inshpamt = Convert.ToDouble(InsShipmntvalue);
                        double inschrgs = Convert.ToDouble(InsChrgs);
                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 124; startY = Y; Offset = 299;
                        graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 308;
                        graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 317;
                        graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 326;
                        graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        //LINE Insurance No
                        startX = 201; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 215; startY = Y; Offset = 315;
                        graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 215; startY = Y; Offset = 324;
                        graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }

                    if (!string.IsNullOrEmpty(ConsignorCode) && string.IsNullOrEmpty(ConsigneeCode))
                    {
                        dr = GetDataReader($"SELECT a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile FROM tblConsignor a Where a.ConsignorCode = '{ConsignorCode}'");
                        if (dr.Read())
                        {
                            // LINE. 1
                            startX = 1; startY = Y+30; Offset = 40;
                            _Font1 = new Font("Arial", 10);
                            graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);
                            //startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 2
                            Offset = Offset + 15; startX = 1; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                            startX = 250; graphics.DrawString("", _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 3
                            Offset = Offset + 12; startX = 1; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 4
                            Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 5
                            Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 6
                            Offset = Offset + 24; startX = 50; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        dr.Close();
                    }
                    else if (string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                    {
                        dr = GetDataReader($"SELECT a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone ,Mobile= a.Mobile FROM tblConsignee a Where a.ConsigneeCode = '{ConsigneeCode}'");
                        if (dr.Read())
                        {
                            // LINE. 1
                            startY = Y+30;
                            _Font1 = new Font("Arial", 10);
                            Offset = 70; startX = 420; graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 2
                            Offset = Offset + 15; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 3
                            Offset = Offset + 12; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 4
                            Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 5
                            Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 6
                            Offset = Offset + 55; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                            startX = 670; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                            // LINE. 7
                            Offset = Offset + 65; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                            startX = 670; graphics.DrawString(dr["Mobile"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        dr.Close();
                    }
                    else if (!string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                    {
                        dr = GetDataReader($"SELECT ConsignorName = d.Name, ConsignorAddress1 = d.Add1, ConsignorAddress2 = d.Add2, ConsignorAddress3 = d.City + '-' + d.PostalCode, ConsignorAddress4 = d.State + ',' + d.Country, ConsignorContact = d.Phone, ConsignorMobile = d.Mobile, ConsignorDeptCode = '', ConsignorCode = d.ConsignorCode," +
                            $" ConsigneeName = a.Name, ConsigneeAddress1 = a.Add1, ConsigneeAddress2 = a.Add2, ConsigneeAddress3 = a.City + '-' + a.PostalCode, ConsigneeAddress4 = a.State + ',' + a.Country, ConsigneeContact = a.Phone + '/' + a.Mobile, ConsigneeMobile = a.Mobile, ConsigneeDeptCode = '', ConsigneeCode = a.ConsigneeCode, ConsigneeAttention = '' FROM tblConsignee a" +
                            $" join tblConsignors d on a.ConsignorCode = d.ConsignorCode" +
                            $" Where a.ConsigneeCode = '{ConsigneeCode}' and d.ConsignorCode = '{ConsignorCode}'");
                        if (dr.Read())
                        {
                            // LINE. 1
                            startX = 1; startY = Y+30; Offset = 40;
                            _Font1 = new Font("Arial", 10);
                            graphics.DrawString(dr["ConsignorName"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                                             // SHIPPER NAME
                            //startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                              // SHIPPER ACCNO

                            // LINE. 2
                            startX = 1; Offset = Offset + 15; graphics.DrawString(dr["ConsignorAddress1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD1
                            startX = 250; graphics.DrawString(dr["ConsignorDeptCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // SHIPPER DEPTCODE

                            // LINE. 3
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD2
                            startX = 420; graphics.DrawString(dr["ConsigneeName"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // CONSIGNEE NAME

                            // LINE. 4
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD3
                            startX = 420; graphics.DrawString(dr["ConsigneeAddress1"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD1

                            // LINE. 5
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD4
                            startX = 420; graphics.DrawString(dr["ConsigneeAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD2

                            // LINE. 6
                            Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                     // CONSIGNEE ADD3

                            // LINE. 7
                            Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                     // CONSIGNEE ADD4
                            startX = 50; graphics.DrawString(dr["ConsignorContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                             // SHIPPER PHONE

                            // LINE. 8
                            Offset = Offset + 55; startX = 470; graphics.DrawString(dr["ConsigneeAttention"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                    // CONSIGNEE ATTENTION
                            startX = 670; graphics.DrawString(dr["ConsigneeContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                            // CONSIGNEE PHONE
                            Offset = Offset + 65; startX = 670; graphics.DrawString(dr["ConsigneeMobile"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                      // CONSIGNEE PHONE
                        }
                        dr.Close();
                    }
                    Y += 400;
                }
            }
        }

        public void TaxInvoices(string CSNo)
        {
            DataTable DTCompany = new DataTable();
            DataTable DTMas = new DataTable();
            DataTable DTTran = new DataTable();
            DataTable DTHelp = new DataTable();

            try
            {
                if (CSNo.Trim() != "")
                {
                    getTaxDetail();
                    string CodeConsignor = "";

                    decimal limitkg = Convert.ToDecimal(30.009);

                    //DTMas = GetDataTable($"SELECT num.CNNum, gen.CSalesNo, gen.CSalesDate, num.ShipmentType, num.DestCode, num.Pieces, num.Weight, num.VLength, num.VBreadth, num.VHeight, num.VolWeight, gen.Price, gen.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, num.InsShipmentDesc, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, gen.StaffDisAmt, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount, gen.ShipmentTotal, gen.TaxTitle, gen.TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate FROM tblCashSales gen" +
                    //    $" LEFT JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo" +
                    //    $" WHERE gen.CSalesNo = '{CSNo}' AND gen.Status = 1");
                    
                    DTMas = GetDataTable($"SELECT iif(num.shipmenttype = 'STATIONERY', item.itemdesc, num.CNNum)  as CNNum, gen.CSalesNo, gen.CSalesDate, num.ShipmentType, num.DestCode, num.Pieces, num.Weight, num.VLength, num.VBreadth, num.VHeight, num.VolWeight, num.Price, num.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, num.InsShipmentDesc, num.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, num.StaffDiscAmt as 'StaffDisAmt', gen.DiscCode, gen.DiscPercentage, num.PromoDiscAmt as 'DiscAmount', gen.ShipmentTotal, num.TaxCode as TaxTitle, num.TaxRate as TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate, (num.TotalAmt + num.InsValue - num.StaffDiscAmt - num.PromoDiscAmt) as AmountperRow, ((num.TotalAmt + num.InsValue - num.StaffDiscAmt - num.PromoDiscAmt) * num.TaxRate / 100) as AmountperRowAfterTax FROM tblCashSales gen" +
                        $" LEFT JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo " +
                        $" LEFT JOIN tblItemMaster item on num.CNNum = item.ItemCode " +
                        $" WHERE gen.CSalesNo = '{CSNo}' AND num.CSalesNo = '{CSNo}' AND gen.Status = 1 ");
                    
                    DTTran = GetDataTable($"Select A.CSalesNo, B.TaxCode, B.TotalAmt AS TotalAmt1, IIF(SUBSTRING(B.ShipmentType, 1, 5) = 'MOTOR' or B.HighestWeight > 30, (B.InsValue * B.TaxRate / 100), (B.TotalAmt * B.TaxRate / 100)) AS TaxAmt1, C.TaxCode, C.TotalAmt AS TotalAmt2, (C.TotalAmt * C.TaxRate / 100) AS TaxAmt2, (A.ShipmentTOTAL + A.STATIONARYTOTAL + A.InsValue) as TotalBeforeTax, A.ShipmentGSTValue AS TotalTaxAmt, (A.DiscAmount + A.StaffDisAmt) as TotalDiscount, C.RASurcharges as RASurcharge, A.CSalesTotal, ISNULL(PaymentMode, 'C A S H') as PaymentMode from tblCashSales a" +
                        $" LEFT JOIN(SELECT CSalesNo, TaxCode, TaxRate, ShipmentType, InsValue, IIF(Weight > VolWeight, Weight, VolWeight) as HighestWeight, SUM(TotalAmt + InsValue - StaffDiscAmt - PromoDiscAmt) AS TotalAmt FROM tblCSaleCNNum WHERE TAXCODE = '{ StandardTaxCode }' GROUP BY CSalesNo, TaxCode, TaxRate, ShipmentType, InsValue, Weight, VolWeight) B ON A.CSalesNo = B.CSalesNo" +
                        $" LEFT JOIN(SELECT CSalesNo, TaxCode, TaxRate, ShipmentType, SUM(TotalAmt + InsValue - StaffDiscAmt - PromoDiscAmt) AS TotalAmt, Sum(ISNull(RASurcharge,0)) as RASurcharges FROM tblCSaleCNNum WHERE TAXCODE = '{ InternationalTaxCode }' GROUP BY CSalesNo, TaxCode, TaxRate, ShipmentType) C ON A.CSalesNo = C.CSalesNo" +
                        $" WHERE A.CSalesNo = '{CSNo}'");

                    DateTime fdate;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{CSNo}'");
                    dr.Read(); fdate = Convert.ToDateTime(dr["CSalesDate"]).Date; dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND ((a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}') OR (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND a.ToDate IS NULL))");
                    if (DTCompany.Rows.Count <= 0)
                    {
                        DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                            $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}'");
                    }

                    DTCompany.AsEnumerable().Where(rec => rec["Address1"].ToString() != "").ToList().ForEach(rec => { rec["Address1"] = rec["Address1"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Address2"].ToString() != "").ToList().ForEach(rec => { rec["Address2"] = rec["Address2"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Address3"].ToString() != "").ToList().ForEach(rec => { rec["Address3"] = rec["Address3"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["City"].ToString() != "").ToList().ForEach(rec => { rec["City"] = rec["City"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["State"].ToString() != "").ToList().ForEach(rec => { rec["State"] = rec["State"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Country"].ToString() != "").ToList().ForEach(rec => { rec["Country"] = rec["Country"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["PostalCode"].ToString() != "").ToList().ForEach(rec => { rec["PostalCode"] = rec["PostalCode"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["PhoneNo"].ToString() != "").ToList().ForEach(rec => { rec["PhoneNo"] = rec["PhoneNo"] + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["FaxNo"].ToString() != "").ToList().ForEach(rec => { rec["FaxNo"] = rec["FaxNo"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["EMail"].ToString() != "").ToList().ForEach(rec => { rec["EMail"] = rec["EMail"].ToString().Trim(); });

                    FrmReportViewer RptViewerGen = new FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompany, "CashSalesDetails", DTMas, "InvoiceSummary", DTTran, "", DTHelp, "RPTCSTaxInvoice.rdlc", "", RptViewerGen.RptViewer, true, false);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
                else { MessageBox.Show("Invalid Cash sales number", "Warning"); }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Function : " + Ex.ToString()); }
        }

        public void AWBPrint(string awb, string outputoption)
        {
            AWBNumber = awb;
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            
            PaperSize psize = new PaperSize("Custom", 950, 400);
            ps.DefaultPageSettings.PaperSize = psize;

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            Margins margins = new Margins(50, 0, 0, 0);
            pd.Document.DefaultPageSettings.Margins = margins;
            pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;
            pdoc.PrintPage += new PrintPageEventHandler(PrintAWB);
            pp.Document = pdoc;
            
            if (outputoption == "Print")
            {
                try
                {
                    if (!string.IsNullOrEmpty(CNPrinter))
                    {
                        pdoc.PrinterSettings.PrinterName = CNPrinter; pdoc.Print();
                    }
                    else
                    {
                        System.Windows.Forms.DialogResult result = pd.ShowDialog();
                        if (result == System.Windows.Forms.DialogResult.OK)
                            pdoc.Print();
                    }
                }
                catch (Exception Ex)
                {
                    System.Windows.Forms.DialogResult result = pd.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                        pdoc.Print();
                }
            }
            else if (outputoption == "Preview")
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }
        }

        public string imageToBase64(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            byte[] imageBytes = ms.ToArray();

            // Convert byte[] to Base64 String
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
            //return ms.ToArray();
        }

        public void AWBPrintSticker(string awb)
        {
            DataTable DTCompany = new DataTable();
            DataTable DTMas = new DataTable();
            DataTable DTTran = new DataTable();
            DataTable DTHelp = new DataTable();

            try
            {
                SqlDataReader dr;
                Barcode bc = new Barcode();
                bc.EncodedType = TYPE.CODE128;
                System.Drawing.Image barcode;
                bool isMobileRef = false;

                barcode = bc.Encode(TYPE.CODE128, awb, awb.Length * 40, 100);

                dr = GetDataReader($"SELECT Top 1 MobRefNum FROM tblCSaleCNNum Where CNNum = '{awb}' and MobRefNum != ''");
                if (dr.Read())
                {
                    isMobileRef = true;
                }
                dr.Close();

                if (isMobileRef)
                {
                    DTTran = GetDataTable($"SELECT '{imageToBase64(barcode)}' as AWBBarcode, '{ MyCompanyCode.Trim() }' as StnCode, c.CashAccNum as AccNo, a.CSalesNo as TransactionNo,a.CNNum as AWBNo, a.ShipmentType as Type, a.Pieces, a.Weight, '' as value, '' as CODAmount, '' as ShipperAccNo, CONVERT(VARCHAR(10), a.CDate, 103) as PickupDate, b.ShipperName, b.ShipperAdd1 as ShipperAddress1,b.ShipperAdd2 as ShipperAddress2, b.ShipperAdd4 as ShipperAddress3, b.ShipperAdd3 as ShipperPostCode, b.ShipperTelNo as ShipperContactNo, '' as ShipperReference, b.SendTo as ConsigneeAttn, b.SendTo as ConsigneeName, b.Address1 as ConsigneeAddress1,b.Address2 as ConsigneeAddress2, b.City, b.State, b.ZipCode as ConsigneeDestCode, b.Phone as ConsigneeTelNo, a.ShipmentType as ShipmentContent, '' as Dimensions, a.VolWeight as ShipmentVolumeWeight, '' as Department, b.ShipperName as ContactPerson, CONVERT(INT, a.VHeight) as Height, CONVERT(INT, a.VBreadth) as Width, CONVERT(INT, a.VLength) as Length, a.VolWeight as VolWt, cast(ROUND((a.TotalAmt + a.InsValue) + (IIF(Substring(a.ShipmentType, 1, 5) = 'Motor' or IIF(a.Weight > a.VolWeight, a.Weight, a.VolWeight) > 30, (a.InsValue * a.TaxRate / 100), ((a.TotalAmt + a.InsValue) * a.TaxRate / 100))),2) as numeric(36,2)) as PaidAmount, a.DestCode, 'By printing this AWB, I agree to Skynet''s Term & Conditions, and a copy of the terms can be provided to me upon request. Strictly no cash, prohibited & dangerous goods. For East Malaysia & Intl, attach original & 3 copies of commercial invoice for Customs Clearance' as ReprintText " +
                        $"FROM tblcsalecnnum a LEFT JOIN tblRefMobileApp b ON a.MobRefNum = b.RefNum " +
                        $"Left join tblStationProfile c on a.OriginCode = c.CompCode WHERE cnnum = '{awb}'");

                    ExecuteQuery($"INSERT INTO tblCSPrintDate(AWBNum, CreatedBy, CreatedDate, Status) VALUES ('{ConsignmentNote}', {LoginUserId}, GETDATE(), 1)");
                }
                //else
                //{
                //    DTTran = GetDataTable($"SELECT '{imageToBase64(barcode)}' as AWBBarcode, '{ MyCompanyCode.Trim() }' as StnCode, d.CashAccNum as AccNo, a.CSalesNo as TransactionNo,a.CNNum as AWBNo, a.ShipmentType as Type, a.Pieces, a.Weight, '' as value, '' as CODAmount, '' as ShipperAccNo, CONVERT(VARCHAR(10), a.CDate, 103) as PickupDate, b.Name as ShipperName, b.Add1 as ShipperAddress1, b.Add2 as ShipperAddress2, b.City as ShipperAddress3, b.PostalCode as ShipperPostCode, b.Phone as ShipperContactNo, '' as ShipperReference, c.Name as ConsigneeAttn, c.Name as ConsigneeName, c.Add1 as ConsigneeAddress1, c.Add2 as ConsigneeAddress2, c.City as City, '' as State, c.PostalCode as ConsigneeDestCode, c.Phone as ConsigneeTelNo, A.ShipmentType as ShipmentContent, '' as Dimension, a.VolWeight as ShipmentVolumeWeight, '' as Department, b.Name as ContactPerson, CONVERT(INT, a.VHeight) as Height, CONVERT(INT, a.VBreadth) as Width, CONVERT(INT, a.VLength) as Length, a.VolWeight as VolWt, a.TotalAmt as PaidAmount, a.DestCode, 'By printing this AWB, I agree to Skynet''s Term & Conditions, and a copy of the terms can be provided to me upon request. Strictly no cash, prohibited & dangerous goods. For East Malaysia & Intl, attach original & 3 copies of commercial invoice for Customs Clearance' as ReprintText FROM tblcsalecnnum a LEFT JOIN tblConsignor b ON a.CNNum = b.AWBNum LEFT JOIN tblConsignee c ON a.CNNum = c.AWBNum LEFT JOIN tblStationProfile d on a.OriginCode = d.CompCode WHERE a.CNNum = '{ awb }'");
                //ExecuteQuery($"INSERT INTO tblCSPrintDate(AWBNum, CreatedBy, CreatedDate, Status) VALUES ('{ConsignmentNote}', {LoginUserId}, GETDATE(), 1)");
                //}
                //DataRow Newrow;
                //DTTran.Columns.Add("AWBBarcode");
                //DTTran.Columns.Add("AWBNo");
                //Newrow = DTTran.NewRow();
                ////barcode = bc.Encode(TYPE.CODE128, awb, awb.Length * 40, 100);
                //barcode = bc.Encode(BarcodeLib.TYPE.CODE128, awb, awb.Length * 40, 80);
                ////var a = imageToByteArray(barcode);
                //Newrow["AWBBarcode"] = imageToBase64(barcode);
                //Newrow["AWBNo"] = awb;

                //DTTran.Rows.Add(Newrow);

                //barcode.Save(@"C:\Users\hp\Desktop\Work\testbarcode.png", System.Drawing.Imaging.ImageFormat.Png);

                FrmReportViewer RptViewerGen = new FrmReportViewer();
                RptViewerGen.Owner = Application.Current.MainWindow;
                ReportViewerVM VM = new ReportViewerVM("", DTCompany, "", DTMas, "StickerAWB", DTTran, "", DTHelp, "AWBSticker.rdlc", "", RptViewerGen.RptViewer, true, false);
                RptViewerGen.DataContext = VM;
                //RptViewerGen.Show();
                RptViewerGen.ShowDialog();
            }
            catch (Exception Ex) { error_log.errorlog("AWB Sticker error : " + Ex.ToString()); }
        }

        //Bytescout.BarCode.Barcode bb = new Bytescout.BarCode.Barcode();
        private void PrintAWB(object sender, PrintPageEventArgs e)
        {
            SqlDataReader dr;

            if (AWBNumber.Trim() != "")
            {
                DataTable dtAWB = new DataTable();
                dtAWB = GetDataTable($"SELECT Top 1 CNNum, CDate, ShipmentType, DestCode, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, 0.00 as CSalesTotal, MobRefNum, PostCodeId, TotalAmt, InsShipmentValue, InsValue, (TotalAmt + InsValue - StaffDiscAmt - PromoDiscAmt) as NetAmount, b.UserName, a.TaxRate, a.RASurcharge, a.TaxCode FROM tblCSaleCNNum a" +
                    $" LEFT JOIN tblUserMaster b ON a.CUserID = b.UserId" +
                    $" WHERE CNNum = '{AWBNumber}'");

                dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile Where RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");
                string CashAccNum = dr.Read() ? dr["CashAccNum"].ToString() : ""; dr.Close();

                CashAccNum = !string.IsNullOrEmpty(CashAccNum) ? CashAccNum : " ";

                Graphics graphics = e.Graphics;
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                Font _Font8 = new Font("Arial", 8);
                Font _Font9 = new Font("Arial", 9);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;

                string refnum = dtAWB.Rows[0]["MobRefNum"].ToString();
                int PostCodeId = Convert.ToInt32(dtAWB.Rows[0]["PostCodeId"]);

                if (!string.IsNullOrEmpty(refnum))
                {
                    dr = GetDataReader($"SELECT * FROM tblRefMobileApp a Where a.RefNum = '{refnum}'");
                    if (dr.Read())
                    {
                        string RefNum = dr["RefNum"].ToString();
                        string ShipperName = dr["ShipperName"].ToString();
                        string ShipperAdd1 = dr["ShipperAdd1"].ToString();
                        string ShipperAdd2 = dr["ShipperAdd2"].ToString();
                        string ShipperAdd3 = dr["ShipperAdd3"].ToString();
                        string ShipperAdd4 = dr["ShipperAdd4"].ToString();
                        string ShipperTelNo = dr["ShipperTelNo"].ToString();
                        string SendTo = dr["SendTo"].ToString();
                        string CompName = dr["CompName"].ToString();
                        string Address1 = dr["Address1"].ToString();
                        string Address2 = dr["Address2"].ToString();
                        string State = dr["State"].ToString();
                        string City = dr["City"].ToString();
                        string ZipCode = dr["ZipCode"].ToString();
                        string Phone = dr["Phone"].ToString();

                        startX = 5; startY = 90;
                        graphics.DrawString(ShipperName, _Font9, _SolidBrush, startX, startY);
                        startY = 105;
                        graphics.DrawString(ShipperAdd1 + ", " + ShipperAdd2, _Font9, _SolidBrush, startX, startY);
                        startY = 120;
                        graphics.DrawString(ShipperAdd3 + ", " + ShipperAdd4, _Font9, _SolidBrush, startX, startY);
                        startX = 250;
                        graphics.DrawString("Ph: " + ShipperTelNo, _Font9, _SolidBrush, startX, startY);

                        startX = 420; startY = 105;
                        graphics.DrawString(SendTo, _Font9, _SolidBrush, startX, startY);
                        startY = 120;
                        graphics.DrawString(CompName, _Font9, _SolidBrush, startX, startY);
                        startY = 135;
                        graphics.DrawString(Address1, _Font9, _SolidBrush, startX, startY);
                        startY = 150;
                        graphics.DrawString(Address2, _Font9, _SolidBrush, startX, startY);
                        startY = 165;
                        graphics.DrawString(City, _Font9, _SolidBrush, startX, startY);
                        startY = 180;
                        graphics.DrawString(State + " - " + ZipCode, _Font9, _SolidBrush, startX, startY);
                        startY = 195;
                        graphics.DrawString("Ph:" + Phone, _Font9, _SolidBrush, startX, startY);
                    }
                    dr.Close();
                }
                //else
                //{
                //    dr = GetDataReader($"SELECT b.Name as ShipperName, b.Add1 as ShipperAddress1, b.Add2 as ShipperAddress2, b.City as ShipperAddress3, b.PostalCode as ShipperPostCode, b.Phone as ShipperContactNo, c.Name as ConsigneeName, c.Add1 as ConsigneeAddress1, c.Add2 as ConsigneeAddress2, c.City as City, c.PostalCode as ConsigneeDestCode, c.Phone as ConsigneeTelNo, c.CompanyName FROM tblcsalecnnum a LEFT JOIN tblConsignor b ON a.CNNum = b.AWBNum LEFT JOIN tblConsignee c ON a.CNNum = c.AWBNum LEFT JOIN tblStationProfile d on a.OriginCode = d.CompCode WHERE a.CNNum = '{ AWBNumber }'");
                //    if (dr.Read())
                //    {
                //        string ShipperName = dr["ShipperName"].ToString(),
                //            ShipperAdd1 = dr["ShipperAddress1"].ToString(),
                //            ShipperAdd2 = dr["ShipperAddress2"].ToString(),
                //            ShipperAdd3 = dr["ShipperAddress3"].ToString(),
                //            ShipperAdd4 = dr["ShipperPostCode"].ToString(),
                //            ShipperTelNo = dr["ShipperContactNo"].ToString(),
                //            SendTo = dr["ConsigneeName"].ToString(),
                //            CompName = dr["CompanyName"].ToString(),
                //            Address1 = dr["ConsigneeAddress1"].ToString(),
                //            Address2 = dr["ConsigneeAddress2"].ToString(),
                //            City = dr["City"].ToString(),
                //            State = "",
                //            ZipCode = dr["ConsigneeDestCode"].ToString(),
                //            Phone = dr["ConsigneeTelNo"].ToString();

                //        startX = 5; startY = 90;
                //        graphics.DrawString(ShipperName, _Font9, _SolidBrush, startX, startY);
                //        //startY = 105;
                //        //graphics.DrawString(ShipperAdd1 + ", " + ShipperAdd2, _Font9, _SolidBrush, startX, startY);
                //        //startY = 120;
                //        //graphics.DrawString(ShipperAdd3 + ", " + ShipperAdd4, _Font9, _SolidBrush, startX, startY);
                //        startY = 105;
                //        graphics.DrawString(ShipperAdd1/* + ", " + ShipperAdd2*/, _Font9, _SolidBrush, startX, startY);
                //        startY = 120;
                //        graphics.DrawString(/*ShipperAdd3 + ", " + ShipperAdd4*/ShipperAdd2, _Font9, _SolidBrush, startX, startY);
                //        startY = 135;
                //        graphics.DrawString(ShipperAdd4 + " " + ShipperAdd3, _Font9, _SolidBrush, startX, startY);
                //        startX = 250;
                //        graphics.DrawString("Ph: " + ShipperTelNo, _Font9, _SolidBrush, startX, startY);

                //        startX = 420; startY = 105;
                //        graphics.DrawString(SendTo, _Font9, _SolidBrush, startX, startY);
                //        startY = 120;
                //        graphics.DrawString(CompName, _Font9, _SolidBrush, startX, startY);
                //        startY = 135;
                //        graphics.DrawString(Address1, _Font9, _SolidBrush, startX, startY);
                //        startY = 150;
                //        graphics.DrawString(Address2, _Font9, _SolidBrush, startX, startY);
                //        //startY = 165;
                //        //graphics.DrawString(City, _Font9, _SolidBrush, startX, startY);
                //        startY = 180;
                //        graphics.DrawString(/*State*/City + " - " + ZipCode, _Font9, _SolidBrush, startX, startY);
                //        startY = 195;
                //        graphics.DrawString("Ph:" + Phone, _Font9, _SolidBrush, startX, startY);
                //    }
                //    dr.Close();
                //}

                else if (PostCodeId > 0)
                {
                    DataTable dt;
                    using (dt = new DataTable())
                    {
                        try
                        {
                            dt = GetDataTable($"SELECT top 1 PostCode, Town, City, State, StnCode, deliveryFrequency FROM tblPostcode2020 WHERE PostCodeId = {PostCodeId} AND isdelete = 0");
                            for (int p = 0; p < dt.Rows.Count; p++)
                            {
                                string SendTo = "";
                                string CompName = "";
                                string Address1 = "";
                                string Address2 = "";
                                string State = dt.Rows[p]["State"].ToString();
                                string City = dt.Rows[p]["City"].ToString();
                                string ZipCode = dt.Rows[p]["PostCode"].ToString();
                                string Phone = "";

                                startX = 420; startY = 105;
                                graphics.DrawString(SendTo, _Font9, _SolidBrush, startX, startY);
                                startY = 120;
                                graphics.DrawString(CompName, _Font9, _SolidBrush, startX, startY);
                                startY = 135;
                                graphics.DrawString(Address1, _Font9, _SolidBrush, startX, startY);
                                startY = 150;
                                graphics.DrawString(Address2, _Font9, _SolidBrush, startX, startY);
                                startY = 165;
                                graphics.DrawString(City, _Font9, _SolidBrush, startX, startY);
                                startY = 180;
                                graphics.DrawString(State + " - " + ZipCode, _Font9, _SolidBrush, startX, startY);
                                startY = 195;
                                graphics.DrawString("Ph:" + Phone, _Font9, _SolidBrush, startX, startY);

                                break;
                            }
                        }
                        catch
                        {

                        }
                    }
                }

                string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                decimal HighestWeight = Convert.ToDecimal(dtAWB.Rows[0]["Weight"]) > Convert.ToDecimal(dtAWB.Rows[0]["VolWeight"]) ? Convert.ToDecimal(dtAWB.Rows[0]["Weight"]) : Convert.ToDecimal(dtAWB.Rows[0]["VolWeight"]);

                decimal PTaxAmt = (Convert.ToDecimal((dtAWB.Rows[0]["ShipmentType"].ToString().Trim().StartsWith("MOTOR") || (HighestWeight > 30)) && dtAWB.Rows[0]["TaxCode"].ToString() == StandardTaxCode ? dtAWB.Rows[0]["Insvalue"] : dtAWB.Rows[0]["NetAmount"]) * Convert.ToDecimal(dtAWB.Rows[0]["TaxRate"]) / 100);

                decimal PAfterTax = Convert.ToDecimal(dtAWB.Rows[0]["NetAmount"]) + PTaxAmt + Convert.ToDecimal(dtAWB.Rows[0]["RASurcharge"]);
                //decimal PAfterTax = Convert.ToDecimal(dtAWB.Rows[0]["NetAmount"]) + (Convert.ToDecimal(dtAWB.Rows[0]["NetAmount"]) * Convert.ToDecimal(dtAWB.Rows[0]["TaxRate"]) / 100);
                //decimal PAfterTax = Convert.ToDecimal(dtAWB.Rows[0]["TotalAmt"]) + (Convert.ToDecimal(dtAWB.Rows[0]["TotalAmt"]) * Convert.ToDecimal(dtAWB.Rows[0]["TaxRate"]) / 100);

                ConsignmentNote = dtAWB.Rows[0]["CNNum"].ToString();
                ShipmentType = dtAWB.Rows[0]["ShipmentType"].ToString();
                DestCode = dtAWB.Rows[0]["DestCode"].ToString();
                Pieces = dtAWB.Rows[0]["Pieces"].ToString();
                Weight = dtAWB.Rows[0]["Weight"].ToString();
                VL = dtAWB.Rows[0]["VLength"].ToString();
                VB = dtAWB.Rows[0]["VBreadth"].ToString();
                VH = dtAWB.Rows[0]["VHeight"].ToString();
                VolW = dtAWB.Rows[0]["VolWeight"].ToString();
                ReceivedBy = dtAWB.Rows[0]["UserName"].ToString();
                Date = dtAWB.Rows[0]["CDate"].ToString();
                IsIns = Convert.ToDecimal(dtAWB.Rows[0]["InsValue"]) > 0 ? "1" : "0";
                DestStation = dtAWB.Rows[0]["DestCode"].ToString();
                InsShipmntvalue = dtAWB.Rows[0]["InsShipmentValue"].ToString();
                InsChrgs = dtAWB.Rows[0]["InsValue"].ToString();
                CourierChrgs = dtAWB.Rows[0]["TotalAmt"].ToString();
                //CourierChrgs = Math.Round(PAfterTax, 2).ToString();
                //TotalChrgs = dtAWB.Rows[0]["NetAmount"].ToString();
                TotalChrgs = Math.Round(PAfterTax, 2).ToString();
                DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                decimal len, brd, hgt, vol;
                len = Math.Round(Convert.ToDecimal(VL), 0);
                brd = Math.Round(Convert.ToDecimal(VB), 0);
                hgt = Math.Round(Convert.ToDecimal(VH), 0);
                vol = Math.Round(Convert.ToDecimal(VolW), 0);
                startX = 400; startY = Y + 31;      // 09-Jun-2020
                BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                int width = 280; int height = 30; //int width = 350; int height = 30;
                string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);

                graphics.DrawImage(CNImages1, startX, startY, width, height);   // Barcode
                graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 300, startY + 7); // AWBNumber

                //LINE DEST Code
                startX = 720; startY = Y + 45; Offset = 30;
                Font _Font10 = new Font("Arial", 18);
                graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);

                //Cash Account Number
                startX = 250; graphics.DrawString(CashAccNum, _Font9, _SolidBrush, startX, startY + Offset);

                if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                {
                    startX = 1; startY = Y; Offset = 203;
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                }
                else
                {
                    startX = 76; startY = Y; Offset = 203;
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                }

                //LINE Vol Weight
                if (!string.IsNullOrEmpty(vol.ToString().Trim()))
                {
                    if (Convert.ToDecimal(vol) > 0)
                    {
                        startX = 200; startY = Y + 45; Offset = 1;
                        graphics.DrawString("Vol. Weight : " + vol.ToString().Trim() + " kg", _Font2, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        startX = 200; startY = Y + 45; Offset = 1;
                        graphics.DrawString("Vol. Weight : ", _Font2, _SolidBrush, startX, startY + Offset);
                    }
                }
                startY = Y; Offset = 234;
                //startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                startX = 90; graphics.DrawString(Weight.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);

                //LINE Pieces
                startX = 24; startY = Y; Offset = 267;
                graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                startY = Y; Offset = 267;
                startX = 230; graphics.DrawString("RM:" + string.Format("{0:0.00}", Convert.ToDecimal(TotalChrgs)), _Font1, _SolidBrush, startX, startY + 264);
                startX = 320; graphics.DrawString(CSDT.ToString("HH:mm"), _Font8, _SolidBrush, startX, startY + Offset);
                startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font8, _SolidBrush, startX, startY + Offset);

                if (IsIns == "True" || IsIns == "1")
                {
                    //LINE Insurance Yes
                    startX = 21; startY = Y; Offset = 280;
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                    double inshpamt = Convert.ToDouble(InsShipmntvalue);
                    double inschrgs = Convert.ToDouble(InsChrgs);
                    double cchrgs = Convert.ToDouble(CourierChrgs);
                    double ctotal = Convert.ToDouble(TotalChrgs);
                    startX = 124; startY = Y; Offset = 299;
                    graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    startX = 110; startY = Y; Offset = 308;
                    graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    startX = 110; startY = Y; Offset = 317;
                    graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    startX = 110; startY = Y; Offset = 326;
                    graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                }
                else
                {
                    //LINE Insurance No
                    startX = 201; startY = Y; Offset = 280;
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                    graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                    double cchrgs = Convert.ToDouble(CourierChrgs);
                    double ctotal = Convert.ToDouble(TotalChrgs);
                    startX = 215; startY = Y; Offset = 315;
                    graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    startX = 215; startY = Y; Offset = 324;
                    graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                }
                Y += 400;

                ExecuteQuery($"INSERT INTO tblCSPrintDate(AWBNum, CreatedBy, CreatedDate, Status) VALUES ('{ConsignmentNote}', {LoginUserId}, GETDATE(), 1)");
            }
        }

    }
}
