﻿using System;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using SkynetCashSales.Model;
using SkynetCashSales.General;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Data.SqlClient;

namespace SkynetCashSales.View.Transactions
{
    public class CancelCashSaleVM : AMGenFunction
    {
        public event Action<ReturnItem> Closed;

        public CancelCashSaleVM()
        {
            EnteredBy = LoginUserName;
        }

        public CancelCashSaleVM(string CSID,string awb,string dest,short pcs,decimal wt,string type,decimal amount)
        {
            CSalesID =Convert.ToInt32(CSID);
            AWBNum = awb;
            DestCode = dest;
            Pieces = pcs;
            Weight = wt;
            ShipmentType = type;
            CSAmount = amount;
            EnteredBy = LoginUserName;
            EnteredDate =  DateTime.Now.Date.ToString();
        }

        public int CSalesID { get { return GetValue(() => CSalesID); } set { SetValue(() => CSalesID, value); OnPropertyChanged("CSalesID"); } }
        public string AWBNum { get { return GetValue(() => AWBNum); } set { SetValue(() => AWBNum, value); OnPropertyChanged("AWBNum"); } }
        public string DestCode { get { return GetValue(() => DestCode); } set { SetValue(() => DestCode, value); OnPropertyChanged("DestCode"); } }
        public string ShipmentType { get { return GetValue(() => ShipmentType); } set { SetValue(() => ShipmentType, value); OnPropertyChanged("ShipmentType"); } }
        public string EnteredBy { get { return GetValue(() => EnteredBy); } set { SetValue(() => EnteredBy, value); OnPropertyChanged("EnteredBy"); } }
        public string EnteredDate { get { return GetValue(() => EnteredDate); } set { SetValue(() => EnteredDate, value); OnPropertyChanged("EnteredDate"); } }
        public short Pieces { get { return GetValue(() => Pieces); } set { SetValue(() => Pieces, value); OnPropertyChanged("Pieces"); } }
        public decimal Weight { get { return GetValue(() => Weight); } set { SetValue(() => Weight, value); OnPropertyChanged("Weight"); } }
        public decimal CSAmount { get { return GetValue(() => CSAmount); } set { SetValue(() => CSAmount, value); OnPropertyChanged("CSAmount"); } }
        public string CancelReason { get { return GetValue(() => CancelReason); } set { SetValue(() => CancelReason, value); OnPropertyChanged("CancelReason"); } }

        private ObservableCollection<ReturnItem> _Items = new ObservableCollection<ReturnItem>();
        public ObservableCollection<ReturnItem> Items { get { return _Items; } set { _Items = value; OnPropertyChanged("Items"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Submit":
                        {
                            if (IsValid)
                            {
                                SqlDataReader dr = GetDataReader($"SELECT TOP 1 a.CSalesID, a.Status, a.CancelCSalesID FROM tblCancelCashSale WHERE  a.CSalesID = '{CSalesID}'  ");
                                //var cancelsale = (from a in CSalesEntity.tblCancelCashSales where a.CSalesID == CSalesID select new { a.CSalesID, a.Status, a.CancelCSalesID }).FirstOrDefault();
                                if (!dr.HasRows)
                                {
                                    int i = ExecuteQuery($"INSERT INTO tblCancelCashSale (CSalesID, AWBNum, ShipmentType , DestCode, Pieces , Weight , CSSaleTotal , CancelReason , CUserID, CUserName , CDate, Status) VALUES (  '{CSalesID}', '{AWBNum}', '{ShipmentType}', '{DestCode}', '{Pieces}', '{Weight}', '{CSAmount}', '{CancelReason}', '{LoginUserId}', '{LoginUserName}', '{DateTime.Now}', 1)");
                                    //CSalesEntity.tblCancelCashSales.Add(new tblCancelCashSale { CSalesID = CSalesID, AWBNum = AWBNum, ShipmentType = ShipmentType, DestCode = DestCode, Pieces = Pieces, Weight = Weight, CSSaleTotal = CSAmount, CancelReason = CancelReason, CUserID = LoginUserId, CUserName = LoginUserName, CDate = DateTime.Now, Status = true });
                                    //CSalesEntity.SaveChanges();

                                    CloseWind();
                                    if (Closed != null)
                                    {
                                        Items.Clear();
                                        Items.Add(new ReturnItem { ReturnValues = "OK" });
                                        var Item = new ReturnItem() { ReturnValues = "Inserted" };
                                        Closed(Item);
                                    }
                                }
                                else
                                {
                                    if (dr.HasRows)
                                    {
                                        if (dr.Read())
                                        {
                                            if (Convert.ToBoolean(dr["Status"]) == false)
                                            {
                                                int i = ExecuteQuery($"UPDATE tblCancelCashSale SET CancelReason = '{CancelReason}', CUserID = '{LoginUserId}' , CUserName = '{LoginUserName}', Status = 1 WHERE CancelCSalesID == '{Convert.ToInt64(dr["CancelCSalesID"])}'");
                                                //tblCancelCashSale csupdt = CSalesEntity.tblCancelCashSales.Where(a => a.CancelCSalesID == cancelsale.CancelCSalesID).FirstOrDefault();
                                                //csupdt.CancelReason = CancelReason;
                                                //csupdt.CUserID = LoginUserId;
                                                //csupdt.CUserName = LoginUserName;
                                                //csupdt.Status = true;
                                                //CSalesEntity.SaveChanges();
                                                CloseWind();
                                                if (Closed != null)
                                                {
                                                    Items.Clear();
                                                    Items.Add(new ReturnItem { ReturnValues = "OK" });
                                                    var Item = new ReturnItem() { ReturnValues = "Updated" };
                                                    Closed(Item);
                                                }
                                            }
                                            else
                                            {
                                                CloseWind();
                                                if (Closed != null)
                                                {
                                                    Items.Clear();
                                                    Items.Add(new ReturnItem { ReturnValues = "NOT" });
                                                    var Item = new ReturnItem() { ReturnValues = "NOT" };
                                                    Closed(Item);
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        } break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { MessageBox.Show(Ex.Message); }
        }

        public class ReturnItem : AMGenFunction
        {
            public string ReturnValues { get { return GetValue(() => ReturnValues); } set { SetValue(() => ReturnValues, value); OnPropertyChanged("ReturnValues"); } }
        }


        public void CancelCashSaleShow(string CSID, string awb, string dest, short pcs, decimal wt, string type, decimal amount)
        {
            FrmCancelCashSale ChildWnd = new FrmCancelCashSale();
            CancelCashSaleVM vm = new CancelCashSaleVM(CSID, awb, dest, pcs, wt, type, amount);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowInTaskbar = false;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public event Action<CancelCashSaleVM.ReturnItem> FrmCancelCashSale_Closed;
        public void ChildWindow_Closed(CancelCashSaleVM.ReturnItem RetVal)
        {
            if (FrmCancelCashSale_Closed != null) FrmCancelCashSale_Closed(RetVal);
            ChildWindowManager.Instance.CloseChildWindow();
        }
        
        //Validation Starts
        static readonly string[] ValidatedProperties = { "AWBNum", "DestCode", "Pieces", "Weight", "CancelReason", "EnteredBy" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {

                    if (GetValidationError(property) != null) // there is an error
                        return false;
                }
                return true;
            }
        }        
        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "AWBNum": if (string.IsNullOrEmpty(AWBNum)) error = "Account Number is required"; break;
                case "DestCode": if (string.IsNullOrEmpty(DestCode)) error = "Destination Code is required"; break;
                case "ShipmentType": if (string.IsNullOrEmpty(ShipmentType)) error = "Shipment Type is required"; break;
                case "Weight": if (Weight <= 0) error = "Weight is required"; break;
                case "Pieces": if (Pieces <= 0) error = "Pieces is required"; break;
                case "CSAmount": if (CSAmount <= 0) error = "Cash sales amount is required"; break;
                case "CancelReason": if (string.IsNullOrEmpty(CancelReason)) error = "Cancel Reason is required"; break;
                case "EnteredBy": if (string.IsNullOrEmpty(EnteredBy)) error = "Entered By person is required"; break;
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }
        //Validation Ends
        
    }
}
