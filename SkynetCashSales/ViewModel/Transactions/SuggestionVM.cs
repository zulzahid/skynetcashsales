﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class SuggestionVM : CNPrintVM
    {
        FrmSuggestion Wndow;
        public SuggestionVM(FrmSuggestion frm)
        {
            Wndow = frm;
            LoadGrid();
        }
        //..[Required(ErrorMessage = "IsSuggestion is Required")]
        public bool IsSuggestions
        {
            get { return GetValue(() => IsSuggestions); }
            set { SetValue(() => IsSuggestions, value); OnPropertyChanged("IsSuggestions"); ChangeType(); }
        }
        //..[Required(ErrorMessage = "IsComplaints is Required")]
        public bool IsComplaints
        {
            get { return GetValue(() => IsComplaints); }
            set { SetValue(() => IsComplaints, value); OnPropertyChanged("IsComplaints"); ChangeType(); }
        }
        //..[Required(ErrorMessage = "IsFeedback is Required")]
        public bool IsFeedback
        {
            get { return GetValue(() => IsFeedback); }
            set { SetValue(() => IsFeedback, value); OnPropertyChanged("IsFeedback"); ChangeType(); }
        }        
        //..[Required(ErrorMessage = "FeedbackID is Required")]
        public long FeedbackID
        {
            get { return GetValue(() => FeedbackID); }
            set { SetValue(() => FeedbackID, value); OnPropertyChanged("FeedbackID"); }
        }
        //..[Required(ErrorMessage = "Category is Required")]
        public string Category
        {
            get { return GetValue(() => Category); }
            set { SetValue(() => Category, value); OnPropertyChanged("Category"); }
        }
        //..[Required(ErrorMessage = "IsComplaints is Required")]
        public string Title
        {
            get { return GetValue(() => Title); }
            set { SetValue(() => Title, value); OnPropertyChanged("Title"); }
        }
        //..[Required(ErrorMessage = "IsComplaints is Required")]
        public string Description
        {
            get { return GetValue(() => Description); }
            set { SetValue(() => Description, value); OnPropertyChanged("Description"); }
        }
        //..[Required(ErrorMessage = "TypeValue is Required")]
        public int TypeValue
        {
            get { return GetValue(() => TypeValue); }
            set { SetValue(() => TypeValue, value); OnPropertyChanged("TypeValue"); }
        }
        private DataGridCellInfo _focusedcell;
        public DataGridCellInfo FocusedCell
        {
            get { return _focusedcell; }
            set { _focusedcell = value; OnPropertyChanged("FocusedCell"); }
        }
        private SuggestionListModel _selecteditem = new SuggestionListModel();
        public SuggestionListModel SelectedItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("SelectedItem"); }
        }
        public SuggestionListModel CurrentItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("CurrentItem"); }
        }
        private string _selectedindex;
        public string SelectedIndex
        {
            get { return _selectedindex; }
            set { _selectedindex = value; OnPropertyChanged("SelectedIndex"); }
        }
        private ObservableCollection<SuggestionListModel> _SuggestionsList = new ObservableCollection<SuggestionListModel>();
        public ObservableCollection<SuggestionListModel> SuggestionsList
        {
            get { return _SuggestionsList; }
            set { _SuggestionsList = value; OnPropertyChanged("SuggestionsList"); }
        }
        public ObservableCollection<SuggestionListModel> ItemInfo
        {
            get { return _SuggestionsList; }
            set { _SuggestionsList = value; OnPropertyChanged("ItemInfo"); }
        }
         private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Save": SaveData(); break;
                    case "Preview": PreviewData(); break;
                    case "New": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
        private void ChangeType()
        {
            if (IsSuggestions == true) { Category = "Suggestion"; Wndow.lblType.Content = "Priority"; }
            else if (IsComplaints == true) { Category = "Complaint"; Wndow.lblType.Content = "Priority"; }
            else if (IsFeedback == true) { Category = "Feedback"; Wndow.lblType.Content = "Rating"; }
            else { Category = ""; }
        }
        private void SaveData()
        {
            if (!string.IsNullOrEmpty(Title) && !string.IsNullOrEmpty(Description) && TypeValue >= 0)
            {
                if (FeedbackID <= 0)
                {
                    int i = ExecuteQuery($"INSERT INTO tblFeedBack (Category, Title, Description, Rating, CreatedBy, CreatedDate, Status, IsRead, IsExported, ExportedDate) VALUES ('{Category}', '{Title}', '{Description}', '{TypeValue}', '{LoginUserName}', '{DateTime.Now}', 1, 0, 0, null )");
                    
                    //CSalesEntity.tblFeedbacks.Add(new tblFeedback { Category=Category, Title=Title, Description=Description, Rating=TypeValue, CreatedBy=LoginUserName, CreatedDate=DateTime.Now, Status=true, IsRead=false, IsExported=false, ExportedDate=null });
                    //CSalesEntity.SaveChanges();
                }
                else
                {
                    int i = ExecuteQuery($"UPDATE tblFeedBack SET Category = '{Category}', Title = '{Title}',Description = '{Description}',Rating = '{TypeValue}',CreatedBy = '{LoginUserName}',CreatedDate = '{DateTime.Now}',Status = 1,IsRead = 0,IsExported = 0 WHERE a.FeedBackId = '{FeedbackID}'");
                    
                    //tblFeedback update = CSalesEntity.tblFeedbacks.Where(a => a.FeedbackID == FeedbackID).FirstOrDefault();
                    //update.Category = Category;
                    //update.Title = Title;
                    //update.Description = Description;
                    //update.Rating = TypeValue;
                    //update.CreatedBy = LoginUserName;
                    //update.CreatedDate = DateTime.Now;
                    //update.Status = true;
                    //update.IsRead = false;
                    //update.IsExported = false;
                    //CSalesEntity.SaveChanges();
                }
                LoadGrid();
            }
            else { MessageBox.Show("Kindly select the row", "Warning"); }
        }
        private void PreviewData()
        {
            if (!string.IsNullOrEmpty(SelectedIndex) && SelectedItem != null)
            {
                int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                FeedbackID = SelectedItem.FeedbackID;
                Category = SelectedItem.Category.ToString();
                if (Category == "Suggestion") { IsSuggestions = true; Wndow.lblType.Content = "Priority"; }
                else if (Category == "Complaint") { IsComplaints = true; Wndow.lblType.Content = "Priority"; }
                else if (Category == "Feedback") { IsFeedback = true; Wndow.lblType.Content = "Rating"; }
                else { IsSuggestions = IsComplaints = IsFeedback = false; Wndow.lblType.Content = "Priority"; }
                Title = SelectedItem.Title.ToString();
                Description = SelectedItem.Description.ToString();
                TypeValue = SelectedItem.Rating;
            }
            else {  }
        }
        private void LoadGrid()
        {
            DataTable Dt = new DataTable();

            Dt = GetDataTable("SELECT a.FeedbackID, a.Category, a.Title, a.Description, a.Rating, a.CreatedBy, a.CreatedDate, a.Status, a.IsRead, a.IsExported from tblFeedback a where a.Status = 1 ORDER BY a.CreatedDate DESC ");
            
            //                select new { a.FeedbackID, a.Category, a.Title, a.Description, a.Rating, a.CreatedBy, a.CreatedDate, a.Status, a.IsRead, a.IsExported }).Distinct().ToList().OrderByDescending(a => a.CreatedDate);
            //Dt = LINQToDataTable(announcementlist);

            if (Dt.Rows.Count > 0)
            {
                SuggestionsList.Clear();
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    SuggestionsList.Add(new SuggestionListModel
                    {
                        SlNo = (i + 1).ToString(),
                        FeedbackID = Convert.ToInt64(Dt.Rows[i][0].ToString()),
                        Category = Dt.Rows[i][1].ToString(),
                        Title = Dt.Rows[i][2].ToString(),
                        Description = Dt.Rows[i][3].ToString(),
                        Rating = Convert.ToInt32(Dt.Rows[i][4].ToString())
                    });
                }
            }
        }
        private void ResetData()
        {
            IsSuggestions = true; Wndow.lblType.Content = "Priority";
            FeedbackID = 0;
            TypeValue = 0;
            Title = Description = Category = string.Empty;
            LoadGrid();
        }
        public class SuggestionListModel : AMGenFunction
        {
            private string _SlNo;
            public string SlNo
            {
                get { return _SlNo; }
                set { _SlNo = value; OnPropertyChanged("SlNo"); }
            }
            private long _FeedbackID;
            public long FeedbackID
            {
                get { return _FeedbackID; }
                set { _FeedbackID = value; OnPropertyChanged("FeedbackID"); }
            }
            private string _Category;
            public string Category
            {
                get { return _Category; }
                set { _Category = value; OnPropertyChanged("Category"); }
            }
            private string _Title;
            public string Title
            {
                get { return _Title; }
                set { _Title = value; OnPropertyChanged("Title"); }
            }
            private string _Description;
            public string Description
            {
                get { return _Description; }
                set { _Description = value; OnPropertyChanged("Description"); }
            }
            private int _Rating;
            public int Rating
            {
                get { return _Rating; }
                set { _Rating = value; OnPropertyChanged("Rating"); }
            }
        }
    }
}
