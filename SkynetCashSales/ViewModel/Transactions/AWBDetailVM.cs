﻿using Newtonsoft.Json;
using SkynetCashSales.General;
using SkynetCashSales.Model;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class AWBDetailVM : CNPrintVM
    {
        FrmAWBDetail MyWndow;
        MobileRefCashSalesVM cashSalesVM;
        public event Action<AWBDetailInfo> Closed;
        int tempAWBNo = 0; bool update = false; bool ispostcodefinder = false; bool LoadEdit = false;
        string[] tempAWBDetail;
        string[] tempRefDetail;
        DataTable dtIntlPostCode;
        DataTable dtIntlCountry;

        public AWBDetailVM(FrmAWBDetail frmAWBDetail, MobileRefCashSalesVM cashSalesVM_)
        {
            MyWndow = frmAWBDetail;
            CheckPrinter();
            getTaxDetail();
            ShipmentComboboxload("");
            cashSalesVM = cashSalesVM_;
            tempAWBNo = cashSalesVM.PendingAWBNo();
            ResetData();
        }

        public AWBDetailVM(FrmAWBDetail frmAWBDetail, MobileRefCashSalesVM cashSalesVM_, string [] awbdetail/*, string [] refdetail*/)
        {
            MyWndow = frmAWBDetail;
            CheckPrinter();
            getTaxDetail();
            MyWndow.ResizeMode = ResizeMode.NoResize;
            ShipmentComboboxload("");
            cashSalesVM = cashSalesVM_;
            tempAWBDetail = awbdetail;
            //tempRefDetail = refdetail;
            LoadScreen(awbdetail/*, refdetail*/);
        }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        
        private ObservableCollection<AWBDetailInfo> _AWBDetails = new ObservableCollection<AWBDetailInfo>();
        public ObservableCollection<AWBDetailInfo> AWBDetails { get { return _AWBDetails; } set { _AWBDetails = value; } }

        private ObservableCollection<CSMRShipmentTypeModel> _shipmenttype = new ObservableCollection<CSMRShipmentTypeModel>();
        public ObservableCollection<CSMRShipmentTypeModel> ShipmentType { get { return _shipmenttype; } set { _shipmenttype = value; OnPropertyChanged("ShipmentType"); } }
        private CSMRShipmentTypeModel _selectedshipment = new CSMRShipmentTypeModel();
        public CSMRShipmentTypeModel SelectedShipment { get { return _selectedshipment; } set { if (_selectedshipment != value) { _selectedshipment = value; OnPropertyChanged("SelectedShipment"); /*ShipmentControl();  */Check(); } } }

        public CSMRShipmentTypeModel SelectedValueShipment { get { return _selectedshipment; } set { _selectedshipment = value; OnPropertyChanged("SelectedValueShipment"); Check(); } }
        
        private ObservableCollection<QuotationInfo> _QuotationItems = new ObservableCollection<QuotationInfo>();
        public ObservableCollection<QuotationInfo> QuotationItems { get { return _QuotationItems; } set { _QuotationItems = value; OnPropertyChanged("QuotationItems"); } }

        public string MobileReferenceNo { get { return GetValue(() => MobileReferenceNo); } set { SetValue(() => MobileReferenceNo, value); OnPropertyChanged("MobileReferenceNo"); if (MobileReferenceNo.Length < 6) { IsMobileRef(false); } } }
        public string MobileReferenceNoColor { get { return GetValue(() => MobileReferenceNoColor); } set { SetValue(() => MobileReferenceNoColor, value); OnPropertyChanged("MobileReferenceNoColor"); } }
        public string DestCode
        {
            get { return GetValue(() => DestCode); }
            set
            {
                SetValue(() => DestCode, value); OnPropertyChanged("DestCode");
                if (DestCode.Length >= 3 && rdbLocalityI == true && update == false)
                {
                    DestCodeValidity();
                }
                else if (DestCode.Length < 3)
                {
                    Price = 0;
                }
                if (rdbLocalityM) { AWBPrice(); }
            }
        }

        public string PostCode
        {
            get { return GetValue(() => PostCode); }
            set
            {
                SetValue(() => PostCode, value); OnPropertyChanged("PostCode");
                if (PostCode.Length >= 5)
                {
                    if(!LoadEdit && !ispostcodefinder)
                    PostCodeDestination();
                }
                else
                {
                    Price = 0; DestCode = "";
                }
            }
        }
        public int PostCodeId { get { return GetValue(() => PostCodeId); } set { SetValue(() => PostCodeId, value); OnPropertyChanged("PostCodeId"); } }
        public int Pieces { get { return GetValue(() => Pieces); } set { SetValue(() => Pieces, value); OnPropertyChanged("Pieces"); Check(); } }        
        public Decimal Weight { get { return GetValue(() => Weight); } set { SetValue(() => Weight, value); OnPropertyChanged("Weight"); Check(); } }
        public Decimal VolLength { get { return GetValue(() => VolLength); } set { SetValue(() => VolLength, value); OnPropertyChanged("VolLength"); VolumetricCal(); } }
        public Decimal VolBreadth { get { return GetValue(() => VolBreadth); } set { SetValue(() => VolBreadth, value); OnPropertyChanged("VolBreadth"); VolumetricCal(); } }
        public Decimal VolHeight { get { return GetValue(() => VolHeight); } set { SetValue(() => VolHeight, value); OnPropertyChanged("VolHeight"); VolumetricCal(); } }
        public Decimal VolWeight { get { return GetValue(() => VolWeight); } set { SetValue(() => VolWeight, value); OnPropertyChanged("VolWeight"); 
                if (VolWeight > 0 && rdbLocalityM) 
                { 
                    AWBPrice(); MyWndow.btnsave.IsEnabled = true; 
                } 
                else if (rdbLocalityM)
                {
                    MyWndow.btnsave.IsEnabled = true;
                }
                else  
                { 
                    MyWndow.btnsave.IsEnabled = false; 
                } 
            } 
        }
        public string StrWeight { get { return GetValue(() => StrWeight); } set { SetValue(() => StrWeight, value); OnPropertyChanged("StrWeight"); Weight = Convert.ToDecimal(StrWeight != null && StrWeight.Trim() != "" && StrWeight.Trim() != "." ? StrWeight : "0.00");  } }
        public decimal OtherCharges { get { return GetValue(() => OtherCharges); } set { SetValue(() => OtherCharges, value); OnPropertyChanged("OtherCharges"); Check(); } }
        public decimal InsShipmentValue { get { return GetValue(() => InsShipmentValue); } set { SetValue(() => InsShipmentValue, value); OnPropertyChanged("InsShipmentValue"); InsurancePriceCal(); } }
        public string InsShipmentDesc { get { return GetValue(() => InsShipmentDesc); } set { SetValue(() => InsShipmentDesc, value); OnPropertyChanged("InsShipmentDesc"); } }
        public bool InsuranceCheckState { get { return GetValue(() => InsuranceCheckState); } set { SetValue(() => InsuranceCheckState, value); OnPropertyChanged("InsuranceCheckState"); } }
        public decimal InsurenceCharges { get { return GetValue(() => InsurenceCharges); } set { SetValue(() => InsurenceCharges, value); OnPropertyChanged("InsurenceCharges"); Check(); } }
        public Decimal Price { get { return GetValue(() => Price); } set { SetValue(() => Price, value); OnPropertyChanged("Price");  } }
        public Decimal RASurcharge { get { return GetValue(() => RASurcharge); } set { SetValue(() => RASurcharge, value); OnPropertyChanged("RASurcharge");  } }
        public Decimal TotalPrice { get { return GetValue(() => TotalPrice); } set { SetValue(() => TotalPrice, value); OnPropertyChanged("TotalPrice"); } }
        public string DestUnder { get { return GetValue(() => DestUnder); } set { SetValue(() => DestUnder, value); OnPropertyChanged("DestUnder"); } }
        public decimal InsNormal { get { return GetValue(() => InsNormal); } set { SetValue(() => InsNormal, value); OnPropertyChanged("InsNormal"); } }
        public decimal InsIntl { get { return GetValue(() => InsIntl); } set { SetValue(() => InsIntl, value); OnPropertyChanged("InsIntl"); } }
        public string QuotationId { get { return GetValue(() => QuotationId); } set { SetValue(() => QuotationId, value); OnPropertyChanged("QuotationId"); } }
        public string DestStation { get { return GetValue(() => DestStation); } set { SetValue(() => DestStation, value); OnPropertyChanged("DestStation"); Weight = Convert.ToDecimal(StrWeight != null && StrWeight != "" ? StrWeight : "0.00"); } }
        public string DestZoneDesc { get { return GetValue(() => DestZoneDesc); } set { SetValue(() => DestZoneDesc, value); OnPropertyChanged("DestZoneDesc"); } }
        public string QuotationType { get { return GetValue(() => QuotationType); } set { SetValue(() => QuotationType, value); OnPropertyChanged("QuotationType"); } }
        public string DFirstTitle { get { return GetValue(() => DFirstTitle); } set { SetValue(() => DFirstTitle, value); OnPropertyChanged("DFirstTitle"); } }
        public decimal DFirstWeight { get { return GetValue(() => DFirstWeight); } set { SetValue(() => DFirstWeight, value); OnPropertyChanged("DFirstWeight"); } }
        public decimal DFirstRate { get { return GetValue(() => DFirstRate); } set { SetValue(() => DFirstRate, value); OnPropertyChanged("DFirstRate"); } }
        public string DAddTitle { get { return GetValue(() => DAddTitle); } set { SetValue(() => DAddTitle, value); OnPropertyChanged("DAddTitle"); } }
        public decimal DAddWeight { get { return GetValue(() => DAddWeight); } set { SetValue(() => DAddWeight, value); OnPropertyChanged("DAddWeight"); } }
        public decimal DAddRate { get { return GetValue(() => DAddRate); } set { SetValue(() => DAddRate, value); OnPropertyChanged("DAddRate"); } }
        public decimal DSurcharge { get { return GetValue(() => DSurcharge); } set { SetValue(() => DSurcharge, value); OnPropertyChanged("DSurcharge"); } }
        public decimal IntlSurCharge { get { return GetValue(() => IntlSurCharge); } set { SetValue(() => IntlSurCharge, value); OnPropertyChanged("IntlSurCharge"); } }
        public string TaxName { get { return GetValue(() => TaxName); } set { SetValue(() => TaxName, value); OnPropertyChanged("TaxName"); } }
        public decimal TaxPercentage { get { return GetValue(() => TaxPercentage); } set { SetValue(() => TaxPercentage, value); OnPropertyChanged("TaxPercentage"); } }

        public string MFirstTitle { get { return GetValue(() => MFirstTitle); } set { SetValue(() => MFirstTitle, value); OnPropertyChanged("MFirstTitle"); } }
        public decimal MFirstWeight { get { return GetValue(() => MFirstWeight); } set { SetValue(() => MFirstWeight, value); OnPropertyChanged("MFirstWeight"); } }
        public decimal MFirstRate { get { return GetValue(() => MFirstRate); } set { SetValue(() => MFirstRate, value); OnPropertyChanged("MFirstRate"); } }
        public string MAddTitle { get { return GetValue(() => MAddTitle); } set { SetValue(() => MAddTitle, value); OnPropertyChanged("MAddTitle"); } }
        public decimal MAddWeight { get { return GetValue(() => MAddWeight); } set { SetValue(() => MAddWeight, value); OnPropertyChanged("MAddWeight"); } }
        public decimal MAddRate { get { return GetValue(() => MAddRate); } set { SetValue(() => MAddRate, value); OnPropertyChanged("MAddRate"); } }
        public decimal MSurcharge { get { return GetValue(() => MSurcharge); } set { SetValue(() => MSurcharge, value); OnPropertyChanged("MSurcharge"); } }

        public string BFirstTitle { get { return GetValue(() => BFirstTitle); } set { SetValue(() => BFirstTitle, value); OnPropertyChanged("BFirstTitle"); } }
        public decimal BFirstWeight { get { return GetValue(() => BFirstWeight); } set { SetValue(() => BFirstWeight, value); OnPropertyChanged("BFirstWeight"); } }
        public decimal BFirstRate { get { return GetValue(() => BFirstRate); } set { SetValue(() => BFirstRate, value); OnPropertyChanged("BFirstRate"); } }
        public string BAddTitle { get { return GetValue(() => BAddTitle); } set { SetValue(() => BAddTitle, value); OnPropertyChanged("BAddTitle"); } }
        public decimal BAddWeight { get { return GetValue(() => BAddWeight); } set { SetValue(() => BAddWeight, value); OnPropertyChanged("BAddWeight"); } }
        public decimal BAddRate { get { return GetValue(() => BAddRate); } set { SetValue(() => BAddRate, value); OnPropertyChanged("BAddRate"); } }
        public decimal BSurcharge { get { return GetValue(() => BSurcharge); } set { SetValue(() => BSurcharge, value); OnPropertyChanged("BSurcharge"); } }

        public string PFirstTitle { get { return GetValue(() => PFirstTitle); } set { SetValue(() => PFirstTitle, value); OnPropertyChanged("PFirstTitle"); } }
        public decimal PFirstWeight { get { return GetValue(() => PFirstWeight); } set { SetValue(() => PFirstWeight, value); OnPropertyChanged("PFirstWeight"); } }
        public decimal PFirstRate { get { return GetValue(() => PFirstRate); } set { SetValue(() => PFirstRate, value); OnPropertyChanged("PFirstRate"); } }
        public string PAddTitle { get { return GetValue(() => PAddTitle); } set { SetValue(() => PAddTitle, value); OnPropertyChanged("PAddTitle"); } }
        public decimal PAddWeight { get { return GetValue(() => PAddWeight); } set { SetValue(() => PAddWeight, value); OnPropertyChanged("PAddWeight"); } }
        public decimal PAddRate { get { return GetValue(() => PAddRate); } set { SetValue(() => PAddRate, value); OnPropertyChanged("PAddRate"); } }
        public decimal PSurcharge { get { return GetValue(() => PSurcharge); } set { SetValue(() => PSurcharge, value); OnPropertyChanged("PSurcharge"); } }

        public decimal FinalSurcharge { get { return GetValue(() => FinalSurcharge); } set { SetValue(() => FinalSurcharge, value); OnPropertyChanged("FinalSurcharge"); } }
        public string FreqDocument { get { return GetValue(() => FreqDocument); } set { SetValue(() => FreqDocument, value); OnPropertyChanged("FreqDocument"); } }
        public string FreqParcel { get { return GetValue(() => FreqParcel); } set { SetValue(() => FreqParcel, value); OnPropertyChanged("FreqParcel"); } }
        public Decimal TotalAmtAfterTax { get { return GetValue(() => TotalAmtAfterTax); } set { SetValue(() => TotalAmtAfterTax, value); OnPropertyChanged("TotalAmtAfterTax"); } }
        public Decimal TotalAmt { get { return GetValue(() => TotalAmt); } set { SetValue(() => TotalAmt, value); OnPropertyChanged("TotalAmt"); } }        
        public string PaymentMode { get { return GetValue(() => PaymentMode); } set { SetValue(() => PaymentMode, value); OnPropertyChanged("PaymentMode"); } }
        public string PaymentReference { get { return GetValue(() => PaymentReference); } set { SetValue(() => PaymentReference, value); OnPropertyChanged("PaymentReference"); } }
        public string NewCN { get { return GetValue(() => NewCN); } set { SetValue(() => NewCN, value); OnPropertyChanged("NewCN"); } }
        public decimal TotalAmount { get { return GetValue(() => TotalAmount); } set { SetValue(() => TotalAmount, value); OnPropertyChanged("TotalAmount"); } }
        public string CSalesNo { get { return GetValue(() => CSalesNo); } set { SetValue(() => CSalesNo, value); OnPropertyChanged("CSalesNo"); } }
        public bool YesInsurance { get { return GetValue(() => YesInsurance); } set { SetValue(() => YesInsurance, value); OnPropertyChanged("YesInsurance"); InsShipmentDesc = ""; InsShipmentValue = 0; MyWndow.txtInsShpmntValue.IsEnabled = MyWndow.txtInsShpmntDesc.IsEnabled = YesInsurance; } }

        //temp for dest code detail        
        public string tempZoneDesc { get { return GetValue(() => tempZoneDesc); } set { SetValue(() => tempZoneDesc, value); OnPropertyChanged("tempZoneDesc"); } }
        public decimal tempFirstWeight { get { return GetValue(() => tempFirstWeight); } set { SetValue(() => tempFirstWeight, value); OnPropertyChanged("tempFirstWeight"); } }
        public string tempShipmentType { get { return GetValue(() => tempShipmentType); } set { SetValue(() => tempShipmentType, value); OnPropertyChanged("tempShipmentType"); } }
        public decimal tempFirstRate { get { return GetValue(() => tempFirstRate); } set { SetValue(() => tempFirstRate, value); OnPropertyChanged("tempFirstRate"); } }
        public decimal tempAddWeight { get { return GetValue(() => tempAddWeight); } set { SetValue(() => tempAddWeight, value); OnPropertyChanged("tempAddWeight"); } }
        public decimal tempAddRate { get { return GetValue(() => tempAddRate); } set { SetValue(() => tempAddRate, value); OnPropertyChanged("tempAddRate"); } }

        //For Reference Detail
        public string ReferenceNum { get { return GetValue(() => ReferenceNum); } set { SetValue(() => ReferenceNum, value); OnPropertyChanged("ReferenceNum"); } }
        public string RShipperName { get { return GetValue(() => RShipperName); } set { SetValue(() => RShipperName, value); OnPropertyChanged("RShipperName"); } }
        public string RShipperAddress { get { return GetValue(() => RShipperAddress); } set { SetValue(() => RShipperAddress, value); OnPropertyChanged("RShipperAddress"); } }
        public string RSenderZipcode { get { return GetValue(() => RSenderZipcode); } set { SetValue(() => RSenderZipcode, value); OnPropertyChanged("RSenderZipcode"); } }
        public string RShipperTelNo { get { return GetValue(() => RShipperTelNo); } set { SetValue(() => RShipperTelNo, value); OnPropertyChanged("RShipperTelNo"); } }
        public string RCreatedDate { get { return GetValue(() => RCreatedDate); } set { SetValue(() => RCreatedDate, value); OnPropertyChanged("RCreatedDate"); } }
        public string RExpiredDate { get { return GetValue(() => RExpiredDate); } set { SetValue(() => RExpiredDate, value); OnPropertyChanged("RExpiredDate"); } }
        public string RSendTo { get { return GetValue(() => RSendTo); } set { SetValue(() => RSendTo, value); OnPropertyChanged("RSendTo"); } }
        public string RCompany { get { return GetValue(() => RCompany); } set { SetValue(() => RCompany, value); OnPropertyChanged("RCompany"); } }
        public string RReceiverAddress { get { return GetValue(() => RReceiverAddress); } set { SetValue(() => RReceiverAddress, value); OnPropertyChanged("RReceiverAddress"); } }
        public string RZipcode { get { return GetValue(() => RZipcode); } set { SetValue(() => RZipcode, value); OnPropertyChanged("RZipcode"); } }
        public string RPhone { get { return GetValue(() => RPhone); } set { SetValue(() => RPhone, value); OnPropertyChanged("RPhone"); } }
        public bool rdbLocalityM { get { return GetValue(() => rdbLocalityM); } set { SetValue(() => rdbLocalityM, value); OnPropertyChanged("rdbLocalityM"); Locality(); } }
        public bool rdbLocalityI { get { return GetValue(() => rdbLocalityI); } set { SetValue(() => rdbLocalityI, value); OnPropertyChanged("rdbLocalityI"); Locality(); } }
        public string lblDestCodePost { get { return GetValue(() => lblDestCodePost); } set { SetValue(() => lblDestCodePost, value); OnPropertyChanged("lblDestCodePost"); } }       
        public Decimal TotAmtBeforeTax { get { return GetValue(() => TotAmtBeforeTax); } set { SetValue(() => TotAmtBeforeTax, value); OnPropertyChanged("TotAmtBeforeTax"); } }
        public decimal TaxAmt { get { return GetValue(() => TaxAmt); } set { SetValue(() => TaxAmt, value); OnPropertyChanged("TaxAmt"); } }
        public string Country { get { return GetValue(() => Country); } set { SetValue(() => Country, value); OnPropertyChanged("Country"); } }
        public string PostCodeIntl { get { return GetValue(() => PostCodeIntl); } set { SetValue(() => PostCodeIntl, value); OnPropertyChanged("PostCodeIntl"); Check(); } }
        //public string IntlCity { get { return GetValue(() => IntlCity); } set { SetValue(() => IntlCity, value); OnPropertyChanged("IntlCity"); Check(); } }
        public Decimal tempPrice { get { return GetValue(() => tempPrice); } set { SetValue(() => tempPrice, value); OnPropertyChanged("tempPrice"); } }

        //Warna button
        public System.Windows.Media.LinearGradientBrush ColorAWB { get { return GetValue(() => ColorAWB); } set { SetValue(() => ColorAWB, value); OnPropertyChanged("ColorAWB"); } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "DestCode":
                        {
                            LoadDestCode();
                        }
                        break;
                    case "PostCode Finder":
                        {
                            if (rdbLocalityM == false) { rdbLocalityM = true; }
                            LoadDestCode();
                        }
                        break;
                    case "Check Reference": CheckReference(); break;
                    case "F10Weight": WeightfromMachine(); break;
                    case "AWBPrice": AWBPrice(); break;
                    case "Save": 
                        {
                            if ((rdbLocalityI && (StrWeight != "0" || VolWeight != 0) && StrWeight != "" && Price > 0) ||
                                (rdbLocalityM && DestCode != "" && (StrWeight != "0" || VolWeight != 0) && StrWeight != "" && Price > 0))
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to Save?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes)
                                {
                                    try
                                    {
                                        SaveData();
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }
                            }
                        } 
                        break;
                    case "Close": {
                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to Close this screen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (MsgConfirmation == MessageBoxResult.Yes)
                            {
                                CleartempDetail();
                                if (!update)
                                {
                                    cashSalesVM.AWBNo = cashSalesVM.AWBNo - 1;
                                }
                                this.CloseWind();
                            }
                        }; 
                        break;
                    case "New":
                        {
                            ResetData();
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }        

        private void LoadScreen(string[] arrAWBDetail)
        {
            //0-tempAWBNo, 1-SelectedShipment.ShipmentTypeName, 2-DestCode, 3-Pieces, 4-Weight, 5-VolLength, 6-VolBreadth, 7-VolHeight, 8-VolWeight, 9-tempAddRate, 10-Price, 11-OtherCharges, 12-YesInsurance, 13-InsShipmentValue, 14-InsShipmentDesc, 15-InsurenceCharges, 16-TotalAmount, 17-TaxPercentage, 18-MobileReferenceNo, 19-Locality, 20 - PostCode, 21 - DestUnder
            update = true; LoadEdit = true;
            if (arrAWBDetail[19] == "Malaysia") { rdbLocalityM = true; }
            else
            {
                Country = arrAWBDetail[24].ToString();
                PostCodeIntl = /*IntlCity =*/ arrAWBDetail[25].ToString();
                rdbLocalityI = true;
                if (rdbLocalityI)
                {
                    MyWndow.rdbLocalityI.Content = "INTL (" + Country + ")";
                }
                else
                {
                    MyWndow.rdbLocalityI.Content = "INTL";
                }
            }
            MobileReferenceNo = arrAWBDetail[18].ToString(); ExecuteCommandGen("Check Reference");
            tempAWBNo = Convert.ToInt32(arrAWBDetail[0]);

            SelectedShipment.ShipmentTypeName = arrAWBDetail[1];
            if (arrAWBDetail[19] == "Malaysia")
            {
                if (SelectedShipment.ShipmentTypeName == "BICYCLES") { SelectedShipmentIndex = 0; }
                else if (SelectedShipment.ShipmentTypeName == "DOCUMENTS") { SelectedShipmentIndex = 1; }
                else if (SelectedShipment.ShipmentTypeName == "MOTOR BIKES") { SelectedShipmentIndex = 2; }
                else if (SelectedShipment.ShipmentTypeName == "PARCELS") { SelectedShipmentIndex = 3; }
            }
            else
            {
                if (SelectedShipment.ShipmentTypeName == "DOCUMENTS") { SelectedShipmentIndex = 0; }
                else if (SelectedShipment.ShipmentTypeName == "PARCELS") { SelectedShipmentIndex = 1; }
            }

            PostCode = arrAWBDetail[20].ToString();
            DestUnder = arrAWBDetail[21].ToString();
            PostCodeId = Convert.ToInt32(arrAWBDetail[22].ToString());

            if (PostCode == "")
            {
                MyWndow.txtPostCode.Visibility = Visibility.Hidden;
                MyWndow.txtDestCode.Visibility = Visibility.Visible;
            }
            else
            {
                MyWndow.txtPostCode.Visibility = Visibility.Visible;
                MyWndow.txtDestCode.Visibility = Visibility.Hidden;
                PostCode = arrAWBDetail[20].ToString();
            }

            DestCode = arrAWBDetail[2].ToString(); //DestinationDetails();
            Pieces = Convert.ToInt32(arrAWBDetail[3]);
            StrWeight = arrAWBDetail[4].ToString();
            VolLength = Convert.ToDecimal(arrAWBDetail[5].ToString());
            VolBreadth = Convert.ToDecimal(arrAWBDetail[6].ToString());
            VolHeight = Convert.ToDecimal(arrAWBDetail[7].ToString());
            VolWeight = Convert.ToDecimal(arrAWBDetail[8]);
            tempAddRate = Convert.ToDecimal(arrAWBDetail[9]);
            Price = Convert.ToDecimal(arrAWBDetail[10]);
            OtherCharges = Convert.ToDecimal(arrAWBDetail[11]);
            YesInsurance = Convert.ToBoolean(arrAWBDetail[12]);
            InsShipmentValue = Convert.ToDecimal(arrAWBDetail[13]);
            InsShipmentDesc = arrAWBDetail[14];
            InsurenceCharges = Convert.ToDecimal(arrAWBDetail[15]);
            TotalAmount = TotalPrice = Convert.ToDecimal(arrAWBDetail[16]) + Convert.ToDecimal(arrAWBDetail[26]);
            TaxPercentage = Convert.ToDecimal(arrAWBDetail[17]);
            RASurcharge = Convert.ToDecimal(arrAWBDetail[26]);
            lblDestCodePost = "Post Code *";

            //Locality = arrAWBDetail[19].ToString(); 

            //tempListAWBDetail.Add(tempAWBNo + "|" + SelectedShipment.ShipmentTypeName + "|" + DestCode + "|" + Pieces + "|" + Weight + "|" + VolLength + "|" + VolBreadth + "|" + VolHeight + "|" + VolWeight + "|" + tempAddRate + "|" + Price + "|" + OtherCharges + "|" + YesInsurance + "|" + InsShipmentValue + "|" + InsShipmentDesc + "|" + InsurenceCharges + "|" + TotalAmount + "|" + TaxPercentage + "|" + MobileReferenceNo + "|" + Locality + "|" + PostCode + "|" + DestUnder + "|" + PostCodeId);         
            MyWndow.rdbLocalityM.IsEnabled = MyWndow.rdbLocalityI.IsEnabled /*= MyWndow.txtPostCode.IsEnabled = MyWndow.txtMobileReferenceNo.IsEnabled = MyWndow.btnCheckRef.IsEnabled = MyWndow.btnDestCode.IsEnabled*/ = false; MyWndow.btnsave.Content = "Update";
            MyWndow.btnNew.Content = "Reset";
            //MyWndow.txtRCreatedDate.IsEnabled = MyWndow.txtRExpiredDate.IsEnabled = false;

            //Load Reference detail if exist
            //if (MobileReferenceNoColor == "Black")
            //{
            //    RShipperName = arrRefDetail[1]; RShipperAddress1 = arrRefDetail[2]; RShipperAddress2 = arrRefDetail[3]; RShipperAddress3 = arrRefDetail[4]; RSenderZipcode = arrRefDetail[5]; RShipperTelNo = arrRefDetail[6]; RSendTo = arrRefDetail[7]; RCompany = arrRefDetail[8]; RReceiverAddress1 = arrRefDetail[9]; RReceiverAddress2 = arrRefDetail[10]; RReceiverAddress3 = arrRefDetail[11]; RZipcode = arrRefDetail[12]; RPhone = arrRefDetail[13];
            //}
            LoadEdit = false;
        }

        private static string errormsg = "";
        static readonly string[] ValidatedProperties = { "DestCode", "Weight", "Pieces", "Price", "TotalAmount" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "DestCode": if (string.IsNullOrEmpty(DestCode)) { error = "DestCode is required"; MyWndow.txtDestCode.Focus(); } break;
                case "Weight": if (Weight <= 0) { error = "Weight is required"; MyWndow.txtWeight.Focus(); } break;
                case "Pieces": if (Pieces <= 0) { error = "Pieces is required"; MyWndow.txtPieces.Focus(); } break;
                //case "Price": if (Price <= 0) { error = "Price of shipment is required"; } break;
                case "TotalAmount": if (TotalAmtAfterTax <= 0) { error = "Cash sales Total can't be less than Zero"; } break;
                case "PaymentReference": if (PaymentMode.ToUpper() != "CASH" && string.IsNullOrEmpty(PaymentReference)) { error = "Payment Reference cannot be empty"; /*Wndow.txtPaymentReference.Focus();*/ } break;
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        public void ShipmentComboboxload(string type)
        {
            try
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GDDesc FROM tblGeneralDet A" +
                        $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                        $" WHERE RTRIM(B.GenDesc) = 'SHIPMENT TYPE' ORDER BY A.GDDesc");

                    if (dt.Rows.Count > 0)
                    {
                        ShipmentType.Clear();
                        for (int i = 0; i < dt.Rows.Count; ++i)
                        {
                            string Shipment = dt.Rows[i]["GDDesc"].ToString() + "S";

                            if (type == "Intl")
                            {
                                if(Shipment.ToUpper() == "DOCUMENTS" || Shipment.ToUpper() == "PARCELS")
                                {
                                    ShipmentType.Add(new CSMRShipmentTypeModel { ShipmentTypeId = Shipment, ShipmentTypeName = Shipment });
                                }
                            }
                            else
                            {
                                ShipmentType.Add(new CSMRShipmentTypeModel { ShipmentTypeId = Shipment, ShipmentTypeName = Shipment });
                            }
                        }
                        if(type == "Intl")
                        {
                            SelectedShipmentIndex = dt.Rows.Count - 3;
                        }
                        else
                        {
                            SelectedShipmentIndex = dt.Rows.Count - 1;
                        }
                        
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Shipment Type Combobox load function:" + Ex.ToString()); }
        }

        private void ShipmentControl()
        {
            if(SelectedShipment.ShipmentTypeName.Trim().ToUpper() == "BICYCLES" || SelectedShipment.ShipmentTypeName.Trim().ToUpper() == "MOTOR BIKES")
            {
                MyWndow.txtPieces.IsEnabled = false; MyWndow.txtWeight.IsEnabled = false;
            }
            else
            {
                MyWndow.txtPieces.IsEnabled = true; MyWndow.txtWeight.IsEnabled = true;
            }
        }

        //Weighing Machine Start
        public string weight;
        public void WeightfromMachine()
        {
            try
            {
                GetWeightUtility obj = new GetWeightUtility();
                string stwt = obj.GetWeight();
                StrWeight = Convert.ToDecimal(!string.IsNullOrEmpty(stwt) ? stwt : "1").ToString();
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private decimal TotWeight = 0;
        DataTable dtQuotationItems;        
        private void AWBPrice()
        {
            if (rdbLocalityM == true)
            {
                decimal CalcWeight = 0, FirstWeight = 0, FirstRate = 0, AddWeight = 0, AddRate = 0, Surcharge = 0;
                TotWeight = 0; Price = 0;
                string ToZone = "";
                QuotationItems.Clear();

                try
                {
                    if (Weight > 0 || VolWeight > 0)
                    {
                        TotWeight = Weight > VolWeight ? Weight : VolWeight;
                        if (SelectedShipment.ShipmentTypeName != null)
                        {
                            if (rdbLocalityM == true && SelectedShipment.ShipmentTypeName.Trim().ToUpper() == "PARCELS" && TotWeight > 1)
                            {
                                TotWeight = Math.Ceiling(TotWeight / 1);
                            }
                        }

                        if (SelectedShipment.ShipmentTypeName != null && SelectedShipment.ShipmentTypeName.Trim() != "" && DestCode.Trim() != "" && DestCode.Trim() != null && TotWeight > 0)
                        {
                            dtQuotationItems = new DataTable();
                            dtQuotationItems = GetDataTable($"SELECT B.QuotationType, A.FromZone, A.ToZone, A.ShipmentType, A.FirstWeight, A.FirstRate, A.AddWeight, A.AddRate, A.Surcharge FROM tblQuotationDet A" +
                                $" JOIN tblQuotation B ON A.QuotationId = B.QuotationId" +
                                $" WHERE A.ToDate IS NULL AND UPPER(RTRIM(A.ShipmentType)) = '{SelectedShipment.ShipmentTypeName.Trim().ToUpper()}' AND A.FirstWeight <= {TotWeight}" +
                                $" AND UPPER(RTRIM(A.ToZone)) IN (SELECT DISTINCT UPPER(RTRIM(ZoneCode)) FROM tblZoneDetail WHERE UPPER(RTRIM(DestCode)) = '{DestCode.Trim().ToUpper()}')" +
                                $" ORDER BY A.FirstWeight DESC, A.FirstRate DESC");
                            if (dtQuotationItems.Rows.Count == 0)
                            {
                                dtQuotationItems = new DataTable();
                                dtQuotationItems = GetDataTable($"SELECT B.QuotationType, A.FromZone, A.ToZone, A.ShipmentType, A.FirstWeight, A.FirstRate, A.AddWeight, A.AddRate, A.Surcharge FROM tblQuotationDet A" +
                                    $" JOIN tblQuotation B ON A.QuotationId = B.QuotationId" +
                                    $" WHERE A.ToDate IS NULL AND UPPER(RTRIM(A.ShipmentType)) = '{SelectedShipment.ShipmentTypeName.Trim().ToUpper()}' AND A.FirstWeight >= {TotWeight}" +
                                    $" AND UPPER(RTRIM(A.ToZone)) IN (SELECT DISTINCT UPPER(RTRIM(ZoneCode)) FROM tblZoneDetail WHERE UPPER(RTRIM(DestCode)) = '{DestCode.Trim().ToUpper()}')" +
                                    $" ORDER BY A.FirstWeight, A.FirstRate DESC");
                            }

                            if (dtQuotationItems.Rows.Count > 0)
                            {
                                DestUnder = ToZone = dtQuotationItems.Rows[0]["ToZone"].ToString();
                                FirstWeight = Convert.ToDecimal(dtQuotationItems.Rows[0]["FirstWeight"]);
                                FirstRate = Convert.ToDecimal(dtQuotationItems.Rows[0]["FirstRate"]);
                                AddWeight = Convert.ToDecimal(dtQuotationItems.Rows[0]["AddWeight"]);
                                AddRate = Convert.ToDecimal(dtQuotationItems.Rows[0]["AddRate"]);
                                Surcharge = Convert.ToDecimal(dtQuotationItems.Rows[0]["Surcharge"]);
                            }

                            DataTable newdt;
                            if (ToZone.Trim() != "")
                            {
                                newdt = new DataTable();
                                newdt = GetDataTable($"SELECT b.ZoneDesc as ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate FROM tblQuotationDet a" +
                                    $" LEFT JOIN tblZone b ON UPPER(RTRIM(a.ToZone)) = UPPER(RTRIM(b.ZoneCode))" +
                                    $" WHERE UPPER(RTRIM(ToZone)) = '{ToZone.ToUpper().Trim()}' AND ToDate IS NULL ORDER BY ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate");


                                tempListDestDetail.Clear();
                                for (int i = 0; i < dtQuotationItems.Rows.Count; i++)
                                {
                                    tempZoneDesc = dtQuotationItems.Rows[i]["ToZone"].ToString();
                                    tempShipmentType = dtQuotationItems.Rows[i]["ShipmentType"].ToString();
                                    tempFirstWeight = Convert.ToDecimal(dtQuotationItems.Rows[i]["FirstWeight"]);
                                    tempFirstRate = Convert.ToDecimal(dtQuotationItems.Rows[i]["FirstRate"]);
                                    tempAddWeight = Convert.ToDecimal(dtQuotationItems.Rows[i]["AddWeight"]);
                                    tempAddRate = Convert.ToDecimal(dtQuotationItems.Rows[i]["AddRate"]);

                                    tempListDestDetail.Add(tempAWBNo + "|" + tempZoneDesc + "|" + tempShipmentType + "|" + tempFirstWeight + "|" + tempFirstRate + "|" + tempAddWeight + "|" + tempAddRate);

                                    QuotationItems.Add(new QuotationInfo { ZoneDesc = tempZoneDesc, ShipmentType = tempShipmentType, FirstWeight = tempFirstWeight, ForstRate = tempFirstRate, AddWeight = tempAddWeight, AddRate = tempAddRate });

                                }

                                CalcWeight = TotWeight;
                                if (CalcWeight <= FirstWeight)
                                {
                                    Price = FirstRate;
                                }
                                else
                                {
                                    Price = FirstRate;
                                    CalcWeight = CalcWeight - FirstWeight;
                                    if (CalcWeight > 0 && AddWeight > 0)
                                    {
                                        CalcWeight = Math.Ceiling(CalcWeight / AddWeight);
                                        Price = Price + (CalcWeight * AddRate);
                                    }
                                }
                            }
                        }

                        Price = GetRoundingVal(Price, 2);
                        TotalPrice = Price + InsurenceCharges + OtherCharges;
                        NetSummary();
                    }
                    else
                    {
                        Price = 0;
                        TotalPrice = Price + InsurenceCharges + OtherCharges;
                        NetSummary();
                    }
                }
                catch (Exception Ex) { error_log.errorlog("AWBPrice() : " + Ex.ToString()); }
            }
            else if (rdbLocalityI == true)
            {
                if (dtIntlPostCode != null && dtIntlPostCode.Rows.Count > 0)
                {
                    if (Weight > 0 || VolWeight > 0)
                    {
                        decimal highestweight = Weight > VolWeight ? Weight : VolWeight;
                        string CheckPostCode = PostCodeIntl == "" /*&& (IntlCity == "" || IntlCity == null)*/ ? null : PostCodeIntl;
                        GetIntlPrice objPrice = new GetIntlPrice();
                        var IntlPrice = objPrice.IntlPrice(Country, CheckPostCode, SelectedShipment.ShipmentTypeName.Replace("S", ""), highestweight);

                        Price = tempPrice = IntlPrice.Item1/* + IntlPrice.Item2*/;
                        //tempttlPrice = IntlPrice.Item4;
                        RASurcharge = IntlPrice.Item3;
                        TotalPrice = Price + OtherCharges + InsurenceCharges + RASurcharge;
                    }
                    else
                    {
                        Price = tempPrice;
                        TotalPrice = Price + OtherCharges + InsurenceCharges;
                    }

                    NetSummary();
                    if (Price > 0)
                        MyWndow.btnsave.IsEnabled = true;
                }
                else//For load data from main screen
                {
                    if (Weight > 0 || VolWeight > 0)
                    {
                        decimal highestweight = Weight > VolWeight ? Weight : VolWeight;
                        GetIntlPrice objPrice = new GetIntlPrice();
                        string CheckPostCode = PostCodeIntl == "" ? null : PostCodeIntl;
                        var IntlPrice = objPrice.IntlPrice(Country, CheckPostCode, SelectedShipment.ShipmentTypeName.Replace("S", ""), highestweight);

                        Price = tempPrice = IntlPrice.Item1/* + IntlPrice.Item2*/;
                        //tempttlPrice = IntlPrice.Item4;
                        RASurcharge = IntlPrice.Item3;
                        TotalPrice = Price + OtherCharges + InsurenceCharges + RASurcharge;
                    }
                    else
                    {
                        Price = tempPrice;
                        TotalPrice = Price + OtherCharges + InsurenceCharges;
                    }                    

                    if (Price > 0) { MyWndow.btnsave.IsEnabled = true; }

                    NetSummary();
                }
            }

        }

        private void VolumetricCal()
        {
            try
            {
                if (VolLength == 0 || VolBreadth == 0 || VolHeight == 0)
                {
                    VolWeight = 0;
                }
                else
                {
                    VolWeight = GetRoundingVal((VolLength * VolBreadth * VolHeight) / 5000, 2);
                }
                
                OnPropertyChanged("VolWeight");
            }
            catch (Exception Ex) { error_log.errorlog("Volumetric Weight Cal Function : " + Ex.ToString()); }
        }

        private void InsurancePriceCal()
        {
            try
            {
                InsurenceCharges = 0;
                if (YesInsurance == true && Price > 0)
                {
                    InsNormal = 0; InsIntl = 0;
                    SqlDataReader dr = GetDataReader($"SELECT InsuranceNormal, InsuranceInternational FROM tblQuotation WHERE upper(QuotationType) = '{QuotationType.Trim().ToUpper()}' AND ToDate IS NULL");
                    if (dr.Read())
                    {
                        InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                        InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                    }
                    dr.Close();

                    if (DestUnder.Trim() == "ZONE 1" || DestUnder.Trim() == "ZONE 2") 
                    { 
                        InsurenceCharges = ((InsShipmentValue * InsNormal) / 100); 
                    }
                    else 
                    { 
                        InsurenceCharges = ((InsShipmentValue * InsIntl) / 100); 
                    }

                    InsurenceCharges = InsurenceCharges > 0 && InsurenceCharges < 5 ? 5 + Convert.ToDecimal(".00") : InsurenceCharges;
                }

                InsurenceCharges = GetRoundingVal(InsurenceCharges, 2);
                NetSummary();
            }
            catch (Exception Ex) { error_log.errorlog("Insurance Price Cal Function : " + Ex.ToString()); }
        }

        public List<string> tempListAWBDetail = new List<string>();
        public List<string> tempListRefDetail = new List<string>();
        public List<string> tempListDestDetail = new List<string>();
        private void SaveData()
        {
            var ChildWnd = new ListHelpGen();

            if (Price > 0)
            {
                string tempMobileRef = MobileReferenceNoColor == "Black" ? "" : MobileReferenceNo;
                if (YesInsurance == false) { InsShipmentValue = 0; InsShipmentDesc = ""; InsurenceCharges = 0; }
                string Locality = rdbLocalityM == true ? "Malaysia" : "INTL";

                //if (update)
                //{
                //    string AWBtoRemove = tempAWBDetail[0] + "|" + tempAWBDetail[1] + "|" + tempAWBDetail[2] + "|" + tempAWBDetail[3] + "|" + tempAWBDetail[4] + "|" + tempAWBDetail[5] + "|" + tempAWBDetail[6] + "|" + tempAWBDetail[7] + "|" + tempAWBDetail[8] + "|" + tempAWBDetail[9] + "|" + tempAWBDetail[10] + "|" + tempAWBDetail[11] + "|" + tempAWBDetail[12] + "|" + tempAWBDetail[13] + "|" + tempAWBDetail[14] + "|" + tempAWBDetail[15] + "|" + tempAWBDetail[16] + "|" + tempAWBDetail[17] + "|" + tempAWBDetail[18] + "|" + tempAWBDetail[19] + "|" + tempAWBDetail[20] + "|" + tempAWBDetail[21] + "|" + tempAWBDetail[22] + "|" + tempAWBDetail[23];

                //    cashSalesVM.listAWBDetail.Remove(AWBtoRemove);                                    
                //}

                //AWB Details
                //tempListAWBDetail.Add(tempAWBNo + "|" + SelectedShipment.ShipmentTypeName + "|" + DestCode + "|" + Pieces + "|" + Weight + "|" + VolLength + "|" + VolBreadth + "|" + VolHeight + "|" + VolWeight + "|" + tempAddRate + "|" + Price + "|" + OtherCharges + "|" + YesInsurance + "|" + InsShipmentValue + "|" + InsShipmentDesc + "|" + InsurenceCharges + "|" + TotalAmount + "|" + TaxPercentage + "|" + tempMobileRef + "|" + Locality + "|" + PostCode + "|" + DestUnder + "|" + PostCodeId + "|" + QuotationType);


                //string[] arrAWBDetail = tempListAWBDetail.ToArray();
                //cashSalesVM.listAWBDetail.Add(arrAWBDetail[0]);

                var AWBList = new AWBDetailInfo()
                {
                    AWBNo = tempAWBNo,
                    ShipmentType = SelectedShipment.ShipmentTypeName,
                    DestCode = DestCode,
                    Pieces = Pieces,
                    Weight = Weight,
                    VolLength = VolLength,
                    VolWidth = VolBreadth,
                    VolHeight = VolHeight,
                    VolWeight = VolWeight,
                    AddRate = tempAddRate,
                    Price = Price,
                    OtherCharges = OtherCharges,
                    InsCheck = YesInsurance,
                    InsShipmentValue = InsShipmentValue,
                    InsShipmentDesc = InsShipmentDesc,
                    InsCharges = InsurenceCharges,
                    TotalAmount = TotalAmount,
                    TaxPercentage = TaxPercentage,
                    MobileRefNo = tempMobileRef,
                    Locality = Locality,
                    PostCodeId = PostCodeId,
                    PostCode = PostCode,
                    DestUnder = DestUnder,
                    QuotationType = rdbLocalityI == true? InternationalQuotationType : QuotationType,
                    Country = Country,
                    PostCodeIntl = PostCodeIntl,
                    RASurcharge = RASurcharge
                };
                Closed(AWBList);

                this.CloseWind();             
            }
        }

        public void CleartempDetail()
        {
            ReferenceNum = ""; RShipperName = ""; RShipperAddress = ""; RSenderZipcode = ""; RShipperTelNo = ""; RCreatedDate = ""; RExpiredDate = ""; RSendTo = ""; RCompany = ""; RReceiverAddress = ""; RZipcode = ""; RPhone = "";

            QuotationItems.Clear();
        }

        //private void PostCodeDestination()
        //{
        //    SqlDataReader dr;
        //    dr = GetDataReader($"Select TOP 1 * from tblPostCode2020 where remarks like '%ODA%' and postcode = '{ PostCode.Trim() }'");
        //    if (dr.Read())
        //    {
        //        MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Some of this destination postcode contain ODA. Choose ODA from PostCode Finder?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
        //        if (MsgConfirmation == MessageBoxResult.Yes)
        //        {
        //            ODAPostCode();
        //            dr.Close();
        //        }
        //        else
        //        {
        //            dr = GetDataReader($"Select Top 1 stncode from tblPostCode2020 where postcode = '{ PostCode.Trim() }' and remarks not like '%ODA%'");
        //            if (dr.Read())
        //            {
        //                DestCode = dr["stncode"].ToString().Trim().ToUpper();
        //            }
        //            else
        //            {
        //                dr.Close();
        //                dr = GetDataReader($"Select Top 1 b.DestCode from tblPostCode2020 a join tblDestination b on a.stncode = b.StationCode where a.postcode = '{ PostCode.Trim() }' and b.IsODA = 1");
        //                if (dr.Read())
        //                {
        //                    DestCode = dr["DestCode"].ToString().Trim().ToUpper();
        //                }
        //                else
        //                {
        //                    MessageBox.Show("Incorrect PostCode");
        //                    Price = 0; DestCode = ""; PostCode = "";
        //                }
        //                dr.Close();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        dr = GetDataReader($"Select Top 1 stncode from tblPostCode2020 where postcode = '{ PostCode.Trim() }'");
        //        if (dr.Read())
        //        {
        //            DestCode = dr["stncode"].ToString().Trim().ToUpper();
        //        }
        //        else
        //        {
        //            MessageBox.Show("Incorrect PostCode");
        //            Price = 0; DestCode = ""; PostCode = "";
        //        }
        //        dr.Close();
        //    }
        //}

        private void PostCodeDestination()
        {
            SqlDataReader dr, dr2, dr_OfficeCollection;
            dr_OfficeCollection = GetDataReader($"Select Top 1 stncode from tblPostCode2020 where postcode = '{ PostCode.Trim() }' and deliveryFrequency = 'OFFICE COLLECTION' and (state = 'SARAWAK' OR state = 'SABAH')");
            if (dr_OfficeCollection.Read())
            {
                DestCode = dr_OfficeCollection["stncode"].ToString() + " ODA";
                dr_OfficeCollection.Close();
            }
            else
            {
                dr_OfficeCollection.Close();
                dr = GetDataReader($"Select Top 1 stncode from tblPostCode2020 where postcode = '{ PostCode.Trim() }' and remarks not like '%ODA%'");
                if (dr.Read())
                {
                    string tempdestcode = dr["stncode"].ToString();
                    dr2 = GetDataReader($"Select Top 1 stncode from tblPostCode2020 where postcode = '{ PostCode.Trim() }' and remarks like '%ODA%'");
                    if (dr2.Read())
                    {
                        dr.Close(); dr2.Close();
                        MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Some of this destination postcode contain ODA. Choose ODA from PostCode Finder?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (MsgConfirmation == MessageBoxResult.Yes)
                        {
                            string PostCodeRemarks = "";
                            var ChildWnd = new ListHelpGen();
                            ChildWnd.FrmPostcodeFinder_Closed += (r => { PostCodeId = r.PostCodeId; DestCode = r.StnCode.Trim(); PostCodeRemarks = r.Remarks.Contains("ODA") ? DestCode = r.StnCode.Trim() + " ODA" : ""; });
                            ChildWnd.PostcodeFinderShowODACheck(PostCode);
                        }
                        else
                        {
                            DestCode = tempdestcode;
                        }
                    }
                    else
                    {
                        DestCode = tempdestcode;
                    }
                }
                else
                {
                    dr.Close();
                    dr = GetDataReader($"Select Top 1 b.DestCode from tblPostCode2020 a join tblDestination b on a.stncode = b.StationCode where a.postcode = '{ PostCode.Trim() }' and b.IsODA = 1");
                    if (dr.Read())
                    {
                        DestCode = dr["DestCode"].ToString().Trim().ToUpper();
                        dr.Close();
                    }
                    else
                    {
                        dr.Close();


                        MessageBox.Show("Incorrect PostCode", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Price = 0; DestCode = ""; PostCode = "";
                    }
                    
                }
            }
        }

        private void DestCodeValidity()
        {
            if (rdbLocalityM)
            {
                SqlDataReader dr;
                dr = GetDataReader($"SELECT RTRIM(A.DestCode) FROM tblDestination A LEFT JOIN tblZoneDetail B ON RTRIM(A.DestCode) = RTRIM(B.DestCode) where a.Country != 'Malaysia' and a.DestCode = '{ DestCode.Trim() }' ORDER BY A.DestCode");
                if (!dr.Read())
                {
                    MessageBox.Show("Incorrect DestCode", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Price = 0; DestCode = "";
                }
                dr.Close();
            }
        }

        private void Locality()
        {            
            if (rdbLocalityM == true)
            {
                QuotationType = ""; lblDestCodePost = "Post Code *"; Price = 0; DestCode = ""; PostCode = ""; YesInsurance = false; InsShipmentValue = 0; InsShipmentDesc = ""; OtherCharges = 0; tempPrice = 0; Country = ""; PostCodeIntl = ""; RASurcharge = Convert.ToDecimal(0.00);
                MyWndow.rdbLocalityI.Content = "INTL"; MyWndow.btnCalcIntl.Visibility = Visibility.Hidden;
                MyWndow.lblRASurchargeLabel.Visibility = Visibility.Hidden; MyWndow.lblRASurchargeAmount.Visibility = Visibility.Hidden;
                if (dtIntlPostCode != null)
                    dtIntlPostCode.Clear();

                MyWndow.lblDestCode.Visibility = Visibility.Visible;
                MyWndow.txtPostCode.Visibility = Visibility.Visible;
                MyWndow.txtDestCode.Visibility = Visibility.Hidden;
                MyWndow.txtPostCodeIntl.Visibility = Visibility.Hidden;
                MyWndow.btnsave.IsEnabled = true;
                MyWndow.txtMobileReferenceNo.IsEnabled = true;
                MyWndow.btnCheckRef.IsEnabled = true;
            }
            else if (rdbLocalityI == true)
            {
                QuotationType = "International"; Price = 0; DestCode = ""; PostCode = ""; YesInsurance = false; InsShipmentValue = 0; InsShipmentDesc = ""; OtherCharges = 0; MobileReferenceNo = ""; ExecuteCommandGen("Check Reference"); MobileReferenceNoColor = "Black"; tempPrice = 0; RASurcharge = Convert.ToDecimal(0.00);
                MyWndow.btnCalcIntl.Visibility = Visibility.Visible;
                MyWndow.lblRASurchargeLabel.Visibility = Visibility.Visible; MyWndow.lblRASurchargeAmount.Visibility = Visibility.Visible;
                DestUnder = "";
                ShipmentComboboxload("Intl");

                MyWndow.txtPostCodeIntl.Visibility = Visibility.Visible;
                MyWndow.txtMobileReferenceNo.IsEnabled = false;
                MyWndow.btnCheckRef.IsEnabled = false;
                  
                IntlPostCodeLoad();
            }
        }

        private void IntlPostCodeLoad()
        {
            bool isLoad = true;
            //string tempCountry = Country, tempPostCodeIntl = PostCodeIntl;
            //Country = ""; PostCodeIntl = "";
            while (isLoad)
            {
                var ChildWnd = new ListHelpGen();

                if (Country != "" && !LoadEdit)
                {
                    //MyWndow.rdbLocalityI.Content = "INTL (" + Country + ")";
                    try
                    {
                        if (dtIntlPostCode != null) { dtIntlPostCode.Clear(); }
                        GetIntlPostCode obj = new GetIntlPostCode();
                        dtIntlPostCode = obj.IntlPostCode(Country);

                        if (dtIntlPostCode != null && dtIntlPostCode.Rows.Count > 0)
                        {
                            int count = 0;
                            foreach (DataRow row in dtIntlPostCode.Rows)
                            {
                                count += 1;
                                foreach (DataColumn column in dtIntlPostCode.Columns)
                                {
                                    row.SetField("country", count);
                                }
                            }

                            ChildWnd.FrmListCol4_Closed += (r => { PostCodeIntl = r.ColTwoText == "" ? r.ColFourText : r.ColTwoText + "-" + r.ColThreeText; /*IntlCity = r.ColFourText;*/ });
                            ChildWnd.FourColWindShow(dtIntlPostCode, "PostCode Selection for " + Country, "No", "PostCode Start", "PostCode End", "City Town");
                        }
                        else
                        {
                            MessageBox.Show("No post code to preview");
                        }
                    }
                    catch (Exception Ex) { error_log.errorlog(Ex.Message); MessageBox.Show(Ex.Message); }

                    AWBPrice();
                    isLoad = false;
                }
                else if (!LoadEdit)
                {
                    MyWndow.rdbLocalityI.Content = "INTL";
                    //dtIntlPostCode = GetDataTable($"SELECT distinct row_number() OVER (order by A.Country) As ColOneId, A.Country As ColOneText FROM tblDestination A LEFT JOIN tblZoneDetail B ON RTRIM(A.DestCode) = RTRIM(B.DestCode) where A.Country != 'MALAYSIA' and A.Country != '' group by A.Country ORDER BY A.Country, ColOneId");

                    //dtIntlPostCode = GetDataTable($"SELECT DISTINCT Country As ColOneText, 0 As ColOneId from tblDestination where Country IS NOT NULL AND Country != '' AND Country != 'MALAYSIA'");

                    dtIntlCountry = IntlCountries();

                    if (dtIntlCountry != null && dtIntlCountry.Rows.Count > 0)
                    {
                        dtIntlCountry.Columns.Add("ColOneId");
                        dtIntlCountry.Columns["country"].ColumnName = "ColOneText";

                        int count = 0;
                        foreach (DataRow row in dtIntlCountry.Rows)
                        {
                            count += 1;
                            foreach (DataColumn column in dtIntlCountry.Columns)
                            {
                                row.SetField("ColOneId", count);
                            }
                        }

                        ChildWnd.FrmListSingleCol_Closed += (r => { Country = DestCode = r.ColOneText; });
                        ChildWnd.SingleColWindShow(dtIntlCountry, "Country List");

                        //SqlDataReader drDestCode;
                        //drDestCode = GetDataReader($"SELECT top 1 A.DestCode FROM tblDestination A LEFT JOIN tblZoneDetail B ON RTRIM(A.DestCode) = RTRIM(B.DestCode) where A.Country = '{Country}' and A.Country != ''");

                        //if (drDestCode.Read()) { DestCode = drDestCode["DestCode"].ToString(); }
                        //drDestCode.Close();
                    }

                    if (Country == "")
                    {
                        isLoad = false;
                        rdbLocalityM = true;
                        rdbLocalityI = false;
                    }
                }
                else
                {
                    isLoad = false;
                }
            }
        }

        private void LoadDestCode()
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();

            if (rdbLocalityM == true)
            {
                string PostCodeRemarks = "";
                ispostcodefinder = true;
                ChildWnd.FrmPostcodeFinder_Closed += (r => { PostCodeId = r.PostCodeId; PostCode = r.PostCode; DestCode = r.StnCode.Trim(); PostCodeRemarks = (r.Remarks.Contains("ODA") || (r.DeliveryFreq.Contains("OFFICE COLLECTION") && (r.State.Contains("SARAWAK") || r.State.Contains("SABAH")))) ? DestCode = r.StnCode.Trim() + " ODA" : ""; RZipcode = r.PostCode; });
                ChildWnd.PostcodeFinderShow();
                ispostcodefinder = false;

                //if (PostCodeId > 0 && PostCodeRemarks.StartsWith("ODA"))
                //{
                //    SqlDataReader dr = GetDataReader($"SELECT DestCode FROM tblDestination WHERE StationCode = '{DestCode}' AND IsODA = 1");
                //    if (dr.Read())
                //    {
                //        DestCode = dr["DestCode"].ToString().Trim();
                //    }
                //    dr.Close();
                //}
            }

            else if (rdbLocalityI == true)
            {
                IntlPostCodeLoad();
            }
        }

        public void DestinationDetails()
        {
            QuotationId = "";
            SqlDataReader dr;
            string tozone = "";
            
            try
            {
                if (!string.IsNullOrEmpty(DestCode) && DestCode.Length >= 3 && rdbLocalityM)
                {
                    if (CheckValidDestStation(DestCode))
                    {
                        dr = GetDataReader($"SELECT StationCode FROM tblDestination WHERE RTRIM(DestCode) = '{DestCode.Trim()}'");
                        if (dr.Read())
                        {
                            DestStation = dr["StationCode"].ToString().Trim().ToUpper();
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT ZoneCode FROM tblZoneDetail WHERE RTRIM(DestCode) = '{DestCode.Trim()}'");
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                tozone = dr["ZoneCode"].ToString().Trim().ToUpper();
                            }
                        }
                        else
                        {
                            dr = GetDataReader($"SELECT ToZone FROM tblQuotationDet WHERE RTRIM(ToZone) = '{DestCode.Trim()}'");

                            if (dr.HasRows)
                            {
                                if (dr.Read())
                                {
                                    tozone = dr["ToZone"].ToString().Trim().ToUpper();
                                }
                            }
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT QuotationId FROM tblQuotationDet WHERE ToZone = '{tozone.Trim()}' AND ToDate IS NULL");
                        if (dr.Read())
                        {
                            QuotationId = dr["QuotationId"].ToString();

                            dr = GetDataReader($"SELECT QuotationType FROM tblQuotation WHERE QuotationId = '{QuotationId}' AND ToDate IS NULL");
                            if (dr.HasRows)
                            {
                                if (dr.Read())
                                {
                                    QuotationType = dr["QuotationType"].ToString();
                                }
                            }
                        }
                        dr.Close();

                        //if (tozone.Trim() == ("ZONE 5") || tozone.Trim() == ("ZONE 6") || tozone.Trim() == ("ZONE 7") || QuotationType == "International")
                        //{
                        //    //Wndow.txtStaffCode.IsEnabled = false;
                        //    //Wndow.lblStaffName.IsEnabled = false;
                        //    //Wndow.StaffCodeButton.IsEnabled = false;
                        //    //StaffCode = "";
                        //}


                        dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational, b.QuotationType FROM tblQuotationDet a" +
                            $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                            $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('DOCUMENT', 'DOCUMENTS') AND a.ToDate IS NULL and b.ToDate IS NULL" +
                            $" ORDER BY a.FromDate DESC");
                        if (dr.Read())
                        {
                            DFirstTitle = "First " + dr["FirstWeight"].ToString() + " Kgs";
                            DFirstWeight = Convert.ToDecimal(dr["FirstWeight"]);
                            DFirstRate = GetRoundingVal(Convert.ToDecimal(dr["FirstRate"]), 2);
                            DAddTitle = "Add " + dr["AddWeight"].ToString() + " Kgs";
                            DAddWeight = Convert.ToDecimal(dr["AddWeight"]);
                            DAddRate = GetRoundingVal(Convert.ToDecimal(dr["AddRate"]), 2);
                            DSurcharge = GetRoundingVal(Convert.ToDecimal(dr["SurCharge"]), 2);

                            DestUnder = dr["ToZone"].ToString();
                            IntlSurCharge = Convert.ToDecimal(dr["IntlSurCharge"]);
                            TaxName = dr["TaxTitle"].ToString();
                            TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                            InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                            InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational FROM tblQuotationDet a" +
                            $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                            $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('MOTOR BIKE', 'MOTOR BIKES') AND a.ToDate IS NULL and b.ToDate IS NULL" +
                            $" ORDER BY a.FromDate DESC");
                        double DoubleWeight = Convert.ToDouble(Weight);
                        if (Weight > 0)
                        {
                            dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational FROM tblQuotationDet a" +
                                $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                                $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('MOTOR BIKE', 'MOTOR BIKES') AND a.FirstWeight <= {DoubleWeight} AND (a.FirstWeight + a.AddWeight) >= {DoubleWeight} AND a.ToDate IS NULL and b.ToDate IS NULL" +
                                $" ORDER BY a.FromDate DESC");
                        }
                        if (dr.Read())
                        {
                            MFirstTitle = "First " + dr["FirstWeight"].ToString() + " Kgs";
                            MFirstWeight = Convert.ToDecimal(dr["FirstWeight"]);
                            MFirstRate = GetRoundingVal(Convert.ToDecimal(dr["FirstRate"]), 2);
                            MAddTitle = "Add " + dr["AddWeight"].ToString() + " Kgs";
                            MAddWeight = Convert.ToDecimal(dr["AddWeight"]);
                            MAddRate = GetRoundingVal(Convert.ToDecimal(dr["AddRate"]), 2);
                            MSurcharge = GetRoundingVal(Convert.ToDecimal(dr["SurCharge"]), 2);

                            DestUnder = dr["ToZone"].ToString();
                            IntlSurCharge = Convert.ToDecimal(dr["IntlSurCharge"]);
                            TaxName = dr["TaxTitle"].ToString();
                            TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                            InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                            InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational FROM tblQuotationDet a" +
                            $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                            $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('BICYCLE', 'BICYCLES') AND a.ToDate IS NULL and b.ToDate IS NULL" +
                            $" ORDER BY a.FromDate DESC");

                        double BDoubleWeight = Convert.ToDouble(Weight);
                        if (Weight > 0)
                        {
                            dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational FROM tblQuotationDet a" +
                                $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                                $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('BICYCLE', 'BICYCLES') AND a.FirstWeight <= {BDoubleWeight} AND (a.FirstWeight + a.AddWeight) >= {BDoubleWeight} AND a.ToDate IS NULL and b.ToDate IS NULL" +
                                $" ORDER BY a.FromDate DESC");
                        }
                        if (dr.Read())
                        {
                            BFirstTitle = "First " + dr["FirstWeight"].ToString() + " Kgs";
                            BFirstWeight = Convert.ToDecimal(dr["FirstWeight"]);
                            BFirstRate = GetRoundingVal(Convert.ToDecimal(dr["FirstRate"]), 2);
                            BAddTitle = "Add " + dr["AddWeight"].ToString() + " Kgs";
                            BAddWeight = Convert.ToDecimal(dr["AddWeight"]);
                            BAddRate = GetRoundingVal(Convert.ToDecimal(dr["AddRate"]), 2);
                            BSurcharge = GetRoundingVal(Convert.ToDecimal(dr["SurCharge"]), 2);

                            DestUnder = dr["ToZone"].ToString();
                            IntlSurCharge = Convert.ToDecimal(dr["IntlSurCharge"]);
                            TaxName = dr["TaxTitle"].ToString();
                            TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                            InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                            InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT a.FirstWeight, a.FirstRate, a.AddWeight, a.AddRate, a.SurCharge, a.FromZone, a.ToZone, a.ShipmentType, b.SurCharge as IntlSurCharge, b.TaxTitle, b.TaxPercentage, b.InsuranceNormal, b.InsuranceInternational FROM tblQuotationDet a" +
                            $" JOIN tblQuotation b on a.QuotationId = b.QuotationId" +
                            $" WHERE RTRIM(ToZone) = '{tozone.Trim()}' and RTRIM(ShipmentType) IN ('PARCEL', 'PARCELS') AND (a.FirstWeight = 0.01 OR a.FirstWeight = 0.5 OR a.FirstWeight = 1.0 OR a.FirstWeight = 1.5 OR a.FirstWeight = 2.0 OR a.FirstWeight = 3.0 OR a.FirstWeight = 4.0 OR a.FirstWeight = 5.0) AND a.ToDate IS NULL and b.ToDate IS NULL" +
                            $" ORDER BY a.FromDate DESC");

                        if (dr.Read())
                        {
                            PFirstTitle = "First " + dr["FirstWeight"].ToString() + " Kgs";
                            PFirstWeight = Convert.ToDecimal(dr["FirstWeight"]);
                            PFirstRate = GetRoundingVal(Convert.ToDecimal(dr["FirstRate"]), 2);
                            PAddTitle = "Add " + dr["AddWeight"].ToString() + " Kgs";
                            PAddWeight = Convert.ToDecimal(dr["AddWeight"]);
                            PAddRate = GetRoundingVal(Convert.ToDecimal(dr["AddRate"]), 2);
                            PSurcharge = GetRoundingVal(Convert.ToDecimal(dr["SurCharge"]), 2);

                            DestUnder = dr["ToZone"].ToString();
                            IntlSurCharge = Convert.ToDecimal(dr["IntlSurCharge"]);
                            TaxName = dr["TaxTitle"].ToString();
                            TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                            InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                            InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT FreqDocument, FreqParcel FROM tblPostCode WHERE StnCode = '{DestCode.Trim()}'");
                        if (dr.Read())
                        {
                            FreqDocument = dr["FreqDocument"].ToString();
                            FreqParcel = dr["FreqParcel"].ToString();
                        }
                        dr.Close();


                        if (SelectedShipment != null)
                        {
                            string shipment = SelectedShipment.ShipmentTypeName;
                            string ZoneDescDest = DestZoneDesc;
                            string zonecodess = "";
                            dr = GetDataReader($"SELECT top 1 UPPER(a.ZoneCode) as 'ZoneCode' FROM tblZoneDetail a WHERE LTRIM(RTRIM(a.DestCode)) = '{DestCode.Trim()}'");
                            if (dr.HasRows)
                            {
                                if (dr.Read())
                                {
                                    zonecodess = dr["ZoneCode"].ToString();
                                }
                            }

                            if (shipment == "MOTOR BIKES" || shipment == "MOTOR BIKE" || shipment == "BICYCLES" || shipment == "BICYCLE")
                            {
                                String strshipment = (shipment == "MOTOR BIKES" || shipment == "MOTOR BIKE") ? "Motor Bikes" : "Bicycles";

                                dr = GetDataReader($"SELECT BMZoneId FROM tblMotorBikeZone a WHERE LTRIM(RTRIM(a.ZoneCode)) = '{zonecodess.Trim()}' AND LTRIM(RTRIM(a.ShipmentType)) = '{strshipment}'");
                                if (!dr.HasRows)
                                {
                                    MessageBox.Show(shipment + " can't send to selected Zone :" + zonecodess, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                                    DestCode = "";                                    
                                }
                                dr.Close();
                            }
                        }
                    }
                    else
                    {
                        WebDb = new CommunicateWebDb();
                        WebDb.GetDestination(DestCode);

                        dr = GetDataReader($"SELECT DestCode FROM tblZoneDetail WHERE DestCode = '{DestCode}'");

                        if (!dr.Read())
                        {
                            MessageBox.Show("Incorrect destination code", "Information", MessageBoxButton.OK, MessageBoxImage.Information); DestCode = ""; MyWndow.txtDestCode.Focus();
                        }
                        else
                        {
                            DestinationDetails();
                        }
                    }
                }
                else
                {
                    //ResetValue();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Shipment Details based on DestCode Function : " + Ex.ToString()); }
        }

        private void CheckReference()
        {
            try
            {
                if (!string.IsNullOrEmpty(MobileReferenceNo))
                {
                    SqlDataReader dr = GetDataReader($"SELECT RefMobID, ExpiredDate FROM tblRefMobileApp WHERE RefNum = '{MobileReferenceNo}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (Convert.ToDateTime(dr["ExpiredDate"]).Date < DateTime.Now.Date)
                                MessageBox.Show("Sorry Reference Code Expired", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            else
                            {
                                dr.Close();
                                LoadReference();
                            }
                        }
                        dr.Close();
                    }
                    else CheckMobileReference();
                }
                else
                {
                    IsMobileRef(false);
                }
            }
            catch (Exception Ex) { error_log.errorlog("CheckReference  in Mobile Ref Cash Sales:" + Ex.Message); }
        }

        private void CheckMobileReference()
        {
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    HttpClient client;
                    MobileRefAuth obtmref;
                    string responsecodestr = "";
                    using (new WaitCursor())
                    {
                        client = new HttpClient();
                        client.BaseAddress = new Uri(APIMobileReferenceBase);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        obtmref = new MobileRefAuth();
                        obtmref.RefNum = MobileReferenceNo;
                        obtmref.Token = APIMobileReferenceToken.Replace("A@MA", "").Replace("S%12", "");

                        var response = client.PostAsJsonAsync(APIMobileReferenceGet, obtmref).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            responsecodestr = "";
                            var query = response.Content.ReadAsStringAsync().Result;
                            RefMobileAppModel item = JsonConvert.DeserializeObject<RefMobileAppModel>(query.ToString());
                            if (item != null)
                            {
                                string refnum = item.RefNum.Trim();

                                SqlDataReader dr = GetDataReader($"SELECT RefMobID FROM tblRefMobileApp WHERE RefNum = '{item.RefNum.Trim()}'");
                                if (dr.HasRows)
                                {
                                    ExecuteQuery($"UPDATE tblRefMobileApp SET" +
                                        $" ShipperName = '{(!string.IsNullOrEmpty(item.ShipperName) ? item.ShipperName.Trim().Replace("'", "''") : "")}'," +
                                        $" ShipperAdd1 = '{(!string.IsNullOrEmpty(item.ShipperAdd1) ? item.ShipperAdd1.Trim().Replace("'", "''") : "")}'," +
                                        $" ShipperAdd2 = '{(!string.IsNullOrEmpty(item.ShipperAdd2) ? item.ShipperAdd2.Trim().Replace("'", "''") : "")}'," +
                                        $" ShipperAdd3 = '{(!string.IsNullOrEmpty(item.ShipperAdd3) ? item.ShipperAdd3.Trim().Replace("'", "''") : "")}'," +
                                        $" ShipperAdd4 = '{(!string.IsNullOrEmpty(item.ShipperAdd4) ? item.ShipperAdd4.Trim().Replace("'", "''") : "")}'," +
                                        $" ShipperTelNo = '{(!string.IsNullOrEmpty(item.ShipperTelNo) ? item.ShipperTelNo.Trim() : "")}'," +
                                        $" SendTo = '{(!string.IsNullOrEmpty(item.SendTo) ? item.SendTo.Trim().Replace("'", "''") : "")}'," +
                                        $" CompName = '{(!string.IsNullOrEmpty(item.CompName) ? item.CompName.Trim().Replace("'", "''") : "")}'," +
                                        $" Address1 = '{(!string.IsNullOrEmpty(item.Address1) ? item.Address1.Trim().Replace("'", "''") : "")}'," +
                                        $" Address2 = '{(!string.IsNullOrEmpty(item.Address2) ? item.Address2.Trim().Replace("'", "''") : "")}'," +
                                        $" City = '{(!string.IsNullOrEmpty(item.City) ? item.City.Trim().Replace("'", "''") : "")}'," +
                                        $" State = '{(!string.IsNullOrEmpty(item.State) ? item.State.Trim().Replace("'", "''") : "")}'," +
                                        $" ZipCode = '{(!string.IsNullOrEmpty(item.ZipCode) ? item.ZipCode.Trim() : "")}'," +
                                        $" Phone = '{(!string.IsNullOrEmpty(item.Phone) ? item.Phone.Trim() : "")}'," +
                                        $" Createdate = '{Convert.ToDateTime(item.CreatedDate).ToString("yyyy-MM-dd hh:mm:ss")}'," +
                                        $" ExpiredDate = '{Convert.ToDateTime(item.ExpiredDate.AddDays(7)).ToString("yyyy-MM-dd hh:mm:ss")}'," +
                                        $" CDate = GETDATE()," +
                                        $" Status = 1, IsExported = 0, ExportedDate = NULL" +
                                        $" WHERE RefNum = '{item.RefNum.Trim()}'");
                                }
                                else
                                {
                                    // RefMobID, RefNum, ShipperName, ShipperAdd1, ShipperAdd2, ShipperAdd3, ShipperAdd4, ShipperTelNo, SendTo, CompName, Address1, Address2, City, State, ZipCode, Phone, Createdate, ExpiredDate, CUserID, CDate, Status, IsExported, ExportedDate
                                    ExecuteQuery($"iNSERT INTO tblRefMobileApp(RefNum, ShipperName, ShipperAdd1, ShipperAdd2, ShipperAdd3, ShipperAdd4, ShipperTelNo, SendTo, CompName, Address1, Address2, City, State, ZipCode, Phone, Createdate, ExpiredDate, CUserID, CDate, Status, IsExported, ExportedDate)" +
                                        $"VALUES('{refnum}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperName) ? item.ShipperName.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperAdd1) ? item.ShipperAdd1.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperAdd2) ? item.ShipperAdd2.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperAdd3) ? item.ShipperAdd3.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperAdd4) ? item.ShipperAdd4.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ShipperTelNo) ? item.ShipperTelNo.Trim() : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.SendTo) ? item.SendTo.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.CompName) ? item.CompName.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.Address1) ? item.Address1.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.Address2) ? item.Address2.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.City) ? item.City.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.State) ? item.State.Trim().Replace("'", "''") : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.ZipCode) ? item.ZipCode.Trim() : "")}'," +
                                        $" '{(!string.IsNullOrEmpty(item.Phone) ? item.Phone.Trim() : "")}'," +
                                        $" '{Convert.ToDateTime(item.CreatedDate).ToString("yyyy-MM-dd hh:mm:ss")}'," +
                                        $" '{Convert.ToDateTime(item.ExpiredDate.AddDays(7)).ToString("yyyy-MM-dd hh:mm:ss")}'," +
                                        $" {LoginUserId}," +
                                        $" GETDATE(), 1, 1, NULL)");
                                }
                                LoadReference();
                            }
                        }
                        else
                        {
                            responsecodestr = response.StatusCode.ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(responsecodestr))
                    {
                        if (responsecodestr.Contains("BadRequest"))
                        {
                            MessageBox.Show("Sorry, it's bad request, kindly contact supporting team", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            MobileReferenceNo = string.Empty;
                        }
                        else if (responsecodestr.Contains("NotFound"))
                        {
                            MessageBox.Show("Sorry, Reference number not found", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            MobileReferenceNoColor = "Black";
                            MobileReferenceNo = string.Empty;
                            //ReferenceNum = ""; RShipperName = ""; RShipperAddress = ""; RShipperTelNo = ""; RCreatedDate = ""; RExpiredDate = ""; RSendTo = ""; RCompany = ""; RReceiverAddress = ""; RZipcode = ""; RPhone = ""; PostCode = ""; DestCode = "";  YesInsurance = false; InsShipmentValue = 0; InsShipmentDesc = ""; OtherCharges = 0;
                            ReferenceNum = RShipperName = RShipperAddress = RSenderZipcode = RShipperTelNo = RCreatedDate = RExpiredDate = RSendTo = RCompany =  RReceiverAddress = RZipcode = RPhone = PostCode = DestCode = InsShipmentDesc = "";
                            //Enable textbox
                            //MyWndow.txtRShipperName.IsEnabled = MyWndow.txtRShipperAddress1.IsEnabled = MyWndow.txtRShipperAddress2.IsEnabled = MyWndow.txtRShipperAddress3.IsEnabled = MyWndow.txtRSenderZipcode.IsEnabled = MyWndow.txtRShipperTelNo.IsEnabled /*= MyWndow.txtRCreatedDate.IsEnabled = MyWndow.txtRExpiredDate.IsEnabled*/ = MyWndow.txtRSendTo.IsEnabled = MyWndow.txtRCompany.IsEnabled = MyWndow.txtRReceiverAddress1.IsEnabled = MyWndow.txtRReceiverAddress2.IsEnabled = MyWndow.txtRReceiverAddress3.IsEnabled = /*MyWndow.txtRZipcode.IsEnabled =*/ MyWndow.txtRPhone.IsEnabled = true;
                            YesInsurance = false; InsShipmentValue = OtherCharges = 0;
                        }
                        else if (responsecodestr.Contains("RefNo Expired"))
                        {
                            MessageBox.Show("Sorry, Reference number expired", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            MobileReferenceNo = string.Empty;
                        }
                        else
                        {
                            MessageBox.Show("Sorry, Unable to proceess now", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            MobileReferenceNo = string.Empty;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Internet connection failure", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    MobileReferenceNo = string.Empty;
                }
            }
            catch (Exception Ex) 
            { 
                General.error_log.errorlog("API in Mobile Ref Cash Sales: " + Ex.ToString());
                IsMobileRef(false);
            }
        }

        private void LoadReference()
        {
            try
            {
                if (!string.IsNullOrEmpty(MobileReferenceNo))
                {
                    SqlDataReader dr = GetDataReader($"SELECT * FROM tblRefMobileApp WHERE RefNum = '{MobileReferenceNo}'");
                    if (dr.Read())
                    {
                        MobileReferenceNoColor = "Green";
                        rdbLocalityM = true;

                        ReferenceNum = dr["RefNum"].ToString();
                        RShipperName = dr["ShipperName"].ToString();
                        RShipperAddress = (dr["ShipperAdd1"] != DBNull.Value ? dr["ShipperAdd1"].ToString() + ", " : "") +
                            (dr["ShipperAdd2"] != DBNull.Value ? dr["ShipperAdd2"].ToString() + "," : "") +
                            (dr["ShipperAdd3"] != DBNull.Value ? dr["ShipperAdd3"].ToString() + "," : "") +
                            (dr["ShipperAdd4"] != DBNull.Value ? dr["ShipperAdd4"].ToString() + "," : "");
                        RSenderZipcode = (dr["ShipperAdd3"] != DBNull.Value ? dr["ShipperAdd3"].ToString() : "");
                        RShipperTelNo = dr["ShipperTelNo"].ToString();
                        RCreatedDate = dr["Createdate"].ToString();
                        RExpiredDate = dr["ExpiredDate"].ToString();
                        RSendTo = dr["SendTo"].ToString();
                        RCompany = dr["CompName"].ToString();
                        RReceiverAddress = (dr["Address1"] != DBNull.Value ? dr["Address1"].ToString() + ", " : "") +
                            (dr["Address2"] != DBNull.Value ? dr["Address2"].ToString() + ", " : "") +
                            (dr["City"] != DBNull.Value ? dr["City"].ToString() + ", " : "") +
                            (dr["State"] != DBNull.Value ? dr["State"].ToString() + ", " : "");
                        RZipcode = dr["ZipCode"].ToString();
                        RPhone = dr["Phone"].ToString();

                        //if(!update) 
                        //{ 
                            PostCode = dr["ZipCode"].ToString();
                        //}

                        IsMobileRef(true);
                    }
                    else
                    {
                        IsMobileRef(false);
                    }
                    dr.Close();

                    //Getting postcode id if contain ODA
                    //if (!update && !string.IsNullOrEmpty(MobileReferenceNo))
                    //{
                    //    SqlDataReader dr2;
                    //    bool CheckingODA = false;
                    //    dr2 = GetDataReader($"SELECT Top 1 * FROM tblDestination a join tblPostcode2020 b on a.StationCode = b.stncode WHERE b.postcode = '{PostCode}' AND a.isoda = 1 AND b.remarks like '%ODA%'");                        
                    //    if (dr2.Read())
                    //    {
                    //        CheckingODA = true;
                    //    }
                    //    dr2.Close();

                    //    if(CheckingODA == true)
                    //    {
                    //        MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Some of this destination contain ODA. Choose ODA from PostCode Finder?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    //        if (MsgConfirmation == MessageBoxResult.Yes)
                    //        {
                    //            MyWndow.TabReference.IsSelected = true;
                    //            ODAPostCode();
                    //            MyWndow.TabAWB.IsSelected = true;
                    //        }                            
                    //    }
                    //}
                }
                else
                {
                    IsMobileRef(false);
                    //ReferenceNum = RShipperName = RShipperAddress = RShipperTelNo = RCreatedDate = RExpiredDate = RSendTo = RCompany = RReceiverAddress = RZipcode = RPhone = "";
                }
            }
            catch (Exception Ex) { error_log.errorlog("LoadReference load function:" + Ex.ToString()); }
        }

        private void IsMobileRef(bool Condition)
        {
            if(Condition == true)
            {
                //MyWndow.txtRShipperName.IsEnabled = MyWndow.txtRShipperAddress1.IsEnabled = MyWndow.txtRShipperAddress2.IsEnabled = MyWndow.txtRShipperAddress3.IsEnabled = MyWndow.txtRSenderZipcode.IsEnabled = MyWndow.txtRShipperTelNo.IsEnabled = MyWndow.txtRCreatedDate.IsEnabled = MyWndow.txtRExpiredDate.IsEnabled = MyWndow.txtRSendTo.IsEnabled = MyWndow.txtRCompany.IsEnabled = MyWndow.txtRReceiverAddress1.IsEnabled = MyWndow.txtRReceiverAddress2.IsEnabled = MyWndow.txtRReceiverAddress3.IsEnabled = MyWndow.txtRZipcode.IsEnabled = MyWndow.txtRPhone.IsEnabled = false;
                MyWndow.lblMobileRefDetails.Visibility = Visibility.Visible;
                MyWndow.lblMobileRefDetails.Content = "Reference Number : " + MobileReferenceNo;
            }
            else if(Condition == false)
            {
                MobileReferenceNoColor = "Black";
                ReferenceNum = RShipperName = RShipperAddress = RSenderZipcode = RShipperTelNo = RCreatedDate = RExpiredDate = RSendTo = RCompany = RReceiverAddress = RZipcode = RPhone = PostCode = DestCode = InsShipmentDesc = "";
                //Enable textbox
                //MyWndow.txtRShipperName.IsEnabled = MyWndow.txtRShipperAddress1.IsEnabled = MyWndow.txtRShipperAddress2.IsEnabled = MyWndow.txtRShipperAddress3.IsEnabled = MyWndow.txtRSenderZipcode.IsEnabled = MyWndow.txtRShipperTelNo.IsEnabled /*= MyWndow.txtRCreatedDate.IsEnabled = MyWndow.txtRExpiredDate.IsEnabled*/ = MyWndow.txtRSendTo.IsEnabled = MyWndow.txtRCompany.IsEnabled = MyWndow.txtRReceiverAddress1.IsEnabled = MyWndow.txtRReceiverAddress2.IsEnabled = MyWndow.txtRReceiverAddress3.IsEnabled =/* MyWndow.txtRZipcode.IsEnabled =*/ MyWndow.txtRPhone.IsEnabled = true;
                MyWndow.lblMobileRefDetails.Visibility = Visibility.Hidden;
                MyWndow.lblMobileRefDetails.Content = "";
            }
        }

        private void NetSummary()
        {
            QuotationType = QuotationType == null || QuotationType.Trim() == "" ? "Standard" : QuotationType.Trim();

            SqlDataReader dr = GetDataReader($"SELECT TaxTitle, TaxPercentage, Surcharge FROM tblQuotation WHERE upper(QuotationType) = '{QuotationType.Trim().ToUpper()}' AND ToDate IS NULL");
            if (dr.Read())
            {
                TaxName = dr["TaxTitle"].ToString();
                TaxPercentage = QuotationType == "International" ? Convert.ToDecimal(dr["Surcharge"]) : Convert.ToDecimal(dr["TaxPercentage"]);
            }
            dr.Close();
            
            TaxPercentage = ((TotWeight > 30 && QuotationType.Trim().ToUpper() == "STANDARD") || (SelectedShipment.ShipmentTypeName == "MOTOR BIKES" || SelectedShipment.ShipmentTypeName == "MOTOR BIKE")) ? 0 : TaxPercentage;

            TotAmtBeforeTax = GetRoundingVal((Price + OtherCharges + InsurenceCharges), 2);
            //PromoAmount = GetRoundingVal((TotAmtBeforeTax > 0 && PromoPercentage > 0 ? TotAmtBeforeTax * PromoPercentage / 100 : 0), 2);
            //StaffDisAmt = GetRoundingVal((TotAmtBeforeTax > 0 && StaffCode.Trim() != "" ? TotAmtBeforeTax * 20 / 100 : 0), 2);
            //TotAmtBeforeTax = GetRoundingVal((TotAmtBeforeTax - (StaffDisAmt + PromoAmount)), 2);
            TaxAmt = GetRoundingVal((TotAmtBeforeTax > 0 && TaxPercentage > 0 ? TotAmtBeforeTax * TaxPercentage / 100 : 0), 2);

            if (TaxAmt == 0 && InsurenceCharges > 0)
            {
                dr = GetDataReader($"SELECT TaxTitle, TaxPercentage, Surcharge FROM tblQuotation WHERE QuotationType = 'Standard' AND ToDate IS NULL");
                if (dr.Read())
                {
                    TaxName = dr["TaxTitle"].ToString();
                    TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                }
                dr.Close();

                TaxAmt = TaxPercentage > 0 ? GetRoundingVal(((InsurenceCharges) * TaxPercentage / 100), 2) : 0;
            }

            TotalAmount = Price + OtherCharges + InsurenceCharges;

            MyWndow.lblShpmntCost1.Content = string.Format("{0:0.00}", Price);
        }

        private void Check()
        {
            if (rdbLocalityM)
            {
                AWBPrice();
                MyWndow.btnsave.IsEnabled = true;
            }
            else
            {
                MyWndow.btnsave.IsEnabled = false;
            }
        }

        private void ResetData()
        {
            if (!update)
            {
                StrWeight = "1"; Pieces = 1; YesInsurance = false; DestCode = ""; InsurenceCharges = Convert.ToDecimal(0.00); MobileReferenceNoColor = "Black"; rdbLocalityM = true; rdbLocalityI = false; lblDestCodePost = "Post Code *"; MobileReferenceNo = ""; RASurcharge = Convert.ToDecimal(0.00);

                MyWndow.txtMobileReferenceNo.Focus();
                RShipperName = RShipperAddress = RSenderZipcode = RShipperTelNo = RSendTo = RCompany = RReceiverAddress = RZipcode = RPhone = "";
                Country = ""; PostCodeIntl = ""; /*IntlCity = "";*/
            }
            else
            {
                LoadScreen(tempAWBDetail);
            }
        }

        public class CSMRShipmentTypeModel : PropertyChangedNotification
        {
            public string ShipmentTypeId { get { return GetValue(() => ShipmentTypeId); } set { SetValue(() => ShipmentTypeId, value); OnPropertyChanged("ShipmentTypeId"); } }
            public string ShipmentTypeName { get { return GetValue(() => ShipmentTypeName); } set { SetValue(() => ShipmentTypeName, value); OnPropertyChanged("ShipmentTypeName"); } }
        }

        public class AWBDetailInfo
        {
            public int AWBNo { get; set; }
            public string ShipmentType { get; set; }
            public string DestCode { get; set; }
            public int Pieces { get; set; }
            public decimal Weight { get; set; }
            public decimal VolLength { get; set; }
            public decimal VolWidth { get; set; }
            public decimal VolHeight { get; set; }
            public decimal VolWeight { get; set; }
            public decimal AddRate { get; set; }
            public decimal Price { get; set; }
            public decimal OtherCharges { get; set; }
            public bool InsCheck { get; set; }
            public decimal InsShipmentValue { get; set; }
            public string InsShipmentDesc { get; set; }
            public decimal InsCharges { get; set; }
            public decimal TotalAmount { get; set; }
            public decimal TaxPercentage { get; set; }
            public string MobileRefNo { get; set; }
            public string Locality { get; set; }
            public int PostCodeId { get; set; }
            public string PostCode { get; set; }
            public string DestUnder { get; set; }
            public string QuotationType { get; set; }
            public string Country { get; set; }
            public string PostCodeIntl { get; set; }
            public decimal RASurcharge { get; set; }
        }
    }
}