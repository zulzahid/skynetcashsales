﻿using SkynetCashSales.General;
using SkynetCashSales.View.Reports;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    class PRIHATINVM : CNPrintVM
    {
        public FrmPRIHATIN MyWind;
        public PRIHATINVM(FrmPRIHATIN Wind)
        {
            MyWind = Wind;
        }

        public PRIHATINVM(FrmPRIHATIN Wind, decimal AVW_, decimal AWeight_, decimal PVW_, 
            decimal PWeight_, decimal PLength_, decimal PHeight_, decimal PWidth_, decimal ALength_, decimal Aheight_, decimal AWidth_,
            decimal AAmount_, decimal PAmount_, string ShipmentType_, int NoofPcs_, string AWBNumber_, string stnCode_, string destStn_)
        {
            MyWind = Wind;
            PWeight = PWeight_;
            AWeight = Convert.ToDecimal(AWeight_.ToString("0.00"));
            PVW = Math.Round((PVW_ == 0 ? 0.00m : PVW_),2);
            AVW = Math.Round((AVW_ == 0 ? 0.00m : AVW_),2);
            decimal HighestWeight = AWeight > AVW ? AWeight : AVW;
            PLength = PLength_;
            PHeight = PHeight_;
            PWidth = PWidth_;
            ALength = ALength_;
            AHeight = Aheight_;
            AWidth = AWidth_;
            PAmount = Math.Round((PAmount_ / 106 * 100),2);
            AAmount = AAmount_;
            PaidSST = Math.Round((PAmount_ - PAmount),2);
            ActualSST = HighestWeight > 30 ? 0 : Math.Round(SSTCalc(AAmount),2);
            PTotal = PAmount_;
            ATotal = Convert.ToDecimal(TotalCalc(AAmount, ActualSST).ToString("0.00"));
            Diff = Convert.ToDecimal((Math.Round((ATotal - PTotal)*20, MidpointRounding.ToEven) /20).ToString("0.00"));
            PaymentStatus = "I have collected RM" + Diff + "\n from this customer";
            ShipmentType = ShipmentType_;
            AWBNumber = AWBNumber_+"D";
            NoofPcs = NoofPcs_;
            StnCode = stnCode_;
            DestStn = destStn_;

            for (int i = 0; i < AWBNumber_.Length; i++)
            {
                if (Char.IsDigit(AWBNumber_[i]))
                    AWBNum_num += AWBNumber_[i];
            }
            PaymentModeComboboxload();
        }

        private int _selectedpaymentmodeindex;
        public int SelectedPaymentModeIndex { get { return _selectedpaymentmodeindex; } set { _selectedpaymentmodeindex = value; OnPropertyChanged("SelectedPaymentModeIndex"); } }        
        
        private ObservableCollection<CSMRShipmentTypeModel> _paymentmode = new ObservableCollection<CSMRShipmentTypeModel>();

        private ObservableCollection<CSMRShipmentTypeModel> _selectedpayment = new ObservableCollection<CSMRShipmentTypeModel>();
        public ObservableCollection<CSMRShipmentTypeModel> SelectedPayment { get { return _selectedpayment; } set { _selectedpayment = value; OnPropertyChanged("SelectedPayment"); } }

        public string SelectedValuePaymentMode { get { return GetValue(() => SelectedValuePaymentMode); } set { SetValue(() => SelectedValuePaymentMode, value); OnPropertyChanged("SelectedValuePaymentMode"); } }

        private ObservableCollection<PaymentModeModel> _PaymentMode = new ObservableCollection<PaymentModeModel>();
        public ObservableCollection<PaymentModeModel> PaymentMode { get { return _PaymentMode; } set { _PaymentMode = value; OnPropertyChanged("PaymentMode"); } }
        public string PaymentModeId { get { return GetValue(() => PaymentModeId); } set { SetValue(() => PaymentModeId, value); OnPropertyChanged("PaymentModeId"); } }
        public string PaymentModeName { get { return GetValue(() => PaymentModeName); } set { SetValue(() => PaymentModeName, value); OnPropertyChanged("PaymentModeName"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        public decimal PWeight { get { return GetValue(() => PWeight); } set { SetValue(() => PWeight, value); OnPropertyChanged("PWeight"); } }
        public decimal AWeight { get { return GetValue(() => AWeight); } set { SetValue(() => AWeight, value); OnPropertyChanged("AWeight"); } }

        public decimal ALength { get { return GetValue(() => ALength); } set { SetValue(() => ALength, value); OnPropertyChanged("ALength"); } }
        public decimal AHeight { get { return GetValue(() => AHeight); } set { SetValue(() => AHeight, value); OnPropertyChanged("AHeight"); } }
        public decimal AWidth { get { return GetValue(() => AWidth); } set { SetValue(() => AWidth, value); OnPropertyChanged("AWidth"); } }

        public decimal PLength { get { return GetValue(() => PLength); } set { SetValue(() => PLength, value); OnPropertyChanged("PLength"); } }
        public decimal PHeight { get { return GetValue(() => PHeight); } set { SetValue(() => PHeight, value); OnPropertyChanged("PHeight"); } }
        public decimal PWidth { get { return GetValue(() => PWidth); } set { SetValue(() => PWidth, value); OnPropertyChanged("PWidth"); } }



        public decimal PVW { get { return GetValue(() => PVW); } set { SetValue(() => PVW, value); OnPropertyChanged("PVW"); } }
        public decimal AVW { get { return GetValue(() => AVW); } set { SetValue(() => AVW, value); OnPropertyChanged("AVW"); } }
        public decimal PAmount { get { return GetValue(() => PAmount); } set { SetValue(() => PAmount, value); OnPropertyChanged("PAmount"); } }
        public decimal AAmount { get { return GetValue(() => AAmount); } set { SetValue(() => AAmount, value); OnPropertyChanged("AAmount"); } }
        public decimal Diff { get { return GetValue(() => Diff); } set { SetValue(() => Diff, value); OnPropertyChanged("Diff"); } }
        public decimal TtlVWT { get { return GetValue(() => TtlVWT); } set { SetValue(() => TtlVWT, value); OnPropertyChanged("TtlVWT"); } }
        public string PaymentStatus { get { return GetValue(() => PaymentStatus); } set { SetValue(() => PaymentStatus, value); OnPropertyChanged("PaymentStatus"); } }
        public string ShipmentType { get { return GetValue(() => ShipmentType); } set { SetValue(() => ShipmentType, value); OnPropertyChanged("ShipmentType"); } }
        public string AWBNumber { get { return GetValue(() => AWBNumber); } set { SetValue(() => AWBNumber, value); OnPropertyChanged("AWBNumber"); } }
        public string PaymentRef { get { return GetValue(() => PaymentRef); } set { SetValue(() => PaymentRef, value); OnPropertyChanged("PaymentRef"); } }
        public int NoofPcs { get { return GetValue(() => NoofPcs); } set { SetValue(() => NoofPcs, value); OnPropertyChanged("NoofPcs"); } }
        public string StnCode { get { return GetValue(() => StnCode); } set { SetValue(() => StnCode, value); OnPropertyChanged("StnCode"); } }
        public string DestStn { get { return GetValue(() => DestStn); } set { SetValue(() => DestStn, value); OnPropertyChanged("DestStn"); } }
        public string AWBNum_num { get { return GetValue(() => AWBNum_num); } set { SetValue(() => AWBNum_num, value); OnPropertyChanged("AWBNum_num"); } }


        public string CSalesNo { get { return GetValue(() => CSalesNo); } set { SetValue(() => CSalesNo, value); OnPropertyChanged("CSalesNo"); } }
        public decimal ActualSST { get { return GetValue(() => ActualSST); } set { SetValue(() => ActualSST, value); OnPropertyChanged("ActualSST"); } }
        public decimal PaidSST { get { return GetValue(() => PaidSST); } set { SetValue(() => PaidSST, value); OnPropertyChanged("PaidSST"); } }
        public decimal ATotal { get { return GetValue(() => ATotal); } set { SetValue(() => ATotal, value); OnPropertyChanged("ATotal"); } }
        public decimal PTotal { get { return GetValue(() => PTotal); } set { SetValue(() => PTotal, value); OnPropertyChanged("PTotal"); } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Close": CloseWind(); break;
                    case "Yes": SaveData(); break;
                    case "No": { this.CloseWind(); }; break;
                    case "Reprint Receipt": Print(); break;
                    default: break;
                    
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void Print()
        {

            MessageBoxResult MsgRes = MessageBox.Show("You want to print Cash Receipt?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (MsgRes == MessageBoxResult.Yes)
            {
                Receipt(CSalesNo);
            }

        }

        public void PaymentModeComboboxload()
        {
            try
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GDDesc FROM tblGeneralDet A" +
                        $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                        $" WHERE RTRIM(B.GenDesc) = 'PAYMENT MODE' and A.GDDesc = 'CASH' ORDER BY A.GDDesc");

                    if (dt.Rows.Count > 0)
                    {
                        PaymentMode.Clear();
                        for (int i = 0; i < dt.Rows.Count; ++i)
                        {
                            string abc = dt.Rows[i]["GDDesc"].ToString();
                            PaymentMode.Add(new PaymentModeModel { PaymentModeId = abc, PaymentModeName = abc });
                        }
                        SelectedPaymentModeIndex = dt.Rows.Count - 1;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("PaymentModee Combobox load function:" + Ex.ToString()); }
        }


        private void Receipt(string CSNo)
        {
            DataTable DTCompany = new DataTable();
            DataTable DTMas = new DataTable();
            DataTable DTTran = new DataTable();
            DataTable DTHelp = new DataTable();

            try
            {
                if (CSNo.Trim() != "")
                {
                    //string CodeConsignor = "";

                    decimal limitkg = Convert.ToDecimal(30.009);

                    DTMas = GetDataTable($"SELECT num.CNNum, gen.CSalesNo, gen.CSalesDate, num.VolWeight, num.Weight, num.DestCode, gen.Price, gen.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, gen.StaffDisAmt, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount, gen.ShipmentTotal, gen.TaxTitle, gen.TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate FROM tblCashSales gen" +
                        $" LEFT JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo" +
                        $" WHERE gen.CSalesNo = '{CSNo}' AND gen.Status = 1 AND num.Status = 1");

                    SqlDataReader sqldr = GetDataReader($"SELECT TOP 1 a.CSalesDate FROM tblCashSales a WHERE a.CSalesNo = '{CSNo}'");
                    DateTime fdate = new DateTime();
                    if (sqldr.HasRows) 
                    {
                        if (sqldr.Read())
                        {
                            fdate = Convert.ToDateTime(sqldr["CSalesDate"]);
                        }
                        
                    }sqldr.Close();

                    fdate = fdate.Date;
                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,''), Address3 = ISNULL(a.Address3,''), City = ISNULL(a.City,''), State = ISNULL(a.State,''), Country = ISNULL(a.Country,''), PostalCode = ISNULL(a.ZipCode,''), PhoneNo = ISNULL(a.PhoneNum,'') , FaxNo = ISNULL(a.Fax,'') , EMail = ISNULL(a.EMail,''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a WHERE a.CompCode like '{MyCompanyCode.Trim()}' AND a.Status = 1");
                    // AND ((a.FromDate <= '{fdate}' AND a.ToDate >= '{tdate}') OR (a.FromDate <= '{fdate}' AND a.ToDate = '{null}'))
                    //var querycompany = (from a in CSalesEntity.tblStationProfiles where a.CompCode.Contains(MyCompanyCode.Trim()) && a.Status == true && ((a.FromDate <= fdate && a.ToDate >= tdate) || (a.FromDate <= fdate && a.ToDate == null)) select new { a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = a.Address1 ?? "", Address2 = a.Address2 ?? "", Address3 = a.Address3 ?? "", City = a.City ?? "", State = a.State ?? "", Country = a.Country ?? "", PostalCode = a.ZipCode ?? "", PhoneNo = a.PhoneNum ?? "", FaxNo = a.Fax ?? "", EMail = a.EMail ?? "", a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status }).ToList();
                    //DTCompany = LINQToDataTable(querycompany);

                    if (DTCompany.Rows.Count <= 0)
                    {
                        DTCompany = GetDataTable($"SELECT  a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,''), Address3 = ISNULL(a.Address3,''), City = ISNULL(a.City,''), State = ISNULL(a.State,''), Country = ISNULL(a.Country,''), PostalCode = ISNULL(a.ZipCode,''), PhoneNo = ISNULL(a.PhoneNum,'') , FaxNo = ISNULL(a.Fax,'') , EMail = ISNULL(a.EMail,'') , a.Status FROM tblStationProfile a WHERE a.CompCode LIKE '%{MyCompanyCode.Trim()}%'");
                    
                    }

                    FrmReportViewer RptViewerGen = new FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompany, "CashSalesDetails", DTMas, "Consignor", DTTran, "", DTHelp, "RPTPrihatin.rdlc", "", RptViewerGen.RptViewer, true, false);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
                else { MessageBox.Show("Invalid Cash sales number", "Warning"); }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Function : " + Ex.ToString()); }
        }



        private void SaveData()
        {
            try
            {
                decimal higestWeight = 0;
                if (AWeight > AVW)
                {
                    higestWeight = AWeight;
                }
                else
                {
                    higestWeight = AVW;
                }

                
                string CSalesDate;
                Boolean print = false;
                Boolean updateCashSales = false, insertCashSales = false;
                //string rdr = ($"SELECT * FROM tblCashSales a join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo WHERE b.CNNum = '{AWBNumber}' and a.Status = 1 and b.Status = 1");

                SqlDataReader dr = GetDataReader($"SELECT * FROM tblCashSales a join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo WHERE b.CNNum = '{AWBNumber}' and a.Status = 1 and b.Status = 1");


                if (dr.HasRows)
                {
                    if (dr.Read())
                    {

                        CSalesDate = dr["CSalesDate"].ToString();
                        DateTime EndDayCSalesDate = Convert.ToDateTime(CSalesDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        DateTime now = DateTime.Now;

                        if (EndDayCSalesDate > now)
                        {
                            print = true;
                            updateCashSales = true;
                            CSalesNo = dr["CSalesNo"].ToString();
                        }
                        else
                        {
                            MessageBox.Show("This AWB has been used before");
                            print = false;
                        }

                    }
                    dr.Close();

                }
                else
                {
                    //CSalesNo = CheckNewCSalesNo(NewCSalesNo());
                    insertCashSales = true;
                    print = true;
                }



                //using (SqlConnection c = new SqlConnection(ConStr))
                //{
                //    using (SqlCommand cmd = new SqlCommand(rdr, c))
                //    {
                //        using (SqlDataReader dr = cmd.ExecuteReader())
                //        {
                //            if (dr.HasRows)
                //            {
                //                if (dr.Read())
                //                {

                //                    CSalesDate = dr["CSalesDate"].ToString();
                //                    DateTime EndDayCSalesDate = Convert.ToDateTime(CSalesDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                //                    DateTime now = DateTime.Now;

                //                    if (EndDayCSalesDate > now)
                //                    {
                //                        print = true;
                //                        updateCashSales = true;
                //                        CSalesNo = dr["CSalesNo"].ToString();
                //                    }
                //                    else
                //                    {
                //                        MessageBox.Show("This AWB has been used before");
                //                        print = false;
                //                    }

                //                }
                //                dr.Close();

                //            }
                //            else
                //            {
                //                CSalesNo = CheckNewCSalesNo(NewCSalesNo());
                //                insertCashSales = true;
                //                print = true;
                //            }
                //        }
                //    }
                //}

                if (insertCashSales)
                {
                    int CSId = ExecuteScalarQuery($"INSERT INTO tblCashSales (CSalesNo, CSalesDate, Price, ShipmentTotal, ShipmentGSTValue , StationaryTotal , IsInsuranced , InsShipmentValue , InsValue, InsGSTValue, InsuranceTotal, CSalesTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, OtherCharges, IsManual, DiscCode, DiscPercentage, DiscAmount, TaxTitle, TaxPercentage, RoundingAmt, Status,PaymentMode,PaymentRefNo,PaymentRemarks)" +
                        $" VALUES (FORMAT(NEXT VALUE FOR CSalesNo, '{CSalesNoPrefix}000000'), GETDATE(), '{ Diff}', '{Diff}', 0, 0, 0, 0, 0, 0, 0, '{Diff}', '', '', 0, '{LoginUserId}', GETDATE(), 0, GETDATE(), '0', 0, 0, 0, 0, '', 0, 0, 1, '{SelectedValuePaymentMode}','{PaymentRef}','')");

                    dr = GetDataReader($"SELECT CSalesNo FROM tblCashSales WHERE CSalesID = {CSId}");
                    CSalesNo = dr.Read() ? dr["CSalesNo"].ToString() : ""; dr.Close();

                    //string insert_tblCashSales = ($"INSERT INTO tblCashSales (CSalesNo, CSalesDate, DestCode, DestStation, ShipmentType, Pieces, Weight, VLength,VBreadth, VHeight, VolWeight , Price, ShipmentTotal, ShipmentGSTValue , StationaryTotal , IsInsuranced , InsShipmentValue , InsShipmentDesc , InsValue, InsGSTValue, InsuranceTotal, CSalesTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, OtherCharges, IsManual, DiscCode, DiscPercentage, DiscAmount, TaxTitle, TaxPercentage, RoundingAmt, RefNum , Status,PaymentMode,PaymentRefNo,PaymentRemarks)" +
                    //                        $"VALUES ('{CSalesNo}', GETDATE(), '{StnCode}',  '{StnCode}',  '{ShipmentType}', '{NoofPcs}', '{AWeight}', '{ALength}', '{AWidth}', '{AHeight}', '{AVW}', '{Diff}', '{Diff}','0','0', '0', '0', '0', '0', 0, '0', '{Diff}', null, null, '0', '{LoginUserId}', GETDATE(), 0, GETDATE(), '0','0', '', '0', '0', '0', '0', '0', null, 1, '{SelectedValuePaymentMode}','{PaymentRef}','{""}' )");


                    string insert_tblCSaleCNNum = ($"INSERT INTO tblCSaleCNNum(CSalesNo, Num, CNNum, MobRefNum, OriginCode, ConsignorCode, PostCodeId, DestCode, ConsigneeCode, ShipmentType, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, HighestWeight, Price, OtherCharges, TotalAmt, InsShipmentValue, InsShipmentDesc, InsRate, InsValue, StaffDiscRate, StaffDiscAmt, PromoCode, PromoRate, PromoDiscAmt, TaxCode, TaxRate, CUserID, CDate, Status, IsExported, ExportedDate) VALUES('{CSalesNo}', {AWBNum_num}, '{AWBNumber}', '', '{MyCompanyCode.Trim()}', '', 0, '', '', '{ShipmentType}', {NoofPcs}, {AWeight}, {ALength}, {AWidth}, {AHeight}, {AVW}, Case when {AVW} >= {AWeight} then {AVW} else {AWeight} end, {Diff}, 0, {Diff}, 0, '', 0, 0, 0, 0.00, '', 0, 0.00, '', 0.00, {LoginUserId}, GETDATE(), 1, 0, NULL)");
                    //executeQueryPrihatin(insert_tblCashSales);
                    //executeQueryPrihatin(insert_tblCSaleCNNum);
                    //ExecuteQuery(insert_tblCashSales);
                    ExecuteQuery(insert_tblCSaleCNNum);
                }

                if(updateCashSales)
                {
                    string update_tblCashSales = ($"UPDATE tblCashSales SET MUserID = {LoginUserId}, MDate = GETDATE(), Price = '{Diff}', CSalesTotal = '{Diff}', ShipmentTotal = '{Diff}' WHERE Status = 1 and CSalesNo In (Select top 1 CSalesNo from tblCSaleCNNum where CNNum = '{AWBNumber}' order by CDate desc)");
                    //executeQueryPrihatin(update_tblCashSales);
                    ExecuteQuery(update_tblCashSales);
                }

                if (print)
                {
                    Print();

                    FrmLodgeIn frm = new FrmLodgeIn();
                    LodgeInVM lodgeinVM = new LodgeInVM(frm, AWBNumber, higestWeight, ShipmentType, NoofPcs);

                    MyWind.btnYes.Visibility = Visibility.Hidden;
                    MyWind.btnNo.Visibility = Visibility.Hidden;
                }
                else
                {
                    FrmLodgeIn frm = new FrmLodgeIn();
                    LodgeInVM lodgeinVM = new LodgeInVM(frm);
                    this.CloseWind();
                }
                
            }
            catch (Exception ex)
            {
                error_log.errorlog("Prihatin execution error : " + ex.ToString());
            }

        }


        private void executeQueryPrihatin(string query)
        {
            using (SqlConnection c = new SqlConnection(ConStr))
            {
                using (SqlCommand cmd = new SqlCommand(query, c))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }



        private decimal SSTCalc(decimal price){price = price * 0.06m;return price;}

        private decimal TotalCalc(decimal price, decimal sst){decimal total =  price + sst;return total;}

        private string CheckNewCSalesNo(string CODE)
        {
            string result = CODE;
            string sub = "";
            if (!string.IsNullOrEmpty(MyCompanySubName))
            {
                if (MyCompanySubName.Trim().Length > 5)
                {
                    sub = MyCompanySubName.Trim().Substring(0, 5);
                }
                else
                {
                    if (MyCompanySubName.Trim().Length == 5)
                    {
                        sub = MyCompanySubName.Trim();
                    }
                    else if (MyCompanySubName.Trim().Length == 4)
                    {
                        sub = MyCompanySubName.Trim() + "0";
                    }
                    else if (MyCompanySubName.Trim().Length == 3)
                    {
                        sub = MyCompanySubName.Trim() + "00";
                    }
                    else if (MyCompanySubName.Trim().Length == 2)
                    {
                        sub = MyCompanyCode.Trim() + MyCompanySubName.Trim();
                    }
                    else if (MyCompanySubName.Trim().Length == 1)
                    {
                        sub = MyCompanyCode.Trim() + MyCompanySubName.Trim() + "0";
                    }
                    else
                    {
                        sub = MyCompanyCode.Trim() + "00";
                    }
                }
            }
            else { sub = MyCompanyCode.Trim() + "00"; }
            try
            {
                if (!string.IsNullOrEmpty(result))
                {
                    string StrDate = DateTime.Now.ToString("ddMMyyyy");
                Loop:
                    string CheckExistence = "";

                    using (SqlDataReader dr = GetDataReader($"Select top 1 CSalesID, CSalesNo from tblCashSales where CSalesNo = '{result.Trim()}' ORDER BY CDate desc"))
                    {
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                CheckExistence = dr["CSalesNo"].ToString();
                            }
                        }
                        dr.Close();
                    }

                    GC.Collect();

                    if (!string.IsNullOrEmpty(CheckExistence))
                    {
                        int l = CheckExistence.Length;
                        string intcode = l == 16 ? CheckExistence.Substring(10) : l == 17 ? CheckExistence.Substring(11) : CheckExistence.Substring(8);
                        int codeint = Convert.ToInt32(intcode != "" ? intcode : "0") + 1;
                        result = "CS" + MyCompanyCode.Trim() + sub.Trim() + codeint.ToString("000000");
                        CODE = result;
                        goto Loop;
                    }
                    else
                    {
                        result = CODE;
                    }
                }
                else
                {
                    result = NewCSalesNo();
                    CheckNewCSalesNo(result);
                }
                return result;
            }
            catch (Exception ex) { error_log.errorlog("Checking Cash Sales No function:" + ex.ToString()); return result; }
        }

        private string NewCSalesNo()
        {
            string result = "";
            try
            {
                string sub = MyCompanySubName.Trim();
                if (!string.IsNullOrEmpty(MyCompanySubName.Trim()))
                {
                    if (MyCompanySubName.Trim().Length > 5)
                    {
                        sub = MyCompanySubName.Trim().Substring(0, 5);
                    }
                    else
                    {
                        if (MyCompanySubName.Trim().Length == 5)
                        {
                            sub = MyCompanySubName.Trim();
                        }
                        else if (MyCompanySubName.Trim().Length == 4)
                        {
                            sub = MyCompanySubName.Trim() + "0";
                        }
                        else if (MyCompanySubName.Trim().Length == 3)
                        {
                            sub = MyCompanySubName.Trim() + "00";
                        }
                        else if (MyCompanySubName.Trim().Length == 2)
                        {
                            sub = MyCompanyCode.Trim() + MyCompanySubName.Trim();
                        }
                        else if (MyCompanySubName.Trim().Length == 1)
                        {
                            sub = MyCompanyCode.Trim() + MyCompanySubName.Trim() + "0";
                        }
                        else
                        {
                            sub = MyCompanyCode.Trim() + "00";
                        }
                    }
                }
                else
                {
                    sub = MyCompanyCode.Trim() + "00";
                }

                string MaxCode = "", StrDate = DateTime.Now.ToString("ddMMyyyy");
                using (SqlDataReader dr = GetDataReader("Select top 1 CSalesNo from tblCashSales ORDER BY CDate desc"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            MaxCode = dr["CSalesNo"].ToString();
                        }

                    }
                }

                if (MaxCode != null && MaxCode != "")
                {
                    //int l = MaxCode.Length;
                    //string intcode = l == 16 ? MaxCode.Substring(10) : l == 17 ? MaxCode.Substring(11) : MaxCode.Substring(8);

                    string intcode = MaxCode.Substring(MaxCode.Length - 8, 8);
                    int codeint = Convert.ToInt32(intcode != "" ? intcode : "0") + 1;
                    result = "CS" + MyCompanyCode.Trim() + sub.Trim() + codeint.ToString("000000");
                }
                else
                {
                    result = "CS" + MyCompanyCode.Trim() + sub.Trim() + 1.ToString("000000");
                }
                return result;
            }
            catch (Exception ex) { error_log.errorlog("NewCSalesNo generating function:" + ex.ToString()); return result; }
        }
    }
}
