﻿using SkynetCashSales.Model;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls.Primitives;
using System.Windows.Shapes;

using SkynetCashSales.View.Help;
using SkynetCashSales.General;
using System.Windows.Documents;
using Newtonsoft.Json.Converters;

using System.Linq;
using System.Text;
using System.Xml;
using System.IO;


namespace SkynetCashSales.ViewModel.Transactions
{
    public class ManageStationeryVM : AMGenFunction
    {
        private string MInvenRef = "";

        private static string errormsg = "";
        private static FrmManageStationery MyWind;
        private static bool ExitFlag = true;
        private static bool EditItem = false;
        private static bool mDashBoardFlag = false;
        private bool mOBEntryFlag = true;
        private bool mHFlag = false, mTmpFlag = false, mEmailFlag = false;

        private string mTranItemCode = "";
        private string mStkTranType = "";
        private string mSeqName = "";
        private string mPrefix = "";
        private string mCostCetre = "", mCashSalesAcNo = "";



        private Int32 mStkId = 0;

        private Int32 mMaxQty = 0, mMaxOrdQty = 0, mTranQty = 0, mTempQty = 0, mCStockQty = 0;

        public DataTable dtTranHistory, dtStockItem, dtFilterData, dtTemp;

        public ManageStationeryVM(FrmManageStationery frm)
        {
            MyWind = frm;

            FromDate = DateTime.Now.Date.ToString("dd-MMM-yyyy");
            ToDate = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("dd-MMM-yyyy");

            createDataTable();

            StationeryList = new ObservableCollection<StationeryModelI>();
            TranHistoryList = new ObservableCollection<TranHistoryModel>();
            TranItemList = new ObservableCollection<TranListModel>();
            StockTranTypeList = new ObservableCollection<StockTranTypeModel>();
            THistoryList = new ObservableCollection<THistoryModel>();

            ResetData("");
        }

        /// <Already removed Controls and its properties>
        /// btnView     btnViewContent      btnViewVisibility   View Tran
        /// btnQMenu1   btnMAddContent      btnMAddBG           btnMAddVisibility   button QMenu1
        /// btnQMenu2   btnMSaveContent     btnMSaveBG          btnMSaveVisibility  button QMenu2
        /// btnQMenu3   btnMViewContent     btnMViewBG          btnMViewVisibility  button QMenu3
        /// </Already removed Controls and its properties>

        public Boolean tabTranTab1Selected { get { return GetValue(() => tabTranTab1Selected); } set { SetValue(() => tabTranTab1Selected, value); OnPropertyChanged("tabTranTab1Selected"); LoadTabControl(); } }
        public Boolean tabTranTab2Selected { get { return GetValue(() => tabTranTab2Selected); } set { SetValue(() => tabTranTab2Selected, value); OnPropertyChanged("tabTranTab2Selected"); LoadTabControl(); } }

        private ObservableCollection<StationeryModelI> _StationeryList = new ObservableCollection<StationeryModelI>();
        public ObservableCollection<StationeryModelI> StationeryList { get { return _StationeryList; } set { _StationeryList = value; OnPropertyChanged("StationeryList"); } }

        private RelayCommand _stationaryimageclick;
        public RelayCommand StationeryItemAdd { get { if (_stationaryimageclick == null) _stationaryimageclick = new RelayCommand(Parameter => StationeryImageClick(Parameter)); return _stationaryimageclick; } }

        //////  DATA GRID ( INSTEAD OF LIST) to Show entered Stationery Items 
        private ObservableCollection<TranListModel> _TranItemlist = new ObservableCollection<TranListModel>();
        public ObservableCollection<TranListModel> TranItemList { get { return _TranItemlist; } set { _TranItemlist = value; } }

        private TranListModel _selectedlistitem = new TranListModel();
        //private TranListModel SelectedTranListItem { get { return _selectedlistitem; } set { _selectedlistitem = value; OnPropertyChanged("SelectedTranListItem"); } }
        private TranListModel SelectedTranListItem { get { return _selectedlistitem; } set { if (_selectedlistitem != value) { _selectedlistitem = value; OnPropertyChanged("SelectedTranListItem"); } } }

        //////  DG - dgTranHistory - Show Stationery History        TranHistoryModel
        private ObservableCollection<TranHistoryModel> _TranHistoryItem = new ObservableCollection<TranHistoryModel>();
        public ObservableCollection<TranHistoryModel> TranHistoryList { get { return _TranHistoryItem; } set { _TranHistoryItem = value; } }

        private TranHistoryModel _selectedItemInfo = new TranHistoryModel();
        public TranHistoryModel SelectedItemInfo { get { return _selectedItemInfo; } set { if (_selectedItemInfo != value) { _selectedItemInfo = value; OnPropertyChanged("SelectedItemInfo"); } } }


        //////  DG - dgTHistory - Show Stationery History        THistoryModel
        private ObservableCollection<THistoryModel> _THistoryItem = new ObservableCollection<THistoryModel>();
        public ObservableCollection<THistoryModel> THistoryList { get { return _THistoryItem; } set { _THistoryItem = value; } }

        private THistoryModel _SelectedTHItemInfo = new THistoryModel();
        public THistoryModel SelectedTHItemInfo { get { return _SelectedTHItemInfo; } set { if (_SelectedTHItemInfo != value) { _SelectedTHItemInfo = value; OnPropertyChanged("SelectedTHItemInfo"); } } }


        //////  COMBO BOX - To GET TRANSACTION CATEGORy        StockTranTypeModel
        private ObservableCollection<StockTranTypeModel> _StockTranTypeList = new ObservableCollection<StockTranTypeModel>();
        public ObservableCollection<StockTranTypeModel> StockTranTypeList { get { return _StockTranTypeList; } set { _StockTranTypeList = value; OnPropertyChanged("StockTranTypeList"); } }

        private StockTranTypeModel _selectedStockTranType = new StockTranTypeModel();
        public StockTranTypeModel SelectedStockTranType { get { return _selectedStockTranType; } set { if (_selectedStockTranType != value) { _selectedStockTranType = value; OnPropertyChanged("SelectedStockTranType"); } } }
        public StockTranTypeModel SelectedTranDesc { get { return _selectedStockTranType; } set { _selectedStockTranType = value; OnPropertyChanged("SelectedTranDesc"); } }


        private int _selectedStockTranTypeIndex;
        public int SelectedStockTranTypeIndex { get { return _selectedStockTranTypeIndex; } set { _selectedStockTranTypeIndex = value; OnPropertyChanged("SelectedStockTranTypeIndex"); } }

        public string StaffCode { get { return GetValue(() => StaffCode); } set { SetValue(() => StaffCode, value); OnPropertyChanged("StaffCode"); } }
        public string StaffName { get { return GetValue(() => StaffName); } set { SetValue(() => StaffName, value); OnPropertyChanged("StaffName"); } }

        public string SideMenuButFGC1 { get { return GetValue(() => SideMenuButFGC1); } set { SetValue(() => SideMenuButFGC1, value); OnPropertyChanged("SideMenuButFGC1"); } }
        public string SideMenuButFGC2 { get { return GetValue(() => SideMenuButFGC2); } set { SetValue(() => SideMenuButFGC2, value); OnPropertyChanged("SideMenuButFGC2"); } }
        public string SideMenuButFGC3 { get { return GetValue(() => SideMenuButFGC3); } set { SetValue(() => SideMenuButFGC3, value); OnPropertyChanged("SideMenuButFGC3"); } }
        public string SideMenuButFGC4 { get { return GetValue(() => SideMenuButFGC4); } set { SetValue(() => SideMenuButFGC4, value); OnPropertyChanged("SideMenuButFGC4"); } }
        public string SideMenuButFGC5 { get { return GetValue(() => SideMenuButFGC5); } set { SetValue(() => SideMenuButFGC5, value); OnPropertyChanged("SideMenuButFGC5"); } }
        public string SideMenuButFGC6 { get { return GetValue(() => SideMenuButFGC6); } set { SetValue(() => SideMenuButFGC6, value); OnPropertyChanged("SideMenuButFGC6"); } }

        public string SideMenuButBGC1 { get { return GetValue(() => SideMenuButBGC1); } set { SetValue(() => SideMenuButBGC1, value); OnPropertyChanged("SideMenuButBGC1"); } }
        public string SideMenuButBGC2 { get { return GetValue(() => SideMenuButBGC2); } set { SetValue(() => SideMenuButBGC2, value); OnPropertyChanged("SideMenuButBGC2"); } }
        public string SideMenuButBGC3 { get { return GetValue(() => SideMenuButBGC3); } set { SetValue(() => SideMenuButBGC3, value); OnPropertyChanged("SideMenuButBGC3"); } }
        public string SideMenuButBGC4 { get { return GetValue(() => SideMenuButBGC4); } set { SetValue(() => SideMenuButBGC4, value); OnPropertyChanged("SideMenuButBGC4"); } }
        public string SideMenuButBGC5 { get { return GetValue(() => SideMenuButBGC5); } set { SetValue(() => SideMenuButBGC5, value); OnPropertyChanged("SideMenuButBGC5"); } }
        public string SideMenuButBGC6 { get { return GetValue(() => SideMenuButBGC6); } set { SetValue(() => SideMenuButBGC6, value); OnPropertyChanged("SideMenuButBGC6"); } }

        public string tabTranVisibility { get { return GetValue(() => tabTranVisibility); } set { SetValue(() => tabTranVisibility, value); OnPropertyChanged("tabTranVisibility"); } }

        public string tabTranTab1Head { get { return GetValue(() => tabTranTab1Head); } set { SetValue(() => tabTranTab1Head, value); OnPropertyChanged("tabTranTab1Head"); } }
        public string tabTranTab2Head { get { return GetValue(() => tabTranTab2Head); } set { SetValue(() => tabTranTab2Head, value); OnPropertyChanged("tabTranTab2Head"); } }
        public string tabTranTab2Visibility { get { return GetValue(() => tabTranTab2Visibility); } set { SetValue(() => tabTranTab2Visibility, value); OnPropertyChanged("tabTranTab2Visibility"); } }

        public string lblTranMainHeadVisibility { get { return GetValue(() => lblTranMainHeadVisibility); } set { SetValue(() => lblTranMainHeadVisibility, value); OnPropertyChanged("lblTranMainHeadVisibility"); } }
        public string lblTranMainHeadContent { get { return GetValue(() => lblTranMainHeadContent); } set { SetValue(() => lblTranMainHeadContent, value); OnPropertyChanged("lblTranMainHeadContent"); } }

        public string btnMAddFG { get { return GetValue(() => btnMAddFG); } set { SetValue(() => btnMAddFG, value); OnPropertyChanged("btnMAddFG"); } }
        public string btnMSaveFG { get { return GetValue(() => btnMSaveFG); } set { SetValue(() => btnMSaveFG, value); OnPropertyChanged("btnMSaveFG"); } }
        public string btnMViewFG { get { return GetValue(() => btnMViewFG); } set { SetValue(() => btnMViewFG, value); OnPropertyChanged("btnMViewFG"); } }


        public string bdrTranInputVisibility { get { return GetValue(() => bdrTranInputVisibility); } set { SetValue(() => bdrTranInputVisibility, value); OnPropertyChanged("bdrTranInputVisibility"); } }
        public string bdrTranListVisibility { get { return GetValue(() => bdrTranListVisibility); } set { SetValue(() => bdrTranListVisibility, value); OnPropertyChanged("bdrTranListVisibility"); } }

        public string lblTranRefHeadContent { get { return GetValue(() => lblTranRefHeadContent); } set { SetValue(() => lblTranRefHeadContent, value); OnPropertyChanged("lblTranRefHeadContent"); } }
        public string lblTranRemarksContent { get { return GetValue(() => lblTranRemarksContent); } set { SetValue(() => lblTranRemarksContent, value); OnPropertyChanged("lblTranRemarksContent"); } }
        public string lblTranRefDateContent { get { return GetValue(() => lblTranRefDateContent); } set { SetValue(() => lblTranRefDateContent, value); OnPropertyChanged("lblTranRefDateContent"); } }
        public string lblTranItemNameContent { get { return GetValue(() => lblTranItemNameContent); } set { SetValue(() => lblTranItemNameContent, value); OnPropertyChanged("lblTranItemNameContent"); } }
        public string lblTranItemQtyContent { get { return GetValue(() => lblTranItemQtyContent); } set { SetValue(() => lblTranItemQtyContent, value); OnPropertyChanged("lblTranItemQtyContent"); } }
        public string lblTranListRefHeadContent { get { return GetValue(() => lblTranListRefHeadContent); } set { SetValue(() => lblTranListRefHeadContent, value); OnPropertyChanged("lblTranListRefHeadContent"); } }
        public string lblUOMContent { get { return GetValue(() => lblUOMContent); } set { SetValue(() => lblUOMContent, value); OnPropertyChanged("lblUOMContent"); } }
        public string lblSysStockQtyContent { get { return GetValue(() => lblSysStockQtyContent); } set { SetValue(() => lblSysStockQtyContent, value); OnPropertyChanged("lblSysStockQtyContent"); } }
        public string lblReOrderLevelContent { get { return GetValue(() => lblReOrderLevelContent); } set { SetValue(() => lblReOrderLevelContent, value); OnPropertyChanged("lblReOrderLevelContent"); } }
        public string lblMaxLimitContent { get { return GetValue(() => lblMaxLimitContent); } set { SetValue(() => lblMaxLimitContent, value); OnPropertyChanged("lblMaxLimitContent"); } }
        public string lblCMaxOrderLimitContent { get { return GetValue(() => lblCMaxOrderLimitContent); } set { SetValue(() => lblCMaxOrderLimitContent, value); OnPropertyChanged("lblCMaxOrderLimitContent"); } }

        public string lblTranRefHeadVisibility { get { return GetValue(() => lblTranRefHeadVisibility); } set { SetValue(() => lblTranRefHeadVisibility, value); OnPropertyChanged("lblTranRefHeadVisibility"); } }
        public string lblTranRemarksVisibility { get { return GetValue(() => lblTranRemarksVisibility); } set { SetValue(() => lblTranRemarksVisibility, value); OnPropertyChanged("lblTranRemarksVisibility"); } }
        public string lblTranRefDateVisibility { get { return GetValue(() => lblTranRefDateVisibility); } set { SetValue(() => lblTranRefDateVisibility, value); OnPropertyChanged("lblTranRefDateVisibility"); } }
        public string lblTranItemNameVisibility { get { return GetValue(() => lblTranItemNameVisibility); } set { SetValue(() => lblTranItemNameVisibility, value); OnPropertyChanged("lblTranItemNameVisibility"); } }
        public string lblTranItemQtyVisibility { get { return GetValue(() => lblTranItemQtyVisibility); } set { SetValue(() => lblTranItemQtyVisibility, value); OnPropertyChanged("lblTranItemQtyVisibility"); } }
        public string lblTranListRefHeadVisibility { get { return GetValue(() => lblTranListRefHeadVisibility); } set { SetValue(() => lblTranListRefHeadVisibility, value); OnPropertyChanged("lblTranListRefHeadVisibility"); } }
        public string lblUOMVisibility { get { return GetValue(() => lblUOMVisibility); } set { SetValue(() => lblUOMVisibility, value); OnPropertyChanged("lblUOMVisibility"); } }
        public string lblSysStockQtyVisibility { get { return GetValue(() => lblSysStockQtyVisibility); } set { SetValue(() => lblSysStockQtyVisibility, value); OnPropertyChanged("lblSysStockQtyVisibility"); } }
        public string lblReOrderLevelVisibility { get { return GetValue(() => lblReOrderLevelVisibility); } set { SetValue(() => lblReOrderLevelVisibility, value); OnPropertyChanged("lblReOrderLevelVisibility"); } }
        public string lblMaxLimitVisibility { get { return GetValue(() => lblMaxLimitVisibility); } set { SetValue(() => lblMaxLimitVisibility, value); OnPropertyChanged("lblMaxLimitVisibility"); } }
        public string lblCMaxOrderLimitVisibility { get { return GetValue(() => lblCMaxOrderLimitVisibility); } set { SetValue(() => lblCMaxOrderLimitVisibility, value); OnPropertyChanged("lblCMaxOrderLimitVisibility"); } }

        public string dtpTranRefDateVisibility { get { return GetValue(() => dtpTranRefDateVisibility); } set { SetValue(() => dtpTranRefDateVisibility, value); OnPropertyChanged("dtpTranRefDateVisibility"); } }

        public string txtTranItemNameVisisbility { get { return GetValue(() => txtTranItemNameVisisbility); } set { SetValue(() => txtTranItemNameVisisbility, value); OnPropertyChanged("txtTranItemNameVisisbility"); } }
        public string txtTranItemQtyVisisbility { get { return GetValue(() => txtTranItemQtyVisisbility); } set { SetValue(() => txtTranItemQtyVisisbility, value); OnPropertyChanged("txtTranItemQtyVisisbility"); } }
        public string txtTranRemarksVisisbility { get { return GetValue(() => txtTranRemarksVisisbility); } set { SetValue(() => txtTranRemarksVisisbility, value); OnPropertyChanged("txtTranRemarksVisisbility"); } }


        public Boolean txtTranItemNameEnabled { get { return GetValue(() => txtTranItemNameEnabled); } set { SetValue(() => txtTranItemNameEnabled, value); OnPropertyChanged("txtTranItemNameEnabled"); } }
        public Boolean txtTranRemarksEnabled { get { return GetValue(() => txtTranRemarksEnabled); } set { SetValue(() => txtTranRemarksEnabled, value); OnPropertyChanged("txtTranRemarksEnabled"); } }

        public string btnAddTranContent { get { return GetValue(() => btnAddTranContent); } set { SetValue(() => btnAddTranContent, value); OnPropertyChanged("btnAddTranContent"); } }
        public string btnAddTranVisibility { get { return GetValue(() => btnAddTranVisibility); } set { SetValue(() => btnAddTranVisibility, value); OnPropertyChanged("btnAddTranVisibility"); } }
        public Boolean btnAddTranEnable { get { return GetValue(() => btnAddTranEnable); } set { SetValue(() => btnAddTranEnable, value); OnPropertyChanged("btnAddTranEnable"); } }

        public string btnSaveTranContent { get { return GetValue(() => btnSaveTranContent); } set { SetValue(() => btnSaveTranContent, value); OnPropertyChanged("btnSaveTranContent"); } }
        public string btnSaveTranVisibility { get { return GetValue(() => btnSaveTranVisibility); } set { SetValue(() => btnSaveTranVisibility, value); OnPropertyChanged("btnSaveTranVisibility"); } }
        public Boolean btnSaveTranEnable { get { return GetValue(() => btnSaveTranEnable); } set { SetValue(() => btnSaveTranEnable, value); OnPropertyChanged("btnSaveTranEnable"); } }

        public string lvTranListVisibility { get { return GetValue(() => lvTranListVisibility); } set { SetValue(() => lvTranListVisibility, value); OnPropertyChanged("lvTranListVisibility"); } }
        public string dgTranListVisibility { get { return GetValue(() => dgTranListVisibility); } set { SetValue(() => dgTranListVisibility, value); OnPropertyChanged("dgTranListVisibility"); } }


        public string scvStationeryItemViewVisibility { get { return GetValue(() => scvStationeryItemViewVisibility); } set { SetValue(() => scvStationeryItemViewVisibility, value); OnPropertyChanged("scvStationeryItemViewVisibility"); } }
        public string lblStatinoryItemVisibility { get { return GetValue(() => lblStatinoryItemVisibility); } set { SetValue(() => lblStatinoryItemVisibility, value); OnPropertyChanged("lblStatinoryItemVisibility"); } }
        public string lblStatinoryItemContent { get { return GetValue(() => lblStatinoryItemContent); } set { SetValue(() => lblStatinoryItemContent, value); OnPropertyChanged("lblStatinoryItemContent"); } }


        public string TranReference { get { return GetValue(() => TranReference); } set { SetValue(() => TranReference, value); OnPropertyChanged("TranReference"); } }
        public string TranDate { get { return GetValue(() => TranDate); } set { SetValue(() => TranDate, value); OnPropertyChanged("TranDate"); } }
        public DateTime TranSelectDate { get { return GetValue(() => TranSelectDate); } set { SetValue(() => TranSelectDate, value); OnPropertyChanged("TranSelectDate"); } }
        public string StationeryItem { get { return GetValue(() => StationeryItem); } set { SetValue(() => StationeryItem, value); OnPropertyChanged("StationeryItem"); } }
        public int TranQty { get { return GetValue(() => TranQty); } set { SetValue(() => TranQty, value); OnPropertyChanged("TranQty"); EnableDisableControl("", TranQty); } }
        public string txtTranRemarks { get { return GetValue(() => txtTranRemarks); } set { SetValue(() => txtTranRemarks, value); OnPropertyChanged("txtTranRemarks"); } }

        public string cnvTranInfoDispBG { get { return GetValue(() => cnvTranInfoDispBG); } set { SetValue(() => cnvTranInfoDispBG, value); OnPropertyChanged("cnvTranInfoDispBG"); } }
        public string cnvTranInfoDispVisibility { get { return GetValue(() => cnvTranInfoDispVisibility); } set { SetValue(() => cnvTranInfoDispVisibility, value); OnPropertyChanged("cnvTranInfoDispVisibility"); } }

        public string cnvTranHistoryVisibility { get { return GetValue(() => cnvTranHistoryVisibility); } set { SetValue(() => cnvTranHistoryVisibility, value); OnPropertyChanged("cnvTranHistoryVisibility"); } }

        public string lblTranViewHeadVisibility { get { return GetValue(() => lblTranViewHeadVisibility); } set { SetValue(() => lblTranViewHeadVisibility, value); OnPropertyChanged("lblTranViewHeadVisibility"); } }
        public string gdTranHistoryVisibility { get { return GetValue(() => gdTranHistoryVisibility); } set { SetValue(() => gdTranHistoryVisibility, value); OnPropertyChanged("gdTranHistoryVisibility"); } }
        public string cnvTranHistoryBG { get { return GetValue(() => cnvTranHistoryBG); } set { SetValue(() => cnvTranHistoryBG, value); OnPropertyChanged("cnvTranHistoryBG"); } }


        public string lblTranHistoryCatVisibility { get { return GetValue(() => lblTranHistoryCatVisibility); } set { SetValue(() => lblTranHistoryCatVisibility, value); OnPropertyChanged("lblTranHistoryCatVisibility"); } }
        public string cbStockTranTypeVisibility { get { return GetValue(() => cbStockTranTypeVisibility); } set { SetValue(() => cbStockTranTypeVisibility, value); OnPropertyChanged("cbStockTranTypeVisibility"); } }
        public string lblTranHistoryFDateVisibility { get { return GetValue(() => lblTranHistoryFDateVisibility); } set { SetValue(() => lblTranHistoryFDateVisibility, value); OnPropertyChanged("lblTranHistoryFDateVisibility"); } }
        public string dtpTranHistoryFDateVisibility { get { return GetValue(() => dtpTranHistoryFDateVisibility); } set { SetValue(() => dtpTranHistoryFDateVisibility, value); OnPropertyChanged("dtpTranHistoryFDateVisibility"); } }
        public string lblTranHistoryTDateVisibility { get { return GetValue(() => lblTranHistoryTDateVisibility); } set { SetValue(() => lblTranHistoryTDateVisibility, value); OnPropertyChanged("lblTranHistoryTDateVisibility"); } }
        public string dtpTranHistoryTDateVisibility { get { return GetValue(() => dtpTranHistoryTDateVisibility); } set { SetValue(() => dtpTranHistoryTDateVisibility, value); OnPropertyChanged("dtpTranHistoryTDateVisibility"); } }
        public string btnShowVisibility { get { return GetValue(() => btnShowVisibility); } set { SetValue(() => btnShowVisibility, value); OnPropertyChanged("btnShowVisibility"); } }
        public string btnPrintVisibility { get { return GetValue(() => btnPrintVisibility); } set { SetValue(() => btnPrintVisibility, value); OnPropertyChanged("btnPrintVisibility"); } }

        public string lblTranViewHeadContent { get { return GetValue(() => lblTranViewHeadContent); } set { SetValue(() => lblTranViewHeadContent, value); OnPropertyChanged("lblTranViewHeadContent"); } }
        public string lblTranHistoryCatContent { get { return GetValue(() => lblTranHistoryCatContent); } set { SetValue(() => lblTranHistoryCatContent, value); OnPropertyChanged("lblTranHistoryCatContent"); } }
        public string lblTranHistoryFDateContent { get { return GetValue(() => lblTranHistoryFDateContent); } set { SetValue(() => lblTranHistoryFDateContent, value); OnPropertyChanged("lblTranHistoryFDateContent"); } }
        public string lblTranHistoryTDateContent { get { return GetValue(() => lblTranHistoryTDateContent); } set { SetValue(() => lblTranHistoryTDateContent, value); OnPropertyChanged("lblTranHistoryTDateContent"); } }
        public string btnShowContent { get { return GetValue(() => btnShowContent); } set { SetValue(() => btnShowContent, value); OnPropertyChanged("btnShowContent"); } }
        public string btnPrintContent { get { return GetValue(() => btnPrintContent); } set { SetValue(() => btnPrintContent, value); OnPropertyChanged("btnPrintContent"); } }


        public string cnvTranDispVisibility { get { return GetValue(() => cnvTranDispVisibility); } set { SetValue(() => cnvTranDispVisibility, value); OnPropertyChanged("cnvTranDispVisibility"); } }
        public string cnvTHDispVisibility { get { return GetValue(() => cnvTHDispVisibility); } set { SetValue(() => cnvTHDispVisibility, value); OnPropertyChanged("cnvTHDispVisibility"); } }

        public string gdTHistoryVisibility { get { return GetValue(() => gdTHistoryVisibility); } set { SetValue(() => gdTHistoryVisibility, value); OnPropertyChanged("gdTHistoryVisibility"); } }

        public string lblTHistoryFDateVisibility { get { return GetValue(() => lblTHistoryFDateVisibility); } set { SetValue(() => lblTHistoryFDateVisibility, value); OnPropertyChanged("lblTHistoryFDateVisibility"); } }
        public string dtpTHistoryFDateVisibility { get { return GetValue(() => dtpTHistoryFDateVisibility); } set { SetValue(() => dtpTHistoryFDateVisibility, value); OnPropertyChanged("dtpTHistoryFDateVisibility"); } }
        public string lblTHistoryTDateVisibility { get { return GetValue(() => lblTHistoryTDateVisibility); } set { SetValue(() => lblTHistoryTDateVisibility, value); OnPropertyChanged("lblTHistoryTDateVisibility"); } }
        public string dtpTHistoryTDateVisibility { get { return GetValue(() => dtpTHistoryTDateVisibility); } set { SetValue(() => dtpTHistoryTDateVisibility, value); OnPropertyChanged("dtpTHistoryTDateVisibility"); } }
        public string btnTShowVisibility { get { return GetValue(() => btnTShowVisibility); } set { SetValue(() => btnTShowVisibility, value); OnPropertyChanged("btnTShowVisibility"); } }
        public string btnTPrintVisibility { get { return GetValue(() => btnTPrintVisibility); } set { SetValue(() => btnTPrintVisibility, value); OnPropertyChanged("btnTPrintVisibility"); } }

        public string lblTHistoryFDateContent { get { return GetValue(() => lblTHistoryFDateContent); } set { SetValue(() => lblTHistoryFDateContent, value); OnPropertyChanged("lblTHistoryFDateContent"); } }
        public string lblTHistoryTDateContent { get { return GetValue(() => lblTHistoryTDateContent); } set { SetValue(() => lblTHistoryTDateContent, value); OnPropertyChanged("lblTHistoryTDateContent"); } }
        public string btnTShowContent { get { return GetValue(() => btnTShowContent); } set { SetValue(() => btnTShowContent, value); OnPropertyChanged("btnTShowContent"); } }
        public string btnTPrintContent { get { return GetValue(() => btnTPrintContent); } set { SetValue(() => btnTPrintContent, value); OnPropertyChanged("btnTPrintContent"); } }

        public int lblTHistoryFDateLeft { get { return GetValue(() => lblTHistoryFDateLeft); } set { SetValue(() => lblTHistoryFDateLeft, value); OnPropertyChanged("lblTHistoryFDateContent"); } }

        private string _FromDate, _ToDate;
        public string FromDate { get { return _FromDate; } set { _FromDate = value; OnPropertyChanged("FromDate"); } }
        public string ToDate { get { return _ToDate; } set { _ToDate = value; OnPropertyChanged("ToDate"); } }


        private ICommand _MenuGen;
        public ICommand MenuGen { get { if (_MenuGen == null) { _MenuGen = new RelayCommand(Parameter => ExecuteMenuGen(Parameter)); } return _MenuGen; } }

        private ICommand _TextGen;
        public ICommand TextGen { get { if (_TextGen == null) { _TextGen = new RelayCommand(Parameter => ExecuteTextGen(Parameter)); } return _TextGen; } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        private ICommand _HLinkAction;
        public ICommand HLinkAction { get { _HLinkAction = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _HLinkAction; } }


        private ICommand _HLinkAction_Click;
        public ICommand HLinkAction_Click { get { _HLinkAction_Click = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _HLinkAction_Click; } }



        /////   All Menu Execution  -----> START
        private void ExecuteMenuGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                mTranItemCode = "";
                //"Purchase"  "Receive Stock"  "Adjust Stock"  "Opening Balance"   "Stock Status"
                switch (Obj.ToString())
                {

                    /////   All Menu cases  -----> START
                    case "Purchase":
                        {
                            ResetData("Purchase");
                        }
                        break;
                    case "Receive Stock":  // Stock IN - Receive Item Entry
                        {
                            ResetData("Receive Stock");
                        }
                        break;
                    //case "Remove Item":  // Stock IN - Receive Item Entry
                    //    {
                    //        RemoveItem("Remove Item");
                    //    }
                    //    break;
                    case "Adjust Stock":  // Stock IN - Receive Item Entry
                        {
                            if (LoginUserAuthentication != "User") { ResetData("Adjust Stock");}
                            else 
                            {
                                UerAccess(); ResetData("");
                            }
                        }
                        break;
                    case "Opening Balance":  // for ob
                        {
                            if (LoginUserAuthentication != "User") { ResetData("Opening Balance"); }
                            else
                            {
                                UerAccess(); ResetData("");
                            }
                        }
                        break;
                    case "Stock Status":  // To show current stock status
                        {
                            //mTranItemCode = "";
                            ResetData("Stock Status");
                        }
                        break;
                    case "Home":  // To show Manage stationery Homed screen or dash board
                        {
                            ResetData("");
                        }
                        break;
                    case "New":
                        {
                            if (!ExitFlag)
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to reset this screen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes) { ResetData(""); }
                            }
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Menu execution : " + Ex.Message); }
        }
        /////   All Menu Execution  -----> END

        /////   All TextBox Input Excution  -----> START
        private void ExecuteTextGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {

                switch (Obj.ToString())
                {
                    case "Input TranQty":  // Purchase request Quantity / Stock IN - Received Quantity /  OB Quantity / Stock Adjust - Excess or Stock Quantity
                        {
                            if (MInvenRef != "Adjust Stock") { AddTranQtytoList(); }
                        }
                        break;
                    case "Input TranRemarks":  // Purchase request Quantity / Stock IN - Received Quantity /  OB Quantity / Stock Adjust - Excess or Stock Quantity
                        {
                            AddTranQtytoList();
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Input excution : " + Ex.Message); }
        }
        /////   All TextBox Input Excution  -----> END


        /////   All Command button Excution  -----> START
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Button Tran Save":
                        {
                            IntegrateData("button Tran Save");
                        }
                        break;
                    case "SaveTran":
                        {
                            ProcessSendEmail("");
                        }
                        break;
                    case "Button Tran ":
                        {
                        }
                        break;

                    /////   All Transaction buttons
                    case "":
                        {
                            IntegrateData("TransactionApprove");
                        }
                        break;
                    case "TShow Tran":
                        {
                            LoadTranHistoryList("Show Tran", "Stock History");
                        }
                        break;
                    case "Show Tran":
                        {
                            LoadTranHistoryList("Show Tran", "Stock History");
                        }
                        break;
                    case "TPrint Tran":
                        {
                            PrintStokReport("Stock History");
                        }
                        break;
                    case "Print Tran":
                        {
                            PrintStokReport("Stock Status");
                        }
                        break;
                    case "Clear":
                        {
                            ResetData("");
                        }
                        break;
                    case "Close":
                        {
                            if (!ExitFlag)
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to close this screen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes) { ResetData(""); }
                            }
                            else
                            { this.CloseWind(); }
                        }
                        break;
                    case "GridClickRemove":
                        {
                            //  if TranStatus != 'OC' then close the purchase order without receiving Pending quantity 
                            //  Upadte cansel quantity as Pending quantity status closed by user  ---->   
                            if (MInvenRef == "")
                            {
                                UpdateCancelQty();
                            }
                        }
                        break;
                    case "GridClickAction":
                        {
                            if (MInvenRef == "")
                            {
                                mTranItemCode = SelectedItemInfo.ItemCode;
                                mStkId = SelectedItemInfo.StkTranID;
                                if (mTranItemCode != "")
                                {
                                    mStkTranType = SelectedItemInfo.TranType;
                                    if (MInvenRef == "")
                                    {
                                        mEmailFlag = false;
                                        if (SelectedItemInfo.Action == "Order Now")
                                        {
                                            ResetData("Purchase");
                                        }
                                        else if (SelectedItemInfo.Action == "E-Mail")
                                        {
                                            mEmailFlag = true;
                                            ResetData("Purchase");
                                        }
                                        else if (SelectedItemInfo.Action == "Receive Stock")
                                        {
                                            ResetData("Receive Stock");
                                        }
                                        mDashBoardFlag = true;
                                        if (mEmailFlag == false) { LoadStationery(mTranItemCode); }
                                    }
                                    else
                                    {
                                        mHFlag = true;
                                        LoadTranHistoryList("View Tran", "");
                                        mHFlag = false;
                                        mTranItemCode = "";
                                    }
                                }
                            }
                        }
                        break;
                    case "HLinkAction_Click":
                        {
                            mTranItemCode = SelectedItemInfo.ItemCode;
                        }
                        break;
                    case "HLinkRemove":
                        {
                            mTranItemCode = SelectedItemInfo.ItemCode;
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(" Command or Linl excution : " + Ex.Message); }
        }
        /////   All Command button Excution  -----> END


        private void ResetData(string TranFlag)
        {
            TranReference = ""; StationeryItem = ""; TranQty = 0; txtTranRemarks = "";
            mMaxQty = 0; mMaxOrdQty = 0; mTranQty = 0; mTempQty = 0; mCStockQty = 0;

            TranSelectDate = DateTime.Now;

            TranItemList.Clear();
            TranHistoryList.Clear();
            THistoryList.Clear();

            try
            {
                if (TranFlag == "")
                {
                    LoadDashBoard("");
                }
                else
                {
                    LoadDashBoard(TranFlag);

                    tabTranVisibility = "Visible";
                    MyWind.tabTran.SelectedIndex = 0;

                    switch (TranFlag)
                    {
                        case "Home":
                            {
                                MInvenRef = "Purchase";
                                SideMenuButBGC1 = "LightSteelBlue"; SideMenuButBGC2 = ""; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightGray";
                                SideMenuButBGC5 = "LightGray"; SideMenuButBGC6 = "LightGray";
                                SideMenuButFGC1 = "Red"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Black";
                                SideMenuButFGC5 = "Black"; SideMenuButFGC6 = "Black";
                            }
                            break;
                        case "Purchase":
                            {

                                MInvenRef = "Purchase";

                                tabTranVisibility = "Visible"; cnvTranDispVisibility = "Visible";

                                bdrTranInputVisibility = "Visible"; bdrTranListVisibility = "Visible";
                                lblTranMainHeadContent = "Purchase"; lblTranMainHeadVisibility = "Visible";
                                tabTranTab2Visibility = "Visible";
                                tabTranTab1Head = "Purchase Order"; tabTranTab2Head = "Purchase Order History";

                                SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightSteelBlue"; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightGray"; SideMenuButBGC5 = "LightGray";
                                SideMenuButBGC6 = "LightGray";
                                SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Red"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Black"; SideMenuButFGC5 = "Black";
                                SideMenuButFGC6 = "Black";

                                LoadInterFace("Purchase Order", "");       //LoadInterFace("Purchase Order Request, "QMenu1");

                            }
                            break;
                        case "Receive Stock":
                            {

                                MInvenRef = "Stock in";

                                tabTranVisibility = "Visible"; cnvTranDispVisibility = "Visible";

                                bdrTranInputVisibility = "Visible"; bdrTranListVisibility = "Visible";
                                lblTranMainHeadContent = "Receive stock"; lblTranMainHeadVisibility = "Visible";
                                tabTranTab2Visibility = "Visible";
                                tabTranTab1Head = "Receive stock"; tabTranTab2Head = "Receive History";

                                SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightGray"; SideMenuButBGC3 = "LightSteelBlue"; SideMenuButBGC4 = "LightGray"; SideMenuButBGC5 = "LightGray";
                                SideMenuButBGC6 = "LightGray";
                                SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Red"; SideMenuButFGC4 = "Black"; SideMenuButFGC5 = "Black";
                                SideMenuButFGC6 = "Black";

                                LoadInterFace("Stock in", "");

                            }
                            break;
                        case "Adjust Stock":
                            {
                                MInvenRef = "Adjust Stock";

                                tabTranVisibility = "Visible"; cnvTranDispVisibility = "Visible";

                                bdrTranInputVisibility = "Visible"; bdrTranListVisibility = "Visible";
                                lblTranMainHeadContent = "Stock Adjust"; lblTranMainHeadVisibility = "Visible";
                                tabTranTab2Visibility = "Visible";
                                tabTranTab1Head = "Stock adjust"; tabTranTab2Head = "Stock adjust History";

                                SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightGray"; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightSteelBlue"; SideMenuButBGC5 = "LightGray";
                                SideMenuButBGC6 = "LightGray";
                                SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Red"; SideMenuButFGC5 = "Black";
                                SideMenuButFGC6 = "Black";

                                LoadInterFace("Adjust Stock", "");
                            }
                            break;
                        case "Opening Balance":
                            {
                                MInvenRef = "Opening Balance";

                                tabTranVisibility = "Visible"; cnvTranDispVisibility = "Visible";

                                bdrTranInputVisibility = "Visible"; bdrTranListVisibility = "Visible";
                                lblTranMainHeadContent = "Opening Balance"; lblTranMainHeadVisibility = "Visible";
                                tabTranTab2Visibility = "Collapsed";

                                tabTranTab1Head = "Opening stock"; tabTranTab2Head = "";

                                SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightGray"; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightGray"; SideMenuButBGC5 = "LightSteelBlue";
                                SideMenuButBGC6 = "LightGray";
                                SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Black"; SideMenuButFGC5 = "Red";
                                SideMenuButFGC6 = "Black";

                                LoadInterFace("Opening Balance", "");
                            }
                            break;
                        case "Stock Status":
                            {

                                MInvenRef = "Stock Status";

                                lblTranMainHeadVisibility = "Visible";
                                lblTranMainHeadContent = "Stock Status";

                                lblTranRefHeadVisibility = "Collapsed";

                                lblTranRemarksVisibility = "Collapsed";
                                lblTranRefDateVisibility = "Collapsed";
                                lblTranItemNameVisibility = "Collapsed"; lblTranItemQtyVisibility = "Collapsed";
                                lblSysStockQtyVisibility = "Collapsed"; lblSysStockQtyContent = "";
                                lblReOrderLevelVisibility = "Collapsed"; lblReOrderLevelContent = "";
                                lblMaxLimitVisibility = "Collapsed"; lblMaxLimitContent = "";
                                lblCMaxOrderLimitVisibility = "Collapsed"; lblCMaxOrderLimitContent = "";

                                lblTranListRefHeadVisibility = "Collapsed";
                                txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                                txtTranItemNameVisisbility = "Collapsed"; txtTranItemQtyVisisbility = "Collapsed";
                                dtpTranRefDateVisibility = "Collapsed";

                                btnAddTranVisibility = "Collapsed"; btnSaveTranVisibility = "Collapsed";
                                btnAddTranContent = ""; btnSaveTranContent = "";
                                lvTranListVisibility = "Collapsed";
                                dgTranListVisibility = "Collapsed";

                                lblStatinoryItemContent = ""; lblStatinoryItemVisibility = "Collapsed";
                                scvStationeryItemViewVisibility = "Collapsed";

                                SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightGray"; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightGray"; SideMenuButBGC5 = "LightGray";
                                SideMenuButBGC6 = "LightSteelBlue";
                                SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Black"; SideMenuButFGC5 = "Black";
                                SideMenuButFGC6 = "Red";

                                ClearWorkSapce("View Hist");

                                MyWind.cnvTranHistory.Margin = new Thickness(175, 50, 0, 0);
                                MyWind.dgTranHistory.Margin = new Thickness(5, 125, 0, 0); MyWind.dgTranHistory.Width = 870; MyWind.dgTranHistory.Height = 565;
                                MyWind.dgTranHistory.FontSize = 14;


                                LoadStockTranType("View Tran");
                                LoadTranHistoryList("View Tran", "Stock Status");

                            }
                            break;
                        case "Stock History":
                            {

                                MInvenRef = "Stock History";

                                lblTranMainHeadVisibility = "Visible";
                                lblTranMainHeadContent = "Stock History";

                                lblTranRefDateVisibility = "Collapsed";
                                lblTranItemNameVisibility = "Collapsed"; lblTranItemQtyVisibility = "Collapsed";
                                lblSysStockQtyVisibility = "Collapsed"; lblSysStockQtyContent = "";
                                lblReOrderLevelVisibility = "Collapsed"; lblReOrderLevelContent = "";
                                lblMaxLimitVisibility = "Collapsed"; lblMaxLimitContent = "";
                                lblCMaxOrderLimitVisibility = "Collapsed"; lblCMaxOrderLimitContent = "";

                                lblTranRemarksVisibility = "Collapsed";
                                lblTranListRefHeadVisibility = "Collapsed";
                                txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                                txtTranItemNameVisisbility = "Collapsed"; txtTranItemQtyVisisbility = "Collapsed";
                                dtpTranRefDateVisibility = "Collapsed";

                                btnAddTranVisibility = "Collapsed"; btnSaveTranVisibility = "Collapsed";
                                btnAddTranContent = ""; btnSaveTranContent = "";
                                lvTranListVisibility = "Collapsed";
                                dgTranListVisibility = "Collapsed";

                                lblStatinoryItemContent = ""; lblStatinoryItemVisibility = "Collapsed";
                                scvStationeryItemViewVisibility = "Collapsed";

                                ClearWorkSapce("View Hist");

                                MyWind.cnvTranHistory.Margin = new Thickness(175, 50, 0, 0);
                                MyWind.dgTranHistory.Margin = new Thickness(5, 125, 0, 0); MyWind.dgTranHistory.Width = 870; MyWind.dgTranHistory.Height = 565;


                                LoadStockTranType("View Tran");
                                LoadTranHistoryList("View Tran", "Stock Status");

                            }
                            break;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Reset Data : " + Ex.Message); }
        }

        private void LoadDashBoard(string mDashBoardOpt)
        {
            try
            {
                MInvenRef = "";

                tabTranVisibility = "Collapsed"; cnvTranDispVisibility = "Collapsed"; cnvTHDispVisibility = "Collapsed";

                lblTranMainHeadVisibility = "Collapsed";
                lblTranMainHeadContent = "";
                lblTranRefHeadVisibility = "Collapsed"; lblTranViewHeadVisibility = "Collapsed";
                lblTranHistoryCatVisibility = "Collapsed"; cbStockTranTypeVisibility = "Collapsed";
                lblTranHistoryFDateVisibility = "Collapsed"; dtpTranHistoryFDateVisibility = "Collapsed";
                lblTranHistoryTDateVisibility = "Collapsed"; dtpTranHistoryTDateVisibility = "Collapsed";
            
                lblTHistoryFDateVisibility = "Collapsed"; dtpTHistoryFDateVisibility = "Collapsed";
                lblTHistoryTDateVisibility = "Collapsed"; dtpTHistoryTDateVisibility = "Collapsed";
                btnTShowVisibility = "Collapsed"; btnTPrintVisibility = "Collapsed";

                ClearWorkSapce("");

                //      Alert for Reorder level
                if (mDashBoardOpt == "")
                {
                    LoadDashBoardAlert();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load dashboard : " + Ex.Message); }
        }


        private void ClearWorkSapce(string CFlag)
        {
            try
            { 
                cnvTranInfoDispVisibility = "Visible"; cnvTranInfoDispBG = "SmokeWhite";

                tabTranVisibility = "Collapsed";
                bdrTranInputVisibility = "Collapsed"; bdrTranListVisibility = "Collapsed";

                lblTranRefDateVisibility = "Collapsed"; lblTranItemNameVisibility = "Collapsed"; lblTranItemQtyVisibility = "Collapsed";
                lblUOMVisibility = "Collapsed"; lblSysStockQtyVisibility = "Collapsed";
                lblMaxLimitVisibility = "Collapsed"; lblReOrderLevelVisibility = "Collapsed";
                lblCMaxOrderLimitVisibility = "Collapsed";
                lblTranRemarksVisibility = "Collapsed";

                lblUOMContent = "";

                lblTranListRefHeadVisibility = "Collapsed";
                txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                txtTranItemNameVisisbility = "Collapsed"; txtTranItemQtyVisisbility = "Collapsed";
                dtpTranRefDateVisibility = "Collapsed";

                txtTranItemNameEnabled = false;

                btnAddTranVisibility = "Collapsed"; btnSaveTranVisibility = "Collapsed";
                btnAddTranContent = ""; btnSaveTranContent = "";

                lblStatinoryItemContent = ""; lblStatinoryItemVisibility = "Collapsed";
                scvStationeryItemViewVisibility = "Collapsed";

                lvTranListVisibility = "Collapsed";
                dgTranListVisibility = "Collapsed";


                if (CFlag == "")
                {
                    ////  LightGray(default) and selection --> LightPink LightCoral LightCyan LightSalmon  LightSeaGreen LightSteelBlue         -->
                    SideMenuButBGC1 = "LightGray"; SideMenuButBGC2 = "LightGray"; SideMenuButBGC3 = "LightGray"; SideMenuButBGC4 = "LightGray"; SideMenuButBGC5 = "LightGray";
                    SideMenuButFGC6 = "Black";
                    SideMenuButFGC1 = "Black"; SideMenuButFGC2 = "Black"; SideMenuButFGC3 = "Black"; SideMenuButFGC4 = "Black"; SideMenuButFGC5 = "Black";
                    SideMenuButFGC6 = "Black";
                }
                else
                {
                    lblTranRefHeadVisibility = "Collapsed";
                }

                btnAddTranEnable = false; btnSaveTranEnable = false;


                //now
                //FromDate = DateTime.Now.Date;
                //ToDate = FromDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                FromDate = DateTime.Now.Date.ToShortDateString();
                ToDate = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59).ToShortDateString();


                mStkTranType = ""; mSeqName = ""; mPrefix = "";

                MyWind.cnvTranHistory.Margin = new Thickness(175, 100, 0, 0); MyWind.cnvTranHistory.Width = 870; MyWind.cnvTranHistory.Width = 565;
                MyWind.dgTranHistory.Margin = new Thickness(5, 55, 0, 0); MyWind.dgTranHistory.Width = 860; MyWind.dgTranHistory.Height = 485;

                cnvTranHistoryVisibility = "Collapsed"; lblTranViewHeadVisibility = "Collapsed"; gdTranHistoryVisibility = "Collapsed";
                lblTranHistoryCatVisibility = "Collapsed"; cbStockTranTypeVisibility = "Collapsed";
                lblTranHistoryFDateVisibility = "Collapsed"; dtpTranHistoryFDateVisibility = "Collapsed";
                lblTranHistoryTDateVisibility = "Collapsed"; dtpTranHistoryTDateVisibility = "Collapsed";
                btnShowVisibility = "Collapsed"; btnPrintVisibility = "Collapsed";

                lblTHistoryFDateVisibility = "Collapsed"; dtpTHistoryFDateVisibility = "Collapsed";
                lblTHistoryTDateVisibility = "Collapsed"; dtpTHistoryTDateVisibility = "Collapsed";
                btnTShowVisibility = "Collapsed"; btnTPrintVisibility = "Collapsed";

                MyWind.dgTranHistory.BorderThickness = new Thickness(0);
            }
            catch (Exception Ex) { error_log.errorlog("Clear Workspace : " + Ex.Message); }
        }


        private void LoadDashBoardAlert()
        {

            tabTranVisibility = "Collapsed";
            try
            { 
                using (dtStockItem = new DataTable())
                {
                    //dtStockItem = GetDataTable($"SELECT 1 AS DispOrder,I.ItemCode,I.ItemDesc,I.Unit1,S.StkTranType,S.StkTranDate AS TranDate,S.StkTranQty,S.StkTranPendingQty,0 AS Closing, 0 AS ReOrder FROM tblItemMaster AS I JOIN tblStockTran AS S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.StkTranType = 'PR' AND S.StkTranPendingQty > 0 UNION SELECT 2 AS DispOrder, I.ItemCode,I.ItemDesc,I.Unit1,'CL' AS StkTranType, S.CreateDate AS TranDate,0 AS StkTranQty,0 AS StkTranPendingQty, S.Closing,I.ReOrder FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.Status = 1 AND Closing <= ReOrder ORDER BY DispOrder");
                    dtStockItem = GetDataTable($"SELECT 1 AS DispOrder,S.StkTranID,I.ItemCode,I.ItemDesc,I.Unit1,S.StkTranType,S.StkTranDate AS TranDate,S.StkTranQty,S.StkTranPendingQty,0 AS Closing,S.TranStatus,I.ReOrder, U.UserName FROM tblItemMaster AS I JOIN tblStockTran AS S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE I.Status = 1 AND S.StkTranType = 'PR' AND (S.TranStatus = 'PR' OR S.TranStatus = 'EM') AND S.StkTranPendingQty > 0 UNION SELECT 2 AS DispOrder,0 AS StkTranID, I.ItemCode,I.ItemDesc,I.Unit1,'CL' AS StkTranType, S.CreateDate AS TranDate,0 AS StkTranQty,0 AS StkTranPendingQty, S.Closing,'' AS TranStatus,I.ReOrder, '' AS UserName FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.Status = 1 AND Closing <= ReOrder AND I.ItemCode NOT IN (SELECT DISTINCT I.ItemCode FROM tblItemMaster AS I JOIN tblStockTran AS S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.StkTranType = 'PR' AND (S.TranStatus = 'PR' OR S.TranStatus = 'EM') AND S.StkTranPendingQty > 0) ORDER BY DispOrder,S.StkTranDate");
                    if (dtStockItem.Rows.Count > 0)
                    {

                        TranHistoryList.Clear();
                        dtTranHistory = new DataTable();

                        var colheadstyle = new Style(typeof(DataGridColumnHeader));
                        var chstyleLeft = new Style(typeof(DataGridColumnHeader));
                        var chstyleRight = new Style(typeof(DataGridColumnHeader));
                        var chstyleCenter = new Style(typeof(DataGridColumnHeader));
                        var rowcellstyleRight = new Style(typeof(DataGridCell));
                        var rowcellstyleCenter = new Style(typeof(DataGridCell));
                        //var rowcellstyleCenterHL = new Style(typeof(DataGridCell));
                        var ButtonHyperLink = new Style(typeof(DataGridCell));


                        MyWind.dgTranHistory.Columns.Clear();

                        //colheadstyle.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleCenter") as Style;
                        colheadstyle.Setters.Add(new Setter(ToolTipService.ToolTipProperty, "my tooltop"));

                        chstyleLeft.BasedOn = MyWind.TryFindResource("DataGridColumnHeaderStyleLeft") as Style;
                        chstyleRight.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicRight") as Style;
                        chstyleCenter.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicCenter") as Style;
                        rowcellstyleRight.BasedOn = MyWind.TryFindResource("DataGridCellRightAlignStyle") as Style;
                        rowcellstyleCenter.BasedOn = MyWind.TryFindResource("DataGridCellCenterAlignStyle") as Style;
                        //rowcellstyleCenterHL.BasedOn = MyWind.TryFindResource("DataGridCellCenterAlignHyperLink") as Style;
                        ButtonHyperLink.BasedOn = MyWind.TryFindResource("DataGridCellButtonHyperLink") as Style;

                        MyWind.dgTranHistory.ColumnHeaderStyle = colheadstyle;

                        MyWind.cnvTranHistory.Margin = new Thickness(170, 100, 0, 0); MyWind.cnvTranHistory.Width = 680; MyWind.cnvTranHistory.Height = 510;

                        MyWind.dgTranHistory.Margin = new Thickness(0, 30, 0, 0); MyWind.dgTranHistory.Width = 875; MyWind.dgTranHistory.Height = 470;

                        MyWind.dgTranHistory.BorderBrush = System.Windows.Media.Brushes.IndianRed;

                        MyWind.dgTranHistory.FontSize = 13;

                        MyWind.dgTranHistory.SelectionUnit = DataGridSelectionUnit.FullRow;

                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Order Date", Binding = new Binding("StrDate"), Width = 140, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Order by", Binding = new Binding("UserName"), Width = 60, HeaderStyle = chstyleLeft });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "StkTranID", Binding = new Binding("StkTranID"), MaxWidth = 0 });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Transation Type(H)", Binding = new Binding("TranType"), MaxWidth = 0 });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 165, HeaderStyle = chstyleLeft });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 45, HeaderStyle = chstyleLeft });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Ordered Quantity", Binding = new Binding("Purchase"), Width = 65, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Pending to Receive", Binding = new Binding("PendingQty"), Width = 78, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 60, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                        MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), Width = 58, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });

                        var style = new Style(typeof(TextBlock));
                        style.Setters.Add(new EventSetter(Hyperlink.ClickEvent, (RoutedEventHandler)EventSetter_OnHandler));
                        MyWind.dgTranHistory.Columns.Add(new DataGridHyperlinkColumn { Header = "Action", Binding = new Binding("Action"), Width = 85, HeaderStyle = chstyleCenter, CellStyle = ButtonHyperLink, ElementStyle = style });

                        style = new Style(typeof(TextBlock));
                        style.Setters.Add(new EventSetter(Hyperlink.ClickEvent, (RoutedEventHandler)EventSetterA_OnHandler));
                        MyWind.dgTranHistory.Columns.Add(new DataGridHyperlinkColumn { Header = "Cancel Pending Qty", Binding = new Binding("Action1"), Width = 60, HeaderStyle = chstyleCenter, CellStyle = ButtonHyperLink, ElementStyle = style });


                        #region --Old Grid Insert for Alert 
                        //for (int i = 0; i < dtStockItem.Rows.Count; i++)
                        //{
                        //    if (dtStockItem.Rows[i]["StkTranType"].ToString() == "PR")
                        //    {
                        //        TranHistoryList.Add(new TranHistoryModel
                        //        {
                        //            StrDate = DateTime.Parse(dtStockItem.Rows[i]["TranDate"].ToString()).ToString("dd-MMM-yyyy hh:mm tt"),
                        //            UserName = dtStockItem.Rows[i]["UserName"].ToString(),
                        //            ItemCode = dtStockItem.Rows[i]["ItemCode"].ToString(),
                        //            ItemDesc = dtStockItem.Rows[i]["ItemDesc"].ToString(),
                        //            Unit1 = dtStockItem.Rows[i]["Unit1"].ToString(),
                        //            Purchase = Convert.ToInt32(dtStockItem.Rows[i]["StkTranQty"]),
                        //            PendingQty = Convert.ToInt32(dtStockItem.Rows[i]["StkTranPendingQty"]),
                        //            Action = "Receive Stock",
                        //            Action1 = "Remove",
                        //            TranType = dtStockItem.Rows[i]["StkTranType"].ToString(),
                        //        });
                        //    }
                        //    else
                        //    {
                        //        TranHistoryList.Add(new TranHistoryModel
                        //        {
                        //            StrDate = "-",
                        //            UserName = "",
                        //            ItemCode = dtStockItem.Rows[i]["ItemCode"].ToString(),
                        //            ItemDesc = dtStockItem.Rows[i]["ItemDesc"].ToString(),
                        //            Unit1 = dtStockItem.Rows[i]["Unit1"].ToString(),
                        //            Closing = Convert.ToInt32(dtStockItem.Rows[i]["Closing"]),
                        //            ReOrderLevel = Convert.ToInt32(dtStockItem.Rows[i]["ReOrder"]),
                        //            Action = "Order Now",
                        //            Action1 = "",
                        //            TranType = dtStockItem.Rows[i]["StkTranType"].ToString(),
                        //        });
                        //        ////MyWind.dgTranHistory.RowBackground = Brushes.Red;
                        //        ////e.Row.Cells[9].ForeColor = System.Drawing.Color.Black;
                        //        //MyWind.dgTranHistory.RowBackground = System.Windows.Media.Brushes.IndianRed;
                        //    }
                        //}
                        ////(MyWind.dgTranHistory.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy HH:mm";
                        #endregion --Old Grid Insert for Alert 


                        Int32 mClosingQty = 0;
                        string mTmpTranItemCode = "";
                        string mAction = "", mAction1 = "Cancel";


                        for (int i = 0; i < dtStockItem.Rows.Count; i++)
                        {

                            mTmpTranItemCode = dtStockItem.Rows[i]["ItemCode"].ToString();
                            using (dtTranHistory = new DataTable())
                            {
                                dtTranHistory = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder,ISNULL(O.OBQTY,0) AS OBQTY,ISNULL(P.PURQTY,0) AS PURQTY,ISNULL(R.RECQTY,0) AS RECQTY, ISNULL(S.SALQTY,0) AS SALQTY, ISNULL(E.EADJQTY,0) AS EADJQTY,ISNULL(T.SADJQTY,0) AS SADJQTY FROM tblItemMaster I " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS PURQTY FROM tblStockTran WHERE StkTranType = 'PR' GROUP BY ItemCode) P ON I.ITEMCODE = P.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SALES AS SALQTY FROM tblStock WHERE STATUS = 1) S ON I.ITEMCODE = S.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0 GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0 GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE WHERE I.ITEMCODE = '{mTmpTranItemCode}'");
                            }
                            mClosingQty = (Convert.ToInt32(dtTranHistory.Rows[0]["OBQTY"]) + Convert.ToInt32(dtTranHistory.Rows[0]["RECQTY"]) + Convert.ToInt32(dtTranHistory.Rows[0]["EADJQTY"]) + Convert.ToInt32(dtTranHistory.Rows[0]["SADJQTY"])) - Convert.ToInt32(dtTranHistory.Rows[0]["SALQTY"]);



                            if (dtStockItem.Rows[i]["StkTranType"].ToString() == "PR")
                            {
                                mAction = dtStockItem.Rows[i]["TranStatus"].ToString() == "PR" ? "E-Mail" : "Receive Stock";

                                mAction1 = Convert.ToInt32(dtStockItem.Rows[i]["StkTranPendingQty"]) < Convert.ToInt32(dtStockItem.Rows[i]["StkTranQty"]) ? "Cancel" : "";

                                TranHistoryList.Add(new TranHistoryModel
                                {
                                    StrDate = DateTime.Parse(dtStockItem.Rows[i]["TranDate"].ToString()).ToString("dd-MMM-yyyy hh:mm tt"),
                                    UserName = dtStockItem.Rows[i]["UserName"].ToString(),
                                    ItemCode = dtStockItem.Rows[i]["ItemCode"].ToString(),
                                    ItemDesc = dtStockItem.Rows[i]["ItemDesc"].ToString(),
                                    Unit1 = dtStockItem.Rows[i]["Unit1"].ToString(),
                                    Purchase = Convert.ToInt32(dtStockItem.Rows[i]["StkTranQty"]),
                                    PendingQty = Convert.ToInt32(dtStockItem.Rows[i]["StkTranPendingQty"]),
                                    Closing = mClosingQty,
                                    ReOrderLevel = Convert.ToInt32(dtStockItem.Rows[i]["ReOrder"]),
                                    Action = mAction,
                                    Action1 = mAction1,
                                    TranType = dtStockItem.Rows[i]["StkTranType"].ToString(),
                                    StkTranID = Convert.ToInt32(dtStockItem.Rows[i]["StkTranID"]),
                                });
                            }
                            else
                            {
                                TranHistoryList.Add(new TranHistoryModel
                                {
                                    StrDate = "-",
                                    UserName = "",
                                    ItemCode = dtStockItem.Rows[i]["ItemCode"].ToString(),
                                    ItemDesc = dtStockItem.Rows[i]["ItemDesc"].ToString(),
                                    Unit1 = dtStockItem.Rows[i]["Unit1"].ToString(),
                                    Closing = mClosingQty,
                                    ReOrderLevel = Convert.ToInt32(dtStockItem.Rows[i]["ReOrder"]),
                                    Action = "Order Now",
                                    Action1 = "",
                                    TranType = dtStockItem.Rows[i]["StkTranType"].ToString(),
                                    StkTranID = Convert.ToInt32(dtStockItem.Rows[i]["StkTranID"]),
                                });
                            }
                        }

                        cnvTranHistoryVisibility = "Visible"; gdTranHistoryVisibility = "Visible";
                        lblTranViewHeadVisibility = "Visible"; lblTranViewHeadContent = TranHistoryList.Count + " No(s) of Stationery Items need action.";

                        cnvTranInfoDispBG = "SmokeWhite"; cnvTranInfoDispVisibility = "Visible";

                        MyWind.dgTranHistory.BorderThickness = new Thickness(1);

                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load dashboard Alert : " + Ex.Message); }
        }


        private void LoadTabControl()
        {
            try
            {
                if (tabTranTab1Selected)
                { }
                else if (tabTranTab2Selected)
                {
                    MyWind.cnvTHDisp.Margin = new Thickness(5, 5, 0, 0);
                    LoadTranHistoryList("View Tran", "Stock History");
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load Tab : " + Ex.Message); }

        }

        private void LoadTranListDataGrid()
        {
            try
            {
                var colheadstyle = new Style(typeof(DataGridColumnHeader));
                var chstyleLeft = new Style(typeof(DataGridColumnHeader));
                var chstyleRight = new Style(typeof(DataGridColumnHeader));
                var chstyleCenter = new Style(typeof(DataGridColumnHeader));
                var rowcellstyleRight = new Style(typeof(DataGridCell));
                var rowcellstyleCenter = new Style(typeof(DataGridCell));

                MyWind.dgTranList.Columns.Clear();

                chstyleLeft.BasedOn = MyWind.TryFindResource("DataGridColumnHeaderStyleLeft") as Style;
                chstyleRight.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicRight") as Style;
                chstyleCenter.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicCenter") as Style;
                rowcellstyleRight.BasedOn = MyWind.TryFindResource("DataGridCellRightAlignStyle") as Style;
                rowcellstyleCenter.BasedOn = MyWind.TryFindResource("DataGridCellCenterAlignStyle") as Style;

                MyWind.dgTranList.ColumnHeaderStyle = colheadstyle;

                MyWind.dgTranList.FontSize = 14;

                switch (MInvenRef)
                {
                    case "Purchase":
                        {

                            MyWind.dgTranList.FontSize = 13;

                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order Date", Binding = new Binding("TranDate"), Width = 137, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order By", Binding = new Binding("UserName"), Width = 44, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 140, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 37, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 56, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), Width = 58, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order Quantity", Binding = new Binding("TranQty"), Width = 64, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Action", MaxWidth = 0 });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Item code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                        }
                        break;
                    case "Stock in":
                        {
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 235, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 100, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 60, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Received Quantity", Binding = new Binding("TranQty"), Width = 70, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 60, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Item code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                        }
                        break;
                    case "Adjust Stock":
                        {
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 235, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 60, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 100, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Adjusted Current Stock", Binding = new Binding("TranQty"), Width = 120, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                        }
                        break;
                    case "Opening Balance":
                        {
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 225, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Opening Balance Entry date", Binding = new Binding("TranDate"), Width = 160, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Opening Quantity", Binding = new Binding("Opening"), Width = 90, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 60, HeaderStyle = chstyleLeft });
                            MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });

                            using (SqlDataReader dr = GetDataReader($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,I.ReOrder FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.StkTranType = 'OB' ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode"))
                            {
                                while (dr.Read())
                                {
                                    TranItemList.Add(new TranListModel
                                    {
                                        ItemDesc = dr["ItemDesc"].ToString(),
                                        TranDate = Convert.ToDateTime(dr["StkTranDate"]),
                                        Unit1 = dr["Unit1"].ToString(),
                                        Opening = Convert.ToInt32(dr["StkTranQty"]),
                                        ReOrderLevel = Convert.ToInt32(dr["ReOrder"]),
                                        ItemCode = dr["ItemCode"].ToString(),
                                    });
                                }
                                dr.Close();
                                (MyWind.dgTranList.Columns[1] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                            }
                        }
                        break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load transaction grid data : " + Ex.Message); }
        }

        public List<string> finalistAWB = new List<string>();
        DataTable dtItemSummary;

        private void createDataTable()
        {
            try
            {
                if (dtItemSummary != null) { dtItemSummary.Clear(); StationeryList.Clear(); }
                using (dtItemSummary = new DataTable())
                {
                    dtItemSummary = GetDataTable($"SELECT A.ItemId As ColOneId, A.ItemCode As ColOneText, A.ItemDesc As StationeryName, A.Unit1 As UOM, A.Unit1SalRateToStation As Rate, 0 as Quantity, 0.0 as Total,0 AS StkTranID  FROM tblItemMaster A" +
                                    $" LEFT JOIN tblGeneralDet B ON A.ItemType = B.GenDetId" +
                                    $" ORDER BY A.ItemCode");

                    StationeryLoad();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Get Stationery data : " + Ex.Message); }
        }

        private void StationeryLoad()
        {
            if (dtItemSummary.Rows.Count > 0)
            {
                SqlDataReader dr;
                try
                {
                    for (int i = 0; i < dtItemSummary.Rows.Count; i++)
                    {
                        if (dtItemSummary.Rows[i]["StationeryName"].ToString() != "PRIHATIN" && dtItemSummary.Rows[i]["StationeryName"].ToString() != "HANDLING CHARGES")
                        {
                            dr = GetDataReader($"SELECT ItemImage FROM tblItemMaster WHERE ItemCode = '{dtItemSummary.Rows[i]["ColOneText"].ToString()}'");
                            if (dr.Read() && dr["ItemImage"] != DBNull.Value)
                            {
                                byte[] imgBytes = (byte[])dr["ItemImage"];
                                using (MemoryStream strm = new MemoryStream(imgBytes))
                                {
                                    strm.Write(imgBytes, 0, imgBytes.Length);
                                    strm.Position = 0;
                                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                                    System.Windows.Media.Imaging.BitmapImage bi = new System.Windows.Media.Imaging.BitmapImage();
                                    bi.BeginInit();
                                    MemoryStream ms = new MemoryStream();
                                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    bi.StreamSource = ms;
                                    bi.EndInit();
                                    StationeryList.Add(new StationeryModelI { itemname = dtItemSummary.Rows[i]["StationeryName"].ToString().ToUpper(), ItemTypeSource = bi as ImageSource });
                                }
                            }
                            else
                            {
                                StationeryList.Add(new StationeryModelI { itemname = dtItemSummary.Rows[i]["StationeryName"].ToString().ToUpper(), ItemTypeSource = null });
                            }
                        }
                    }

                    if (dtItemSummary.Rows.Count % 2 == 0)
                    {
                        StationeryList.Add(new StationeryModelI { itemname = "", ItemTypeSource = null });
                    }
                    SelectedStationaryIndex = dtItemSummary.Rows.Count - 1;
                }
                catch (Exception Ex) { error_log.errorlog("Load Stationery Image : " + Ex.Message); }
            }
        }
        public string tblCashSalesInfo = "", PaymentView = "";

        private void StationeryImageClick(object obj)
        {
            mTranItemCode = "";
            try
            {
                string itemName = (((StationeryModelI)(obj)).itemname).ToString();
                StationeryItem = itemName;
                if (itemName != "")
                {
                    foreach (DataRow item in dtItemSummary.Rows)
                    {
                        if (item["StationeryName"].ToString().ToUpper() == itemName)
                        {
                            mTranItemCode = item["ColOneText"].ToString().ToUpper();
                            break;
                        }
                    }
                }
                if (mTranItemCode != "") { LoadStationery(mTranItemCode); }
            }
            catch (Exception Ex) { error_log.errorlog("Get Stationery info : " + Ex.Message); }
        }

        private void LoadStationery(string pSItemCode)
        {
            try
            {
                EditItem = false;
                double mOpeningQty = 0, mClosingQty = 0;
                mOBEntryFlag = true;

                mTranItemCode = pSItemCode;

                using (SqlDataReader dr = GetDataReader($"SELECT I.ItemCode,I.ItemDesc,I.Unit1,S.Opening,S.Closing,I.ReOrder FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.ItemCode = '{mTranItemCode}' AND I.Status = 1 AND S.Status = 1"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            mOpeningQty = Convert.ToInt32(dr["Opening"]);
                            mClosingQty = Convert.ToInt32(dr["Closing"]);
                        }
                    }
                    dr.Close();
                }

                if (MInvenRef == "Opening Balance" && mOpeningQty > 0)
                {
                    MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("You can't modify Opening balance !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    mOBEntryFlag = false;
                }
                else
                { mOBEntryFlag = true; }


                if (mOBEntryFlag)
                {
                    foreach (DataRow item in dtItemSummary.Rows)
                    {
                        if (item["ColOneText"].ToString().ToUpper() == mTranItemCode)
                        {
                            if (Convert.ToInt32(item["Quantity"]) > 0)
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Already exists. Want to edit quantity?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes)
                                {
                                    EditItem = true;
                                    StationeryItem = item["StationeryName"].ToString().ToUpper();
                                    TranQty = Convert.ToInt32(item["Quantity"]);
                                    lblUOMContent = item["UOM"].ToString().ToUpper();
                                }
                                else
                                {
                                    StationeryItem = ""; TranQty = 0; lblUOMContent = "";
                                }
                            }
                            else
                            {
                                StationeryItem = item["StationeryName"].ToString().ToUpper();
                                TranQty = 0;
                                lblUOMContent = item["UOM"].ToString().ToUpper();
                            }

                            mMaxQty = 0;
                            mMaxOrdQty = 0;
                            if (StationeryItem != "")
                            {
                                using (SqlDataReader dr = GetDataReader($"SELECT I.ItemCode,I.ItemDesc,I.Unit1,S.Closing,I.MaxStk,I.ReOrder FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.ItemCode = '{mTranItemCode}' AND I.Status = 1 AND S.Status = 1"))
                                {
                                    if (dr.HasRows)
                                    {
                                        if (dr.Read())
                                        {

                                            mMaxQty = Convert.ToInt32(dr["MaxStk"]);
                                            mMaxOrdQty = Convert.ToInt32(dr["MaxStk"]);
                                            mCStockQty = Convert.ToInt32(dr["Closing"]);
                                            if (mMaxQty > 0)
                                            {
                                                if (mMaxQty > Convert.ToInt32(dr["Closing"]))
                                                {
                                                    mMaxOrdQty = mMaxQty - Convert.ToInt32(dr["Closing"]);
                                                }
                                                else
                                                {
                                                    mMaxOrdQty = 0;
                                                }
                                            }
                                            else
                                            {
                                                mMaxOrdQty = Convert.ToInt32(dr["ReOrder"]);
                                            }

                                            lblSysStockQtyContent = "Current Stock : " + Convert.ToInt32(mCStockQty).ToString() + " " + dr["Unit1"].ToString();
                                            lblMaxLimitContent = "Maximum Stock : " + Convert.ToInt32(mMaxQty).ToString() + " " + dr["Unit1"].ToString();
                                            lblReOrderLevelContent = "Reorder level      : " + Convert.ToInt32(dr["ReOrder"]).ToString() + " " + dr["Unit1"].ToString();

                                            if (mMaxOrdQty > 0)
                                            {
                                                lblCMaxOrderLimitContent = "Order quantity should not exceed : " + Convert.ToInt32(mMaxOrdQty).ToString() + " " + dr["Unit1"].ToString();
                                            }
                                            else if (mMaxOrdQty <= 0)
                                            {
                                                lblCMaxOrderLimitContent = "    You have sufficient/excess quantity in stock.";
                                            }
                                        }
                                    }
                                    dr.Close();
                                }
                            }
                            break;
                        }
                    }

                    if (mOBEntryFlag == true)
                    {
                        MyWind.txtTranItemQty.SelectAll();
                        MyWind.txtTranItemQty.Focus();
                    }
                }
                else
                {
                    StationeryItem = ""; TranQty = 0; lblUOMContent = "";
                    lblSysStockQtyContent = "";
                    lblReOrderLevelContent = "";
                }

            }
            catch (Exception Ex) { error_log.errorlog("Load Stationery : " + Ex.Message); }
        }


        private void LoadInterFace(string TranFlag, string QuickMenu)
        {
            try
            {

                //CSInvenOBPrefix, CSInvenPRPrefix, CSInvenPOPrefix, CSInvenGRNPrefix, CSInvenSASPrefix;
                if (cnvTranHistoryVisibility != "Collapsed")
                {
                    cnvTranHistoryVisibility = "Collapsed"; lblTranViewHeadVisibility = "Collapsed"; gdTranHistoryVisibility = "Collapsed";
                    lblTranHistoryCatVisibility = "Collapsed"; cbStockTranTypeVisibility = "Collapsed";
                    lblTranHistoryFDateVisibility = "Collapsed"; dtpTranHistoryFDateVisibility = "Collapsed";
                    lblTranHistoryTDateVisibility = "Collapsed"; dtpTranHistoryTDateVisibility = "Collapsed";
                    btnShowVisibility = "Collapsed"; btnPrintVisibility = "Collapsed";
                }

                TranReference = ""; StationeryItem = ""; TranQty = 0; txtTranRemarks = "";
                TranSelectDate = DateTime.Now;
                TranItemList.Clear();
                TranHistoryList.Clear();

                createDataTable();

                LoadTranListDataGrid();         ///// LIST VIEW now CHANGED TO DATAGRID

                scvStationeryItemViewVisibility = "Visible";
                lblUOMVisibility = "Visible";
                lblUOMContent = "";

                switch (TranFlag)
                {
                    case "Purchase Order":
                        {

                            mStkTranType = "PR";
                            mSeqName = "ITranRefNumPR";
                            mPrefix = CSInvenPRPrefix;

                            lblTranRefHeadContent = "Request Stationery";
                            lblTranRemarksContent = "Remarks"; lblTranRefDateContent = "Date";
                            lblTranItemNameContent = "Stationery Item"; lblTranItemQtyContent = "Order Quantity";
                            //lblTranListRefHeadContent = "List of requested stationery items to be E-Mailed";
                            lblTranListRefHeadContent = "List of requested stationery purchase order to be E-Mailed";
                            //MyWind.lblTranListRefHead.FontSize = 16;


                            lblTranRefHeadVisibility = "Visible";
                            lblTranRemarksVisibility = "Collapsed";
                            lblTranRefDateVisibility = "Visible";
                            lblTranItemNameVisibility = "Visible"; lblTranItemQtyVisibility = "Visible";
                            lblTranListRefHeadVisibility = "Visible";
                            lblSysStockQtyContent = "Current Stock  :"; lblSysStockQtyVisibility = "Visible";
                            lblReOrderLevelContent = "Reorder Level :"; lblReOrderLevelVisibility = "Visible";
                            lblMaxLimitContent = "Maximum Stock :"; lblMaxLimitVisibility = "Visible";
                            lblCMaxOrderLimitContent = ""; lblCMaxOrderLimitVisibility = "Visible";

                            txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                            txtTranItemNameVisisbility = "Visible"; txtTranItemQtyVisisbility = "Visible";
                            dtpTranRefDateVisibility = "Visible";

                            btnAddTranContent = "Save"; btnAddTranVisibility = "Visible";
                            btnSaveTranContent = "Send Email"; btnSaveTranVisibility = "Visible";

                            lvTranListVisibility = "Visible";
                            dgTranListVisibility = "Visible";

                            lblStatinoryItemContent = "Stationery Item List"; lblStatinoryItemVisibility = "Visible";

                            LoaddgTranList();

                            #region MOVED TO METHOD LoaddgTranList ----> START
                            //TranItemList.Clear();
                            //dtTranHistory = new DataTable();
                            ////////  Load Pending items to be email    ---> START
                            //using (dtTranHistory = new DataTable())
                            //{
                            //    ////	StkTranType,StkTranDate,ItemCode,Unit1,StkTranQty,TranStatus,StkTranRemarks,CreateBy
                            //    //dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemDesc,I.ReOrder,S.StkTranID,S.StkTranType,S.StkTranDate,S.ItemCode,S.Unit1,S.StkTranQty,S.StkTranRemarks,U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE S.StkTranType = 'PR' AND S.TranStatus != 'EM' ORDER BY S.StkTranDate");
                            //    dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemDesc,I.ReOrder,S.StkTranID,S.StkTranType,S.StkTranDate,S.ItemCode,S.Unit1,S.StkTranQty,S.StkTranRemarks,0 as ClosingStk,U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemCode = S.ItemCode JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE S.StkTranType = 'PR' AND S.TranStatus != 'EM' ORDER BY S.StkTranDate");
                            //    if (dtTranHistory.Rows.Count > 0)
                            //    {

                            //        Int32 mClosingQty = 0;
                            //        string mTmpTranItemCode = "";
                            //        DateTime frmdate = DateTime.Now;

                            //        for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                            //        {

                            //            //frmdate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]);

                            //            mTmpTranItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString();
                            //            using (dtTemp = new DataTable())
                            //            {
                            //                dtTemp = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder,ISNULL(O.OBQTY,0) AS OBQTY,ISNULL(R.RECQTY,0) AS RECQTY, ISNULL(S.SALQTY,0) AS SALQTY, ISNULL(E.EADJQTY,0) AS EADJQTY,ISNULL(T.SADJQTY,0) AS SADJQTY FROM tblItemMaster I " +
                            //                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' AND ItemCode = '{mTmpTranItemCode}' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE   " +
                            //                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                            //                        $"LEFT JOIN (SELECT ItemCode,SALES AS SALQTY FROM tblStock WHERE STATUS = 1 AND ItemCode = '{mTmpTranItemCode}') S ON I.ITEMCODE = S.ITEMCODE " +
                            //                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                            //                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE WHERE I.ITEMCODE = '{mTmpTranItemCode}'");
                            //            }

                            //            mClosingQty = (Convert.ToInt32(dtTemp.Rows[0]["OBQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["RECQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["EADJQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["SADJQTY"])) - Convert.ToInt32(dtTemp.Rows[0]["SALQTY"]);

                            //            //TranItemList.Add(new TranListModel { ItemNo = i+1, ItemCode = item["ColOneText"].ToString(), ItemDesc = item["StationeryName"].ToString().ToUpper(), Unit1 = item["UOM"].ToString(), TranQty = TranQty, Closing = Convert.ToInt32(dtStockItem.Rows[0]["Closing"]), ReOrderLevel = Convert.ToInt32(dtStockItem.Rows[0]["ReOrder"]) });
                            //            TranItemList.Add(new TranListModel
                            //            {
                            //                ItemNo = i + 1,
                            //                ItemCode = dtTranHistory.Rows[i]["ItemCOde"].ToString(),
                            //                ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                            //                Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                            //                TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                            //                Closing = mClosingQty,
                            //                ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                            //                UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                            //                TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),
                            //                StkTranID = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"]),
                            //            });

                            //            (MyWind.dgTranList.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";

                            //            dtTranHistory.AsEnumerable().Where(rec => Convert.ToInt32(rec["StkTranID"]) == Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"])).ToList().ForEach(rec => { rec["ClosingStk"] = mClosingQty; });

                            //        }

                            //        btnSaveTranVisibility = "Visible"; btnSaveTranEnable = true;
                            //    }
                            //}
                            ////////  Load Pending items to be email    ---> END
                            #endregion MOVED TO METHOD LoaddgTranList ----> END

                            if (mTranItemCode != "" && mEmailFlag == false)
                            {
                                LoadStationery(mTranItemCode);
                            }
                        }
                        break;
                    case "Stock in":
                        {

                            mStkTranType = "GRN";
                            mSeqName = "ITranRefNumGRN";
                            mPrefix = CSInvenGRNPrefix;

                            lblTranRefHeadContent = "Stock in";
                            lblTranRemarksContent = "Reamrks"; lblTranRefDateContent = "Inward Date";
                            lblTranItemNameContent = "Stationery Item"; lblTranItemQtyContent = "Received Quantity";
                            lblTranListRefHeadContent = "Received List of stationery Items";

                            lblTranRefHeadVisibility = "Visible";
                            lblTranRefDateVisibility = "Visible";
                            lblTranItemNameVisibility = "Visible"; lblTranItemQtyVisibility = "Visible";
                            lblTranListRefHeadVisibility = "Visible";
                            lblSysStockQtyContent = "Current Stock  : "; lblSysStockQtyVisibility = "Visible";
                            lblTranRemarksVisibility = "Collapsed";

                            txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                            txtTranItemNameVisisbility = "Visible"; txtTranItemQtyVisisbility = "Visible";
                            dtpTranRefDateVisibility = "Visible";

                            btnAddTranContent = "Save"; btnAddTranVisibility = "Visible";
                            //btnSaveTranContent = "Save Inward Item"; /btnSaveTranVisibility = "Visible";

                            lvTranListVisibility = "Visible";
                            dgTranListVisibility = "Visible";


                            lblStatinoryItemContent = "Stationery Item List"; lblStatinoryItemVisibility = "Visible";
                            scvStationeryItemViewVisibility = "Visible";

                            if (mTranItemCode != "")
                            {
                                LoadStationery(mTranItemCode);
                            }
                        }
                        break;
                    case "Adjust Stock":
                        {

                            mStkTranType = "SA";
                            mSeqName = "ITranRefNumSA";
                            mPrefix = CSInvenSAPrefix;

                            lblTranRefHeadContent = "Adjust Stock";

                            lblTranRemarksContent = "Remarks"; lblTranRefDateContent = "Date";
                            lblTranItemNameContent = "Stationery Item"; lblTranItemQtyContent = "Adj. Current Stock";

                            lblTranListRefHeadContent = "Stock adjusted list of stationery item(s) ";

                            lblTranRefHeadVisibility = "Visible";
                            lblTranRefDateVisibility = "Visible";
                            lblTranItemNameVisibility = "Visible"; lblTranItemQtyVisibility = "Visible";
                            lblTranListRefHeadVisibility = "Visible";
                            lblSysStockQtyContent = "Current Stock  : "; lblSysStockQtyVisibility = "Visible";
                            lblTranRemarksVisibility = "Visible";

                            txtTranRemarksVisisbility = "Visible"; txtTranRemarksEnabled = true;
                            txtTranItemNameVisisbility = "Visible"; txtTranItemQtyVisisbility = "Visible";
                            dtpTranRefDateVisibility = "Visible";

                            btnAddTranContent = "Save"; btnAddTranVisibility = "Visible";
                            btnSaveTranVisibility = "Collapsed"; btnSaveTranContent = "";

                            lvTranListVisibility = "Visible";
                            dgTranListVisibility = "Visible";


                            lblStatinoryItemContent = "Stationery Item List"; lblStatinoryItemVisibility = "Visible";
                            scvStationeryItemViewVisibility = "Visible";

                            if (mTranItemCode != "")
                            {
                                LoadStationery(mTranItemCode);
                            }
                        }
                        break;
                    case "Opening Balance":
                        {
                            mStkTranType = "OB";

                            lblTranRefHeadContent = "Opening Balance";

                            lblTranRemarksContent = "Remarks"; lblTranRefDateContent = "OB entry date";
                            lblTranItemNameContent = "Stationery Item"; lblTranItemQtyContent = "Opening Quantity";

                            lblTranListRefHeadContent = "Opening balance status of Stationery Item";

                            lblTranRefHeadVisibility = "Visible";
                            lblTranRefDateVisibility = "Visible";
                            lblTranItemNameVisibility = "Visible"; lblTranItemQtyVisibility = "Visible";
                            lblTranListRefHeadVisibility = "Visible";

                            lblTranRemarksVisibility = "Collapsed";

                            lblSysStockQtyContent = ""; lblSysStockQtyVisibility = "Collapsed";
                            lblSysStockQtyContent = ""; lblSysStockQtyVisibility = "Collapsed";


                            txtTranRemarksVisisbility = "Collapsed"; txtTranRemarksEnabled = false;
                            txtTranItemNameVisisbility = "Visible"; txtTranItemQtyVisisbility = "Visible";
                            dtpTranRefDateVisibility = "Visible";

                            btnAddTranVisibility = "Visible"; btnSaveTranVisibility = "Collapsed";
                            btnAddTranContent = "Save"; btnSaveTranContent = "";

                            lvTranListVisibility = "Visible";
                            dgTranListVisibility = "Visible";


                            lblStatinoryItemContent = "Stationery Item List"; lblStatinoryItemVisibility = "Visible";
                            scvStationeryItemViewVisibility = "Visible";

                            if (mTranItemCode != "")
                            {
                                LoadStationery(mTranItemCode);
                            }
                        }
                        break;
                    case "Stock Status":
                        {

                        }
                        break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load Interface : " + Ex.Message); }
        }

        private void LoaddgTranList()
        {
            TranItemList.Clear();
            dtTranHistory = new DataTable();

            var colheadstyle = new Style(typeof(DataGridColumnHeader));
            var chstyleLeft = new Style(typeof(DataGridColumnHeader));
            var chstyleRight = new Style(typeof(DataGridColumnHeader));
            var chstyleCenter = new Style(typeof(DataGridColumnHeader));
            var rowcellstyleRight = new Style(typeof(DataGridCell));
            var rowcellstyleCenter = new Style(typeof(DataGridCell));


            MyWind.dgTranList.Columns.Clear();

            try
            {
                chstyleLeft.BasedOn = MyWind.TryFindResource("DataGridColumnHeaderStyleLeft") as Style;
                chstyleRight.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicRight") as Style;
                chstyleCenter.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicCenter") as Style;
                rowcellstyleRight.BasedOn = MyWind.TryFindResource("DataGridCellRightAlignStyle") as Style;
                rowcellstyleCenter.BasedOn = MyWind.TryFindResource("DataGridCellCenterAlignStyle") as Style;

                MyWind.dgTranList.ColumnHeaderStyle = colheadstyle;

                MyWind.dgTranList.FontSize = 13;

                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order Date", Binding = new Binding("TranDate"), Width = 137, HeaderStyle = chstyleLeft });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order By", Binding = new Binding("UserName"), Width = 44, HeaderStyle = chstyleLeft });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 140, HeaderStyle = chstyleLeft });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 37, HeaderStyle = chstyleLeft });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 56, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), Width = 58, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Order Quantity", Binding = new Binding("TranQty"), Width = 64, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Action", MaxWidth = 0 });
                MyWind.dgTranList.Columns.Add(new DataGridTextColumn { Header = "Item code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });

                //////  Load Pending items to be email    ---> START
                using (dtTranHistory = new DataTable())
                {
                    ////	StkTranType,StkTranDate,ItemCode,Unit1,StkTranQty,TranStatus,StkTranRemarks,CreateBy
                    //dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemDesc,I.ReOrder,S.StkTranID,S.StkTranType,S.StkTranDate,S.ItemCode,S.Unit1,S.StkTranQty,S.StkTranRemarks,U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE S.StkTranType = 'PR' AND S.TranStatus != 'EM' ORDER BY S.StkTranDate");
                    dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemDesc,I.ReOrder,S.StkTranID,S.StkTranType,S.StkTranDate,S.ItemCode,S.Unit1,S.StkTranQty,S.StkTranRemarks,0 as ClosingStk,U.UserName,0 AS PurOrdQty1,'' AS PurOrdDT1,0 AS PurOrdQty2,'' AS PurOrdDT2,0 AS PurOrdQty3,'' AS PurOrdDT3,0 AS SalesQty1,'' AS SalesMnthYr1,0 AS SalesQty2,'' AS SalesMnthYr2,0 AS SalesQty3,'' AS SalesMnthYr3 FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemCode = S.ItemCode JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE S.StkTranType = 'PR' AND S.TranStatus = 'PR' ORDER BY S.StkTranDate");
                    if (dtTranHistory.Rows.Count > 0)
                    {

                        Int32 mClosingQty = 0;
                        string mTmpTranItemCode = "";
                        DateTime frmDate1 = DateTime.Now, toDate1 = DateTime.Now;
                        for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                        {
                            mTmpTranItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString();
                            using (dtTemp = new DataTable())
                            {
                                dtTemp = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder,ISNULL(O.OBQTY,0) AS OBQTY,ISNULL(R.RECQTY,0) AS RECQTY, ISNULL(S.SALQTY,0) AS SALQTY, ISNULL(E.EADJQTY,0) AS EADJQTY,ISNULL(T.SADJQTY,0) AS SADJQTY FROM tblItemMaster I " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' AND ItemCode = '{mTmpTranItemCode}' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE   " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmDate1.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                                        $"LEFT JOIN (SELECT CNNum,SUM(Pieces) AS SALQTY FROM tblCSaleCNNum WHERE ShipmentType = 'STATIONERY' AND CNNum = '{mTmpTranItemCode}' AND CDate <= '{frmDate1.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY CNNum) S ON I.ITEMCODE = S.CNNum " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmDate1.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                                        $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmDate1.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE WHERE I.ITEMCODE = '{mTmpTranItemCode}'");
                            }

                            mClosingQty = (Convert.ToInt32(dtTemp.Rows[0]["OBQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["RECQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["EADJQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["SADJQTY"])) - Convert.ToInt32(dtTemp.Rows[0]["SALQTY"]);
                            dtTranHistory.AsEnumerable().Where(rec => Convert.ToInt32(rec["StkTranID"]) == Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"])).ToList().ForEach(rec => { rec["ClosingStk"] = mClosingQty; });
                        }


                        frmDate1 = DateTime.Now.AddMonths(-3);
                        frmDate1 = frmDate1.AddDays(1 - frmDate1.Day);
                        toDate1 = frmDate1.AddMonths(1).AddDays(-1);

                        DateTime frmDate2 = toDate1.AddDays(1), toDate2 = frmDate2.AddMonths(1).AddDays(-1), frmDate3 = toDate2.AddDays(1), toDate3 = frmDate3.AddMonths(1).AddDays(-1);

                        //  UPADTE LAST 3 MONTH SALES INTO DATA TABLE - dtTranHistory
                        using (dtTemp = new DataTable())
                        {
                            dtTemp = GetDataTable($"SELECT CNNum as ItemCode, " +
                                                    $"SUM(CASE WHEN CDATE BETWEEN '{frmDate1.ToString("MM/dd/yyyy")}' AND '{toDate1.ToString("MM/dd/yyyy")}' THEN Pieces ELSE 0 END) AS SalesQty1, " +
                                                    $"SUM(CASE WHEN CDATE BETWEEN '{frmDate2.ToString("MM/dd/yyyy")}' AND '{toDate2.ToString("MM/dd/yyyy")}' THEN Pieces ELSE 0 END) AS SalesQty2, " +
                                                    $"SUM(CASE WHEN CDATE BETWEEN '{frmDate3.ToString("MM/dd/yyyy")}' AND '{toDate3.ToString("MM/dd/yyyy")}' THEN Pieces ELSE 0 END) AS SalesQty3 " +
                                                    $"FROM tblCSaleCNNum " +
                                                    $"WHERE ShipmentType = 'STATIONERY' AND CDate BETWEEN '{frmDate1.ToString("MM/dd/yyyy")}' AND '{toDate3.ToString("MM/dd/yyyy")}' " +
                                                    $"AND CNNum IN(SELECT ITEMCODE FROM tblStockTran WHERE StkTranType = 'PR' AND TranStatus = 'PR') GROUP BY CNNum");

                            var updateQuery = from r1 in dtTranHistory.AsEnumerable()
                                              join r2 in dtTemp.AsEnumerable()
                                              on r1.Field<string>("ItemCode") equals r2.Field<string>("ItemCode")
                                              select new { r1, r2 };

                            foreach (var x in updateQuery)
                            {
                                x.r1.SetField("SalesQty1", x.r2.Field<Int32>("SalesQty1"));
                                x.r1.SetField("SalesQty2", x.r2.Field<Int32>("SalesQty2"));
                                x.r1.SetField("SalesQty3", x.r2.Field<Int32>("SalesQty3"));
                                x.r1.SetField("SalesMnthYr1", frmDate1.ToString("MMM yyyy"));
                                x.r1.SetField("SalesMnthYr2", frmDate2.ToString("MMM yyyy"));
                                x.r1.SetField("SalesMnthYr3", frmDate3.ToString("MMM yyyy"));
                            }

                        }

                        //  UPADTE LAST 3 PURCHASE ORDER INSERT INTO DATA TABLE - dtTranHistory
                        using (dtTemp = new DataTable())
                        {
                            dtTemp = GetDataTable($"SELECT S.ITEMCODE, S.StkTranDate, S.StkTranQty " +
                                                    $"FROM(SELECT *, ROW_NUMBER() OVER(PARTITION BY[ITEMCODE] ORDER BY[StkTranDate] DESC) AS PURNUM " +
                                                    $"FROM tblStockTran WHERE ItemCode IN(SELECT ITEMCODE FROM tblStockTran WHERE StkTranType = 'PR' AND TranStatus = 'PR') AND(StkTranType = 'PR' AND TranStatus != 'PR')) S " +
                                                    $"WHERE PURNUM < 4 ORDER BY S.ITEMCODE, S.StkTranDate");

                            var updateQuery = from r1 in dtTranHistory.AsEnumerable()
                                              join r2 in dtTemp.AsEnumerable()
                                              on r1.Field<string>("ItemCode") equals r2.Field<string>("ItemCode")
                                              select new { r1, r2 };
                            int mCtr = 0;
                            string mItemCode = "";

                            foreach (var x in updateQuery)
                            {
                                if (mItemCode != x.r2.Field<string>("ItemCode"))
                                { mItemCode = x.r2.Field<string>("ItemCode"); mCtr = 1; }
                                else
                                { mCtr += 1; }

                                switch (mCtr)
                                {
                                    case 1:
                                        {
                                            x.r1.SetField("PurOrdDT1", x.r2.Field<DateTime>("StkTranDate").ToString("dd-MMM-yyyy"));
                                            x.r1.SetField("PurOrdQty1", x.r2.Field<Decimal>("StkTranQty"));
                                        }
                                        break;
                                    case 2:
                                        {
                                            x.r1.SetField("PurOrdDT2", x.r2.Field<DateTime>("StkTranDate").ToString("dd-MMM-yyyy"));
                                            x.r1.SetField("PurOrdQty2", x.r2.Field<Decimal>("StkTranQty"));
                                        }
                                        break;
                                    case 3:
                                        {
                                            x.r1.SetField("PurOrdDT3", x.r2.Field<DateTime>("StkTranDate").ToString("dd-MMM-yyyy"));
                                            x.r1.SetField("PurOrdQty3", x.r2.Field<Decimal>("StkTranQty"));
                                        }
                                        break;
                                    default: break;
                                }
                            }
                        }
                        //  UPADTE LAST 3 PURCHASE ORDER QUANTITY (ALREADY EMAILED) INTO DATA TABLE - dtTranHistory

                        for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                        {
                            TranItemList.Add(new TranListModel
                            {
                                ItemNo = i + 1,
                                ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                Closing = Convert.ToInt32(dtTranHistory.Rows[i]["ClosingStk"]),
                                ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                                TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),

                                Purchase1 = Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty1"]) > 0 ? dtTranHistory.Rows[i]["PurOrdDT1"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty1"]).ToString() : "-",
                                Purchase2 = Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty2"]) > 0 ? dtTranHistory.Rows[i]["PurOrdDT2"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty2"]).ToString() : "-",
                                Purchase3 = Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty3"]) > 0 ? dtTranHistory.Rows[i]["PurOrdDT3"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["PurOrdQty3"]).ToString() : "-",

                                Sales1 = Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty1"]) > 0 ? dtTranHistory.Rows[i]["SalesMnthYr1"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty1"]).ToString() : "-",
                                Sales2 = Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty2"]) > 0 ? dtTranHistory.Rows[i]["SalesMnthYr2"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty2"]).ToString() : "-",
                                Sales3 = Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty3"]) > 0 ? dtTranHistory.Rows[i]["SalesMnthYr3"].ToString() + " : " + Convert.ToInt32(dtTranHistory.Rows[i]["SalesQty3"]).ToString() : "-",

                                StkTranID = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"]),
                            });
                            (MyWind.dgTranList.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";

                            foreach (DataRow row in dtItemSummary.Rows)
                            {
                                if (row["ColOneText"].ToString() == dtTranHistory.Rows[i]["ItemCode"].ToString())
                                {
                                    row["Quantity"] = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]);
                                    row["StkTranID"] = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"]);
                                    break;
                                }
                            }

                        }

                        btnSaveTranVisibility = "Visible"; btnSaveTranEnable = true;
                    }
                }
                //////  Load Pending items to be email    ---> END
            }
            catch (Exception Ex) { error_log.errorlog("Load Transation : " + Ex.Message); }
        }

        private void IntegrateData(string TranFlag)
        {
            switch (TranFlag)
            {
                case "button Tran Save":
                    {

                        AddTranQtytoList();

                        switch (MInvenRef)
                        {
                            case "Purchase":
                                {
                                }
                                break;
                            case "Stock in":
                                {
                                }
                                break;
                            case "Adjust Stock":
                                {
                                }
                                break;
                            case "Opening Balance":
                                {
                                }
                                break;
                            case "Stock Status":
                                {
                                }
                                break;
                        }
                    }
                    break;

                case "button Main Save":
                    {

                        if (MInvenRef == "Purchase")
                        {
                        }
                        else if (MInvenRef == "Stock in")
                        {
                        }
                        else if (MInvenRef == "Adjust Stock")
                        {
                        }
                        else if (MInvenRef == "Opening Balance")
                        {
                        }
                        else if (MInvenRef == "Stock Status")
                        {
                        }
                    }
                    break;
                case "TransactionApprove":
                    {
                    }
                    break;
                case "TransactionSave":
                    {
                    }
                    break;
            }
        }

        private void LoadStockTranType(string TranFlag)
        {
            try
            {
                StockTranTypeList.Clear();
                dtTranHistory = new DataTable();
                switch (MInvenRef)
                {
                    case "Stock Status":
                        {
                            using (dtTranHistory = new DataTable())
                            {
                                dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.Unit1Cost,S.Opening,S.Purchase,S.Excess,S.Sales,S.Short,S.Closing,I.ReOrder FROM tblItemMaster As I JOIN tblStock As S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.Status = 1 ORDER BY I.ItemDesc");
                                if (dtTranHistory.Rows.Count > 0)
                                {
                                    StockTranTypeList.Add(new StockTranTypeModel { TranType = "ALL", TranDesc = "All" });
                                    for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                    {
                                        StockTranTypeList.Add(new StockTranTypeModel { TranType = dtTranHistory.Rows[i]["ItemCode"].ToString(), TranDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString() });
                                    }
                                }
                                SelectedStockTranTypeIndex = 0;
                            }
                        }
                        break;

                    case "Stock History":
                        {
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "PR", TranDesc = "Purchase request" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "GRN", TranDesc = "Stock In" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "SA", TranDesc = "Stock Adjust" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "SH", TranDesc = "Stock History" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "OS", TranDesc = "Opening Stock" });
                            SelectedStockTranTypeIndex = 0;
                        }
                        break;
                    case "Purchase":
                        {
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "All", TranDesc = "Purchase All" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "PR", TranDesc = "Purchase request" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "PA", TranDesc = "Purchase Approval" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "PO", TranDesc = "Purchase Order" });
                            SelectedStockTranTypeIndex = 0;
                        }
                        break;
                    case "Stock in":
                        {
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "GRN", TranDesc = "Purchase Receivd" });
                            SelectedStockTranTypeIndex = 0;
                        }
                        break;
                    case "Adjust Stock":
                        {
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "All", TranDesc = "Stock Adjust All" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "ES", TranDesc = "Excess Stock" });
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "SS", TranDesc = "Short Stock" });
                            SelectedStockTranTypeIndex = 0;
                        }
                        break;
                    case "Opening Balance":
                        {
                            StockTranTypeList.Add(new StockTranTypeModel { TranType = "OB", TranDesc = "Opening Stock" });
                            SelectedStockTranTypeIndex = 0;
                        }
                        break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load Transaction type : " + Ex.Message); }
        }

        private void LoadTranHistoryList(string TranFlag, string mRefFlag)
        {


            TranHistoryList.Clear();
            THistoryList.Clear();
            dtTranHistory = new DataTable();

            string MSqlStr = "", MFTDate = "";

            try
            { 
                if (MInvenRef != "Stock Status" && TranFlag != "Show Tran" && mHFlag == false)
                {
                    FromDate = DateTime.Now.Date.AddMonths(-1).AddDays(1).ToShortDateString();
                    ToDate = DateTime.Now.Date.ToShortDateString();
                }

                DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                using (dtTranHistory = new DataTable())
                {
                    switch (mRefFlag)
                    {
                        case "Stock Status": case "":
                            {

                                dtTranHistory = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder,ISNULL(O.OBQTY,0) AS OBQTY,ISNULL(P.PURQTY,0) AS PURQTY,ISNULL(R.RECQTY,0) AS RECQTY, ISNULL(S.SALQTY,0) AS SALQTY, ISNULL(E.EADJQTY,0) AS EADJQTY,ISNULL(T.SADJQTY,0) AS SADJQTY FROM tblItemMaster I " +
                                                             $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE " +
                                                             $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS PURQTY FROM tblStockTran WHERE StkTranType = 'PR' GROUP BY ItemCode) P ON I.ITEMCODE = P.ITEMCODE " +
                                                             $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                                                             $"LEFT JOIN (SELECT ItemCode,SALES AS SALQTY FROM tblStock WHERE STATUS = 1) S ON I.ITEMCODE = S.ITEMCODE " +
                                                             $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0 GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                                                             $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0 GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE ORDER BY I.ITEMDESC");
                            }
                            break;
                        case "Stock History":
                            {
                                switch (MInvenRef)       //  Purchase    Stock in    Adjust Stock    Opening Balance     Stock Status
                                {
                                    case "Purchase":
                                        {
                                            //dtTranHistory = GetDataTable("SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,I.ReOrder FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.StkTranType = 'PR' ORDER BY S.StkTranDate,S.StkTranType,S.ItemCode");
                                            MSqlStr = "S.StkTranType = 'PR'";
                                        }
                                        break;
                                    case "Stock in":
                                        {
                                            MSqlStr = "S.StkTranType = 'GRN'";
                                        }
                                        break;
                                    case "Adjust Stock":
                                        {
                                            MSqlStr = "S.StkTranType = 'SA'";
                                        }
                                        break;
                                    case "Opening Balance":
                                        {
                                            MSqlStr = "S.StkTranType = 'OB'";
                                            dtTranHistory = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,S.StkTranPendingQty, S.StkTranQty - S.StkTranPendingQty as ReceivedQty, I.ReOrder FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND {MSqlStr} ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");
                                        }
                                        break;
                                }

                                if (MInvenRef != "Opening Balance")
                                {
                                    //dtTranHistory = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,S.StkTranPendingQty, S.StkTranPendingQty + S.StkTranQty as ReceivedQty, I.ReOrder, S.CreateBy, S.ModifyBy,S.ModifyDate, U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate");

                                    if (MInvenRef == "Adjust Stock")
                                    {
                                        dtTranHistory = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranID,S.StkTranDate,S.StkTranType,S.StkTranQty,S.StkTranPendingQty, S.StkTranPendingQty + S.StkTranQty AS ReceivedQty, I.ReOrder, S.CreateBy, S.ModifyBy,S.ModifyDate, U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate");
                                    }
                                    else
                                    {
                                        DateTime f1date = Convert.ToDateTime(FromDate).Date, t1date = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                                        string mRepDateStr = "";
                                        if (f1date != null && t1date != null)
                                        {
                                            if (f1date.ToString(sqlDateShorFormat) != t1date.ToString(sqlDateShorFormat))
                                            {
                                                mRepDateStr = " for the period from " + Convert.ToDateTime(FromDate).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(ToDate).ToString("dd-MMM-yyyy");
                                            }
                                            else
                                            {
                                                mRepDateStr = " for the day " + Convert.ToDateTime(FromDate).ToString("dd-MMM-yyyy");
                                            }
                                        }

                                        dtTranHistory = GetDataTable($"SELECT 'Stationery stock received list ' + '{ mRepDateStr }' as Header, GETDATE() as FromDate, GETDATE() As ToDate, '{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode, I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranID,S.StkTranDate,S.StkTranType,S.StkTranQty,S.CancelQty,S.StkTranPendingQty,S.StkTranQty - (S.CancelQty + S.StkTranPendingQty) AS ReceivedQty, CASE WHEN S.TranStatus = 'PR'THEN 'Pending' ELSE 'Sent' END AS TranStatus, I.ReOrder, S.CreateBy, S.ModifyBy,S.ModifyDate, U.UserName FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate");
                                    }
                                }
                            }
                            break;
                    }

                    var colheadstyle = new Style(typeof(DataGridColumnHeader));
                    var chstyleLeft = new Style(typeof(DataGridColumnHeader));
                    var chstyleRight = new Style(typeof(DataGridColumnHeader));
                    var chstyleCenter = new Style(typeof(DataGridColumnHeader));
                    var rowcellstyleRight = new Style(typeof(DataGridCell));
                    var rowcellstyleCenter = new Style(typeof(DataGridCell));

                    colheadstyle.Setters.Add(new Setter(ToolTipService.ToolTipProperty, "my tooltop"));
                    chstyleLeft.BasedOn = MyWind.TryFindResource("DataGridColumnHeaderStyleLeft") as Style;
                    chstyleRight.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicRight") as Style;
                    chstyleCenter.BasedOn = MyWind.TryFindResource("DataGridColHeaderStyleBasicCenter") as Style;
                    rowcellstyleRight.BasedOn = MyWind.TryFindResource("DataGridCellRightAlignStyle") as Style;
                    rowcellstyleCenter.BasedOn = MyWind.TryFindResource("DataGridCellCenterAlignStyle") as Style;

                    MyWind.dgTranHistory.ColumnHeaderStyle = colheadstyle;

                    MyWind.dgTranHistory.Columns.Clear();


                    switch (mRefFlag)
                    {
                        case "Stock History":
                            {

                                MyWind.dgTHistory.Columns.Clear();
                                MyWind.dgTHistory.ColumnHeaderStyle = colheadstyle;
                                MyWind.dgTHistory.BorderBrush = System.Windows.Media.Brushes.DarkGray;
                                MyWind.dgTHistory.BorderThickness = new Thickness(1);

                                lblTranViewHeadContent = "Stock History";

                                lblTHistoryFDateContent = "From Date"; lblTHistoryTDateContent = "To Date";
                                btnTShowContent = "View"; btnTPrintContent = "Print";

                                switch (MInvenRef)
                                {
                                    case "Purchase":
                                        {

                                            lblTranViewHeadContent = "Stock History - Purchase Request ";
                                            lblTHistoryFDateLeft = 90;

                                            MyWind.dgTHistory.Margin = new Thickness(2, -25, 0, 0); MyWind.dgTHistory.Width = 850; MyWind.dgTHistory.Height = 460;
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Order Date", Binding = new Binding("TranDate"), Width = 155, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Order By", Binding = new Binding("UserName"), Width = 60, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 170, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 50, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Order Quantity", Binding = new Binding("TranQty"), Width = 70, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Received Quantity", Binding = new Binding("Receive"), Width = 70, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Cancelled Quantity", Binding = new Binding("CancelQty"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Pending Quantity", Binding = new Binding("PendingQty"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "E-Mail Status", Binding = new Binding("TranStatus"), MaxWidth = 60, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Transaction Category", Binding = new Binding("TranType"), MaxWidth = 0, HeaderStyle = chstyleLeft, CellStyle = rowcellstyleRight });

                                            if (dtTranHistory.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                                {
                                                    THistoryList.Add(new THistoryModel
                                                    {
                                                        TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),
                                                        UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                                                        ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                                        ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                                        Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                                        TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                                        Receive = Convert.ToInt32(dtTranHistory.Rows[i]["ReceivedQty"]),
                                                        CancelQty = Convert.ToInt32(dtTranHistory.Rows[i]["CancelQty"]),
                                                        PendingQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranPendingQty"]),
                                                        ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                                        TranType = dtTranHistory.Rows[i]["StkTranType"].ToString(),
                                                        TranStatus = dtTranHistory.Rows[i]["TranStatus"].ToString(),
                                                        ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"])
                                                    });
                                                }
                                                (MyWind.dgTHistory.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                            }
                                        }
                                        break;
                                    case "Stock in":
                                        {

                                            lblTranViewHeadContent = "Stock History - Receive Stock";

                                            lblTHistoryFDateLeft = 90;

                                            MyWind.dgTHistory.Margin = new Thickness(5, -25, 0, 0); MyWind.dgTHistory.Width = 850; MyWind.dgTHistory.Height = 450;

                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Received Date", Binding = new Binding("TranDate"), Width = 155, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Received By", Binding = new Binding("UserName"), Width = 90, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 210, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 85, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Received Quantity", Binding = new Binding("TranQty"), Width = 140, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("Closing"), Width = 100, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), MaxWidth = 0, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Transaction Category", Binding = new Binding("TranType"), MaxWidth = 0, HeaderStyle = chstyleLeft, CellStyle = rowcellstyleRight });

                                            if (dtTranHistory.Rows.Count > 0)
                                            {

                                                Int32 mClosingQty = 0;
                                                string mTmpTranItemCode = "";

                                                DateTime frmdate = DateTime.Now;

                                                for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                                {

                                                    mTmpTranItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString();
                                                    using (dtTemp = new DataTable())
                                                    {
                                                    
                                                        dtTemp = GetDataTable($"SELECT I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder,ISNULL(O.OBQTY,0) AS OBQTY,ISNULL(R.RECQTY,0) AS RECQTY, ISNULL(S.SALQTY,0) AS SALQTY, ISNULL(E.EADJQTY,0) AS EADJQTY,ISNULL(T.SADJQTY,0) AS SADJQTY FROM tblItemMaster I " +
                                                                $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' AND ItemCode = '{mTmpTranItemCode}' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE   " +
                                                                $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                                                                $"LEFT JOIN (SELECT CNNum,SUM(Pieces) AS SALQTY FROM tblCSaleCNNum WHERE ShipmentType = 'STATIONERY' AND CNNum = '{mTmpTranItemCode}' AND CDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY CNNum) S ON I.ITEMCODE = S.CNNum " +
                                                                $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                                                                $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0  AND ItemCode = '{mTmpTranItemCode}' AND StkTranDate <= '{frmdate.ToString("MM/dd/yyyy HH:mm:ss:fff")}' GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE WHERE I.ITEMCODE = '{mTmpTranItemCode}'");
                                                    }

                                                    mClosingQty = (Convert.ToInt32(dtTemp.Rows[0]["OBQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["RECQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["EADJQTY"]) + Convert.ToInt32(dtTemp.Rows[0]["SADJQTY"])) - Convert.ToInt32(dtTemp.Rows[0]["SALQTY"]);

                                                    THistoryList.Add(new THistoryModel
                                                    {
                                                        TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),
                                                        UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                                                        ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                                        ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                                        TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                                        Closing = mClosingQty,
                                                        Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                                        ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                                        TranType = dtTranHistory.Rows[i]["StkTranType"].ToString(),
                                                        ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"])
                                                    });

                                                    dtTranHistory.AsEnumerable().Where(rec => Convert.ToInt32(rec["StkTranID"]) == Convert.ToInt32(dtTranHistory.Rows[i]["StkTranID"])).ToList().ForEach(rec => { rec["ReceivedQty"] = mClosingQty; });

                                                }
                                            (MyWind.dgTHistory.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                            }
                                        }
                                        break;
                                    case "Adjust Stock":
                                        {
                                            lblTranViewHeadContent = "Stock History - Stock Adjust";

                                            lblTranHistoryCatContent = "Select stock option";
                                            lblTHistoryFDateLeft = 90;
                                            MyWind.dgTHistory.Margin = new Thickness(20, -25, 0, 0); MyWind.dgTHistory.Width = 810; MyWind.dgTHistory.Height = 450;
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Adjusted Date", Binding = new Binding("TranDate"), Width = 150, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Adjusted by", Binding = new Binding("UserName"), Width = 80, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 190, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 50, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Current Stock", Binding = new Binding("PendingQty"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Adjusted Quantity", Binding = new Binding("TranQty"), Width = 80, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Adj. Current Stock", Binding = new Binding("Closing"), Width = 110, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), MaxWidth = 0, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Transaction Category", Binding = new Binding("TranType"), MaxWidth = 0, CellStyle = rowcellstyleRight, HeaderStyle = chstyleLeft });

                                            if (dtTranHistory.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                                {
                                                    //mStr = "    " + dtTranHistory.Rows[i]["Unit1"].ToString();
                                                    THistoryList.Add(new THistoryModel
                                                    {
                                                        TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),
                                                        UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                                                        ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                                        ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                                        PendingQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranPendingQty"]),
                                                        TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                                        Closing = Convert.ToInt32(dtTranHistory.Rows[i]["ReceivedQty"]),
                                                        Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                                        ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                                        TranType = dtTranHistory.Rows[i]["StkTranType"].ToString(),
                                                        ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"])
                                                    });
                                                }
                                            (MyWind.dgTHistory.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                            }
                                        }
                                        break;
                                    case "Opening Balance":
                                        {
                                            lblTranViewHeadContent = "Opening Balance";

                                            lblTranHistoryCatContent = "Select stock option";

                                            MyWind.dgTHistory.Margin = new Thickness(10, -25, 0, 0); MyWind.dgTHistory.Width = 540; MyWind.dgTHistory.Height = 450;

                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Opening Stock Entry date", Binding = new Binding("TranDate"), Width = 120, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "User Name", Binding = new Binding("UserName"), Width = 90, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 210, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Quantity", Binding = new Binding("TranQty"), Width = 100, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 60, HeaderStyle = chstyleLeft });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), MaxWidth = 0, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                            MyWind.dgTHistory.Columns.Add(new DataGridTextColumn { Header = "Transaction Category", Binding = new Binding("TranType"), MaxWidth = 0, HeaderStyle = chstyleLeft, CellStyle = rowcellstyleRight });

                                            if (dtTranHistory.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                                {
                                                    THistoryList.Add(new THistoryModel
                                                    {
                                                        TranDate = Convert.ToDateTime(dtTranHistory.Rows[i]["StkTranDate"]),
                                                        UserName = dtTranHistory.Rows[i]["UserName"].ToString(),
                                                        ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                                        ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                                        TranQty = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                                        Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                                        ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                                        TranType = dtTranHistory.Rows[i]["StkTranType"].ToString(),
                                                        ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"])
                                                    });
                                                }
                                            (MyWind.dgTHistory.Columns[0] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                            }

                                        }
                                        break;
                                }
                                cnvTHDispVisibility = "Visible";
                                gdTHistoryVisibility = "Visible";
                                btnTPrintVisibility = "Visible";

                                lblTHistoryFDateVisibility = "Visible"; dtpTHistoryFDateVisibility = "Visible";
                                lblTHistoryTDateVisibility = "Visible"; dtpTHistoryTDateVisibility = "Visible";
                                btnTShowVisibility = "Visible";

                            }
                            break;
                        case "Stock History1":
                            {
                                lblTranViewHeadContent = "Stock History";

                                lblTranHistoryCatContent = "Stock History";
                                lblTranHistoryFDateContent = "From Date"; lblTranHistoryTDateContent = "To Date";
                                btnShowContent = "View"; btnPrintContent = "Print";

                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), Width = 60, HeaderStyle = chstyleLeft });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 150, HeaderStyle = chstyleLeft });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 50, HeaderStyle = chstyleLeft });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Opening Stock", Binding = new Binding("Opening"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Purchase", Binding = new Binding("Purchase"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Excess", Binding = new Binding("Excess"), Width = 70, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Sales", Binding = new Binding("Sales"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Short", Binding = new Binding("Short"), Width = 70, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Closing Stock", Binding = new Binding("Closing"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), Width = 75, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });

                                if (dtTranHistory.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                    {
                                        TranHistoryList.Add(new TranHistoryModel
                                        {
                                            ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                            Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                            Purchase = Convert.ToInt32(dtTranHistory.Rows[i]["StkTranQty"]),
                                            ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                            ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"]),
                                            ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                        });
                                    }
                                    (MyWind.dgTranHistory.Columns[1] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                }
                            }
                            break;
                        case "Stock Status": case "":
                            {

                                MyWind.dgTranHistory.Columns.Clear();
                                MyWind.dgTranHistory.ColumnHeaderStyle = colheadstyle;
                                MyWind.dgTranHistory.BorderBrush = System.Windows.Media.Brushes.DarkGray;
                                MyWind.dgTranHistory.BorderThickness = new Thickness(1);

                                lblTranViewHeadContent = "";

                                lblTranHistoryCatContent = "";

                                lblTranHistoryFDateContent = ""; lblTranHistoryTDateContent = "";

                                btnShowContent = "View"; btnPrintContent = "Print";

                                MyWind.lblTranHistoryCat.Margin = new Thickness(0, 0, 0, 0);// MyWind.cbStockTranType.Width = 145; //(Default)
                                MyWind.cbStockTranType.Margin = new Thickness(-50, 0, 0, 0); MyWind.cbStockTranType.Width = 175; //Deaflt Width = 145

                                MyWind.dgTranHistory.Margin = new Thickness(5, 45, 0, 0); MyWind.dgTranHistory.Width = 850; MyWind.dgTranHistory.Height = 420;


                                MyWind.dgTranHistory.SelectionUnit = DataGridSelectionUnit.FullRow;


                                if (mHFlag == false)
                                {
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 175, HeaderStyle = chstyleLeft });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Opening Stock", Binding = new Binding("Opening"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stock In", Binding = new Binding("Purchase"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Sales", Binding = new Binding("Sales"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Adjusted Quantity", Binding = new Binding("Excess"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Closing Stock", Binding = new Binding("Closing"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), Width = 85, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 55, HeaderStyle = chstyleLeft });
                                }
                                else
                                {
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Item Code", Binding = new Binding("ItemCode"), MaxWidth = 0, HeaderStyle = chstyleLeft });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stationery", Binding = new Binding("ItemDesc"), Width = 95, HeaderStyle = chstyleLeft });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Transaction", Binding = new Binding("Action"), Width = 90, HeaderStyle = chstyleLeft });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Date", Binding = new Binding("TranDate"), Width = 90, HeaderStyle = chstyleCenter, CellStyle = rowcellstyleCenter });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Opening Stock", Binding = new Binding("Opening"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Stock In", Binding = new Binding("Purchase"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Sales", Binding = new Binding("Sales"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Adjusted Quantity", Binding = new Binding("Short"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Closing Stock", Binding = new Binding("Closing"), Width = 95, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Reorder Level", Binding = new Binding("ReOrderLevel"), MaxWidth = 0, HeaderStyle = chstyleRight, CellStyle = rowcellstyleRight });
                                    MyWind.dgTranHistory.Columns.Add(new DataGridTextColumn { Header = "Unit", Binding = new Binding("Unit1"), Width = 55, HeaderStyle = chstyleLeft });
                                }

                                if (dtTranHistory.Rows.Count > 0)
                                {
                                    string mTranType = "";

                                    Int32 mClosingQty = 0;

                                    for (int i = 0; i < dtTranHistory.Rows.Count; i++)
                                    {
                                        mClosingQty = (Convert.ToInt32(dtTranHistory.Rows[i]["OBQTY"]) + Convert.ToInt32(dtTranHistory.Rows[i]["RECQTY"]) + Convert.ToInt32(dtTranHistory.Rows[i]["EADJQTY"]) + Convert.ToInt32(dtTranHistory.Rows[i]["SADJQTY"])) - Convert.ToInt32(dtTranHistory.Rows[i]["SALQTY"]);

                                        TranHistoryList.Add(new TranHistoryModel
                                        {
                                            ItemDesc = dtTranHistory.Rows[i]["ItemDesc"].ToString(),
                                            Unit1 = dtTranHistory.Rows[i]["Unit1"].ToString(),
                                            Opening = Convert.ToInt32(dtTranHistory.Rows[i]["OBQTY"]),
                                            Purchase = Convert.ToInt32(dtTranHistory.Rows[i]["RECQTY"]),
                                            Sales = Convert.ToInt32(dtTranHistory.Rows[i]["SALQTY"]),
                                            Excess = Convert.ToInt32(dtTranHistory.Rows[i]["EADJQTY"]) + Convert.ToInt32(dtTranHistory.Rows[i]["SADJQTY"]),
                                            Closing = mClosingQty,
                                            ReOrderLevel = Convert.ToInt32(dtTranHistory.Rows[i]["ReOrder"]),
                                            ItemId = Convert.ToInt32(dtTranHistory.Rows[i]["ItemId"]),
                                            ItemCode = dtTranHistory.Rows[i]["ItemCode"].ToString(),
                                        });
                                    }
                                    mTranItemCode = "";
                                }

                                cnvTranHistoryVisibility = "Visible";
                                lblTranViewHeadVisibility = "Collapsed";
                                gdTranHistoryVisibility = "Visible";
                                btnPrintVisibility = "Visible";

                                lblTranHistoryCatVisibility = "Collapsed"; cbStockTranTypeVisibility = "Collapsed";
                                lblTranHistoryFDateVisibility = "Collapsed"; dtpTranHistoryFDateVisibility = "Collapsed";
                                lblTranHistoryTDateVisibility = "Collapsed"; dtpTranHistoryTDateVisibility = "Collapsed";

                                btnShowVisibility = "Collapsed";
                            }
                            break;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Load Transaction History : " + Ex.Message); }
        }

        public void AddTranQtytoList()
        {
            int mNo = 1;

            try
            { 
                mTempQty = MyWind.txtTranItemQty.Text == "" ? 0 : Convert.ToInt32(MyWind.txtTranItemQty.Text);

                mTmpFlag = true;
                if (StationeryItem == "" || mTempQty <= 0)
                {
                    mTmpFlag = false;
                }
                else
                    if (MInvenRef == "Purchase")
                {
                    if (mMaxOrdQty <= 0 || mMaxOrdQty < mTempQty)
                    {
                        mTmpFlag = false;
                    }
                }

                if (mTmpFlag)
                {

                    foreach (DataRow item in dtItemSummary.Rows)
                    {
                        if (item["ColOneText"].ToString().ToUpper() == mTranItemCode)
                        {
                            bool mSaveFlag = false;
                            double mPendingQty = 0;

                        
                            item["Quantity"] = Convert.ToInt32(TranQty);

                            mPendingQty = (mStkTranType == "PR" ? Convert.ToInt32(TranQty) : 0);

                            mTranQty = mCStockQty;
                            mTranQty = (mStkTranType == "SA" ? Convert.ToInt32(TranQty) - mTranQty : TranQty);

                            using (dtStockItem = new DataTable())
                            {
                            
                                dtStockItem = GetDataTable($"SELECT I.*,S.Closing FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.Status = 1 AND S.Status = 1 AND I.ItemCode = '{mTranItemCode}'");
                                if (dtStockItem.Rows.Count > 0)
                                {

                                    if (MInvenRef == "Adjust Stock")
                                    {
                                        mPendingQty = Convert.ToInt32(dtStockItem.Rows[0]["Closing"]);

                                        if (TranQty < mPendingQty)
                                        {
                                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("This will reduce stock quantity from " + mPendingQty + " to " + TranQty + "?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
                                            if (MsgConfirmation == MessageBoxResult.Yes) 
                                            {
                                                mTranQty = Convert.ToInt32(TranQty) - Convert.ToInt32(mPendingQty);
                                                mSaveFlag = true;  
                                            }
                                        }
                                        else if (TranQty > mPendingQty)
                                        {
                                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("This will increase stock quantity from " + mPendingQty + " to " + TranQty + "?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
                                            if (MsgConfirmation == MessageBoxResult.Yes) 
                                            {
                                                mTranQty = Convert.ToInt32(TranQty) - Convert.ToInt32(mPendingQty);
                                                mSaveFlag = true; 
                                            }
                                        }
                                        else
                                        {
                                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Both quantity were same ! Changes not saved.", "Information", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                                            mSaveFlag = false;
                                        }
                                    }
                                    else
                                    { mSaveFlag = true; }

                                    if (mSaveFlag == true)
                                    {

                                        //              StkTranType         StkTranRefNum         StkTranDate     ItemId              ItemCode        UNIT1           UNIT2
                                        //              StkTranQty          Rate                DiscountPer     DiscountAmount      TaxPer          TaxAmount
                                        //              TranAmount          CancelQty         StkTranPendingQty  TranStatus          StkTranDesc     StkTranRemarks
                                        //              CreateBy            ModifyBy            ModifyDate   IsExported          ExportedDate
                                        if (EditItem == false)
                                        {
                                            ExecuteQuery($"INSERT INTO tblStockTran(StkTranType,StkTranRefNum,StkTranDate,ItemId,ItemCode,UNIT1,UNIT2,StkTranQty,Rate,DiscountPer,DiscountAmount,TaxPer,TaxAmount,TranAmount,CancelQty,StkTranPendingQty,TranStatus,StkTranDesc,StkTranRemarks,CreateBy,ModifyBy,ModifyDate,EMailBy,EMailDate,IsExported,ExportedDate)" +
                                                            $"VALUES('{mStkTranType}'," +
                                                            $" FORMAT(NEXT VALUE FOR {mSeqName}, '{mPrefix}000000') ," +
                                                            $" GETDATE()," +
                                                            $" {Convert.ToInt32(dtStockItem.Rows[0]["ItemId"])}, " +
                                                            $" '{dtStockItem.Rows[0]["ItemCode"].ToString()}', " +
                                                            $" '{dtStockItem.Rows[0]["UNIT1"].ToString()}', " +
                                                            $" '{dtStockItem.Rows[0]["UNIT2"].ToString()}', " +
                                                            $" {mTranQty} ," +
                                                            $" {Convert.ToDecimal(dtStockItem.Rows[0]["Unit1Cost"])}, " +
                                                            $" 0," +
                                                            $" 0," +
                                                            $" 0," +
                                                            $" 0," +
                                                            $" 0," +
                                                            $" 0," +
                                                            $" {mPendingQty} ," +
                                                            $" '{mStkTranType}' ," +
                                                            $" '{MInvenRef}' ," +
                                                            $" '{txtTranRemarks}' ," +
                                                            $" {LoginUserId}," +
                                                            $" 0," +
                                                            $" NULL," +
                                                            $" 0," +
                                                            $" NULL," +
                                                            $" 0," +
                                                            $" NULL)");
                                        }
                                        else
                                        {

                                            foreach (DataRow row in dtItemSummary.Rows)
                                            {
                                                if (row["ColOneText"].ToString() == mTranItemCode)
                                                {
                                                    ExecuteQuery($"UPDATE tblStockTran SET StkTranQty = {mTranQty}, StkTranPendingQty = {mTranQty}, ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE StkTranID = {Convert.ToInt32(row["StkTranID"])}");
                                                    break;
                                                }
                                            }
                                        }

                                        //  UPDATE STOCK TABLE 
                                        //              GRN         ->  ADD Purchase + ADD Closing
                                        //              Excess      ->  ADD Excess + ADD Closing
                                        //              Short       ->  DEDUCT Short + DEDUCT Closing
                                        //              Closing

                                        if (MInvenRef != "Purchase")
                                        {
                                            if (Convert.ToInt32(item["Quantity"]) > 0)
                                            {
                                                //TranItemList.Add(new TranListModel { itemno = mNo, ItemCode = item["ColOneText"].ToString(), ItemDesc = item["StationeryName"].ToString().ToUpper(), Unit1 = item["UOM"].ToString(), rate = Convert.ToDecimal(item["Rate"]).ToString("0.00"), OtherCharges = "-", Insurans = "-", quantity = item["Quantity"].ToString(), discount = "-", total = TotSum.ToString() });
                                                TranItemList.Add(new TranListModel { ItemNo = mNo, ItemCode = item["ColOneText"].ToString(), ItemDesc = item["StationeryName"].ToString().ToUpper(), Unit1 = item["UOM"].ToString(), TranQty = TranQty, Closing = Convert.ToInt32(dtStockItem.Rows[0]["Closing"]), ReOrderLevel = Convert.ToInt32(dtStockItem.Rows[0]["ReOrder"]) });
                                                mNo += 1;
                                                (MyWind.dgTranList.Columns[1] as DataGridTextColumn).Binding.StringFormat = "dd-MMM-yyyy hh:mm tt";
                                            }
                                        }

                                        switch (mStkTranType)
                                        {
                                            case "GRN":
                                                {

                                                    //ExecuteQuery($"UPDATE tblStock SET Purchase = purchase + {TranQty}, Closing = Closing + {TranQty} WHERE Status = 1 AND ItemId = {Convert.ToInt32(dtStockItem.Rows[0]["ItemId"])}");
                                                    ExecuteQuery($"UPDATE tblStock SET Purchase = purchase + {TranQty}, Closing = Closing + {TranQty} WHERE Status = 1 AND ItemCode = '{mTranItemCode}'");

                                                    mPendingQty = Convert.ToInt32(TranQty);

                                                    using (SqlDataReader dr = GetDataReader($"SELECT * FROM tblStockTran WHERE StkTranID = {mStkId}"))
                                                    {
                                                        if (dr.HasRows)
                                                        {
                                                            while (dr.Read())
                                                            {

                                                                if (mPendingQty <= Convert.ToInt32(dr["StkTranPendingQty"]))
                                                                {
                                                                    ExecuteQuery($"UPDATE tblStockTran SET StkTranPendingQty = StkTranPendingQty - {mPendingQty} WHERE StkTranId = {Convert.ToInt32(dr["StkTranId"])} AND ItemCode = '{mTranItemCode}'");
                                                                    mPendingQty = 0;
                                                                }
                                                                else
                                                                {
                                                                    ExecuteQuery($"UPDATE tblStockTran SET StkTranPendingQty = 0 WHERE StkTranId = {Convert.ToInt32(dr["StkTranId"])} AND ItemCode = '{mTranItemCode}'");
                                                                    mPendingQty = mPendingQty - Convert.ToInt32(dr["StkTranPendingQty"]);
                                                                }

                                                                if (mPendingQty == 0)
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                            dr.Close();
                                                        }
                                                    }

                                                    if (mPendingQty > 0)
                                                    {
                                                        using (SqlDataReader dr = GetDataReader($"SELECT * FROM tblStockTran WHERE StkTranType = 'PR' AND ItemId = {Convert.ToInt32(dtStockItem.Rows[0]["ItemId"])} AND StkTranPendingQty > 0"))
                                                        {
                                                            if (dr.HasRows)
                                                            {
                                                                while (dr.Read())
                                                                {

                                                                    if (mPendingQty <= Convert.ToInt32(dr["StkTranPendingQty"]))
                                                                    {
                                                                        ExecuteQuery($"UPDATE tblStockTran SET StkTranPendingQty = StkTranPendingQty - {mPendingQty} WHERE StkTranId = {Convert.ToInt32(dr["StkTranId"])} AND ItemCode = '{mTranItemCode}'");
                                                                        mPendingQty = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        ExecuteQuery($"UPDATE tblStockTran SET StkTranPendingQty = 0 WHERE StkTranId = {Convert.ToInt32(dr["StkTranId"])} AND ItemCode = '{mTranItemCode}'");
                                                                        mPendingQty = mPendingQty - Convert.ToInt32(dr["StkTranPendingQty"]);
                                                                    }

                                                                    if (mPendingQty == 0)
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                                dr.Close();
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case "SA":
                                                {
                                                    ////using (SqlDataReader dr = GetDataReader($"SELECT S.* FROM tblItemMaster AS I JOIN tblStock AS S ON I.ItemId = S.ItemId WHERE I.ItemCode = '{mTranItemCode}' AND I.Status = 1 AND S.Status = 1"))
                                                    //using (SqlDataReader dr = GetDataReader($"SELECT * FROM tblStock WHERE Status = 1 AND ItemId = {Convert.ToInt32(dtStockItem.Rows[0]["ItemId"])}"))
                                                    using (SqlDataReader dr = GetDataReader($"SELECT * FROM tblStock WHERE Status = 1 AND ItemCode = '{mTranItemCode}'"))
                                                    {
                                                        if (dr.HasRows)
                                                        {

                                                            //ExecuteQuery($"UPDATE tblStock SET ModifyBy = {LoginUserId}, ModifyDate = GETDATE(), Status = 0 WHERE Status = 1 AND ItemId = {Convert.ToInt32(dtStockItem.Rows[0]["ItemId"])}");

                                                            if (TranQty > mPendingQty)
                                                            {
                                                                ExecuteQuery($"UPDATE tblStock SET StockAdj = StockAdj + {mTranQty}, Excess = Excess + {mTranQty}, Closing = Closing + {mTranQty}, ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE Status = 1 AND ItemCode = '{mTranItemCode}'");
                                                            }
                                                            else if (TranQty < mPendingQty)
                                                            {
                                                                ExecuteQuery($"UPDATE tblStock SET StockAdj = StockAdj + {mTranQty}, Short = Short + {mTranQty}, Closing = Closing + {mTranQty}, ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE Status = 1 AND ItemCode = '{mTranItemCode}'");
                                                            }

                                                            //if (dr.Read())
                                                            //{
                                                            //    //  ItemId,Opening,Purchase,PurchaseRet,Receive,Issue,Sales,SalesRet,Excess,Short,Closing
                                                            //    //  IsExported,ExportedDate,StockAdj,Remarks,CreateBy,CreateDate,ModifyBy,ModifyDate,Status

                                                            //    //txtTranRemarks = "Test Remarks ";

                                                            //    ExecuteQuery($"INSERT INTO tblStock(ItemId,ItemCode,Opening,Purchase,PurchaseRet,Receive,Issue,Sales,SalesRet,Excess,Short,Closing,IsExported,ExportedDate,StockAdj,Remarks,CreateBy,CreateDate,ModifyBy,ModifyDate,Status)" +
                                                            //                    $"VALUES({Convert.ToInt32(dr["ItemId"])} ," +
                                                            //                    $" '{mTranItemCode}'," +
                                                            //                    $" {Convert.ToInt32(dr["Opening"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["Purchase"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["PurchaseRet"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["Receive"])}," +
                                                            //                    $" {Convert.ToInt32(dr["Issue"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["Sales"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["SalesRet"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["Excess"])} ," +
                                                            //                    $" {Convert.ToInt32(dr["Short"])} ," +
                                                            //                    $" {TranQty} ," +
                                                            //                    $" 0," +
                                                            //                    $" NULL," +
                                                            //                    $" 0," +
                                                            //                    $" '{txtTranRemarks}' ," +
                                                            //                    $" {LoginUserId}," +
                                                            //                    $" GETDATE() ," +
                                                            //                    $" 0," +
                                                            //                    $" NULL," +
                                                            //                    $" 1)");
                                                            //}
                                                        }
                                                        dr.Close();
                                                    }
                                                }
                                                break;
                                        }

                                        lblSysStockQtyVisibility = "Collapsed";
                                        lblSysStockQtyContent = "";
                                        txtTranRemarks = "";
                                        lblSysStockQtyContent = "Current Stock : ";
                                        lblReOrderLevelContent = "Reorder level      : ";
                                        lblCMaxOrderLimitContent = "";
                                        lblMaxLimitContent = "Maximum Stock : ";

                                        if (MInvenRef == "Purchase") { LoaddgTranList(); }

                                    }
                                    //else
                                    //{ MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Changes not made !", "Information", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK); }
                                }

                                //  SAVE -> PR/PA/PO/GRN/ES/SS/OB

                                //  TABLE NAME: tblStockDetails
                                //  COLUMNS:    StkTranID
                                //              StkTranType     StkTranDate         ItemId ItemCode     UNIT1           UNIT2
                                //              StkTranQty      Rate                DiscountPer         DiscountAmount  TaxPer          TaxAmount TranAmount
                                //              CancelQty     StkTranPendingQty      TranStatus          StkTranDesc
                                //              CreateBy        ModifyBy            ModifyDate       IsExported      ExportedDate

                            }
                        }
                    }
                    if (!btnSaveTranEnable) { btnSaveTranEnable = true; }

                    StationeryItem = ""; TranQty = 0; lblUOMContent = "";
                    mTranItemCode = "";
                    if (mDashBoardFlag == false)
                    {
                        MyWind.scvStationeryItem.Focus();
                    }
                    else
                    {
                        mDashBoardFlag = false;
                        LoadDashBoard("");
                    }
                }
                else
                {
                    mTmpFlag = false;
                    if (StationeryItem == "")
                    { MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Please select stationery item !", "Information", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else
                    {
                        if (mTempQty <= 0)
                        {
                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Order quantity should be entered to procced !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            mTmpFlag = true;
                        }
                        else if (mMaxOrdQty == 0)
                        {
                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("You have sufficient stock. Can't place order !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            mTmpFlag = true;
                        }
                        else if (mMaxOrdQty < mTempQty)
                        {
                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Order quantity exceeds limit !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            mTmpFlag = true;
                        }

                        if (mTmpFlag)
                        {
                            MyWind.txtTranItemQty.SelectAll(); MyWind.txtTranItemQty.Focus();
                        }
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Save/Update : " + Ex.Message); }
        }


        public void UpdateCancelQty()
        {
            try
            { 
                mTranItemCode = SelectedItemInfo.ItemCode;
                mTempQty = Convert.ToInt32(SelectedItemInfo.PendingQty);
                mStkId = SelectedItemInfo.StkTranID;
                if (mTranItemCode != "" && mStkId > 0 && mTempQty > 0)                
                {

                    MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Cancel pending quantity " + mTempQty.ToString() + " from list ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgConfirmation == MessageBoxResult.Yes)
                    {
                        mStkTranType = SelectedItemInfo.TranType;
                        //UpDtPendingStatus(mTranItemCode);
                        ExecuteQuery($"UPDATE tblStockTran SET CancelQty = CancelQty + {mTempQty},StkTranPendingQty = StkTranPendingQty - {mTempQty},TranStatus = 'ND' WHERE StkTranID = {mStkId}");
                        ResetData("");
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        //  1. Remove Item - 
        private void RemoveItem(string ProcessFlag)
        {
            try
            { 
                if (ProcessFlag == "Remove Item")
                {
                    foreach (DataRow item in dtItemSummary.Rows)
                    {
                        MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Remove from entry ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (MsgConfirmation == MessageBoxResult.Yes)
                        {
                            if (item["ColOneText"].ToString().ToUpper() == SelectedTranListItem.ItemCode.ToUpper())
                            {
                                item["Quantity"] = Convert.ToInt32(item["Quantity"]) - 1;
                                item["Total"] = Convert.ToInt32(item["Quantity"]) * Convert.ToDouble(item["Rate"]);
                                ////if (SelectedTranListItem.quantity == "0") { TranItemList.Remove(SelectedTranListItem); }
                                //if (SelectedTranListItem.TranQty == "0") { TranItemList.Remove(SelectedTranListItem); }
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Remove Item : " + Ex.Message); }
        }


        public void PrintStokReport(string pPrintOpt)
        {
            DataTable dt = new DataTable();
            string rptFileName = "";
            string MSqlStr = "";

            try
            {
                DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                string mRepDateStr = "";
                if (fdate != null && tdate != null)
                {
                    if (fdate.ToString(sqlDateShorFormat) != tdate.ToString(sqlDateShorFormat))
                    {
                        mRepDateStr = " for the period from " + Convert.ToDateTime(FromDate).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(ToDate).ToString("dd-MMM-yyyy");
                    }
                    else
                    {
                        if (MInvenRef == "Stock Status")
                        {
                            mRepDateStr = " as on " + Convert.ToDateTime(FromDate).ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            mRepDateStr = " for the day " + Convert.ToDateTime(FromDate).ToString("dd-MMM-yyyy");
                        }
                    }
                }

                switch (pPrintOpt)
                {
                    case "Stock History":
                        {
                            ////////////////////////////////////////////////////////////////////////////
                            switch (MInvenRef)
                            {
                                case "Purchase":
                                    {
                                        MSqlStr = "S.StkTranType = 'PR'";
                                        rptFileName = "RPTStanPurchaseRequest.rdlc";
                                        dt = GetDataTable($"SELECT 'Stationery purchase request list ' + '{ mRepDateStr }' as Header, GETDATE() as FromDate, GETDATE() As ToDate, I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,S.CancelQty,S.StkTranPendingQty as TranPendingQty, S.StkTranQty - (S.CancelQty + S.StkTranPendingQty) as ReceivedQty, CASE WHEN S.TranStatus='PR' THEN 'Pending' ELSE 'Sent' END AS TranStatus, I.ReOrder AS ReOrderLevel,S.CreateBy,S.ModifyBy,S.ModifyDate,U.UserName,'{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId " +
                                            $" WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");
                                    }
                                    break;
                                case "Stock in":
                                    {
                                        MSqlStr = "S.StkTranType = 'GRN'";
                                        rptFileName = "RPTStanStockIn.rdlc";
                                        dt = GetDataTable($"SELECT 'Stationery stock received list ' + '{ mRepDateStr }' as Header, GETDATE() as FromDate, GETDATE() As ToDate, I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,S.StkTranPendingQty, S.StkTranQty - S.StkTranPendingQty as ReceivedQty, I.ReOrder AS ReOrderLevel,S.CreateBy,S.ModifyBy,S.ModifyDate,U.UserName,'{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId " +
                                            $" WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");
                                    }
                                    break;
                                case "Adjust Stock":
                                    {
                                        MSqlStr = "S.StkTranType = 'SA'";
                                        rptFileName = "RPTStanStockAdjust.rdlc";
                                        dt = GetDataTable($"SELECT 'Stationery stock adjustment list ' + '{ mRepDateStr }' as Header, GETDATE() as FromDate, GETDATE() As ToDate, I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,S.StkTranPendingQty as TranPendingQty, S.StkTranPendingQty + S.StkTranQty AS Closing, I.ReOrder AS ReOrderLevel,S.CreateBy,S.ModifyBy,S.ModifyDate,U.UserName,'{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId " +
                                            $" WHERE I.Status = 1 AND {MSqlStr} AND (S.StkTranDate >= '{fdate.ToString(sqlDateLongFormat)}' AND S.StkTranDate <= '{tdate.ToString(sqlDateLongFormat)}') ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");

                                    }
                                    break;
                                case "Opening Balance":
                                    {
                                        MSqlStr = "S.StkTranType = 'OB'";
                                        rptFileName = "RPTStanOBList.rdlc";
                                        dt = GetDataTable($"SELECT 'Opening stock list of Stationery' as Header, GETDATE() as FromDate, GETDATE() As ToDate, I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate As TranDate,S.StkTranType,S.StkTranQty AS Opening,I.ReOrder,S.CreateBy,S.ModifyBy,S.ModifyDate,U.UserName,'{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId JOIN tblUserMaster AS U ON S.CreateBy = U.UserId " +
                                            $" WHERE I.Status = 1 AND {MSqlStr} ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");
                                    }
                                    break;
                            }
                        }
                        break;
                    
                    case "Opening Balance":
                        {
                            rptFileName = "RPTStanOBList.rdlc";
                            dt = GetDataTable($"SELECT 'Opening stock list of Stationery ' +  as Header, GETDATE() as FromDate, GETDATE() As ToDate, SELECT I.ItemId,I.ItemCode,I.ItemDesc,S.UNIT1,S.StkTranDate,S.StkTranType,S.StkTranQty,I.ReOrder,S.CreateBy,S.ModifyBy,S.ModifyDate,U.UserName,'{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblItemMaster As I JOIN tblStockTran As S ON I.ItemId = S.ItemId " +
                                $" WHERE I.Status = 1 AND S.StkTranType = 'OB' ORDER BY S.StkTranDate,S.StkTranType,I.ItemCode");

                        }
                        break;

                    case "Stock Status":
                        {

                            rptFileName = "RPTStanStockSummary.rdlc";
                            using (dtTranHistory = new DataTable())
                            {
                                switch (MInvenRef)
                                {
                                    case "Stock Status":
                                    case "":
                                        {
                                            dt.Clear();
                                            dt = new DataTable();
                                            dt = GetDataTable($"SELECT 'Stock Status list of Stationery ' + '{ mRepDateStr }' as Header, GETDATE() as FromDate, GETDATE() As ToDate, I.ItemId,I.ItemCode,I.ItemDesc,I.Unit1,I.ReOrder AS ReOrderLevel,ISNULL(O.OBQTY,0) AS Opening,ISNULL(P.PURQTY,0) AS PURQTY,ISNULL(R.RECQTY,0) AS Purchase, ISNULL(S.SALQTY,0) AS Sales, ISNULL(E.EADJQTY,0) + ISNULL(T.SADJQTY,0) AS SAAdd, ISNULL(T.SADJQTY,0) AS SADed FROM tblItemMaster I " +
                                                                         $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS OBQTY FROM tblStockTran WHERE StkTranType = 'OB' GROUP BY ItemCode) O ON I.ITEMCODE = O.ITEMCODE " +
                                                                         $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS PURQTY FROM tblStockTran WHERE StkTranType = 'PR' GROUP BY ItemCode) P ON I.ITEMCODE = P.ITEMCODE " +
                                                                         $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS RECQTY FROM tblStockTran WHERE StkTranType = 'GRN' GROUP BY ItemCode) R ON I.ITEMCODE = R.ITEMCODE " +
                                                                         $"LEFT JOIN(SELECT ItemCode, SALES AS SALQTY FROM tblStock WHERE STATUS = 1) S ON I.ITEMCODE = S.ITEMCODE " +
                                                                         $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS EADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty >= 0 GROUP BY ItemCode) E ON I.ITEMCODE = E.ITEMCODE " +
                                                                         $"LEFT JOIN (SELECT ItemCode,SUM(StkTranQty) AS SADJQTY FROM tblStockTran WHERE StkTranType = 'SA' AND StkTranQty < 0 GROUP BY ItemCode) T ON I.ITEMCODE = T.ITEMCODE ORDER BY I.ItemDesc" );
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                }

                if (dt.Rows.Count > 0)
                {
                    DataTable dtCompany = new DataTable();
                    dtCompany = GetDataTable($"SELECT  a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,'') , Address3 = ISNULL(a.Address3,'') , City = ISNULL(a.City,'') , State = ISNULL(a.State,'') , Country =ISNULL(a.Country,'') , PostalCode = ISNULL(a.ZipCode,'')  , PhoneNo =  ISNULL(a.PhoneNum,'') , FaxNo =ISNULL(a.Fax,'')  , EMail = ISNULL(a.EMail,'') , a.Status  FROM tblStationProfile a" +
                        $" WHERE RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");

                    View.Reports.FrmReportViewer RptViewerGen = new View.Reports.FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;

                    if (MInvenRef == "Stock in")
                    {
                        Reports.ReportViewerVM VM = new Reports.ReportViewerVM("CompanyMas", dtCompany, "StationeryStock", dtTranHistory, "", dtTranHistory, "", dtTranHistory, rptFileName, "", RptViewerGen.RptViewer, false, true);
                        RptViewerGen.DataContext = VM;
                        RptViewerGen.Show();
                    }
                    else
                    {
                        Reports.ReportViewerVM VM = new Reports.ReportViewerVM("CompanyMas", dtCompany, "StationeryStock", dt, "", dt, "", dt, rptFileName, "", RptViewerGen.RptViewer, false, true);
                        RptViewerGen.DataContext = VM;
                        RptViewerGen.Show();
                    }
                }
            }
            catch (Exception ex) { error_log.errorlog($"CheckinAndPODVM.PrintReport({MInvenRef}) : " + ex.ToString()); }
        }


        public void ProcessSendEmail(string pEmail)
        {

            string mEMailId = "", mCCEMailId = "";
            string BodyMessage = "";
            string mSenderStn = "";
            //string mCostCetre = "", mCashSalesAcNo = "";
            string mCashSalesAcNo = "";

            try
            { 
                if (MyCompanySubName.Trim() != "" && MyCompanyCode.Trim() != "")
                {
                    mSenderStn = MyCompanySubName.Trim().ToUpper() + " - "  + MyCompanyCode.Trim().ToUpper();
                }
                else if (MyCompanySubName.Trim() == "" && MyCompanyCode.Trim() != "")
                {
                    mSenderStn = MyCompanyCode.Trim().ToUpper();
                }
                else if (MyCompanySubName.Trim() != "" && MyCompanyCode.Trim() == "")
                {
                    mSenderStn = MyCompanySubName.Trim().ToUpper();
                }


                //dtTemp
                mEMailId = ""; mCCEMailId = "";
                try
                {
                    using (dtTemp = new DataTable())
                    {
                        dtTemp = GetDataTable($"SELECT ParamName,ParamValue FROM SystemParams WHERE ParamName LIKE 'PurOrd%'");
                        for (int i = 0; i < dtTemp.Rows.Count; i++)
                        {
                            if (dtTemp.Rows[i]["ParamName"].ToString() == "PurOrdEmailIdToPIC")
                            {
                                mEMailId = dtTemp.Rows[i]["ParamValue"].ToString();
                            }
                            if (dtTemp.Rows[i]["ParamName"].ToString() == "PurOrdCcEmailId")
                            {
                                mCCEMailId = dtTemp.Rows[i]["ParamValue"].ToString();
                            }
                        }
                    }
                }
                catch (Exception Ex) {error_log.errorlog("Get E-Mail ID : " + Ex.Message);}

                try
                {
                    //mCostCetre = "-";
                    mCashSalesAcNo = "-";
                    SqlDataReader dr = GetDataReader($"SELECT CashAccNum FROM tblStationProfile WHERE RTRIM(CompCode) = '{MyCompanyCode.Trim()}'");
                    if (dr.Read())
                    {
                        //mCostCetre = dr[""].ToString();
                        mCashSalesAcNo = dr["CashAccNum"].ToString();
                    }
                    dr.Close();
                }
                catch (Exception Ex) { error_log.errorlog("Get WIC A/c number : " + Ex.Message); }


                if (mEMailId != "" || mCCEMailId != "")
                {

                    BodyMessage = "<!DOCTYPE html><html><head>";

                    BodyMessage += "</head><body>";
                    BodyMessage += "<div align='left'><p><img src='https://www.imgur.com/Qy9WpNo.png' alt='logo' border='0' height='60' width='200'></p></div>";

                    BodyMessage += "<h3 style=\'font-family:arial\'>Stationery purchase order</h3>";

                    BodyMessage += $"<p style=\'font-family:arial;font-size:14px\'>Date      : {DateTime.Now.ToString("dd-MMM-yyyy")}<br>From : {mSenderStn}</p>" +
                    $"<p style=\'font-family:arial;font-size:14px\'>WIC A/c Number : {mCashSalesAcNo}</p>";
                    //$"<p style=\'font-family:arial;font-size:14px\'>Cost centre : {mCashSalesAcNo}</p>";

                    //BodyMessage += "<br>";
                    BodyMessage += $"<p style=\'font-family:arial;font-size:14px\'>Dear Ms.Michelle,</p>" +
                    $"<p style=\'font-family:arial;font-size:14px\'>Please be informed that below stationery items need to be ordered as per our inventory system shows stock under reorder level at {mSenderStn}(Cash Sales).</p>";

                    BodyMessage += "<br>";
                    BodyMessage += "<table style=\'border-collapse:collapse; text-align:center;\'>" +

                    "<tr style=\'background-color:#6FA1D2; color:#ffffff;\'>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>#</th>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>Stationery Name</th>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>Order Quantity</th>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>Unit</th>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>Order Summary</th>" +
                    "<th style=\'font-family:arial;font-size:12px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>Sales Summary</th>" +
                    "</tr>";

                    string mItem = TranItemList[0].ItemDesc;
                    int mCtr = 1;

                    for (int i = 0; i < TranItemList.Count; i++)
                    {

                        BodyMessage += "<tr>" +
                                    $"<td rowspan='3' valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>{i + 1}</td>" +
                                    $"<td rowspan='3' valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].ItemDesc}</td>" +
                                    $"<td rowspan='3' valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].TranQty}</td> " +
                                    $"<td rowspan='3' valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Unit1}</td>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Purchase1}</td>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Sales1}</td>" +
                                    "</tr>";

                        BodyMessage += "<tr>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Purchase2}</td>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Sales2}</td>" +
                                    "</tr>";

                        BodyMessage += "<tr>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Purchase3}</td>" +
                                    $"<td valign='top' style=\'font-family:arial;font-size:11px;border-color:#5c87b2;border-style:solid;text-align:left;border-width:thin;spacing:5px;padding:5px;\'>{TranItemList[i].Sales3}</td>" +
                                    "</tr>";
                    }
                    BodyMessage += "</table>";

                    BodyMessage += "<br>" +
                    "<p style=\'font-family:arial;font-size:14px\'>Kindly confirm the purchase order for the above listed stationery items.</p>" +
                    "<br>" +
                    $"<p style=\'font-family:arial;font-size:14px\'>Thank you<br>Skynet Cash Sales {MyCompanyCode.Trim()}</p>";
                    //BodyMessage += $"<p><input type='button' onclick = 'window.print()' value='Print Order'></p>";
                    BodyMessage += "</body></html>";

                    new SendEmail(mEMailId, mCCEMailId, $"Cash Sales System - Stationery Order from { mSenderStn }", BodyMessage, "");

                    for (int i = 0; i < TranItemList.Count; i++)
                    {
                        ExecuteQuery($"UPDATE tblStockTran SET TranStatus = 'EM', StkTranRemarks = 'E-Mail Sent', EMailBy = {LoginUserId}, EMailDate = GetDate() WHERE StkTranId = {Convert.ToInt32(TranItemList[i].StkTranID)}");
                    }

                    TranItemList.Clear();

                    btnSaveTranEnable = false;

                    if (mDashBoardFlag == false)
                    {
                        MyWind.scvStationeryItem.Focus();
                    }
                    else
                    {
                        mDashBoardFlag = false;
                        LoadDashBoard("");
                    }
                }
                else
                {
                    MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("E-Mail ID not found! Please contact SCS Support.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Process E-Mail : " + Ex.Message); }
        }

        private void EnableDisableControl(string mStr, int mTranQty)
        {
            if (mTranQty > 0)
            { btnAddTranEnable = true; }
            else
            { btnAddTranEnable = false; }
        }

        //private int mselectedItem;
        //public int SelectedItem
        //{
        //    get { return mselectedItem; }
        //    set
        //    {
        //        mselectedItem = value;
        //    }
        //}

        public class StationeryModelI : PropertyChangedNotification
        {
            //  int itemno                  string itemcode    string itemname      string UOM          string quantity
            //  string sst                  string discount     string total        string Insurans     string OtherCharges    string Color
            private int _itemno;
            public int itemno
            {
                get { return _itemno; }
                set { _itemno = value; OnPropertyChanged("itemno"); }
            }

            private string _itemcode;
            public string itemcode
            {
                get { return _itemcode; }
                set { _itemcode = value; OnPropertyChanged("itemcode"); }
            }

            //Description
            private string _itemname;
            public string itemname
            {
                get { return _itemname; }
                set { _itemname = value; OnPropertyChanged("itemname"); }
            }
            private string _UOM;
            public string UOM
            {
                get { return _UOM; }
                set { _UOM = value; OnPropertyChanged("UOM"); }
            }

            private string _rate;
            public string rate
            {
                get { return _rate; }
                set { _rate = value; OnPropertyChanged("rate"); }
            }
            private string _quantity;
            public string quantity
            {
                get { return _quantity; }
                set { _quantity = value; OnPropertyChanged("quantity"); }
            }
            private string _sst;
            public string sst
            {
                get { return _sst; }
                set { _sst = value; OnPropertyChanged("sst"); }
            }
            private string _discount;
            public string discount
            {
                get { return _discount; }
                set { _discount = value; OnPropertyChanged("discount"); }
            }
            private string _total;
            public string total
            {
                get { return _total; }
                set { _total = value; OnPropertyChanged("total"); }
            }
            private string _Insurans;
            public string Insurans
            {
                get { return _Insurans; }
                set { _Insurans = value; OnPropertyChanged("Insurans"); }
            }
            private string _OtherCharges;
            public string OtherCharges
            {
                get { return _OtherCharges; }
                set { _OtherCharges = value; OnPropertyChanged("OtherCharges"); }
            }
            private string _Color;
            public string Color
            {
                get { return _Color; }
                set { _Color = value; OnPropertyChanged("Color"); }
            }
            public ImageSource ItemTypeSource { get { return GetValue(() => ItemTypeSource); } set { SetValue(() => ItemTypeSource, value); OnPropertyChanged("ItemTypeSource"); } }
            public string StationeryType1 { get { return GetValue(() => StationeryType1); } set { SetValue(() => StationeryType1, value); OnPropertyChanged("StationeryType"); } }
        }

        public class StockTranTypeModel : PropertyChangedNotification
        {
            public string TranType { get { return GetValue(() => TranType); } set { SetValue(() => TranType, value); OnPropertyChanged("TranType"); } }
            public string TranDesc { get { return GetValue(() => TranDesc); } set { SetValue(() => TranDesc, value); OnPropertyChanged("TranDesc"); } }
            public string ItemDesc { get { return GetValue(() => ItemDesc); } set { SetValue(() => ItemDesc, value); OnPropertyChanged("ItemDesc"); } }
            public string ItemCode { get { return GetValue(() => ItemCode); } set { SetValue(() => ItemCode, value); OnPropertyChanged("ItemCode"); } }
        }

        public class InventoryTranTypeModel : AMGenFunction
        {
            private string _shipmenttypeid;
            public string ShipmentTypeId
            {
                get { return _shipmenttypeid; }
                set { _shipmenttypeid = value; OnPropertyChanged("ShipmentTypeId"); }
            }
            private string _shipmenttypename;
            public string ShipmentTypeName
            {
                get { return _shipmenttypename; }
                set { _shipmenttypename = value; OnPropertyChanged("ShipmentTypeName"); }
            }
        }

        public class TranHistoryModel : AMGenFunction
        {
            public int StkTranID { get; set; }
            public string ItemDesc { get; set; }
            public DateTime TranDate { get; set; }
            public string TranType { get; set; }
            public string Unit1 { get; set; }
            public decimal Unit1Cost { get; set; }
            public decimal TranQty { get; set; }
            public decimal TranValue { get; set; }
            public decimal Opening { get; set; }
            public decimal Purchase { get; set; }
            public decimal Receive { get; set; }
            public decimal SalesRet { get; set; }
            public decimal Excess { get; set; }
            public decimal Sales { get; set; }
            public decimal Issue { get; set; }
            public decimal PurchaseRet { get; set; }
            public decimal Short { get; set; }
            public decimal Closing { get; set; }
            public decimal Inword { get; set; }
            public decimal Outword { get; set; }
            public decimal ATranValue { get; set; }
            public decimal StockAdj { get; set; }
            public decimal PendingQty { get; set; }
            public int ItemId { get; set; }
            public string ItemCode { get; set; }
            public decimal ReOrderLevel { get; set; }
            public string Action { get; set; }
            public string UserName { get; set; }
            public string StrDate{ get; set; }
            public string Action1 { get; set; }

        }

        public class THistoryModel : AMGenFunction
        {
            public int StkTranID { get; set; }
            public string ItemDesc { get; set; }
            public DateTime TranDate { get; set; }
            public string TranType { get; set; }
            public string Unit1 { get; set; }
            public decimal Unit1Cost { get; set; }
            public decimal TranQty { get; set; }
            public decimal TranValue { get; set; }
            public decimal Opening { get; set; }
            public decimal Purchase { get; set; }
            public decimal Receive { get; set; }
            public decimal SalesRet { get; set; }
            public decimal Excess { get; set; }
            public decimal Sales { get; set; }
            public decimal Issue { get; set; }
            public decimal PurchaseRet { get; set; }
            public decimal Short { get; set; }
            public decimal Closing { get; set; }
            public decimal Inword { get; set; }
            public decimal Outword { get; set; }
            public decimal ATranValue { get; set; }
            public decimal StockAdj { get; set; }
            public decimal PendingQty { get; set; }
            public decimal CancelQty { get; set; }
            public int ItemId { get; set; }
            public string ItemCode { get; set; }
            public decimal ReOrderLevel { get; set; }
            public string Action { get; set; }
            public string UserName { get; set; }
            public string StrDate { get; set; }
            public string Action1 { get; set; }
            public string TranStatus { get; set; }
        }

        public class TranListModel : AMGenFunction
        {
            public int ItemNo { get; set; }
            public string ItemDesc { get; set; }
            public DateTime TranDate { get; set; }
            public string TranType { get; set; }
            public string Unit1 { get; set; }
            public decimal TranQty { get; set; }
            public decimal Opening { get; set; }
            public decimal Closing { get; set; }
            public decimal ReOrderLevel { get; set; }
            public string Remarks { get; set; }
            public int ItemId { get; set; }
            public string ItemCode { get; set; }
            public string UserName { get; set; }
            public string Purchase1 { get; set; }
            public string Purchase2 { get; set; }
            public string Purchase3 { get; set; }
            public string Sales1 { get; set; }
            public string Sales2 { get; set; }
            public string Sales3 { get; set; }
            public int StkTranID { get; set; }
        }

        void EventSetter_OnHandler(object sender, RoutedEventArgs e)
        {
            ExecuteCommandGen("GridClickAction");
        }
        void EventSetterA_OnHandler(object sender, RoutedEventArgs e)
        {
            ExecuteCommandGen("GridClickRemove");
        }
    }
}


//catch (Exception Ex) { error_log.errorlog("" + Ex.ToString()); }



