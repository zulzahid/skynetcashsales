﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class CashSalesListVM : CNPrintVM
    {
        public CashSalesListVM(FrmCashSaleList frm)
        {
            CSaleDate = DateTime.Now.ToShortDateString();            
            LoadGrid();
        }

        public string CSalesNo { get { return GetValue(() => CSalesNo); } set { SetValue(() => CSalesNo, value); OnPropertyChanged("CSalesNo"); } }
        public string CSaleDate { get { return GetValue(() => CSaleDate); } set { SetValue(() => CSaleDate, value); OnPropertyChanged("CSaleDate"); } }
        
        private DataGridCellInfo _focusedcell;
        public DataGridCellInfo FocusedCell { get { return _focusedcell; } set { _focusedcell = value; OnPropertyChanged("FocusedCell"); } }

        private CSalesListModel _selecteditem = new CSalesListModel();
        public CSalesListModel SelectedItem { get { return _selecteditem; } set { _selecteditem = value; OnPropertyChanged("SelectedItem"); } }
        
        public CSalesListModel CurrentItem { get { return _selecteditem; } set { _selecteditem = value; OnPropertyChanged("CurrentItem"); } }
        
        private string _selectedindex;
        public string SelectedIndex { get { return _selectedindex; } set { _selectedindex = value; OnPropertyChanged("SelectedIndex"); } }

        private ObservableCollection<CSalesListModel> _CSalesList = new ObservableCollection<CSalesListModel>();
        public ObservableCollection<CSalesListModel> CSalesList { get { return _CSalesList; } set { _CSalesList = value; OnPropertyChanged("CSalesList"); } }
        
        public ObservableCollection<CSalesListModel> ItemInfo { get { return _CSalesList; } set { _CSalesList = value; OnPropertyChanged("ItemInfo"); } }
        
        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "New": ResetData(); break;
                    case "CSCancel": CSCancelData(); break;
                    case "Print": PrintData(Obj.ToString()); break;
                    case "Preview": PrintData(Obj.ToString()); break;
                    case "TestPrint": PrintORPreview("TestPrint"); break;
                    case "Go": LoadGrid(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
        private void CSCancelData()
        {
            string result = "";
            if (!string.IsNullOrEmpty(SelectedIndex) && SelectedItem != null)
            {
                int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                if (SelectedItem.CSalesID != null && SelectedItem.CSalesID != 0)
                {
                    DateTime Dt = DateTime.Now; DateTime CSDt = Convert.ToDateTime(SelectedItem.CSalesDate);
                    if (Dt.Date == CSDt.Date)
                    {
                        System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("Do you want to cancel?", "Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo);
                        if (dialogResult1 == System.Windows.Forms.DialogResult.Yes)
                        {
                            var CW = new CancelCashSaleVM(SelectedItem.CSalesID.ToString(), SelectedItem.AWBNum, SelectedItem.DestCode, Convert.ToInt16(SelectedItem.Pieces), Convert.ToDecimal(SelectedItem.Weight), SelectedItem.ShipmentType, Convert.ToDecimal(SelectedItem.TotalAmount));
                            CW.FrmCancelCashSale_Closed += (r => { result = r.ReturnValues != null ? r.ReturnValues.ToString() : ""; });
                            CW.CancelCashSaleShow(SelectedItem.CSalesID.ToString(), SelectedItem.AWBNum, SelectedItem.DestCode, Convert.ToInt16(SelectedItem.Pieces), Convert.ToDecimal(SelectedItem.Weight), SelectedItem.ShipmentType, Convert.ToDecimal(SelectedItem.TotalAmount));
                            if (result == "Inserted" || result == "Updated")
                            {
                                MessageBox.Show("Cancelled successfully", "Intimation");
                            }
                        }
                        else { }
                    }
                    else { MessageBox.Show("Can't cancel now", "Warning"); }
                }
            }
        }

        private void PrintData(string PrintOption)
        {
            if (!string.IsNullOrEmpty(SelectedIndex) && SelectedItem != null)
            {
                int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                error_log.errorlog("rowindex:" + rowindex.ToString() + ", CSalesId:" + SelectedItem.CSalesID.ToString() + ", IsManual:" + SelectedItem.IsManual.ToString() + ", CSDate:" + SelectedItem.CSalesDate.ToString());
                if (SelectedItem.CSalesID > 0)
                {
                    if (SelectedItem.IsManual == "N")
                    {
                        error_log.errorlog("CSDt:" + Convert.ToDateTime(SelectedItem.CSalesDate).Date + "CurrentDt:" + DateTime.Now.Date);
                        if (Convert.ToDateTime(SelectedItem.CSalesDate).Date == DateTime.Now.Date)
                        {
                            error_log.errorlog("CSNo:" + SelectedItem.CSalesNo + ", CSID:" + SelectedItem.CSalesID);
                            CheckPrinter();
                            SqlDataReader dr;
                            bool isMobileRef = false;
                            dr = GetDataReader($"SELECT MobRefNum FROM tblCSaleCNNum WHERE CNNum = '{ SelectedItem.AWBNum }' AND ShipmentType != 'STATIONERY'");
                            if (dr.Read())
                            {
                                isMobileRef = dr["MobRefNum"].ToString() == "" ? false : true;
                            }
                            dr.Close();

                            if (isStickerPrinter && isMobileRef)
                            {
                                AWBPrintSticker(SelectedItem.AWBNum);
                            }
                            else
                            {
                                AWBPrint(SelectedItem.AWBNum, "Preview");
                            }
                            //AWBPrint(SelectedItem.AWBNum, "Preview");
                        }
                        else { MessageBox.Show("Sorry can't perform this action", "Warning"); }
                    }
                    else { MessageBox.Show("Manual AWB's can't print, don't try again", "Warning"); }
                }
            }
            else { MessageBox.Show("Kindly select the row", "Warning"); }
        }

        private void LoadGrid()
        {
            CSalesList.Clear();
            DataTable csDt = new DataTable();

            if (!string.IsNullOrEmpty(CSaleDate))
            {
                DateTime dte = Convert.ToDateTime(CSaleDate).Date;
                csDt = GetDataTable($"select b.CSalesID, b.CSalesNo, b.CSalesDate, a.CNNum, a.DestCode, a.ShipmentType, a.Pieces, a.HighestWeight, a.Price, a.OtherCharges, a.InsValue, a.StaffDiscAmt, a.PromoDiscAmt, (a.TotalAmt + a.InsValue - a.StaffDiscAmt - a.PromoDiscAmt) as TotalAmt, b.IsManual FROM tblCSaleCNNum a" +
                    $" LEFT JOIN tblCashSales b ON a.CSalesNo = b.CSalesNo" +
                    $" WHERE b.CDate BETWEEN '{dte.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{dte.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy-MM-dd HH:mm:ss")}' AND a.CDate BETWEEN '{dte.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{dte.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy-MM-dd HH:mm:ss")}' AND a.ShipmentType != 'STATIONERY' AND a.DestCode != ''" +
                    $" ORDER BY b.CSalesDate");
            }
            else
            {
                csDt = GetDataTable($"SELECT b.CSalesID, b.CSalesNo, b.CSalesDate, a.CNNum, a.DestCode, a.shipmenttype, a.Pieces, a.HighestWeight, a.Price, a.OtherCharges, a.InsValue, a.StaffDiscAmt, a.PromoDiscAmt, (a.TotalAmt + a.InsValue - a.StaffDiscAmt - a.PromoDiscAmt) as TotalAmt, b.IsManual FROM tblCSaleCNNum a" +
                    $" LEFT JOIN tblCashSales b ON a.CSalesNo = b.CSalesNo" +
                    $" WHERE a.ShipmentType != 'STATIONERY' AND a.DestCode != ''" +
                    $" ORDER BY b.CSalesDate");
            }

            if (csDt.Rows.Count > 0)
            {
                for (int i = 0; i < csDt.Rows.Count; i++)
                {
                    CSalesList.Add(new CSalesListModel
                    {
                        SlNo = (i + 1),
                        CSalesID = Convert.ToInt32(csDt.Rows[i]["CSalesID"]),
                        CSalesNo = csDt.Rows[i]["CSalesNo"].ToString(),
                        CSalesDate = csDt.Rows[i]["CSalesDate"].ToString(),
                        DestCode = csDt.Rows[i]["DestCode"].ToString(),
                        Pieces = Convert.ToInt32(csDt.Rows[i]["Pieces"]),
                        Weight = Convert.ToDecimal(csDt.Rows[i]["HighestWeight"]),
                        //Price = Convert.ToDecimal(csDt.Rows[i]["Price"]),
                        ShipmentAmount = Convert.ToDecimal(csDt.Rows[i]["Price"]),
                        //StationaryAmount = Convert.ToDecimal(csDt.Rows[i][8]),
                        InsuranceAmount = Convert.ToDecimal(csDt.Rows[i]["InsValue"]),
                        DiscountAmount = Convert.ToDecimal(csDt.Rows[i]["PromoDiscAmt"]) > 0 ? Convert.ToDecimal(csDt.Rows[i]["PromoDiscAmt"]) : Convert.ToDecimal(csDt.Rows[i]["StaffDiscAmt"]),
                        TotalAmount = Convert.ToDecimal(csDt.Rows[i]["TotalAmt"]),
                        ShipmentType = csDt.Rows[i]["ShipmentType"].ToString(),
                        AWBNum = csDt.Rows[i]["CNNum"].ToString(),
                        //IsManual = csDt.Rows[i][""].ToString() == "False" ? "N" : "Y",
                        OtherCharges = Convert.ToDecimal(csDt.Rows[i]["OtherCharges"]),
                        IsManual = csDt.Rows[i]["IsManual"].ToString() == "False" ? "N" : "Y",

                        //SlNo = (i + 1),
                        //CSalesID = Convert.ToInt32(csDt.Rows[i][0]),
                        //CSalesNo = csDt.Rows[i][1].ToString(),
                        //CSalesDate = csDt.Rows[i][2].ToString(),
                        //DestCode = csDt.Rows[i][3].ToString(),
                        //Pieces = Convert.ToInt32(csDt.Rows[i][4]),
                        //Weight = Convert.ToDecimal(csDt.Rows[i][5]),
                        //Price = Convert.ToDecimal(csDt.Rows[i][6]),
                        //ShipmentAmount = Convert.ToDecimal(csDt.Rows[i][7]),
                        //StationaryAmount = Convert.ToDecimal(csDt.Rows[i][8]),
                        //InsuranceAmount = Convert.ToDecimal(csDt.Rows[i][9]),
                        //DiscountAmount = Convert.ToDecimal(csDt.Rows[i][10]) > 0 ? Convert.ToDecimal(csDt.Rows[i][10]) : Convert.ToDecimal(csDt.Rows[i][11]),
                        //TotalAmount = Convert.ToDecimal(csDt.Rows[i][12]),
                        //ShipmentType = csDt.Rows[i][13].ToString(),
                        //AWBNum = csDt.Rows[i][14].ToString(),
                        //IsManual = csDt.Rows[i][15].ToString() == "False" ? "N" : "Y",
                        //OtherCharges = Convert.ToDecimal(csDt.Rows[i][16])
                    });
                }
            }
        }
        private RelayCommand _PrintCommandRDLC;
        public RelayCommand PrintCommandRDLC
        {
            get
            {
                if (_PrintCommandRDLC == null)
                    _PrintCommandRDLC = new RelayCommand(PrintDatasRDLC, CanPrintDatas);
                return _PrintCommandRDLC;
            }
            set { _PrintCommandRDLC = value; }
        }
        private void PrintDatasRDLC(object parameter)
        {
            string opt = parameter as string;
            int index = ItemInfo.IndexOf(parameter as CSalesListModel);
            if (index < ItemInfo.Count)
            {
                int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                if (SelectedItem.CSalesID != null && SelectedItem.CSalesID != 0)
                {
                    string csno = SelectedItem.CSalesNo.ToString();
                    string strcsid = SelectedItem.CSalesID.ToString();
                    int csid = Convert.ToInt32(strcsid != null && strcsid != "" ? strcsid : "0");
                    RDLCCNPrintVM rr = new RDLCCNPrintVM();
                    rr.RDLCPrintORPreview(csno, csid, opt);
                }
            }
        }
        private RelayCommand _PrintCommand;
        public RelayCommand PrintCommand
        {
            get
            {
                if (_PrintCommand == null)
                    _PrintCommand = new RelayCommand(PrintDatas, CanPrintDatas);
                return _PrintCommand;
            }
            set { _PrintCommand = value; }
        }
        private void PrintDatas(object parameter)
        {
            try
            {
                string opt = parameter as string;
                int index = ItemInfo.IndexOf(parameter as CSalesListModel);
                if (index < ItemInfo.Count)
                {
                    int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                    if (SelectedItem.CSalesID != null && SelectedItem.CSalesID != 0)
                    {
                        if (SelectedItem.IsManual == "N")
                        {
                            DateTime CSDt = Convert.ToDateTime(SelectedItem.CSalesDate); DateTime CurrentDt = DateTime.Now;
                            if (CSDt.ToShortDateString() == CurrentDt.ToShortDateString())
                            {
                                string csno = SelectedItem.CSalesNo.ToString();
                                string strcsid = SelectedItem.CSalesID.ToString();
                                int csid = Convert.ToInt32(strcsid != null && strcsid != "" ? strcsid : "0");

                                DataTable dtlist = new DataTable();
                                dtlist = GetDataTable($"SELECT a.CNNum, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, c.UserName, b.CSalesDate, b.IsInsuranced, b.DestStation, b.InsShipmentValue, b.InsuranceTotal, b.ShipmentTotal, b.CSalesTotal FROM tblCSaleCNNum a" +
                                    $" JOIN tblCashSale b ON a.CSalesNo = b.CSalesNo" +
                                    $" join tblUserMaster c on b.CUserID = c.UserId" +
                                    $" WHERE a.CSalesNo = '{csno}' and b.CSalesID = {csid}");

                                PrintORPreview(csno, csid, dtlist, opt);
                            }
                            else { MessageBox.Show("Sorry can't perform this action", "Warning"); }
                        }
                        else { MessageBox.Show("Manual AWB's can't print, don't try again", "Warning"); }
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("CashSales List PrintDatas Command Function: " + Ex.ToString()); }
        }
        private bool CanPrintDatas(object parameter) { return true; }
        private void ResetData()
        {

        }

        public class CSalesListModel : AMGenFunction
        {
            private int _SlNo;
            public int SlNo
            {
                get { return _SlNo; }
                set { _SlNo = value; OnPropertyChanged("SlNo"); }
            }
            private int _CSalesID;
            public int CSalesID
            {
                get { return _CSalesID; }
                set { _CSalesID = value; OnPropertyChanged("CSalesID"); }
            }
            private string _CSalesNo;
            public string CSalesNo
            {
                get { return _CSalesNo; }
                set { _CSalesNo = value; OnPropertyChanged("CSalesNo"); }
            }
            private string _AWBNum;
            public string AWBNum
            {
                get { return _AWBNum; }
                set { _AWBNum = value; OnPropertyChanged("AWBNum"); }
            }
            private string _CSalesDate;
            public string CSalesDate
            {
                get { return _CSalesDate; }
                set { _CSalesDate = value; OnPropertyChanged("CSalesDate"); }
            }
            private string _IsManual;
            public string IsManual
            {
                get { return _IsManual; }
                set { _IsManual = value; OnPropertyChanged("IsManual"); }
            }
            private string _ShipmentType;
            public string ShipmentType
            {
                get { return _ShipmentType; }
                set { _ShipmentType = value; OnPropertyChanged("ShipmentType"); }
            }
            private string _DestCode;
            public string DestCode
            {
                get { return _DestCode; }
                set { _DestCode = value; OnPropertyChanged("DestCode"); }
            }
            private int _Pieces;
            public int Pieces
            {
                get { return _Pieces; }
                set { _Pieces = value; OnPropertyChanged("Pieces"); }
            }
            private decimal _Weight;
            public decimal Weight
            {
                get { return _Weight; }
                set { _Weight = value; OnPropertyChanged("Weight"); }
            }
            private decimal _Price;
            public decimal Price
            {
                get { return _Price; }
                set { _Price = value; OnPropertyChanged("Price"); }
            }
            private decimal _ShipmentAmount;
            public decimal ShipmentAmount
            {
                get { return _ShipmentAmount; }
                set { _ShipmentAmount = value; OnPropertyChanged("ShipmentAmount"); }
            }
            private decimal _StationaryAmount;
            public decimal StationaryAmount
            {
                get { return _StationaryAmount; }
                set { _StationaryAmount = value; OnPropertyChanged("StationaryAmount"); }
            }
            private decimal _InsuranceAmount;
            public decimal InsuranceAmount
            {
                get { return _InsuranceAmount; }
                set { _InsuranceAmount = value; OnPropertyChanged("InsuranceAmount"); }
            }
            private decimal _OtherCharges;
            public decimal OtherCharges
            {
                get { return _OtherCharges; }
                set { _OtherCharges = value; OnPropertyChanged("OtherCharges"); }
            }
            private decimal _DiscountAmount;
            public decimal DiscountAmount
            {
                get { return _DiscountAmount; }
                set { _DiscountAmount = value; OnPropertyChanged("DiscountAmount"); }
            }
            private decimal _TotalAmount;
            public decimal TotalAmount
            {
                get { return _TotalAmount; }
                set { _TotalAmount = value; OnPropertyChanged("TotalAmount"); }
            }
        }
    }
}
