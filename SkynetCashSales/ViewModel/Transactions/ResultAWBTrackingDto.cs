﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class ResultAWBTrackingDto
    {
        public ResultAWBTrackingDto()
        {
            status = status;
            code = code;
            message = message;
        }

        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
