﻿using SkynetCashSales.General;
using SkynetCashSales.Model;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class MobileRefCashSalesVM : CNPrintVM
    {
        private static string errormsg = "";
        private static FrmMobileRefCashSales Wndow;
        static MobileRefUpdateAuth obtmupdateref = new MobileRefUpdateAuth();
        AWBDetailVM awbVM;
        private bool isPay = false;
        public bool isDone = false;

        //public MobileRefCashSalesVM()
        //{

        //}
        public MobileRefCashSalesVM(FrmMobileRefCashSales frm)
        {
            Wndow = frm;
            getStationeryTax();
            CheckPrinter();
            getTaxDetail();
            ResetData();
        }

        //Validation Starts 
        static readonly string[] ValidatedProperties = { "DestCode", "Weight", "Pieces", "Price", "TotalAmount", "PaymentReference" };
        static readonly string[] ValidatedPropertiesStationary = { "PaymentReference" };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        System.Windows.MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private ObservableCollection<AWBInfo> _AWBDetailList = new ObservableCollection<AWBInfo>();
        public ObservableCollection<AWBInfo> AWBDetailList { get { return _AWBDetailList; } set { _AWBDetailList = value; OnPropertyChanged("AWBInfo"); } }

        private ObservableCollection<StationaryModel> _sttnryItemlist = new ObservableCollection<StationaryModel>();
        public ObservableCollection<StationaryModel> Itemlist { get { return _sttnryItemlist; } set { _sttnryItemlist = value; OnPropertyChanged("Itemlist"); } }
        private ObservableCollection<StationaryModel> _StationaryList = new ObservableCollection<StationaryModel>();
        public ObservableCollection<StationaryModel> StationaryList { get { return _StationaryList; } set { _StationaryList = value; OnPropertyChanged("StationaryList"); } }
        private RelayCommand _stationaryimageclick;
        public RelayCommand StationaryItemAdd { get { if (_stationaryimageclick == null) _stationaryimageclick = new RelayCommand(Parameter => StationaryImageClick(Parameter)); return _stationaryimageclick; } }
        private StationaryModel _selecteditem = new StationaryModel();
        public StationaryModel SelectedItem { get { return _selecteditem; } set { _selecteditem = value; OnPropertyChanged("SelectedItem"); /*SelectedAWBrow();*/ } }
        private ObservableCollection<QuotationInfo> _QuotationItems = new ObservableCollection<QuotationInfo>();
        public ObservableCollection<QuotationInfo> QuotationItems { get { return _QuotationItems; } set { _QuotationItems = value; OnPropertyChanged("QuotationItems"); } }

        public decimal StationarySum { get { return GetValue(() => StationarySum); } set { SetValue(() => StationarySum, value); OnPropertyChanged("StationarySum");/* NetSummary(); */} }
        public string StationeryTax { get { return GetValue(() => StationeryTax); } set { SetValue(() => StationeryTax, value); OnPropertyChanged("StationeryTax"); } }
        public System.Windows.Media.LinearGradientBrush ColorAWB { get { return GetValue(() => ColorAWB); } set { SetValue(() => ColorAWB, value); OnPropertyChanged("ColorAWB"); } }
        public string DiscPercentage { get { return GetValue(() => DiscPercentage); } set { SetValue(() => DiscPercentage, value); OnPropertyChanged("DiscPercentage"); } }
        //public string PromoTitle { get { return GetValue(() => PromoTitle); } set { SetValue(() => PromoTitle, value); OnPropertyChanged("PromoTitle"); } }
        public string NameOrTitle { get { return GetValue(() => NameOrTitle); } set { SetValue(() => NameOrTitle, value); OnPropertyChanged("NameOrTitle"); } }

        public decimal PromoValue;
        public string PromoCode { get { return GetValue(() => PromoCode); } set { SetValue(() => PromoCode, value); OnPropertyChanged("PromoCode"); } }
        public string PromoDiscPercentage { get { return GetValue(() => PromoDiscPercentage); } set { SetValue(() => PromoDiscPercentage, value); OnPropertyChanged("PromoDiscPercentage"); } }
        public bool IsPromoPercentage { get { return GetValue(() => IsPromoPercentage); } set { SetValue(() => IsPromoPercentage, value); OnPropertyChanged("IsPromoPercentage"); } }
        public string PromoShipmentType { get { return GetValue(() => PromoShipmentType); } set { SetValue(() => PromoShipmentType, value); OnPropertyChanged("PromoShipmentType"); } }
        public string StaffCode { get { return GetValue(() => StaffCode); } set { SetValue(() => StaffCode, value); OnPropertyChanged("StaffCode"); } }
        public string StaffName { get { return GetValue(() => StaffName); } set { SetValue(() => StaffName, value); OnPropertyChanged("StaffName"); } }
        public bool IsPromoCode { get { return GetValue(() => IsPromoCode); } set { SetValue(() => IsPromoCode, value); OnPropertyChanged("IsPromoCode"); } }
        public bool IsStaffCode { get { return GetValue(() => IsStaffCode); } set { SetValue(() => IsStaffCode, value); OnPropertyChanged("IsStaffCode"); } }
        public string DestZoneDesc { get { return GetValue(() => DestZoneDesc); } set { SetValue(() => DestZoneDesc, value); OnPropertyChanged("DestZoneDesc"); } }
        public string DestCode { get { return GetValue(() => DestCode); } set { SetValue(() => DestCode, value); OnPropertyChanged("DestCode"); } }
        public decimal DiscAmount { get { return GetValue(() => DiscAmount); } set { SetValue(() => DiscAmount, value); OnPropertyChanged("DiscAmount"); } }
        public decimal TotAmtBeforeTax { get { return GetValue(() => TotAmtBeforeTax); } set { SetValue(() => TotAmtBeforeTax, value); OnPropertyChanged("TotAmtBeforeTax"); } }
        public decimal TotAllItem { get { return GetValue(() => TotAllItem); } set { SetValue(() => TotAllItem, value); OnPropertyChanged("TotAllItem"); } }
        public decimal TaxAmount { get { return GetValue(() => TaxAmount); } set { SetValue(() => TaxAmount, value); OnPropertyChanged("TaxAmount"); } }
        public decimal TaxPercentage { get { return GetValue(() => TaxPercentage); } set { SetValue(() => TaxPercentage, value); OnPropertyChanged("TaxPercentage"); } }
        public decimal LocalTaxAmount { get { return GetValue(() => LocalTaxAmount); } set { SetValue(() => LocalTaxAmount, value); OnPropertyChanged("LocalTaxAmount"); } }
        public decimal INTLTaxAmount { get { return GetValue(() => INTLTaxAmount); } set { SetValue(() => INTLTaxAmount, value); OnPropertyChanged("INTLTaxAmount"); } }
        public decimal TotalStationaryPrice { get { return GetValue(() => TotalStationaryPrice); } set { SetValue(() => TotalStationaryPrice, value); OnPropertyChanged("TotalStationaryPrice"); } }
        public decimal TotalAmt { get { return GetValue(() => TotalAmt); } set { SetValue(() => TotalAmt, value); OnPropertyChanged("TotalAmt"); } }
        public decimal RoundingAmt { get { return GetValue(() => RoundingAmt); } set { SetValue(() => RoundingAmt, value); OnPropertyChanged("RoundingAmt"); } }
        public decimal TotalAmtAfterTax { get { return GetValue(() => TotalAmtAfterTax); } set { SetValue(() => TotalAmtAfterTax, value); OnPropertyChanged("TotalAmtAfterTax"); } }       

        //For Display on total       
        public string DBeforeTax { get { return GetValue(() => DBeforeTax); } set { SetValue(() => DBeforeTax, value); OnPropertyChanged("DBeforeTax"); } }
        public string DSST { get { return GetValue(() => DSST); } set { SetValue(() => DSST, value); OnPropertyChanged("DSST"); } }
        public string DSurcharge { get { return GetValue(() => DSurcharge); } set { SetValue(() => DSurcharge, value); OnPropertyChanged("DSurcharge"); } }
        public string RASurcharge { get { return GetValue(() => RASurcharge); } set { SetValue(() => RASurcharge, value); OnPropertyChanged("RASurcharge"); } }
        public string DRounding { get { return GetValue(() => DRounding); } set { SetValue(() => DRounding, value); OnPropertyChanged("DRounding"); } }
        public string DTotalAmt { get { return GetValue(() => DTotalAmt); } set { SetValue(() => DTotalAmt, value); OnPropertyChanged("DTotalAmt"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "AWBDetail":
                        {
                            ChildWnd.FrmAWBDetail_Closed += 
                                (r => {
                                    AWBDetailList.Add(
                                        new AWBInfo
                                        {
                                            AWBNo = r.AWBNo,
                                            ShipmentType = r.ShipmentType,
                                            DestCode = r.DestCode,
                                            Pieces = r.Pieces,
                                            Weight = r.Weight,
                                            VolLength = r.VolLength,
                                            VolWidth = r.VolWidth,
                                            VolHeight = r.VolHeight,
                                            VolWeight = r.VolWeight,
                                            AddRate = r.AddRate,
                                            Price = r.Price,
                                            OtherCharges = r.OtherCharges,
                                            InsCheck = r.InsCheck,
                                            InsShipmentValue = r.InsShipmentValue,
                                            InsShipmentDesc = r.InsShipmentDesc,
                                            InsCharges = r.InsCharges,
                                            TotalAmount = r.TotalAmount,
                                            TaxPercentage = r.TaxPercentage,
                                            MobileRefNo = r.MobileRefNo,
                                            Locality = r.Locality,
                                            PostCodeId = r.PostCodeId,
                                            PostCode = r.PostCode,
                                            DestUnder = r.DestUnder,
                                            QuotationType = r.QuotationType,
                                            Country = r.Country,
                                            PostCodeIntl = r.PostCodeIntl,
                                            RASurcharge = r.RASurcharge
                                        });
                                });
                            ChildWnd.AWBDetailScreen(this);

                            LoadGrid();
                        }
                        break;
                    case "PromoCode":
                        {
                            if (Itemlist.Count > 0)
                            {
                                ChildWnd.PromoCodeScreen(this);
                                if (IsPromoCode && SpecialConditionPromo())
                                {
                                    Wndow.textnameortitle.Content = "Promo Title :";
                                    Wndow.lblDiscPercentage.Visibility = Visibility.Visible;
                                    //PromoCode = NameOrTitle;
                                    IsPromoPercentage = true;

                                    if (DiscPercentage.Contains("%"))
                                    {
                                        Wndow.textDiscPercORStaffCode.Content = "Disc Percentage :";
                                        PromoDiscPercentage = DiscPercentage.Replace("%", "").Replace("RM", "");
                                    }
                                    else
                                    {
                                        Wndow.textDiscPercORStaffCode.Content = "Disc Amount :";
                                        PromoDiscPercentage = DiscPercentage.Replace("RM", "").Replace("%", "");
                                    }

                                    StaffName = "";
                                    StaffCode = "";
                                    IsStaffCode = false;
                                    LoadGrid();
                                }
                                else
                                {
                                    LoadGrid();
                                }
                            }
                        }
                        break;
                    case "StaffCode":
                        {
                            if (Itemlist.Count > 0 && isStaffCodeValid())
                            {
                                ChildWnd.StaffCodeScreen(this);
                                if (IsStaffCode)
                                {
                                    string[] Staff = NameOrTitle.Split('|');
                                    NameOrTitle = Staff[0];
                                    Wndow.textnameortitle.Content = "Staff Name :";
                                    Wndow.textDiscPercORStaffCode.Content = "Staff Code :             " + Staff[1];
                                    Wndow.lblDiscPercentage.Visibility = Visibility.Hidden;
                                    StaffName = Staff[0];
                                    StaffCode = Staff[1];
                                    PromoCode = "";
                                    PromoDiscPercentage = "";
                                    IsPromoCode = false;
                                    LoadGrid();
                                }
                            }
                        }
                        break;
                    case "CancelDiscount":
                        {
                            if (DiscPercentage != "" && NameOrTitle != "")
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm cancel discount?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes)
                                {
                                    CancelDiscount();
                                    LoadGrid();
                                }
                            }
                        }
                        break;
                    case "Payment":
                        {
                            if (TotalAmtAfterTax > 0)
                            {
                                isPay = true;
                                LoadGrid();
                                ChildWnd.PaymentScreen(this);
                            }

                            if (isDone)
                            {
                                ResetData();
                            }

                        }
                        break;
                    case "View AWB":
                        {
                            for (int i = 0; i < AWBDetailList.Count; i++)
                            {
                                bool isMobileRef = AWBDetailList[i].MobileRefNo.Length > 0 ? true : false;
                                string Destination = AWBDetailList[i].DestCode, tempAWBNo = AWBDetailList[i].AWBNo.ToString();

                                if (SelectedItem != null)
                                {
                                    if (SelectedItem.itemname == ("AWB No. Pending.. Dest -" + Destination + " (" + tempAWBNo + ")"))
                                    {
                                        string[] arr =
                                            {
                                                AWBDetailList[i].AWBNo.ToString(),
                                                AWBDetailList[i].ShipmentType.ToString(),
                                                AWBDetailList[i].DestCode,
                                                AWBDetailList[i].Pieces.ToString(),
                                                AWBDetailList[i].Weight.ToString(),
                                                AWBDetailList[i].VolLength.ToString(),
                                                AWBDetailList[i].VolWidth.ToString(),
                                                AWBDetailList[i].VolHeight.ToString(),
                                                AWBDetailList[i].VolWeight.ToString(),
                                                AWBDetailList[i].AddRate.ToString(),
                                                AWBDetailList[i].Price.ToString(),
                                                AWBDetailList[i].OtherCharges.ToString(),
                                                AWBDetailList[i].InsCheck.ToString(),
                                                AWBDetailList[i].InsShipmentValue.ToString(),
                                                AWBDetailList[i].InsShipmentDesc,
                                                AWBDetailList[i].InsCharges.ToString(),
                                                AWBDetailList[i].TotalAmount.ToString(),
                                                AWBDetailList[i].TaxPercentage.ToString(),
                                                AWBDetailList[i].MobileRefNo,
                                                AWBDetailList[i].Locality,
                                                AWBDetailList[i].PostCode,
                                                AWBDetailList[i].DestUnder,
                                                AWBDetailList[i].PostCodeId.ToString(),
                                                AWBDetailList[i].QuotationType,
                                                AWBDetailList[i].Country,
                                                AWBDetailList[i].PostCodeIntl,
                                                AWBDetailList[i].RASurcharge.ToString()
                                            };

                                        ChildWnd.FrmAWBDetail_Closed +=
                                        (r =>
                                        {
                                            AWBDetailList[i].AWBNo = r.AWBNo;
                                            AWBDetailList[i].ShipmentType = r.ShipmentType;
                                            AWBDetailList[i].DestCode = r.DestCode;
                                            AWBDetailList[i].Pieces = r.Pieces;
                                            AWBDetailList[i].Weight = r.Weight;
                                            AWBDetailList[i].VolLength = r.VolLength;
                                            AWBDetailList[i].VolWidth = r.VolWidth;
                                            AWBDetailList[i].VolHeight = r.VolHeight;
                                            AWBDetailList[i].VolWeight = r.VolWeight;
                                            AWBDetailList[i].AddRate = r.AddRate;
                                            AWBDetailList[i].Price = r.Price;
                                            AWBDetailList[i].OtherCharges = r.OtherCharges;
                                            AWBDetailList[i].InsCheck = r.InsCheck;
                                            AWBDetailList[i].InsShipmentValue = r.InsShipmentValue;
                                            AWBDetailList[i].InsShipmentDesc = r.InsShipmentDesc;
                                            AWBDetailList[i].InsCharges = r.InsCharges;
                                            AWBDetailList[i].TotalAmount = r.TotalAmount;
                                            AWBDetailList[i].TaxPercentage = r.TaxPercentage;
                                            AWBDetailList[i].MobileRefNo = r.MobileRefNo;
                                            AWBDetailList[i].Locality = r.Locality;
                                            AWBDetailList[i].PostCodeId = r.PostCodeId;
                                            AWBDetailList[i].PostCode = r.PostCode;
                                            AWBDetailList[i].DestUnder = r.DestUnder;
                                            AWBDetailList[i].QuotationType = r.QuotationType;
                                            AWBDetailList[i].Country = r.Country;
                                            AWBDetailList[i].PostCodeIntl = r.PostCodeIntl;
                                            AWBDetailList[i].RASurcharge = r.RASurcharge;
                                        });
                                        ChildWnd.AWBDetailScreen2(this, arr);
                                        break;
                                    }
                                }
                            }

                            foreach (DataRow stationaryrow in dtItemSummary.Rows)
                            {
                                string StationeryName = ""; int Quantity = 0;
                                if (SelectedItem != null)
                                {
                                    if (SelectedItem.itemname.ToString().ToUpper() == stationaryrow["StationaryName"].ToString().ToUpper())
                                    {
                                        ChildWnd.FrmQuantity_Closed += (r => { StationeryName = r.Name; Quantity = r.Quantity; });
                                        ChildWnd.QuantityScreen(this, SelectedItem.itemname.ToString(), Convert.ToInt32(SelectedItem.quantity));

                                        stationaryrow["Quantity"] = Quantity;
                                        stationaryrow["Total"] = (Quantity * Convert.ToDecimal(stationaryrow["Rate"])).ToString();
                                        StationarySum += Convert.ToDecimal(stationaryrow["Rate"].ToString());
                                        break;
                                    }
                                }
                            }

                            LoadGrid();
                        }
                        break;
                    case "Delete Selected Record":
                        {
                            foreach (DataRow item in dtItemSummary.Rows)
                            {
                                if (item["StationaryName"].ToString().ToUpper() == SelectedItem.itemname.ToUpper())
                                {
                                    //item["Quantity"] = Convert.ToInt32(item["Quantity"]) - 1;
                                    //item["Total"] = Convert.ToInt32(item["Quantity"]) * Convert.ToDouble(item["Rate"]);
                                    //StationarySum -= Convert.ToDecimal(item["Rate"].ToString());
                                    //if (SelectedItem.quantity == "0") { Itemlist.Remove(SelectedItem); }
                                    MsgRes = System.Windows.MessageBox.Show("Confirm to remove this Stationery ?", "Confirmation", MessageBoxButton.YesNo);
                                    if (MsgRes == MessageBoxResult.Yes)
                                    {
                                        item["Quantity"] = 0;
                                        item["Total"] = 0;
                                        StationarySum -= Convert.ToDecimal(item["Rate"].ToString());
                                        if (SelectedItem.quantity == "0") { Itemlist.Remove(SelectedItem); }
                                    }
                                    break;
                                }
                            }

                            for (int i = 0; i < AWBDetailList.Count; i++)
                            {
                                if (SelectedItem != null)
                                {
                                    if (SelectedItem.itemname == ("AWB No. Pending.. Dest -" + AWBDetailList[i].DestCode + " (" + AWBDetailList[i].AWBNo + ")"))
                                    {
                                        MsgRes = System.Windows.MessageBox.Show("Confirm to remove this AWB ?", "Confirmation", MessageBoxButton.YesNo);
                                        if (MsgRes == MessageBoxResult.Yes)
                                        {
                                            AWBDetailList.Remove(AWBDetailList[i]);
                                            break;
                                        }
                                    }
                                }
                            }

                            SpecialConditionPromo();

                            if (!isStaffCodeValid())
                            {
                                CancelDiscount();
                            }

                            LoadGrid();
                        }
                        break;
                    case "Close":
                        {
                            if (Itemlist.Count > 0)
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to close this screen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes)
                                {
                                    InitializedDashboard();
                                    this.CloseWind();
                                }
                            }
                            else
                            {
                                InitializedDashboard();
                                this.CloseWind();
                            }
                        }
                        break;
                    case "New":
                        {
                            if (Itemlist.Count > 0)
                            {
                                MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Confirm to reset this screen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (MsgConfirmation == MessageBoxResult.Yes)
                                {
                                    ResetData();
                                }
                            }
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {

            }
            return error;
        }

        public int AWBNo = 0;
        public int PendingAWBNo()
        {
            return AWBNo += 1;
        }

        public int CheckCurrentAWBNo()
        {
            return AWBNo;
        }

        //public List<string> listAWBDetail = new List<string>();
        public List<string> listDestCodeDetail = new List<string>();
        public List<string> listRefDetail = new List<string>();
        public List<string> listStationaryDetail = new List<string>();


        public string tblCashSalesInfo = "", PaymentView = "";
        public List<string> finalistAWB = new List<string>();
        public List<string> finalistRef = new List<string>();

        private void createDataTable()
        {
            if (dtItemSummary != null) { dtItemSummary.Clear(); StationaryList.Clear(); }
            using (dtItemSummary = new DataTable())
            {
                dtItemSummary = GetDataTable($"SELECT A.ItemId As ColOneId, A.ItemCode As ColOneText, A.ItemDesc As StationaryName, A.Unit1 As UOM, A.Unit1SalRateToStation As Rate, 0 as Quantity, 0.0 as Total  FROM tblItemMaster A" +
                                $" LEFT JOIN tblGeneralDet B ON A.ItemType = B.GenDetId" +
                                $" ORDER BY A.ItemCode");

                StationaryLoad();
            }
        }

        DataTable dtAWBInfo;
        DataTable dtItemSummary;
        private void StationaryLoad()
        {
            if (dtItemSummary.Rows.Count > 0)
            {
                SqlDataReader dr;
                try
                {
                    for (int i = 0; i < dtItemSummary.Rows.Count; i++)
                    {
                        if (dtItemSummary.Rows[i]["StationaryName"].ToString() != "PRIHATIN" && dtItemSummary.Rows[i]["StationaryName"].ToString() != "HANDLING CHARGES")
                        {
                            dr = GetDataReader($"SELECT ItemImage FROM tblItemMaster WHERE ItemCode = '{dtItemSummary.Rows[i]["ColOneText"].ToString()}'");
                            if (dr.Read() && dr["ItemImage"] != DBNull.Value)
                            {
                                byte[] imgBytes = (byte[])dr["ItemImage"];
                                using (MemoryStream strm = new MemoryStream(imgBytes))
                                {
                                    strm.Write(imgBytes, 0, imgBytes.Length);
                                    strm.Position = 0;
                                    System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                                    System.Windows.Media.Imaging.BitmapImage bi = new System.Windows.Media.Imaging.BitmapImage();
                                    bi.BeginInit();
                                    MemoryStream ms = new MemoryStream();
                                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    bi.StreamSource = ms;
                                    bi.EndInit();
                                    StationaryList.Add(new StationaryModel { itemname = dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper(), ItemTypeSource = bi as ImageSource });
                                }
                            }
                            else
                            {
                                StationaryList.Add(new StationaryModel { itemname = dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper(), ItemTypeSource = null });
                            }
                        }
                    }

                    if (dtItemSummary.Rows.Count % 2 == 0)
                    {
                        StationaryList.Add(new StationaryModel { itemname = "", ItemTypeSource = null });
                    }
                    SelectedStationaryIndex = dtItemSummary.Rows.Count - 1;
                }
                catch (Exception Ex) { error_log.errorlog("Load Stationery Image : " + Ex.Message); }
            }
        }

        //private void StationaryLoad1()
        //{
        //    string path = "/SkynetCashSales;component/Images/";
        //    string image_FlyersA = path + "FlyersA.png", image_FlyersB = path + "FlyersB.png", image_FlyersC = path + "FlyersC.png", image_Wrapping = path + "wrapping.png", image_boxBig = path + "boxBig.png", image_boxSmall = path + "boxSmall.png", image_Handling = path + "handling.png", image_micpac = path + "micpac.png", image_Pouch = path + "Pouch.jpeg", image_bubblewrap = path + "bubblewrap.png", image_facemask = path + "facemask.png";
        //    string image = "";
        //    if (dtItemSummary.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtItemSummary.Rows.Count; i++)
        //        {
        //            image = "";
        //            if (dtItemSummary.Rows[i]["StationaryName"].ToString() == "FLYER A") { image = image_FlyersA; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "FLYER B") { image = image_FlyersB; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "FLYER C") { image = image_FlyersC; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "WRAPPING SERVICE") { image = image_Wrapping; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "CARTON BOX - BIG") { image = image_boxBig; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "CARTON BOX - SMALL") { image = image_boxSmall; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "HANDLING CHARGES") { image = image_Handling; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "MIC-PAC") { image = image_micpac; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "POUCH") { image = image_Pouch; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "BUBBLE WRAP") { image = image_bubblewrap; }
        //            else if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() == "FACE MASK") { image = image_facemask; }

        //            if (dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() != "PRIHATIN" && dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper() != "HANDLING CHARGES")
        //                StationaryList.Add(new StationaryModel { itemname = dtItemSummary.Rows[i]["StationaryName"].ToString().ToUpper(), ItemTypeSource = image });
        //        }

        //        if (dtItemSummary.Rows.Count % 2 == 0)
        //        {
        //            StationaryList.Add(new StationaryModel { itemname = "", ItemTypeSource = "" });
        //        }
        //        SelectedStationaryIndex = dtItemSummary.Rows.Count - 1;
        //    }
        //}

        private void StationaryImageClick(object obj)
        {
            try
            {
                string itemName = (((StationaryModel)(obj)).itemname).ToString();
                foreach (DataRow item in dtItemSummary.Rows)
                {
                    if (item["StationaryName"].ToString().ToUpper() == itemName)
                    {
                        item["Quantity"] = Convert.ToInt32(item["Quantity"]) + 1;
                        item["Total"] = (Convert.ToDecimal(item["Quantity"]) * Convert.ToDecimal(item["Rate"])).ToString();
                        //StationarySum += Convert.ToDecimal(item["Rate"].ToString());
                        break;
                    }
                }
                LoadGrid();
            }
            catch (Exception e) { System.Windows.MessageBox.Show(e.ToString()); }
        }

        public void LoadGrid()
        {
            Itemlist.Clear(); listStationaryDetail.Clear(); finalistAWB.Clear(); finalistRef.Clear(); tblCashSalesInfo = ""; PaymentView = ""; tblCashSalesInfo = ""; PaymentView = ""; TotAllItem = 0; TaxAmount = 0; LocalTaxAmount = 0; INTLTaxAmount = 0; TotalStationaryPrice = 0; 
            int no = 1;
            decimal TotalShipmentAWB = 0, TotalAWB = 0, TotalInsurance = 0, TotalDiscAmt = 0, TotalOtherCharges = 0, LocalAmountBeforeTax = 0, INTLAmountBeforeTax = 0, TotalRASurchage = 0;
            SpecialConditionPromo();
            SqlDataReader dr;

            //Start inserting awb into gridview
            for (int i = 0; i < AWBDetailList.Count; i++)
            {
                string
                    tempSelectedShipment = AWBDetailList[i].ShipmentType,
                    tempDestCode = AWBDetailList[i].DestCode,
                    tempInsShipmentDesc = AWBDetailList[i].InsShipmentDesc,
                    tempMobileReferenceNo = AWBDetailList[i].MobileRefNo,
                    tempLocality = AWBDetailList[i].Locality,
                    tempPostCode = AWBDetailList[i].PostCode,
                    tempDestUnder = AWBDetailList[i].DestUnder,
                    tempQuotationType = AWBDetailList[i].QuotationType,
                    tempCountry = AWBDetailList[i].Country;

                int 
                    tempAWBNo = AWBDetailList[i].AWBNo, 
                    tempPieces = AWBDetailList[i].Pieces,
                    tempPostCodeId = AWBDetailList[i].PostCodeId;

                decimal 
                    tempWeight = AWBDetailList[i].Weight, 
                    tempVolLength = AWBDetailList[i].VolLength, 
                    tempVolWidth = AWBDetailList[i].VolWidth, 
                    tempVolHeight = AWBDetailList[i].VolHeight, 
                    tempVolWeight = AWBDetailList[i].VolWeight, 
                    tempAddRate = AWBDetailList[i].AddRate,
                    tempPrice = AWBDetailList[i].Price,
                    tempOtherCharges = AWBDetailList[i].OtherCharges,
                    tempInsShipmentValue = AWBDetailList[i].InsShipmentValue,
                    tempInsurenceCharges = AWBDetailList[i].InsCharges,
                    tempTotalAmount = AWBDetailList[i].TotalAmount,
                    tempTaxPercentage = AWBDetailList[i].TaxPercentage,
                    tempRASurcharge = AWBDetailList[i].RASurcharge;

                bool tempYesInsurance = AWBDetailList[i].InsCheck;               

                decimal TaxCalc = 0;
                decimal HighestWeight = Convert.ToDecimal(tempWeight) > Convert.ToDecimal(tempVolWeight) ? Convert.ToDecimal(tempWeight) : Convert.ToDecimal(tempVolWeight);

                string DestUnder = tempDestUnder;
                DestCode = tempDestCode; DiscAmount = 0;

                decimal TotAmtAfterDisc = 0, TotAmtexcludeInsurans = 0;

                TotAmtexcludeInsurans = Convert.ToDecimal(tempPrice) + Convert.ToDecimal(tempOtherCharges);
                //TotAmtBeforeTax = Convert.ToDecimal(tempTotalAmount);//Total Amount from AWBDetail

                //getting discount if have
                if (TotAmtexcludeInsurans > 0 && IsStaffCode)
                {
                    if (tempQuotationType == "Standard")//Check for local quotation type only, then proceed
                    {
                        dr = GetDataReader($"SELECT * FROM tblStaffDiscount WHERE Status = 1");
                        {
                            if (dr.Read())
                            {
                                if (Convert.ToBoolean(dr["IsPercentage"]))
                                {
                                    DiscAmount = GetRoundingVal(TotAmtexcludeInsurans * Convert.ToDecimal(dr["DiscountValue"]) / 100, 2);
                                }
                                else
                                {
                                    DiscAmount = Convert.ToDecimal(dr["DiscountValue"]);
                                }
                            }
                            dr.Close();
                        }
                    }
                }
                else if (TotAmtexcludeInsurans > 0 && IsPromoCode)
                {
                    if (DiscPercentage.Contains("%"))
                    {
                        if (PromoCode != null && PromoCode == "BKS05S21")
                        {
                            if (tempMobileReferenceNo != "" && PromoShipmentType.ToUpper().Contains(tempSelectedShipment))
                            {
                                DiscAmount = GetRoundingVal((IsPromoPercentage && TotAmtexcludeInsurans > 0 && PromoDiscPercentage.Trim() != "" ? TotAmtexcludeInsurans * Convert.ToDecimal(PromoDiscPercentage) / 100 : 0), 2);
                            }
                        }
                        else
                        {
                            DiscAmount = GetRoundingVal((IsPromoPercentage && TotAmtexcludeInsurans > 0 && PromoDiscPercentage.Trim() != "" ? TotAmtexcludeInsurans * Convert.ToDecimal(PromoDiscPercentage) / 100 : 0), 2);
                        }
                    }
                    else
                    {
                        DiscAmount = GetRoundingVal((IsPromoPercentage && TotAmtexcludeInsurans > 0 && PromoDiscPercentage.Trim() != "" ? Convert.ToDecimal(PromoDiscPercentage) : 0), 2);
                        if (PromoCode != null && PromoCode == "DEC2021S20" && DiscAmount > 0)
                        {
                            DiscAmount = GetRoundingVal(PromoValue >= 250 && PromoShipmentType.ToUpper().Contains(tempSelectedShipment) ? (DiscAmount / PromoValue * TotAmtexcludeInsurans) : 0, 2);
                        }
                        else if(PromoCode != null && PromoCode == "CNY88S21" && DiscAmount > 0)
                        {
                            DiscAmount = GetRoundingVal(PromoValue >= 188 && PromoShipmentType.ToUpper().Contains(tempSelectedShipment) ? (DiscAmount / PromoValue * TotAmtexcludeInsurans) : 0, 2);
                        }
                        else
                        {
                            //Default calculation for promo
                            DiscAmount = GetRoundingVal((IsPromoPercentage && TotAmtexcludeInsurans > 0 && PromoDiscPercentage.Trim() != "" ? Convert.ToDecimal(PromoDiscPercentage) : 0), 2);
                        }
                    }
                }

                TotalDiscAmt += DiscAmount;

                //if (IsPromoCode && PromoCode == "DEC2021S20" && DiscAmount > 0)
                //{
                //    if ((listAWBDetail.Count - 1) == i)
                //    {
                //        if (TotalDiscAmt != Convert.ToDecimal(20.21))
                //        {
                //            TotalDiscAmt -= DiscAmount;
                //            DiscAmount += Convert.ToDecimal(0.01);
                //            TotalDiscAmt += DiscAmount;

                //            if (TotalDiscAmt != Convert.ToDecimal(20.21))
                //            {
                //                TotalDiscAmt -= DiscAmount;
                //                DiscAmount -= Convert.ToDecimal(0.02);
                //                TotalDiscAmt += DiscAmount;
                //            }
                //        }
                //    }
                //}

                if (IsPromoCode && PromoCode == "CNY88S21" && DiscAmount > 0)
                {
                    if ((AWBDetailList.Count - 1) == i)
                    {
                        if (TotalDiscAmt > Convert.ToDecimal(8.80))
                        {
                            TotalDiscAmt -= DiscAmount;
                            DiscAmount -= Convert.ToDecimal(0.01);
                            TotalDiscAmt += DiscAmount;                            
                        }
                        else if (TotalDiscAmt < Convert.ToDecimal(8.80))
                        {
                            TotalDiscAmt -= DiscAmount;
                            DiscAmount += Convert.ToDecimal(0.01);
                            TotalDiscAmt += DiscAmount;
                        }
                    }
                }

                //Check Tax need to be calculated
                if ((tempSelectedShipment.StartsWith("MOTOR") || HighestWeight > 30) && tempLocality == "Malaysia")
                {
                    TaxCalc = GetRoundingVal((Convert.ToDecimal(tempInsurenceCharges)), 2);
                }
                else
                {
                    TaxCalc = GetRoundingVal((TotAmtexcludeInsurans - DiscAmount + Convert.ToDecimal(tempInsurenceCharges)), 2);
                }

                TotAmtAfterDisc = GetRoundingVal((TotAmtexcludeInsurans - DiscAmount + Convert.ToDecimal(tempInsurenceCharges)), 2);
                TotAllItem += TotAmtAfterDisc;
                TotalRASurchage += tempRASurcharge;

                if (tempQuotationType == StandardQuotationType && Convert.ToDecimal(tempTaxPercentage) > 0) { LocalAmountBeforeTax += TaxCalc; } else if (tempQuotationType == InternationalQuotationType) { INTLAmountBeforeTax += TaxCalc; }

                TotalShipmentAWB += Convert.ToDecimal(tempPrice);
                TotalAWB += Convert.ToDecimal(tempTotalAmount);
                TotalInsurance += Convert.ToDecimal(tempInsurenceCharges);
                TotalOtherCharges += Convert.ToDecimal(tempOtherCharges);

                Itemlist.Add(new StationaryModel { itemno = no, itemname = "AWB No. Pending.. Dest -" + tempDestCode + " (" + tempAWBNo + ")", rate = tempPrice.ToString(), OtherCharges = tempOtherCharges.ToString() == "0" ? "-" : tempOtherCharges.ToString(), Insurans = tempInsurenceCharges.ToString() == "0" ? "-" : tempInsurenceCharges.ToString(), quantity = "1", discount = DiscAmount.ToString() == "0" ? "-" : DiscAmount.ToString(), total = TotAmtAfterDisc.ToString() });
                no += 1;

                if (isPay)//Proceed for payment
                {
                    string MobRefNum_ = tempMobileReferenceNo == "" ? "" : tempMobileReferenceNo,
                        PostCodeId_ = tempPostCode.ToString() == "" ? "0" : tempPostCodeId.ToString(),
                        DestCode_ = tempDestCode == "" ? "" : tempDestCode,
                        ShipmentType_ = tempSelectedShipment == "" ? "" : tempSelectedShipment,
                        Pieces_ = tempPieces.ToString() == "" ? "1" : tempPieces.ToString(),
                        Weight_ = tempWeight.ToString() == "" ? "0" : tempWeight.ToString(),
                        VLength_ = tempVolLength.ToString() == "" ? "0" : tempVolLength.ToString(),
                        VBreadth_ = tempVolWidth.ToString() == "" ? "0" : tempVolWidth.ToString(),
                        VHeight_ = tempVolHeight.ToString() == "" ? "0" : tempVolHeight.ToString(),
                        VolWeight_ = tempVolWeight.ToString() == "" ? "0" : tempVolWeight.ToString(),
                        HighestWeight_ = Convert.ToDecimal(Weight_) > Convert.ToDecimal(VolWeight_) ? Weight_ : VolWeight_,
                        Price_ = tempPrice.ToString() == "" ? "0" : tempPrice.ToString(),
                        OtherCharges_ = tempOtherCharges.ToString() == "" ? "0" : tempOtherCharges.ToString(),
                        TotalAm_ = (Convert.ToDecimal(Price_) + Convert.ToDecimal(OtherCharges_)).ToString(),
                        InsShipmentValue_ = tempInsShipmentValue.ToString() == "" ? "0" : tempInsShipmentValue.ToString(),
                        InsShipmentDesc_ = tempInsShipmentDesc == "" ? "" : tempInsShipmentDesc,
                        InsValue_ = tempInsurenceCharges.ToString() == "" ? "0" : tempInsurenceCharges.ToString(),
                        StaffDiscAmt_ = IsStaffCode == true ? DiscAmount.ToString() : "0",
                        PromoCode_ = IsPromoCode == true ? PromoCode : "",
                        PromoRate_ = IsPromoCode == true ? DiscPercentage.Replace("%", "").Replace("RM", "") : "0",
                        PromoDiscAmt_ = IsPromoCode == true ? DiscAmount.ToString() : "0",
                        TaxCode_ = tempQuotationType == "Standard" ? StandardTaxCode : InternationalTaxCode,
                        TaxRate_ = tempTaxPercentage.ToString();

                    finalistAWB.Add(MobRefNum_ + "|" + PostCodeId_ + "|" + DestCode_ + "|" + ShipmentType_ + "|" + Pieces_ + "|" + Weight_ + "|" + VLength_ + "|" + VBreadth_ + "|" + VHeight_ + "|" + VolWeight_ + "|" + HighestWeight_ + "|" + Price_ + "|" + OtherCharges_ + "|" + TotalAm_ + "|" + InsShipmentValue_ + "|" + InsShipmentDesc_ + "|" + InsValue_ + "|" + StaffDiscAmt_ + "|" + PromoCode_ + "|" + PromoRate_ + "|" + PromoDiscAmt_ + "|" + TaxCode_ + "|" + TaxRate_ + "|" + tempAWBNo + "|" + tempRASurcharge);
                }

            }            

            //Start inserting stationery into gridview
            foreach (DataRow stationaryrow in dtItemSummary.Rows)
            {
                if (Convert.ToInt32(stationaryrow["Quantity"]) > 0)
                {
                    decimal TotSum = 0, StationaryRate = 0;
                    StationaryRate = Convert.ToDecimal(stationaryrow["Rate"]);
                    TotSum = GetRoundingVal(StationaryRate, 2) * Convert.ToInt32(stationaryrow["Quantity"]);
                    TotalStationaryPrice += TotSum;
                    TotAllItem += TotSum;
                    Itemlist.Add(new StationaryModel { itemno = no, itemcode = stationaryrow["ColOneText"].ToString(), itemname = stationaryrow["StationaryName"].ToString().ToUpper(), UOM = stationaryrow["UOM"].ToString(), rate = Convert.ToDecimal(stationaryrow["Rate"]).ToString("0.00"), OtherCharges = "-", Insurans = "-", quantity = stationaryrow["Quantity"].ToString(), discount = "-", total = TotSum.ToString() });
                    no += 1;

                    if (isPay)//Proceed for payment
                    {
                        listStationaryDetail.Add(stationaryrow["ColOneId"].ToString() + "|" + stationaryrow["ColOneText"].ToString() + "|" + stationaryrow["StationaryName"].ToString() + "|" + stationaryrow["Quantity"].ToString() + "|" + stationaryrow["UOM"].ToString() + "|" + Convert.ToDecimal(stationaryrow["Rate"]).ToString() + "|" + TotSum.ToString());
                    }
                }
            }

            LocalAmountBeforeTax += TotalStationaryPrice;
            LocalTaxAmount = GetRoundingVal((LocalAmountBeforeTax > 0 ? LocalAmountBeforeTax * StandardTaxRate / 100 : 0), 2);
            INTLTaxAmount = GetRoundingVal((INTLAmountBeforeTax > 0 ? INTLAmountBeforeTax * InternationalTaxRate / 100 : 0), 2);
            TotalAmtAfterTax = GetRoundingVal((TotAllItem + LocalTaxAmount + INTLTaxAmount + TotalRASurchage), 2);
            RoundingAmt = RoundingValue(TotalAmtAfterTax);
            TotalAmtAfterTax = GetRoundingVal((TotalAmtAfterTax + RoundingAmt), 2);


            if (isPay)//Proceed for payment
            {
                string Price_ = TotalShipmentAWB.ToString(),
                    ShipmentTotal_ = TotalAWB.ToString(),
                    TaxAmount_ = (LocalTaxAmount + INTLTaxAmount).ToString(),
                    StationaryTotal_ = TotalStationaryPrice.ToString(),
                    IsInsuranced_ = "0",
                    InsShipmentValue_ = "0",
                    InsValue_ = "0",
                    InsGSTValue_ = "0",
                    InsuranceTotal_ = TotalInsurance.ToString(),
                    CSalesTotal_ = TotalAmtAfterTax.ToString(),
                    StaffCode_ = StaffCode,
                    StaffName_ = StaffName,
                    StaffDisAmt_ = StaffName == "" ? "0" : TotalDiscAmt.ToString(),
                    CUserID_ = LoginUserId.ToString(),
                    OtherCharges_ = TotalOtherCharges == 0 ? "0" : TotalOtherCharges.ToString(),
                    PromoCode_ = PromoCode,
                    PromoPercentage_ = IsPromoCode == true ? PromoDiscPercentage : "0",
                    PromoAmount_ = PromoCode == "" ? "0" : TotalDiscAmt.ToString(),
                    TaxTitle_ = "",
                    TaxPercentage_ = "0",
                    RoundingAmt_ = RoundingAmt.ToString();

                tblCashSalesInfo = Price_ + "|" + ShipmentTotal_ + "|" + TaxAmount_ + "|" + StationaryTotal_ + "|" + IsInsuranced_ + "|" + InsShipmentValue_ + "|" + InsValue_ + "|" + InsGSTValue_ + "|" + InsuranceTotal_ + "|" + CSalesTotal_ + "|" + StaffCode_ + "|" + StaffName_ + "|" + StaffDisAmt_ + "|" + CUserID_ + "|" + OtherCharges_ + "|" + PromoCode_ + "|" + PromoPercentage_ + "|" + PromoAmount_ + "|" + TaxTitle_ + "|" + TaxPercentage_ + "|" + RoundingAmt_;

                PaymentView = TotalAWB + "|" + TotalStationaryPrice + "|" + TotalDiscAmt + "|" + LocalTaxAmount + "|" + INTLTaxAmount + "|" + TotalRASurchage + "|" + TotalAmtAfterTax + "|" + RoundingAmt;
            }

            //For Display on CashSales Screen
            DBeforeTax = TotAllItem <= 0 ? "RM 0.00" : "RM " + TotAllItem.ToString();
            DSST = LocalTaxAmount <= 0 ? "RM 0.00" : "RM " + LocalTaxAmount.ToString();
            DSurcharge = INTLTaxAmount <= 0 ? "RM 0.00" : "RM " + INTLTaxAmount.ToString();
            RASurcharge = TotalRASurchage <= 0 ? "RM 0.00" : "RM " + TotalRASurchage.ToString();
            DRounding = RoundingAmt == 0 ? "RM 0.00" : "RM " + RoundingAmt.ToString();
            DTotalAmt = TotalAmtAfterTax <= 0 ? "RM 0.00" : "RM " + TotalAmtAfterTax.ToString();

            isPay = false;
        }

        private bool SpecialConditionPromo()
        {
            bool result = true;
            PromoValue = 0;

            if (PromoCode != null && PromoCode == "DEC2021S20")
            {
                for (int i = 0; i < AWBDetailList.Count; i++)
                {
                    if (PromoCode != "" && PromoShipmentType.ToUpper().Contains(AWBDetailList[i].ShipmentType))
                    {
                        PromoValue += (AWBDetailList[i].Price + AWBDetailList[i].OtherCharges);
                    }
                }

                if (PromoValue < 250)
                {
                    CancelDiscount();
                    //LoadGrid();
                    result = false;
                    MessageBox.Show("Promo Code (DEC2021S20) only valid for spending more than RM250 excluded material/ insurance charges/ motor and bicycles of shipment", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }               
            }

            if (PromoCode != null && PromoCode == "BKS05S21" && AWBDetailList.Count > 0)
            {
                bool IsMobileRefExist = false;                

                for (int i = 0; i < AWBDetailList.Count; i++)
                {
                    if (AWBDetailList[i].MobileRefNo != "")
                    {
                        IsMobileRefExist = true;
                        break;
                    }
                }

                if (IsMobileRefExist)
                {
                    result = true;
                }
                else
                {
                    CancelDiscount();
                    result = false;
                    MessageBox.Show("Promo Code (BKS05S21) only valid for BookShipment AWB only excluded courier material/ insurance charges/ motor and bicycles type of shipments", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

            if (PromoCode != null && PromoCode == "CNY88S21")
            {
                for (int i = 0; i < AWBDetailList.Count; i++)
                {
                    if (PromoCode != "" && PromoShipmentType.ToUpper().Contains(AWBDetailList[i].ShipmentType))
                    {
                        PromoValue += (AWBDetailList[i].Price + AWBDetailList[i].OtherCharges);
                    }
                }

                if (PromoValue < 188)
                {
                    CancelDiscount();
                    result = false;
                    MessageBox.Show("Promo Code (CNY88S21) only valid for spending more than RM188 excluded material/ insurance charges/ motor and bicycles of shipment", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

            return result;
        }

        private decimal RoundingValue(decimal Val)
        {
            decimal res = 0, roundby = (decimal)0.05;

            try
            {
                decimal temp = GetRoundingVal(Val % roundby, 2);

                if (temp == (decimal).01 || temp == (decimal).02)
                    res = temp * -1;
                else if (temp == (decimal).03 || temp == (decimal).04)
                    res = roundby - temp;
            }
            catch { }

            return res;
        }

        private bool isStaffCodeValid()
        {
            bool isValid_ = false;

            for (int i = 0; i < AWBDetailList.Count; i++)
            {
                string QuotationType = AWBDetailList[i].QuotationType;
                if (QuotationType != "Standard")
                {
                    isValid_ = false;
                }
                else
                {
                    isValid_ = true;
                    break;
                }
            }

            return isValid_;
        }

        private void CancelDiscount()
        {
            PromoCode = "";
            StaffCode = "";
            IsStaffCode = false;
            IsPromoCode = false;
            Wndow.textnameortitle.Content = "Promo Title :"; NameOrTitle = ""; StaffName = ""; StaffCode = "";
            Wndow.textDiscPercORStaffCode.Content = "Disc Percentage :"; DiscAmount = 0; DiscPercentage = ""; PromoDiscPercentage = "";
        }

        private void ResetData()
        {
            CancelDiscount();
            DiscPercentage = "";
            LocalTaxAmount = 0; INTLTaxAmount = 0;
            Itemlist.Clear();
            createDataTable();
            AWBNo = 0;
            listDestCodeDetail.Clear(); listRefDetail.Clear(); tblCashSalesInfo = ""; PaymentView = "";
            AWBDetailList.Clear();
            finalistAWB.Clear();
            IsStaffCode = false;
            IsPromoCode = false;
            Wndow.textnameortitle.Content = "Promo Title :"; NameOrTitle = ""; StaffName = ""; StaffCode = "";
            Wndow.textDiscPercORStaffCode.Content = "Disc Percentage :"; DiscAmount = 0; DiscPercentage = ""; PromoDiscPercentage = "";
            TotalAmtAfterTax = 0; TotAllItem = 0; RoundingAmt = 0;

            Rectangle diagonalFillRectangle = new Rectangle();
            diagonalFillRectangle.Width = 200;
            diagonalFillRectangle.Height = 100;
            isDone = false;
            DBeforeTax = "RM 0.00"; DSST = "RM 0.00"; DSurcharge = "RM 0.00"; DRounding = "RM 0.00"; DTotalAmt = "RM 0.00"; RASurcharge = "RM 0.00";
        }

        private void getStationeryTax()
        {
            SqlDataReader dr;
            dr = GetDataReader($"Select QuotationType, TaxTitle, TaxPercentage FROM tblquotation");
            while (dr.Read())
            {
                if (dr["QuotationType"].ToString() == "Standard")
                {
                    StationeryTax = dr["TaxPercentage"].ToString();
                }
            }
        }

        private bool CheckStaffCode()
        {
            bool res = false;
            try
            {
                StaffInfo Staff = new StaffInfo(); Staff.StaffCode = StaffCode;
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = client.PostAsJsonAsync($"http://hrms.skynet.com.my:8021/getStaffStatus.php/", Staff).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var query = response.Content.ReadAsStringAsync().Result;
                        if (query.ToString().Contains("Active"))
                            res = true;
                    }
                    else
                    {
                        error_log.errorlog("[MobileRefCashSalesVM.CheckStaffCode] Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    }
                }
                else
                {
                    MessageBox.Show("Internet connection failure", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { General.error_log.errorlog("API in Check StaffCode : " + Ex.ToString()); }
            return res;
        }

        public class StaffInfo
        {
            public string StaffCode { get; set; }
        }
    }

    public class CSMRShipmentTypeModel : PropertyChangedNotification
    {
        public string ShipmentTypeId { get { return GetValue(() => ShipmentTypeId); } set { SetValue(() => ShipmentTypeId, value); OnPropertyChanged("ShipmentTypeId"); } }
        public string ShipmentTypeName { get { return GetValue(() => ShipmentTypeName); } set { SetValue(() => ShipmentTypeName, value); OnPropertyChanged("ShipmentTypeName"); } }
    }

    public class PaymentModeModel : PropertyChangedNotification
    {
        public string PaymentModeId { get { return GetValue(() => PaymentModeId); } set { SetValue(() => PaymentModeId, value); OnPropertyChanged("PaymentModeId"); } }
        public string PaymentModeName { get { return GetValue(() => PaymentModeName); } set { SetValue(() => PaymentModeName, value); OnPropertyChanged("PaymentModeName"); } }
    }

    public class StationaryModel : PropertyChangedNotification
    {
        private int _itemno;
        public int itemno
        {
            get { return _itemno; }
            set { _itemno = value; OnPropertyChanged("itemno"); }
        }

        private string _itemcode;
        public string itemcode
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("itemcode"); }
        }

        //Description
        private string _itemname;
        public string itemname
        {
            get { return _itemname; }
            set { _itemname = value; OnPropertyChanged("itemname"); }
        }
        private string _UOM;
        public string UOM
        {
            get { return _UOM; }
            set { _UOM = value; OnPropertyChanged("UOM"); }
        }

        //Price
        private string _rate;
        public string rate
        {
            get { return _rate; }
            set { _rate = value; OnPropertyChanged("rate"); }
        }
        private string _quantity;
        public string quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged("quantity"); }
        }
        private string _sst;
        public string sst
        {
            get { return _sst; }
            set { _sst = value; OnPropertyChanged("sst"); }
        }
        private string _discount;
        public string discount
        {
            get { return _discount; }
            set { _discount = value; OnPropertyChanged("discount"); }
        }
        private string _total;
        public string total
        {
            get { return _total; }
            set { _total = value; OnPropertyChanged("total"); }
        }
        private string _Insurans;
        public string Insurans
        {
            get { return _Insurans; }
            set { _Insurans = value; OnPropertyChanged("Insurans"); }
        }
        private string _OtherCharges;
        public string OtherCharges
        {
            get { return _OtherCharges; }
            set { _OtherCharges = value; OnPropertyChanged("OtherCharges"); }
        }
        private string _Color;
        public string Color
        {
            get { return _Color; }
            set { _Color = value; OnPropertyChanged("Color"); }
        }
        //public string ItemTypeSource { get { return GetValue(() => ItemTypeSource); } set { SetValue(() => ItemTypeSource, value); OnPropertyChanged("ItemTypeSource"); } }
        public ImageSource ItemTypeSource { get { return GetValue(() => ItemTypeSource); } set { SetValue(() => ItemTypeSource, value); OnPropertyChanged("ItemTypeSource"); } }
        public string StationaryType { get { return GetValue(() => StationaryType); } set { SetValue(() => StationaryType, value); OnPropertyChanged("StationaryType"); } }
    }

    public class AWBDetailModel : PropertyChangedNotification
    {
        public string MobileReferenceNo_ { get; set; }
        public string ShipmentType_ { get; set; }
        public string DestCode_ { get; set; }
        public int Pieces_ { get; set; }
        public string StrWeight_ { get; set; }
        public decimal VolWeight_ { get; set; }
        public decimal OtherCharges_ { get; set; }
    }

    public class QuotationInfo
    {
        public string AWBNumber { get; set; }
        public string ZoneDesc { get; set; }
        public string ShipmentType { get; set; }
        public decimal FirstWeight { get; set; }
        public decimal ForstRate { get; set; }
        public decimal AddWeight { get; set; }
        public decimal AddRate { get; set; }
    }

    public class AWBInfo : PropertyChangedNotification
    {
        public int AWBNo { get; set; }
        public string ShipmentType { get; set; }
        public string DestCode { get; set; }
        public int Pieces { get; set; }
        public decimal Weight { get; set; }
        public decimal VolLength { get; set; }
        public decimal VolWidth { get; set; }
        public decimal VolHeight { get; set; }
        public decimal VolWeight { get; set; }
        public decimal AddRate { get; set; }
        public decimal Price { get; set; }
        public decimal OtherCharges { get; set; }
        public bool InsCheck { get; set; }
        public decimal InsShipmentValue { get; set; }
        public string InsShipmentDesc { get; set; }
        public decimal InsCharges { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxPercentage { get; set; }
        public string MobileRefNo { get; set; }
        public string Locality { get; set; }
        public int PostCodeId { get; set; }
        public string PostCode { get; set; }
        public string DestUnder { get; set; }
        public string QuotationType { get; set; }
        public string Country { get; set; }
        public string PostCodeIntl { get; set; }
        public decimal RASurcharge { get; set; }
    }

    public class Payment_AWBInfo : PropertyChangedNotification
    {
        public int AWBNo { get; set; }
        public string ShipmentType { get; set; }
        public string DestCode { get; set; }
        public int Pieces { get; set; }
        public decimal Weight { get; set; }
        public decimal VolLength { get; set; }
        public decimal VolWidth { get; set; }
        public decimal VolHeight { get; set; }
        public decimal VolWeight { get; set; }
        public decimal AddRate { get; set; }
        public decimal Price { get; set; }
        public decimal OtherCharges { get; set; }
        public bool InsCheck { get; set; }
        public decimal InsShipmentValue { get; set; }
        public string InsShipmentDesc { get; set; }
        public decimal InsCharges { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxPercentage { get; set; }
        public string MobileRefNo { get; set; }
        public string Locality { get; set; }
        public int PostCodeId { get; set; }
        public string PostCode { get; set; }
        public string DestUnder { get; set; }
        public string QuotationType { get; set; }

    }
}

