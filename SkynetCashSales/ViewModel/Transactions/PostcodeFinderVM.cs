﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Reports;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class PostcodeFinderVM : AMGenFunction
    {
        public FrmPostcodeFinder MyWind;
        public DataTable dtPostCode, dtFilterData;
        public event Action<PostcodeInfo> Closed;
        public PostcodeFinderVM(FrmPostcodeFinder Wind)
        {
            MyWind = Wind;
            PostcodeDetails = new ObservableCollection<PostcodeInfo>();
            IsLoadFromMainWindow = true;
            ResetData();
        }

        public PostcodeFinderVM(FrmPostcodeFinder Wind, bool IsFromOtherWind)
        {
            MyWind = Wind;
            PostcodeDetails = new ObservableCollection<PostcodeInfo>();
            IsLoadFromMainWindow = false;
            ResetData();
        }

        public PostcodeFinderVM(FrmPostcodeFinder Wind, bool IsFromOtherWind, string PostCode_)
        {
            MyWind = Wind;
            PostcodeDetails = new ObservableCollection<PostcodeInfo>();
            IsLoadFromMainWindow = false;
            ResetData();
            PostCode = PostCode_;
            Remarks = "ODA";
        }

        bool IsLoadFromMainWindow;
        public string PostCode { get { return GetValue(() => PostCode); } set { SetValue(() => PostCode, value); OnPropertyChanged("PostCode"); if (PostCode != null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string Town { get { return GetValue(() => Town); } set { SetValue(() => Town, value); OnPropertyChanged("Town"); if (Town != null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string City { get { return GetValue(() => City); } set { SetValue(() => City, value); OnPropertyChanged("City"); if (City != null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string State { get { return GetValue(() => State); } set { SetValue(() => State, value); OnPropertyChanged("State"); if (State != null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string StnCode { get { return GetValue(() => StnCode); } set { SetValue(() => StnCode, value); OnPropertyChanged("StnCode"); if (StnCode!= null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string FreqText { get { return GetValue(() => FreqText); } set { SetValue(() => FreqText, value); OnPropertyChanged("FreqText"); if (FreqText!= null && dtPostCode.Rows.Count > 0) GenerateReport(); } }
        public string Remarks { get { return GetValue(() => Remarks); } set { SetValue(() => Remarks, value); OnPropertyChanged("Remarks"); if (Remarks != null && dtPostCode.Rows.Count > 0) GenerateReport(); } }

        public string DispText1 { get { return GetValue(() => DispText1); } set { SetValue(() => DispText1, value); OnPropertyChanged("DispText1"); } }
        public string DelStnCode { get { return GetValue(() => DelStnCode); } set { SetValue(() => DelStnCode, value); OnPropertyChanged("DelStnCode"); } }

        public string DispPostCode { get { return GetValue(() => DispPostCode); } set { SetValue(() => DispPostCode, value); OnPropertyChanged("DispPostCode"); } }
        public string DispTown { get { return GetValue(() => DispTown); } set { SetValue(() => DispTown, value); OnPropertyChanged("DispTown"); } }
        public string DispCity { get { return GetValue(() => DispCity); } set { SetValue(() => DispCity, value); OnPropertyChanged("DispCity"); } }
        public string DispState { get { return GetValue(() => DispState); } set { SetValue(() => DispState, value); OnPropertyChanged("DispState"); } }
        public string DispDelStnCode { get { return GetValue(() => DispDelStnCode); } set { SetValue(() => DispDelStnCode, value); OnPropertyChanged("DispDelStnCode"); } }
        public string DispDeliveryFreq { get { return GetValue(() => DispDeliveryFreq); } set { SetValue(() => DispDeliveryFreq, value); OnPropertyChanged("DispDeliveryFreq"); } }
        public string DispRemarks { get { return GetValue(() => DispRemarks); } set { SetValue(() => DispRemarks, value); OnPropertyChanged("DispRemarks"); } }

        public string SelectPostCode { get { return GetValue(() => SelectPostCode); } set { SetValue(() => SelectPostCode, value); OnPropertyChanged("SelectPostCode"); } }
        public string SelectTown { get { return GetValue(() => SelectTown); } set { SetValue(() => SelectTown, value); OnPropertyChanged("SelectTown"); } }
        public string SelectCity { get { return GetValue(() => SelectCity); } set { SetValue(() => SelectCity, value); OnPropertyChanged("SelectCity"); } }
        public string SelectState { get { return GetValue(() => SelectState); } set { SetValue(() => SelectState, value); OnPropertyChanged("SelectState"); } }
        public string SelectDelStnCode { get { return GetValue(() => SelectDelStnCode); } set { SetValue(() => SelectDelStnCode, value); OnPropertyChanged("SelectDelStnCode"); } }
        public string SelectDeliveryFreq { get { return GetValue(() => SelectDeliveryFreq); } set { SetValue(() => SelectDeliveryFreq, value); OnPropertyChanged("SelectDeliveryFreq"); } }
        public string SelectRemarks { get { return GetValue(() => SelectRemarks); } set { SetValue(() => SelectRemarks, value); OnPropertyChanged("SelectRemarks"); } }

        private ObservableCollection<PostcodeInfo> _PostcodeDetails = new ObservableCollection<PostcodeInfo>();
        public ObservableCollection<PostcodeInfo> PostcodeDetails { get { return _PostcodeDetails; } set { _PostcodeDetails = value; } }

        private PostcodeInfo _SelectedPostcode = new PostcodeInfo();
        public PostcodeInfo SelectedPostcode { get { return _SelectedPostcode; } set { if (_SelectedPostcode != value) { _SelectedPostcode = value; OnPropertyChanged("SelectedItem"); } if (_SelectedPostcode != null) DispSelectedItem(); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "Print": 
                        {
                            FrmReportViewer RptViewerGen = new FrmReportViewer();
                            RptViewerGen.Owner = Application.Current.MainWindow;
                            ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompAddress, "", dtFilterData, "", dtFilterData, "dtPostCode", dtFilterData, "PostCode.rdlc", "", RptViewerGen.RptViewer, true, false);
                            RptViewerGen.DataContext = VM;
                            RptViewerGen.Show();
                        } break;
                    case "Ok":
                        {
                            if (IsLoadFromMainWindow == false)
                            {
                                CloseWind();
                                if (Closed != null & SelectedPostcode != null)
                                {
                                    var SelectedItems = new PostcodeInfo() { PostCodeId = SelectedPostcode.PostCodeId, PostCode = SelectedPostcode.PostCode, Town = SelectedPostcode.Town, City = SelectedPostcode.City, State = SelectedPostcode.State, StnCode = SelectedPostcode.StnCode, DeliveryFreq = SelectedPostcode.DeliveryFreq, Remarks = SelectedPostcode.Remarks };

                                    try
                                    {
                                        DataTable dtDestcode = new DataTable();
                                        dtDestcode = GetDataTable($"SELECT DestCode FROM tblDestination WHERE DestCode = '{SelectedPostcode.StnCode}'");
                                        if (dtDestcode == null || dtDestcode.Rows.Count == 0)
                                        {
                                            WebDb = new CommunicateWebDb();
                                            WebDb.GetDestination(SelectedPostcode.StnCode);
                                        }
                                        else
                                        {
                                            dtDestcode = new DataTable();
                                            dtDestcode = GetDataTable($"SELECT DestCode FROM tblZoneDetail WHERE DestCode = '{SelectedPostcode.StnCode}'");
                                            if (dtDestcode == null || dtDestcode.Rows.Count == 0)
                                            {
                                                WebDb = new CommunicateWebDb();
                                                WebDb.GetDestination(SelectedPostcode.StnCode);
                                            }
                                        }
                                    }
                                    catch { }

                                    Closed(SelectedItems);
                                }
                            }
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void DispSelectedItem()
        {
            DispText1 = "\n Information of Delivery Station. "; DelStnCode = SelectedPostcode.StnCode;

            DispPostCode = "\n Postcode           : "; SelectPostCode = SelectedPostcode.PostCode;
            DispTown = "\n Town                 : "; SelectTown = SelectedPostcode.Town;
            DispCity = "\n City                   : "; SelectCity = SelectedPostcode.City;
            DispState = "\n Sate                   : "; SelectState = SelectedPostcode.State;
            DispDelStnCode = "\n Del Station        : "; SelectDelStnCode = SelectedPostcode.StnCode; 
            DispDeliveryFreq = "\n Delivery Freq.    : "; SelectDeliveryFreq = SelectedPostcode.DeliveryFreq;
            DispRemarks = "\n Remarks            : "; SelectRemarks = SelectedPostcode.Remarks;
        }

        private void ResetData()
        {
            dtPostCode = new DataTable();

            PostCode = ""; Town = ""; City = ""; State = "" ; StnCode = ""; FreqText = ""; Remarks = "";

            DispText1 = "\n Information of Delivery Station. "; DelStnCode = "";

            DispPostCode = "\n Postcode           : "; SelectPostCode = "";
            DispTown = "\n Town                 : "; SelectTown = "";
            DispCity = "\n City                   : "; SelectCity = "";
            DispState = "\n Sate                   : "; SelectState = "";
            DispDelStnCode = "\n Del Station        : "; SelectDelStnCode = "";
            DispDeliveryFreq = "\n Delivery Freq.    : "; SelectDeliveryFreq = "";
            DispRemarks = "\n Remarks            : "; SelectRemarks = "";

            PostcodeDetails.Clear();
            dtPostCode = GetDataTable("SELECT PostCodeId, PostCode, Town, City, State, StnCode, deliveryFrequency, Remarks FROM tblPostcode2020 WHERE isdelete = 0");

            GenerateReport();

            MyWind.txtPostCode.Focus();
        }

        private void GenerateReport()
        {
            dtFilterData = new DataTable();

            PostcodeDetails.Clear();
            if (dtPostCode != null && dtPostCode.Rows.Count > 0)
            {
                dtFilterData = dtPostCode;

                if (dtFilterData != null && dtFilterData.Rows.Count > 0)
                {
                    if (PostCode.Trim() == "" && Town.Trim() == "" && City.Trim() == "" && State.Trim() == "" && StnCode.Trim() == "" && FreqText.Trim() == "" && Remarks.Trim() == "")
                    {
                        using (dtFilterData = new DataTable())
                        {
                            dtFilterData = GetDataTable("SELECT TOP 1000 PostCodeId, PostCode, Town, City, State, StnCode, deliveryFrequency, Remarks FROM tblPostcode2020 WHERE isdelete = 0");
                            dtFilterData = DataTableSort(dtFilterData, "PostCode, StnCode, State, City, Town", "ASC");
                            for (int i = 0; i < dtFilterData.Rows.Count; i++)
                            {
                                PostcodeDetails.Add(new PostcodeInfo { Item = PostcodeDetails.Count + 1, PostCodeId = Convert.ToInt32(dtFilterData.Rows[i]["PostCodeId"]), PostCode = dtFilterData.Rows[i]["PostCode"].ToString(), Town = dtFilterData.Rows[i]["Town"].ToString(), City = dtFilterData.Rows[i]["City"].ToString(), State = dtFilterData.Rows[i]["State"].ToString(), StnCode = dtFilterData.Rows[i]["StnCode"].ToString(), DeliveryFreq = dtFilterData.Rows[i]["deliveryFrequency"].ToString(), Remarks = dtFilterData.Rows[i]["Remarks"].ToString() });
                            }
                        }
                    }
                    else
                    {
                        using (DataView view = new DataView(dtFilterData, "PostCode LIKE '" + PostCode + "%' AND Town LIKE '" + Town.Replace("'", "''") + "%' AND City LIKE '" + City.Replace("'", "''") + "%' AND State LIKE '" + State.Replace("'", "''") + "%' AND StnCode LIKE '" + StnCode + "%' AND deliveryFrequency LIKE '" + FreqText.Replace("'", "''") + "%' AND Remarks LIKE '" + Remarks.Replace("'", "''") + "%'", "", DataViewRowState.CurrentRows))
                        {
                            using (dtFilterData = new DataTable())
                            {
                                dtFilterData = view.ToTable();
                                dtFilterData = DataTableSort(dtFilterData, "PostCode, StnCode, State, City, Town", "ASC");
                                for (int i = 0; i < dtFilterData.Rows.Count; i++)
                                {
                                    PostcodeDetails.Add(new PostcodeInfo { Item = PostcodeDetails.Count + 1, PostCodeId = Convert.ToInt32(dtFilterData.Rows[i]["PostCodeId"]), PostCode = dtFilterData.Rows[i]["PostCode"].ToString(), Town = dtFilterData.Rows[i]["Town"].ToString(), City = dtFilterData.Rows[i]["City"].ToString(), State = dtFilterData.Rows[i]["State"].ToString(), StnCode = dtFilterData.Rows[i]["StnCode"].ToString(), DeliveryFreq = dtFilterData.Rows[i]["deliveryFrequency"].ToString(), Remarks = dtFilterData.Rows[i]["Remarks"].ToString() });
                                }
                            }
                        }
                    }
                }
            }
        }

        public class PostcodeInfo : AMGenFunction
        {
            public int Item { get; set; }
            public int PostCodeId { get; set; }
            public string PostCode { get; set; }            
            public string Town { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string StnCode { get; set; }
            public string DeliveryFreq { get; set; }
            public string Remarks { get; set; }
        }

    }
}
