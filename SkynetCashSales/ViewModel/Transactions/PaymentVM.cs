﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    class PaymentVM : CNPrintVM
    {
        public FrmPayment MyWind;
        MobileRefCashSalesVM cashSalesVM;
        List<string> AWBs = new List<string>();
        List<string> Ref = new List<string>();
        List<string> StationaryListItems = new List<string>();

        public PaymentVM(FrmPayment frmPayment, MobileRefCashSalesVM cashSalesVM_)
        {
            MyWind = frmPayment;
            MyWind.ResizeMode = ResizeMode.NoResize;
            cashSalesVM = cashSalesVM_;
            CheckPrinter();
            getTaxDetail();
            PaymentModeComboboxload();
            Load();
        }

        private ObservableCollection<PaymentModeModel> _PaymentModeList = new ObservableCollection<PaymentModeModel>();
        public ObservableCollection<PaymentModeModel> PaymentModeList { get { return _PaymentModeList; } set { _PaymentModeList = value; OnPropertyChanged("PaymentModeList"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        public string PaymentMode { get { return GetValue(() => PaymentMode); } set { SetValue(() => PaymentMode, value); OnPropertyChanged("PaymentMode"); } }
        public string PaymentReference { get { return GetValue(() => PaymentReference); } set { SetValue(() => PaymentReference, value); OnPropertyChanged("PaymentReference"); } }
        public string CSalesNo { get { return GetValue(() => CSalesNo); } set { SetValue(() => CSalesNo, value); OnPropertyChanged("CSalesNo"); } }
        public string TotalAmtAfterTax { get { return GetValue(() => TotalAmtAfterTax); } set { SetValue(() => TotalAmtAfterTax, value); OnPropertyChanged("TotalAmtAfterTax"); } }
        public string AWBPrices { get { return GetValue(() => AWBPrices); } set { SetValue(() => AWBPrices, value); OnPropertyChanged("AWBPrices"); } }
        public string StationeryPrices { get { return GetValue(() => StationeryPrices); } set { SetValue(() => StationeryPrices, value); OnPropertyChanged("StationeryPrices"); } }
        public string Discounts { get { return GetValue(() => Discounts); } set { SetValue(() => Discounts, value); OnPropertyChanged("Discounts"); } }
        public string SSTs { get { return GetValue(() => SSTs); } set { SetValue(() => SSTs, value); OnPropertyChanged("SSTs"); } }
        public string Surcharges { get { return GetValue(() => Surcharges); } set { SetValue(() => Surcharges, value); OnPropertyChanged("Surcharges"); } }
        public string RASurcharge { get { return GetValue(() => RASurcharge); } set { SetValue(() => RASurcharge, value); OnPropertyChanged("RASurcharge"); } }
        public string Roundings { get { return GetValue(() => Roundings); } set { SetValue(() => Roundings, value); OnPropertyChanged("Roundings"); } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Pay":
                        {
                            //if (isCNPrinter == false && isStickerPrinter == false)
                            //{
                            //    MessageBox.Show("Please select printer before proceed. \n System > System Setting");
                            //}
                            //else
                            //{
                            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("Proceed this payment?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (MsgConfirmation == MessageBoxResult.Yes)
                            {
                                
                                Pay();
                                this.CloseWind();                                
                            }
                            //}
                        }
                        break;
                    default: break;
                    case "Cancel":
                        {
                            this.CloseWind();
                        }
                        break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void Load()
        {
            PaymentReference = "";
            string[] ViewSummary = cashSalesVM.PaymentView.Split('|');
            string TotalAWBPrice = ViewSummary[0],
                TotalStationaryPrice = ViewSummary[1],
                TotalDiscount = ViewSummary[2],
                LocalTax = ViewSummary[3],
                INTLTax = ViewSummary[4],
                RASurchages = ViewSummary[5],
                GrandTotal = ViewSummary[6],
                RoundingAmount = ViewSummary[7];

            AWBPrices = TotalAWBPrice == "0" ? "0.00" : TotalAWBPrice;
            StationeryPrices = TotalStationaryPrice == "0" ? "0.00" : TotalStationaryPrice;
            Discounts = TotalDiscount == "0" ? "0.00" : TotalDiscount;
            SSTs = LocalTax == "0" ? "0.00" : LocalTax;
            Surcharges = INTLTax == "0" ? "0.00" : INTLTax;
            RASurcharge = RASurchages == "0" ? "0.00" : RASurchages;
            Roundings = RoundingAmount == "0" ? "0.00" : RoundingAmount;

            TotalAmtAfterTax = GrandTotal;
        }

        private DataTable InsertData(string CSalesNo, string MobRefNum, string ConsignorCode, int PostCodeId, string DestCode, string ConsigneeCode, string ShipmentType, int Pieces, decimal Weight, decimal VLength, decimal VBreadth, decimal VHeight, decimal VolWeight, decimal HighestWeight, decimal Price, decimal OtherCharges, decimal TotalAmt, decimal InsShipmentValue, string InsShipmentDesc, decimal InsRate, decimal InsValue, decimal StaffDiscRate, decimal StaffDiscAmt, string PromoCode, decimal PromoRate, decimal PromoDiscAmt, string TaxCode, decimal TaxRate, decimal RASurcharges)
        {
            using (DataTable tblCSaleCNNum = new DataTable())
            {
                {
                    if (SqlCon.State == ConnectionState.Closed) SqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SP_AddCnNumber", SqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.Clear();

                    sqlCmd.Parameters.Add(new SqlParameter("@CSalesNo", CSalesNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@MobRefNum", MobRefNum));
                    sqlCmd.Parameters.Add(new SqlParameter("@OriginCode", MyCompanyCode.Trim()));
                    sqlCmd.Parameters.Add(new SqlParameter("@ConsignorCode", ConsignorCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@PostCodeId", PostCodeId));
                    sqlCmd.Parameters.Add(new SqlParameter("@DestCode", DestCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@ConsigneeCode", ConsigneeCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@ShipmentType", ShipmentType));
                    sqlCmd.Parameters.Add(new SqlParameter("@Pieces", Pieces));
                    sqlCmd.Parameters.Add(new SqlParameter("@Weight", Weight));
                    sqlCmd.Parameters.Add(new SqlParameter("@VLength", VLength));
                    sqlCmd.Parameters.Add(new SqlParameter("@VBreadth", VBreadth));
                    sqlCmd.Parameters.Add(new SqlParameter("@VHeight", VHeight));
                    sqlCmd.Parameters.Add(new SqlParameter("@VolWeight", VolWeight));
                    sqlCmd.Parameters.Add(new SqlParameter("@HighestWeight", HighestWeight));
                    sqlCmd.Parameters.Add(new SqlParameter("@Price", Price));
                    sqlCmd.Parameters.Add(new SqlParameter("@OtherCharges", OtherCharges));
                    sqlCmd.Parameters.Add(new SqlParameter("@TotalAmt", TotalAmt));
                    sqlCmd.Parameters.Add(new SqlParameter("@InsShipmentValue", InsShipmentValue));
                    sqlCmd.Parameters.Add(new SqlParameter("@InsShipmentDesc", InsShipmentDesc));
                    sqlCmd.Parameters.Add(new SqlParameter("@InsRate", InsRate));
                    sqlCmd.Parameters.Add(new SqlParameter("@InsValue", InsValue));
                    sqlCmd.Parameters.Add(new SqlParameter("@StaffDiscRate", StaffDiscRate));
                    sqlCmd.Parameters.Add(new SqlParameter("@StaffDiscAmt", StaffDiscAmt));
                    sqlCmd.Parameters.Add(new SqlParameter("@PromoCode", PromoCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@PromoRate", PromoRate));
                    sqlCmd.Parameters.Add(new SqlParameter("@PromoDiscAmt", PromoDiscAmt));
                    sqlCmd.Parameters.Add(new SqlParameter("@TaxCode", TaxCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@TaxRate", TaxRate));
                    sqlCmd.Parameters.Add(new SqlParameter("@RASurcharges", RASurcharges));
                    sqlCmd.Parameters.Add(new SqlParameter("@CUserID", LoginUserId));
                    sqlCmd.CommandTimeout = 0;

                    SqlDataAdapter sqladapter = new SqlDataAdapter(sqlCmd);
                    sqladapter.Fill(tblCSaleCNNum);                    
                }
                return tblCSaleCNNum;
            }
        }

        private void Pay()
        {
            SqlDataReader dr;
            int CSId = 0;
            string NewCN = "";
            CSalesNo = "";
            bool IsStationary = false;

            try
            {
                if (NoRange == true)
                {
                    MessageBox.Show("Kindly get the range first, If you want to Print AWB", "Warning");
                }
                else
                {
                    bool isblocked = false;
                    dr = GetDataReader($"SELECT ISNULL(IsBlockAWB, 0) As IsBlockAWB FROM tblBlockAWB WHERE Status = 1");
                    if (dr.Read())
                    {
                        isblocked = Convert.ToBoolean(dr["IsBlockAWB"]);
                    }
                    dr.Close();

                    if (!isblocked)
                    {
                        if (CSId == 0)
                        {
                            //Price_[0], ShipmentTotal_[1], TaxAmount_[2], StationaryTotal_[3], IsInsuranced_[4], InsShipmentValue_[5], InsValue_[6], InsGSTValue_[7], InsuranceTotal_[8], CSalesTotal_[9], StaffCode_[10], StaffName_[11], StaffDisAmt_[12], CUserID_[13], OtherCharges_[14], PromoCode_[15], PromoPercentage_[16], PromoAmount_[17], TaxTitle_[18], TaxPercentage_[19], RoundingAmt_[20];
                            //For tblCashSalesInfo
                            string[] CSinfo = cashSalesVM.tblCashSalesInfo.Split('|');
                            string Price_ = CSinfo[0].ToString(),
                                ShipmentTotal_ = CSinfo[1].ToString(),
                                TaxAmount_ = CSinfo[2].ToString(),
                                StationaryTotal_ = CSinfo[3].ToString(),
                                IsInsuranced_ = CSinfo[4].ToString(),
                                InsShipmentValue_ = CSinfo[5].ToString(),
                                InsValue_ = CSinfo[6].ToString(),
                                InsGSTValue_ = CSinfo[7].ToString(),
                                InsuranceTotal_ = CSinfo[8].ToString(),
                                CSalesTotal_ = CSinfo[9].ToString(),
                                StaffCode_ = CSinfo[10].ToString(),
                                StaffName_ = CSinfo[11].ToString(),
                                StaffDisAmt_ = CSinfo[12].ToString(),
                                CUserID_ = CSinfo[13].ToString(),
                                OtherCharges_ = CSinfo[14].ToString(),
                                PromoCode_ = CSinfo[15].ToString(),
                                PromoPercentage_ = CSinfo[16].ToString(),
                                PromoAmount_ = CSinfo[17].ToString(),
                                TaxTitle_ = CSinfo[18].ToString(),
                                TaxPercentage_ = CSinfo[19].ToString(),
                                RoundingAmt_ = CSinfo[20].ToString();

                            if (Convert.ToDecimal(StationaryTotal_) > 0) { IsStationary = true; }


                            CSId = ExecuteScalarQuery($"INSERT INTO tblCashSales (CSalesNo, CSalesDate, Price, ShipmentTotal, ShipmentGSTValue , StationaryTotal , IsInsuranced , InsShipmentValue , InsValue, InsGSTValue, InsuranceTotal, CSalesTotal, StaffCode, StaffName, StaffDisAmt, CUserID, CDate, MUserID, MDate, OtherCharges, IsManual, DiscCode, DiscPercentage, DiscAmount, TaxTitle, TaxPercentage, RoundingAmt, Status,PaymentMode,PaymentRefNo,PaymentRemarks, RASurcharge)" +
                                                        $"VALUES (FORMAT(NEXT VALUE FOR CSalesNo, '{CSalesNoPrefix}000000'), GETDATE(), '{ Price_}', '{ShipmentTotal_}','{ TaxAmount_}','{StationaryTotal_}', '{IsInsuranced_}', '{ InsShipmentValue_}', '{ InsValue_ }', 0, '{InsuranceTotal_}', '{CSalesTotal_}', '{ StaffCode_}', '{ StaffName_}', '{ StaffDisAmt_}', '{LoginUserId}', GETDATE(), 0, GETDATE(), {OtherCharges_}, 0, '{PromoCode_}', '{PromoPercentage_}', '{PromoAmount_}', '{TaxTitle_}', {TaxPercentage_}, {RoundingAmt_}, 1, '{PaymentMode}','{PaymentReference}','{""}', '{RASurcharge}')");

                            dr = GetDataReader($"SELECT CSalesNo FROM tblCashSales WHERE CSalesID = {CSId}");
                            CSalesNo = dr.Read() ? dr["CSalesNo"].ToString() : ""; dr.Close();

                        }

                        AWBs = cashSalesVM.finalistAWB;
                        string[] arrAWBs = AWBs.ToArray();

                        //Ref = cashSalesVM.listRefDetail;
                        //string[] arrRef = Ref.ToArray();

                        int Inserted = 0;
                        decimal TaxAmt = 0, TotTaxAmt = 0;
                        DataTable dtCSaleCNNum;
                        for (int i = 0; i < arrAWBs.Length; i++)
                        {
                            NewCN = ""; Inserted = 0; ConsignorCode = ""; ConsigneeCode = ""; TaxAmt = 0;
                            //0-MobRefNum_ 1 -PostCodeId_ 2- DestCode_ 3- ShipmentType_ 4- Pieces_ 5- Weight_ 6- VLength_ 7- VBreadth_ 8- VHeight_ 9- VolWeight_ 10- HighestWeight_ 11- Price_ 12- OtherCharges_ 13- TotalAm_ 14- InsShipmentValue_ 15- InsShipmentDesc_ 16- InsValue_ 17- StaffDiscAmt_ 18- PromoCode_ 19- PromoRate_ 20- PromoDiscAmt_ 21-TaxCode_ 22- TaxRate_);                   
                            string[] allAWB = arrAWBs[i].ToString().Split('|');

                            string MobRefNum_ = allAWB[0].ToString(),
                                PostCodeId_ = allAWB[1].ToString(),
                                DestCode_ = allAWB[2].ToString(),
                                ShipmentType_ = allAWB[3].ToString(),
                                Pieces_ = allAWB[4].ToString(),
                                Weight_ = allAWB[5].ToString(),
                                VLength_ = allAWB[6].ToString(),
                                VBreadth = allAWB[7].ToString(),
                                VHeight_ = allAWB[8].ToString(),
                                VolWeight_ = allAWB[9].ToString(),
                                HighestWeight_ = allAWB[10].ToString(),
                                Price_ = allAWB[11].ToString(),
                                OtherCharges_ = allAWB[12].ToString(),
                                TotalAm_ = allAWB[13].ToString(),
                                InsShipmentValue_ = allAWB[14].ToString(),
                                InsShipmentDesc_ = allAWB[15].ToString(),
                                InsValue_ = allAWB[16].ToString(),
                                StaffDiscAmt_ = allAWB[17].ToString(),
                                PromoCode_ = allAWB[18].ToString(),
                                PromoRate_ = allAWB[19].ToString(),
                                PromoDiscAmt_ = allAWB[20].ToString(),
                                TaxCode_ = allAWB[21].ToString(),
                                TaxRate_ = allAWB[22].ToString(),
                                TempAWBNumber = allAWB[23].ToString(),
                                RASurcharge_ = allAWB[24].ToString();

                            TaxAmt = ShipmentType_.StartsWith("MOTOR") ? (Convert.ToDecimal(InsValue_) * Convert.ToDecimal(TaxRate_) / 100) : (Convert.ToDecimal(TotalAm_) + Convert.ToDecimal(InsValue_) - Convert.ToDecimal(StaffDiscAmt_) - Convert.ToDecimal(PromoDiscAmt_)) * Convert.ToDecimal(TaxRate_) / 100;
                            TotTaxAmt += TaxAmt;

                            dtCSaleCNNum = new DataTable();
                            //dtCSaleCNNum = InsertData(CSalesNo, MobRefNum_, ConsignorCode, PostCodeId_, DestCode_, ConsigneeCode, ShipmentType_, Pieces_, Weight_, VLength_, VBreadth, VHeight_, VolWeight_, HighestWeight_, Price_, OtherCharges_, TotalAm_, InsShipmentValue_, InsShipmentDesc_, InsRate, InsValue_, StaffDiscRate, StaffDiscAmt_, PromoCode_, PromoRate_, PromoDiscAmt_, TaxCode_, TaxRate_);
                            dtCSaleCNNum = InsertData(CSalesNo, MobRefNum_, ConsignorCode, Convert.ToInt32(PostCodeId_), DestCode_, ConsigneeCode, ShipmentType_, Convert.ToInt32(Pieces_), Convert.ToDecimal(Weight_), Convert.ToDecimal(VLength_), Convert.ToDecimal(VBreadth), Convert.ToDecimal(VHeight_), Convert.ToDecimal(VolWeight_), Convert.ToDecimal(HighestWeight_), Convert.ToDecimal(Price_), Convert.ToDecimal(OtherCharges_), Convert.ToDecimal(TotalAm_), Convert.ToDecimal(InsShipmentValue_), InsShipmentDesc_, 0, Convert.ToDecimal(InsValue_), 0, Convert.ToDecimal(StaffDiscAmt_), PromoCode_, Convert.ToDecimal(PromoRate_), Convert.ToDecimal(PromoDiscAmt_), TaxCode_, Convert.ToDecimal(TaxRate_), Convert.ToDecimal(RASurcharge_));
                            //for (int n = 0; n < dtCSaleCNNum.Rows.Count; n++)
                            //{
                            //    NewCN = dtCSaleCNNum.Rows[n]["CNNum"].ToString();
                            //    Inserted = Convert.ToInt32(dtCSaleCNNum.Rows[n]["InsertedId"]);
                            //    break;
                            //}

                            //if (NewCN.Trim() != "")
                            //{
                            //    tempListRefDetail.Add(tempAWBNo + "|" + RShipperName + "|" + RShipperAddress + "|" + RShipperTelNo + "|" + RSendTo + "|" + RCompany + "|" + RReceiverAddress + "|" + RZipcode + "|" + RPhone)
                            //    for (int j = 0; j < arrRef.Length; j++)
                            //    {
                            //        string[] RefDet = arrRef[j].ToString().Split('|');
                            //        string TempRefNumber = RefDet[0],
                            //            TempRShipperName = RefDet[1],
                            //            TempRShipperAddress1 = RefDet[2],
                            //            TempRShipperAddress2 = RefDet[3],
                            //            TempRShipperAddress3 = RefDet[4],
                            //            TempRShipperZipcode = RefDet[5],
                            //            TempRShipperTelNo = RefDet[6],
                            //            TempRSendTo = RefDet[7],
                            //            TempRCompany = RefDet[8],
                            //            TempRReceiverAddress1 = RefDet[9],
                            //            TempRReceiverAddress2 = RefDet[10],
                            //            TempRReceiverAddress3 = RefDet[11],
                            //            TempRZipcode = RefDet[12],
                            //            TempRPhone = RefDet[13];

                            //        if (TempAWBNumber == TempRefNumber)
                            //        {
                            //            ExecuteQuery($"INSERT INTO tblConsignor (ConsignorCode, Name, Add1, Add2, City, State, Country, PostalCode, Phone, Fax, Mobile, Email, GstNum, CUserID, CDate, MUserID, MDate, Status, IsExported, ExportedDate, AWBNum) VALUES ('', '{ TempRShipperName }', '{TempRShipperAddress1}', '{TempRShipperAddress2}', '{TempRShipperAddress3}', '', '', '{TempRShipperZipcode}', '{ TempRShipperTelNo }', '', '', '', '', '{ LoginUserId }', GetDate(), null, null, 1, 0, '', '{ NewCN }')");

                            //            ExecuteQuery($"INSERT INTO tblConsignee (ConsigneeCode, Name, Add1, Add2, City, State, Country, PostalCode, Phone, Fax, Mobile, Email, GSTNum, CUserID, CDate, MUserID, MDate, Status, IsExported, ExportedDate, AWBNum, CompanyName) VALUES ('', '{ TempRSendTo }', '{TempRReceiverAddress1}', '{ TempRReceiverAddress2 }', '{ TempRReceiverAddress3 }', '', '', '{ TempRZipcode }', '{TempRPhone}', '', '', '', '', '{LoginUserId}', GetDate(), null, null, 1, 0, '', '{ NewCN }', '{ TempRCompany }')");
                            //        }
                            //    }
                            //}
                        }

                        if (IsStationary)
                        {
                            string[] arrStationary = cashSalesVM.listStationaryDetail.ToArray();

                            for (int i = 0; i < arrStationary.Length; i++)
                            {
                                TaxAmt = 0;
                                string[] splitStationary = arrStationary[i].ToString().Split('|');
                                string itemid = splitStationary[0],
                                    itemcode = splitStationary[1],
                                    itemname = splitStationary[2],
                                    quantity = splitStationary[3],
                                    UOM = splitStationary[4],
                                    rate = splitStationary[5],
                                    total = splitStationary[6];

                                TaxAmt = Convert.ToDecimal(total) * StandardTaxRate / 100;
                                TotTaxAmt += TaxAmt;

                                ExecuteQuery($"INSERT INTO tblCSaleCNNum (CSalesNo, Num, CNNum, MobRefNum, OriginCode, ConsignorCode, PostCodeId, DestCode, ConsigneeCode, ShipmentType, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, HighestWeight, Price, OtherCharges, TotalAmt, InsShipmentValue, InsShipmentDesc, InsRate, InsValue, StaffDiscRate, StaffDiscAmt, PromoCode, PromoRate, PromoDiscAmt, TaxCode, TaxRate, CUserID, CDate, Status, IsExported, ExportedDate) VALUES ('{CSalesNo}', 0, '{itemcode}', '', '{MyCompanyCode.Trim()}', '', 0, '', '', 'STATIONERY', {quantity}, 0, 0, 0, 0, 0, 0, {rate}, 0, {total}, 0, '', 0, 0, 0, 0, '', 0, 0, '{StandardTaxCode}', {StandardTaxRate}, {LoginUserId}, GETDATE(), 1, 0, NULL)");

                                int sales = 0, closing = 0;
                                dr = GetDataReader($"SELECT * FROM tblStock where ItemId = {itemid}");
                                if (dr.Read())
                                {
                                    sales = Convert.ToInt32(dr["Sales"]);
                                    closing = Convert.ToInt32(dr["Closing"]);
                                    dr.Close();
                                }
                                ExecuteQuery($"UPDATE tblStock SET Sales = ({sales} + {quantity}), Closing = ({closing} - {quantity}) WHERE ItemId = {itemid}");
                            }
                        }

                        if (CSalesNo.Trim() != "" && TotTaxAmt > 0)
                        {
                            dr = GetDataReader($"SELECT ISNULL(ShipmentTotal, 0) AS ShipmentTotal, ISNULL(StationaryTotal, 0) AS StationaryTotal, ISNULL(StaffDisAmt, 0) AS StaffDisAmt, ISNULL(DiscAmount, 0) AS DiscAmount FROM tblCashSales where CSalesNo = '{CSalesNo}'");
                            if (dr.Read())
                            {
                                decimal CSalesTotal = (Convert.ToDecimal(dr["ShipmentTotal"]) + Convert.ToDecimal(dr["StationaryTotal"]) + TotTaxAmt) - (Convert.ToDecimal(dr["StaffDisAmt"]) + Convert.ToDecimal(dr["DiscAmount"]));
                                decimal RndAmt = RoundingValue(CSalesTotal);
                                CSalesTotal += RndAmt;
                                dr.Close();
                                //ExecuteScalarQuery($"UPDATE tblCashSales SET ShipmentGSTValue = {TotTaxAmt}, CSalesTotal = {CSalesTotal}, RoundingAmt = {RndAmt} WHERE CSalesNo = '{CSalesNo}'");
                            }
                        }                        
                        cashSalesVM.isDone = true;
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("PaymentVM.Pay() : " + ex.ToString());
                MessageBox.Show("Error while saving transaction. Please redo the payment.");
                DeleteIncompleteTransaction();
            }

            try
            {
                //Print Consignment Note or AWB Sticker
                DataTable dtPrint = new DataTable();
                dtPrint = GetDataTable($"SELECT CNNum, MobRefNum FROM tblCSaleCNNum WHERE CSalesNo = '{ CSalesNo }' AND ShipmentType != 'STATIONERY'");
                if (dtPrint.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPrint.Rows.Count; i++)
                    {
                        string AWBNumber = dtPrint.Rows[i]["CNNum"].ToString();
                        bool isMobileRef = dtPrint.Rows[i]["MobRefNum"].ToString() == "" ? false : true;

                        if (isStickerPrinter && isMobileRef)
                        {
                            AWBPrintSticker(AWBNumber);
                        }
                        else
                        {
                            AWBPrint(AWBNumber, "Preview");
                        }
                    }
                }

                //Print Payment Receipt
                dtPrint = new DataTable();
                dtPrint = GetDataTable($"SELECT CNNum, MobRefNum FROM tblCSaleCNNum WHERE CSalesNo = '{CSalesNo}'");
                if (dtPrint.Rows.Count > 0)
                {
                    MessageBoxResult MsgRes = System.Windows.MessageBox.Show("You want to print Cash Receipt?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        TaxInvoices(CSalesNo);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Printing Error");
                error_log.errorlog("Printing Error : " + ex.ToString());
            }
        }

        private void DeleteIncompleteTransaction()
        {
            if(CSalesNo != "")
            {
                ExecuteQuery($"DELETE FROM tblCashSales WHERE CSalesNo = '{CSalesNo}'");
                ExecuteQuery($"DELETE FROM tblCSaleCNNum WHERE CSalesNo = '{CSalesNo}'");
                CSaleNo = "";
            }
        }

        private decimal RoundingValue(decimal Val)
        {
            decimal res = 0, roundby = (decimal)0.05;

            try
            {
                decimal temp = GetRoundingVal(Val % roundby, 2);

                if (temp == (decimal).01 || temp == (decimal).02)
                    res = temp * -1;
                else if (temp == (decimal).03 || temp == (decimal).04)
                    res = roundby - temp;
            }
            catch { }

            return res;
        }

        private string NextBarCode()
        {
            SqlDataReader dr;
            string LastCN = "", NextCN = "";
            long RunningNo = 0;
            int CheckDigit = 0;

            try
            {
                dr = GetDataReader("SELECT CNNum FROM tblLastCNNum");
                if (dr.HasRows)
                {
                    dr = GetDataReader($"SELECT CNNum FROM tblLastCNNum WHERE CNNum IS NOT NULL AND CNNum != ''");
                    if (dr.Read())
                    {
                        LastCN = dr["CNNum"].ToString();
                    }
                    dr.Close();
                }
                else
                {
                    dr = GetDataReader($"SELECT * FROM tblAWBAllocation");
                    if (dr.HasRows)
                    {
                        dr = GetDataReader($"SELECT top 1 FromAWB FROM tblAWBAllocation WHERE Status = 'ACTIVE' AND IsDelete = 0 ORDER BY FromAWB");
                        if (dr.Read())
                        {
                            LastCN = dr["FromAWB"].ToString();
                        }
                        dr.Close();
                    }
                }

            GenerateNextAWB:
                if (LastCN.Trim() != "")
                {
                    if (LastCN.Length == 12)
                    {
                        RunningNo = Convert.ToInt64(LastCN.Substring(0, LastCN.Length - 1)) + 1;
                        CheckDigit = Convert.ToInt16(RunningNo % 7);
                        NextCN = string.Concat(RunningNo, CheckDigit);
                    }
                    else
                    {
                        RunningNo = Convert.ToInt64(LastCN) + 1;
                        NextCN = RunningNo.ToString();
                    }
                }

                if (NextCN.Trim() != "")
                {
                    using (dr = GetDataReader($"SELECT CNNum FROM tblCSaleCNNum WHERE CNNum = '{NextCN}'"))
                    {
                        if (dr.HasRows)
                        {
                            LastCN = NextCN;
                            goto GenerateNextAWB;
                        }
                    }

                    using (dr = GetDataReader($"SELECT AllocateId FROM tblAWBAllocation WHERE ('{NextCN}' BETWEEN FromAWBRunNum AND ToAWBRunNum) AND Status = 'ACTIVE'"))
                    {
                        if (!dr.HasRows)
                        {
                            using (SqlDataReader dr1 = GetDataReader($"SELECT AllocateId FROM tblAWBAllocation WHERE Status = 'NEXT'"))
                            {
                                if (dr1.HasRows)
                                {
                                    ExecuteQuery($"UPDATE tblAWBAllocation SET Status = 'USED', ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE Status = 'ACTIVE'");
                                    ExecuteQuery($"UPDATE tblAWBAllocation SET Status = 'ACTIVE', ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE AllocateId = (SELECT MIN(AllocateId) FROM tblAWBAllocation WHERE Status = 'NEXT')");

                                    using (SqlDataReader dr2 = GetDataReader($"SELECT FromAWB FROM tblAWBAllocation WHERE Status = 'ACTIVE'"))
                                    {
                                        if (dr2.Read())
                                        {
                                            LastCN = dr2["FromAWB"].ToString();
                                        }
                                        dr2.Close();
                                    }
                                }
                            }
                        }
                    }

                    if (NextCN.Length == 12)
                    {
                        RunningNo = Convert.ToInt64(NextCN.Substring(0, NextCN.Length - 1));
                        CheckDigit = Convert.ToInt16(RunningNo - ((RunningNo / 7) * 7));

                        if (NextCN != string.Concat(RunningNo, CheckDigit))
                        {
                            error_log.errorlog("Invalid 12 digit barcode " + NextCN);
                            NextCN = "";
                        }
                    }
                }
            }
            catch (Exception ex) { error_log.errorlog("Check BarCode function:" + ex.ToString()); return ""; }

            return NextCN;
        }


        //private string CheckNewCSalesNo(string CODE)
        //{
        //    string result = CODE;
        //    string sub = "";
        //    if (!string.IsNullOrEmpty(MyCompanySubName))
        //    {
        //        if (MyCompanySubName.Trim().Length > 5)
        //        {
        //            sub = MyCompanySubName.Trim().Substring(0, 5);
        //        }
        //        else
        //        {
        //            if (MyCompanySubName.Trim().Length == 5)
        //            {
        //                sub = MyCompanySubName.Trim();
        //            }
        //            else if (MyCompanySubName.Trim().Length == 4)
        //            {
        //                sub = MyCompanySubName.Trim() + "0";
        //            }
        //            else if (MyCompanySubName.Trim().Length == 3)
        //            {
        //                sub = MyCompanySubName.Trim() + "00";
        //            }
        //            else if (MyCompanySubName.Trim().Length == 2)
        //            {
        //                sub = MyCompanyCode.Trim() + MyCompanySubName.Trim();
        //            }
        //            else if (MyCompanySubName.Trim().Length == 1)
        //            {
        //                sub = MyCompanyCode.Trim() + MyCompanySubName.Trim() + "0";
        //            }
        //            else
        //            {
        //                sub = MyCompanyCode.Trim() + "00";
        //            }
        //        }
        //    }
        //    else { sub = MyCompanyCode.Trim() + "00"; }
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(result))
        //        {
        //            string StrDate = DateTime.Now.ToString("ddMMyyyy");
        //        Loop:
        //            string CheckExistence = "";

        //            using (SqlCommand sqlcmd = new SqlCommand($"Select top 1 CSalesID, CSalesNo from tblCashSales where CSalesNo = '{result.Trim()}' ORDER BY CDate desc", GetConnection()))
        //            {
        //                using (SqlDataReader dr = sqlcmd.ExecuteReader())
        //                {
        //                    if (dr.HasRows)
        //                    {
        //                        if (dr.Read())
        //                        {
        //                            CheckExistence = dr["CSalesNo"].ToString();
        //                        }

        //                    }
        //                    dr.Close();
        //                }
        //            }

        //            GC.Collect();

        //            if (!string.IsNullOrEmpty(CheckExistence))
        //            {
        //                int l = CheckExistence.Length;
        //                string intcode = l == 16 ? CheckExistence.Substring(10) : l == 17 ? CheckExistence.Substring(11) : CheckExistence.Substring(8);
        //                int codeint = Convert.ToInt32(intcode != "" ? intcode : "0") + 1;
        //                result = "CS" + MyCompanyCode.Trim() + sub.Trim() + codeint.ToString("000000");
        //                CODE = result;
        //                goto Loop;
        //            }
        //            else
        //            {
        //                result = CODE;
        //            }
        //        }
        //        else
        //        {
        //            result = NewCSalesNo();
        //            CheckNewCSalesNo(result);
        //        }
        //        return result;
        //    }
        //    catch (Exception ex) { error_log.errorlog("Checking Cash Sales No function:" + ex.ToString()); return result; }
        //}


        private string NewCSalesNo()
        {
            string result = "";
            try
            {
                string sub = MyCompanySubName.Trim();
                if (!string.IsNullOrEmpty(MyCompanySubName.Trim()))
                {
                    if (MyCompanySubName.Trim().Length > 5)
                    {
                        sub = MyCompanySubName.Trim().Substring(0, 5);
                    }
                    else
                    {
                        if (MyCompanySubName.Trim().Length == 5)
                        {
                            sub = MyCompanySubName.Trim();
                        }
                        else if (MyCompanySubName.Trim().Length == 4)
                        {
                            sub = MyCompanySubName.Trim() + "0";
                        }
                        else if (MyCompanySubName.Trim().Length == 3)
                        {
                            sub = MyCompanySubName.Trim() + "00";
                        }
                        else if (MyCompanySubName.Trim().Length == 2)
                        {
                            sub = MyCompanyCode.Trim() + MyCompanySubName.Trim();
                        }
                        else if (MyCompanySubName.Trim().Length == 1)
                        {
                            sub = MyCompanyCode.Trim() + MyCompanySubName.Trim() + "0";
                        }
                        else
                        {
                            sub = MyCompanyCode.Trim() + "00";
                        }
                    }
                }
                else
                {
                    sub = MyCompanyCode.Trim() + "00";
                }

                string MaxCode = "", StrDate = DateTime.Now.ToString("ddMMyyyy");
                using (SqlDataReader dr = GetDataReader("Select top 1 CSalesNo from tblCashSales ORDER BY CDate desc"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            MaxCode = dr["CSalesNo"].ToString();
                        }

                    }
                }

                if (MaxCode != null && MaxCode != "")
                {
                    //int l = MaxCode.Length;
                    //string intcode = l == 16 ? MaxCode.Substring(10) : l == 17 ? MaxCode.Substring(11) : MaxCode.Substring(8);

                    string intcode = MaxCode.Substring(MaxCode.Length - 8, 8);
                    int codeint = Convert.ToInt32(intcode != "" ? intcode : "0") + 1;
                    result = "CS" + MyCompanyCode.Trim() + sub.Trim() + codeint.ToString("000000");
                }
                else
                {
                    result = "CS" + MyCompanyCode.Trim() + sub.Trim() + 1.ToString("000000");
                }
                return result;
            }
            catch (Exception ex) { error_log.errorlog("NewCSalesNo generating function:" + ex.ToString()); return result; }
        }

        public void PaymentModeComboboxload()
        {
            try
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GDDesc FROM tblGeneralDet A" +
                        $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                        $" WHERE RTRIM(B.GenDesc) = 'PAYMENT MODE' and Status = 1 ORDER BY A.GDDesc");

                    if (dt.Rows.Count > 0)
                    {
                        PaymentModeList.Clear();
                        for (int i = 0; i < dt.Rows.Count; ++i)
                        {
                            string abc = dt.Rows[i]["GDDesc"].ToString();
                            PaymentModeList.Add(new PaymentModeModel { PaymentModeId = abc, PaymentModeName = abc });
                        }
                        PaymentMode = "CASH";
                        //SelectedPaymentModeIndex = dt.Rows.Count - 1;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("PaymentModee Combobox load function:" + Ex.ToString()); }
        }

    }

}
