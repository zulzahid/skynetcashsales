﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class CheckinAndPODVM : AMGenFunction
    {
        private ItemInfo ObjItemInfo;
        private FrmCheckinAndPOD MyWind;
        public CheckinAndPODVM(FrmCheckinAndPOD wind)
        {
            MyWind = wind;
            ObjItemInfo = new ItemInfo();
            CheckinList = new ObservableCollection<ItemInfo>();
            PODList = new ObservableCollection<ItemInfo>();
            ReportList = new ObservableCollection<ItemInfo>();
            ResetData();
        }

        public string CheckinAWBNumber { get { return GetValue(() => CheckinAWBNumber); } set { SetValue(() => CheckinAWBNumber, value); OnPropertyChanged("CheckinAWBNumber"); } }
        public string PodAWBNumber { get { return GetValue(() => PodAWBNumber); } set { SetValue(() => PodAWBNumber, value); OnPropertyChanged("PodAWBNumber"); } }
        public bool Delivered { get { return GetValue(() => Delivered); } set { SetValue(() => Delivered, value); OnPropertyChanged("Delivered"); if (Delivered) Status = "Delivered"; } }
        public bool ReturnedToShipper { get { return GetValue(() => ReturnedToShipper); } set { SetValue(() => ReturnedToShipper, value); OnPropertyChanged("ReturnedToShipper"); if (ReturnedToShipper) Status = "Returned to shipper"; } }
        public string Status { get { return GetValue(() => Status); } set { SetValue(() => Status, value); OnPropertyChanged("Status"); } }
        public string DeliveredTo { get { return GetValue(() => DeliveredTo); } set { SetValue(() => DeliveredTo, value); OnPropertyChanged("DeliveredTo"); } }
        public string IDProof { get { return GetValue(() => IDProof); } set { SetValue(() => IDProof, value); OnPropertyChanged("IDProof"); } }

        public DateTime FromDate { get { return GetValue(() => FromDate); } set { SetValue(() => FromDate, value); OnPropertyChanged("FromDate"); } }
        public DateTime ToDate { get { return GetValue(() => ToDate); } set { SetValue(() => ToDate, value); OnPropertyChanged("ToDate"); } }
        public bool AllCheckinShipments { get { return GetValue(() => AllCheckinShipments); } set { SetValue(() => AllCheckinShipments, value); OnPropertyChanged("AllCheckinShipments"); ReportListColumnVisibility(); } }
        public bool CheckinButNoPod { get { return GetValue(() => CheckinButNoPod); } set { SetValue(() => CheckinButNoPod, value); OnPropertyChanged("CheckinButNoPod"); ReportListColumnVisibility(); } }
        public bool PODShipments { get { return GetValue(() => PODShipments); } set { SetValue(() => PODShipments, value); OnPropertyChanged("PODShipments"); ReportListColumnVisibility(); } }

        private ObservableCollection<ItemInfo> _CheckinList = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> CheckinList { get { return _CheckinList; } set { _CheckinList = value; OnPropertyChanged("CheckinList"); } }

        private ObservableCollection<ItemInfo> _PODList = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> PODList { get { return _PODList; } set { _PODList = value; OnPropertyChanged("PODList"); } }
        
        private ObservableCollection<ItemInfo> _ReportList = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> ReportList { get { return _ReportList; } set { _ReportList = value; OnPropertyChanged("ReportList"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "Checkin AWBNumber": CheckinItem(); break;
                    case "Update POD": PODItem(); break;
                    case "Clear POD data": ResetData(); MyWind.txtPodAWBNumber.Focus(); break;
                    case "Generate Report": GenerateReport(Obj); break;
                    case "Print Report": GenerateReport(Obj); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog($"CheckinAndPODVM.ExecuteCommandGen({Obj.ToString()}) : " + ex.ToString());
            }
        }

        private void ReportListColumnVisibility()
        {
            ReportList.Clear();
            // Columns.Width = 902
            if (CheckinButNoPod)
            {
                MyWind.dgReportList.Columns[0].Width = 52;  // #
                MyWind.dgReportList.Columns[1].Width = 150; // AWB Number
                MyWind.dgReportList.Columns[2].Width = 150; // Checkin Date
                MyWind.dgReportList.Columns[3].Width = 350; // Checkin By
                MyWind.dgReportList.Columns[6].Width = 200; // Status

                MyWind.dgReportList.Columns[4].Visibility = Visibility.Hidden;
                MyWind.dgReportList.Columns[5].Visibility = Visibility.Hidden;
                MyWind.dgReportList.Columns[7].Visibility = Visibility.Hidden;
            }
            else
            {
                MyWind.dgReportList.Columns[0].Width = 50;  // #
                MyWind.dgReportList.Columns[1].Width = 110; // AWB Number
                MyWind.dgReportList.Columns[2].Width = 150; // Checkin Date
                MyWind.dgReportList.Columns[3].Width = 110; // Checkin By
                MyWind.dgReportList.Columns[4].Width = 150; // POD Date
                MyWind.dgReportList.Columns[5].Width = 110; // POD By
                MyWind.dgReportList.Columns[6].Width = 80; // Status
                MyWind.dgReportList.Columns[7].Width = 142; // Recipient

                MyWind.dgReportList.Columns[4].Visibility = Visibility.Visible;
                MyWind.dgReportList.Columns[5].Visibility = Visibility.Visible;
                MyWind.dgReportList.Columns[7].Visibility = Visibility.Visible;
            }
        }

        private void CheckinItem()
        {
            Status = "CHECKIN";
            string[] ValidatedProperties = { "CheckinAWBNumber" };
            if (IsValid(ValidatedProperties))
            {
                try
                {
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                    {
                        List<TrackingAwbInfo> track = new List<TrackingAwbInfo>();
                        track.Add(new TrackingAwbInfo
                        {
                            MAWBNumber = CheckinAWBNumber,
                            AwbNumber = CheckinAWBNumber,
                            AWBDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            ShipmentType = "",
                            DestStn = MyCompanyCode.Trim(),
                            CreatedBy = LoginUserName + " @" + (MyCompanySubName.Trim() != "" ? (MyCompanyCode.Trim() + "-" + MyCompanySubName.Trim()) : MyCompanyCode.Trim()),
                            EventType = Status,
                            Reference = "CSS Chckin by WIC",
                            CreatedByStn = MyCompanyCode
                        });

                        TrackingAPIInfo apiref = new TrackingAPIInfo();
                        apiref.Access_Token = "fc7017130c796ce23f43a0adc89f382d";
                        apiref.FunctionStr = "createEvents";
                        apiref.AWBTracking = track;

                        //var json = Newtonsoft.Json.JsonConvert.SerializeObject(apiref);

                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.skynet.com.my");
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            var response = client.PostAsJsonAsync("api/sn/createTrackingstatus", apiref).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var res = response.Content.ReadAsStringAsync().Result;
                                if (res.Contains("status has been created successfully"))
                                {
                                    ExecuteQuery($"INSERT INTO tblPODShipment(AWBNumber, CheckinDate, CheckinBy, PODDate, Status, DeliveredTo, IDProof, IDProofType, PODBy, StnCode, TransmittedOn)" +
                                        $" VALUES('{CheckinAWBNumber}', GETDATE(), '{LoginUserName}', NULL, '{Status}', '', '', '', '', '{(MyCompanySubName.Trim() != "" ? (MyCompanyCode.Trim() + "-" + MyCompanySubName.Trim()) : MyCompanyCode.Trim())}', NULL)");
                                }
                                else { MessageBox.Show(res.ToString(), PodAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information); }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please check your Internet connection", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (Exception ex)
                {
                    error_log.errorlog("CheckinAndPODVM.CheckinItem() : " + ex.ToString());
                }

                ResetData();
                MyWind.txtCheckinAWBNumber.Focus();
            }
            else
            {
                CheckinAWBNumber = "";
                MyWind.txtCheckinAWBNumber.Focus();
            }
        }

        private void PODItem()
        {
            Status = ReturnedToShipper ? "RTS" : "POD";
            string[] ValidatedProperties = { "PodAWBNumber", "DeliveredTo", "IDProof" };
            if (IsValid(ValidatedProperties))
            {
                try
                {
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                    {
                        List<PODAwbInfo> pod = new List<PODAwbInfo>();
                        pod.Add(new PODAwbInfo
                        {
                            AwbNumber = PodAWBNumber,
                            DeliveredDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            CreatedByStn = MyCompanyCode.Trim(),
                            CreatedBy = LoginUserName + " @" + (MyCompanySubName.Trim() != "" ? (MyCompanyCode.Trim() + "-" + MyCompanySubName.Trim()) : MyCompanyCode.Trim()),
                            CreatedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            EventType = Status,
                            Signature = $"NAME: {DeliveredTo}  I/D: {IDProof}"
                        });

                        PODAPIInfo apiref = new PODAPIInfo();
                        apiref.Access_Token = "fc7017130c796ce23f43a0adc89f382d";
                        apiref.platform = "CSS";
                        apiref.FunctionStr = "createPOD";
                        apiref.AWBTracking = pod;

                        //var json = Newtonsoft.Json.JsonConvert.SerializeObject(apiref);

                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.skynet.com.my");
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            var response = client.PostAsJsonAsync("api/sn/createDelivery", apiref).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var res = response.Content.ReadAsStringAsync().Result;
                                if (res.Contains("POD received"))
                                {
                                    ExecuteQuery($"UPDATE tblPODShipment SET PODDate = GETDATE(), Status = '{Status}', DeliveredTo = '{DeliveredTo}', IDProof = '{IDProof}', PODBy = '{LoginUserName}', TransmittedOn = GETDATE()" +
                                        $" WHERE AWBNumber = '{PodAWBNumber}' AND PODDate IS NULL");
                                }
                                else { MessageBox.Show(res.ToString(), PodAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information); }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please check your Internet connection", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (Exception ex)
                {
                    error_log.errorlog("CheckinAndPODVM.PODItem() : " + ex.ToString());
                }
                ResetData();
                MyWind.txtPodAWBNumber.Focus();
            }
        }

        private bool IsValid(string[] strProperties)
        {
            foreach (string property in strProperties)
            {
                if (GetValidationError(property) != null) return false;
            }
            return true;
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)
            {
                case "CheckinAWBNumber":
                    {
                        if(CheckinAWBNumber.Trim() != "")
                        {
                            SqlDataReader dr = GetDataReader($"SELECT * FROM tblPODShipment WHERE AWBNumber = '{CheckinAWBNumber}'");
                            if (dr.Read())
                            {
                                if (dr["PODDate"] != DBNull.Value)
                                    error = $"This shipment already delivered @{Convert.ToDateTime(dr["PODDate"]).ToString("dd-MMM-yyyy")}";
                                else
                                    error = $"This shipment already checked in @{Convert.ToDateTime(dr["CheckinDate"]).ToString("dd-MMM-yyyy")}";
                            }
                        }
                        else {error = $"Invalid AWB Number to Checkin"; MyWind.txtCheckinAWBNumber.Focus(); }
                    }
                    break;
                case "PodAWBNumber":
                    {
                        if (PodAWBNumber.Trim() != "")
                        {
                            SqlDataReader dr = GetDataReader($"SELECT * FROM tblPODShipment WHERE AWBNumber = '{PodAWBNumber}'");
                            if (dr.Read())
                            {
                                if (dr["PODDate"] != DBNull.Value) error = $"This shipment already delivered @{Convert.ToDateTime(dr["PODDate"]).ToString("dd-MMM-yyyy")}";
                                MyWind.txtPodAWBNumber.Focus();
                            }
                            else
                            {
                                error = $"This shipment is not in checkin list";
                                PodAWBNumber = "";
                                MyWind.txtPodAWBNumber.Focus();
                            }
                            dr.Close();
                        }
                        else { error = $"Invalid AWB Number to POD"; MyWind.txtPodAWBNumber.Focus(); }
                    }
                    break;
                case "DeliveredTo": if (string.IsNullOrEmpty(DeliveredTo)) { error = $"Please enter Recipient Name"; MyWind.txtDeliveredTo.Focus(); } break;
                case "IDProof": if (string.IsNullOrEmpty(IDProof)) { error = $"Please enter I/C or Passport No."; MyWind.txtIDProof.Focus(); } break;
            }
            
            if (error != null) MessageBox.Show(error, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            return error;
        }

        private void GenerateReport(object Obj)
        {
            DataTable dt = new DataTable();
            string rptFileName = "RPTCheckinList.rdlc";
            ReportList.Clear();

            if (AllCheckinShipments)
            {
                dt = GetDataTable($"SELECT 'All Checkin Shipments' as Header, '{FromDate.ToString("yyyy-MM-dd")}' as FromDate, '{ToDate.ToString("yyyy-MM-dd")}' As ToDate, AWBNumber, CheckinDate, CheckinBy, PODDate, Status, DeliveredTo, IDProof, PODBy, StnCode, '{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblPODShipment" +
                    $" WHERE (CheckinDate BETWEEN '{FromDate.Date.ToString("MM/dd/yyyy HH:mm:ss")}' AND '{ToDate.Date.AddHours(23.9999).ToString("MM/dd/yyyy HH:mm:ss")}') ORDER BY CheckinDate, AWBNumber");

            }
            else if(CheckinButNoPod)
            {
                dt = GetDataTable($"SELECT 'Checkin but not done POD' as Header, '{FromDate.ToString("yyyy-MM-dd")}' as FromDate, '{ToDate.ToString("yyyy-MM-dd")}' As ToDate, AWBNumber, CheckinDate, CheckinBy, PODDate, Status, DeliveredTo, IDProof, PODBy, StnCode, '{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblPODShipment" +
                    $" WHERE (CheckinDate BETWEEN '{FromDate.Date.ToString("MM/dd/yyyy HH:mm:ss")}' AND '{ToDate.Date.AddHours(23.9999).ToString("MM/dd/yyyy HH:mm:ss")}') AND PODDate IS NULL ORDER BY CheckinDate, AWBNumber");
            }
            else if(PODShipments)
            {
                dt = GetDataTable($"SELECT 'All POD Shipments' as Header, '{FromDate.ToString("yyyy-MM-dd")}' as FromDate, '{ToDate.ToString("yyyy-MM-dd")}' As ToDate, AWBNumber, CheckinDate, CheckinBy, PODDate, Status, DeliveredTo, IDProof, PODBy, StnCode, '{(MyCompanySubName.Trim() != "" ? MyCompanySubName.Trim() : MyCompanyCode.Trim())}' as WICCode FROM tblPODShipment" +
                    $" WHERE PODDate IS NOT NULL AND (PODDate BETWEEN '{FromDate.Date.ToString("MM/dd/yyyy HH:mm:ss")}' AND '{ToDate.Date.AddHours(23.9999).ToString("MM/dd/yyyy HH:mm:ss")}') ORDER BY CheckinDate, AWBNumber");
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ReportList.Add(new ItemInfo
                    {
                        Item = ReportList.Count + 1,
                        AWBNumber = dt.Rows[i]["AWBNumber"].ToString(),
                        CheckinDate = Convert.ToDateTime(dt.Rows[i]["CheckinDate"]).ToString("dd-MMM-yyyy hh:mm:ss tt"),
                        CheckinBy = dt.Rows[i]["CheckinBy"].ToString(),
                        PODDate = dt.Rows[i]["PODDate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["PODDate"]).ToString("dd-MMM-yyyy hh:mm:ss tt") : "",
                        PODBy = dt.Rows[i]["PODDate"] != DBNull.Value ? dt.Rows[i]["PODBy"].ToString() : "",
                        Status = dt.Rows[i]["Status"].ToString(),
                        DeliveredTo = dt.Rows[i]["PODDate"] != DBNull.Value ? $"{(dt.Rows[i]["DeliveredTo"].ToString().Trim() != "" ? $"NAME : {dt.Rows[i]["DeliveredTo"].ToString().Trim()}" : "")}  {(dt.Rows[i]["IDProof"].ToString().Trim() != "" ? $"I/D: {dt.Rows[i]["IDProof"].ToString().Trim()}" : "")}" : "",
                    });
                }

                if (Obj.ToString() == "Print Report")
                {
                    DataTable dtCompany = new DataTable();
                    dtCompany = GetDataTable($"SELECT  a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,'') , Address3 = ISNULL(a.Address3,'') , City = ISNULL(a.City,'') , State = ISNULL(a.State,'') , Country =ISNULL(a.Country,'') , PostalCode = ISNULL(a.ZipCode,'')  , PhoneNo =  ISNULL(a.PhoneNum,'') , FaxNo =ISNULL(a.Fax,'')  , EMail = ISNULL(a.EMail,'') , a.Status  FROM tblStationProfile a" +
                        $" WHERE RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");

                    View.Reports.FrmReportViewer RptViewerGen = new View.Reports.FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    Reports.ReportViewerVM VM = new Reports.ReportViewerVM("CompanyMas", dtCompany, "PODShipment", dt, "", dt, "", dt, rptFileName, "", RptViewerGen.RptViewer, false, true);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
            }
        }

        private void ResetData()
        {
            try
            {
                CheckinAWBNumber = ""; CheckinList.Clear();

                PodAWBNumber = ""; Delivered = true; Status = "Delivered"; DeliveredTo = ""; IDProof = "";
                PODList.Clear();

                SqlDataReader dr = GetDataReader($"SELECT * FROM tblPODShipment WHERE PODDate IS NULL OR FORMAT(PODDate, 'yyyy-MMM-dd') = FORMAT(GETDATE(), 'yyyy-MMM-dd')");
                while (dr.Read())
                {
                    if (dr["PODDate"] == DBNull.Value)
                        CheckinList.Add(new ItemInfo { Item = CheckinList.Count + 1, AWBNumber = dr["AWBNumber"].ToString(), CheckinDate = Convert.ToDateTime(dr["CheckinDate"]).ToString("dd-MMM-yyyy hh:mm:ss tt"), Status = dr["Status"].ToString() });
                    else
                        PODList.Add(new ItemInfo
                        {
                            Item = PODList.Count + 1,
                            AWBNumber = dr["AWBNumber"].ToString(),
                            CheckinDate = Convert.ToDateTime(dr["CheckinDate"]).ToString("dd-MMM-yyyy hh:mm:ss tt"),
                            PODDate = Convert.ToDateTime(dr["PODDate"]).ToString("hh:mm:ss tt"),
                            Status = dr["Status"].ToString(),
                            DeliveredTo = $"{(dr["DeliveredTo"].ToString().Trim() != "" ? $"NAME : {dr["DeliveredTo"].ToString().Trim()}" : "")}  {(dr["IDProof"].ToString().Trim() != "" ? $"I/D: {dr["IDProof"].ToString().Trim()}" : "")}",
                            IDProof = dr["IDProof"].ToString()
                        });
                }
                dr.Close();

                FromDate = ToDate = DateTime.Now.Date;
                AllCheckinShipments = true;
                MyWind.txtCheckinAWBNumber.Focus();
            }
            catch (Exception ex)
            {
                error_log.errorlog("CheckinAndPODVM.ResetData() : " + ex.ToString());
            }
        }

        public class ItemInfo : AMGenFunction
        {
            public int Item { get; set; }
            public string AWBNumber { get; set; }
            public string CheckinDate { get; set; }
            public string CheckinBy { get; set; }
            public string PODDate { get; set; }            
            public string PODBy { get; set; }
            public string Status { get; set; }
            public string DeliveredTo { get; set; }
            public string IDProof { get; set; }
        }

        public class TrackingAPIInfo
        {
            public string Access_Token { get; set; }
            public string FunctionStr { get; set; }
            public List<TrackingAwbInfo> AWBTracking { get; set; }
        }

        public class TrackingAwbInfo
        {
            public string MAWBNumber { get; set; }
            public string AwbNumber { get; set; }
            public string AWBDate { get; set; }
            public string ShipmentType { get; set; }
            public string DestStn { get; set; }
            public string CreatedBy { get; set; }
            public string EventType { get; set; }
            public string Reference { get; set; }
            public string CreatedByStn { get; set; }
        }

        public class PODAPIInfo
        {
            public string Access_Token { get; set; }
            public string platform { get; set; }
            public string FunctionStr { get; set; }
            public List<PODAwbInfo> AWBTracking { get; set; }
        }

        public class PODAwbInfo
        {
            public string AwbNumber { get; set; }
            public string DeliveredDate { get; set; }
            public string CreatedByStn { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedDate { get; set; }
            public string EventType { get; set; }
            public string Signature { get; set; }
        }

    }
}
