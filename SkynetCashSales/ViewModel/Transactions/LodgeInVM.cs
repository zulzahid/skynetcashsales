﻿using Newtonsoft.Json;
using SkynetCashSales.General;
using SkynetCashSales.View.Reports;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class LodgeInVM : CNPrintVM
    {
        private static string errormsg = "";
        FrmLodgeIn Wndow;
        public LodgeInVM(FrmLodgeIn frm)
        {
            Wndow = frm;
            VWVisibility = false;
            ResetData();
        }

        public LodgeInVM(FrmLodgeIn frm, string awbnumber, decimal weight, string shipmenttype, int noofpcs)
        {
            Wndow = frm;
            ResetData();
            EnterAWBNumber = awbnumber;
            EnterWeight = weight;
            EnterShipmentType = shipmenttype;
            EnterNoofPcs = noofpcs;
            DataValid();
        }

        private ObservableCollection<LodgeInOnlyModel> _LodgeInlistitems = new ObservableCollection<LodgeInOnlyModel>();
        public ObservableCollection<LodgeInOnlyModel> LodgeInListItems { get { return _LodgeInlistitems; } set { _LodgeInlistitems = value; OnPropertyChanged("LodgeInListItems"); } }
        private ObservableCollection<CSMRShipmentTypeModel> _shipmenttype = new ObservableCollection<CSMRShipmentTypeModel>();
        public ObservableCollection<CSMRShipmentTypeModel> ShipmentType { get { return _shipmenttype; } set { _shipmenttype = value; OnPropertyChanged("ShipmentType"); } }
        private int _selectedshipmentindex;
        public int SelectedShipmentIndex { get { return _selectedshipmentindex; } set { _selectedshipmentindex = value; OnPropertyChanged("SelectedShipmentIndex"); } }
        private LodgeInOnlyModel _selectedsitem = new LodgeInOnlyModel();
        public LodgeInOnlyModel SelectedSItem { get { return _selectedsitem; } set { _selectedsitem = value; OnPropertyChanged("SelectedSItem"); } }

        //..[Required(ErrorMessage = "AWBNumber is Required")]
        public string EnterAWBNumber { get { return GetValue(() => EnterAWBNumber); } set { SetValue(() => EnterAWBNumber, value); OnPropertyChanged("EnterAWBNumber"); UpdateVisibility(); } }
        public decimal EnterWeight { get { return GetValue(() => EnterWeight); } set { SetValue(() => EnterWeight, value); OnPropertyChanged("EnterWeight"); } }
        public decimal EnterLength { get { return GetValue(() => EnterLength); } set { SetValue(() => EnterLength, value); OnPropertyChanged("EnterLength"); VolumetricCal(); } }
        public decimal EnterWidth { get { return GetValue(() => EnterWidth); } set { SetValue(() => EnterWidth, value); OnPropertyChanged("EnterWidth"); VolumetricCal(); } }
        public decimal EnterHeight { get { return GetValue(() => EnterHeight); } set { SetValue(() => EnterHeight, value); OnPropertyChanged("EnterHeight"); VolumetricCal(); } }
        public decimal EnterVM { get { return GetValue(() => EnterVM); } set { SetValue(() => EnterVM, value); OnPropertyChanged("EnterVM"); } }
        public string EnterShipmentType { get { return GetValue(() => EnterShipmentType); } set { SetValue(() => EnterShipmentType, value); OnPropertyChanged("EnterShipmentType"); UpdateVisibility(); } }
        public int EnterNoofPcs { get { return GetValue(() => EnterNoofPcs); } set { SetValue(() => EnterNoofPcs, value); OnPropertyChanged("EnterNoofPcs"); } }
        
        public string RecFromMobile { get { return GetValue(() => RecFromMobile); } set { SetValue(() => RecFromMobile, value); OnPropertyChanged("RecFromMobile"); } }
        public string RecFromName { get { return GetValue(() => RecFromName); } set { SetValue(() => RecFromName, value); OnPropertyChanged("RecFromName"); } }
        public string UserName { get { return GetValue(() => UserName); } set { SetValue(() => UserName, value); OnPropertyChanged("UserName"); } }
        public string StrTotalNoofPcs { get { return GetValue(() => StrTotalNoofPcs); } set { SetValue(() => StrTotalNoofPcs, value); OnPropertyChanged("StrTotalNoofPcs"); } }

        //Item to Visibile = false
        public bool VWVisibility { get { return GetValue(() => VWVisibility); } set { SetValue(() => VWVisibility, value); OnPropertyChanged("VWVisibility"); } }

        public decimal EnterPrice { get { return GetValue(() => EnterPrice); } set { SetValue(() => EnterPrice, value); OnPropertyChanged("EnterPrice"); } }

        //Validation Starts 
        static readonly string[] ValidatedProperties = { "EnterAWBNumber", "EnterWeight", "EnterShipmentType", "EnterNoofPcs" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null) // there is an error
                        return false;
                }
                return true;
            }
        }
        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "EnterAWBNumber": if (string.IsNullOrEmpty(EnterAWBNumber)) error = "AWBNumber is required"; break;
                case "EnterWeight": if (EnterWeight <= 0) error = "Weight is required"; break;
                case "EnterShipmentType": if (string.IsNullOrEmpty(EnterShipmentType)) error = "ShipmentType is required"; break;
                case "EnterNoofPcs": if (EnterNoofPcs <= 0) error = "Number of Pcs is required"; break;
                
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }
        //Validation Ends        
        
        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "GetWeight": { WeightfromMachine(); } break;
                    case "AddEnterAWB": SaveDataAsync(); break;
                    case "Delete Selected Record": DeleteRecord(); break;
                    case "New": ResetData(); break;
                    case "Print": PrintData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        public bool AWBStatus()
        {
            bool ret = false;
            var stat = "";
            var res = "";
            
            bool IsNetworkAvailable = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (IsNetworkAvailable)
            {
                if (CheckLazadaAwbAllocation() == false) // It will check for our operation event(Pickupupcheckin/POD)
                {
                    // LAZMA, LAZWA, TLMKA, TLWHA, NLMYA, NLMYWA, IKFPA, IKMPA, ERC459460087MY, ERC468149403MY
                    if (EnterAWBNumber.StartsWith("LAZ") || EnterAWBNumber.StartsWith("TLM") || EnterAWBNumber.StartsWith("TLW") || EnterAWBNumber.StartsWith("NLM") || EnterAWBNumber.StartsWith("IKF") || EnterAWBNumber.StartsWith("IKM") || (EnterAWBNumber.StartsWith("ERC") && EnterAWBNumber.EndsWith("MY")))
                    {
                        // This is for check lazada shipment whethere the shipment can checkin or cancelled
                        var order = new OrderStatusParam
                        {
                            Token = "86d321823d134762aa9685f8034a174d",
                            AWBNumber = EnterAWBNumber,
                            IsPickupCheckin = true
                        };

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://api.skynet.com.my/PickupData/lazada/CheckPickupData");
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = JsonConvert.SerializeObject(order);
                            streamWriter.Write(json);
                        }

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                        {
                            res = streamReader.ReadToEnd();
                        }
                        if (res == "") { res = httpResponse.StatusDescription; }

                        List<OrderStatus> ord = new List<OrderStatus>();
                        try
                        {
                            ord = JsonConvert.DeserializeObject<List<OrderStatus>>(res);
                        }
                        catch (Exception ex)
                        {
                            ord = null;
                        }

                        if (ord != null)
                        {
                            stat = ord.First().Status.Trim().ToLower();
                        }

                        if (stat == "cancelled")
                        {
                            ret = true;
                            MessageBox.Show("Lazada merchant has cancelled this order. Please do not outbound this shipment, and return it to sender", EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else if (stat == "error" || stat == "notexist")
                        {
                            ret = true;
                            MessageBox.Show("Lazada merchant has not set Ready To Ship.  Please do not outbound this shipment, and return it to sender", EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
                else
                {
                    ret = true;
                }
            }           

            return ret;
        }

        private bool CheckLazadaAwbAllocation()
        {
            bool ret = false;
            var res = "";
            try
            {
                var order = new OrderStatusParam
                {
                    Token = "6BA9F5D57DDBAEA6158474EC4F7AE",
                    AWBNumber = EnterAWBNumber,
                    IsPickupCheckin = true
                };

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://mapi.skynet.com.my/api/Allocation/isvalidawb");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(order);
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    res = streamReader.ReadToEnd();
                }
                if (res == "") { res = httpResponse.StatusDescription; }

                AWBAllocation awb = new AWBAllocation();
                try
                {
                    awb = Newtonsoft.Json.JsonConvert.DeserializeObject<AWBAllocation>(res.ToString());
                }
                catch (Exception ex)
                {
                    awb = null; ret = false;
                }

                if (awb != null && awb.Status != "Valid")
                {
                    ret = true;
                    MessageBox.Show(awb.Remarks, EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                }

                //HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage response = client.GetAsync($"http://skynetmy.ddns.net:8020/api/Allocation/isvalidawb?AWB={EnterAWBNumber}").Result;
                //if (response.IsSuccessStatusCode)
                //{
                //    var query = response.Content.ReadAsStringAsync().Result;
                //    AWBAllocation item = Newtonsoft.Json.JsonConvert.DeserializeObject<AWBAllocation>(query.ToString());

                //    if (item.Status == "Valid" && item.customerAccNum.Trim() == "0114410070")
                //    {
                //        ret = true;
                //    }
                //    else
                //    {
                //        ret = false;
                //        MessageBox.Show($"This shipment already finished {item.status}", EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                //    }
                //}
                //else
                //{
                //    error_log.errorlog("[LodgeinVM.CheckLazadaAwbAllocation] Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                //}
            }
            catch (Exception Ex) { error_log.errorlog("LodgeinVM.ACheckLazadaAwbAllocation: " + Ex.ToString()); }

            return ret;
        }

        public void ShipmentComboboxload()
        {
            try
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GenDetId As ColOneId, A.GDDesc As ColOneText FROM tblGeneralDet A" +
                                $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                                $" WHERE RTRIM(B.GenDesc) = 'SHIPMENT TYPE' ORDER BY A.GDDesc");

                    if (dt.Rows.Count > 0)
                    {
                        ShipmentType.Clear();
                        for (int i = 0; i < dt.Rows.Count; ++i)
                        {
                            string abc = dt.Rows[i]["ColOneText"].ToString();
                            ShipmentType.Add(new CSMRShipmentTypeModel { ShipmentTypeId = abc, ShipmentTypeName = abc });
                        }
                        SelectedShipmentIndex = dt.Rows.Count - 3;
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Shipment Type Combobox load function:" + Ex.ToString()); }
        }

        private void DeleteRecord()
        {
            try
            {
                if (LodgeInListItems.Count > 0 && SelectedSItem != null)
                {
                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Do you want remove entire row?", "Remove Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblLodginAWB SET ModifyBy = {LoginUserId}, ModifyDate = GETDATE(), Status = 0, IsExported = 0 WHERE AWBNum = '{SelectedSItem.AWBNum}'");
                        if(SelectedSItem.AWBNum.Contains("EHQ") && SelectedSItem.AWBNum.Contains("D"))
                        {
                            ExecuteQuery($"UPDATE tblCashSales SET MUserID = {LoginUserId}, MDate = GETDATE(), Status = 0 WHERE CSalesNo In (Select top 1 CSalesNo from tblCSaleCNNum where CNNum = '{SelectedSItem.AWBNum}' order by CDate desc)");
                            ExecuteQuery($"UPDATE tblCSaleCNNum SET Status = 0 WHERE CNNum = '{SelectedSItem.AWBNum}'");

                        }
                        ResetData();
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("LodgeInVM Datagrid Remove Command function" + Ex.ToString()); }
        }

        public void WeightfromMachine()
        {
            try
            {
                GetWeightUtility obj = new GetWeightUtility();
                string stwt = obj.GetWeight();
                EnterWeight = Convert.ToDecimal(!string.IsNullOrEmpty(stwt) ? stwt : "1");
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void LoadGridData()
        {
            try
            {
                LodgeInListItems.Clear();
                StrTotalNoofPcs = "";
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT * FROM tblLodginAWB where FORMAT(Date, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and Status = 1");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        LodgeInListItems.Add(new LodgeInOnlyModel { LodgeinID = Convert.ToInt64(dt.Rows[i]["LodgeinID"]), SlNo = LodgeInListItems.Count + 1, AWBNum = dt.Rows[i]["AWBNum"].ToString(), Weight = Convert.ToDecimal(dt.Rows[i]["Weight"]), ShipmentType = dt.Rows[i]["ShipmentType"].ToString(), NoofPcs = Convert.ToInt32(dt.Rows[i]["NoofPcs"]) });
                    }
                    StrTotalNoofPcs = LodgeInListItems.Sum(a => a.NoofPcs).ToString();
                }

                InitializedDashboard();
             }
            catch (Exception Ex) { error_log.errorlog("LodgeInVM - LoadGridData function :" + Ex.ToString()); }
        }

        private async Task SaveDataAsync()
        {
            try
                {
                if (EnterAWBNumber.Trim() != "" && AWBStatus() == false)
                {
                    if (EnterWeight <= 0) WeightfromMachine();

                    if (EnterAWBNumber.Contains("EHQ") && EnterAWBNumber.Length == 12 && IsValid)
                    {
                        if (EnterShipmentType == "PARCEL")
                        {
                            if (EnterLength > 0 && EnterWidth > 0 && EnterHeight > 0)
                            {
                                decimal Highest_wt = CalcHigher(Convert.ToDecimal(EnterWeight), EnterLength, EnterHeight, EnterWidth), EnterPrice = 0;
                                //EnterPrice = PricePrihatinCalc(Higher);
                                await ComparePriceAsync(Highest_wt, EnterShipmentType);
                            }
                            else
                            {
                                MessageBox.Show("Please enter Length, Width and Height", EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (EnterShipmentType == "DOCUMENT")
                        {
                            //EnterPrice = PricePrihatinCalc(EnterWeight);
                            await ComparePriceAsync(EnterWeight, EnterShipmentType);
                        }
                        else
                        {
                            if (IsValid)
                            {
                                DataValid();
                            }
                        }
                    }
                    else
                    {
                        if (IsValid)
                        {
                            DataValid();
                        }
                    }
                }
            }
            catch (Exception ex) { error_log.errorlog("Lodge in  Save function:" + ex.ToString()); }
        }

        private async Task ComparePriceAsync(decimal EnterWeight_, string EnterShipmentType_)
        {
            try
            {
                string fromWeb = await getAPI();

                if (fromWeb == "No Internet")
                {
                    if (IsValid)
                    {
                        DataValid();
                    }
                }
                else
                {
                    string[] splitAPIdata = fromWeb.Split('|');
                    string stnCode = splitAPIdata[1], destStn = splitAPIdata[2];
                    string[] splitString = splitAPIdata[0].Split(';');

                    if (splitString[0] == "Not Found")
                    {
                        MessageBox.Show("AWB not paid yet! Please do not accept this AWB", EnterAWBNumber, MessageBoxButton.OK, MessageBoxImage.Information);
                        ResetData();
                    }
                    else
                    {
                        decimal userWeight = Convert.ToDecimal(splitString[0].Replace("Wt=", "0"));
                        decimal userLength = Convert.ToDecimal(splitString[1].Replace("L=", "0"));
                        decimal userHeight = Convert.ToDecimal(splitString[2].Replace("H=", "0"));
                        decimal userWidth = Convert.ToDecimal(splitString[3].Replace("W=", "0"));
                        decimal userPrice = Convert.ToDecimal(splitString[4].Replace("P=RM", "0"));
                        decimal userVM = (userLength * userHeight * userWeight) / 5000;

                        decimal EnterPrice_ = CalcWeight(EnterWeight_, destStn, EnterShipmentType);

                        if (EnterPrice_ > userPrice)
                        {
                            FrmPRIHATIN frm = new FrmPRIHATIN();
                            PRIHATINVM vm = new PRIHATINVM(frm, EnterVM, EnterWeight, userVM, userWeight,
                                userLength, userHeight, userWidth, EnterLength, EnterHeight, EnterWidth, EnterPrice_, userPrice, EnterShipmentType_, EnterNoofPcs, EnterAWBNumber, stnCode, destStn);
                            frm.Owner = Application.Current.MainWindow;
                            frm.ShowInTaskbar = false;
                            frm.DataContext = vm;
                            frm.ShowDialog();
                            ResetData();
                        }
                        else if (IsValid)
                        {
                            DataValid();
                        }
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private bool CheckConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        //public void DataValid()
        //{
        //    SqlDataReader dr = GetDataReader($"SELECT * FROM tblLodginAWB WHERE AWBNum = '{EnterAWBNumber}' and Status = 1");
        //    if (dr.HasRows)
        //    {
        //        ExecuteQuery($"UPDATE tblLodginAWB SET Weight = {EnterWeight}, ShipmentType = '{EnterShipmentType}', NoofPcs = {EnterNoofPcs}, RecFromName = '{(!string.IsNullOrEmpty(RecFromName) ? RecFromName : "")}', RecFromMobile = '{(!string.IsNullOrEmpty(RecFromMobile) ? RecFromMobile : "")}', ModifyBy = {LoginUserId}, ModifyDate = GETDATE(), Status = 1, IsExported = 0" +
        //            $" WHERE AWBNum = '{EnterAWBNumber}' and CreateDate IN (Select top 1 CreateDate from tblLodginAWB where AWBNum = '{EnterAWBNumber}' order by CreateDate desc)");
        //    }
        //    else
        //    {
        //        ExecuteQuery($"INSERT INTO tblLodginAWB(Date, AWBNum, Weight, ShipmentType, NoofPcs, RecFromMobile, RecFromName, CreateBy, CreateDate, Status, IsExported)" +
        //            $" VALUES(GETDATE(), '{EnterAWBNumber}', {EnterWeight}, '{EnterShipmentType}', {EnterNoofPcs}, '{(!string.IsNullOrEmpty(RecFromMobile) ? RecFromMobile : "")}', '{(!string.IsNullOrEmpty(RecFromName) ? RecFromName : "")}', {LoginUserId}, GETDATE(), 1, 0)");
        //    }

        //    ResetData();
        //}

        public void DataValid()
        {
            SqlDataReader dr = GetDataReader($"SELECT * FROM tblLodginAWB WHERE AWBNum = '{EnterAWBNumber}' and Status = 1");
            bool insertquery = false, updatequery = false;

            if (dr.HasRows)
            {
                DateTime today = DateTime.Today;
                if (dr.Read())
                {
                    if (today.ToString("yyyy-MM-dd") != Convert.ToDateTime(dr["CreateDate"]).ToString("yyyy-MM-dd"))
                    {
                        MessageBox.Show("This AWB has been used");
                    }
                    else
                    {
                        updatequery = true;
                    }
                }
            }
            else
            {
                insertquery = true;
            }

            if (updatequery)
            {
                string update_tblLodginAWB = ($"UPDATE tblLodginAWB SET Weight = {EnterWeight}, ShipmentType = '{EnterShipmentType}', NoofPcs = {EnterNoofPcs}, RecFromName = '{(!string.IsNullOrEmpty(RecFromName) ? RecFromName : "")}', RecFromMobile = '{(!string.IsNullOrEmpty(RecFromMobile) ? RecFromMobile : "")}', ModifyBy = {LoginUserId}, ModifyDate = GETDATE(), Status = 1, IsExported = 0 WHERE AWBNum = '{EnterAWBNumber}' and CreateDate IN (Select top 1 CreateDate from tblLodginAWB where AWBNum = '{EnterAWBNumber}' order by CreateDate desc) ");
                ExecuteQuery(update_tblLodginAWB);
            }

            if (insertquery)
            {
                string insert_tblLodginAWB = ($"INSERT INTO tblLodginAWB(Date, AWBNum, Weight, ShipmentType, NoofPcs, RecFromMobile, RecFromName, CreateBy, CreateDate, Status, IsExported)" +
                                $" VALUES(GETDATE(), '{EnterAWBNumber}', {EnterWeight}, '{EnterShipmentType}', {EnterNoofPcs}, '{(!string.IsNullOrEmpty(RecFromMobile) ? RecFromMobile : "")}', '{(!string.IsNullOrEmpty(RecFromName) ? RecFromName : "")}', {LoginUserId}, GETDATE(), 1, 0)");
                ExecuteQuery(insert_tblLodginAWB);
            }

            ResetData();
        }

        private void VolumetricCal()
        {
            try
            {
                EnterVM = (EnterLength * EnterWidth * EnterHeight) / 5000;
                OnPropertyChanged("EnterVM");
            }
            catch (Exception Ex) { error_log.errorlog("Volumetric Weight Cal Function : " + Ex.ToString()); }
        }

        private void UpdateVisibility()
        {
            if(EnterAWBNumber.Contains("EHQ") && EnterAWBNumber.Length == 12 && EnterShipmentType == "PARCEL")
            {
                VWVisibility = true;
            }
            else
            {
                VWVisibility = false;
            }
        }

        private decimal PricePrihatinCalc(decimal Weight)
        {
            decimal price = 5.00m, additional = 3.00m;

            if(Weight > 1)
            {
                int iPart = (int)Weight - 1;
                decimal dPart = Weight % 1.0m;

                if(dPart > 0)
                {
                    price = price + additional + (iPart*additional);
                }
                else
                {
                    price = price + (iPart * additional);
                }
            }

            //if(Weight >= 1)
            //{
            //    minusOneKG = Math.Round((Weight - 1),2);
            //}

            //price = price + (minusOneKG * additional);

            return price;
        }

        private async Task<string> getAPI()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://api.skynet.com.my/");

            if(CheckConnection("https://www.google.com/") == false)
            {
                return "No Internet";
            }
            else
            {

                client.DefaultRequestHeaders.Accept.Add(
                   new MediaTypeWithQualityHeaderValue("application/json"));

                var data = new Data();

                data.access_token = "C4447448SND5F0S484DSNB3FASN5B2E07A83094";
                data.functionstr = "getReferenceAndEventType";
                data.GetType = "AWBNumber";
                data.GetVal = EnterAWBNumber;

                var response = client.PostAsJsonAsync("api/sn/getAnyData", data).Result;

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();

                    try
                    {
                        List<Model> result = JsonConvert.DeserializeObject<List<Model>>(jsonString);
                        return result[0].Reference + "|" + result[0].StnCode + "|" + result[0].DestStn;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        private decimal CalcHigher(decimal wt, decimal l, decimal h, decimal w)
        {
            decimal VWT = Math.Round(((h * l * w) / 5000), 2);
            if(VWT > wt)
            {
                return VWT;
            }
            else
            {
                return wt;
            }            
        }

        public class Data
        {
            public string access_token { get; set; }
            public string functionstr { get; set; }
            public string LenGetTypegth { get; set; }
            public string GetType { get; set; }
            public string GetVal { get; set; }
        }

        public class Model
        {
            public string EventType { get; set; }
            public string Reference { get; set; }
            public string StnCode { get; set; }
            public string DestStn { get; set; }
        }

        private void PrintData()
        {
            try
            {
                string RPTHeaderName  = "Lodge in AWB list from " + DateTime.Now.ToString("dd-MMM-yyyy") + " to " + DateTime.Now.Date.AddHours(23.9999).ToString("dd-MMM-yyyy");
                DataTable dtRpt, DTCompany;
                using (dtRpt = new DataTable())
                {
                    dtRpt = GetDataTable($"SELECT LodgeinID, Date, AWBNum, Weight, ShipmentType, NoofPcs, RecFromName, RecFromMobile, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate, '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm")}' As FromDate, '{DateTime.Now.Date.AddHours(23.9999).ToString("yyyy-MM-dd HH:mm:ss")}' As ToDate, '{LoginUserName}' As CurrentUser FROM tblLodginAWB" +
                        $" WHERE (Date BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm")}' AND '{DateTime.Now.Date.AddHours(23.9999).ToString("yyyy-MM-dd HH:mm:ss")}') AND Status = 1");

                    if (dtRpt.Rows.Count > 0)
                    {
                        DTCompany = new DataTable();
                        DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.Status FROM tblStationProfile a where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' and a.Status = 1 and a.ToDate is null");

                        FrmReportViewer RptViewerGen = new FrmReportViewer();
                        RptViewerGen.Owner = Application.Current.MainWindow;
                        ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompany, "LodgeInAWB", dtRpt, "", dtRpt, "", dtRpt, "RPTLodgeInAWB.rdlc", RPTHeaderName, RptViewerGen.RptViewer, true, false);
                        RptViewerGen.DataContext = VM;
                        RptViewerGen.Show();
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Lodge In AWB Function : " + Ex.ToString()); }
        }

        private decimal CalcWeight(decimal Weight, string deststn, string shipmenttype)
        {
            decimal Price = 0;

            if (shipmenttype.ToUpper() == "PARCEL" && Weight > 1)
            {
                Weight = Math.Ceiling(Weight / 1);
            }

            if (shipmenttype != null && shipmenttype != "" && deststn != "" && deststn != null && Weight > 0)
            {
                DataTable dtQuotationItems = new DataTable();
                dtQuotationItems = GetDataTable($"SELECT B.QuotationType, A.FromZone, A.ToZone, A.ShipmentType, A.FirstWeight, A.FirstRate, A.AddWeight, A.AddRate, A.Surcharge FROM tblQuotationDet A" +
                    $" JOIN tblQuotation B ON A.QuotationId = B.QuotationId" +
                    $" WHERE A.ToDate IS NULL AND UPPER(RTRIM(A.ShipmentType)) = '{shipmenttype.ToUpper()}S' AND A.FirstWeight <= {Weight}" +
                    $" AND UPPER(RTRIM(A.ToZone)) IN (SELECT DISTINCT UPPER(RTRIM(ZoneCode)) FROM tblZoneDetail WHERE UPPER(RTRIM(DestCode)) = '{deststn.ToUpper()}')" +
                    $" ORDER BY A.FirstWeight DESC, A.FirstRate DESC");
                if (dtQuotationItems.Rows.Count == 0)
                {
                    dtQuotationItems = new DataTable();
                    dtQuotationItems = GetDataTable($"SELECT B.QuotationType, A.FromZone, A.ToZone, A.ShipmentType, A.FirstWeight, A.FirstRate, A.AddWeight, A.AddRate, A.Surcharge FROM tblQuotationDet A" +
                        $" JOIN tblQuotation B ON A.QuotationId = B.QuotationId" +
                        $" WHERE A.ToDate IS NULL AND UPPER(RTRIM(A.ShipmentType)) = '{shipmenttype.ToUpper()}S' AND A.FirstWeight >= {Weight}" +
                        $" AND UPPER(RTRIM(A.ToZone)) IN (SELECT DISTINCT UPPER(RTRIM(ZoneCode)) FROM tblZoneDetail WHERE UPPER(RTRIM(DestCode)) = '{deststn.ToUpper()}')" +
                        $" ORDER BY A.FirstWeight, A.FirstRate DESC");
                }
                string ToZone = "";
                decimal FirstWeight = 0, FirstRate = 0, AddWeight = 0, AddRate = 0, Surcharge = 0;
                if (dtQuotationItems.Rows.Count > 0)
                {
                    ToZone = dtQuotationItems.Rows[0]["ToZone"].ToString();
                    FirstWeight = Convert.ToDecimal(dtQuotationItems.Rows[0]["FirstWeight"]);
                    FirstRate = Convert.ToDecimal(dtQuotationItems.Rows[0]["FirstRate"]);
                    AddWeight = Convert.ToDecimal(dtQuotationItems.Rows[0]["AddWeight"]);
                    AddRate = Convert.ToDecimal(dtQuotationItems.Rows[0]["AddRate"]);
                    Surcharge = Convert.ToDecimal(dtQuotationItems.Rows[0]["Surcharge"]);
                }
                DataTable newdt;
                if (ToZone.Trim() != "")
                {
                    newdt = new DataTable();
                    newdt = GetDataTable($"SELECT b.ZoneDesc as ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate FROM tblQuotationDet a" +
                        $" LEFT JOIN tblZone b ON UPPER(RTRIM(a.ToZone)) = UPPER(RTRIM(b.ZoneCode))" +
                        $" WHERE UPPER(RTRIM(ToZone)) = '{ToZone.ToUpper().Trim()}' AND ToDate IS NULL ORDER BY ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate");
                    //tempListDestDetail.Clear();
                    //for (int i = 0; i < dtQuotationItems.Rows.Count; i++)
                    //{
                    //    tempZoneDesc = dtQuotationItems.Rows[i]["ToZone"].ToString();
                    //    tempShipmentType = dtQuotationItems.Rows[i]["ShipmentType"].ToString();
                    //    tempFirstWeight = Convert.ToDecimal(dtQuotationItems.Rows[i]["FirstWeight"]);
                    //    tempFirstRate = Convert.ToDecimal(dtQuotationItems.Rows[i]["FirstRate"]);
                    //    tempAddWeight = Convert.ToDecimal(dtQuotationItems.Rows[i]["AddWeight"]);
                    //    tempAddRate = Convert.ToDecimal(dtQuotationItems.Rows[i]["AddRate"]);
                    //    tempListDestDetail.Add(tempAWBNo + "|" + tempZoneDesc + "|" + tempShipmentType + "|" + tempFirstWeight + "|" + tempFirstRate + "|" + tempAddWeight + "|" + tempAddRate);
                    //    QuotationItems.Add(new QuotationInfo { ZoneDesc = tempZoneDesc, ShipmentType = tempShipmentType, FirstWeight = tempFirstWeight, ForstRate = tempFirstRate, AddWeight = tempAddWeight, AddRate = tempAddRate });
                    //}
                    decimal CalcWeight = Weight;
                    if (CalcWeight <= FirstWeight)
                    {
                        Price = FirstRate;
                    }
                    else
                    {
                        Price = FirstRate;
                        CalcWeight = CalcWeight - FirstWeight;
                        if (CalcWeight > 0 && AddWeight > 0)
                        {
                            CalcWeight = Math.Ceiling(CalcWeight / AddWeight);
                            Price = Price + (CalcWeight * AddRate);
                        }
                    }
                }
            }
            Price = GetRoundingVal(Price, 2);
            return Price;
        }

        private void ResetData()
        {
            //InitializedDashboard(); // this used inside LoadGridData(), so no need to call here.
            string Str = string.Empty;
            StrTotalNoofPcs = "";
            EnterAWBNumber = ""; EnterWeight = 0; EnterLength = 0; EnterHeight = 0; EnterWidth = 0; /* EnterShipmentType = "DOC/SPX";*/
            RecFromMobile = RecFromName = "";
            UserName = LoginUserName;
            EnterNoofPcs = 1; Wndow.txtEnterAWBNumber.Focus();
            LoadGridData();
            ShipmentComboboxload();
        }

    }

    public class AWBAllocation
    {
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public string Remarks { get; set; }
        public string Origin { get; set; }
        public string CustomerAccNum { get; set; }
        public string CustDeptCode { get; set; }
    }

    public class OrderStatusParam
    {
        public string Token { get; set; }
        public string AWBNumber { get; set; }
        public bool IsPickupCheckin { get; set; }
    }

    public class OrderStatus
    {
        public string Status { get; set; }
        public string ShipDescription { get; set; }
        public string CancelDate { get; set; }
        public string AWBNumber { get; set; }
    }

    public class LodgeInOnlyModel : PropertyChangedNotification
    {
        private long _lodgeiniD;
        public long LodgeinID
        {
            get { return _lodgeiniD; }
            set { _lodgeiniD = value; OnPropertyChanged("LodgeinID"); }
        }
        private int _slno;
        public int SlNo
        {
            get { return _slno; }
            set { _slno = value; OnPropertyChanged("SlNo"); }
        }
        private string _awbnum;
        public string AWBNum
        {
            get { return _awbnum; }
            set { _awbnum = value; OnPropertyChanged("AWBNum"); }
        }
        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; OnPropertyChanged("Weight"); }
        }
        private string _shipmenttype;
        public string ShipmentType
        {
            get { return _shipmenttype; }
            set { _shipmenttype = value; OnPropertyChanged("ShipmentType"); }
        }
        private int _noofpcs;
        public int NoofPcs
        {
            get { return _noofpcs; }
            set { _noofpcs = value; OnPropertyChanged("NoofPcs"); }
        }
    }
}
