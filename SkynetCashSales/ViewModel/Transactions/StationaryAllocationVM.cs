﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
//using System.Windows.Forms;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions

{
    public class StationaryAllocationVM : AMGenFunction
    {
        public FrmStationaryAllocation MyWind;
        public Window abc;
        private static DataTable DTConsignee = new DataTable();
        public StationaryAllocationVM(FrmStationaryAllocation Wind)
        {
            MyWind = Wind;
            ResetData();
            if (LoginUserAuthentication == "User")
            {
                MyWind.BtnSave.Visibility = Visibility.Hidden; 
                MyWind.BtnDelete.Visibility = Visibility.Hidden;
                MyWind.BtnNew.Visibility = Visibility.Hidden;
            }
            else
            {
                MyWind.BtnSave.Visibility = Visibility.Visible;
                MyWind.BtnDelete.Visibility = Visibility.Visible;
                MyWind.BtnNew.Visibility = Visibility.Visible;
            }
        }
        public static DataTable csvData;

                
        
        private int _NoOfCode;
        private bool _AllocateNumEnabled;
        private long _AllocateId, _ItemId;
        private string _AllocateNum, _AllocateDate, _ItemCode, _ItemName, _FromCode, _ToCode, _Prefix, _Suffix, _FinalFromCode, _FinalToCode;

        public long AllocateId { get { return _AllocateId; } set { _AllocateId = value; OnPropertyChanged("AllocateId"); } }
        public string AllocateNum { get { return _AllocateNum; } set { _AllocateNum = value; OnPropertyChanged("AllocateNum"); } }
        public bool AllocateNumEnabled { get { return _AllocateNumEnabled; } set { _AllocateNumEnabled = value; OnPropertyChanged("AllocateNumEnabled"); } }
        public string AllocateDate { get { return _AllocateDate; } set { _AllocateDate = value; OnPropertyChanged("AllocateDate"); } }
        public long Item_Id { get { return _ItemId; } set { _ItemId = value; OnPropertyChanged("Item_Id"); } }
        public string Item_Code { get { return _ItemCode; } set { _ItemCode = value; OnPropertyChanged("Item_Code"); } }
        public string Item_Name { get { return _ItemName; } set { _ItemName = value; OnPropertyChanged("Item_Name"); } }
        public string Prefix { get { return _Prefix; } set { _Prefix = value; OnPropertyChanged("Prefix"); } }
        public string Suffix { get { return _Suffix; } set { _Suffix = value; OnPropertyChanged("Suffix"); } }
        public int NoOfCode { get { return _NoOfCode; } set { _NoOfCode = value; OnPropertyChanged("NoOfCode");  } }
        public string FromCode { get { return _FromCode; } set { _FromCode = value; OnPropertyChanged("FromCode"); } }
        public string ToCode { get { return _ToCode; } set { _ToCode = value; OnPropertyChanged("ToCode"); } }
        public string FinalFromCode { get { return _FinalFromCode; } set { _FinalFromCode = value; OnPropertyChanged("FinalFromCode"); } }
        public string FinalToCode { get { return _FinalToCode; } set { _FinalToCode = value; OnPropertyChanged("FinalToCode"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
                
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();
            try
            {
                switch (Obj.ToString())
                {
                    case "Stationary Allocation Number":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.AllocateId, ColOneText = a.ItemCode, ColTwoText = a.AllocateNum, ColThreeText = a.AllocateDate FROM tblStationaryAllocation where a.IsDelete = '0' order by a.AllocateNum asc ");
                            //var query = (from a in CSalesEntity.tblStationaryAllocations
                            //             where a.IsDelete == false
                            //             orderby a.AllocateNum ascending
                            //             select new { ColOneId = a.AllocateId, ColOneText = a.ItemCode, ColTwoText = a.AllocateNum, ColThreeText = a.AllocateDate }).ToList();
                            //dt = LINQToDataTable(query);
                            

                            ChildWnd.FrmListCol3_Closed += (r => { AllocateId = Convert.ToInt64(r.Id); AllocateNum = r.ColTwoText; AllocateDate = r.ColThreeText; LoadData(); });
                            ChildWnd.ThreeColWindShow(dt, "Stationary Allocation No.", "Item Code", "AllocateNo", "Date");
                        }
                        break;
                    case "Item":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.ItemId, ColOneText = a.ItemCode, ColTwoText = a.ItemDesc FROM tblItemMaster WHERE a.Status = '1' ORDER BY a.ItemDesc asc");
                            //var query = (from a in CSalesEntity.tblItemMasters
                            //             where a.Status == true
                            //             orderby a.ItemDesc ascending
                            //             select new { ColOneId = a.ItemId, ColOneText = a.ItemCode, ColTwoText = a.ItemDesc }).ToList();
                            //dt = LINQToDataTable(query);
                            

                            ChildWnd.FrmListCol2_Closed += (r => { Item_Id = Convert.ToInt64(r.Id); Item_Code = r.ColOneText; Item_Name = r.ColTwoText; });
                            ChildWnd.TwoColWindShow(dt, "Item Selection", "Item Code", "Item Name");
                        }
                        break;
                    case "Load Allocation Details": LoadData(); break;
                    case "New": ResetData(); AllocateNumEnabled = false; MyWind.txtItemCode.Focus(); break;
                    case "Generate ToNumber":
                        {
                            if (NoOfCode == 0)
                            {
                                System.Windows.MessageBox.Show("Please eneter no. of Code to be locked");
                                FromCode = null;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(FromCode))
                                {                                    
                                    if (CheckAllocation("SingleCode", FromCode, ""))
                                    {
                                        if (FromCode.Length == 12)
                                        {
                                            ToCode = (Convert.ToInt64(FromCode.Substring(0, FromCode.Length - 1)) + (NoOfCode - 1)).ToString() + ((Convert.ToInt64(FromCode.Substring(0, FromCode.Length - 1)) + (NoOfCode - 1)) % 7).ToString();
                                            //(Convert.ToInt64(FromCode) + NoOfCode).ToString();
                                        }
                                        else
                                        {
                                            ToCode = (Convert.ToInt64(FromCode) + (NoOfCode - 1)).ToString();
                                        }
                                        NumberCode();
                                    }
                                    if (CheckAllocation("FROMTOCode", FromCode, ToCode) && !string.IsNullOrEmpty(ToCode))
                                    {
                                        NumberCode();
                                        MyWind.BtnSave.Focus();
                                    }
                                    else
                                    {
                                        System.Windows.MessageBox.Show("Code Number is already allocated", "Warning");
                                        FromCode = ToCode = "";
                                        MyWind.txtFromNumber.Focus();
                                    }

                                }
                            }//FocusNextControl();
                        }
                        break;
                    case "Save":
                        {
                            string auth=StrAuthenticateType();
                            if (auth == "ADMIN"||auth == "Admin")
                            {
                                SaveData();
                            }
                            else { MessageBox.Show("ADMIN only, your authentication type: '" + auth+"'", "Warning"); }
                        } break;
                    case "Delete":
                        {
                            if (AllocateNum != null && AllocateNum != "")
                            {
                                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                                if (messageBoxResult == MessageBoxResult.Yes)
                                    Delete();
                            }
                            else
                                System.Windows.MessageBox.Show("Select Account Number to delete");
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                System.Windows.MessageBox.Show(Ex.Message);
            }
        }
        public void NumberCode()
        {
            if (!string.IsNullOrEmpty(FromCode) && !string.IsNullOrEmpty(ToCode))
            {
                if (!string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    FinalFromCode = Prefix.Trim() + FromCode + Suffix.Trim();
                    FinalToCode = Prefix.Trim() + ToCode + Suffix.Trim();
                }
                else if (!string.IsNullOrEmpty(Prefix) && string.IsNullOrEmpty(Suffix))
                {
                    FinalFromCode = Prefix.Trim() + FromCode;
                    FinalToCode = Prefix.Trim() + ToCode;
                }
                else if (string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    FinalFromCode = FromCode + Suffix.Trim();
                    FinalToCode = ToCode + Suffix.Trim();
                }
                else
                {
                    FinalFromCode = FromCode;
                    FinalToCode = ToCode;
                }
            }
        }
        public void Delete()
        {
            int i = ExecuteQuery($"UPDATE tblstationaryAllocation a SET a.IsDelete='1'  WHERE a.AllocateNum = '{AllocateNum}'");
            //var delete = CSalesEntity.tblStationaryAllocations.Where(a => a.AllocateNum == AllocateNum).ToList();
            //delete.ForEach(a => a.IsDelete = true);
            //CSalesEntity.SaveChanges();
            ResetData();
        }

        private bool CheckAllocation(string Tag, string FromNumber, string ToNumber)
        {
            long fromNUMBER = FromNumber != null && FromNumber != "" ? Convert.ToInt64(FromNumber) : 0;
            long toNUMBER = ToNumber != null && ToNumber != "" ? Convert.ToInt64(ToNumber) : 0;
            string checking = "";
            if (Tag == "SingleCode")
            {
               
                //For Tag type SingleCode in SP only FROM Number will check to number not check.
                if (!string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where a.Prefix = '{Prefix}' AND a.Suffix = '{Suffix}' AND a.FromCodeRunNum >= '{fromNUMBER}' AND a.ToCodeRunNum <= '{fromNUMBER}' ");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }dr.Close();
                 
                }
                else if (!string.IsNullOrEmpty(Prefix) && string.IsNullOrEmpty(Suffix))
                {
                    //long idcheck = (from a in CSalesEntity.tblStationaryAllocations where a.Prefix == Prefix && a.FromCodeRunNum >= fromNUMBER && a.ToCodeRunNum <= fromNUMBER select a.AllocateId).FirstOrDefault();
                    //checking = idcheck > 0 ? "" : "NOTYET";

                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Prefix = '{Prefix}' AND a.FromCodeRunNum >= '{fromNUMBER}' AND a.ToCodeRunNum <= '{fromNUMBER}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 
                }
                else if (string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Suffix == '{Suffix}'  AND a.FromCodeRunNum >= '{fromNUMBER}' AND a.ToCodeRunNum <= '{fromNUMBER}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 

           
                }
                else
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Suffix == '{Suffix}'  AND a.FromCodeRunNum >= '{fromNUMBER}' AND a.ToCodeRunNum <= '{fromNUMBER}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 


                }
            }
            else if (Tag == "FROMTOCode")
            {
                //For Tag type FROMTOCode in SP we need to pass FROMCode && TOCode Numbers.
                if (!string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Prefix = '{Prefix}' AND a.Suffix = '{Suffix}' AND a.FromCodeRunNum >= '{fromNUMBER}' AND a.ToCodeRunNum <= '{fromNUMBER}' AND a.FromCodeRunNum >= '{toNUMBER}' AND a.ToCodeRunNum <= '{toNUMBER}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 

                }
                else if (!string.IsNullOrEmpty(Prefix) && string.IsNullOrEmpty(Suffix))
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Prefix == '{Prefix}' && a.FromCodeRunNum >= '{fromNUMBER}' && a.ToCodeRunNum <= '{fromNUMBER}' && a.FromCodeRunNum >= '{toNUMBER}' && a.ToCodeRunNum <= '{toNUMBER}'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 

                }
                else if (string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Suffix))
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where  a.Suffix = '{Suffix}' && a.FromCodeRunNum >= '{fromNUMBER}' && a.ToCodeRunNum <= '{fromNUMBER}' && a.FromCodeRunNum >= '{toNUMBER}' && a.ToCodeRunNum <= '{toNUMBER}' ");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 

                }
                else
                {
                    SqlDataReader dr = GetDataReader($"SELECT  a.AllocateId FROM tblStationaryAllocation a where   a.FromCodeRunNum >= '{fromNUMBER}' && a.ToCodeRunNum <= '{fromNUMBER}' && a.FromCodeRunNum >= '{toNUMBER}' && a.ToCodeRunNum <= '{toNUMBER}' ");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {

                            long idcheck = Convert.ToInt64(dr["AllocateId"]);
                            checking = idcheck > 0 ? "" : "NOTYET";
                        }
                    }
                    dr.Close(); 

                }
            }
            else { }
            if (checking == "NOTYET") { return true; }
            else { return false; }
        }

        private void SaveData()
        {
            if (IsValidate)
            {
                if (CheckAllocation("FROMTOCode", FromCode, ToCode))
                {
                    if (AllocateId == 0)
                    {
                        int i = ExecuteQuery($"INSERT INTO tblStationaryAllocation (AllocateNum,AllocateDate,StnCode, SubName, ItemCode, NoOfCode,Prefix,Suffix,FromCode,FromCodeRunNum,ToCode,ToCodeRunNum,CreateBy,CreateDate,ModifyBy,ModifyDate,Status,IsExported,IsDelete) " +
                            $"VALUES ('{AllocateNum}','{Convert.ToDateTime(AllocateDate)}','{MyCompanyCode}','{MyCompanySubName}','{Item_Code}','{NoOfCode}','{Prefix}','{Suffix}','{FinalFromCode}','{Convert.ToInt64(FromCode)}','{FinalToCode}','{Convert.ToInt64(ToCode)}','{LoginUserId}','{DateTime.Now}','{null}','{null}','ACTIVE','0','0')");
                        
                        //CSalesEntity.tblStationaryAllocations.Add(new tblStationaryAllocation
                        //{
                        //    AllocateNum = AllocateNum,
                        //    AllocateDate = Convert.ToDateTime(AllocateDate),
                        //    StnCode = MyCompanyCode,
                        //    SubName = MyCompanySubName,
                        //    ItemCode=Item_Code,
                        //    NoOfCode = NoOfCode,
                        //    Prefix = Prefix,
                        //    Suffix = Suffix,
                        //    FromCode = FinalFromCode,
                        //    FromCodeRunNum = Convert.ToInt64(FromCode),
                        //    ToCode = FinalToCode,
                        //    ToCodeRunNum = Convert.ToInt64(ToCode),
                        //    CreateBy = LoginUserId,
                        //    CreateDate = DateTime.Now,
                        //    ModifyBy = null,
                        //    ModifyDate = null,
                        //    Status = "ACTIVE",
                        //    IsExported = false,
                        //    IsDelete = false,
                        //}); 
                        //CSalesEntity.SaveChanges();
                        AllocateId = ExecuteQuery($"SELECT max(a.AllocateId) FROM tblStationaryAllocation a");
                        //AllocateId = CSalesEntity.tblStationaryAllocations.Select(a => a.AllocateId).Max();
                        

                        AllocateNum = "M" + string.Format("{0:00000}", AllocateId);

                 
                        int j = ExecuteQuery($"UPDATE tblStationaryAllocation  a SET a.AllocateNum = '{AllocateNum}' WHERE  a.AllocateId = {AllocateId}");
                        

                        //tblStationaryAllocation updateallocation = CSalesEntity.tblStationaryAllocations.Where(a => a.AllocateId == AllocateId).FirstOrDefault();
                        //updateallocation.AllocateNum = AllocateNum;
                        //CSalesEntity.SaveChanges();

                        if (j > 0)
                        {
                            MessageBox.Show("Saved Successfully"); ResetData();
                        }
                        else
                        {
                            MessageBox.Show("Update Failed"); 
                        }
                        
                    }
                    else
                    {
                        
                        int j = ExecuteQuery($"UPDATE tblStationaryAllocation  a SET a.AllocateNum = '{AllocateNum}', a.AllocateDate = '{Convert.ToDateTime(AllocateDate)}'," +
                            $"a.StnCode = '{MyCompanyCode}', a.SubName = '{MyCompanySubName}', a.ItemCode = '{Item_Code}',a.Prefix = '{Prefix}',a.Suffix = '{Suffix}',a.NoOfCode = '{NoOfCode}',a.FromCode = '{FinalFromCode}', " +
                            $"a.FromCodeRunNum = '{Convert.ToInt64(FromCode)}' , a.ToCode = '{FinalToCode}' , a.ToCodeRunNum = '{Convert.ToInt64(ToCode)}' , a.ModifyBy = '{LoginUserId}' , a.ModifyDate = '{DateTime.Now}' , a.Status = 'ACTIVE', a.IsExported = 0, a.IsDelete = 0 WHERE  a.AllocateId = {AllocateId}");
                        

                        //tblStationaryAllocation updateallocation = CSalesEntity.tblStationaryAllocations.Where(a => a.AllocateId == AllocateId).FirstOrDefault();
                        //updateallocation.AllocateNum = AllocateNum;
                        //updateallocation.AllocateDate = Convert.ToDateTime(AllocateDate);
                        //updateallocation.StnCode = MyCompanyCode;
                        //updateallocation.SubName = MyCompanySubName;
                        //updateallocation.ItemCode = Item_Code;
                        //updateallocation.Prefix = Prefix;
                        //updateallocation.Suffix = Suffix;
                        //updateallocation.NoOfCode = NoOfCode;
                        //updateallocation.FromCode = FinalFromCode;
                        //updateallocation.FromCodeRunNum = Convert.ToInt64(FromCode);
                        //updateallocation.ToCode = FinalToCode;
                        //updateallocation.ToCodeRunNum = Convert.ToInt64(ToCode);
                        //updateallocation.ModifyBy = LoginUserId;
                        //updateallocation.ModifyDate = DateTime.Now;
                        //updateallocation.Status = "ACTIVE";
                        //updateallocation.IsExported = false;
                        //updateallocation.IsDelete = false;

                        //CSalesEntity.SaveChanges();
                        if (j > 0)
                        {
                            MessageBox.Show("Updated Successfully"); ResetData();
                        }
                        else
                        {
                            MessageBox.Show("Update Failed");
                        }
                        
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("Code Numbers already allocated", "Warning");
                }
            }
            LoadData();
        }
        
        private void ResetData()
        {
            SqlDataReader dr = GetDataReader("SELECT max(a.ToCode) as ToCode FROM tblStationaryAllocation ");
            string LastNum = dr["ToCode"].ToString();
            AllocateId = 0; AllocateNum = ""; AllocateNumEnabled = true; AllocateDate = DateTime.Now.ToString();
            Prefix = Suffix = FinalFromCode = FinalToCode = string.Empty;
            Item_Code = ""; Item_Name = ""; NoOfCode = 0;
            FromCode = ""; ToCode = ""; Item_Name = "";
            MyWind.txtAllocateNum.Focus(); MyWind.txtAllocateNum.SelectionStart = MyWind.txtAllocateNum.Text.Length;
        }
        private void LoadData()
        {
            AllocateDate = DateTime.Now.ToShortDateString(); NoOfCode = 0; FromCode = ""; ToCode = "";
            if (AllocateNum != null && AllocateNum != "")
            {
                SqlDataReader dr = GetDataReader($"SELECT a.AllocateNum, a.AllocateDate, a.ItemCode, a.Prefix, a.Suffix, a.FromCodeRunNum, a.ToCodeRunNum, a.NoOfCode, a.FromCode, a.ToCode, a.Status FROM tblStationaryAllocation a WHERE a.AllocateNum = '{AllocateNum}' AND a.IsDelete = 0 AND a.StnCode = '{MyCompanyCode}' AND (a.SubName = null OR a.SubName = '{MyCompanySubName}') ");
                //var query = (from a in CSalesEntity.tblStationaryAllocations where a.AllocateNum == AllocateNum && a.IsDelete == false && a.StnCode == MyCompanyCode && (a.SubName == null || a.SubName == MyCompanySubName) select new { a.AllocateNum, a.AllocateDate, a.ItemCode, a.Prefix, a.Suffix, a.FromCodeRunNum, a.ToCodeRunNum, a.NoOfCode, a.FromCode, a.ToCode, a.Status }).FirstOrDefault();

                if (dr.HasRows)
                {
                    AllocateNum = dr["AllocateNum"].ToString();
                    AllocateDate = dr["AllocateDate"].ToString();
                    Item_Code = dr["ItemCode"].ToString();
                    Prefix = dr["Prefix"].ToString();
                    Suffix = dr["Suffix"].ToString();
                    NoOfCode = Convert.ToInt32(dr["NoOfCode"]);
                    FromCode = dr["FromCodeRunNum"].ToString();
                    ToCode = dr["ToCodeRunNum"].ToString();
                    NumberCode();
                }
            }
        }

        static readonly string[] CustAccProperties = { "Item_Code", "NoOfCode", "FromCode", "ToCode" };
        public bool IsValidate
        {
            get
            {
                foreach (string property in CustAccProperties)
                {
                    if (GetValidationError(property) != null) return false;
                }
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "Item_Code": if (string.IsNullOrEmpty(Item_Code)) { error = "Invalid Account No."; MyWind.txtItemCode.Focus(); } break;
                case "NoOfCode": if (NoOfCode <= 0) { error = "Invalid No. Of Code"; MyWind.txtNumOfCode.Focus(); } break;
                case "FromCode": if (string.IsNullOrEmpty(FromCode)) { error = "Invalid From Code"; MyWind.txtFromNumber.Focus(); } break;
                case "ToCode": if (string.IsNullOrEmpty(ToCode)) { error = "Invalid To Code"; MyWind.txtToNumber.Focus(); } break;

                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) System.Windows.MessageBox.Show(error);
            return error;
        }

    }
}
