﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SkynetCashSales.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

namespace SkynetCashSales.ViewModel.Transactions
{
    class TrackingHTTPClient
    {
        public class Tracking
        {
           
            public string MAWBNumber { get; set; }
            public string AWBNumber { get; set; }
            public System.DateTime AWBDate { get; set; }
            public string ShipmentType { get; set; }
            public string DestStn { get; set; }
            public string CreatedBy { get; set; }
            public string EventType { get; set; }
            public string Reference { get; set; }
            public string CreatedByStn { get; set; }
           

        }


        public class TrackingModel
        {
            public string access_token { get; set; }
            public string functionstr { get; set; }
            public List<Tracking> AWBTracking = new List<Tracking>();
        }

        

        public class Program
        {
            static HttpClient client = new HttpClient();

            //static void ShowProduct(Tracking product)
            //{
            //    Console.WriteLine($"Name: {product.Name}\tPrice: " +
            //        $"{product.Price}\tCategory: {product.Category}");
            //}

            public static ResultAWBTrackingDto CreateProductAsync(TrackingModel tracking)
            {
                try
                {
                    ResultAWBTrackingDto result = new ResultAWBTrackingDto();
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(tracking);


                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = client.PostAsync("http://api.skynet.com.my/api/sn/createTrackingstatus", content).Result;

                    var responseJson = response.Content.ReadAsStringAsync().Result;


                    //MessageBox.Show(responseJson.ToString());
                    responseJson = responseJson.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });

                    string code = JObject.Parse(responseJson)["code"].ToString(Newtonsoft.Json.Formatting.None);
                    string message = JObject.Parse(responseJson)["message"].ToString(Newtonsoft.Json.Formatting.None);
                    string status = JObject.Parse(responseJson)["status"].ToString(Newtonsoft.Json.Formatting.None);

                    code = code.TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
                    message = message.TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
                    status = status.TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
                    result.code = Convert.ToInt32(code);
                    result.message = message.ToString();
                    result.status = status.ToString();

                    return result;
                }
                catch (Exception ex)
                {
                    ResultAWBTrackingDto result = new ResultAWBTrackingDto();

                    result.code = 500;
                    result.message = "Failed to Contact TrackingAPI";
                    result.status = "FAILED";

                    error_log.errorlog(ex.ToString());

                    return result;
                }
                
            }

            public static List<CSSUpgradeDto> GetProductAsync(string token)
            {
                try
                {
                    var my_jsondata = new
                    {
                        access_token = "86d321823d134762aa9685f8034a174d"
                    };
                    string json_data = JsonConvert.SerializeObject(my_jsondata);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");

                    List<CSSUpgradeDto> cssupgraderesp = new List<CSSUpgradeDto>();
                    Tracking product = null;
                    HttpResponseMessage response = client.PostAsync("http://skynetmy.ddns.net:8100/api/Skynet/GetTblCSSUpgrade", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //var responseJson = response.Content.ReadAsStringAsync().Result;
                        cssupgraderesp = response.Content.ReadAsAsync<List<CSSUpgradeDto>>().Result;
                    }
                    return cssupgraderesp;
                } 
                catch (Exception ex)
                {
                    error_log.errorlog("API GetProductAsync:" + ex.ToString());
                    return null;
                }
      
            }

            static async Task<Tracking> UpdateProductAsync(Tracking tracking)
            {
                HttpResponseMessage response = await client.PutAsJsonAsync(
                    $"api/sn/pub/AWBTracking/{tracking.AWBNumber}", tracking);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                tracking = await response.Content.ReadAsAsync<Tracking>();
                return tracking;
            }

            static async Task<HttpStatusCode> DeleteProductAsync(string id)
            {
                HttpResponseMessage response = await client.DeleteAsync(
                    $"api/products/{id}");
                return response.StatusCode;
            }

         
          
        }
    }
}
