﻿using SkynetCashSales.General;
using SkynetCashSales.View.Reports;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class QuotationVM : AMGenFunction
    {
        public FrmQuotation MyWind;
        public QuotationVM(FrmQuotation Wind, string type)
        {
            MyWind = Wind;
            Items = new ObservableCollection<QuotationInfo>();
            
            if (type == "Standard")
                StandardQuotation = true;
            else if (type == "International")
                InternationalQuotation = true;

            LoadData();

            if (LoginUserAuthentication == "User")
                MyWind.btnSave.Visibility = Visibility.Hidden;
            else 
                MyWind.btnSave.Visibility = Visibility.Visible;
        }

        public QuotationVM(FrmQuotation Wind)
        {
            MyWind = Wind;
            Items = new ObservableCollection<QuotationInfo>();
            QuotationId = 0; QuotationNo = ""; InsNormal = 0; InsIntl = 0; TaxName = ""; TaxPercentage = 0;
            StandardQuotation = true;

            LoadData();

            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        public long QuotationId { get { return GetValue(() => QuotationId); } set { SetValue(() => QuotationId, value); OnPropertyChanged("QuotationId"); } }
        public string QuotationNo { get { return GetValue(() => QuotationNo); } set { SetValue(() => QuotationNo, value); OnPropertyChanged("QuotationNo"); } }
        public bool StandardQuotation { get { return GetValue(() => StandardQuotation); } set { SetValue(() => StandardQuotation, value); OnPropertyChanged("StandardQuotation"); if (StandardQuotation) QuotationType = "Standard"; } }
        public bool InternationalQuotation { get { return GetValue(() => InternationalQuotation); } set { SetValue(() => InternationalQuotation, value); OnPropertyChanged("InternationalQuotation"); if(InternationalQuotation) QuotationType = "International"; } }
        public decimal InsNormal { get { return GetValue(() => InsNormal); } set { SetValue(() => InsNormal, value); OnPropertyChanged("InsNormal"); } }
        public decimal InsIntl { get { return GetValue(() => InsIntl); } set { SetValue(() => InsIntl, value); OnPropertyChanged("InsIntl"); } }
        public string TaxName { get { return GetValue(() => TaxName); } set { SetValue(() => TaxName, value); OnPropertyChanged("TaxName"); } }
        public decimal TaxPercentage { get { return GetValue(() => TaxPercentage); } set { SetValue(() => TaxPercentage, value); OnPropertyChanged("TaxPercentage"); } }

        public string QuotationType;

        private ObservableCollection<QuotationInfo> _Items = new ObservableCollection<QuotationInfo>();
        public ObservableCollection<QuotationInfo> Items { get { return _Items; } set { _Items = value; OnPropertyChanged("Items"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Quotation Number":
                        {
                            dt = GetDataTable($"SELECT DISTINCT QuotationId As ColOneId, QuotationNum As ColOneText FROM tblQuotation" +
                                $" WHERE QuotationType = '{QuotationType}' AND ToDate IS NULL" +
                                $" ORDER BY QuotationNum");

                            ChildWnd.FrmListSingleCol_Closed += (r => { QuotationId = Convert.ToInt32(r.Id); QuotationNo = r.ColOneText; LoadData(); });
                            ChildWnd.SingleColWindShow(dt, "Quotation No. Selection");
                        }
                        break;
                    case "Standard Quotation": { QuotationId = 0; QuotationNo = ""; QuotationType = "Standard"; } LoadData(); break;
                    case "International Quotation": { QuotationId = 0; QuotationNo = ""; QuotationType = "International"; } LoadData(); break;
                    case "Save":
                        {
                            string auth = StrAuthenticateType();
                            if (!string.IsNullOrEmpty(auth) && auth != "User")
                            {
                                SaveData();
                            }
                            else { MessageBox.Show("ADMIN only, your authentication type: '" + auth + "'", "Warning"); }
                        } break;
                    case "Print": PrintReport(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void Rights()
        {
            string position = LoginUserAuthentication;
            position = position.ToUpper();
            if (position == "MANAGER" || position == "ADMIN" || position == "IT" || position == "PROGRAMMER")
            {
                MyWind.txtQuotationNo.Visibility = Visibility.Visible;
                MyWind.btnQuotationNo.Visibility = Visibility.Visible;
                MyWind.btnSave.Visibility = Visibility.Visible;
            }
            else
            {
                MyWind.txtQuotationNo.Visibility = Visibility.Hidden;
                MyWind.btnQuotationNo.Visibility = Visibility.Hidden;
                MyWind.btnSave.Visibility = Visibility.Hidden;
            }
        }

        private void LoadData()
        {
            Items.Clear();
            Rights();
            DataTable dt, dtZones, dtProduct;
            SqlDataReader dr;

            dtProduct = new DataTable();
            dtProduct = GetDataTable($"SELECT ItemDesc FROM tblServiceProduct WHERE Status = 1 ORDER BY ItemDesc");

            dtZones = new DataTable();
            dtZones = GetDataTable($"SELECT ZoneCode, ZoneDesc, ZoneType FROM tblZone WHERE RTRIM(ZoneType) = '{(StandardQuotation ? "LOCAL" : "INT")}' ORDER BY ZoneCode");

            if (QuotationNo.Trim() != "")
            {
                QuotationId = 0; InsNormal = 0; InsIntl = 0; TaxName = ""; TaxPercentage = 0;
                dr = GetDataReader($"SELECT * FROM tblQuotation WHERE QuotationNum = '{QuotationNo}' AND ToDate IS NULL");
            }
            else
            {
                QuotationId = 0; QuotationNo = ""; InsNormal = 0; InsIntl = 0; TaxName = ""; TaxPercentage = 0;
                dr = GetDataReader($"SELECT * FROM tblQuotation WHERE QuotationType = '{QuotationType}' AND ToDate IS NULL");
            }

            if (dr.HasRows)
            {
                dr = GetDataReader($"SELECT * FROM tblQuotation WHERE QuotationType = '{QuotationType}' AND ToDate IS NULL");
                if (dr.Read())
                {
                    QuotationId = Convert.ToInt64(dr["QuotationId"]);
                    QuotationNo = dr["QuotationNum"].ToString();
                    InsNormal = Convert.ToDecimal(dr["InsuranceNormal"]);
                    InsIntl = Convert.ToDecimal(dr["InsuranceInternational"]);
                    TaxName = dr["TaxTitle"].ToString();
                    TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);

                    dr.Close();
                    using (dt = new DataTable())
                    {
                        dt = GetDataTable($"SELECT A.*, B.ZoneDesc FROM tblQuotationDet A" +
                            $" JOIN tblZone B ON RTRIM(ToZone) = RTRIM(B.ZoneCode)" +
                            $" WHERE QuotationId = {QuotationId} AND ToDate IS NULL ORDER BY B.ZoneDesc, A.ShipmentType");
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                Items.Add(new QuotationInfo 
                                { 
                                    FromZone = dt.Rows[i]["FromZone"].ToString().Trim(),
                                    ToZone = dt.Rows[i]["ToZone"].ToString().Trim(), 
                                    ToZoneDesc = dt.Rows[i]["ZoneDesc"] != DBNull.Value && dt.Rows[i]["ZoneDesc"].ToString().Trim() != "" ? dt.Rows[i]["ZoneDesc"].ToString().Trim() : dt.Rows[i]["ToZone"].ToString().Trim(), 
                                    ServiceCode = dt.Rows[i]["ShipmentType"].ToString().Trim(), 
                                    FirstWeight = Convert.ToDecimal(dt.Rows[i]["FirstWeight"]), 
                                    FirstRate = Convert.ToDecimal(dt.Rows[i]["FirstRate"]), 
                                    AddWeight = Convert.ToDecimal(dt.Rows[i]["AddWeight"]), 
                                    AddRate = Convert.ToDecimal(dt.Rows[i]["AddRate"]), 
                                    SurCharge = Convert.ToDecimal(dt.Rows[i]["SurCharge"]), 
                                    QuotationDetId = Convert.ToInt64(dt.Rows[i]["QuotationDetId"]) 
                                });
                            }
                        }
                        else
                        {
                            for (int i = 0; i < dtZones.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtProduct.Rows.Count; j++)
                                {
                                    Items.Add(new QuotationInfo { FromZone = "LOCAL", ToZone = dt.Rows[i]["ZoneCode"].ToString().Trim(), ToZoneDesc = dt.Rows[i]["ZoneDesc"].ToString().Trim(), ServiceCode = dt.Rows[j]["ItemDesc"].ToString().Trim(), FirstWeight = 0, FirstRate = 0, AddWeight = 0, AddRate = 0, SurCharge = 0, QuotationDetId = 0 });
                                }
                            }
                        }
                    }
                }
                dr.Close();

            }

            if (QuotationId > 0)
            {
                MyWind.txtQuotationNo.IsEnabled = false;
            }
            else
            {
                MyWind.txtQuotationNo.IsEnabled = true;
            }
        }

        private void PrintReport()
        {
            DataTable dt;
            try
            {
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT C.QuotationNum As QuotationNo, C.InsuranceNormal As InsNormal, C.InsuranceInternational As InsIntl, C.TaxTitle As TaxName, C.TaxPercentage As TaxPercentage, A.FromZone, A.ToZone, B.ZoneDesc, A.ShipmentType, A.FirstWeight, A.FirstRate, A.AddWeight, A.AddRate, A.SurCharge, A.QuotationDetId FROM tblQuotationDet A" +
                        $" JOIN tblZone B ON RTRIM(ToZone) = RTRIM(B.ZoneCode)" +
                        $" JOIN tblQuotation C ON A.QuotationId = C.QuotationId" +
                        $" WHERE A.QuotationId = {QuotationId} AND A.ToDate IS NULL ORDER BY B.ZoneDesc, A.ShipmentType, A.FirstWeight, A.FirstRate");

                    DataTable DTCompany = new DataTable();
                    DTCompany = GetDataTable($"SELECT CompId, CompCode, CompName As Name, CompType As SName, TaxNo As GSTno, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode As PostalCode, PhoneNum As PhoneNo, Fax As FaxNo, EMail, CreateBy, CreateDate, ModifyBy, ModifyDate, Status FROM tblStationProfile" +
                        $" WHERE RTRIM(CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");

                    FrmReportViewer RptViewerGen = new FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompAddress, "Quotation", dt, "Quotation", dt, "Quotation", dt, "RPTQuotation.rdlc", "", RptViewerGen.RptViewer, true, false);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
            }
            catch (Exception Ex) { }
        }

        private void SaveData()
        {
            if (LoginUserAuthentication != "User")
            {
                if (QuotationId > 0)
                {
                    SqlDataReader dr = GetDataReader($"SELECT * FROM tblQuotation WHERE QuotationId = {QuotationId} AND ToDate IS NULL");
                    if (dr.HasRows)
                    {
                        ExecuteQuery($"UPDATE tblQuotation SET TaxTitle = '{TaxName}', TaxPercentage = {TaxPercentage}, InsuranceNormal = {InsNormal}, InsuranceInternational = {InsIntl}, ModifyBy = {LoginUserId}, ModifyDate = GETDATE() WHERE QuotationId = {QuotationId} AND ToDate IS NULL");

                        for (int i = 0; i < Items.Count; i++)
                        {
                            ExecuteQuery($"UPDATE tblQuotationDet SET SlNo = {i + 1}, FromZone = '{Items[i].FromZone}', ToZone = '{Items[i].ToZone}', ShipmentType = '{Items[i].ServiceCode}', FirstWeight = {Items[i].FirstWeight}, FirstRate = {Items[i].FirstRate}, AddWeight = {Items[i].AddWeight}, AddRate = {Items[i].AddRate}, SurCharge = {Items[i].SurCharge}, FromDate = GETDATE() WHERE QuotationId = {QuotationId} AND QuotationDetId = {Items[i].QuotationDetId} AND ToDate IS NULL");
                        }
                    }
                }
                LoadData();
            }
        }
        public class QuotationInfo : AMGenFunction
        {
            public string FromZone { get { return GetValue(() => FromZone); } set { SetValue(() => FromZone, value); OnPropertyChanged("FromZone"); } }
            public string ToZone { get { return GetValue(() => ToZone); } set { SetValue(() => ToZone, value); OnPropertyChanged("ToZone"); } }
            public string ToZoneDesc { get { return GetValue(() => ToZoneDesc); } set { SetValue(() => ToZoneDesc, value); OnPropertyChanged("ToZoneDesc"); } }
            public string ServiceCode { get { return GetValue(() => ServiceCode); } set { SetValue(() => ServiceCode, value); OnPropertyChanged("ServiceCode"); } }
            public decimal FirstWeight { get { return GetValue(() => FirstWeight); } set { SetValue(() => FirstWeight, value); OnPropertyChanged("FirstWeight"); } }
            public decimal FirstRate { get { return GetValue(() => FirstRate); } set { SetValue(() => FirstRate, value); OnPropertyChanged("FirstRate"); } }
            public decimal AddWeight { get { return GetValue(() => AddWeight); } set { SetValue(() => AddWeight, value); OnPropertyChanged("AddWeight"); } }
            public decimal AddRate { get { return GetValue(() => AddRate); } set { SetValue(() => AddRate, value); OnPropertyChanged("AddRate"); } }
            public decimal SurCharge { get { return GetValue(() => SurCharge); } set { SetValue(() => SurCharge, value); OnPropertyChanged("SurCharge"); } }
            public long QuotationDetId { get { return GetValue(() => QuotationDetId); } set { SetValue(() => QuotationDetId, value); OnPropertyChanged("QuotationDetId"); } }
        }
    }
}