﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class PromoCodeVM : CNPrintVM
    {
        FrmPromoCode Wndow;
        MobileRefCashSalesVM cashSalesVM;

        public PromoCodeVM(FrmPromoCode frmPromo, MobileRefCashSalesVM cashSalesVM_)
        {
            Wndow = frmPromo;
            cashSalesVM = cashSalesVM_;
            Reset();
        }

        private ICommand _CommandGen;
        private string _promocode;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }

        public string PromoCode { get { return _promocode; } set { _promocode = value; OnPropertyChanged("PromoCode"); } }
        public string CurrentPromoCode { get { return GetValue(() => CurrentPromoCode); } set { SetValue(() => CurrentPromoCode, value); OnPropertyChanged("CurrentPromoCode"); } }
        public string PromoTitle { get { return GetValue(() => PromoTitle); } set { SetValue(() => PromoTitle, value); OnPropertyChanged("PromoTitle"); if (PromoTitle == "") { Wndow.btnSave.IsEnabled = false; } else { Wndow.btnSave.IsEnabled = true; } } }
        public bool IsPromoPercentage { get { return GetValue(() => IsPromoPercentage); } set { SetValue(() => IsPromoPercentage, value); OnPropertyChanged("IsPromoPercentage"); } }
        public decimal PromoPercentage { get { return GetValue(() => PromoPercentage); } set { SetValue(() => PromoPercentage, value); OnPropertyChanged("PromoPercentage"); } }
        public decimal PromoPercentageCal { get { return GetValue(() => PromoPercentageCal); } set { SetValue(() => PromoPercentageCal, value); OnPropertyChanged("PromoPercentageCal"); } }
        public string PromoShipmentType { get { return GetValue(() => PromoShipmentType); } set { SetValue(() => PromoShipmentType, value); OnPropertyChanged("PromoShipmentType"); } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "ViewPromo":
                        {
                            dt = GetDataTable($"SELECT PromoID As ColOneId,PromoTitle As ColOneText, PromoDesc As ColTwoText, PromoCode As ColThreeText from tblallpromotion WHERE (GETDATE() > FromDate AND GETDATE() < ToDate) AND status = 1");
                            ChildWnd.FrmListCol3_Closed += (r => { PromoCode = r.ColThreeText.Trim(); ApplyPromotion(); });
                            ChildWnd.ThreeColWindShow(dt, "Promo Code Selection", "Title", "Description", "Code");                            
                        }
                        break;
                    case "Save": { SaveData(); }
                        break;
                    case "Cancel":
                        {
                            this.CloseWind();
                        }
                        break;
                    case "ApplyPromotion": ApplyPromotion(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void SaveData()
        {
            if (IsValid())
            {
                cashSalesVM.NameOrTitle = PromoTitle;
                cashSalesVM.PromoCode = CurrentPromoCode;
                cashSalesVM.DiscPercentage = IsPromoPercentage ? PromoPercentage + "%" : "RM" + PromoPercentage;
                cashSalesVM.IsPromoCode = true;
                cashSalesVM.PromoShipmentType = PromoShipmentType;
                this.CloseWind();
            }
            else
            {
                MessageBox.Show("Promocode not valid", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Reset();
            }
        }

        private void ApplyPromotion()
        {
            CurrentPromoCode = "";
            if (!string.IsNullOrEmpty(PromoCode))
            {
                //SqlDataReader dr = GetDataReader($"SELECT PromoCode, Percentage, IsPercentage, PromoTitle, ShipmentType, Status FROM tblAllPromotion WHERE RTRIM(PromoCode) = '{PromoCode.Trim()}' AND (FromDate <= GETDATE() AND ToDate >= GETDATE()) AND (RTRIM(StnCode) = 'All' or RTRIM(StnCode) = '{MyCompanyCode.Trim()}') AND (UPPER(RTRIM(ShipmentType)) = 'ALL')");
                SqlDataReader dr = GetDataReader($"SELECT PromoCode, Percentage, IsPercentage, PromoTitle, ShipmentType, Status FROM tblAllPromotion WHERE RTRIM(PromoCode) = '{PromoCode.Trim()}' AND (FromDate <= GETDATE() AND ToDate >= GETDATE()) AND (RTRIM(StnCode) = 'All' or RTRIM(StnCode) = '{MyCompanyCode.Trim()}')");
                if (dr.Read())
                {
                    PromoShipmentType = dr["ShipmentType"].ToString();

                    if (PromoCode == "DEC12S20" && !IsSpecial())
                    {
                        MessageBox.Show("This Promo can use from 12noon - 1pm", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Reset();
                    }
                    else
                    {
                        if (Convert.ToBoolean(dr["Status"]) == true)
                        {
                            PromoTitle = dr["PromoTitle"].ToString();
                            IsPromoPercentage = Convert.ToBoolean(dr["IsPercentage"]);
                            if(!IsPromoPercentage)
                            {
                                Wndow.lblValueOrPercentage.Content = "Promo Value : ";
                            }
                            else
                            {
                                Wndow.lblValueOrPercentage.Content = "Promo Percentage : ";
                            }
                            //PromoPercentage = IsPromoPercentage == true ? Convert.ToDecimal(dr["Percentage"]) : 0;
                            PromoPercentage = Convert.ToDecimal(dr["Percentage"]);
                            //PromoPercentageCal = Convert.ToDecimal(dr["Percentage"]);
                        }
                        else
                        {
                            dr.Close();
                            dr = GetDataReader($"SELECT FromDate, ToDate FROM tblAllPromotion WHERE RTRIM(PromoCode) = '{PromoCode.Trim()}'");
                            if (dr.Read())
                                MessageBox.Show("Promocode Expired, it's only valid from " + Convert.ToDateTime(dr["FromDate"]).ToString("dd-MMM-yyyy hh:mm tt") + " to " + Convert.ToDateTime(dr["ToDate"]).ToString("dd-MMM-yyyy hh:mm tt"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            else
                                MessageBox.Show("Promocode not valid", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            dr.Close();
                        }
                    }
                }
                else
                {
                    dr.Close();
                    DataTable dt;
                    using (dt = new DataTable())
                    {
                        dt = GetDataTable($"SELECT * FROM tblAllPromotion WHERE RTRIM(PromoCode) = '{PromoCode.Trim()}'");
                        if (dt.Rows.Count > 0)
                        {
                            bool applicabletoWIC = false; string wicpromo = ""; bool applicabletoshipment = false; string shipmnts = ""; DateTime promotefromdate = DateTime.Now; DateTime promotetodate = DateTime.Now;
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                shipmnts = string.IsNullOrEmpty(shipmnts) ? (shipmnts + dt.Rows[i]["ShipmentType"].ToString().Trim()) : (shipmnts + "," + dt.Rows[i]["ShipmentType"].ToString().Trim());
                                if (dt.Rows[i]["ShipmentType"].ToString().Trim().ToUpper() == "ALL")
                                {
                                    applicabletoshipment = true; promotefromdate = Convert.ToDateTime(dt.Rows[i]["FromDate"]); promotetodate = Convert.ToDateTime(dt.Rows[i]["ToDate"]);
                                }
                                if (dt.Rows[i]["StnCode"].ToString().Trim() != MyCompanyCode.Trim())
                                {
                                    applicabletoWIC = true; wicpromo = dt.Rows[i]["StnCode"].ToString().Trim();
                                }
                            }

                            if (applicabletoWIC == true)
                            {
                                MessageBox.Show("Promocode valid only to " + wicpromo, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                if (applicabletoshipment == true)
                                    MessageBox.Show("Promocode not valid, it's only valid from " + promotefromdate.ToString("dd-MMM-yyyy hh:mm tt") + " to " + promotetodate.ToString("dd-MMM-yyyy hh:mm tt"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                                else
                                    MessageBox.Show("Promocode valid only to " + shipmnts, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else MessageBox.Show("Promocode not valid", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PromoCode = ""; PromoTitle = ""; PromoPercentage = 0;
                }
                dr.Close();

                CurrentPromoCode = PromoCode;
            }
            else MessageBox.Show("Enter Promocode to apply discount", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private bool IsSpecial()
        {
            bool proceed = false;
            //Only Apply on 12 dec 2020 to 14 dec 2020. 12.12 Sales

            DateTime ServerDate = DateTime.Now;

            SqlDataReader dr = GetDataReader($"SELECT GETDATE() as ServerDate");
            if (dr.Read())
            {
                ServerDate = Convert.ToDateTime(dr["ServerDate"]);
            }
            dr.Close();

            if (PromoCode == "DEC12S20")
            {
                TimeSpan starttime = new TimeSpan(12, 0, 0); //12 noon
                TimeSpan endtime = new TimeSpan(13, 00, 0); //1 PM

                if (ServerDate.TimeOfDay >= starttime & ServerDate.TimeOfDay <= endtime)
                {
                    proceed = true;
                }
            }

            return proceed;
        }

        private bool IsValid()
        {
            bool result = false;
            //from tblallpromotion WHERE (GETDATE() > FromDate AND GETDATE() < ToDate) AND status = 1
            SqlDataReader dr = GetDataReader($"SELECT PromoCode from tblallpromotion WHERE PromoCode = '{PromoCode.Trim()}' and (GETDATE() > FromDate AND GETDATE() < ToDate) AND status = 1");
            if (dr.Read())
            {
                result = true;
            }
            return result;
        }


        private void Reset()
        {
            Wndow.btnSave.IsEnabled = false;
            Wndow.txtPromoCode.Focus();
            Wndow.lblValueOrPercentage.Content = "Promo Percentage : ";
            PromoTitle = ""; PromoPercentage = 0; PromoCode = ""; CurrentPromoCode = "";
        }
    }
}