﻿using SkynetCashSales.General;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Drawing.Text;
using System.Windows.Media.Imaging;
using SkynetCashSales.View.Reports;
using System.Windows;
using Bytescout.BarCode;
using SkynetCashSales.Reports;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using SkynetCashSales.ViewModel.Reports;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class RDLCCNPrintVM : AMGenFunction
    {
        private static DataTable DTCNList = new DataTable();
        public RDLCCNPrintVM() { }
        public void RDLCPrintORPreview(string csno, int csid,string option)
        {
            CSaleNo = csno;
            CSalesID = csid;
            DataTable DTMas = new DataTable();
            DTMas = GetDataTable($"SELECT num.CNNum, CNNumBarcode = num.CNNum, gen.CSalesNo, gen.CSalesDate, gen.ShipmentType, gen.DestCode, gen.Pieces, gen.Weight, gen.VLength, gen.VBreadth, gen.VHeight, gen.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, gen.InsShipmentDesc, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.CUserID, CUserName = u.UserName, gen.CSalesTotal,gen.Status FROM tblCashSales gen JOIN tblCSaleCNNums num ON gen.CSalesNo = CSalesNo JOIN tblUserMaster u ON gen.CUserID = u.UserId" +
                $" WHERE gen.Status = 1 AND gen.CSalesNo = '{CSaleNo}' ");
            //var querymas = (from gen in CSalesEntity.tblCashSales
            //                join num in CSalesEntity.tblCSaleCNNums on gen.CSalesNo equals num.CSalesNo
            //                join u in CSalesEntity.tblUserMasters on gen.CUserID equals u.UserId
            //                where gen.Status == true && gen.CSalesNo == CSaleNo
            //                select new { num.CNNum, CNNumBarcode = num.CNNum, gen.CSalesNo, gen.CSalesDate, gen.ShipmentType, gen.DestCode, gen.Pieces, gen.Weight, gen.VLength, gen.VBreadth, gen.VHeight, gen.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, gen.InsShipmentDesc, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.CUserID, CUserName = u.UserName, gen.CSalesTotal,gen.Status });
            //DTMas = LINQToDataTable(querymas);
            
            if (DTMas.Rows.Count > 0)
            {
                Barcode bc = new Barcode(SymbologyType.Code128);
                bc.RegistrationName = "demo";
                bc.RegistrationKey = "demo";
                bc.DrawCaption = false;

                RptDS.CNPrintDetailsDataTable RptDt = new RptDS.CNPrintDetailsDataTable();
                if (DTMas.Rows.Count > 0)
                {
                    DataRow _row;
                    for (int i = 0; i < DTMas.Rows.Count; i++)
                    {
                        //gen.InsShipmentValue, gen.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CUserID, CUserName = u.UserName, gen.CSalesTotal
                        _row = RptDt.NewRow();
                        _row["CNNum"] = DTMas.Rows[i][0];
                        byte[] DRSBytes = Encoding.ASCII.GetBytes(DTMas.Rows[i][1].ToString());
                        bc.Value = DTMas.Rows[i][1].ToString();
                        _row["CNNumBarcode"] = bc.GetImageBytesPNG(); 
                        _row["CSalesNo"] = DTMas.Rows[i][2];
                        _row["CSalesDate"] = DTMas.Rows[i][3];
                        _row["ShipmentType"] = DTMas.Rows[i][4];
                        _row["DestCode"] = DTMas.Rows[i][5];
                        _row["Pieces"] = DTMas.Rows[i][6];
                        _row["Weight"] = DTMas.Rows[i][7];
                        _row["VLength"] = DTMas.Rows[i][8];
                        _row["VBreadth"] = DTMas.Rows[i][9];
                        _row["VHeight"] = DTMas.Rows[i][10];
                        _row["VolWeight"] = DTMas.Rows[i][11];
                        _row["Price"] = DTMas.Rows[i][12];
                        _row["ShipmentGSTValue"] = DTMas.Rows[i][13];
                        _row["ShipmentTotal"] = DTMas.Rows[i][14];
                        _row["StationaryTotal"] = DTMas.Rows[i][15];
                        _row["IsInsuranced"] = DTMas.Rows[i][16];
                        _row["InsShipmentValue"] = DTMas.Rows[i][17];
                        _row["InsShipmentDesc"] = DTMas.Rows[i][18];
                        _row["InsShipmentValue"] = DTMas.Rows[i][19];
                        _row["InsuranceTotal"] = DTMas.Rows[i][20];
                        _row["InsGSTValue"] = DTMas.Rows[i][21];
                        _row["CUserID"] = DTMas.Rows[i][22];
                        _row["CUserName"] = DTMas.Rows[i][23];
                        _row["CSalesTotal"] = DTMas.Rows[i][24];
                        _row["Status"] = DTMas.Rows[i][25];
                        RptDt.Rows.Add(_row);
                    }
                }

                //var querycompany = (from a in CSalesEntity.tblStationProfiles where a.CompCode == MyCompanyCode && a.Status == true select new { a.CompId, a.CompCode, Name = a.CompName,GSTNo="", SName = a.CompType, a.RegNum, a.IATACode, a.HQCode, Address1 = a.Address1 ?? "", Address2 = a.Address2 ?? "", Address3 = a.Address3 ?? "", City = a.City ?? "", State = a.State ?? "", Country = a.Country ?? "", PostalCode = a.ZipCode ?? "", PhoneNo = a.PhoneNum ?? "", FaxNo = a.Fax ?? "", EMail = a.EMail ?? "", a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status }).ToList();
                DataTable DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName,GSTNo='', SName = a.CompType, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,''), Address3 = ISNULL(a.Address3,''), City = ISNULL(a.City,''), State =  ISNULL(a.State,''), Country = ISNULL(a.Country,''), PostalCode = ISNULL(a.ZipCode,''), PhoneNo = ISNULL(a.PhoneNum,''), FaxNo = ISNULL(a.Fax,''), EMail = ISNULL(a.EMail,''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a WHERE  a.CompCode = '{MyCompanyCode}' AND a.Status = 1 ");

                LocalReport report = new LocalReport();
                ReportDataSource RptDS1 = new ReportDataSource();
                ReportDataSource RptDS2 = new ReportDataSource();
                RptDS1.Name = "CompanyMas";
                RptDS1.Value = DTCompany;
                report.DataSources.Add(RptDS1);

                RptDS2.Name = "CNPrintDetails";
                RptDS2.Value = RptDt;
                report.DataSources.Add(RptDS2);
                
                
                report.ReportEmbeddedResource = "RPTCNPrint.rdlc";
                report.Refresh();
                //report.PrintToPrinter();
                DataTable dtempty = new DataTable();
                string RPTName = "RPTAWB.rdlc", RPTHeaderName = "";
                string strDTMas = "CNPrintDetails", strDTTran = "", strDTHelp = "";
                FrmReportViewer RptViewerGen = new FrmReportViewer();
                RptViewerGen.Owner = Application.Current.MainWindow;
                ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompany, strDTMas, RptDt, strDTTran, dtempty, strDTHelp, dtempty, RPTName, RPTHeaderName, RptViewerGen.RptViewer, true, false);
                RptViewerGen.DataContext = VM;
                RptViewerGen.Show();
            }
        }
        public void PrintORPreview(string csno, int csid, DataTable dtcnlist, string option)
        {
            CSaleNo = csno;
            CSalesID = csid;
            if (dtcnlist.Rows.Count > 0)
            {
                DTCNList = dtcnlist;
                Print(option, DTCNList);
            }
        }
        //..[Required(ErrorMessage = "CSNo is Required")]
        public string CSaleNo
        {
            get { return GetValue(() => CSaleNo); }
            set { SetValue(() => CSaleNo, value); OnPropertyChanged("CSaleNo"); }
        }
        //..[Required(ErrorMessage = "CSalesID is Required")]
        public int CSalesID
        {
            get { return GetValue(() => CSalesID); }
            set { SetValue(() => CSalesID, value); OnPropertyChanged("CSalesID"); }
        }
        //..[Required(ErrorMessage = "ConsignorCode is Required")]
        public string ConsignorCode
        {
            get { return GetValue(() => ConsignorCode); }
            set { SetValue(() => ConsignorCode, value); OnPropertyChanged("ConsignorCode"); }
        }
        //..[Required(ErrorMessage = "ConsigneeCode is Required")]
        public string ConsigneeCode
        {
            get { return GetValue(() => ConsigneeCode); }
            set { SetValue(() => ConsigneeCode, value); OnPropertyChanged("ConsigneeCode"); }
        }
        //..[Required(ErrorMessage = "ConsignmentNote is Required")]
        public string ConsignmentNote
        {
            get { return GetValue(() => ConsignmentNote); }
            set { SetValue(() => ConsignmentNote, value); OnPropertyChanged("ConsignmentNote"); }
        }
        private Bitmap GetBarCodeBitMap(String data)
        {
            Bitmap barcode = new Bitmap(1, 1);
            Font ThreeofNine = new Font("Free 3 of 9", 60, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            Graphics graphics = Graphics.FromImage(barcode);
            SizeF dataSize = graphics.MeasureString(data,ThreeofNine);
            barcode = new Bitmap(barcode, dataSize.ToSize());
            graphics = Graphics.FromImage(barcode);
            graphics.Clear(System.Drawing.Color.White);
            graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;
            graphics.DrawString(data, ThreeofNine, new SolidBrush(System.Drawing.Color.Black), 0, 0);
            graphics.Flush();
            ThreeofNine.Dispose();
            graphics.Dispose();
            return barcode;
        }
        private BitmapImage GetBarCodeBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
        public Image GetBarCodeImage(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(System.Drawing.Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(System.Drawing.Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();

                    Convert.ToBase64String(byteImage);
                    return bitMap;
                }             
            }
        }
        private System.Windows.Controls.Image barImage = new System.Windows.Controls.Image();
        public byte[] FillBarCode(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();                     
                    return byteImage;
                    //barImage.Source = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                }
                //plBarCode.Controls.Add(imgBarCode);
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public String FillBarCodeStr(string cnNumber)
        {
            string barCode = cnNumber;

            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("IDAutomationHC39M", 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();

                    Convert.ToBase64String(byteImage);
                    return Convert.ToBase64String(byteImage);
                }
                //plBarCode.Controls.Add(imgBarCode);
            }
        }
        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        private void PrintData(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 8);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);

                if (!string.IsNullOrEmpty(ConsignorCode) && string.IsNullOrEmpty(ConsigneeCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT TOP 1 a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile FROM tblConsignor a WHERE a.ConsignorCode = '{ConsignorCode}'");
                    //var query = (from a in CSalesEntity.tblConsignors
                    //             where a.ConsignorCode == ConsignorCode
                    //             select new { a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile }).FirstOrDefault();
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            // LINE. 0
                            startX = 350; startY = 5; Offset = 5;
                            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                            int width = 400; int height = 35;
                            string cnn = "*" + ConsignmentNote.Trim() + "*";
                            Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                            graphics.DrawImage(CNImages1, startX, 0, width, height);
                            startX = 550; startY = 42; Offset = 5;
                            graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 1
                            startX = 1; startY = 30; Offset = 40;
                            _Font1 = new Font("Arial", 10);
                            graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);
                            startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 2
                            Offset = Offset + 15; startX = 1; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                            startX = 250; graphics.DrawString("", _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 3
                            Offset = Offset + 12; startX = 1; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 4
                            Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 5
                            Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 6
                            Offset = Offset + 24; startX = 50; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        
                    }
                    dr.Close(); 
                }
                else if (string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT TOP 1 a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile FROM tblConsignee a WHERE a.ConsigneeCode = '{ConsigneeCode}' ");
                    //var query = (from a in CSalesEntity.tblConsignees
                    //             where a.ConsigneeCode == ConsigneeCode
                    //             select new { a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile }).FirstOrDefault();
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            startX = 350; startY = 5; Offset = 5;
                            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                            int width = 400; int height = 35;
                            string cnn = "*" + ConsignmentNote.Trim() + "*";
                            Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                            graphics.DrawImage(CNImages1, startX, 0, width, height);
                            startX = 550; startY = 42; Offset = 5;
                            graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 1
                            startY = 30;
                            _Font1 = new Font("Arial", 10);
                            Offset = 70; startX = 420; graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 2
                            Offset = Offset + 15; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 3
                            Offset = Offset + 12; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 4
                            Offset = Offset + 12; graphics.DrawString( dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 5
                            Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                            // LINE. 6
                            Offset = Offset + 55; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                            startX = 670; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        }
                        // LINE. 0
                       
                    }dr.Close();
                }
                else if (!string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT TOP 1 ConsignorName = b.Name, ConsignorAddress1 = b.Add1, ConsignorAddress2 = b.Add2, ConsignorAddress3 = b.City + '-' + b.PostalCode, ConsignorAddress4 = b.State + ',' + b.Country, ConsignorContact = b.Phone + '/' + b.Mobile, ConsignorDeptCode = '', ConsignorCode = b.ConsignorCode, ConsigneeName = a.Name, ConsigneeAddress1 = a.Add1, ConsigneeAddress2 = a.Add2, ConsigneeAddress3 = a.City + '-' + a.PostalCode, ConsigneeAddress4 = a.State + ',' + a.Country, ConsigneeContact = a.Phone + '/' + a.Mobile, ConsigneeDeptCode = '', ConsigneeCode = a.ConsigneeCode, ConsigneeAttention = '' FROM tblConsignee a JOIN tblConsignor b ON a.ConsignorCode = b.ConsignorCode  where a.ConsigneeCode = '{ConsigneeCode}' AND b.ConsignorCode = '{ConsignorCode}' ");
                    //var query = (from a in CSalesEntity.tblConsignees 
                    //             join b in CSalesEntity.tblConsignors on a.ConsignorCode equals b.ConsignorCode
                    //             where a.ConsigneeCode == ConsigneeCode && b.ConsignorCode == ConsignorCode
                    //             select new
                    //             {
                    //                 ConsignorName = b.Name,
                    //                 ConsignorAddress1 = b.Add1,
                    //                 ConsignorAddress2 = b.Add2,
                    //                 ConsignorAddress3 = b.City+"-"+b.PostalCode,
                    //                 ConsignorAddress4 = b.State+","+b.Country,
                    //                 ConsignorContact = b.Phone+"/"+b.Mobile,
                    //                 ConsignorDeptCode = "",
                    //                 ConsignorCode = b.ConsignorCode,
                    //                 ConsigneeName = a.Name,
                    //                 ConsigneeAddress1 = a.Add1,
                    //                 ConsigneeAddress2 = a.Add2,
                    //                 ConsigneeAddress3 = a.City + "-" +a.PostalCode,
                    //                 ConsigneeAddress4 = a.State + "," + a.Country,
                    //                 ConsigneeContact = a.Phone + "/" + a.Mobile,
                    //                 ConsigneeDeptCode = "",
                    //                 ConsigneeCode = a.ConsigneeCode,
                    //                 ConsigneeAttention = ""
                    //             }).FirstOrDefault();

                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            // LINE. 0
                            startX = 350; startY = 5; Offset = 5;
                            Bitmap bp = GetBarCodeBitMap(ConsignmentNote);
                            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                            Image CNImage = Image.FromFile(@System.Windows.Forms.Application.StartupPath + "\\tempCN.jpg");
                            // Working width for A4 fine is int width = 600; int height = 100;
                            int width = 400; int height = 35;
                            string cnn = "*" + ConsignmentNote.Trim() + "*";
                            Image CNImages1 = b.Encode(type, cnn, Color.Black, Color.White, width, height);
                            graphics.DrawImage(CNImages1, startX, 0, width, height);
                            startX = 550; startY = 42; Offset = 5;
                            graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                            // LINE. 1
                            startX = 1; startY = 30; Offset = 40;
                            _Font1 = new Font("Arial", 10);
                            graphics.DrawString(dr["ConsignorName"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                                             // SHIPPER NAME
                            startX = 250; graphics.DrawString(dr["ConsignorCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                              // SHIPPER ACCNO

                            // LINE. 2
                            startX = 1; Offset = Offset + 15; graphics.DrawString(dr["ConsignorAddress1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD1
                            startX = 250; graphics.DrawString(dr["ConsignorDeptCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // SHIPPER DEPTCODE

                            // LINE. 3
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD2
                            startX = 420; graphics.DrawString(dr["ConsigneeName"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // CONSIGNEE NAME

                            // LINE. 4
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);           // SHIPPER ADD3
                            startX = 420; graphics.DrawString(dr["ConsigneeAddress1"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD1

                            // LINE. 5
                            startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);          // SHIPPER ADD4
                            startX = 420; graphics.DrawString(dr["ConsigneeAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD2

                            // LINE. 6
                            Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);         // CONSIGNEE ADD3

                            // LINE. 7
                            Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);        // CONSIGNEE ADD4
                            startX = 50; graphics.DrawString(dr["ConsignorContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // SHIPPER PHONE

                            // LINE. 8
                            Offset = Offset + 55; startX = 470; graphics.DrawString(dr["ConsigneeAttention"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                    // CONSIGNEE ATTENTION
                            startX = 670; graphics.DrawString(dr["ConsigneeContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                        }
                                                                                   // CONSIGNEE PHONE
                    }dr.Close();
                }
            }
        }
        public void Print(string OutputLocation)
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);

            PaperSize psize = new PaperSize("Custom", 200, 100);
            ps.DefaultPageSettings.PaperSize = psize;

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;

            pdoc.DefaultPageSettings.PaperSize.Height = 320;
            pdoc.DefaultPageSettings.PaperSize.Width = 820;
            pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;
            pdoc.PrintPage += new PrintPageEventHandler(PrintData);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            pp.Document = pdoc;

            if (OutputLocation == "Print")
            {
                System.Windows.Forms.DialogResult result = pd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                    pdoc.Print();
            }
            else
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }
        }
        public void Print(string OutputLocation,DataTable dtcn)
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            PrintDocument pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Arial", 15);
            System.Windows.Forms.PrintPreviewDialog pp = new System.Windows.Forms.PrintPreviewDialog();
            int numberofrows = dtcn.Rows.Count;
            if (numberofrows > 0)
            {                
                    PaperSize psize = new PaperSize("Custom", 820, 400 * numberofrows);
                    ps.DefaultPageSettings.PaperSize = psize;

                    pd.Document = pdoc;
                    pd.Document.DefaultPageSettings.PaperSize = psize;

                    pdoc.DefaultPageSettings.PaperSize.Height = 400 * numberofrows;//320
                    pdoc.DefaultPageSettings.PaperSize.Width = 820;
                    pdoc.DefaultPageSettings.PrinterSettings.Copies = 1;

                    if (OutputLocation == "TestPrint")
                    {
                        pdoc.PrintPage += new PrintPageEventHandler(TestPDTBC);
                    }
                    else { pdoc.PrintPage += new PrintPageEventHandler(PrintDataTable); }
                    pp.Document = pdoc;
            }
            if (OutputLocation == "Print")
            {
                System.Windows.Forms.DialogResult result = pd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                    pdoc.Print();
            }
            else
            {
                System.Windows.Forms.DialogResult result = pp.ShowDialog();
            }

        }
        private void PrintDataTable(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;
                for (int i = 0; i < DTCNList.Rows.Count; i++)
                {
                    string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                    ConsignmentNote = DTCNList.Rows[i][0].ToString();
                    ShipmentType = DTCNList.Rows[i][1].ToString();
                    DestCode = DTCNList.Rows[i][2].ToString();
                    Pieces = DTCNList.Rows[i][3].ToString();
                    Weight = DTCNList.Rows[i][4].ToString();
                    VL = DTCNList.Rows[i][5].ToString();
                    VB = DTCNList.Rows[i][6].ToString();
                    VH = DTCNList.Rows[i][7].ToString();
                    VolW = DTCNList.Rows[i][8].ToString();
                    ReceivedBy = DTCNList.Rows[i][9].ToString();
                    Date = DTCNList.Rows[i][10].ToString();
                    IsIns = DTCNList.Rows[i][11].ToString();
                    DestStation = DTCNList.Rows[i][12].ToString();
                    InsShipmntvalue = DTCNList.Rows[i][13].ToString();
                    InsChrgs = DTCNList.Rows[i][14].ToString();
                    CourierChrgs = DTCNList.Rows[i][15].ToString();
                    TotalChrgs = DTCNList.Rows[i][16].ToString();
                    DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                    decimal len, brd, hgt, vol;
                    len = Math.Round(Convert.ToDecimal(VL), 0);
                    brd = Math.Round(Convert.ToDecimal(VB), 0);
                    hgt = Math.Round(Convert.ToDecimal(VH), 0);
                    vol = Math.Round(Convert.ToDecimal(VolW), 0);
                    // LINE. 0
                    startX = 430; startY = Y + 15;
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                    int width = 280; int height = 30; //int width = 350; int height = 30;
                    string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                    Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                    graphics.DrawImage(CNImages1, startX, startY, width, height);
                    graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 350, startY + 7);
                    startX = 500; startY = Y + 45; Offset = 1;
                    graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                    //LINE DEST Code
                    startX = 720; startY = Y + 45; Offset = 30;
                    Font _Font10 = new Font("Arial", 18);
                    graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);

                    if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                    {
                        startX = 1; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }
                    else
                    {
                        startX = 76; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }

                    //LINE Vol Weight
                    startY = Y; Offset = 234;
                    startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Pieces
                    startX = 24; startY = Y; Offset = 267;
                    graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Weight
                    startY = Y; Offset = 271;
                    startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
                    startY = Y; Offset = 267;
                    startX = 230; graphics.DrawString("RM:" + TotalChrgs, _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 320; graphics.DrawString(CSDT.ToShortTimeString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font2, _SolidBrush, startX, startY + Offset);

                    if (IsIns == "True" || IsIns == "1")
                    {
                        //LINE Insurance Yes
                        startX = 21; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double inshpamt = Convert.ToDouble(InsShipmntvalue);
                        double inschrgs = Convert.ToDouble(InsChrgs);
                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 124; startY = Y; Offset = 299;
                        graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 308;
                        graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 317;
                        graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 326;
                        graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        //LINE Insurance No
                        startX = 201; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 215; startY = Y; Offset = 315;
                        graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 215; startY = Y; Offset = 324;
                        graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    Y += 400;
                }
            }
        }
        private void TestPDTBC(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            //Font font = new Font("Courier New", 10);
            Font font = new Font("Arial", 10);
            float fontHeight = font.GetHeight();
            int startX = 1;
            int startY = 30;
            int Offset = 40;

            Font _Font1 = new Font("Arial", 10);
            Font _Font2 = new Font("Arial", 6);
            SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
            int Y = 0;
            for (int i = 0; i < DTCNList.Rows.Count; i++)
            {
                ConsignmentNote = DTCNList.Rows[i][0].ToString();
                // LINE. 0
                startX = 470; startY = Y + 5; Offset = 5;
                BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                int width = 190; int height = 28; //int width = 350; int height = 30;
                string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 1
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 300; height = 30;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 2
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 190; height = 20;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 3
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 200; height = 20;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 4
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 150; height = 10;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 5
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 175; height = 30;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
                // LINE. 6
                Y = startY;
                startX = 200; startY = Y + 5; Offset = 5;
                width = 150; height = 20;
                graphics.DrawImage(CNImages1, startX, startY, width, height);
                startX = 370; startY = Y + 35; Offset = 5;
                graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);
            }
        }
        private void PrintDataTablewWORKED(object sender, PrintPageEventArgs e)
        {
            if (CSalesID > 0)
            {
                Graphics graphics = e.Graphics;
                //Font font = new Font("Courier New", 10);
                Font font = new Font("Arial", 10);
                float fontHeight = font.GetHeight();
                int startX = 1;
                int startY = 30;
                int Offset = 40;

                Font _Font0 = new Font("Arial", 12);
                Font _Font1 = new Font("Arial", 10);
                Font _Font2 = new Font("Arial", 6);
                Font _Font3 = new Font("Arial", 4);
                SolidBrush _SolidBrush = new SolidBrush(System.Drawing.Color.Black);
                int Y = 0;
                for (int i = 0; i < DTCNList.Rows.Count; i++)
                {
                    string ShipmentType, DestCode, Pieces, Weight, VL, VB, VH, VolW, ReceivedBy, Date, IsIns, DestStation, InsShipmntvalue, InsChrgs, CourierChrgs, TotalChrgs;
                    ConsignmentNote = DTCNList.Rows[i][0].ToString();
                    ShipmentType = DTCNList.Rows[i][1].ToString();
                    DestCode = DTCNList.Rows[i][2].ToString();
                    Pieces = DTCNList.Rows[i][3].ToString();
                    Weight = DTCNList.Rows[i][4].ToString();
                    VL = DTCNList.Rows[i][5].ToString();
                    VB = DTCNList.Rows[i][6].ToString();
                    VH = DTCNList.Rows[i][7].ToString();
                    VolW = DTCNList.Rows[i][8].ToString();
                    ReceivedBy = DTCNList.Rows[i][9].ToString();
                    Date = DTCNList.Rows[i][10].ToString();
                    IsIns = DTCNList.Rows[i][11].ToString();
                    DestStation = DTCNList.Rows[i][12].ToString();
                    InsShipmntvalue = DTCNList.Rows[i][13].ToString();
                    InsChrgs = DTCNList.Rows[i][14].ToString();
                    CourierChrgs = DTCNList.Rows[i][15].ToString();
                    TotalChrgs = DTCNList.Rows[i][16].ToString();
                    DateTime CSDT = Convert.ToDateTime(Date != null && Date != "" ? Date : DateTime.Now.ToString());
                    decimal len, brd, hgt, vol;
                    len = Math.Round(Convert.ToDecimal(VL), 0);
                    brd = Math.Round(Convert.ToDecimal(VB), 0);
                    hgt = Math.Round(Convert.ToDecimal(VH), 0);
                    vol = Math.Round(Convert.ToDecimal(VolW), 0);
                    // LINE. 0
                    startX = 430; startY = Y + 15; 
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;//CODE39
                    int width = 280; int height = 30; //int width = 350; int height = 30;
                    string cnn = ConsignmentNote.Trim();// "*" + ConsignmentNote.Trim() + "*";
                    Image CNImages1 = b.Encode(type, cnn);//(type, cnn, Color.Black, Color.White, width, height);
                    graphics.DrawImage(CNImages1, startX, startY, width, height);
                    graphics.DrawString(ConsignmentNote, _Font0, _SolidBrush, 350, startY+7);
                    startX = 500; startY = Y + 45; Offset = 1;
                    graphics.DrawString(ConsignmentNote, _Font1, _SolidBrush, startX, startY + Offset);

                    //LINE DEST Code
                    startX = 720; startY = Y + 45; Offset = 30;
                    Font _Font10 = new Font("Arial", 18);
                    graphics.DrawString(DestStation, _Font10, _SolidBrush, startX, startY + Offset);

                    if (ShipmentType == "DOCUMENT" || ShipmentType == "DOCUMENTS")
                    {
                        startX = 1; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }
                    else
                    {
                        startX = 76; startY = Y; Offset = 203;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);
                    }

                    //LINE Vol Weight
                    startY = Y; Offset = 234;
                    startX = 90; graphics.DrawString(vol.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 225; graphics.DrawString(len.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 255; graphics.DrawString(brd.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 290; graphics.DrawString(hgt.ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 330; graphics.DrawString(ReceivedBy, _Font2, _SolidBrush, startX, startY + Offset);
                    
                    //LINE Pieces
                    startX =24; startY = Y; Offset = 267;
                    graphics.DrawString(Pieces, _Font2, _SolidBrush, startX, startY + Offset);

                    //LINE Weight
                    startY = Y; Offset = 271;
                    startX = 90; graphics.DrawString(Weight, _Font2, _SolidBrush, startX, startY + Offset);
                    startY = Y; Offset = 267;
                    startX = 230; graphics.DrawString("RM:"+TotalChrgs, _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 320; graphics.DrawString(CSDT.ToShortTimeString(), _Font2, _SolidBrush, startX, startY + Offset);
                    startX = 360; graphics.DrawString(CSDT.ToShortDateString(), _Font2, _SolidBrush, startX, startY + Offset);

                    if (IsIns == "True" || IsIns == "1")
                    {
                        //LINE Insurance Yes
                        startX = 21; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double inshpamt = Convert.ToDouble(InsShipmntvalue);
                        double inschrgs = Convert.ToDouble(InsChrgs);
                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 124; startY = Y; Offset = 299;
                        graphics.DrawString(inshpamt.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 308;
                        graphics.DrawString(inschrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 317;
                        graphics.DrawString(cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 110; startY = Y; Offset = 326;
                        graphics.DrawString(ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }
                    else
                    {
                        //LINE Insurance No
                        startX = 201; startY = Y; Offset = 280;
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 7, startY + Offset + 15, startX + 11, startY + Offset + 18);
                        graphics.DrawLine(new Pen(Color.FromArgb(13, 13, 13), 1), startX + 11, startY + Offset + 18, startX + 18, startY + Offset + 10);

                        double cchrgs = Convert.ToDouble(CourierChrgs);
                        double ctotal = Convert.ToDouble(TotalChrgs);
                        startX = 215; startY = Y; Offset = 315;
                        graphics.DrawString("Courier Charges RM:" + cchrgs.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                        startX = 215; startY = Y; Offset = 324;
                        graphics.DrawString("Total Charges   RM:" + ctotal.ToString("0.00"), _Font3, _SolidBrush, startX, startY + Offset);
                    }

                    if (!string.IsNullOrEmpty(ConsignorCode) && string.IsNullOrEmpty(ConsigneeCode))
                    {
                        SqlDataReader dr = GetDataReader($"SELECT TOP 1 a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile  FROM tblConsignor WHERE a.ConsignorCode = '{ConsignorCode}' ");
                        //var query = (from a in CSalesEntity.tblConsignors
                        //             where a.ConsignorCode == ConsignorCode
                        //             select new { a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone + '/' + a.Mobile }).FirstOrDefault();
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                // LINE. 1
                                startX = 1; startY = Y + 30; Offset = 40;
                                _Font1 = new Font("Arial", 10);
                                graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);
                                //startX = 250; graphics.DrawString(query.ConsignorCode, _Font1, _SolidBrush, startX, startY + Offset);

                                // LINE. 2
                                Offset = Offset + 15; startX = 1; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                                startX = 250; graphics.DrawString("", _Font1, _SolidBrush, startX, startY + Offset);

                                // LINE. 3
                                Offset = Offset + 12; startX = 1; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 4
                                Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 5
                                Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 6
                                Offset = Offset + 24; startX = 50; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                            }
                           
                        }dr.Close();
                    }
                    else if (string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                    {
                        SqlDataReader dr = GetDataReader($"SELECT a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone ,Mobile= a.Mobile  FROM tblConsignee WHERE a.ConsigneeCode = '{ConsigneeCode}'");
                        //var query = (from a in CSalesEntity.tblConsignees
                        //             where a.ConsigneeCode == ConsigneeCode
                        //             select new { a.Name, a.ConsignorCode, Add1 = a.Add1, Add2 = a.Add2, Add3 = a.City + '-' + a.PostalCode, Add4 = a.State + ',' + a.Country, Contact = a.Phone ,Mobile= a.Mobile }).FirstOrDefault();
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                // LINE. 1
                                startY = Y + 30;
                                _Font1 = new Font("Arial", 10);
                                Offset = 70; startX = 420; graphics.DrawString(dr["Name"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);

                                // LINE. 2
                                Offset = Offset + 15; graphics.DrawString(dr["Add1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 3
                                Offset = Offset + 12; graphics.DrawString(dr["Add2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 4
                                Offset = Offset + 12; graphics.DrawString(dr["Add3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 5
                                Offset = Offset + 12; graphics.DrawString(dr["Add4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);

                                // LINE. 6
                                Offset = Offset + 55; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                                startX = 670; graphics.DrawString(dr["Contact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                                // LINE. 7
                                Offset = Offset + 65; startX = 470; graphics.DrawString("", _Font2, _SolidBrush, startX, startY + Offset);
                                startX = 670; graphics.DrawString(dr["Mobile"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);
                            }
                            
                        }dr.Close();
                    }
                    else if (!string.IsNullOrEmpty(ConsignorCode) && !string.IsNullOrEmpty(ConsigneeCode))
                    {
                        SqlDataReader dr = GetDataReader($"SELECT TOP 1 ConsignorName = d.Name, ConsignorAddress1 = d.Add1, ConsignorAddress2 = d.Add2, ConsignorAddress3 = d.City + '-' + d.PostalCode, ConsignorAddress4 = d.State + ',' + d.Country, ConsignorContact = d.Phone, ConsignorMobile = d.Mobile, ConsignorDeptCode = '', ConsignorCode = d.ConsignorCode, ConsigneeName = a.Name, ConsigneeAddress1 = a.Add1, ConsigneeAddress2 = a.Add2, ConsigneeAddress3 = a.City + '-' + a.PostalCode, ConsigneeAddress4 = a.State + ',' + a.Country, ConsigneeContact = a.Phone + '/' + a.Mobile, ConsigneeMobile = a.Mobile, ConsigneeDeptCode = '', ConsigneeCode = a.ConsigneeCode, ConsigneeAttention = '' FROM tblConsignee a JOIN tblConsignor d ON a.ConsignorCode = d.ConsignorCode" +
                            $" WHERE a.ConsigneeCode = '{ConsigneeCode}' AND d.ConsignorCode = '{ConsignorCode}'");
                        //var query = (from a in CSalesEntity.tblConsignees
                        //             join d in CSalesEntity.tblConsignors on a.ConsignorCode equals d.ConsignorCode
                        //             where a.ConsigneeCode == ConsigneeCode && d.ConsignorCode == ConsignorCode
                        //             select new
                        //             {
                        //                 ConsignorName = d.Name,
                        //                 ConsignorAddress1 = d.Add1,
                        //                 ConsignorAddress2 = d.Add2,
                        //                 ConsignorAddress3 = d.City + "-" + d.PostalCode,
                        //                 ConsignorAddress4 = d.State + "," + d.Country,
                        //                 ConsignorContact = d.Phone ,
                        //                 ConsignorMobile=d.Mobile,
                        //                 ConsignorDeptCode = "",
                        //                 ConsignorCode = d.ConsignorCode,
                        //                 ConsigneeName = a.Name,
                        //                 ConsigneeAddress1 = a.Add1,
                        //                 ConsigneeAddress2 = a.Add2,
                        //                 ConsigneeAddress3 = a.City + "-" + a.PostalCode,
                        //                 ConsigneeAddress4 = a.State + "," + a.Country,
                        //                 ConsigneeContact = a.Phone + "/" + a.Mobile,
                        //                 ConsigneeMobile = a.Mobile,
                        //                 ConsigneeDeptCode = "",
                        //                 ConsigneeCode = a.ConsigneeCode,
                        //                 ConsigneeAttention = ""
                        //             }).FirstOrDefault();

                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                // LINE. 1
                                startX = 1; startY = Y + 30; Offset = 40;
                                _Font1 = new Font("Arial", 10);
                                graphics.DrawString(dr["ConsignorName"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                                             // SHIPPER NAME
                                                                                                                                                                                                    //startX = 250; graphics.DrawString(query.ConsignorCode, _Font1, _SolidBrush, startX, startY + Offset);                                                              // SHIPPER ACCNO

                                // LINE. 2
                                startX = 1; Offset = Offset + 15; graphics.DrawString(dr["ConsignorAddress1"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD1
                                startX = 250; graphics.DrawString(dr["ConsignorDeptCode"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // SHIPPER DEPTCODE

                                // LINE. 3
                                startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD2
                                startX = 420; graphics.DrawString(dr["ConsigneeName"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                               // CONSIGNEE NAME

                                // LINE. 4
                                startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD3
                                startX = 420; graphics.DrawString(dr["ConsigneeAddress1"].ToString(), _Font1, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD1

                                // LINE. 5
                                startX = 1; Offset = Offset + 12; graphics.DrawString(dr["ConsignorAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                       // SHIPPER ADD4
                                startX = 420; graphics.DrawString(dr["ConsigneeAddress2"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                           // CONSIGNEE ADD2

                                // LINE. 6
                                Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress3"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                     // CONSIGNEE ADD3

                                // LINE. 7
                                Offset = Offset + 12; startX = 420; graphics.DrawString(dr["ConsigneeAddress4"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                     // CONSIGNEE ADD4
                                startX = 50; graphics.DrawString(dr["ConsignorContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                             // SHIPPER PHONE

                                // LINE. 8
                                Offset = Offset + 55; startX = 470; graphics.DrawString(dr["ConsigneeAttention"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                    // CONSIGNEE ATTENTION
                                startX = 670; graphics.DrawString(dr["ConsigneeContact"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                                            // CONSIGNEE PHONE
                                Offset = Offset + 65; startX = 670; graphics.DrawString(dr["ConsigneeMobile"].ToString(), _Font2, _SolidBrush, startX, startY + Offset);                                      // CONSIGNEE PHONE
                            }
                            
                        }dr.Close();
                    }
                    Y += 400;
                }
            }
        }
    }
}
