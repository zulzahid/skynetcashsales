﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class QuantityVM : CNPrintVM
    {
        FrmQuantity Wndow;
        MobileRefCashSalesVM cashSalesVM;
        public event Action<ListInfo> Closed;

        public QuantityVM(FrmQuantity frmQuantity, MobileRefCashSalesVM cashSalesVM_, string stationeryname, int quantity)
        {
            Wndow = frmQuantity;
            cashSalesVM = cashSalesVM_;
            StationeryName = stationeryname;
            StationeryQuantity = "";
            CurrentQuantity = quantity;
            Wndow.txtQuantity.Focus();
        }

        private ICommand _CommandGen;
        private string _promocode;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        public string StationeryName { get { return GetValue(() => StationeryName); } set { SetValue(() => StationeryName, value); OnPropertyChanged("StationeryName"); } }
        public string StationeryQuantity { get { return GetValue(() => StationeryQuantity); } set { SetValue(() => StationeryQuantity, value); OnPropertyChanged("StationeryQuantity"); } }
        int CurrentQuantity = 0;

        //private ListInfo _SelectedItem = new ListInfo();
        //public ListInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Save":
                        {
                            CloseWind();
                            if (Closed != null && StationeryQuantity != "")
                            {
                                var Items = new ListInfo() { Name = StationeryName, Quantity = Convert.ToInt32(StationeryQuantity) };
                                Closed(Items);
                            }
                            else
                            {
                                ExecuteCommandGen("Cancel");
                            }
                        }
                        break;
                    case "Cancel":
                        {
                            CloseWind();
                            if (Closed != null)
                            {
                                var Items = new ListInfo() { Name = StationeryName, Quantity = CurrentQuantity };
                                Closed(Items);
                            }
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        public class ListInfo
        {
            public string Name { get; set; }
            public int Quantity { get; set; }
        }
    }
}