﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class StaffCodeVM : CNPrintVM
    {        
        FrmStaffCode Wndow;
        MobileRefCashSalesVM cashSalesVM;

        public StaffCodeVM(FrmStaffCode frmStaff, MobileRefCashSalesVM cashSalesVM_)
        {
            Wndow = frmStaff;
            cashSalesVM = cashSalesVM_;
            Reset();
        }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        //private ObservableCollection<StaffCodeInfo> _StaffcodeDetails = new ObservableCollection<StaffCodeInfo>();
        //public ObservableCollection<StaffCodeInfo> StaffCodeDetails { get { return _StaffcodeDetails; } set { _StaffcodeDetails = value; } }

        public string StaffCode { get { return GetValue(() => StaffCode); } set { SetValue(() => StaffCode, value); OnPropertyChanged("StaffCode"); CheckStaffisActive(); } }
        public string StaffName { get { return GetValue(() => StaffName); } set { SetValue(() => StaffName, value); OnPropertyChanged("StaffName"); /*if (StaffName == "") { Wndow.btnSave.IsEnabled = false; } else { Wndow.btnSave.IsEnabled = true; }*/ } }



        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Save":
                        {
                            cashSalesVM.NameOrTitle = StaffName + "|" + StaffCode;
                            cashSalesVM.DiscPercentage = "20%";
                            cashSalesVM.IsStaffCode = true;
                            this.CloseWind();
                        }
                        break;
                    case "SaveName":
                        {
                            StaffName = Wndow.txtName.Text;
                            ExecuteQuery($"INSERT INTO tblStaff(StaffName, StaffCode, StaffTypeId, SupervisorId, ICNum, SOCSO, EPFNum, TaxFileNum, Salary, JoinedDate, LeftDate, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, MobNum, PagerNum, Fax, EMail, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                        $" VALUES('{StaffName}', '{StaffCode}', '60', '0', '', '', '', '', '0', '', NULL, '', '', '{""}', '', '', '', '', '', '{""}', '{""}', '', '', '{LoginUserId}', GETDATE(), 0, NULL, '1')");
                            Wndow.btnSave.IsEnabled = true;                            
                            Wndow.lblName.Visibility = Visibility.Hidden;
                            Wndow.txtName.Visibility = Visibility.Hidden;
                            Wndow.btnSaveName.Visibility = Visibility.Hidden;
                        }
                        break;
                    case "Cancel":
                        {
                            this.CloseWind();
                        }
                        break;
                    case "StaffCode":
                        {
                            Wndow.lblName.Visibility = Visibility.Hidden;
                            Wndow.txtName.Visibility = Visibility.Hidden;
                            Wndow.btnSaveName.Visibility = Visibility.Hidden;
                            dt = GetDataTable($"SELECT StaffId As ColOneId, StaffName As ColOneText, StaffCode As ColTwoText FROM tblStaff WHERE Status = 1 ORDER BY StaffName");
                            ChildWnd.FrmListCol2_Closed += (r => { StaffName = r.ColOneText.Trim(); StaffCode = r.ColTwoText.Trim(); checkStaffDiscount(); });
                            ChildWnd.TwoColWindShow(dt, "Staff Selection", "Staff", "Code");                            
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void CheckStaffisActive()
        {
            if(StaffCode.Length >= 6 )
            {
                bool isUpdateinactive = false;
                SqlDataReader dr;
                
                dr = GetDataReader($"SELECT StaffId, StaffName, StaffCode As ColTwoText FROM tblStaff WHERE StaffCode = '{StaffCode}' and Status = 1 ORDER BY StaffName");
                if (dr.Read())
                {
                    //if (CheckStaffCode())
                    //{
                    StaffName = dr["StaffName"].ToString();
                    Wndow.btnSave.IsEnabled = true;
                    //}
                    //else
                    //{
                    //    isUpdateinactive = true;
                    //    MessageBox.Show("This Staff Code is not valid or not active");
                    //    Wndow.btnSave.IsEnabled = false;

                    //}
                }
                else
                {
                    if (CheckStaffCode())
                    {
                        MessageBox.Show("Staff does not register in this station! Please insert name");
                        Wndow.lblName.Visibility = Visibility.Visible;
                        Wndow.txtName.Visibility = Visibility.Visible;
                        Wndow.btnSaveName.Visibility = Visibility.Visible;                        
                    }
                    else
                    {
                        MessageBox.Show("This Staff Code is not valid or not active");
                        Wndow.btnSave.IsEnabled = false;
                        StaffCode = "";
                        StaffName = "";
                    }
                }
                dr.Close();


                if (isUpdateinactive)
                {
                    ExecuteQuery($"UPDATE tblStaff SET Status = 0 Where StaffCode = '{StaffCode}'");
                    StaffCode = "";
                    StaffName = "";
                }
            }
            else
            {
                Wndow.btnSave.IsEnabled = false;
                StaffName = "";
                Wndow.lblName.Visibility = Visibility.Hidden;
                            Wndow.txtName.Visibility = Visibility.Hidden;
                            Wndow.btnSaveName.Visibility = Visibility.Hidden;
            }
        }

        private void checkStaffDiscount()
        {
            if (!string.IsNullOrEmpty(StaffCode))
            {
                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT * FROM tblCashSales WHERE StaffCode = '{StaffCode}' AND (CSalesDate BETWEEN '{new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("yyyy-MM-dd HH:mm")}' AND '{DateTime.Now.Date.AddHours(23.99).ToString("yyyy-MM-dd HH:mm:ss")}')");
                    if (dt.Rows.Count >= 3)
                    {
                        MessageBox.Show("Sorry already this Staff used staff discount 3 times. Next month you can use this discount", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        StaffCode = "";
                    }
                }
            }
        }

        private bool CheckStaffCode()
        {
            bool res = false;
            try
            {
                if (CheckForInternetConnection("http://hrms.skynet.com.my:8021/login.php") == false)
                {
                    return true;
                }
                else
                {

                    StaffInfo Staff;
                    Staff = new StaffInfo(); Staff.StaffCode = StaffCode;
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                    {
                        HttpClient client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage response = client.PostAsJsonAsync($"http://hrms.skynet.com.my:8021/getStaffStatus.php/", Staff).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var query = response.Content.ReadAsStringAsync().Result;
                            if (query.ToString().Contains("Active"))
                                res = true;
                        }
                        else
                        {
                            error_log.errorlog("[MobileRefCashSalesVM.CheckStaffCode] Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Internet connection failure", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception Ex) { General.error_log.errorlog("API in Check StaffCode : " + Ex.ToString()); }
            return res;
        }

        public static bool CheckForInternetConnection(string URL)
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead(URL))
                    return true;
            }
            catch
            {
                return false;
            }
        }

        public class StaffInfo
        {
            public string StaffCode { get; set; }
        }

        private void Reset()
        {
            Wndow.btnSave.IsEnabled = false;
            Wndow.lblName.Visibility = Visibility.Hidden;
            Wndow.txtName.Visibility = Visibility.Hidden;
            Wndow.btnSaveName.Visibility = Visibility.Hidden;
            Wndow.txtStaffCode.Focus();
        }
    }
}

