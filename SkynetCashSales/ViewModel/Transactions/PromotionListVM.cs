﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class PromotionListVM : CNPrintVM
    {
        public PromotionListVM(FrmPromotionList frm)
        {
            PromotionList = new ObservableCollection<PromotionListModel>();
            LoadGrid();
        }
        
        private DataGridCellInfo _focusedcell;
        public DataGridCellInfo FocusedCell
        {
            get { return _focusedcell; }
            set { _focusedcell = value; OnPropertyChanged("FocusedCell"); }
        }
        private PromotionListModel _selecteditem = new PromotionListModel();
        public PromotionListModel SelectedItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("SelectedItem"); }
        }
        public PromotionListModel CurrentItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("CurrentItem"); }
        }
        private string _selectedindex;
        public string SelectedIndex
        {
            get { return _selectedindex; }
            set { _selectedindex = value; OnPropertyChanged("SelectedIndex"); }
        }
        private ObservableCollection<PromotionListModel> _PromotionList = new ObservableCollection<PromotionListModel>();
        public ObservableCollection<PromotionListModel> PromotionList
        {
            get { return _PromotionList; }
            set { _PromotionList = value; OnPropertyChanged("PromotionList"); }
        }
        public ObservableCollection<PromotionListModel> ItemInfo
        {
            get { return _PromotionList; }
            set { _PromotionList = value; OnPropertyChanged("ItemInfo"); }
        }
         private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "New": break;
                    case "Go": LoadGrid(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
        private void LoadGrid()
        {
            try
            {
                PromotionList.Clear();
                DataTable csDt = new DataTable();
                using (csDt = new DataTable())
                {
                    csDt = GetDataTable($"SELECT Distinct a.StnCode, a.ShipmentType, a.PromoTitle, a.PromoCode, a.PromoDesc, a.Percentage, a.IsPercentage, a.FromDate, a.ToDate FROM tblAllPromotion a WHERE a.Status = 1 ORDER BY FromDate");
                    if (csDt.Rows.Count > 0)
                    {
                        for (int i = 0; i < csDt.Rows.Count; i++)
                        {
                            PromotionList.Add(new PromotionListModel
                            {
                                SlNo = (i + 1).ToString(),
                                StnCode = csDt.Rows[i][0].ToString(),
                                ShipmentType = csDt.Rows[i][1].ToString(),
                                PromoTitle = csDt.Rows[i][2].ToString(),
                                PromoCode = csDt.Rows[i][3].ToString(),
                                PromoDesc = csDt.Rows[i][4].ToString(),
                                Percentage = csDt.Rows[i][5].ToString(),
                                IsPercentage = Convert.ToBoolean(csDt.Rows[i][6]),
                                FromDate = csDt.Rows[i][7].ToString(),
                                ToDate = csDt.Rows[i][8].ToString()
                            });
                        }
                    }
                }
            }
            catch (Exception Ex) { }
        }
        public class PromotionListModel : AMGenFunction
        {
            private string _SlNo;
            public string SlNo
            {
                get { return _SlNo; }
                set { _SlNo = value; OnPropertyChanged("SlNo"); }
            }
            private string _StnCode;
            public string StnCode
            {
                get { return _StnCode; }
                set { _StnCode = value; OnPropertyChanged("StnCode"); }
            }

            private string _ShipmentType;
            public string ShipmentType
            {
                get { return _ShipmentType; }
                set { _ShipmentType = value; OnPropertyChanged("ShipmentType"); }
            }
            private string _PromoTitle;
            public string PromoTitle
            {
                get { return _PromoTitle; }
                set { _PromoTitle = value; OnPropertyChanged("PromoTitle"); }
            }
            private string _PromoCode;
            public string PromoCode
            {
                get { return _PromoCode; }
                set { _PromoCode = value; OnPropertyChanged("PromoCode"); }
            }
            private string _PromoDesc;
            public string PromoDesc
            {
                get { return _PromoDesc; }
                set { _PromoDesc = value; OnPropertyChanged("PromoDesc"); }
            }
            private string _Percentage;
            public string Percentage
            {
                get { return _Percentage; }
                set { _Percentage = value; OnPropertyChanged("Percentage"); }
            }
            private bool _IsPercentage;
            public bool IsPercentage
            {
                get { return _IsPercentage; }
                set { _IsPercentage = value; OnPropertyChanged("IsPercentage"); }
            }
            private string _FromDate;
            public string FromDate
            {
                get { return _FromDate; }
                set { _FromDate = value; OnPropertyChanged("FromDate"); }
            }
            private string _ToDate;
            public string ToDate
            {
                get { return _ToDate; }
                set { _ToDate = value; OnPropertyChanged("ToDate"); }
            }
        }
    }
}
