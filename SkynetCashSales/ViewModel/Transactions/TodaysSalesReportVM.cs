﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class TodaysSalesReportVM : CNPrintVM
    {
        private FrmTodaysSalesReport MyWind;
        private static string errormsg = "";
        private BackgroundWorker bg = new BackgroundWorker();
        
        public TodaysSalesReportVM(FrmTodaysSalesReport wind)
        {
            MyWind = wind;
            WebDb = new CommunicateWebDb();
            AWBList = new ObservableCollection<AWBInfo>();
            SumOfAWBs = new ObservableCollection<AWBInfo>();
            PaymentModes = new ObservableCollection<AWBInfo>();
            LoadData();
        }

        private DateTime ClosedDateTime;
        private void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MyWind.gif.Visibility = Visibility.Hidden;
        }

        private void bg_DoWork(object sender, DoWorkEventArgs e)
        {
            WebDb.StartProcess();
        }

        private ObservableCollection<AWBInfo> _AWBList = new ObservableCollection<AWBInfo>();
        public ObservableCollection<AWBInfo> AWBList { get { return _AWBList; } set { _AWBList = value; } }

        private AWBInfo _SelectedAWB = new AWBInfo();
        public AWBInfo SelectedAWB { get { return _SelectedAWB; } set { _SelectedAWB = value; OnPropertyChanged("SelectedAWB"); } }

        private ObservableCollection<AWBInfo> _PaymentModes = new ObservableCollection<AWBInfo>();
        public ObservableCollection<AWBInfo> PaymentModes { get { return _PaymentModes; } set { _PaymentModes = value; } }

        public string CSalesNo { get { return GetValue(() => CSalesNo); } set { SetValue(() => CSalesNo, value); OnPropertyChanged("CSalesNo"); } }
        public string PaymentMode { get { return GetValue(() => PaymentMode); } set { SetValue(() => PaymentMode, value); OnPropertyChanged("PaymentMode"); } }
        public string PaymentRef { get { return GetValue(() => PaymentRef); } set { SetValue(() => PaymentRef, value); OnPropertyChanged("PaymentRef"); } }

        private ObservableCollection<AWBInfo> _SumOfAWBs = new ObservableCollection<AWBInfo>();
        public ObservableCollection<AWBInfo> SumOfAWBs { get { return _SumOfAWBs; } set { _SumOfAWBs = value; } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            try
            {
                switch (Obj.ToString())
                {
                    case "Print": PrintData(); break;
                    case "Load Payment Mode":
                        {
                            if (SelectedAWB != null && SelectedAWB.AWBNumber != null)
                            {
                                DataTable dt;
                                string Cost_ = "", OtherCharges_ = "", StationeryAmt_ = "", InsuranceAmt_ = "", DiscAmt_ = "", BeforeTax_ = "", TaxAmt_ = "", RoundingAmt_ = "", TotalAmt_ = "", RASurcharge_;
                                using (dt = new DataTable())
                                {
                                    dt = GetDataTable($"SELECT distinct a.PaymentMode, a.PaymentRefNo, (a.ShipmentTotal - a.OtherCharges - a.InsuranceTotal) as ShipmentTotal, a.OtherCharges, a.StationaryTotal, a.InsuranceTotal, IIF(a.DiscAmount != 0.00, a.DiscAmount, a.StaffDisAmt) as DiscAmount, (a.CSalesTotal - a.ShipmentGSTValue - a.RoundingAmt) as BeforeTax, a.ShipmentGSTValue as TaxAmt, ISNULL(a.RASurcharge, 0) as RASurcharge, a.RoundingAmt, a.CSalesTotal as TotalAmt " +
                                        $"FROM tblCashSales a left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo where a.CSalesNo = '{SelectedAWB.CSalesNo}'");

                                    Cost_ = Convert.ToDecimal(dt.Rows[0]["ShipmentTotal"]).ToString("0.00");
                                    OtherCharges_ = Convert.ToDecimal(dt.Rows[0]["OtherCharges"]).ToString("0.00");
                                    StationeryAmt_ = Convert.ToDecimal(dt.Rows[0]["StationaryTotal"]).ToString("0.00");
                                    InsuranceAmt_ = Convert.ToDecimal(dt.Rows[0]["InsuranceTotal"]).ToString("0.00");
                                    DiscAmt_ = Convert.ToDecimal(dt.Rows[0]["DiscAmount"]).ToString("0.00");
                                    BeforeTax_ = Convert.ToDecimal(dt.Rows[0]["BeforeTax"]).ToString("0.00");
                                    TaxAmt_ = Convert.ToDecimal(dt.Rows[0]["TaxAmt"]).ToString("0.00");
                                    RASurcharge_ = Convert.ToDecimal(dt.Rows[0]["RASurcharge"]).ToString("0.00");
                                    RoundingAmt_ = Convert.ToDecimal(dt.Rows[0]["RoundingAmt"]).ToString("0.00");
                                    TotalAmt_ = Convert.ToDecimal(dt.Rows[0]["TotalAmt"]).ToString("0.00");
                                }

                                MyWind.lblShpmntCost2.Content = string.Format("{0:0.00}", Cost_);
                                MyWind.lblOtherCharges.Content = string.Format("{0:0.00}", OtherCharges_);
                                MyWind.lblStationarySum2.Content = string.Format("{0:0.00}", StationeryAmt_);
                                MyWind.lblInsurenceCharges2.Content = string.Format("{0:0.00}", InsuranceAmt_);
                                MyWind.lblTotalDisAmt.Content = string.Format("{0:0.00}", DiscAmt_);
                                MyWind.lblTotAmtBeforeTax.Content = string.Format("{0:0.00}", BeforeTax_);
                                MyWind.lblTax.Content = "Tax Amt [+]";
                                MyWind.lblTaxAmt.Content = string.Format("{0:0.00}", TaxAmt_);
                                MyWind.lblRASurcharge.Content = string.Format("{0:0.00}", RASurcharge_);
                                MyWind.lblRoundingAmt.Content = string.Format("{0:0.00}", RoundingAmt_);
                                MyWind.lblTotalAmtAfterTax.Content = string.Format("{0:0.00}", TotalAmt_);
                                MyWind.lblTotalAmtAfterTax1.Content = "RM" + string.Format("{0:0.00}", TotalAmt_);
                                

                                CSalesNo = SelectedAWB.CSalesNo;
                                MyWind.lblAWBNumber.Content = SelectedAWB.AWBNumber + "@" + CSalesNo;
                                PaymentMode = SelectedAWB.PaymentMode;
                                PaymentRef = SelectedAWB.PaymentRef;
                                MyWind.canvsPaymentMode.Visibility = Visibility.Visible;
                            }
                        }
                        break;
                    case "Update Payment Mode":
                        {
                            if (IsValid)
                            {
                                ExecuteQuery($"UPDATE tblCashSales SET PaymentMode = '{PaymentMode}', PaymentRefNo = '{PaymentRef}' WHERE CSalesNo = '{CSalesNo}'");
                                LoadData();

                            }
                        }
                        break;
                    case "Refresh": LoadData(); break;
                    case "Print Invoice": TaxInvoices(CSalesNo); break;
                    case "SalesClose": SalesClose(); break;
                    case "Close Payment Mode": LoadData(); break;
                    case "Close": CloseWind(); break;
                }

                InitializedDashboard();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintData()
        {
            string RPTHeaderName = "Cash Sales & Stationery Summary from " + DateTime.Now.ToString("dd/MM/yyyy") + " to " + DateTime.Now.ToString("dd/MM/yyyy") + " (Overall)";

            using (SqlDataReader dr = GetDataReader($"SELECT 'Cash Sales & Stationery Summary from ' + FORMAT(MIN(CSalesDate), 'dd-MMM-yyyy') + ' to ' + FORMAT(MAX(CSalesDate), 'dd-MMM-yyyy') AS RPTHeader FROM tblCashSales WHERE FORMAT(ClosedSalesTime, 'yyyy-MM-dd') = '{ClosedDateTime.ToString("yyyy-MM-dd")}' AND Status = 1"))
            {
                if (dr.Read())
                {
                    RPTHeaderName = dr["RPTHeader"].ToString();
                }
                dr.Close();
            }

            DataTable dt;
            using (dt = new DataTable())
            {
                dt = GetDataTable($"Select distinct '{RPTHeaderName}' As Header, 0 as ID, a.CSalesNo, a.CSalesDate as PrintDate, '' as AWBNo, '' as ShipmentType, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and a.CSalesNo LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge FROM tblCashSales a " +
                    $"WHERE FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') = '{ClosedDateTime.ToString("yyyy-MM-dd")}' AND a.Status = 1 " +
                    $"UNION " +
                    $"Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesNo, a.CSalesDate as PrintDate, iif(b.shipmenttype = 'STATIONERY', f.itemdesc, b.CNNum)  as AWBNo, b.ShipmentType as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], c.PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, b.DestCode as AWBDestCode, b.Pieces as AWBPieces, b.HighestWeight as AWBWeight, b.VolWeight as AWBVolWeight, IIF(b.ShipmentType = 'STATIONERY', b.Price, 0) as StationaryPrice, b.InsValue as AWBInsurans, IIF(b.ShipmentType != 'STATIONERY', b.Price, 0) as AWBPrice, b.OtherCharges as AWBOtherCharges, (b.TotalAmt + b.InsValue - b.StaffDiscAmt - b.PromoDiscAmt) as AWBTotalAmt, b.StaffDiscAmt as AWBStaffDiscAmt, b.PromoDiscAmt as AWBPromoDiscAmt, IIF(b.TaxCode = 'Surcharge Tax', b.destcode, 'MALAYSIA') as Country, ISNULL(b.RASurcharge, 0) FROM tblCashSales a " +
                    $"left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                    $"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                    $"left join tblDestination d on b.destcode = d.DestCode " +
                    $"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                    $"LEFT JOIN tblItemMaster f on b.CNNum = f.ItemCode " +
                    $"WHERE FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') = '{ClosedDateTime.ToString("yyyy-MM-dd")}' AND b.CSalesNo IS NOT NULL AND a.Status = 1 " +
                    $"UNION " +
                    $"Select '{RPTHeaderName}' As Header, 0 as ID, LogDescription AS CSalesNo, CreatedDate as PrintDate, LogTitle as AWBNo, '' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount, 0 as RoundingAmt, '' as PaymentMode, '' as PaymentRefNo, 0 AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge  FROM tblDailyLog WHERE FORMAT(CreatedDate, 'yyyy-MM-dd') = '{ClosedDateTime.ToString("yyyy-MM-dd")}' AND IsDeleted = 0 AND LogTitle = 'Closed' Order By PrintDate");

                dt.AsEnumerable().Where(rec => rec["AWBNo"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => { rec["AWBNo"] = "Stationery"; });

                if (dt.Rows.Count > 0)
                {
                    DataTable DTCompany = new DataTable();
                    DTCompany = GetDataTable($"SELECT  a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1,''), Address2 = ISNULL(a.Address2,'') , Address3 = ISNULL(a.Address3,'') , City = ISNULL(a.City,'') , State = ISNULL(a.State,'') , Country =ISNULL(a.Country,'') , PostalCode = ISNULL(a.ZipCode,'')  , PhoneNo =  ISNULL(a.PhoneNum,'') , FaxNo =ISNULL(a.Fax,'')  , EMail = ISNULL(a.EMail,'') , a.Status  FROM tblStationProfile a" +
                        $" WHERE RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND Status = 1");

                    SkynetCashSales.View.Reports.FrmReportViewer RptViewerGen = new SkynetCashSales.View.Reports.FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    SkynetCashSales.ViewModel.Reports.ReportViewerVM VM = new SkynetCashSales.ViewModel.Reports.ReportViewerVM("CompanyMas", DTCompany, "CSSummary", dt, "", dt, "", dt, "RPTCSSummary.rdlc", "", RptViewerGen.RptViewer, false, true);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
            }
        }

        private void SalesClose()
        {
            try
            {
                string pcname = AMGeneral.GetPCName();
                var messageresult = MessageBox.Show("Are you sure to Closed today Sales", "Confirmation", MessageBoxButton.YesNo);

                if (messageresult == MessageBoxResult.Yes)
                {
                    SqlDataReader dr = GetDataReader($"SELECT LogID, CreatedDate FROM tblDailyLog WHERE (CreatedDate BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{DateTime.Now.Date.AddHours(23.99).ToString("yyyy-MM-dd HH:mm:ss")}') AND LogTitle = 'Closed'");
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            //ClosedTitle = "Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                            MessageBox.Show("Sorry already Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToString("dd-MM-yyyy hh:mm:ss tt"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        dr.Close();
                        ExecuteQuery($"INSERT INTO tblDailyLog(StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported)" +
                            $" VALUES('{MyCompanyCode.Trim()}', 'Closed', '{("Sales closed at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt"))}', '{pcname}', '{LoginUserName}', '{LoginUserId}', GETDATE(), 0, 0)");

                        int i = ExecuteQuery($"UPDATE tblCashSales SET ClosedSalesTime = GETDATE() WHERE ClosedSalesTime IS NULL");
                        //ClosedTitle = "Closed at " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                        LoadData();

                        if (i > 0)
                        {
                            MessageBox.Show("Sales Closed at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Failed to Update Today Sales Close");
                        }
                    }
                    dr.Close();

                    loadprocess();

                }

            }
            catch (Exception ex)
            {
                error_log.errorlog("Sales Close:" + ex.ToString());
            }
        }

        private void loadprocess()
        {
            MyWind.gif.Visibility = Visibility.Visible;

            bg.WorkerReportsProgress = true;
            bg.DoWork += bg_DoWork;
            bg.RunWorkerCompleted += bg_RunWorkerCompleted;

            if (!bg.IsBusy) bg.RunWorkerAsync();
        }

        private void LoadData()
        {
            AWBList.Clear(); SumOfAWBs.Clear(); PaymentModes.Clear();

            try
            {
                MyWind.lblAWBNumber.Content = ""; CSalesNo = ""; PaymentMode = ""; PaymentRef = "";
                MyWind.lblShpmntCost2.Content = string.Format("{0:0.00}", 0);
                MyWind.lblOtherCharges.Content = string.Format("{0:0.00}", 0);
                MyWind.lblStationarySum2.Content = string.Format("{0:0.00}", 0);
                MyWind.lblInsurenceCharges2.Content = string.Format("{0:0.00}", 0);
                MyWind.lblTotalDisAmt.Content = string.Format("{0:0.00}", 0);
                MyWind.lblTotAmtBeforeTax.Content = string.Format("{0:0.00}", 0);
                MyWind.lblTax.Content = "SST 6% Amt [+]";
                MyWind.lblTaxAmt.Content = string.Format("{0:0.00}", 0);
                MyWind.lblRoundingAmt.Content = string.Format("{0:0.00}", 0);
                MyWind.lblTotalAmtAfterTax.Content = string.Format("{0:0.00}", 0);
                MyWind.lblTotalAmtAfterTax1.Content = "RM" + string.Format("{0:0.00}", 0);
                MyWind.canvsPaymentMode.Visibility = Visibility.Hidden;

                using (SqlDataReader dr = GetDataReader($"SELECT TOP 1 ClosedSalesTime FROM tblCashSales WHERE ClosedSalesTime IS NOT NULL ORDER BY ClosedSalesTime DESC"))
                {
                    if (dr.Read())
                    {
                        ClosedDateTime = Convert.ToDateTime(dr["ClosedSalesTime"]);
                        if (DateTime.Now.ToString("dd-MMM-yyyy") == ClosedDateTime.ToString("dd-MMM-yyyy"))
                        {
                            MyWind.cmdSalesClose.Content = $"Closed @{Convert.ToDateTime(dr["ClosedSalesTime"]).ToString("dd-MMM-yyyy HH:mm:ss")}";
                            MyWind.cmdSalesClose.IsEnabled = false;
                            MyWind.cmdPrint.IsEnabled = true;
                        }
                        else
                        {
                            MyWind.cmdSalesClose.Content = "Close Today Sales";
                            MyWind.cmdSalesClose.IsEnabled = true;
                            MyWind.cmdPrint.IsEnabled = false;
                        }
                    }
                    dr.Close();
                }

                DataTable dt;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT a.CSalesNo,iif(ISNULL(b.shipmenttype, '') = 'STATIONERY', d.itemdesc, ISNULL(b.CNNum, '')) As AWBNumber, b.CDate As AWBDate, iif(ISNULL(b.shipmenttype, '') = 'STATIONERY', 0, ISNULL(c.AWBPrint, 0)) as AWBPrint, ISNULL(b.ShipmentType, '') AS ShipmentType, ISNULL(b.DestCode, '') AS DestCode, ISNULL(b.Pieces, 0) AS Pieces, ISNULL(b.Weight, 0) AS Weight, ISNULL(b.VolWeight, 0) AS VolWeight, IIF(ISNULL(b.ShipmentType, '') = 'STATIONERY', 0, ISNULL(b.Price, 0)) as Price, IIF(ISNULL(b.ShipmentType, '') = 'STATIONERY', ISNULL(b.Price, 0), 0) As StationaryAmt, ISNULL(b.InsValue, 0) As InsuranceAmt, ISNULL(b.OtherCharges, 0) AS OtherCharges, a.TaxTitle, a.TaxPercentage, a.ShipmentGSTValue As TaxAmt, ISNULL(b.StaffDiscAmt, 0) as StaffDisAmt, ISNULL(b.PromoDiscAmt, 0) as DisAmt, a.RoundingAmt, a.CSalesTotal as TotalAmt, a.PaymentMode, a.PaymentRefNo, ISNULL(b.TaxCode, '') AS TaxCode, ISNULL(b.RASurcharge, 0) as RASurcharge FROM tblCashSales a " +
                        $"LEFT JOIN tblCSaleCNNum b ON a.CSalesNo = b.CSalesNo " +
                        $"LEFT JOIN(SELECT AWBNum, COUNT(AWBNum) As AWBPrint FROM tblCSPrintDate GROUP BY AWBNum) c ON b.CNNum = C.AWBNum " +
                        $"LEFT JOIN tblItemMaster d on b.CNNum = d.ItemCode " +
                        $" WHERE (a.CDate BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{DateTime.Now.Date.AddHours(23.9999).ToString("yyyy-MM-dd HH:mm:ss")}') AND (b.CDate BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{DateTime.Now.Date.AddHours(23.9999).ToString("yyyy-MM-dd HH:mm:ss")}') AND a.Status = 1");

                    dt.AsEnumerable().Where(rec => rec["AWBNumber"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => {
                        rec["AWBNumber"] = "Stationery";
                        rec["ShipmentType"] = ""; rec["DestCode"] = ""; rec["Pieces"] = 0; rec["Weight"] = 0.00; rec["VolWeight"] = 0.00;
                    });


                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            AWBList.Add(new AWBInfo
                            {
                                Item = AWBList.Count + 1,
                                CSalesNo = dt.Rows[i]["CSalesNo"].ToString(),
                                AWBNumber = dt.Rows[i]["AWBNumber"].ToString(),
                                AWBPrintCount = dt.Rows[i]["AWBPrint"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[i]["AWBPrint"]),
                                ShipmentType = dt.Rows[i]["ShipmentType"].ToString(),
                                DestCode = dt.Rows[i]["DestCode"].ToString(),
                                Pieces = Convert.ToInt32(dt.Rows[i]["Pieces"]),
                                Weight = Convert.ToDecimal(dt.Rows[i]["Weight"]) > Convert.ToDecimal(dt.Rows[i]["VolWeight"]) ? Convert.ToDecimal(dt.Rows[i]["Weight"]) : Convert.ToDecimal(dt.Rows[i]["VolWeight"]),
                                Cost = dt.Rows[i]["ShipmentType"].ToString() != "STATIONERY" ? Convert.ToDecimal(dt.Rows[i]["Price"]) : 0,
                                OtherCharges = Convert.ToDecimal(dt.Rows[i]["OtherCharges"]),
                                StationeryAmt = dt.Rows[i]["ShipmentType"].ToString() == "STATIONERY" ? Convert.ToDecimal(dt.Rows[i]["StationaryAmt"]) : 0,
                                InsuranceAmt = Convert.ToDecimal(dt.Rows[i]["InsuranceAmt"]),
                                StaffDisAmt = Convert.ToDecimal(dt.Rows[i]["StaffDisAmt"]),
                                DisAmt = Convert.ToDecimal(dt.Rows[i]["DisAmt"]),
                                BeforeTax = dt.Rows[i]["ShipmentType"].ToString() == "STATIONERY" ?
                                 Convert.ToInt32(dt.Rows[i]["Pieces"]) * Convert.ToDecimal(dt.Rows[i]["StationaryAmt"]) :
                                (Convert.ToDecimal(dt.Rows[i]["Price"]) + Convert.ToDecimal(dt.Rows[i]["OtherCharges"]) + Convert.ToDecimal(dt.Rows[i]["StationaryAmt"]) + Convert.ToDecimal(dt.Rows[i]["InsuranceAmt"])) - (Convert.ToDecimal(dt.Rows[i]["StaffDisAmt"]) + Convert.ToDecimal(dt.Rows[i]["DisAmt"])),
                                TaxCode = dt.Rows[i]["TaxCode"].ToString(),
                                TaxRate = dt.Rows[i]["TaxPercentage"] != DBNull.Value ? Convert.ToDecimal(dt.Rows[i]["TaxPercentage"]) : 0,
                                TaxAmt = Convert.ToDecimal(dt.Rows[i]["TaxAmt"]),
                                RoundingAmt = dt.Rows[i]["RoundingAmt"] != DBNull.Value ? Convert.ToDecimal(dt.Rows[i]["RoundingAmt"]) : 0,
                                TotalAmt = Convert.ToDecimal(dt.Rows[i]["TotalAmt"]),
                                AWBDateTime = Convert.ToDateTime(dt.Rows[i]["AWBDate"]).ToString("hh:mm:ss tt"),
                                PaymentMode = dt.Rows[i]["PaymentMode"].ToString(),
                                PaymentRef = dt.Rows[i]["PaymentRefNo"].ToString(),
                                RASurcharge = Convert.ToDecimal(dt.Rows[i]["RASurcharge"])
                            });
                        }

                        SumOfAWBs.Add(new AWBInfo
                        {
                            Description = "Total : ",
                            Pieces = AWBList.Sum(a => a.ShipmentType == "STATIONERY" ? 0 : a.Pieces),
                            Weight = AWBList.Sum(a => a.Weight),
                            Cost = AWBList.Sum(a => a.Cost),
                            OtherCharges = AWBList.Sum(a => a.OtherCharges),
                            StationeryAmt = AWBList.Sum(a => a.StationeryAmt),
                            InsuranceAmt = AWBList.Sum(a => a.InsuranceAmt),
                            StaffDisAmt = AWBList.Sum(a => a.StaffDisAmt),
                            DisAmt = AWBList.Sum(a => a.DisAmt),
                            BeforeTax = AWBList.Sum(a => a.BeforeTax),
                            TaxAmt = AWBList.Sum(a => a.TaxAmt),
                            TotalAmt = AWBList.Sum(a => a.TotalAmt),
                            RoundingAmt = AWBList.Sum(a => a.RoundingAmt),
                            RASurcharge = AWBList.Sum(a => a.RASurcharge)
                        });
                    }
                }

                MyWind.canvsPaymentMode.Visibility = Visibility.Hidden;
                using (dt = new DataTable())
                {
                    dt = GetDataTable($"SELECT A.GDDesc FROM tblGeneralDet A" +
                        $" LEFT JOIN tblGeneralMaster B ON A.GenId = B.GenId" +
                        $" WHERE RTRIM(B.GenDesc) = 'PAYMENT MODE' ORDER BY A.GDDesc");

                    for (int i = 0; i < dt.Rows.Count; ++i)
                    {
                        PaymentModes.Add(new AWBInfo { PaymentMode = dt.Rows[i]["GDDesc"].ToString() });
                    }
                }
            }
            catch (Exception ex) { error_log.errorlog("LoadData at TodaysSalesReportVM:" + ex.ToString()); }
        }
        static readonly string[] ValidatedProperties = { "PaymentMode", "PaymentReference" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null) // there is an error
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "PaymentMode": if (string.IsNullOrEmpty(PaymentMode)) error = "PaymentMode is required"; MyWind.cmbPaymentMode.Focus(); break;
                case "PaymentReference": if (PaymentMode != "CASH" && string.IsNullOrEmpty(PaymentRef)) error = "Payment Reference cannot be empty"; MyWind.CanvasPaymentModeref.Focus(); break;
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }


        public class AWBInfo
        {
            public int Item { get; set; }
            public string Description { get; set; }
            public string CSalesNo { get; set; }
            public string AWBNumber { get; set; }
            public int AWBPrintCount { get; set; }
            public string ShipmentType { get; set; }
            public string DestCode { get; set; }
            public int Pieces { get; set; }
            public decimal Weight { get; set; }
            public decimal Cost { get; set; }
            public decimal OtherCharges { get; set; }
            public decimal StationeryAmt { get; set; }
            public decimal InsuranceAmt { get; set; }
            public decimal StaffDisAmt { get; set; }
            public decimal DisAmt { get; set; }
            public decimal BeforeTax { get; set; }
            public string TaxCode { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal RoundingAmt { get; set; }
            public decimal TotalAmt { get; set; }
            public string AWBDateTime { get; set; }
            public string PaymentMode { get; set; }
            public string PaymentRef { get; set; }
            public decimal RASurcharge { get; set; }
        }

    }
}
