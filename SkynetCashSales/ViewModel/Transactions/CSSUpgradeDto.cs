﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class CSSUpgradeDto
    {
        public CSSUpgradeDto(string releasedate, string versionno, string filename, string iscumpolsory, string isupdated, string updatedDate, string features, string ftp_url, string ftp_username, string ftp_password)
        {
            ReleaseDate = releasedate;
            VersionNo = versionno;
            FileName = filename;
            IsCumpolsory = iscumpolsory;
            IsUpdated = isupdated;
            UpdatedDate = updatedDate;
            Features = features;
            Ftp_URL = ftp_url;
            Ftp_Username = ftp_username;
            Ftp_Password = ftp_password;

        }

        public List<CSSUpgradeDto> CSSUpgrade = new List<CSSUpgradeDto>();

        public string ReleaseDate { get; set; }
        public string VersionNo { get; set; }
        public string FileName { get; set; }
        public string IsCumpolsory { get; set; }
        public string IsUpdated { get; set; }
        public string UpdatedDate { get; set; }
        public string Features { get; set; }
        public string Ftp_URL { get; set; }
        public string Ftp_Username { get; set; }
        public string Ftp_Password { get; set; }
  
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
