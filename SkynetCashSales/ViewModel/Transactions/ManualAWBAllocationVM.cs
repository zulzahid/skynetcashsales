﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
//using System.Windows.Forms;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions

{
    public class ManualAWBAllocationVM : AMGenFunction
    {
        public FrmManualAWBAllocation MyWind;
        public Window abc;
        private static DataTable DTConsignee = new DataTable();
        public ManualAWBAllocationVM(FrmManualAWBAllocation Wind)
        {
            MyWind = Wind;
            ResetData();
            if (LoginUserAuthentication == "User")
            {
                MyWind.btnSave.Visibility = Visibility.Hidden; 
                MyWind.btnDelete.Visibility = Visibility.Hidden;
                MyWind.btnNew.Visibility = Visibility.Hidden;
            }
            else
            {
                MyWind.btnSave.Visibility = Visibility.Visible;
                MyWind.btnDelete.Visibility = Visibility.Visible;
                MyWind.btnNew.Visibility = Visibility.Visible;
            }
        }
        public static DataTable csvData;

        private long _AllocateId, _StationId;
        private string _AllocateNum, _AllocateDate, _StationCode, _FromAWB, _ToAWB;
        
        private int _NoOfAWB;
        private bool _AllocateNumEnabled, _IsActive, _IsNext, _IsUsed;

        public bool IsActive { get { return _IsActive; } set { _IsActive = value; OnPropertyChanged("IsActive"); } }
        public bool IsNext { get { return _IsNext; } set { _IsNext = value; OnPropertyChanged("IsNext"); } }
        public bool IsUsed { get { return _IsUsed; } set { _IsUsed = value; OnPropertyChanged("IsUsed"); } }
        
        public long AllocateId { get { return _AllocateId; } set { _AllocateId = value; OnPropertyChanged("AllocateId"); } }
        public string AllocateNum { get { return _AllocateNum; } set { _AllocateNum = value; OnPropertyChanged("AllocateNum"); } }
        public bool AllocateNumEnabled { get { return _AllocateNumEnabled; } set { _AllocateNumEnabled = value; OnPropertyChanged("AllocateNumEnabled"); } }
        public string AllocateDate { get { return _AllocateDate; } set { _AllocateDate = value; OnPropertyChanged("AllocateDate"); } }
        public long StationId { get { return _StationId; } set { _StationId = value; OnPropertyChanged("StationId"); } }
        public string StationCode { get { return _StationCode; } set { _StationCode = value; OnPropertyChanged("StationCode"); } }

        private string _ConsignorCode, _ConsignorName;
        public string ConsignorCode { get { return _ConsignorCode; } set { _ConsignorCode = value; OnPropertyChanged("ConsignorCode"); } }
        public string ConsignorName { get { return _ConsignorName; } set { _ConsignorName = value; OnPropertyChanged("ConsignorName"); } }
        
        public int NoOfAWB { get { return _NoOfAWB; } set { _NoOfAWB = value; OnPropertyChanged("NoOfAWB"); if (NoOfAWB > 0 && !string.IsNullOrEmpty(FromAWB)) { ToAWB = (Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)).ToString() + ((Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)) % 7).ToString(); } } }
        public string FromAWB { get { return _FromAWB; } set { _FromAWB = value; OnPropertyChanged("FromAWB"); } }
        public string ToAWB { get { return _ToAWB; } set { _ToAWB = value; OnPropertyChanged("ToAWB"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
                
        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "AWB Allocation Number":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.AllocateId, ColOneText = a.StnCode, ColTwoText = a.AllocateNum, ColThreeText = a.AllocateDate FROM tblManualAWBAllocation a WHERE a.IsDelete = '0' Order by a.AllocateNum asc   ");
                            //var query = (from a in CSalesEntity.tblManualAWBAllocations
                            //             where a.IsDelete == false
                            //             orderby a.AllocateNum ascending
                            //             select new { ColOneId = a.AllocateId, ColOneText = a.StnCode, ColTwoText = a.AllocateNum, ColThreeText = a.AllocateDate }).ToList();
                            //dt = LINQToDataTable(query);
                            

                            ChildWnd.FrmListCol3_Closed += (r => { AllocateId = Convert.ToInt64(r.Id); AllocateNum = r.ColTwoText; AllocateDate = r.ColThreeText; LoadData(); });
                            ChildWnd.ThreeColWindShow(dt, "AWB Allocation No.", "Station Code", "AllocateNo", "Date");
                        }
                        break;
                    case "Consignor":
                        {
                            dt.Reset();

                            dt = GetDataTable($"SELECT distinct ColOneId = a.ConsignorID, ColOneText = a.ConsignorCode, ColTwoText = a.Name, ColThreeText = a.City, ColFourText = a.Mobile FROM tblConsignor a ORDER BY a.Name asc");
                            
                            //var query = (from a in CSalesEntity.tblConsignors
                            //             orderby a.Name ascending
                            //             select new { ColOneId = a.ConsignorID, ColOneText = a.ConsignorCode, ColTwoText = a.Name, ColThreeText = a.City, ColFourText = a.Mobile }).Distinct().ToList();
                            //dt = LINQToDataTable(query);

                            ChildWnd.FrmListCol4_Closed += (r => { ConsignorCode = r.ColOneText; ConsignorName = r.ColTwoText; });
                            ChildWnd.FourColWindShow(dt, "Consignor Selection", "Consignor Code", "Consignor Name", "City", "Mobile");
                        } break;
                    case "Load Allocation Details": LoadData(); break;
                    case "New": ResetData(); AllocateNumEnabled = false; MyWind.txtStationCode.Focus(); break;
                    case "Generate ToNumber":
                        {
                            if (NoOfAWB == 0)
                            {
                                System.Windows.MessageBox.Show("Please eneter no. of AWB to be locked");
                                FromAWB = null;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(FromAWB))
                                {
                                    if (CheckAllocation("SingleAWB", FromAWB, ""))
                                    {
                                        ToAWB = (Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)).ToString() + ((Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)) % 7).ToString();
                                            //(Convert.ToInt64(FromAWB) + NoOfAWB).ToString();
                                    }
                                    if (CheckAllocation("FROMTOAWB", FromAWB, ToAWB) && !string.IsNullOrEmpty(ToAWB))
                                    {
                                        MyWind.btnSave.Focus();
                                    }
                                    else
                                    {

                                        System.Windows.MessageBox.Show("AWB Number is already allocated", "Warning");
                                        FromAWB = ToAWB = "";
                                        MyWind.txtFromNumber.Focus();
                                    }

                                }
                            }//FocusNextControl();
                        }
                        break;
                    case "Save":
                        {
                            string auth=StrAuthenticateType();
                            if (auth == "ADMIN"||auth == "Admin")
                            {
                                SaveData();
                            }
                            else { MessageBox.Show("ADMIN only, your authentication type: '" + auth+"'", "Warning"); }
                        } break;
                    case "Delete":
                        {
                            if (AllocateNum != null && AllocateNum != "")
                            {
                                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                                if (messageBoxResult == MessageBoxResult.Yes)
                                    Delete();
                            }
                            else
                                System.Windows.MessageBox.Show("Select Account Number to delete");
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                System.Windows.MessageBox.Show(Ex.Message);
            }
        }
        
        public void Delete()
        {
            int i = ExecuteQuery($"UPDATE tblManualAWBAllocation a SET a.Isdelete = 1 WHERE a.AllocateNum = '{AllocateNum}'");
            
            //var delete = CSalesEntity.tblManualAWBAllocations.Where(a => a.AllocateNum == AllocateNum).ToList();
            //delete.ForEach(a => a.IsDelete = true);
            //CSalesEntity.SaveChanges();
            if (i > 0)
            {
                ResetData();
            }
            
        }

        private bool CheckAllocation(string Tag, string FromNumber, string ToNumber)
        {
            long fromNUMBER = FromNumber != null && FromNumber != "" ? Convert.ToInt64(FromNumber) : 0;
            long toNUMBER = ToNumber != null && ToNumber != "" ? Convert.ToInt64(ToNumber) : 0;
            string checking = "";
            if (Tag == "SingleAWB")
            {
                //For Tag type SingleAWB in SP only FROM Number will check to number not check.
                using (SqlDataReader dr = GetDataReader($"SELECT  TOP 1 a.AllocateId FROM tblManualAWBAllocation a WHERE a.FromAWBRunNum >= '{fromNUMBER}' AND a.ToAWBRunNum <= '{fromNUMBER}' "))
                {
                    long idcheck = Convert.ToInt64(dr["AllocateId"]);
                    checking = idcheck > 0 ? "" : "NOTYET";
                }
                 
            }
            else if (Tag == "FROMTOAWB")
            {
                //For Tag type FROMTOAWB in SP we need to pass FROMAWB && TOAWB Numbers.
               
                DataTable dt = GetDataTable($"SELECT TOP 1 a.AllocateId FROM tblManualAWBAllocation  a WHERE  a.FromAWBRunNum >= '{fromNUMBER}' AND a.ToAWBRunNum <= '{fromNUMBER}' AND a.FromAWBRunNum >= '{toNUMBER}' AND a.ToAWBRunNum <= '{toNUMBER}'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        long idcheck = Convert.ToInt64(dr["AllocateId"]);
                        checking = idcheck > 0 ? "" : "NOTYET";
                    }
                }
                
                    
            }
            else { }
            if (checking == "NOTYET") { return true; }
            else { return false; }
        }

        private void SaveData()
        {
            if (IsValidate)
            {
                if (CheckAllocation("FROMTOAWB", FromAWB, ToAWB))
                {
                    if (AllocateId == 0)
                    {
                        string Statusvalue = IsActive == true ? "ACTIVE" : IsNext == true ? "NEXT" : IsUsed == true ? "USED" : "ACTIVE";
                        int i = ExecuteQuery($"INSERT INTO tblManualAWBAllocation (AllocateNum,AllocateDate,ConsignorCode,ConsignorName,StnCode" +
                            $",NoOfAWB,AWBPrefix,AWBSuffix,FromAWB,FromAWBRunNum,ToAWB,ToAWBRunNum,CreateBy,CreateDate,ModifyBy,ModifyDate,Status," +
                            $"IsExported,IsDelete) VALUES ('{AllocateNum}','{ Convert.ToDateTime(AllocateDate)}','{ConsignorCode}','{ConsignorName}','{StationCode}','{NoOfAWB}'," +
                            $" '','','{FromAWB}','{Convert.ToInt64(FromAWB)}','{ToAWB}','{Convert.ToInt64(ToAWB)}','{LoginUserId}','{DateTime.Now}',null,null,'{Statusvalue}',0," +
                            $"0)");
                        //CSalesEntity.tblManualAWBAllocations.Add(new tblManualAWBAllocation
                        //{
                        //    AllocateNum = AllocateNum,
                        //    AllocateDate = Convert.ToDateTime(AllocateDate),
                        //    ConsignorCode = ConsignorCode,
                        //    ConsignorName = ConsignorName,
                        //    StnCode = StationCode,
                        //    NoOfAWB = NoOfAWB,
                        //    AWBPrefix = "",
                        //    AWBSuffix = "",
                        //    FromAWB = FromAWB,
                        //    FromAWBRunNum = Convert.ToInt64(FromAWB),
                        //    ToAWB = ToAWB,
                        //    ToAWBRunNum = Convert.ToInt64(ToAWB),
                        //    CreateBy = LoginUserId,
                        //    CreateDate = DateTime.Now,
                        //    ModifyBy = null,
                        //    ModifyDate = null,
                        //    Status = IsActive == true ? "ACTIVE" : IsNext == true ? "NEXT" : IsUsed == true ? "USED" : "ACTIVE",
                        //    IsExported = false,
                        //    IsDelete = false,
                        //}); 
                        //CSalesEntity.SaveChanges();
                        SqlDataReader dr = GetDataReader("SELECT max(a.AllocateId) as AllocateId from tblManualAWBAllocation");
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                AllocateId = Convert.ToInt64(dr["AllocateId"]);
                                AllocateNum = "M" + string.Format("{0:00000}", AllocateId);
                            }
                        }
                        dr.Close(); 

                   
                        int j = ExecuteQuery($"UPDATE tblManualAWBAllocation SET AllocateNum = '{AllocateNum}' WHERE a.AllocateId = '{AllocateId}'");
                        
                        //tblManualAWBAllocation updateallocation = CSalesEntity.tblManualAWBAllocations.Where(a => a.AllocateId == AllocateId).FirstOrDefault();
                        //updateallocation.AllocateNum = AllocateNum;
                        //CSalesEntity.SaveChanges();
                        if (j > 0)
                        {
                            MessageBox.Show("Saved Successfully"); ResetData();
                        }
                        else
                        {
                            MessageBox.Show("Saved Failed");
                        }
                       
                    }
                    else
                    {
                        string statusvalue = IsActive == true ? "ACTIVE" : IsNext == true ? "NEXT" : IsUsed == true ? "USED" : "ACTIVE";
                        int i = ExecuteQuery($"UPDATE tblManualAWBAllocation SET AllocateNum = '{AllocateNum}',AllocateDate = '{Convert.ToDateTime(AllocateDate)}',ConsignorCode = '{ConsignorCode}',ConsignorName = '{ConsignorName}'," +
                            $"StnCode = '{StationCode}',NoOfAWB = '{NoOfAWB}',FromAWB = '{FromAWB}',FromAWBRunNum = '{Convert.ToInt64(FromAWB)}',ToAWB = '{ToAWB}',ToAWBRunNum = '{Convert.ToInt64(ToAWB)}',ModifyBy = '{LoginUserId}'," +
                            $"ModifyDate = '{DateTime.Now}',Status = '{statusvalue}',IsExported = 0,IsDelete = 0 WHERE AllocateId = '{AllocateId}'");
                        //tblManualAWBAllocation updateallocation = CSalesEntity.tblManualAWBAllocations.Where(a => a.AllocateId == AllocateId).FirstOrDefault();
                        //updateallocation.AllocateNum = AllocateNum;
                        //updateallocation.AllocateDate = Convert.ToDateTime(AllocateDate);
                        //updateallocation.ConsignorCode = ConsignorCode;
                        //updateallocation.ConsignorName = ConsignorName;
                        //updateallocation.StnCode = StationCode;
                        //updateallocation.NoOfAWB = NoOfAWB;
                        //updateallocation.FromAWB = FromAWB;
                        //updateallocation.FromAWBRunNum = Convert.ToInt64(FromAWB);
                        //updateallocation.ToAWB = ToAWB;
                        //updateallocation.ToAWBRunNum = Convert.ToInt64(ToAWB);
                        //updateallocation.ModifyBy = LoginUserId;
                        //updateallocation.ModifyDate = DateTime.Now;
                        //updateallocation.Status = IsActive == true ? "ACTIVE" : IsNext == true ? "NEXT" : IsUsed == true ? "USED" : "ACTIVE";
                        //updateallocation.IsExported = false;
                        //updateallocation.IsDelete = false;

                        //CSalesEntity.SaveChanges();
                        
                        if (i > 0)
                        {
                            MessageBox.Show("Updated Successfully"); ResetData();
                        }
                        else
                        {
                            MessageBox.Show("Update failed");
                        }
                        
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("AWB Numbers already allocated", "Warning");
                }
            }
            LoadData();
        }
        
        private void ResetData()
        {
            DataTable dt = GetDataTable($"select max(a.ToAWB) as ToAWB from tblManualAWBAllocation a ");
            string LastNum = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    LastNum = row["ToAWB"].ToString();
                }
            }dt.Dispose(); 
           
            IsActive = true;
            AllocateId = 0; AllocateNum = ""; AllocateNumEnabled = true; AllocateDate = DateTime.Now.ToString();
            StationId = 0; StationCode = ""; ConsignorCode = ""; ConsignorName = ""; NoOfAWB = 0;
            //if (string.IsNullOrEmpty(LastNum)) FromAWB = ""; else FromAWB = ToAWBNumber(LastNum, 2); ToAWB = "";
            FromAWB = ""; ToAWB = "";
            StationId = MyCompanyId; StationCode = MyCompanyCode;
            MyWind.txtAllocateNum.Focus(); MyWind.txtAllocateNum.SelectionStart = MyWind.txtAllocateNum.Text.Length;
        }



        private void LoadData()
        {
            AllocateDate = DateTime.Now.ToShortDateString(); NoOfAWB = 0; FromAWB = ""; ToAWB = "";
            if (AllocateNum != null && AllocateNum != "")
            {

                //var query = (from a in CSalesEntity.tblManualAWBAllocations where a.AllocateNum == AllocateNum && a.IsDelete == false select new { a.AllocateNum,a.AllocateDate,a.ConsignorCode,a.ConsignorName,a.StnCode,a.NoOfAWB,a.FromAWB,a.ToAWB,a.Status}).FirstOrDefault();
                DataTable query = GetDataTable($"SELECT TOP 1 a.AllocateNum,a.AllocateDate,a.ConsignorCode,a.ConsignorName,a.StnCode,a.NoOfAWB,a.FromAWB,a.ToAWB,a.Status FROM tblManualAWBAllocation WHERE  a.AllocateNum = '{AllocateNum}' AND a.IsDelete = 0");

                if (query.Rows.Count > 0)
                {
                    foreach (DataRow row in query.Rows)
                    {
                        AllocateNum = row["AllocateNum"].ToString();
                        AllocateDate = row["AllocateDate"].ToString();
                        ConsignorCode = row["ConsignorCode"].ToString();
                        ConsignorName = row["ConsignorName"].ToString();
                        StationCode = row["StnCode"].ToString();
                        NoOfAWB = Convert.ToInt32(row["NoOfAWB"]);
                        FromAWB = row["FromAWB"].ToString();
                        ToAWB = row["ToAWB"].ToString();
                        string status = row["Status"].ToString();
                        if (status == "ACTIVE") { IsActive = true; }
                        else if (status == "NEXT") { IsNext = true; }
                        else if (status == "USED") { IsUsed = true; }
                        else { IsActive = true; }
                    }
                    
                }
                
               
            }
        }

        static readonly string[] CustAccProperties = { "StationCode", "NoOfAWB", "FromAWB", "ToAWB" };//we can't define all the consignors at WIC level "ConsignorCode"
        public bool IsValidate
        {
            get
            {
                foreach (string property in CustAccProperties)
                {
                    if (GetValidationError(property) != null) return false;
                }
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "ConsignorCode": if (string.IsNullOrEmpty(ConsignorCode)) { error = "Invalid Consignor Code"; MyWind.txtConsignorCode.Focus(); } break;
                case "StationCode": if (string.IsNullOrEmpty(StationCode)) { error = "Invalid Account No."; MyWind.txtStationCode.Focus(); } break;
                case "NoOfAWB": if (NoOfAWB <= 0) { error = "Invalid No. Of AWB"; MyWind.txtNumOfAWB.Focus(); } break;
                case "FromAWB": if (string.IsNullOrEmpty(FromAWB)) { error = "Invalid From AWB"; MyWind.txtFromNumber.Focus(); } break;
                case "ToAWB": if (string.IsNullOrEmpty(ToAWB)) { error = "Invalid To AWB"; MyWind.txtToNumber.Focus(); } break;

                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) System.Windows.MessageBox.Show(error);
            return error;
        }
    }
}
