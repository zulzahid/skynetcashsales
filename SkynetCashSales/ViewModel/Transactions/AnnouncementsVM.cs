﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions
{
    public class AnnouncementsVM : CNPrintVM
    {
        public AnnouncementsVM(FrmAnnouncements frm)
        {
            LoadGrid();
        }
        //..[Required(ErrorMessage = "IsAnnouncements is Required")]
        public bool IsAnnouncements
        {
            get { return GetValue(() => IsAnnouncements); }
            set { SetValue(() => IsAnnouncements, value); OnPropertyChanged("IsAnnouncements"); }
        }
        //..[Required(ErrorMessage = "IsVersions is Required")]
        public bool IsVersions
        {
            get { return GetValue(() => IsVersions); }
            set { SetValue(() => IsVersions, value); OnPropertyChanged("IsVersions"); }
        }
        //..[Required(ErrorMessage = "IsVersions is Required")]
        public string Title
        {
            get { return GetValue(() => Title); }
            set { SetValue(() => Title, value); OnPropertyChanged("Title"); }
        }
        //..[Required(ErrorMessage = "IsVersions is Required")]
        public string Description
        {
            get { return GetValue(() => Description); }
            set { SetValue(() => Description, value); OnPropertyChanged("Description"); }
        }
        private DataGridCellInfo _focusedcell;
        public DataGridCellInfo FocusedCell
        {
            get { return _focusedcell; }
            set { _focusedcell = value; OnPropertyChanged("FocusedCell"); }
        }
        private AnnouncementVersionListModel _selecteditem = new AnnouncementVersionListModel();
        public AnnouncementVersionListModel SelectedItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("SelectedItem"); }
        }
        public AnnouncementVersionListModel CurrentItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; OnPropertyChanged("CurrentItem"); }
        }
        private string _selectedindex;
        public string SelectedIndex
        {
            get { return _selectedindex; }
            set { _selectedindex = value; OnPropertyChanged("SelectedIndex"); }
        }
        private ObservableCollection<AnnouncementVersionListModel> _AnnouncementVersionList = new ObservableCollection<AnnouncementVersionListModel>();
        public ObservableCollection<AnnouncementVersionListModel> AnnouncementVersionList
        {
            get { return _AnnouncementVersionList; }
            set { _AnnouncementVersionList = value; OnPropertyChanged("AnnouncementVersionList"); }
        }
        public ObservableCollection<AnnouncementVersionListModel> ItemInfo
        {
            get { return _AnnouncementVersionList; }
            set { _AnnouncementVersionList = value; OnPropertyChanged("ItemInfo"); }
        }
         private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Preview": PreviewData(); break;
                    case "New": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
        private void PreviewData()
        {
            if (!string.IsNullOrEmpty(SelectedIndex) && SelectedItem != null)
            {
                int rowindex = Convert.ToInt32(SelectedIndex != null ? SelectedIndex : "0");
                Title = SelectedItem.Title.ToString();
                Description = SelectedItem.Description.ToString();
            }
            else { MessageBox.Show("Kindly select the row", "Warning"); }
        }
        private void LoadGrid()
        {
            DataTable Dt = new DataTable();
            if (IsAnnouncements==true)
            {
                Dt = GetDataTable($"SELECT a.Title, Description = CONCAT('Created at ', format(a.CreatedDate, 'dd-MMM-yyyy'), ' ', a.Description), a.CreatedDate FROM tblAnnouncements a WHERE Status = 1 ORDER BY a.CreatedDate desc");
            }
            else if (IsVersions == true)
            {
                Dt = GetDataTable($"SELECT Title = CONCAT(a.VersionCode, ' ', format(a.VersionDate, 'dd-MMM-yyyy')), Description = a.VersionDescription, a.CreatedDate FROM tblVersionDetails a ORDER BY a.CreatedDate desc");
            }
            else
            {
                Dt = GetDataTable($"SELECT a.Title, Description = CONCAT('Created at ', a.CreatedDate, ' ', a.Description), a.CreatedDate FROM tblAnnouncements a where a.Status = 1" +
                    $" UNION ALL" +
                    $" SELECT Title = CONCAT(a.VersionCode, ' ', format(a.VersionDate, 'dd-MMM-yyyy')), Description = a.VersionDescription, a.CreatedDate from tblVersionDetails a");
            }
            if (Dt.Rows.Count > 0)
            {
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    AnnouncementVersionList.Add(new AnnouncementVersionListModel
                    {
                        SlNo = (i + 1).ToString(),
                        Title = Dt.Rows[i][0].ToString(),
                        Description = Dt.Rows[i][1].ToString()
                    });
                }
            }
        }
        private void ResetData()
        {
            IsAnnouncements = true;
            Title = Description = string.Empty;
        }
        public class AnnouncementVersionListModel : AMGenFunction
        {
            private string _SlNo;
            public string SlNo
            {
                get { return _SlNo; }
                set { _SlNo = value; OnPropertyChanged("SlNo"); }
            }
            private string _Title;
            public string Title
            {
                get { return _Title; }
                set { _Title = value; OnPropertyChanged("Title"); }
            }
            private string _Description;
            public string Description
            {
                get { return _Description; }
                set { _Description = value; OnPropertyChanged("Description"); }
            }
        }
    }
}
