﻿using SkynetCashSales.General;
using SkynetCashSales.View.Transactions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Transactions

{
    public class AWBAllocationVM : AMGenFunction
    {
        public FrmAWBAllocation MyWind;
        public Window abc;
        private static DataTable DTConsignee = new DataTable();
        public AWBAllocationVM(FrmAWBAllocation Wind)
        {
            MyWind = Wind;
            ResetData();
            if (LoginUserAuthentication == "User")
            {
                MyWind.btnSave.Visibility = Visibility.Hidden; 
                MyWind.btnDelete.Visibility = Visibility.Hidden;
                MyWind.btnNew.Visibility = Visibility.Hidden;
            }
            else
            {
                MyWind.btnSave.Visibility = Visibility.Visible;
                MyWind.btnDelete.Visibility = Visibility.Visible;
                MyWind.btnNew.Visibility = Visibility.Visible;
            }
        }
        public static DataTable csvData;
        public string StationName { get { return GetValue(() => StationName); } set { SetValue(() => StationName, value); OnPropertyChanged("StationName"); } }

        public bool IsActive { get { return GetValue(() => IsActive); } set { SetValue(() => IsActive, value); OnPropertyChanged("IsActive"); } }
        public bool IsNext { get { return GetValue(() => IsNext); } set { SetValue(() => IsNext, value); OnPropertyChanged("IsNext"); } }
        public bool IsUsed { get { return GetValue(() => IsUsed); } set { SetValue(() => IsUsed, value); OnPropertyChanged("IsUsed"); } }
        
        public long AllocateId { get { return GetValue(() => AllocateId); } set { SetValue(() => AllocateId, value); OnPropertyChanged("AllocateId"); } }
        public string AllocateNum { get { return GetValue(() => AllocateNum); } set { SetValue(() => AllocateNum, value); OnPropertyChanged("AllocateNum"); } }
        public bool AllocateNumEnabled { get { return GetValue(() => AllocateNumEnabled); } set { SetValue(() => AllocateNumEnabled, value); OnPropertyChanged("AllocateNumEnabled"); } }
        public string AllocateDate { get { return GetValue(() => AllocateDate); } set { SetValue(() => AllocateDate, value); OnPropertyChanged("AllocateDate"); } }
        public long StationId { get { return GetValue(() => StationId); } set { SetValue(() => StationId, value); OnPropertyChanged("StationId"); } }
        public string StationCode { get { return GetValue(() => StationCode); } set { SetValue(() => StationCode, value); OnPropertyChanged("StationCode"); } }

        public int NoOfAWB { get { return GetValue(() => NoOfAWB); } set { SetValue(() => NoOfAWB, value); OnPropertyChanged("NoOfAWB"); if (NoOfAWB > 0 && !string.IsNullOrEmpty(FromAWB)) { if (FromAWB.Length == 12) { ToAWB = (Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)).ToString() + ((Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)) % 7).ToString(); } else { ToAWB = (Convert.ToInt64(FromAWB) + (NoOfAWB - 1)).ToString(); } } } }
        public string FromAWB { get { return GetValue(() => FromAWB); } set { SetValue(() => FromAWB, value); OnPropertyChanged("FromAWB"); } }
        public string ToAWB { get { return GetValue(() => ToAWB); } set { SetValue(() => ToAWB, value); OnPropertyChanged("ToAWB"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) { _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); } return _CommandGen; } }
                
        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "AWB Allocation Number":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.AllocateId, ColOneText = a.StnCode, ColTwoText = a.AllocateNum, ColThreeText = a.AllocateDate FROM tblAWBAllocation a where a.IsDelete = 0 order by a.AllocateNum");
                            ChildWnd.FrmListCol3_Closed += (r => { AllocateId = Convert.ToInt64(r.Id); AllocateNum = r.ColTwoText; AllocateDate = r.ColThreeText; LoadData(); });
                            ChildWnd.ThreeColWindShow(dt, "AWB Allocation No.", "Station Code", "AllocateNo", "Date");
                        }
                        break;
                    case "Load Allocation Details": LoadData(); break;
                    case "New": ResetData(); AllocateNumEnabled = false; MyWind.txtStationCode.Focus(); break;
                    case "Generate ToNumber":
                        {
                            if (NoOfAWB == 0)
                            {
                                System.Windows.MessageBox.Show("Please enter no. of AWB to be locked");
                                FromAWB = null;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(FromAWB))
                                {                                    
                                    if (CheckAllocation("SingleAWB", FromAWB, ""))
                                    {
                                        if (FromAWB.Length == 12)
                                        {
                                            ToAWB = (Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)).ToString() + ((Convert.ToInt64(FromAWB.Substring(0, FromAWB.Length - 1)) + (NoOfAWB - 1)) % 7).ToString();
                                            //(Convert.ToInt64(FromAWB) + NoOfAWB).ToString();
                                        }
                                        else
                                        {
                                            ToAWB = (Convert.ToInt64(FromAWB) + (NoOfAWB - 1)).ToString();
                                        }
                                    }
                                    if (CheckAllocation("FROMTOAWB", FromAWB, ToAWB) && !string.IsNullOrEmpty(ToAWB))
                                    {
                                        MyWind.btnSave.Focus();
                                    }
                                    else
                                    {
                                        System.Windows.MessageBox.Show("AWB Number is already allocated", "Warning");
                                        FromAWB = ToAWB = "";
                                        MyWind.txtFromNumber.Focus();
                                    }
                                }
                            }//FocusNextControl();
                        }
                        break;
                    case "Save":
                        {
                            string auth=StrAuthenticateType();
                            if (auth == "ADMIN"||auth == "Admin")
                            {
                                SaveData();
                            }
                            else { MessageBox.Show("ADMIN only, your authentication type: '" + auth+"'", "Warning"); }
                        } break;
                    case "Delete":
                        {
                            if (AllocateNum != null && AllocateNum != "")
                            {
                                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                                if (messageBoxResult == MessageBoxResult.Yes)
                                    Delete();
                            }
                            else
                                System.Windows.MessageBox.Show("Select Account Number to delete");
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                System.Windows.MessageBox.Show(Ex.Message);
            }
        }
        
        public void Delete()
        {
            ExecuteQuery($"UPDATE tblAWBAllocation SET IsDelete = 1 Where AllocateId = {AllocateId}");
            ResetData();
        }

        private bool CheckAllocation(string Tag, string FromNumber, string ToNumber)
        {
            SqlDataReader dr;

            long fromNUMBER = FromNumber != null && FromNumber != "" ? Convert.ToInt64(FromNumber) : 0;
            long toNUMBER = ToNumber != null && ToNumber != "" ? Convert.ToInt64(ToNumber) : 0;
            string checking = "";
            if (Tag == "SingleAWB")
            {
                //For Tag type SingleAWB in SP only FROM Number will check to number not check.
                dr = GetDataReader($"SELECT a.AllocateId FROM tblAWBAllocation a where a.FromAWBRunNum >= {fromNUMBER} and a.ToAWBRunNum <= {fromNUMBER}");
                long idcheck = dr.Read() ? Convert.ToInt64(dr["AllocateId"]) : 0; dr.Close();

                checking = idcheck > 0 ? "" : "NOTYET";
            }
            else if (Tag == "FROMTOAWB")
            {
                //For Tag type FROMTOAWB in SP we need to pass FROMAWB && TOAWB Numbers.
                dr = GetDataReader($"SELECT a.AllocateId FROM tblAWBAllocation a where a.FromAWBRunNum >= {fromNUMBER} and a.ToAWBRunNum <= {fromNUMBER} and a.FromAWBRunNum >= {toNUMBER} and a.ToAWBRunNum <= {toNUMBER}");
                long idcheck = dr.Read() ? Convert.ToInt64(dr["AllocateId"]) : 0; dr.Close();

                checking = idcheck > 0 ? "" : "NOTYET";
            }
            else { }
            if (checking == "NOTYET") { return true; }
            else { return false; }
        }

        private void SaveData()
        {
            if (IsValidate)
            {
                if (CheckAllocation("FROMTOAWB", FromAWB, ToAWB))
                {
                    if (AllocateId == 0)
                    {
                        AllocateId = 1;
                        SqlDataReader dr = GetDataReader($"SELECT REPLACE(ISNULL(MAX(AllocateNum), 0), 'M', '') As AllocateId FROM tblAWBAllocation");
                        if(dr.Read())
                        {
                            AllocateId += Convert.ToInt64(dr["AllocateId"]);
                        }
                        dr.Close();

                        AllocateNum = "M" + string.Format("{0:00000}", AllocateId);

                        ExecuteQuery($"INSERT INTO tblAWBAllocation(AllocateNum, AllocateDate, CompanyName, StnCode, NoOfAWB, AWBPrefix, AWBSuffix, FromAWB, FromAWBRunNum, ToAWB, ToAWBRunNum, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, IsDelete)" +
                            $" VALUES('{AllocateNum}', '{Convert.ToDateTime(AllocateDate).ToString(sqlDateLongFormat)}', '{StationName}', '{StationCode}', {NoOfAWB}, '', '', '{FromAWB}', {Convert.ToInt64(FromAWB)}, '{ToAWB}', {Convert.ToInt64(ToAWB)}, {LoginUserId}, GETDATE(), NULL, NULL," + 
                            $" '{(IsActive ? "ACTIVE" : (IsNext ? "NEXT" : (IsUsed ? "USED" : "ACTIVE")))}', 0, 0)");
                        
                        MessageBox.Show("Saved Successfully"); ResetData();
                    }
                    else
                    {
                        ExecuteQuery($"UPDATE tblAWBAllocation SET" +
                            $" AllocateNum = '{AllocateNum}'," +
                            $" AllocateDate = '{Convert.ToDateTime(AllocateDate).ToString("yyyy-MM-dd HH:mm:ss")}'," +
                            $" CompanyName = '{StationName}'," +
                            $" StnCode = '{StationCode}'," +
                            $" NoOfAWB = {NoOfAWB}," +
                            $" FromAWB = '{FromAWB}'," +
                            $" FromAWBRunNum = {Convert.ToInt64(FromAWB)}," +
                            $" ToAWB = '{ToAWB}'," +
                            $" ToAWBRunNum = {Convert.ToInt64(ToAWB)}," +
                            $" ModifyBy = {LoginUserId}," +
                            $" ModifyDate = GETDATE()," +
                            $" Status = '{(IsActive == true ? "ACTIVE" : (IsNext == true ? "NEXT" : (IsUsed == true ? "USED" : "ACTIVE")))}'," +
                            $" IsExported = 0," +
                            $" IsDelete = 0" +
                            $" Where AllocateId = {AllocateId}");

                        MessageBox.Show("Updated Successfully"); ResetData();
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("AWB Numbers already allocated", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            LoadData();
        }
        
        private void ResetData()
        {
            SqlDataReader dr = GetDataReader($"SELECT MAX(a.ToAWB) AS ToAWB FROM tblAWBAllocation a");
            string LastNum = dr.Read() ? dr["ToAWB"].ToString() : ""; dr.Close();

            IsNext = true;
            AllocateId = 0; AllocateNum = ""; AllocateNumEnabled = true; AllocateDate = DateTime.Now.ToString();
            StationId = 0; StationCode = ""; StationName = ""; NoOfAWB = 0;
            //if (string.IsNullOrEmpty(LastNum)) FromAWB = ""; else FromAWB = ToAWBNumber(LastNum, 2); ToAWB = "";
            FromAWB = ""; ToAWB = ""; StationName = "";
            StationId = MyCompanyId; StationCode = MyCompanyCode; StationName = MyCompanyName;
            MyWind.txtAllocateNum.Focus(); MyWind.txtAllocateNum.SelectionStart = MyWind.txtAllocateNum.Text.Length;
        }

        private void LoadData()
        {
            AllocateDate = DateTime.Now.ToShortDateString(); NoOfAWB = 0; FromAWB = ""; ToAWB = "";
            if (AllocateNum != null && AllocateNum != "")
            {
                SqlDataReader dr = GetDataReader($"SELECT a.AllocateNum,a.AllocateDate,a.CompanyName,a.StnCode,a.NoOfAWB,a.FromAWB,a.ToAWB,a.Status FROM tblAWBAllocation a where a.AllocateNum = '{AllocateNum}' AND a.IsDelete = 0");
                if (dr.Read())
                {
                    AllocateNum = dr["AllocateNum"].ToString();
                    AllocateDate = Convert.ToDateTime(dr["AllocateDate"]).ToShortDateString();
                    StationName = dr["CompanyName"].ToString();
                    StationCode = dr["StnCode"].ToString();
                    NoOfAWB = Convert.ToInt32(dr["NoOfAWB"]);
                    FromAWB = dr["FromAWB"].ToString();
                    ToAWB = dr["ToAWB"].ToString();
                    string status = dr["Status"].ToString();
                    if (status == "ACTIVE") { IsActive = true; }
                    else if (status == "NEXT") { IsNext = true; }
                    else if (status == "USED") { IsUsed = true; }
                    else { IsActive = true; }
                }
                dr.Close();
            }
        }

        static readonly string[] CustAccProperties = { "StationCode", "NoOfAWB", "FromAWB", "ToAWB" };
        public bool IsValidate
        {
            get
            {
                foreach (string property in CustAccProperties)
                {
                    if (GetValidationError(property) != null) return false;
                }
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "StationCode": if (string.IsNullOrEmpty(StationCode)) { error = "Invalid Account No."; MyWind.txtStationCode.Focus(); } break;
                case "NoOfAWB": if (NoOfAWB <= 0) { error = "Invalid No. Of AWB"; MyWind.txtNumOfAWB.Focus(); } break;
                case "FromAWB": if (string.IsNullOrEmpty(FromAWB)) { error = "Invalid From AWB"; MyWind.txtFromNumber.Focus(); } break;
                case "ToAWB": if (string.IsNullOrEmpty(ToAWB)) { error = "Invalid To AWB"; MyWind.txtToNumber.Focus(); } break;

                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) System.Windows.MessageBox.Show(error);
            return error;
        }

    }
}
