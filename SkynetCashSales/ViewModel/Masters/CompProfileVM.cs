﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class CompProfileVM : AMGenFunction
    {
        public FrmCompProfile MyWind;

        public CompProfileVM(FrmCompProfile Wind)
        {
            MyWind = Wind;
            DispSelectedItem("Company", MyCompanyCode); 
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }

            //SELECT 'INSERT INTO tblWICProfile (WICId, WICCode, WICSubCode, WICName, CompType, AccNumber, StnCode, HQCode, RegNum, TaxTitle, TaxNo, IATACode, Address1, Address2, Address3, City, State, Country, PostCode, PhoneNum, Fax, EMail, QuotationNum, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate) VALUES(' +
            //CONCAT(CompId, ', ''', CompCode, ''', ''', SubName, ''', ''', CompName, ''', ''', CompType, ''', ''', CashAccNum, ''', ''', CompCode, ''', ''', RTRIM(HQCode), ''', ''', RegNum, ''', ''', TaxTitle, ''', ''', TaxNo, ''', ''', RTRIM(IATACode), ''', ''', Address1, ''', ''', Address2, ''', ''', Address3, ''', ''', City, ''', ''', State, ''', ''', Country, ''', ''', ZipCode, ''', ''', PhoneNum, ''', ''', Fax, ''', ''', EMail, ''', '''', ''', FromDate, ''', NULL, ''ADMIN'', ''', CreateDate, ''', '''', NULL, ''ACTIVE'', 1, GETDATE())') FROM tblStationProfile

        }

        public int CompanyId { get { return GetValue(() => CompanyId); } set { SetValue(() => CompanyId, value); OnPropertyChanged("CompanyId"); } }
        public string CompanyName { get { return GetValue(() => CompanyName); } set { SetValue(() => CompanyName, value); OnPropertyChanged("CompanyName"); } }
        public string CompanySubName { get { return GetValue(() => CompanySubName); } set { SetValue(() => CompanySubName, value); OnPropertyChanged("CompanySubName"); } }
        public string CompanyCode { get { return GetValue(() => CompanyCode); } set { SetValue(() => CompanyCode, value); OnPropertyChanged("CompanyCode"); } }
        public string CompanyType { get { return GetValue(() => CompanyType); } set { SetValue(() => CompanyType, value); OnPropertyChanged("CompanyType"); } }
        public string RegNum { get { return GetValue(() => RegNum); } set { SetValue(() => RegNum, value); OnPropertyChanged("RegNum"); } }
        public string GSTNum { get { return GetValue(() => GSTNum); } set { SetValue(() => GSTNum, value); OnPropertyChanged("GSTNum"); } }
        public string IATACode { get { return GetValue(() => IATACode); } set { SetValue(() => IATACode, value); OnPropertyChanged("IATACode"); } }
        public string HQCode { get { return GetValue(() => HQCode); } set { SetValue(() => HQCode, value); OnPropertyChanged("HQCode"); } }
        public string CashAccNum { get { return GetValue(() => CashAccNum); } set { SetValue(() => CashAccNum, value); OnPropertyChanged("CashAccNum"); } }

        public string Address1 { get { return GetValue(() => Address1); } set { SetValue(() => Address1, value); OnPropertyChanged("Address1"); } }
        public string Address2 { get { return GetValue(() => Address2); } set { SetValue(() => Address2, value); OnPropertyChanged("Address2"); } }
        public string City { get { return GetValue(() => City); } set { SetValue(() => City, value); OnPropertyChanged("City"); } }
        public string State { get { return GetValue(() => State); } set { SetValue(() => State, value); OnPropertyChanged("State"); } }
        public string Country { get { return GetValue(() => Country); } set { SetValue(() => Country, value); OnPropertyChanged("Country"); } }
        public string ZipCode { get { return GetValue(() => ZipCode); } set { SetValue(() => ZipCode, value); OnPropertyChanged("ZipCode"); } }
        public string PhoneNum { get { return GetValue(() => PhoneNum); } set { SetValue(() => PhoneNum, value); OnPropertyChanged("PhoneNum"); } }
        public string Fax { get { return GetValue(() => Fax); } set { SetValue(() => Fax, value); OnPropertyChanged("Fax"); } }
        public string EMail { get { return GetValue(() => EMail); } set { SetValue(() => EMail, value); OnPropertyChanged("EMail"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Company":
                        {
                            dt = GetDataTable("SELECT CompId As ColOneId, CompName As ColOneText, CompCode As ColTwoText FROM tblStationProfile WHERE Status = 1 ORDER BY CompName");
                            ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), r.ColTwoText); });
                            ChildWnd.TwoColWindShow(dt, "Existing Company Selection", "Company", "Code");
                        }
                        break;
                    case "New Company":
                        {
                            dt = GetDataTable("SELECT StationId As ColOneId, StationCode As ColOneText, StationName As ColTwoText FROM tblNetworkStation WHERE Status = 1 ORDER BY StationName");
                            ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), r.ColOneText); });
                            ChildWnd.TwoColWindShow(dt, "New Company Selection", "Code", "Company");
                        }
                        break;
                    case "HQ Code":
                        {
                            dt = GetDataTable("SELECT BranchId As ColOneId, BranchCode As ColOneText, BranchName As ColTwoText FROM tblBranch ORDER BY BranchCode");
                            ChildWnd.FrmListCol2_Closed += (r => { HQCode = r.ColOneText; });
                            ChildWnd.TwoColWindShow(dt, "HQ Selection", "Code", "Name");
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }

        }

        private void DispSelectedItem(string RefDesc, string StationnCode)
        {
            GSTNum = "";
            try
            {
                if (RefDesc == "Company" && StationnCode != null)
                {
                    SqlDataReader dr = GetDataReader($"SELECT * FROM tblStationProfile WHERE UPPER(RTRIM(CompCode)) = '{StationnCode.Trim().ToUpper()}' AND ToDate IS NULL");
                    if (dr.Read())
                    {
                        CompanyId = Convert.ToInt32(dr["CompId"]);
                        CompanyName = dr["CompName"].ToString();
                        CompanySubName = dr["SubName"].ToString();
                        CompanyCode = dr["CompCode"].ToString().Trim().ToUpper();
                        CompanyType = dr["CompType"].ToString();
                        RegNum = dr["RegNum"].ToString();
                        IATACode = dr["IATACode"].ToString();
                        HQCode = dr["HQCode"].ToString();
                        Address1 = dr["Address1"].ToString();
                        Address2 = dr["Address2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        ZipCode = dr["ZipCode"].ToString();
                        PhoneNum = dr["PhoneNum"].ToString();
                        Fax = dr["Fax"].ToString();
                        EMail = dr["EMail"].ToString();
                        GSTNum = dr["TaxNo"].ToString();
                        CashAccNum = dr["CashAccNum"].ToString();
                    }
                }
                else if (RefDesc == "New Company" && StationnCode != null)
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.StationId, a.StationName, a.StationCode, a.StationType, a.RegNum, a.IATACode, a.BranchCode, b.Address1, b.Address2, b.City, b.State, b.Country, b.ZipCode, b.PhoneNum, b.Fax, b.EMail FROM tblNetworkStation a" +
                        $" LEFT JOIN tblAddressStation b on a.StationId = b.StationId" +
                        $" WHERE UPPER(RTRIM(a.StationCode)) = '{StationnCode.Trim().ToUpper()}'");

                    if (dr.Read())
                    {
                        CompanyId = 0;
                        CompanyName = dr["StationName"].ToString();
                        CompanyName = dr["StationCode"].ToString();
                        CompanyCode = dr["StationCode"].ToString().Trim().ToUpper();
                        CompanyType = dr["StationType"].ToString();
                        RegNum = dr["RegNum"].ToString();
                        IATACode = dr["IATACode"].ToString();
                        HQCode = dr["BranchCode"].ToString();
                        Address1 = dr["Address1"].ToString();
                        Address2 = dr["Address2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        ZipCode = dr["ZipCode"].ToString();
                        PhoneNum = dr["PhoneNum"].ToString();
                        Fax = dr["Fax"].ToString();
                        EMail = dr["EMail"].ToString();
                        GSTNum = "";
                    }
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        static readonly string[] ValidatedProperties = { "CompanyName", "CompanyCode", "CashAccNum" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    string errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "CompanyName": if (string.IsNullOrEmpty(CompanyName)) { error = "CompanyName is required"; MyWind.txtCompanyName.Focus(); } break;
                case "CompanyCode": if (string.IsNullOrEmpty(CompanyCode)) { error = "CompanyCode is required"; MyWind.txtCompanyCode.Focus(); } break;
                case "CashAccNum": if (string.IsNullOrEmpty(CashAccNum)) { error = "AccNum is required"; MyWind.txtAccNum.Focus(); } break;
                default: throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        private void SaveData()
        {
            if (IsValid)
            {
                MessageBoxResult MsgRes;
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblStationProfile WHERE CompId = {CompanyId}");
                if (dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm Modify this Company?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblStationProfile SET" +
                            $" CompName = '{CompanyName}'," +
                            $" SubName = '{CompanySubName}'," +
                            $" CompCode = '{CompanyCode}'," +
                            $" CompType = '{CompanyType}'," +
                            $" CashAccNum = '{CashAccNum}'," +
                            $" RegNum = '{RegNum}'," +
                            $" IATACode = '{IATACode}'," +
                            $" HQCode = '{HQCode}'," +
                            $" Address1 = '{Address1}'," +
                            $" Address2 = '{Address2}'," +
                            $" Address3 = '{""}'," +
                            $" City = '{City}'," +
                            $" State = '{State}'," +
                            $" Country = '{Country}'," +
                            $" ZipCode = '{ZipCode}'," +
                            $" PhoneNum = '{PhoneNum}'," +
                            $" Fax = '{Fax}'," +
                            $" EMail = '{EMail}'," +
                            $" TaxTitle = '{"SST No"}'," +
                            $" TaxNo = '{GSTNum}'," +
                            $" ModifyBy = '{LoginUserId}'," +
                            $" ModifyDate = GETDATE()," +
                            $" Status = 1," +
                            $" IsExported = 0" +
                            $" WHERE CompId = {CompanyId}");
                        MessageBox.Show("Company modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                        MyCompanyCode = CompanyCode;
                        MyCompanyName = CompanyName;
                        MyCompanySubName = CompanySubName;
                        MyStateName = State;
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Create this Company?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    {
                        if (MsgRes == MessageBoxResult.Yes)
                        {
                            ExecuteQuery($"INSERT INTO tblStationProfile (CompName, SubName, CompCode, CompType, CashAccNum, RegNum, IATACode, HQCode, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, Fax, EMail, TaxTitle, TaxNo, FromDate, ToDate, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported)" +
                                $" VALUES('{CompanyName}', '{CompanySubName}', '{CompanyCode}', '{CompanyType}', '{CashAccNum}', '{RegNum}', '{IATACode}', '{HQCode}', '{Address1}', '{Address2}', '{""}', '{City}', '{State}', '{Country}', '{ZipCode}', '{PhoneNum}', '{Fax}', '{EMail}', '{"SST No"}', '{GSTNum}', GETDATE(), NULL, {LoginUserId}, GETDATE(), 0, NULL, 1, 0)");

                            dr = GetDataReader($"SELECT MAX(CompId) As LastId FROM tblStationProfile");
                            CompanyId = dr.Read() ? Convert.ToInt32(dr["LastId"]) : 1; dr.Close();

                            MessageBox.Show("Company Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }

                }
            }
        }
    }
}
