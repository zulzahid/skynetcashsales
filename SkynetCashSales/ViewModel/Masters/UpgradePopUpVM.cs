﻿using Ionic.Zip;
using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    class UpgradePopUpVM : AMGenFunction
    {
        private FrmUpgradePopUp MyWind;
        public UpgradePopUpVM() 
        { 
        
        }

        public UpgradePopUpVM(FrmUpgradePopUp wind)
        {
            MyWind = wind;
            VersionNo = CurrentVersion;

            InitializeBackgroundWorker();

            if (cssupgrade.IsCumpolsory == true)
                MyWind.btnCancel.IsEnabled = false;
            else
                MyWind.btnCancel.IsEnabled = true;
        }

        public string VersionNo { get { return GetValue(() => VersionNo); } set { SetValue(() => VersionNo, value); OnPropertyChanged("VersionNo"); } }
        public string UpdateVersion { get { return GetUpdateVersion(); } }
        public string Features { get { return GetValue(() => Features); } set { SetValue(() => Features, value); OnPropertyChanged("Features"); } }
        public Visibility UpdatingLabelVisibility { get { return GetValue(() => UpdatingLabelVisibility); } set { SetValue(() => UpdatingLabelVisibility, value); OnPropertyChanged("UpdatingLabelVisibility"); } }
        public bool btnEnabled { get { return GetValue(() => btnEnabled); } set { SetValue(() => btnEnabled, value); OnPropertyChanged("btnEnabled"); } }
        private string DBFileName = "";

        private ICommand _Buttons;
        public ICommand Buttons { get { _Buttons = new RelayCommand(Parameter => ExecuteButtons(Parameter)); return _Buttons; } }

        private void ExecuteButtons(object obj)
        {
            switch (obj.ToString())
            {
                case "Update": worker.RunWorkerAsync(); break;
                case "Cancel":
                    {
                        if (cssupgrade.IsCumpolsory == true)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                MyWind.Close();
                                Application.Current.Shutdown();
                            });
                        }
                        else { MyWind.Close(); }
                    }
                    break;
                default: throw new Exception("Unexpected Command");
            }
        }

        private string GetUpdateVersion()
        {
            string NewFeatures = cssupgrade.Features;
            StringReader strReader = new StringReader(NewFeatures);
            string ReleasesTo = strReader.ReadLine(); // First line (Changes at <version> :)
            ReleasesTo = strReader.ReadLine(); // Second line (1. ReleasedTo : <ALL/CR3,CR4,CR6>)

            if (ReleasesTo.Contains("ReleasedTo"))
                ReleasesTo = ReleasesTo.Split(':')[1].ToString().Trim();
            else
                ReleasesTo = "ALL";

            //if (IsNewVersion(cssupgrade))
            if (MyCompanyCode.Trim() == "TEST" || ReleasesTo == "ALL" || ReleasesTo.Contains(MyCompanyCode.Trim()) || (ReleasesTo == "EAST MALAYSIA" && (MyStateName == "SABAH" || MyStateName == "SARAWAK")) || (ReleasesTo == "WEST MALAYSIA" && (MyStateName != "SABAH" && MyStateName != "SARAWAK")))
            {
                DBFileName = cssupgrade.FileName;
                Features = cssupgrade.Features.ToString() == null ? "" : cssupgrade.Features.ToString();
                return cssupgrade.VersionNo.ToString();
            }
            else { return "UptoDate"; }
        }

        private BackgroundWorker worker = new BackgroundWorker();
        private void InitializeBackgroundWorker()
        {
            UpdatingLabelVisibility = Visibility.Collapsed;
            btnEnabled = true;
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdatingLabelVisibility = Visibility.Visible;
            btnEnabled = false;
            Update();
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MyWind.UpdatingLabel.Visibility = Visibility.Hidden;
            btnEnabled = true;
        }

        public void Update()
        {
            string currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var process = Process.GetCurrentProcess(); // Or whatever method you are using
            string fullPath = process.MainModule.FileName;
            string NewExe = "";
            try
            {
                Download(currentPath);

                using (ZipFile zip1 = ZipFile.Read(DBFileName))
                {
                    foreach (ZipEntry e in zip1)
                    {
                        var CheckExtension = Path.GetExtension(e.FileName);
                        if (CheckExtension == ".exe")
                        {
                            NewExe = e.FileName;
                            if (!File.Exists(e.FileName))
                            {
                                e.Extract(currentPath, ExtractExistingFileAction.OverwriteSilently);                                
                            }
                        }
                        else if (CheckExtension == ".dll")
                        {
                            if (!File.Exists(e.FileName))
                            {                                
                                e.Extract(currentPath, ExtractExistingFileAction.OverwriteSilently);                                
                            }
                            else
                            {
                                FileInfo file = new FileInfo(e.FileName);
                                if (File.Exists(e.FileName + "_Copy.dll"))
                                {
                                    File.Delete(e.FileName + "_Copy.dll");
                                }

                                file.MoveTo(e.FileName + "_Copy.dll");
                                e.Extract(currentPath, ExtractExistingFileAction.OverwriteSilently);
                            }
                        }
                        else
                        {
                            if (!File.Exists(e.FileName))
                            {
                                e.Extract(currentPath, ExtractExistingFileAction.OverwriteSilently);
                            }
                        }
                    }
                }

                File.Delete(DBFileName);
                cssupgrade.IsUpdated = true;
                cssupgrade.UpdatedDate = DateTime.Now;

                System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    CloseWind();
                    if (File.Exists("SkynetCashSales.exe"))
                    {
                        Process.Start("SkynetCashSales.exe");
                    }
                    Application.Current.Shutdown();
                });
            }
            catch (Exception ex)
            {
                error_log.errorlog("Update:" + ex.ToString());
                MessageBox.Show("Update:" + ex.ToString());
                File.Delete(DBFileName);
                MessageBox.Show("Check Internet connection or something went wrong. Contact support");
            }
        }

        private void Download(string SourceFile)
        {
            var DecrypPass = Decrypt(cssupgrade.Ftp_Password);
            string UploadPath = cssupgrade.Ftp_URL + "/CSS Versions/" + DBFileName;
            string Username = cssupgrade.Ftp_UserName;
            string Password = DecrypPass;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(UploadPath);
            request.Credentials = new NetworkCredential(Username, Password);

            request.UseBinary = true;
            request.UsePassive = false;
            request.KeepAlive = false;

            request.Method = WebRequestMethods.Ftp.GetFileSize;
            request.Proxy = null;

            try
            {
                long fileSize; // this is the key for ReportProgress
                using (WebResponse resp = request.GetResponse()) fileSize = resp.ContentLength;

                request = (FtpWebRequest)WebRequest.Create(UploadPath);
                request.Credentials = new NetworkCredential(Username, Password);

                request.Method = WebRequestMethods.Ftp.DownloadFile;

                using (FtpWebResponse responseFileDownload = (FtpWebResponse)request.GetResponse())
                using (Stream responseStream = responseFileDownload.GetResponseStream())
                using (FileStream writeStream = new FileStream(SourceFile + "\\" + DBFileName, FileMode.Create))
                {
                    int Length = 2048;
                    Byte[] buffer = new Byte[Length];
                    int bytesRead = responseStream.Read(buffer, 0, Length);
                    int bytes = 0;

                    while (bytesRead > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                        bytesRead = responseStream.Read(buffer, 0, Length);
                        bytes += bytesRead;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

    }
}
