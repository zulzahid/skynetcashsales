﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class StaffDiscountVM : AMGenFunction
    {
        public FrmStaffDiscount MyWind;
        public StaffDiscountVM(FrmStaffDiscount Wind)
        {
            MyWind = Wind;
            StaffDiscountDetails = new ObservableCollection<StaffDiscountInfo>();
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; MyWind.btnDelete.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; MyWind.btnDelete.Visibility = Visibility.Visible; }
        }

        public long StaffDiscID { get { return GetValue(() => StaffDiscID); } set { SetValue(() => StaffDiscID, value); OnPropertyChanged("StaffDiscID"); } }
        public string Code { get { return GetValue(() => Code); } set { SetValue(() => Code, value); OnPropertyChanged("Code"); } }
        public bool IsZoneCode { get { return GetValue(() => IsZoneCode); } set { SetValue(() => IsZoneCode, value); OnPropertyChanged("IsZoneCode"); } }
        public bool IsDestCode { get { return GetValue(() => IsDestCode); } set { SetValue(() => IsDestCode, value); OnPropertyChanged("IsDestCode"); } }
        public bool IsPercentageDisc { get { return GetValue(() => IsPercentageDisc); } set { SetValue(() => IsPercentageDisc, value); OnPropertyChanged("IsPercentageDisc"); } }
        public bool IsAmountDesc { get { return GetValue(() => IsAmountDesc); } set { SetValue(() => IsAmountDesc, value); OnPropertyChanged("IsAmountDesc"); } }
        public decimal DiscountValue { get { return GetValue(() => DiscountValue); } set { SetValue(() => DiscountValue, value); OnPropertyChanged("DiscountValue"); } }
        public int MonthlyValids { get { return GetValue(() => MonthlyValids); } set { SetValue(() => MonthlyValids, value); OnPropertyChanged("MonthlyValids"); } }
        public string FromDate { get { return GetValue(() => FromDate); } set { SetValue(() => FromDate, value); OnPropertyChanged("FromDate"); } }
        public string ToDate { get { return GetValue(() => ToDate); } set { SetValue(() => ToDate, value); OnPropertyChanged("ToDate"); } }

        private ObservableCollection<StaffDiscountInfo> _StaffDiscountDetails = new ObservableCollection<StaffDiscountInfo>();
        public ObservableCollection<StaffDiscountInfo> StaffDiscountDetails { get { return _StaffDiscountDetails; } set { _StaffDiscountDetails = value; } }

        private StaffDiscountInfo _SelectedItem = new StaffDiscountInfo();
        public StaffDiscountInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "Code":
                        {
                            if (IsZoneCode == true)
                            {
                                dt = GetDataTable($"SELECT ColOneId = a.ZoneId, ColOneText = a.ZoneCode, ColTwoText = a.ZoneDesc FROM tblZone a");
                                ChildWnd.FrmListCol2_Closed += (r => { Code = r.ColOneText; });
                                ChildWnd.TwoColWindShow(dt, "Zone Selection", "Zone", "Description");
                            }
                            else if (IsDestCode == true)
                            {
                                dt = GetDataTable($"SELECT ColOneId = a.DestId, ColOneText = a.DestCode, ColTwoText = a.DestName FROM tblDestination a");
                                ChildWnd.FrmListCol2_Closed += (r => { Code = r.ColOneText; });
                                ChildWnd.TwoColWindShow(dt, "Destination Selection", "Destination", "Description");
                            }
                        }
                        break;
                    case "Load Selected Record": if (SelectedItem != null) DispSelectedItem(SelectedItem.StaffDiscID); break;
                    case "Save":
                        {
                            string auth = StrAuthenticateType();
                            if (!string.IsNullOrEmpty(auth) && auth != "User")
                            {
                                SaveData();
                            }
                        }
                        break;
                    case "Delete":
                        {
                            string auth = StrAuthenticateType();
                            if (!string.IsNullOrEmpty(auth) && auth != "User")
                            {
                                DeleteData(SelectedItem.StaffDiscID);
                            }
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
           
        private void DispSelectedItem(long RefId)
        {
            try
            {
                if (RefId > 0)
                {
                    var query = (from a in StaffDiscountDetails where a.StaffDiscID == RefId select a).FirstOrDefault();
                    if (query != null)
                    {
                        StaffDiscID = query.StaffDiscID;
                        Code = query.Code;
                        IsZoneCode = query.IsZone;
                        IsDestCode = !IsZoneCode;
                        IsPercentageDisc = query.IsPercentage;
                        IsAmountDesc = !IsPercentageDisc;
                        DiscountValue = query.DiscountValue;
                        MonthlyValids = query.MonthlyValids;
                        FromDate = query.FromDate.ToString();
                        ToDate = query.ToDate.ToString();
                    }
                    MyWind.txtCode.Focus();
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }
        private void ResetData()
        {
            StaffDiscID = 0; Code = ""; IsZoneCode = true;
            IsPercentageDisc = true;
            DiscountValue = 0; MonthlyValids = 3;
            FromDate = ToDate = string.Empty;
            StaffDiscountDetails.Clear();

            SqlDataReader dr = GetDataReader($"SELECT a.StaffDiscID, a.IsZone, a.Code, a.IsPercentage, a.DiscountValue, a.MonthlyValid, a.FromDate, a.ToDate, a.Status FROM tblStaffDiscount a" +
                $" WHERE a.Status = 1 AND a.ToDate IS NULL ORDER BY a.Code");
            while (dr.Read())
            {
                StaffDiscountDetails.Add(new StaffDiscountInfo { StaffDiscID = Convert.ToInt64(dr["StaffDiscID"]), IsZone = Convert.ToBoolean(dr["IsZone"]), Code = dr["Code"].ToString(), IsPercentage = Convert.ToBoolean(dr["IsPercentage"]), DiscountValue = Convert.ToDecimal(dr["DiscountValue"]), MonthlyValids = Convert.ToInt32(dr["MonthlyValid"]), FromDate = Convert.ToDateTime(dr["FromDate"]).ToString("dd-MMM-yyyy"), ToDate = dr["ToDate"] != DBNull.Value ? Convert.ToDateTime(dr["ToDate"]).ToString("dd-MMM-yyyy") : "", Status = Convert.ToBoolean(dr["Status"]) });
            }
            dr.Close();

            MyWind.txtCode.Focus();
        }

        private void SaveData()
        {
            string dtf = !string.IsNullOrEmpty(FromDate) ? ("'" + Convert.ToDateTime(FromDate).ToString("MM/dd/yyyy") + "'") : ("'" + DateTime.Now.ToString("MM/dd/yyyy") + "'");
            string dtt = !string.IsNullOrEmpty(ToDate) ? ("'" + Convert.ToDateTime(ToDate).ToString("MM/dd/yyyy") + "'") : "NULL";

            if (Code.Trim() != "")
            {
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblStaffDiscount where StaffDiscID = {StaffDiscID}");
                if (!dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm Save this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblStaffDiscount(IsZone, Code, IsPercentage, DiscountValue, MonthlyValid, FromDate, ToDate, CreatedBy, CreatedDate, IsExported, ExportedDate, Status)" +
                            $" VALUES('{IsZoneCode}', '{Code}', '{IsPercentageDisc}', {DiscountValue}, {MonthlyValids}, {dtf}, {dtt}, '{LoginUserName}', GETDATE(), 0, null, 1)");

                        MessageBox.Show("Saved Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        ResetData();
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Modify this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblStaffDiscount SET" +
                            $" IsZone = '{IsZoneCode}'," +
                            $" Code = '{Code}'," +
                            $" IsPercentage = '{IsPercentageDisc}'," +
                            $" DiscountValue = {DiscountValue}," +
                            $" FromDate = {dtf}," +
                            $" ToDate = {dtt}," +
                            $" ModifiedBy = '{LoginUserName}'," +
                            $" ModifiedDate = GETDATE()," +
                            $" Status = 1" +
                            $" where StaffDiscID = {StaffDiscID}");
                        MessageBox.Show("Data Modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        ResetData();
                    }
                }
            }
            else MessageBox.Show("Invalid Data to Save", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void DeleteData(long StaffDiscID)
        {
            if (StaffDiscID > 0)
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Delete this data?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    ExecuteQuery($"UPDATE tblStaffDiscount SET ToDate = GETDATE(), Status = 0, IsExported = 0 where StaffDiscID = {StaffDiscID}");
                }
                ResetData();
            }
        }
        public class StaffDiscountInfo : AMGenFunction
        {
            public long StaffDiscID { get { return GetValue(() => StaffDiscID); } set { SetValue(() => StaffDiscID, value); OnPropertyChanged("StaffDiscID"); } }
            public bool IsZone { get { return GetValue(() => IsZone); } set { SetValue(() => IsZone, value); OnPropertyChanged("IsZone"); } }
            public string Code { get { return GetValue(() => Code); } set { SetValue(() => Code, value); OnPropertyChanged("Code"); } }
            public bool IsPercentage { get { return GetValue(() => IsPercentage); } set { SetValue(() => IsPercentage, value); OnPropertyChanged("IsPercentage"); } }
            public decimal DiscountValue { get { return GetValue(() => DiscountValue); } set { SetValue(() => DiscountValue, value); OnPropertyChanged("DiscountValue"); } }
            public int MonthlyValids { get { return GetValue(() => MonthlyValids); } set { SetValue(() => MonthlyValids, value); OnPropertyChanged("MonthlyValids"); } }
            public string FromDate { get { return GetValue(() => FromDate); } set { SetValue(() => FromDate, value); OnPropertyChanged("FromDate"); } }
            public string ToDate { get { return GetValue(() => ToDate); } set { SetValue(() => ToDate, value); OnPropertyChanged("ToDate"); } }
            public bool Status { get { return GetValue(() => Status); } set { SetValue(() => Status, value); OnPropertyChanged("Status"); } }
        }
    }
}
