﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class PostCodeVM : AMGenFunction
    {
        public FrmPostCode MyWind;
        public PostCodeVM(FrmPostCode Wind)
        {
            MyWind = Wind;
            PostcodeDetails = new ObservableCollection<PostcodeInfo>();
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        public int PostCodeId { get { return GetValue(() => PostCodeId); } set { SetValue(() => PostCodeId, value); OnPropertyChanged("PostCodeId"); } }
        public string PostCode { get { return GetValue(() => PostCode); } set { SetValue(() => PostCode, value); OnPropertyChanged("PostCode"); } }
        public string Town { get { return GetValue(() => Town); } set { SetValue(() => Town, value); OnPropertyChanged("Town"); } }
        public string City { get { return GetValue(() => City); } set { SetValue(() => City, value); OnPropertyChanged("City"); } }
        public string State { get { return GetValue(() => State); } set { SetValue(() => State, value); OnPropertyChanged("State"); } }
        public string DelStation { get { return GetValue(() => DelStation); } set { SetValue(() => DelStation, value); OnPropertyChanged("DelStation"); } }
        public string DeliveryFreq { get { return GetValue(() => DeliveryFreq); } set { SetValue(() => DeliveryFreq, value); OnPropertyChanged("DeliveryFreq"); } }
        public string Remarks { get { return GetValue(() => Remarks); } set { SetValue(() => Remarks, value); OnPropertyChanged("Remarks"); } }

        private ObservableCollection<PostcodeInfo> _PostcodeDetails = new ObservableCollection<PostcodeInfo>();
        public ObservableCollection<PostcodeInfo> PostcodeDetails { get { return _PostcodeDetails; } set { _PostcodeDetails = value; } }

        //private PostcodeInfo _SelectedPostcode = new PostcodeInfo();
        //public PostcodeInfo SelectedPostcode { get { return _SelectedPostcode; } set { if (_SelectedPostcode != value) { _SelectedPostcode = value; OnPropertyChanged("SelectedItem"); } if (_SelectedPostcode != null) DispSelectedItem(); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "PostCode Id":
                        {
                            dt = GetDataTable($"SELECT Id as ColOneId, postcode as ColOneText , town as ColTwoText, city as ColThreeText, state as ColFourText, stncode as ColFiveText FROM PostCode2020 order by postcode asc");
                            ChildWnd.FrmListCol5_Closed += (r => { PostCodeId = Convert.ToInt32(r.ColOneText); LoadData(); });
                            ChildWnd.FiveColWindShow(dt, "Postcode Selection", "PostCode", "Town", "City", "State", "StnCode");
                        } break;
                    case "Town":
                        {
                            dt = GetDataTable("SELECT A.CityId As ColOneId, A.CityName As ColOneText, B.StateName As ColTwoText, C.CountryName As ColThreeText FROM City A" +
                                " LEFT JOIN State B ON A.StateCode = B.StateCode" +
                                " LEFT JOIN Country C ON B.CountryCode = C.CountryCode3" +
                                " ORDER BY A.CityName");
                            ChildWnd.FrmListCol3_Closed += (r => { Town = r.ColOneText; State = r.ColTwoText; });
                            ChildWnd.ThreeColWindShow(dt, "Town Selection", "Town", "State", "Country");
                        } break;
                    case "State":
                        {
                            dt = GetDataTable("SELECT A.StateId As ColOneId, A.StateName As ColOneText, B.CountryName As ColTwoText FROM State A" +
                                " LEFT JOIN Country B ON A.CountryCode = B.CountryCode3" +
                                " ORDER BY A.StateName");
                            ChildWnd.FrmListCol2_Closed += (r => { State = r.ColOneText; });
                            ChildWnd.TwoColWindShow(dt, "State Selection", "State", "Country");
                        } break;
                    case "Station Code":
                        {
                            dt = GetDataTable("SELECT StationId As ColOneId, StationCode As ColOneText, StationName As ColTwoText FROM NetworkStation ORDER BY StationName");
                            ChildWnd.FrmListCol2_Closed += (r => { DelStation = r.ColOneText; });
                            ChildWnd.TwoColWindShow(dt, "Delivery Station Selection", "Station Code", "Station Name");
                        } break;
                    case "Save": if(IsValidate) SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: throw new Exception("Unexpected Parameter on Service");
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void SaveData()
        {
            if (LoginUserAuthentication != "User")
            {
                if (PostCodeId == 0)
                {
                    SqlDataReader dr = GetDataReader("SELECT MAX(Id) As LastId FROM tblPostCode2020");
                    if (dr.Read())
                        PostCodeId = Convert.ToInt32(dr["LastId"]) + 1;
                    else
                        PostCodeId = 2000;
                    dr.Close();

                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Create this PostCode?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery("INSERT INTO tblPostCode2020 ([postcode],[postcodeid],[town],[city],[state],[stncode],[deliveryFrequency],[remarks],[isdelete],[timeTransmitted],[timeReceived])" +
                            " VALUES('" + PostCode + "','" + PostCodeId + "', '" + Town + "', '" + City + "', '" + State + "', '" + DelStation + "', '" + DeliveryFreq + "', '" + Remarks + "', '0', null, null,null )");
                        MessageBox.Show("PostCode Created Successfully");
                        ResetData();
                    }
                }
                else if (PostCodeId > 0)
                {
                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Modify this PostCode?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery("UPDATE PostCode2020 SET Town = '" + Town + "', PostCode = '" + PostCode + "', City = '" + City + "', State = '" + State + "', StnCode = '" + DelStation + "', deliveryFrequency = '" + DeliveryFreq + "', remarks = '" + Remarks + "', Isdelete = 0, timeTransmitted = NULL, timeReceived = NULL WHERE Id = " + PostCodeId);
                        MessageBox.Show("PostCode Modified Successfully");
                        ResetData();
                    }
                }
            }
        }

        private void LoadData()
        {
            if (PostCodeId > 0)
            {
                SqlDataReader dr = GetDataReader($"SELECT PostCodeId, PostCode, Town,City, State, StnCode, deliveryFrequency, remarks FROM PostCode2020 WHERE PostCodeId = " + PostCodeId);
                if (dr.Read())
                {
                    PostCodeId = Convert.ToInt32(dr["Id"]);
                    PostCode = dr["PostCode"].ToString();
                    City = dr["City"].ToString();
                    Town = dr["Town"].ToString();
                    State = dr["State"].ToString();
                    DelStation = dr["StnCode"].ToString();
                    DeliveryFreq = dr["deliveryFrequency"].ToString();
                    Remarks = dr["remarks"].ToString();
                }
                dr.Close();
            }
        }

        //private void DispSelectedItem()
        //{
        //    FromPostCode = SelectedPostcode.FromCode; ToPostCode = SelectedPostcode.ToCode;
        //    Town = SelectedPostcode.Town;
        //    State = SelectedPostcode.State;
        //    DelStation = SelectedPostcode.StnCode;
        //    FreqDocument = SelectedPostcode.FreqDocument;
        //    FreqParcel = SelectedPostcode.FreqParcel;
        //}

        private void ResetData()
        {
            PostCodeId = 0; PostCode = ""; Town = ""; City = ""; State = ""; DelStation = ""; DeliveryFreq = ""; Remarks = "";
            PostcodeDetails.Clear();

            DataTable dt;
            using (dt = new DataTable())
            {
                dt = GetDataTable("SELECT PostCodeId, PostCode, Town, City, State, StnCode, deliveryFrequency FROM tblPostcode2020 WHERE isdelete = 0");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PostcodeDetails.Add(new PostcodeInfo { Item = PostcodeDetails.Count + 1, PostCodeId = Convert.ToInt32(dt.Rows[i]["PostCodeId"]), PostCode = dt.Rows[i]["PostCode"].ToString(), Town = dt.Rows[i]["Town"].ToString(), City = dt.Rows[i]["City"].ToString(), State = dt.Rows[i]["State"].ToString(), StnCode = dt.Rows[i]["StnCode"].ToString(), DeliveryFreq = dt.Rows[i]["deliveryFrequency"].ToString() });
                }
            }

            MyWind.txtPostCode.Focus();
        }

        static readonly string[] ValidatedProperties = { "PostCode", "Town", "City", "State", "DelStation", "DeliveryFreq" };
        public bool IsValidate
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null) // there is an error
                        return false;
                }
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "PostCode": if (string.IsNullOrEmpty(PostCode)) { error = "Invalid From PostCode"; MyWind.txtPostCode.Focus(); } break;
                case "Town": if (string.IsNullOrEmpty(Town)) { error = "Invalid Town"; MyWind.txtTown.Focus(); } break;
                case "City": if (string.IsNullOrEmpty(City)) { error = "Invalid City"; MyWind.txtCity.Focus(); } break;
                case "State": if (string.IsNullOrEmpty(State)) { error = "Invalid State"; MyWind.txtState.Focus(); } break;
                case "DelStation": if (string.IsNullOrEmpty(DelStation)) { error = "Invalid DelStation"; MyWind.txtDelStation.Focus(); } break;
                case "DeliveryFreq": if (string.IsNullOrEmpty(DeliveryFreq)) { error = "Invalid Delivery Freq"; MyWind.txtDeliveryFreq.Focus(); } break;
                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) MessageBox.Show(error);
            return error;
        }

        public class PostcodeInfo : AMGenFunction
        {
            public int Item { get; set; }
            public int PostCodeId { get; set; }
            public string PostCode { get; set; }
            public string Town { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string StnCode { get; set; }
            public string DeliveryFreq { get; set; }
        }

    }
}
