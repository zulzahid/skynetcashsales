﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class ConsignorVM : AMGenFunction
    {
        FrmConsignor Wndow;
        public ConsignorVM(FrmConsignor frm)
        {
            Wndow = frm;
            ClearData();
        }

        public string ConsignorCode { get { return GetValue(() => ConsignorCode); } set { SetValue(() => ConsignorCode, value); OnPropertyChanged("ConsignorCode"); } }
        public string ConsignorName { get { return GetValue(() => ConsignorName); } set { SetValue(() => ConsignorName, value); OnPropertyChanged("ConsignorName"); } }
        public string GSTNumber { get { return GetValue(() => GSTNumber); } set { SetValue(() => GSTNumber, value); OnPropertyChanged("GSTNumber"); } }
        public string Address1 { get { return GetValue(() => Address1); } set { SetValue(() => Address1, value); OnPropertyChanged("Address1"); } }
        public string Address2 { get { return GetValue(() => Address2); } set { SetValue(() => Address2, value); OnPropertyChanged("Address2"); } }
        public string City { get { return GetValue(() => City); } set { SetValue(() => City, value); OnPropertyChanged("City"); } }
        public string State { get { return GetValue(() => State); } set { SetValue(() => State, value); OnPropertyChanged("State"); } }
        public string Country { get { return GetValue(() => Country); } set { SetValue(() => Country, value); OnPropertyChanged("Country"); } }
        public string PostalCode { get { return GetValue(() => PostalCode); } set { SetValue(() => PostalCode, value); OnPropertyChanged("PostalCode"); } }
        public string Mobile { get { return GetValue(() => Mobile); } set { SetValue(() => Mobile, value); OnPropertyChanged("Mobile"); } }
        public string Phone { get { return GetValue(() => Phone); } set { SetValue(() => Phone, value); OnPropertyChanged("Phone"); } }
        public string Fax { get { return GetValue(() => Fax); } set { SetValue(() => Fax, value); OnPropertyChanged("Fax"); } }
        public string Email { get { return GetValue(() => Email); } set { SetValue(() => Email, value); OnPropertyChanged("Email"); } }
        
        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }
        public void ExecuteCommandGen(object ObjCommand)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                string CaseName = "", ControlName = "";
                string[] StrParameter = ObjCommand.ToString().Split('-');
                if (StrParameter.Length > 1) { CaseName = StrParameter[0]; ControlName = StrParameter[1]; } else { CaseName = StrParameter[0]; }

                switch (CaseName)
                {
                    case "Consignor":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.ConsignorID, ColOneText = a.ConsignorCode, ColTwoText = a.Name, ColThreeText = a.City, ColFourText = a.Mobile FROM tblConsignor a ORDER BY a.Name");
                            ChildWnd.FrmListCol4_Closed += (r => { ConsignorCode = r.ColOneText; LoadData(); });
                            ChildWnd.FourColWindShow(dt, "Consignor Selection", "Consignor Code", "Consignor Name", "City", "Mobile");
                        } break;
                    case "Submit": using (new WaitCursor()) { SubmitData(); } break;
                    case "Clear": ClearData(); break;
                    case "Print": break;
                    case "Close":CloseWind();break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private static string errormsg = "";
        static readonly string[] ValidatedProperties = { "ConsignorName", "GSTNumber", "Address1", "Mobile" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null) // there is an error
                        return false;
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "ConsignorName": if (string.IsNullOrEmpty(ConsignorName)) error = "Consignor name is required"; Wndow.txtConsignorName.Focus(); break;
                case "GSTNumber": if (string.IsNullOrEmpty(GSTNumber)) error = "Consignor GST number is required"; Wndow.txtConsignorGSTNumber.Focus(); break;
                case "Address1": if (string.IsNullOrEmpty(Address1)) error = "Consignor address is required"; Wndow.txtConsignorAddline1.Focus(); break;
                case "Mobile": if (string.IsNullOrEmpty(Mobile)) error = "Mobile is required"; Wndow.txtConsignorMobile.Focus(); break;
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        private void LoadData()
        {
            try
            {
                if (!string.IsNullOrEmpty(ConsignorCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT * FROM tblConsignor WHERE RTRIM(ConsignorCode) = '{ConsignorCode.Trim()}'");
                    if (dr.Read())
                    {
                        ConsignorName = dr["Name"].ToString();
                        Address1 = dr["Add1"].ToString();
                        Address2 = dr["Add2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        PostalCode = dr["PostalCode"].ToString();
                        Phone = dr["Phone"].ToString();
                        Mobile = dr["Mobile"].ToString();
                        Fax = dr["Fax"].ToString();
                        Email = dr["Email"].ToString();
                        GSTNumber = dr["GSTNum"].ToString();
                    }
                    dr.Close();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Consignor load Function : " + Ex.ToString()); }
        }

        private void SubmitData()
        {
            try
            {
                if (IsValid)
                {
                    string CodeConsignor = "";
                    if (string.IsNullOrEmpty(ConsignorCode))
                    {
                        SqlDataReader dr = GetDataReader($"SELECT * FROM tblConsignor WHERE Name = '{ConsignorName}' AND Mobile = '{Mobile}'");
                        if (dr.HasRows)
                        {
                            dr.Read();
                            CodeConsignor = dr["ConsignorCode"].ToString();
                            dr.Close();

                            ExecuteQuery($"UPDATE tblConsignor SET " +
                                $" Add1 = '{Address1}'," +
                                $" Add2 = '{Address2}'," +
                                $" City = '{City}'," +
                                $" State = '{State}'," +
                                $" Country = '{Country}'," +
                                $" PostalCode = '{PostalCode}'," +
                                $" Phone = '{Phone}'," +
                                $" Fax = '{Fax}'," +
                                $" Mobile = '{Mobile}'," +
                                $" Email = '{Email}'," +
                                $" GSTNum = '{GSTNumber}'" +
                                $" WHERE RTRIM(ConsignorCode) = '{CodeConsignor.Trim()}'");
                        }
                        else
                        {
                            dr = GetDataReader($"SELECT MAX(ConsignorCode) As MaxCode FROM tblConsignor");
                            if(dr.Read())
                            {
                                string maxcode = dr["MaxCode"].ToString();
                                int nos = Convert.ToInt32(maxcode);
                                CodeConsignor = (nos + 1).ToString("000000");
                            }
                            else
                            {
                                CodeConsignor = "000001";
                            }

                            ExecuteQuery($"INSERT INTO tblConsignor (ConsignorCode, Name, GSTNum, Add1, Add2, City, State, Country, PostalCode, Mobile, Email, Phone, Fax, CUserID, CDate, Status) " +
                                $" VALUES('{CodeConsignor}', '{ConsignorName}', '{GSTNumber}', '{Address1}', '{Address2}', '{City}', '{State}', '{Country}', '{PostalCode}', '{Mobile}', '{Email}', '{Phone}', '{Fax}', {LoginUserId}, GETDATE(), 1)");
                        }
                    }
                    else
                    {
                        CodeConsignor = ConsignorCode;

                        ExecuteQuery($"UPDATE tblConsignor SET " +
                            $" Add1 = '{Address1}'," +
                            $" Add2 = '{Address2}'," +
                            $" City = '{City}'," +
                            $" State = '{State}'," +
                            $" Country = '{Country}'," +
                            $" PostalCode = '{PostalCode}'," +
                            $" Phone = '{Phone}'," +
                            $" Fax = '{Fax}'," +
                            $" Mobile = '{Mobile}'," +
                            $" Email = '{Email}'," +
                            $" GSTNum = '{GSTNumber}'" +
                            $" WHERE RTRIM(ConsignorCode) = '{CodeConsignor.Trim()}'");
                    }
                    ClearData();
                }
                else { MessageBox.Show(errormsg, "Warning"); }
            }
            catch (Exception Ex) { error_log.errorlog("Consignor creation Submit Function : " + Ex.ToString()); }
        }

        private void ClearData()
        {
            string emptyStr = string.Empty; Phone = emptyStr; Fax = emptyStr;
            ConsignorCode = ConsignorName = GSTNumber = Address1 = Address2 = City = State = Country = PostalCode = Mobile = Email = "";
            Wndow.txtConsignorName.Focus();
        }
    }
}
