﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class StaffVM : AMGenFunction
    {
        public FrmStaff MyWind;
        public StaffVM(FrmStaff Wind)
        {
            MyWind = Wind;
            StaffDetails = new ObservableCollection<StaffInfo>();
            ResetData();
        }

        public bool IsActive { get { return GetValue(() => IsActive); } set { SetValue(() => IsActive, value); OnPropertyChanged("IsActive"); } }
        public long StaffId { get { return GetValue(() => StaffId); } set { SetValue(() => StaffId, value); OnPropertyChanged("StaffId"); } }
        public string StaffName { get { return GetValue(() => StaffName); } set { SetValue(() => StaffName, value); OnPropertyChanged("StaffName"); } }
        public long StaffTypeId { get { return GetValue(() => StaffTypeId); } set { SetValue(() => StaffTypeId, value); OnPropertyChanged("StaffTypeId"); } }
        public string StaffType { get { return GetValue(() => StaffType); } set { SetValue(() => StaffType, value); OnPropertyChanged("StaffType"); } }
        public string StaffCode { get { return GetValue(() => StaffCode); } set { SetValue(() => StaffCode, value); OnPropertyChanged("StaffCode"); } }
        public long Supervisorid { get { return GetValue(() => Supervisorid); } set { SetValue(() => Supervisorid, value); OnPropertyChanged("Supervisorid"); } }
        public string Supervisor { get { return GetValue(() => Supervisor); } set { SetValue(() => Supervisor, value); OnPropertyChanged("Supervisor"); } }
        public string ICNum { get { return GetValue(() => ICNum); } set { SetValue(() => ICNum, value); OnPropertyChanged("ICNum"); } }
        public string SOCSO { get { return GetValue(() => SOCSO); } set { SetValue(() => SOCSO, value); OnPropertyChanged("SOCSO"); } }
        public string EPFNum { get { return GetValue(() => EPFNum); } set { SetValue(() => EPFNum, value); OnPropertyChanged("EPFNum"); } }
        public string TaxFileNum { get { return GetValue(() => TaxFileNum); } set { SetValue(() => TaxFileNum, value); OnPropertyChanged("TaxFileNum"); } }
        public decimal Salary { get { return GetValue(() => Salary); } set { SetValue(() => Salary, value); OnPropertyChanged("Salary"); } }
        public string JoinedDate { get { return GetValue(() => JoinedDate); } set { SetValue(() => JoinedDate, value); OnPropertyChanged("JoinedDate"); } }
        public string LeftDate { get { return GetValue(() => LeftDate); } set { SetValue(() => LeftDate, value); OnPropertyChanged("LeftDate"); } }

        public string Address1 { get { return GetValue(() => Address1); } set { SetValue(() => Address1, value); OnPropertyChanged("Address1"); } }
        public string Address2 { get { return GetValue(() => Address2); } set { SetValue(() => Address2, value); OnPropertyChanged("Address2"); } }
        public string City { get { return GetValue(() => City); } set { SetValue(() => City, value); OnPropertyChanged("City"); } }
        public string State { get { return GetValue(() => State); } set { SetValue(() => State, value); OnPropertyChanged("State"); } }
        public string Country { get { return GetValue(() => Country); } set { SetValue(() => Country, value); OnPropertyChanged("Country"); } }
        public string ZipCode { get { return GetValue(() => ZipCode); } set { SetValue(() => ZipCode, value); OnPropertyChanged("ZipCode"); } }
        public string PhoneNum { get { return GetValue(() => PhoneNum); } set { SetValue(() => PhoneNum, value); OnPropertyChanged("PhoneNum"); } }
        public string Fax { get { return GetValue(() => Fax); } set { SetValue(() => Fax, value); OnPropertyChanged("Fax"); } }
        public string EMail { get { return GetValue(() => EMail); } set { SetValue(() => EMail, value); OnPropertyChanged("EMail"); } }

        private ObservableCollection<StaffInfo> _StaffDetails = new ObservableCollection<StaffInfo>();
        public ObservableCollection<StaffInfo> StaffDetails { get { return _StaffDetails; } set { _StaffDetails = value; } }

        private StaffInfo _SelectedItem = new StaffInfo();
        public StaffInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "Staff Name":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.StaffId, ColOneText = a.StaffCode, ColTwoText = a.StaffName FROM tblStaff a ORDER BY a.StaffName");
                            ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt32(r.Id)); });
                            ChildWnd.TwoColWindShow(dt, "Staff Selection", "Code", "Staff");
                        }
                        break;
                    case "Selected Staff Name": DispSelectedItem(Obj.ToString(), SelectedItem.StaffId); break;
                    case "Staff Type":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenDetId, ColOneText = a.GDDesc FROM tblGeneralDet a" +
                                $" join tblGeneralMaster b on a.GenId = b.GenId" +
                                $" where a.Status = 1 and RTRIM(b.GenDesc) = 'STAFF TYPE' ORDER BY a.GDDesc");

                            ChildWnd.FrmListSingleCol_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt32(r.Id)); StaffTypeId = Convert.ToInt32(r.Id); StaffType = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Staff Type Selection");
                        }
                        break;
                    case "MBZone":
                        {

                        }
                        break;
                    case "Supervisor":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.StaffId, ColOneText = a.StaffName FROM tblStaff a" +
                                $" join tblGeneralDet b on a.StaffTypeId = b.GenDetId" +
                                $" where a.Status = 1 and UPPER(RTRIM(b.GDDesc)) = 'SUPERVISOR' ORDER BY a.StaffName");

                            ChildWnd.FrmListSingleCol_Closed += (r => { Supervisorid = Convert.ToInt32(r.Id); Supervisor = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Supervisor Selection");
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void DispSelectedItem(object Obj, long RefId)
        {
            SqlDataReader dr;
            try
            {
                if ((Obj.ToString() == "Staff Name" || Obj.ToString() == "Selected Staff Name") && RefId > 0)
                {
                    using (dr = GetDataReader($"Select a.StaffId, a.StaffName, a.StaffTypeId, '' as StaffType, a.StaffCode, a.SupervisorId, a.ICNum, a.SOCSO, a.EPFNum, a.TaxFileNum, a.JoinedDate, a.LeftDate, a.Salary, a.Address1, a.Address2, a.City, a.State, a.Country, a.ZipCode, a.PhoneNum, a.Fax, a.EMail, a.Status from tblStaff a" +
                        $" where a.StaffId = {RefId}"))
                    {
                        if (dr.Read())
                        {
                            StaffId = Convert.ToInt64(dr["StaffId"]);
                            StaffName = dr["StaffName"].ToString();
                            StaffTypeId = Convert.ToInt64(dr["StaffTypeId"]);
                            StaffType = dr["StaffType"].ToString();
                            StaffCode = dr["StaffCode"].ToString();
                            Supervisorid = Convert.ToInt64(dr["SupervisorId"]);
                            Supervisor = "";
                            ICNum = dr["ICNum"].ToString();
                            SOCSO = dr["SOCSO"].ToString();
                            EPFNum = dr["EPFNum"].ToString();
                            TaxFileNum = dr["TaxFileNum"].ToString();
                            JoinedDate = Convert.ToDateTime(dr["JoinedDate"]).ToShortDateString();
                            LeftDate = dr["LeftDate"] != DBNull.Value ? Convert.ToDateTime(dr["LeftDate"]).ToShortDateString() : "";
                            Salary = Convert.ToDecimal(dr["Salary"]);
                            Address1 = dr["Address1"].ToString();
                            Address2 = dr["Address2"].ToString();
                            City = dr["City"].ToString();
                            State = dr["State"].ToString();
                            Country = dr["Country"].ToString();
                            ZipCode = dr["ZipCode"].ToString();
                            PhoneNum = dr["PhoneNum"].ToString();
                            Fax = dr["Fax"].ToString();
                            EMail = dr["EMail"].ToString();
                            IsActive = Convert.ToBoolean(dr["Status"]);
                        }
                        dr.Close();
                    }

                    if (StaffTypeId > 0)
                    {
                        dr = GetDataReader($"Select GDDesc from tblGeneralDet where GenDetId = {StaffTypeId}");
                        if (dr.Read())
                            StaffType = dr["GDDesc"].ToString();
                        dr.Close();
                    }

                    if (Supervisorid > 0)
                    {
                        dr = GetDataReader($"Select StaffName from tblStaff where StaffId = {Supervisorid}");
                        if (dr.Read())
                            Supervisor = dr["StaffName"].ToString();
                        dr.Close();
                    }
                }
                else if (Obj.ToString() == "Staff Type" && RefId > 0)
                {
                    dr = GetDataReader($"SELECT StaffTypeId = a.GenDetId, StaffType = a.GDDesc FROM tblGeneralDet a where a.GenDetId = {RefId} and a.Status = 1");
                    if (dr.Read())
                    {
                        StaffTypeId = Convert.ToInt64(dr["StaffTypeId"]);
                        StaffType = dr["StaffType"].ToString();
                    }
                    dr.Close();
                }

                StaffDetails.Clear();
                if (StaffTypeId > 0)
                {                    
                    dr = GetDataReader($"SELECT a.StaffId, a.StaffCode, a.StaffName FROM tblStaff a where a.StaffTypeId = {StaffTypeId} and a.Status = 1 order by a.StaffName");                  
                    while (dr.Read())
                    {
                        StaffDetails.Add(new StaffInfo { StaffId = Convert.ToInt32(dr["StaffId"]), StaffCode = dr["StaffCode"].ToString(), StaffName = dr["StaffName"].ToString() });
                    }
                    dr.Close();
                }
                else
                {
                    dr = GetDataReader($"SELECT a.StaffId, a.StaffCode, a.StaffName FROM tblStaff a where a.Status = 1 order by a.StaffName");
                    while (dr.Read())
                    {
                        StaffDetails.Add(new StaffInfo { StaffId = Convert.ToInt32(dr["StaffId"]), StaffCode = dr["StaffCode"].ToString(), StaffName = dr["StaffName"].ToString() });
                    }
                    dr.Close();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void ResetData()
        {
            StaffId = 0; StaffName = ""; StaffTypeId = 0; StaffType = ""; StaffCode = ""; Supervisorid = 0; Supervisor = "";
            ICNum = ""; SOCSO = ""; EPFNum = ""; TaxFileNum = ""; JoinedDate = DateTime.Now.ToString(); LeftDate = DateTime.Now.ToString(); Salary = 0;
            Address1 = ""; Address2 = ""; City = ""; State = ""; Country = ""; ZipCode = ""; PhoneNum = ""; Fax = ""; EMail = "";IsActive = false;

            StaffDetails.Clear();
            SqlDataReader dr = GetDataReader($"SELECT a.StaffId, a.StaffCode, a.StaffName FROM tblStaff a where a.Status = 1 order by a.StaffName");
            while (dr.Read())
            {
                StaffDetails.Add(new StaffInfo { StaffId = Convert.ToInt32(dr["StaffId"]), StaffCode = dr["StaffCode"].ToString(), StaffName = dr["StaffName"].ToString() });
            }
            dr.Close();

            MyWind.txtStaffName.Focus();
        }

        private void SaveData()
        {
            if (StaffId == 0 && StaffName != "" && StaffCode != "")
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Create this Staff?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    ExecuteQuery($"INSERT INTO tblStaff(StaffName, StaffCode, StaffTypeId, SupervisorId, ICNum, SOCSO, EPFNum, TaxFileNum, Salary, JoinedDate, LeftDate, Address1, Address2, Address3, City, State, Country, ZipCode, PhoneNum, MobNum, PagerNum, Fax, EMail, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                        $" VALUES('{StaffName}', '{StaffCode}', '{StaffTypeId}', '{Supervisorid}', '{ICNum}', '{SOCSO}', '{EPFNum}', '{TaxFileNum}', '{Salary}', '{Convert.ToDateTime(JoinedDate).ToString("yyyy-MM-dd hh:mm:ss")}', NULL, '{Address1}', '{Address2}', '{""}', '{City}', '{State}', '{Country}', '{ZipCode}', '{PhoneNum}', '{""}', '{""}', '{Fax}', '{EMail}', '{LoginUserId}', GETDATE(), 0, NULL, '{IsActive}')");

                    MessageBox.Show("Staff Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    ResetData();
                }
            }
            else if (StaffId > 0 && StaffName != "" && StaffCode != "")
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Modify this Staff?", "Confirmation", MessageBoxButton.YesNo);
                string ldate = !string.IsNullOrEmpty(LeftDate) ? "LeftDate = '" + Convert.ToDateTime(LeftDate).ToString(sqlDateLongFormat) + "'," : "";
                if (MsgRes == MessageBoxResult.Yes)
                {
                    ExecuteQuery($"UPDATE tblStaff SET StaffName = '{StaffName}', " +
                        $"StaffCode = '{StaffCode}', " +
                        $"StaffTypeId = '{StaffTypeId}', " +
                        $"SupervisorId = '{Supervisorid}', " +
                        $"ICNum = '{ICNum}', " +
                        $"SOCSO = '{SOCSO}', " +
                        $"EPFNum = '{EPFNum}', " +
                        $"TaxFileNum = '{TaxFileNum}', " +
                        $"Salary = '{Salary}', " +
                        $"JoinedDate = '{Convert.ToDateTime(JoinedDate).ToString(sqlDateLongFormat)}', " +
                        $"{ldate}" +
                        $"Address1 = '{Address1}', " +
                        $"Address2 = '{Address2}', " +
                        $"Address3 = '{""}', " +
                        $"City = '{City}', " +
                        $"State = '{State}', " +
                        $"Country = '{Country}', " +
                        $"ZipCode = '{ZipCode}', " +
                        $"PhoneNum = '{PhoneNum}', " +
                        $"Fax = '{Fax}', " +
                        $"EMail = '{EMail}', " +
                        $"ModifyBy = '{LoginUserId}', " +
                        $"ModifyDate = GETDATE(), " +
                        $"Status = '{IsActive}' Where StaffId = {StaffId}");

                    MessageBox.Show("Staff Modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    ResetData();
                }
            }
            else
                MessageBox.Show("Invalid Data to Save", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public class StaffInfo : AMGenFunction
        {
            public long StaffId { get { return GetValue(() => StaffId); } set { SetValue(() => StaffId, value); OnPropertyChanged("StaffId"); } }
            public string StaffName { get { return GetValue(() => StaffName); } set { SetValue(() => StaffName, value); OnPropertyChanged("StaffName"); } }
            public string StaffCode { get { return GetValue(() => StaffCode); } set { SetValue(() => StaffCode, value); OnPropertyChanged("StaffCode"); } }
        }

    }
}
