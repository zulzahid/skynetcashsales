﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Masters;
using SkynetCashSales.View.Reports;
using SkynetCashSales.ViewModel.Reports;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Input;


namespace SkynetCashSales.ViewModel
{
    public class ItemVM : AMGenFunction
    {
        public FrmItem MyWind;
        private string ImgPath;


        public ItemVM(FrmItem Wind)
        {
            MyWind = Wind;
            Items = new ObservableCollection<ItemInfo>();
            LoadData(); ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        private ObservableCollection<ItemInfo> _Items = new ObservableCollection<ItemInfo>();
        public ObservableCollection<ItemInfo> Items { get { return _Items; } set { if (_Items != value) { _Items = value; OnPropertyChanged("Items"); } } }

        private ItemInfo _SelectedItem = new ItemInfo();
        public ItemInfo SelectedItem { get { return _SelectedItem; } set { if (_SelectedItem != value) { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } } }

        public int ItemId { get { return GetValue(() => ItemId); } set { SetValue(() => ItemId, value); OnPropertyChanged("ItemId"); } }
        public string ItemCode { get { return GetValue(() => ItemCode); } set { SetValue(() => ItemCode, value); OnPropertyChanged("ItemCode"); } }
        public string ItemDesc { get { return GetValue(() => ItemDesc); } set { SetValue(() => ItemDesc, value); OnPropertyChanged("ItemDesc"); } }
        public int ItemTypeId { get { return GetValue(() => ItemTypeId); } set { SetValue(() => ItemTypeId, value); OnPropertyChanged("ItemTypeId"); } }
        public string ItemType { get { return GetValue(() => ItemType); } set { SetValue(() => ItemType, value); OnPropertyChanged("ItemType"); } }
        public string SerialNo { get { return GetValue(() => SerialNo); } set { SetValue(() => SerialNo, value); OnPropertyChanged("SerialNo"); } }
        public string Brand { get { return GetValue(() => Brand); } set { SetValue(() => Brand, value); OnPropertyChanged("Brand"); } }
        public string Manufacturer { get { return GetValue(() => Manufacturer); } set { SetValue(() => Manufacturer, value); OnPropertyChanged("Manufacturer"); } }
        public int Unit1Id { get { return GetValue(() => Unit1Id); } set { SetValue(() => Unit1Id, value); OnPropertyChanged("Unit1Id"); } }
        public string Unit1 { get { return GetValue(() => Unit1); } set { SetValue(() => Unit1, value); OnPropertyChanged("Unit1"); } }
        public double Unit1Cost { get { return GetValue(() => Unit1Cost); } set { SetValue(() => Unit1Cost, value); OnPropertyChanged("Unit1Cost"); } }
        public double Unit1StationSalPrice { get { return GetValue(() => Unit1StationSalPrice); } set { SetValue(() => Unit1StationSalPrice, value); OnPropertyChanged("Unit1StationSalPrice"); } }
        public double Unit1BranchSalPrice { get { return GetValue(() => Unit1BranchSalPrice); } set { SetValue(() => Unit1BranchSalPrice, value); OnPropertyChanged("Unit1BranchSalPrice"); } }
        public double Unit1DeptSalPrice { get { return GetValue(() => Unit1DeptSalPrice); } set { SetValue(() => Unit1DeptSalPrice, value); OnPropertyChanged("Unit1DeptSalPrice"); } }
        public int Unit2Id { get { return GetValue(() => Unit2Id); } set { SetValue(() => Unit2Id, value); OnPropertyChanged("Unit2Id"); } }
        public string Unit2 { get { return GetValue(() => Unit2); } set { SetValue(() => Unit2, value); OnPropertyChanged("Unit2"); } }
        public int Unit2Factor { get { return GetValue(() => Unit2Factor); } set { SetValue(() => Unit2Factor, value); OnPropertyChanged("Unit2Factor"); } }
        public double Unit2StationSalPrice { get { return GetValue(() => Unit2StationSalPrice); } set { SetValue(() => Unit2StationSalPrice, value); OnPropertyChanged("Unit2StationSalPrice"); } }
        public double Unit2BranchSalPrice { get { return GetValue(() => Unit2BranchSalPrice); } set { SetValue(() => Unit2BranchSalPrice, value); OnPropertyChanged("Unit2BranchSalPrice"); } }
        public double Unit2DeptSalPrice { get { return GetValue(() => Unit2DeptSalPrice); } set { SetValue(() => Unit2DeptSalPrice, value); OnPropertyChanged("Unit2DeptSalPrice"); } }
        public double OpStock { get { return GetValue(() => OpStock); } set { SetValue(() => OpStock, value); OnPropertyChanged("OpStock"); } }
        public double MinStock { get { return GetValue(() => MinStock); } set { SetValue(() => MinStock, value); OnPropertyChanged("MinStock"); } }
        public double MaxStock { get { return GetValue(() => MaxStock); } set { SetValue(() => MaxStock, value); OnPropertyChanged("MaxStock"); } }
        public double ReOrder { get { return GetValue(() => ReOrder); } set { SetValue(() => ReOrder, value); OnPropertyChanged("ReOrder"); } }
        public double StockAdjustment { get { return GetValue(() => StockAdjustment); } set { SetValue(() => StockAdjustment, value); OnPropertyChanged("StockAdjustment"); } }
        public bool IsActivated { get { return GetValue(() => IsActivated); } set { SetValue(() => IsActivated, value); OnPropertyChanged("IsActivated"); } }
        public bool IsUpdateStock { get { return GetValue(() => IsUpdateStock); } set { SetValue(() => IsUpdateStock, value); OnPropertyChanged("IsUpdateStock"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private ICommand _TextGen;
        public ICommand TextGen { get { if (_TextGen == null) { _TextGen = new RelayCommand(Parameter => ExecuteTextGen(Parameter)); } return _TextGen; } }


        public void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "ItemCode":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.ItemId, ColOneText = a.ItemCode, ColTwoText = a.ItemDesc FROM tblItemMaster a WHERE a.Status = 1 ORDER BY a.ItemDesc");
                            ChildWnd.FrmListCol2_Closed += (r => { DispData(r.ColOneText); });
                            ChildWnd.TwoColWindShow(dt, "Item Name Selection!", "ItemCode", "ItemDesc");
                        }
                        break;
                    case "Disp Item": if (ItemCode.Trim() != "") DispData(ItemCode); break;
                    case "Disp Selected Item": if(SelectedItem != null) DispData(SelectedItem.ItemCode); break;
                    case "ItemType":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenDetId, ColOneText = a.GDDesc FROM tblGeneralDet a" +
                                $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                                $" WHERE a.Status = 1 and b.GenDesc = 'INVENTORY - ITEM TYPE' ORDER BY a.GDDesc");
                            ChildWnd.FrmListSingleCol_Closed += (r => { ItemTypeId = Convert.ToInt32(r.Id); ItemType = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Item Type Selection!");
                        }
                        break;
                    case "Unit1":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenDetId, ColOneText = a.GDDesc FROM tblGeneralDet a" +
                                $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                                $" WHERE a.Status = 1 and b.GenDesc = 'INVENTORY - ITEM UNIT' ORDER BY a.GDDesc");

                            ChildWnd.FrmListSingleCol_Closed += (r => { Unit1Id = Convert.ToInt32(r.Id); Unit1 = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Item Unit Selection!");
                        } break;
                    case "Unit2":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenDetId, ColOneText = a.GDDesc FROM tblGeneralDet a" +
                                $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                                $" WHERE a.Status = 1 and b.GenDesc = 'INVENTORY - ITEM UNIT' ORDER BY a.GDDesc");

                            ChildWnd.FrmListSingleCol_Closed += (r => { Unit2Id = Convert.ToInt32(r.Id); Unit2 = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Item Unit Selection!");
                        }
                        break;
                    case "Unit2 Price": Unit2PriceCalc(); break;
                    //case "DecimalCheck": RemoveSpecialChars(Unit1Cost); break;
                    case "Select Image":
                        {
                            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
                            ofd.Filter = "Image Files|*.png";
                            ofd.Title = "Save as Image File";
                            System.Windows.Forms.DialogResult result = ofd.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(ofd.FileName))
                            {
                                ImgPath = ofd.FileName;
                                using (FileStream fs = new FileStream(ImgPath, FileMode.Open, FileAccess.Read))
                                {
                                    byte[] imgBytes = System.IO.File.ReadAllBytes(ImgPath);
                                    fs.Read(imgBytes, 0, System.Convert.ToInt32(fs.Length));
                                    fs.Close();
                                    if (imgBytes != null) ViewImage(imgBytes);
                                }
                            }
                        }
                        break;
                    case "Cancel Image":
                        {
                            ExecuteQuery($"UPDATE tblItemMaster SET ItemImage = NULL WHERE ItemCode = '{ItemCode}'");
                            ImgPath = "";
                            MyWind.imgItem.Source = null;
                        }
                        break;
                    case "Save": 
                        {
                            if (IsValidate)
                            {
                                string auth = StrAuthenticateType();
                                if (auth == "ADMIN" || auth == "Admin")
                                {
                                    Save();
                                    LoadData(); ResetData();
                                }
                                else { System.Windows.MessageBox.Show("ADMIN only, your authentication type: '" + auth + "'", "Warning"); }
                            }
                        } break;
                    case "Print": Print(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void ExecuteTextGen(object Obj)
        {
            try
            {

                switch (Obj.ToString())
                {
                    case "Input Unit1Cost":
                        {
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Input excution : " + Ex.Message); }
        }
        /////   All TextBox Input Excution  -----> END

        private void ViewImage(byte[] data)
        {
            using (MemoryStream strm = new MemoryStream(data))
            {
                strm.Write(data, 0, data.Length);
                strm.Position = 0;
                System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
                System.Windows.Media.Imaging.BitmapImage bi = new System.Windows.Media.Imaging.BitmapImage();
                bi.BeginInit();
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                bi.StreamSource = ms;
                bi.EndInit();
                MyWind.imgItem.Source = bi;
            }
        }

        static readonly string[] DValidatedProperties = { "ItemCode", "ItemDesc", "ItemType", "Unit1Cost", "OpStock", "MaxStock", "ReOrder", "Image" };
        public bool IsValidate
        {
            get
            {
                foreach (string property in DValidatedProperties)
                {
                    if (GetValidationError(property) != null) // there is an error
                        return false;
                }
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "ItemCode": if (string.IsNullOrEmpty(ItemCode)) { error = "Invalid Item Code!"; MyWind.txtItemCode.Focus(); } break;
                case "ItemDesc": if (string.IsNullOrEmpty(ItemDesc)) { error = "Invalid Item Name!"; MyWind.txtItemName.Focus(); } break;
                case "ItemType": if (string.IsNullOrEmpty(ItemType)) { error = "Invalid Type!"; MyWind.txtItemType.Focus(); } break;
                case "Unit1Cost": if (Unit1Cost <= 0) { error = "Invalid price!"; MyWind.txtUnit1Cost.Focus(); } break;
                case "OpStock": if (OpStock < 0 ) { error = "Invalid Opening Balance!"; MyWind.txtOpStock.Focus(); } break;
                case "MaxStock": if (MaxStock <= 0) { error = "Invalid Maximum Stock!"; MyWind.txtMaxStock.Focus(); } break;
                case "ReOrder": if (ReOrder <= 0 || MaxStock < ReOrder) { error = "Invalid Reorder level!"; MyWind.txtReorder.Focus(); } break;
                case "Image": if (MyWind.imgItem.Source == null || MyWind.imgItem.Source.Equals("")) { error = "Invalid Image!"; MyWind.btnImage.Focus(); } break;
                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) MessageBox.Show(error);
            return error;
        }

        private void Unit2PriceCalc()
        {
            Unit2StationSalPrice = Unit1StationSalPrice / Unit2Factor;
            Unit2BranchSalPrice = Unit1BranchSalPrice / Unit2Factor;
            Unit2DeptSalPrice = Unit1DeptSalPrice / Unit2Factor;
            MyWind.txtOpStock.Focus();
        }
        
        private void DispData(string mItemCode)
        {
            try
            {
                SqlDataReader dr = GetDataReader($"SELECT a.ItemId, a.ItemCode, a.ItemDesc, ItemTypeId = a.ItemType, ItemType = b.GDDesc, a.SerialNo, a.Brand, a.Manufacturer," +
                    $" a.Unit1Id, a.Unit1, a.Unit1Cost, a.Unit1SalRateToStation, a.Unit1SalRateToBranch, a.Unit1SalRateToDept,a.Unit2Id, a.Unit2, a.UnitFactor, a.Unit2SalRateToStation," +
                    $" a.Unit2SalRateToBranch, a.Unit2SalRateToDept, a.OpeningStk, a.MinStk, a.MaxStk, a.ReOrder, a.Status, a.ItemImage FROM tblItemMaster a" +
                    $" JOIN tblGeneralDet b on a.ItemType = b.GenDetId" +
                    $" JOIN tblGeneralMaster c on b.GenId = c.GenId" +
                    $" WHERE a.ItemCode = '{mItemCode}'");
                if (dr.Read())
                {
                    ItemId = Convert.ToInt32(dr["ItemId"]);
                    ItemCode = dr["ItemCode"].ToString();
                    ItemDesc = dr["ItemDesc"].ToString();
                    ItemTypeId = Convert.ToInt32(dr["ItemTypeId"]);
                    ItemType = dr["ItemType"].ToString();
                    SerialNo = dr["SerialNo"].ToString();
                    Brand = dr["Brand"].ToString();
                    Manufacturer = dr["Manufacturer"].ToString();
                    Unit1Id = Convert.ToInt32(dr["Unit1Id"]);
                    Unit1 = dr["Unit1"].ToString();
                    Unit1Cost = Convert.ToDouble(dr["Unit1Cost"]);
                    Unit1StationSalPrice = Convert.ToDouble(dr["Unit1SalRateToStation"]);
                    Unit1BranchSalPrice = Convert.ToDouble(dr["Unit1SalRateToBranch"]);
                    Unit1DeptSalPrice = Convert.ToDouble(dr["Unit1SalRateToDept"]);
                    Unit2Id = Convert.ToInt32(dr["Unit2Id"]);
                    Unit2 = dr["Unit2"].ToString();
                    Unit2Factor = Convert.ToInt32(dr["UnitFactor"]);
                    Unit2StationSalPrice = Convert.ToDouble(dr["Unit2SalRateToStation"]);
                    Unit2BranchSalPrice = Convert.ToDouble(dr["Unit2SalRateToBranch"]);
                    Unit2DeptSalPrice = Convert.ToDouble(dr["Unit2SalRateToDept"]);
                    OpStock = Convert.ToDouble(dr["OpeningStk"]);
                    MinStock = Convert.ToDouble(dr["MinStk"]);
                    MaxStock = Convert.ToDouble(dr["MaxStk"]);
                    ReOrder = Convert.ToDouble(dr["ReOrder"]);
                    IsActivated = Convert.ToBoolean(dr["Status"]);

                    if (dr["ItemImage"] != DBNull.Value)
                    {
                        ViewImage((byte[])dr["ItemImage"]);
                    }
                    else
                        MyWind.imgItem.Source = null;
                }
                dr.Close();
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void ResetData()
        {
            try
            {
                SqlDataReader dr = GetDataReader($"SELECT ISNULL(Max(ItemId), 0) as ItemId FROM tblItemMaster");
                if (dr.Read())
                {
                    ItemCode = "I" + (Convert.ToInt32(dr["ItemId"]) + 1).ToString("0000");
                }
                else
                {
                    ItemCode = "I0001";
                }
                dr.Close();

                ItemId = 0; ItemDesc = ""; ItemTypeId = 0; ItemType = ""; SerialNo = ""; Brand = ""; Manufacturer = "";
                Unit1Id = 0; Unit1 = ""; Unit1Cost = 0; Unit1StationSalPrice = 0; Unit1BranchSalPrice = 0; Unit1DeptSalPrice = 0;
                Unit2Id = 0; Unit2 = ""; Unit2Factor = 0; Unit2StationSalPrice = 0; Unit2BranchSalPrice = 0; Unit2DeptSalPrice = 0;
                OpStock = 0; MinStock = 0; MaxStock = 0; ReOrder = 0;StockAdjustment = 0;
                IsActivated = true;
                
                ImgPath = "";
                MyWind.imgItem.Source = null;

                MyWind.txtItemCode.SelectionStart = MyWind.txtItemCode.Text.Length;
                MyWind.txtItemCode.Focus();
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        public void LoadData()
        {
            try
            {
                Items.Clear();
                SqlDataReader dr = GetDataReader($"SELECT a.ItemCode,a.ItemDesc,a.Status FROM tblItemMaster a order by a.ItemDesc");
                while (dr.Read())
                {
                    Items.Add(new ItemInfo { ItemCode = dr["ItemCode"].ToString(), ItemDesc = dr["ItemDesc"].ToString(), Status = Convert.ToBoolean(dr["Status"]) });
                }
                dr.Close();
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void Save()
        {
            Unit1StationSalPrice = Unit1Cost; Unit1BranchSalPrice = Unit1Cost; Unit1DeptSalPrice = Unit1Cost;
            Unit2StationSalPrice = Unit1Cost; Unit2BranchSalPrice = Unit1Cost; Unit2DeptSalPrice = Unit1Cost;
            Unit2Id = Unit1Id; Unit2 = Unit1;
            Unit2Factor = 1;

            SerialNo = "1";
            DataTable dt = new DataTable();
            try
            {
                dt = GetDataTable($"SELECT MAX(SerialNo+0) AS MSerialNo FROM tblItemMaster");
                if (dt.Rows.Count >= 0)
                {
                    if (dt.Rows[0]["MSerialNo"].ToString() != "")
                        SerialNo = (Convert.ToInt32(dt.Rows[0]["MSerialNo"]) + 1).ToString();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Get Item Serial Number : " + Ex.Message); }


            SqlDataReader dr = GetDataReader($"SELECT * FROM tblItemMaster a where a.ItemId = {ItemId}");
            if (!dr.HasRows)
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Create this Item?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    ItemId = ExecuteScalarQuery($"INSERT INTO tblItemMaster(ItemCode, ItemDesc, ItemType, SerialNo, Brand, Manufacturer, Unit1Id, Unit1, Unit1Cost, Unit1SalRateToStation, Unit1SalRateToBranch, Unit1SalRateToDept, Unit2Id, Unit2, UnitFactor, Unit2SalRateToStation, Unit2SalRateToBranch, Unit2SalRateToDept, OpeningStk, MinStk, MaxStk, ReOrder, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, ItemImage)" +
                        $" VALUES('{ItemCode}', '{ItemDesc}', {ItemTypeId}, '{SerialNo}', '{Brand}', '{Manufacturer}'," +
                        $" {Unit1Id}, '{Unit1}', {Unit1Cost}, {Unit1StationSalPrice}, {Unit1BranchSalPrice}, {Unit1DeptSalPrice}," +
                        $" {Unit2Id}, '{Unit2}', {Unit2Factor}, {Unit2StationSalPrice}, {Unit2BranchSalPrice}, {Unit2DeptSalPrice}," +
                        $" {OpStock}, {MinStock}, {MaxStock}, {ReOrder}, {LoginUserId}, GETDATE(), 0, null, '{IsActivated}'," +
                        $" {(ImgPath.Trim() != "" ? $"(SELECT * FROM OPENROWSET(BULK '{ImgPath}', SINGLE_BLOB) as img)" : "NULL")})");

                    ExecuteQuery($"INSERT INTO tblStock(ItemId, Opening, Purchase, PurchaseRet, Receive, Issue, Sales, SalesRet, Excess, Short, Closing, StockAdj, CreateBy, ModifyBy, Status, ItemCode, IsExported, Remarks)" +
                        $" VALUES({ItemId}, {Convert.ToDecimal(OpStock)}, 0, 0, 0, 0, 0, 0, 0, 0, {(Convert.ToDecimal(OpStock)+ Convert.ToDecimal(StockAdjustment))}, {Convert.ToDecimal(StockAdjustment)},0,0,1,'{ItemCode}',0,'')");

                    ExecuteQuery($"INSERT INTO tblStockTran(StkTranType, StkTranRefNum, StkTranDate, ItemId, ItemCode, Unit1, Unit2, StkTranQty, Rate, DiscountPer, DiscountAmount, TaxPer, TaxAmount, TranAmount, CancelQty, StkTranPendingQty, TranStatus, StkTranDesc, CreateBy, ModifyBy, IsExported)" +
                                                        $" VALUES('OB', FORMAT(NEXT VALUE FOR ITranRefNumOB, '{CSInvenOBPrefix.ToString()}00000'), GETDATE(),{ItemId},'{ItemCode}','{Unit1}','{Unit1}',{Convert.ToDecimal(OpStock)},{Unit1Cost},0, 0, 0, 0, 0, 0, 0,'C','OPENING STOCK', {LoginUserId},0,0)");
                                                                      //FORMAT(NEXT VALUE FOR ITranRefNumOB, 'OB{MyCompanyCode}{(MyCompanySubName ?? "").ToString()}000000')
                    MessageBox.Show("Item Created Successfully!");
                }
            }
            else if (ItemId > 0)
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Modify this Item?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    dr.Close();
                    ExecuteQuery($"UPDATE tblItemMaster SET ItemDesc = '{ItemDesc}', ItemType = {ItemTypeId}, SerialNo = '{SerialNo}', Brand = '{Brand}', Manufacturer = '{Manufacturer}'," +
                        $" Unit1Id = {Unit1Id}, Unit1 = '{Unit1}', Unit1Cost = {Unit1Cost}, Unit1SalRateToStation = {Unit1StationSalPrice}, Unit1SalRateToBranch = {Unit1BranchSalPrice}, Unit1SalRateToDept = {Unit1DeptSalPrice}," +
                        $" Unit2Id = {Unit2Id}, Unit2 = '{Unit2}', UnitFactor = {Unit2Factor}, Unit2SalRateToStation = {Unit2StationSalPrice}, Unit2SalRateToBranch = {Unit2BranchSalPrice}, Unit2SalRateToDept = {Unit2DeptSalPrice}," +
                        $" MinStk = {MinStock}, MaxStk = {MaxStock}, ReOrder = {ReOrder}, ModifyBy = {LoginUserId}, ModifyDate = GETDATE(), Status = '{IsActivated}'" +
                        $" {((ImgPath.Trim() != "" && MyWind.imgItem.Source != null) ? $", ItemImage = (SELECT * FROM OPENROWSET(BULK '{ImgPath}', SINGLE_BLOB) as img)" : (MyWind.imgItem.Source != null ? "" : ", ItemImage = null"))}" +
                        $" Where ItemCode = '{ItemCode}'");

                    ImgPath = "";
                    MyWind.imgItem.Source = null;

                    ////// *************************************************************************************************************
                    ////// IsUpdateStock [Check box] alreday removed. So, no more update feature for table : tblStock         ---> START
                    ////// *************************************************************************************************************
                    //if (IsUpdateStock == true)
                    //{
                    //    dr = GetDataReader($"SELECT ISNULL(Opening, 0) as Opening FROM tblStock Where ItemId = {ItemId}");
                    //    decimal oldopen = dr.Read() ? Convert.ToDecimal(dr["Opening"]) : 0; dr.Close();
                    //    dr = GetDataReader($"SELECT ISNULL(Closing, 0) as Closing FROM tblStock Where ItemId = {ItemId}");
                    //    decimal oldclose = dr.Read() ? Convert.ToDecimal(dr["Closing"]) : 0; dr.Close();
                    //    decimal currentclose = 0;
                    //    if (Convert.ToDecimal(OpStock) == oldclose) { currentclose = Convert.ToDecimal(OpStock); }
                    //    else if (oldclose > 0)
                    //    {
                    //        currentclose = Convert.ToDecimal(OpStock) - oldopen + oldclose;
                    //    }
                    //    ExecuteQuery($"UPDATE tblStock SET Opening = {Convert.ToDecimal(OpStock)}, StockAdj = {Convert.ToDecimal(StockAdjustment)}, Closing = {currentclose + Convert.ToDecimal(StockAdjustment)} Where ItemId = {ItemId}");
                    //}
                    ////// *************************************************************************************************************
                    ////// IsUpdateStock Check box alreday removed so, no more update feature for table : tblStock         ---> END
                    ////// *************************************************************************************************************
                    
                    MessageBox.Show("Item Modified Successfully!");
                }
            }
        }

        private void Print()
        {
            DataTable dt;
            using (dt = new DataTable())
            {
                dt = GetDataTable($"SELECT a.ItemId, a.ItemCode, a.ItemDesc, a.ItemType, a.Brand, a.CreateBy, a.Manufacturer, a.MinStk, a.MaxStk, a.OpeningStk, a.SerialNo, a.Unit1, a.Unit2, a.Unit2Id, a.Unit2SalRateToBranch, a.Unit2SalRateToDept, a.Unit2SalRateToStation, a.UnitFactor FROM tblItemMaster a ORDER BY a.ItemDesc");
                FrmReportViewer RptViewerGen = new FrmReportViewer();
                RptViewerGen.Owner = Application.Current.MainWindow;
                ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompAddress, "", dt, "", dt, "ItemMaster", dt, "RPTItemList.rdlc", "", RptViewerGen.RptViewer, true, false);
                RptViewerGen.DataContext = VM;
                RptViewerGen.Show();
            }
        }

        public class ItemInfo : AMGenFunction
        {
            public string ItemCode { get { return GetValue(() => ItemCode); } set { SetValue(() => ItemCode, value); OnPropertyChanged("ItemCode"); } }
            public string ItemDesc { get { return GetValue(() => ItemDesc); } set { SetValue(() => ItemDesc, value); OnPropertyChanged("ItemDesc"); } }
            public bool Status { get { return GetValue(() => Status); } set { SetValue(() => Status, value); OnPropertyChanged("Status"); } }
        }

    }
}
