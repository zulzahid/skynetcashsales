﻿using Newtonsoft.Json;
using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class DestinationVM : AMGenFunction
    {
        public FrmDestination MyWind;
        public DestinationVM(FrmDestination Wind)
        {
            MyWind = Wind;
            ZoneDetails = new ObservableCollection<ZoneInfo>();
            ResetData();
        }

        public long DestId { get { return GetValue(() => DestId); } set { SetValue(() => DestId, value); OnPropertyChanged("DestId"); } }
        public string DestCode { get { return GetValue(() => DestCode); } set { SetValue(() => DestCode, value); OnPropertyChanged("DestCode"); } }
        public string DestName { get { return GetValue(() => DestName); } set { SetValue(() => DestName, value); OnPropertyChanged("DestName"); } }
        public string StationCode { get { return GetValue(() => StationCode); } set { SetValue(() => StationCode, value); OnPropertyChanged("StationCode"); } }
        public string StateCode { get { return GetValue(() => StateCode); } set { SetValue(() => StateCode, value); OnPropertyChanged("StateCode"); } }
        public string StateName { get { return GetValue(() => StateName); } set { SetValue(() => StateName, value); OnPropertyChanged("StateName"); } }
        public string CountryCode { get { return GetValue(() => CountryCode); } set { SetValue(() => CountryCode, value); OnPropertyChanged("CountryCode"); } }
        public string CountryName { get { return GetValue(() => CountryName); } set { SetValue(() => CountryName, value); OnPropertyChanged("CountryName"); } }
        public string RegionCode { get { return GetValue(() => RegionCode); } set { SetValue(() => RegionCode, value); OnPropertyChanged("RegionCode"); } }
        public string RegionName { get { return GetValue(() => RegionName); } set { SetValue(() => RegionName, value); OnPropertyChanged("RegionName"); } }
        public bool IsODA { get { return GetValue(() => IsODA); } set { SetValue(() => IsODA, value); OnPropertyChanged("IsODA"); } }

        private ObservableCollection<ZoneInfo> _ZoneDetails = new ObservableCollection<ZoneInfo>();
        public ObservableCollection<ZoneInfo> ZoneDetails { get { return _ZoneDetails; } set { _ZoneDetails = value; } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Destination Code":
                        {
                            dt = GetDataTable($"SELECT DestId As ColOneId, RTRIM(DestCode) As ColOneText, DestName As ColTwoText, Country As ColThreeText FROM tblDestination ORDER BY DestCode");
                            ChildWnd.FrmListCol3_Closed += (r => { DisplayItem(Obj.ToString(), r.ColOneText, Convert.ToInt64(r.Id)); });
                            ChildWnd.ThreeColWindShow(dt, "Destination Selection", "Code", "Destination", "Country");
                        }
                        break;
                    case "Station":
                        {
                            dt = GetDataTable($"SELECT StationId As ColOneId, StationCode As ColOneText, StationName As ColTwoText FROM tblNetworkStation ORDER BY StationCode");
                            ChildWnd.FrmListCol2_Closed += (r => { StationCode = r.ColOneText; });
                            ChildWnd.TwoColWindShow(dt, "Station Selection", "Code", "Station");
                        }
                        break; ;
                    case "Load Destination":
                        {
                            DisplayItem(Obj.ToString(), DestCode, DestId);
                        }
                        break;
                    case "State Name":
                        {
                            dt = GetDataTable($"SELECT A.StateId As ColOneId, A.StateCode As ColOneText, A.StateName As ColTwoText, B.CountryName As ColThreeText FROM tblState A" +
                                $" LEFT JOIN tblCountry B ON A.CountryCode = B.CountryCode3" +
                                $" ORDER BY A.StateName");

                            ChildWnd.FrmListCol3_Closed += (r => { DisplayItem(Obj.ToString(), r.ColOneText, Convert.ToInt64(r.Id)); });
                            ChildWnd.ThreeColWindShow(dt, "State Selection", "Code", "State", "Country");
                        }
                        break;
                    case "Country Name":
                        {
                            dt = GetDataTable($"SELECT CountryId As ColOneId, CountryCode3 As ColOneText, CountryName As ColTwoText FROM tblCountry ORDER BY CountryName");
                            ChildWnd.FrmListCol2_Closed += (r => { DisplayItem(Obj.ToString(), r.ColOneText, Convert.ToInt64(r.Id)); });
                            ChildWnd.TwoColWindShow(dt, "Country Selection", "Code", "Country");

                            CheckData("Country");
                        }
                        break;
                    case "Region Name":
                        {
                            dt = GetDataTable($"SELECT RegionId As ColOneId, RegionCode As ColOneText, RegionName As ColTwoText FROM tblRegion ORDER BY RegionName");
                            ChildWnd.FrmListCol2_Closed += (r => { RegionCode = r.ColOneText; RegionName = r.ColOneText; });
                            ChildWnd.TwoColWindShow(dt, "Region Selection", "Code", "Region");
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Delete":
                        {
                            if (DestId > 0 && DestCode != "" && DestName != "")
                            {
                                MessageBoxResult MsgRes = MessageBox.Show("Confirm Delete this Destination?", "Confirmation", MessageBoxButton.YesNo);
                                if (MsgRes == MessageBoxResult.Yes)
                                {
                                    ExecuteQuery($"DELETE FROM tblDestination WHERE DestId = {DestId}");
                                    ResetData();
                                }
                            }
                        }
                        break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void CheckData(object Obj)
        {
            if (Obj.ToString() == "Country")
            {
                SqlDataReader dr = GetDataReader($"SELECT A.StateCode, A.StateName, B.CountryCode3, B.CountryName FROM tblState A" +
                    $" LEFT JOIN tblCountry B ON A.CountryCode = B.CountryCode3" +
                    $" WHERE A.StateCode = '{StateCode}' AND B.CountryCode3 = '{CountryCode}'");
                if (dr.Read())
                {
                    StateCode = dr["StateCode"].ToString();
                    StateName = dr["StateName"].ToString();
                }
                else
                {
                    StateCode = "";
                    StateName = "";
                }
                dr.Close();
            }
        }
        
        private void DisplayItem(string Ref, string RefCode, long RefId)
        {
            try
            {
                if (Ref == "Destination Code" || Ref == "Load Destination")
                {
                    if (RefCode.Trim() != "")
                    {
                        SqlDataReader dr = GetDataReader($"SELECT DestId, DestCode, DestName, StationCode, State, Country, Region, IsODA FROM tblDestination WHERE RTRIM(DestCode) = '{RefCode.Trim()}'");
                        if (dr.Read())
                        {
                            DestId = Convert.ToInt64(dr["DestId"]);
                            DestCode = dr["DestCode"].ToString();
                            DestName = dr["DestName"].ToString();
                            StationCode = dr["StationCode"].ToString();
                            StateName = dr["State"].ToString();
                            CountryName = dr["Country"].ToString();
                            RegionName = dr["Region"].ToString();
                            IsODA = Convert.ToBoolean(dr["IsODA"]);
                            dr.Close();

                            ZoneDetails.Clear();
                            DataTable dt = GetDataTable($"SELECT b.ZoneCode, b.ZoneDesc FROM tblZoneDetail a" +
                                $" LEFT JOIN tblZone b on a.ZoneCode = b.ZoneCode" +
                                $" where RTRIM(a.DestCode) = '{DestCode.Trim()}'");
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                ZoneDetails.Add(new ZoneInfo { ZoneCode = dt.Rows[i]["ZoneCode"].ToString().Trim(), ZoneDesc = dt.Rows[i]["ZoneDesc"].ToString().Trim() }); ;
                            }
                        }
                        else
                        {
                            CheckWebData(DestCode, "Load Destination");
                        }
                        dr.Close();
                    }
                    MyWind.txtDestName.Focus();
                }
                else if (Ref == "State Name")
                {
                    if (RefCode.Trim() != "")
                    {
                        SqlDataReader dr = GetDataReader($"SELECT A.StateCode, A.StateName, B.CountryCode3, B.CountryName FROM tblState A" +
                            $" LEFT JOIN tblCountry B ON A.CountryCode = B.CountryCode3" +
                            $" WHERE RTRIM(A.StateCode) = '{RefCode.Trim()}'");
                        if (dr.Read())
                        {
                            StateCode = dr["StateCode"].ToString();
                            StateName = dr["StateName"].ToString();
                            CountryCode = dr["CountryCode3"].ToString();
                            CountryName = dr["CountryName"].ToString();
                        }
                        dr.Close();
                    }
                }
                else if (Ref == "Country Name")
                {
                    if (RefCode.Trim() != "")
                    {
                        SqlDataReader dr = GetDataReader($"SELECT CountryCode3, CountryName FROM tblCountry WHERE RTRIM(CountryCode3) = '{RefCode.Trim()}'");
                        if (dr.Read())
                        {
                            CountryCode = dr["CountryCode3"].ToString();
                            CountryName = dr["CountryName"].ToString();
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void ResetData()
        {
            IsODA = false;
            DestId = 0; DestCode = ""; DestName = ""; StationCode = "";
            StateCode = ""; StateName = ""; CountryCode = ""; CountryName = ""; RegionCode = ""; RegionName = "";
            ZoneDetails.Clear();
            MyWind.txtDestCode.Focus();
        }

        private void SaveData()
        {
            if (IsValid)
            {
                MessageBoxResult MsgRes;
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblDestination WHERE RTRIM(DestCode) = '{DestCode}'");
                if(dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm modify this Destination?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblDestination SET DestCode = '{DestCode.Trim()}', DestName = '{DestName.Trim()}', StationCode = '{StationCode.Trim()}', State = '{StateName.Trim()}', Country = '{CountryName.Trim()}', Region = '{RegionName.Trim()}', IsODA = '{IsODA}', IsDeleted = 0 WHERE RTRIM(DestCode) = '{DestCode}'");
                        CheckWebData(DestCode, "SaveData");
                        MessageBox.Show("Destination modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Create this Destination?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted)" +
                            $" VALUES('{DestCode.Trim()}', '{DestName.Trim()}', '{StationCode.Trim()}', '{StateName.Trim()}', '{CountryName.Trim()}', '{RegionName.Trim()}', '{IsODA}', 0)");
                        CheckWebData(DestCode, "SaveData");
                        MessageBox.Show("Destination Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                ResetData();
            }
        }

        private void CheckWebData(string DestCode, string refStr)
        {
            bool Status = false;
            List<clsDestination> data;
            clsGetDestination apiref;

            try
            {
                if (DestCode.Trim() != "")
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://skynetmy.ddns.net:8089");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        data = new List<clsDestination>();
                        apiref = new clsGetDestination
                        {
                            AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                            FunctionStr = "GetDest",
                            DestCode = DestCode,
                            destData = data
                        };

                        var response = client.PostAsJsonAsync("api/WebMasters/GetDest", apiref).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            if (!res.Contains("Record not found"))
                            {
                                clsGetDestinationRes item = JsonConvert.DeserializeObject<clsGetDestinationRes>(res.ToString());
                                if (item.status == "Success" && item.data.Count > 0)
                                {
                                    Status = false;
                                    for (int i = 0; i < item.data.Count; i++)
                                    {
                                        if (item.data[i].DestCode == DestCode)
                                        {
                                            Status = true;
                                            SqlDataReader dr = GetDataReader($"SELECT DestCode FROM tblDestination WHERE DestCode = '{DestCode}'");
                                            if (!dr.HasRows)
                                            {
                                                refStr = refStr != "SaveData" ? "Import Destination" : refStr;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if(Status && refStr == "Import Destination")
                        {
                            WebDb = new CommunicateWebDb();
                            WebDb.GetDestination(DestCode);

                            DisplayItem("Load Destination", DestCode, 0);
                        }
                        else if (Status == false && refStr == "SaveData")
                        {
                            data = new List<clsDestination>();
                            data.Add(new clsDestination
                            {
                                DestCode = DestCode,
                                DestName = DestName,
                                StationCode = StationCode,
                                State = StateName,
                                Country = CountryName,
                                Region = RegionName,
                                IsODA = IsODA,
                                IsDeleted = false
                            });

                            apiref = new clsGetDestination
                            {
                                AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                                FunctionStr = "InsertDest",
                                destData = data
                            };

                            response = client.PostAsJsonAsync("api/WebMasters/InsertZoneDet", apiref).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var res = response.Content.ReadAsStringAsync().Result;
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog($"DestinationVM.CheckWebData({DestCode}) : " + ex.ToString());
            }
        }

        static readonly string[] ValidatedProperties = { "DestCode", "DestName", "StationCode", "State", "Country" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    string errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "DestCode": if (string.IsNullOrEmpty(DestCode)) { error = "DestCode is required"; MyWind.txtDestCode.Focus(); } break;
                case "DestName": if (string.IsNullOrEmpty(DestName)) { error = "DestName is required"; MyWind.txtDestName.Focus(); } break;
                case "StationCode": if (string.IsNullOrEmpty(StationCode)) { error = "StationCode is required"; MyWind.txtStatioCode.Focus(); } break;
                case "State": if (string.IsNullOrEmpty(StateName)) { error = "State is required"; MyWind.txtState.Focus(); } break;
                case "Country": if (string.IsNullOrEmpty(CountryName)) { error = "Country is required"; MyWind.txtCountry.Focus(); } break;
                default: throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        public class ZoneInfo : AMGenFunction
        {
            public string ZoneCode { get { return GetValue(() => ZoneCode); } set { SetValue(() => ZoneCode, value); OnPropertyChanged("ZoneCode"); } }
            public string ZoneDesc { get { return GetValue(() => ZoneDesc); } set { SetValue(() => ZoneDesc, value); OnPropertyChanged("ZoneDesc"); } }
        }

        public class clsDestination
        {
            public int DestId { get; set; }
            public string DestCode { get; set; }
            public string DestName { get; set; }
            public string StationCode { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Region { get; set; }
            public bool IsODA { get; set; }
            public bool IsDeleted { get; set; }
            public string TransmittedTime { get; set; }
            public string ReceivedTime { get; set; }
        }

        public class clsGetDestination
        {
            public string AccessToken { get; set; }
            public string FunctionStr { get; set; }
            public string DestCode { get; set; }
            public List<clsDestination> destData = new List<clsDestination>();
        }

        public class clsGetDestinationRes
        {
            public string status { get; set; }
            public string code { get; set; }
            public string message { get; set; }
            public string referenceNumber { get; set; }
            public List<clsDestination> data = new List<clsDestination>();
        }

    }
}
