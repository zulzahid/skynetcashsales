﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel
{
    public class RegionVM : AMGenFunction
    {
        public FrmRegion MyWind;
        public RegionVM(FrmRegion Wind)
        {
            MyWind = Wind;
            ResetData();
        }

        public int RegionId { get { return GetValue(() => RegionId); } set { SetValue(() => RegionId, value); OnPropertyChanged("RegionId"); } }
        public string RegionCode { get { return GetValue(() => RegionCode); } set { SetValue(() => RegionCode, value); OnPropertyChanged("RegionCode"); } }
        public string RegionName { get { return GetValue(() => RegionName); } set { SetValue(() => RegionName, value); OnPropertyChanged("RegionName"); } }
        public bool Status { get { return GetValue(() => Status); } set { SetValue(() => Status, value); OnPropertyChanged("Status"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Region Code":
                        {
                            dt = GetDataTable($"SELECT RegionId As ColOneId, RTRIM(RegionCode) As ColOneText, RegionName As ColTwoText, Status As ColThreeText FROM tblRegion ORDER BY RegionName");
                            ChildWnd.FrmListCol3_Closed += (r => { RegionId = Convert.ToInt32(r.Id); RegionCode = r.ColOneText; RegionName = r.ColTwoText; Status = Convert.ToBoolean(r.ColThreeText); });
                            ChildWnd.ThreeColWindShow(dt, "Region Selection", "Code", "Name", "Status");
                        }
                        break;
                    case "Load Region":
                        {
                            if (string.IsNullOrEmpty(RegionCode))
                            {
                                MessageBox.Show("Invalid Region Code", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                SqlDataReader dr = GetDataReader($"SELECT RegionId, RegionCode, RegionName, Status FROM tblRegion WHERE RTRIM(RegionCode) = '{RegionCode.Trim()}'");
                                if (dr.Read())
                                {
                                    RegionId = Convert.ToInt32(dr["RegionId"]);
                                    RegionCode = dr["RegionCode"].ToString().Trim();
                                    RegionName = dr["RegionName"].ToString();
                                    Status = Convert.ToBoolean(dr["Status"]);
                                }
                                dr.Close();
                            }
                            MyWind.txtRegionCode.Focus();
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void ResetData()
        {
            RegionId = 0; RegionCode = ""; RegionName = ""; Status = false; MyWind.txtRegionCode.Focus();
        }

        static readonly string[] ValidatedProperties = { "RegionCode", "RegionName" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    string errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "RegionCode": if (string.IsNullOrEmpty(RegionCode)) { error = "RegionCode is required"; MyWind.txtRegionCode.Focus(); } break;
                case "RegionName": if (string.IsNullOrEmpty(RegionName)) { error = "RegionName is required"; MyWind.txtRegionName.Focus(); } break;
                default: throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        private void SaveData()
        {
            if (IsValid)
            {
                MessageBoxResult MsgRes;

                SqlDataReader dr = GetDataReader($"SELECT RegionId, RegionCode, RegionName, Status FROM tblRegion WHERE RegionId = {RegionId}");
                if (dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm Modify this Region?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblRegion SET RegionCode = '{RegionCode.Trim()}', RegionName = '{RegionName.Trim()}', Status = '{Status}' WHERE RegionId = {RegionId}");
                        MessageBox.Show("Region modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Create this Region?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblRegion (RegionCode, RegionName, Status) VALUES('{RegionCode.Trim()}', '{RegionName.Trim()}', '{Status}')");
                        MessageBox.Show("Region Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                ResetData();
            }
        }
    }
}
