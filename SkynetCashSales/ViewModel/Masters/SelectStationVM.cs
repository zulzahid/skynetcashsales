﻿using SkynetCashSales.General;
using System;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;
using System.Xml;

namespace SkynetCashSales.View.Masters
{
    public class SelectStationVM : AMGenFunction
    {
        public FrmSelectStation MyWind;
        public SelectStationVM(FrmSelectStation Wind)
        {
            MyWind = Wind;
        }
        private string _StnCode, _SelectStationCode, _CurrencyCode;

        public string StnCode { get { return _StnCode; } set { _StnCode = value; OnPropertyChanged("StnCode"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            if (Obj.ToString() == "OK")
            {
                if (!string.IsNullOrEmpty(StnCode))
                {
                    SqlDataReader dr = GetDataReader($"SELECT * FROM tblNetworkStation WHERE RTRIM(StationCode) = '{StnCode.Trim()}'");
                    if (dr.HasRows)
                    {
                        XMLUpdate();
                    }
                    else { MessageBox.Show("Kindly enter valid Station Code", "Warning"); }
                }
                else { MessageBox.Show("Kindly enter valid Station Code", "Warning"); }
            }
            else if (Obj.ToString() == "QUIT")
            {
                Application.Current.Shutdown();
            }
            else if (Obj.ToString() == "Close")
            {
                CloseWind();
            }
        }
        private void XMLUpdate()
        {
            try
            {
                if (System.IO.File.Exists(@System.Windows.Forms.Application.StartupPath + "\\" + "ServerConfig.xml"))
                {
                    XmlDocument xml = new XmlDocument();

                    xml.Load(@System.Windows.Forms.Application.StartupPath + "\\" + "ServerConfig.xml");

                    foreach (XmlElement element in xml.SelectNodes("//ServerConfig"))
                    {
                        foreach (XmlElement element1 in element)
                        {
                            if (element1.Name == "Station")
                            {
                                //MessageBox.Show(element1.InnerText);
                                XmlNode newvalue = xml.CreateElement("Station");
                                newvalue.InnerText = StnCode;
                                element.ReplaceChild(newvalue, element1);

                                xml.Save(@System.Windows.Forms.Application.StartupPath + "\\" + "ServerConfig.xml");
                                CodeChoosen = StnCode;
                                CloseWind();
                            }
                        }
                    }
                }
            }
            catch (Exception Ex) { error_log.errorlog("Station Code Selection XML update: " + Ex.ToString()); }
        }
    }
}
