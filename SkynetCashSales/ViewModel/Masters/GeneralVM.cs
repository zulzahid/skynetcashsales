﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class GeneralVM : AMGenFunction
    {
        public FrmGeneral MyWind;
        public GeneralVM(FrmGeneral Wind)
        {
            MyWind = Wind;
            GenDetails = new ObservableCollection<GenInfo>();
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        public long GenId { get { return GetValue(() => GenId); } set { SetValue(() => GenId, value); OnPropertyChanged("GenId"); } }
        public string GenDesc { get { return GetValue(() => GenDesc); } set { SetValue(() => GenDesc, value); OnPropertyChanged("GenDesc"); } }
        public string GenSDesc { get { return GetValue(() => GenSDesc); } set { SetValue(() => GenSDesc, value); OnPropertyChanged("GenSDesc"); } }
        public long GroupId { get { return GetValue(() => GroupId); } set { SetValue(() => GroupId, value); OnPropertyChanged("GroupId"); } }
        public string GroupDesc { get { return GetValue(() => GroupDesc); } set { SetValue(() => GroupDesc, value); OnPropertyChanged("GroupDesc"); } }

        private ObservableCollection<GenInfo> _GenDetails = new ObservableCollection<GenInfo>();
        public ObservableCollection<GenInfo> GenDetails { get { return _GenDetails; } set { _GenDetails = value; } }

        private GenInfo _SelectedItem = new GenInfo();
        public GenInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "Load Selected Item": if (SelectedItem != null) DispSelectedItem(Obj.ToString(), SelectedItem.GenId); break;
                    case "Gen. Desc.":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenDetId, ColOneText = a.GDDesc, ColTwoText = b.GenDesc FROM tblGeneralDet a" +
                                $" JOIN tblGeneralMaster b ON a.GenId = b.GenId" +
                                $" ORDER BY a.GDDesc");
                            ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt64(r.Id)); });
                            ChildWnd.TwoColWindShow(dt, "Gen. Desc. Selection", "Description", "Group");
                        }
                        break;
                    case "Group Desc.":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.GenId, ColOneText = a.GenDesc FROM tblGeneralMaster a ORDER BY a.GenDesc");
                            ChildWnd.FrmListSingleCol_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt32(r.Id)); });
                            ChildWnd.SingleColWindShow(dt, "Groip Selection");
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
           
        private void DispSelectedItem(string Ref, long RefId)
        {
            try
            {
                SqlDataReader dr;
                if (Ref == "Load Selected Item" || Ref == "Gen. Desc.")
                {
                    if (RefId > 0)
                    {
                        dr = GetDataReader($"SELECT a.GenDetId, a.GDDesc, a.GDSDesc, b.GenId, b.GenDesc FROM tblGeneralDet a" +
                            $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                            $" WHERE a.GenDetId = {RefId}");
                        if (dr.Read())
                        {
                            GenId = Convert.ToInt64(dr["GenDetId"]);
                            GenDesc = dr["GDDesc"].ToString();
                            GenSDesc = dr["GDSDesc"].ToString();
                            GroupDesc = dr["GenDesc"].ToString();
                            GroupId = Convert.ToInt64(dr["GenId"]);
                        }
                        dr.Close();
                    }
                }
                else if (Ref == "Group Desc.")
                {
                    if (RefId > 0)
                    {
                        dr = GetDataReader($"SELECT GenId, GenDesc FROM tblGeneralMaster WHERE GenId = {RefId}");
                        if (dr.Read())
                        {
                            GroupDesc = dr["GenDesc"].ToString();
                            GroupId = Convert.ToInt64(dr["GenId"]);
                        }
                        dr.Close();
                    }
                }

                GenDetails.Clear();
                dr = GetDataReader($"SELECT a.GenDetId, a.GDDesc, b.GenDesc FROM tblGeneralDet a" +
                    $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                    $" WHERE b.GenId = {GroupId} ORDER BY a.GDDesc");
                while (dr.Read())
                {
                    GenDetails.Add(new GenInfo { GenId = Convert.ToInt32(dr["GenDetId"]), GenDesc = dr["GDDesc"].ToString(), GroupDesc = dr["GenDesc"].ToString() });
                }
                dr.Close();
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void ResetData()
        {
            GenId = 0; GenDesc = ""; GenSDesc = ""; GroupId = 0; GroupDesc = "";
            GenDetails.Clear();

            SqlDataReader dr = GetDataReader($"SELECT a.GenDetId, a.GDDesc, b.GenDesc FROM tblGeneralDet a" +
                $" JOIN tblGeneralMaster b on a.GenId = b.GenId" +
                $" ORDER BY a.GDDesc");
            while (dr.Read())
            {
                GenDetails.Add(new GenInfo { GenId = Convert.ToInt32(dr["GenDetId"]), GenDesc = dr["GDDesc"].ToString(), GroupDesc = dr["GenDesc"].ToString() });
            }
            dr.Close();
            MyWind.txtGenDesc.Focus();
        }

        private void SaveData()
        {
            if (GenDesc != "" && GroupId > 0 && GroupDesc != "")
            {
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblGeneralDet a WHERE a.GenDetId = {GenId}");
                if (dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm Modify this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblGeneralDet SET " +
                            $" GenId = {Convert.ToInt32(GroupId)}," +
                            $" GDDesc = '{GenDesc}'," +
                            $" GDSDesc = '{GenSDesc}'," +
                            $" ModifyBy = {LoginUserId}," +
                            $" ModifyDate = GETDATE()," +
                            $" Status = 1" +
                            $" WHERE GenDetId = {GenId}");

                        MessageBox.Show("Data Modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Save this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblGeneralDet (GenId, GDDesc, GDSDesc, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                            $" VALUES({Convert.ToInt32(GroupId)}, '{GenDesc}', '{GenSDesc}', {LoginUserId}, GETDATE(), 0, null, 1)");
                        
                        MessageBox.Show("Saved Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else
                MessageBox.Show("Invalid Data to Save", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

            ResetData();
        }

        public class GenInfo : AMGenFunction
        {
            public long GenId { get { return GetValue(() => GenId); } set { SetValue(() => GenId, value); OnPropertyChanged("GenId"); } }
            public string GenDesc { get { return GetValue(() => GenDesc); } set { SetValue(() => GenDesc, value); OnPropertyChanged("GenDesc"); } }
            public string GroupDesc { get { return GetValue(() => GroupDesc); } set { SetValue(() => GroupDesc, value); OnPropertyChanged("GroupDesc"); } }
        }
    }
}
