﻿using Newtonsoft.Json;
using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class ZoneVM : AMGenFunction
    {
        public FrmZone MyWind;
        public ZoneVM(FrmZone Wind)
        {
            MyWind = Wind;
            AvailableDestDetails = new ObservableCollection<SelectedDestInfo>();
            SelectedDestDetails = new ObservableCollection<SelectedDestInfo>();
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }
        private int _SelectedIndexGen1, _SelectedIndexGen2;

        public long ZoneId { get { return GetValue(() => ZoneId); } set { SetValue(() => ZoneId, value); OnPropertyChanged("ZoneId"); } }
        public string ZoneCode { get { return GetValue(() => ZoneCode); } set { SetValue(() => ZoneCode, value); OnPropertyChanged("ZoneCode"); } }
        public string ZoneDesc { get { return GetValue(() => ZoneDesc); } set { SetValue(() => ZoneDesc, value); OnPropertyChanged("ZoneDesc"); } }
        public string ZoneType { get { return GetValue(() => ZoneType); } set { SetValue(() => ZoneType, value); OnPropertyChanged("ZoneType"); } }

        public string UnAssignedDestName { get { return GetValue(() => UnAssignedDestName); } set { SetValue(() => UnAssignedDestName, value); OnPropertyChanged("UnAssignedDestName"); FilterData(); } }
        public string UnAssignedDestCode { get { return GetValue(() => UnAssignedDestCode); } set { SetValue(() => UnAssignedDestCode, value); OnPropertyChanged("UnAssignedDestCode"); FilterData(); } }
        public string AssignedDestCode { get { return GetValue(() => AssignedDestCode); } set { SetValue(() => AssignedDestCode, value); OnPropertyChanged("AssignedDestCode"); FilterData(); } }

        private ObservableCollection<SelectedDestInfo> _AvailableDestDetails = new ObservableCollection<SelectedDestInfo>();
        public ObservableCollection<SelectedDestInfo> AvailableDestDetails { get { return _AvailableDestDetails; } set { _AvailableDestDetails = value; } }

        private ObservableCollection<SelectedDestInfo> _SelectedDestDetails = new ObservableCollection<SelectedDestInfo>();
        public ObservableCollection<SelectedDestInfo> SelectedDestDetails { get { return _SelectedDestDetails; } set { _SelectedDestDetails = value; } }

        private SelectedDestInfo _SelectedItemGen1 = new SelectedDestInfo();
        public SelectedDestInfo SelectedItemGen1 { get { return _SelectedItemGen1; } set { _SelectedItemGen1 = value; OnPropertyChanged("SelectedItemGen1"); } }

        private SelectedDestInfo _SelectedItemGen2 = new SelectedDestInfo();
        public SelectedDestInfo SelectedItemGen2 { get { return _SelectedItemGen2; } set { _SelectedItemGen2 = value; OnPropertyChanged("SelectedItemGen2"); } }

        public int SelectedIndexGen1 { get { return _SelectedIndexGen1; } set { _SelectedIndexGen1 = value; OnPropertyChanged("SelectedIndexGen1"); } }
        public int SelectedIndexGen2 { get { return _SelectedIndexGen2; } set { _SelectedIndexGen2 = value; OnPropertyChanged("SelectedIndexGen2"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "Zone Code":
                        {
                            dt = GetDataTable($"SELECT ZoneId As ColOneId, RTRIM(ZoneCode) As ColOneText, ZoneDesc As ColTwoText FROM tblZone ORDER BY ZoneDesc");
                            ChildWnd.FrmListCol2_Closed += (r => { ZoneId = Convert.ToInt32(r.Id); ZoneCode = r.ColOneText.Trim(); ZoneDesc = r.ColTwoText.Trim(); LoadData(); });
                            ChildWnd.TwoColWindShow(dt, "Zone Selection", "Code", "Zone");
                        }
                        break;
                    case "Load Zone": LoadData(); break;
                    case "Domestic Zone": { ResetData(); ZoneType = "LOCAL"; } break;
                    case "International Zone": { ResetData(); ZoneType = "INT"; } break;
                    case "Select Single Destination": AddZoneItem(); break;
                    case "DeSelect Single Destination": CancelZoneItem(); break;
                    case "Save": SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void ResetData()
        {
            AvailableDestDetails.Clear(); SelectedDestDetails.Clear();
            MyWind.txtZoneCode.IsReadOnly = false;
            ZoneId = 0; ZoneCode = ""; ZoneDesc = ""; ZoneType = "LOCAL";
            UnAssignedDestCode = ""; UnAssignedDestName = ""; AssignedDestCode = "";
            MyWind.txtZoneCode.Focus();
        }

        private void LoadData()
        {
            if (ZoneCode != "")
            {
                SelectedDestDetails.Clear();
                SqlDataReader dr = GetDataReader($"SELECT ZoneId, ZoneCode, ZoneDesc, ZoneType FROM tblZone WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}'");
                if (dr.Read())
                {
                    ZoneId = Convert.ToInt64(dr["ZoneId"]);
                    ZoneCode = dr["ZoneCode"].ToString().Trim();
                    ZoneDesc = dr["ZoneDesc"].ToString().Trim();
                    if (dr["ZoneType"].ToString().Trim() == "LOCAL")
                    {
                        MyWind.rdbDomesticZone.IsChecked = true;
                    }
                    else if (dr["ZoneType"].ToString().Trim() == "INT")
                    {
                        MyWind.rdbInternationalZone.IsChecked = true;
                    }

                    dr.Close();

                    DataTable dt = GetDataTable($"SELECT b.Country, b.State, b.DestName, b.DestCode FROM tblZoneDetail a" +
                        $" LEFT JOIN tblDestination b on a.DestCode = b.DestCode" +
                        $" where RTRIM(a.ZoneCode) = '{ZoneCode.Trim()}'");
                    for (int n = 0; n < dt.Rows.Count; n++)
                    {
                        SelectedDestDetails.Add(new SelectedDestInfo { CountryName = dt.Rows[n]["Country"].ToString(), StateName = dt.Rows[n]["State"].ToString(), DestName = dt.Rows[n]["DestName"].ToString(), DestCode = dt.Rows[n]["DestCode"].ToString().Trim() });
                        for (int i = 0; i < AvailableDestDetails.Count; i++)
                        {
                            if (AvailableDestDetails[i].DestCode.Trim() == dt.Rows[n]["DestCode"].ToString().Trim()) { AvailableDestDetails.RemoveAt(i); break; }
                        }
                    }
                    MyWind.txtZoneCode.IsReadOnly = true;
                }
                else MyWind.txtZoneDesc.Focus();
                dr.Close();
            }
        }

        private void AddZoneItem()
        {
            if (IsValid && SelectedItemGen1 != null)
            {
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblZone WHERE ZoneId = {ZoneId}");
                if (!dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm Create this Zone?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblZone (ZoneCode, ZoneDesc, ZoneType) VALUES('{ZoneCode.Trim()}', '{ZoneDesc.Trim()}', '{ZoneType.Trim()}')");                        
                        ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('{ZoneCode.Trim()}', '{SelectedItemGen1.DestCode.Trim()}', 0, NULL)");
                        //SelectedDestDetails.Add(new SelectedDestInfo { CountryName = SelectedItemGen1.CountryName, StateName = SelectedItemGen1.StateName, DestName = SelectedItemGen1.DestName, DestCode = SelectedItemGen1.DestCode });
                        //AvailableDestDetails.RemoveAt(SelectedIndexGen1);
                        MessageBox.Show("Zone Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        QuotationDetInsert();
                        CheckWebData(ZoneCode.Trim(), SelectedItemGen1.DestCode.Trim());
                    }
                }
                else
                {
                    dr = GetDataReader($"SELECT * FROM tblZoneDetail WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}' AND RTRIM(DestCode) = '{SelectedItemGen1.DestCode.Trim()}'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('{ZoneCode.Trim()}', '{SelectedItemGen1.DestCode.Trim()}', 0, NULL)");
                        //SelectedDestDetails.Add(new SelectedDestInfo { CountryName = SelectedItemGen1.CountryName, StateName = SelectedItemGen1.StateName, DestName = SelectedItemGen1.DestName, DestCode = SelectedItemGen1.DestCode });
                        //AvailableDestDetails.RemoveAt(SelectedIndexGen1);
                        CheckWebData(ZoneCode.Trim(), SelectedItemGen1.DestCode.Trim());
                    }                    
                }
                FilterData();
            }
        }

        private void CancelZoneItem()
        {
            if (SelectedItemGen2 != null)
            {
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblZoneDetail WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}' AND RTRIM(DestCode) = '{SelectedItemGen2.DestCode.Trim()}'");
                if (dr.HasRows)
                {
                    ExecuteQuery($"DELETE FROM tblZoneDetail WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}' AND RTRIM(DestCode) = '{SelectedItemGen2.DestCode.Trim()}'");
                }

                FilterData();

                //AvailableDestDetails.Add(new SelectedDestInfo { CountryName = SelectedItemGen2.CountryName, StateName = SelectedItemGen2.StateName, DestName = SelectedItemGen2.DestName, DestCode = SelectedItemGen2.DestCode });
                //SelectedDestDetails.RemoveAt(SelectedIndexGen2);
            }
        }

        static readonly string[] ValidatedProperties = { "ZoneCode", "ZoneDesc" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    string errormsg = GetValidationError(property);
                    if (errormsg != null)
                    { // there is an error
                        MessageBox.Show(errormsg);
                        return false;
                    }
                }
                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "ZoneCode":
                    {
                        if (string.IsNullOrEmpty(ZoneCode))
                        {
                            error = "ZoneCode is required"; MyWind.txtZoneCode.Focus();
                        }
                        else if (ZoneCode.Trim() != "" && ZoneId <= 0)
                        {
                            SqlDataReader dr = GetDataReader($"SELECT * FROM tblZone WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}'");
                            if (dr.Read())
                            {
                                ZoneId = Convert.ToInt64(dr["ZoneId"]);
                                ZoneDesc = dr["ZoneDesc"].ToString().Trim();
                                if (dr["ZoneType"].ToString().Trim() == "LOCAL")
                                {
                                    MyWind.rdbDomesticZone.IsChecked = true;
                                }
                                else if (dr["ZoneType"].ToString().Trim() == "INT")
                                {
                                    MyWind.rdbInternationalZone.IsChecked = true;
                                }
                            }
                            dr.Close();
                        }
                    }
                    break;
                case "ZoneDesc": if (string.IsNullOrEmpty(ZoneDesc)) { error = "ZoneDesc is required"; MyWind.txtZoneDesc.Focus(); } break;
                default: throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }

        private void SaveData()
        {
            if (IsValid)
            {
                MessageBoxResult MsgRes;
                SqlDataReader dr = GetDataReader($"SELECT * FROM tblZone WHERE ZoneId = {ZoneId}");
                if (dr.HasRows)
                {
                    MsgRes = MessageBox.Show("Confirm modify this Zone?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblZone SET ZoneCode = '{ZoneCode.Trim()}', ZoneDesc = '{ZoneDesc.Trim()}', ZoneType = '{ZoneType.Trim()}' WHERE ZoneId = {ZoneId}");
                        MessageBox.Show("Zone modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MsgRes = MessageBox.Show("Confirm Create this Zone?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblZone (ZoneCode, ZoneDesc, ZoneType) VALUES('{ZoneCode.Trim()}', '{ZoneDesc.Trim()}', '{ZoneType.Trim()}')");
                        MessageBox.Show("Zone Created Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                QuotationDetInsert();
                ResetData();
            }            
        }

        private void QuotationDetInsert()
        {
            string QuotationType = ZoneType == "LOCAL" ? "Standard" : "International";
            long QuotationId = 0;
            SqlDataReader dr = GetDataReader($"SELECT QuotationId FROM tblQuotation WHERE UPPER(RTRIM(QuotationType)) = '{QuotationType.ToUpper()}' AND ToDate IS NULL");
            if (dr.Read())
            {
                QuotationId = Convert.ToInt64(dr["QuotationId"]);
            }

            if (QuotationId > 0)
            {
                dr = GetDataReader($"SELECT QuotationId FROM tblQuotationDet WHERE QuotationId = {QuotationId} AND UPPER(RTRIM(ToZone)) = '{ZoneCode.Trim().ToUpper()}' AND UPPER(RTRIM(ShipmentType)) = 'DOCUMENTS'");
                if (!dr.HasRows)
                {
                    ExecuteQuery($"INSERT INTO tblQuotationDet (QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate)" +
                        $" VALUES({QuotationId}, 1, 'LOCAL', '{ZoneCode.Trim()}', 'DOCUMENTS', 0.50, 6.50, 0.50, 2.00, 0.00, GETDATE(), NULL, 0, NULL)");
                }

                dr = GetDataReader($"SELECT QuotationId FROM tblQuotationDet WHERE QuotationId = {QuotationId} AND UPPER(RTRIM(ToZone)) = '{ZoneCode.Trim().ToUpper()}' AND UPPER(RTRIM(ShipmentType)) = 'PARCELS'");
                if (!dr.HasRows)
                {
                    ExecuteQuery($"INSERT INTO tblQuotationDet (QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate)" +
                        $" VALUES({QuotationId}, 1, 'LOCAL', '{ZoneCode.Trim()}', 'PARCELS', 0.50, 6.50, 0.50, 2.00, 0.00, GETDATE(), NULL, 0, NULL)");
                }
            }
        }

        private void FilterData()
        {
            if (ZoneCode != null && UnAssignedDestCode != null && UnAssignedDestName != null && AssignedDestCode != null)
            {
                DataTable dtMain = GetDataTable($" SELECT DISTINCT a.Country, a.State, a.DestName, a.DestCode, ISNULL(b.ZoneCode, '') AS ZoneCode FROM tblDestination a LEFT JOIN tblZoneDetail b ON a.DestCode = b.DestCode");
                DataTable dt;
                if (ZoneCode.Trim() != "")
                {
                    if (ZoneId <= 0)
                    {
                        SqlDataReader dr = GetDataReader($"SELECT * FROM tblZone WHERE RTRIM(ZoneCode) = '{ZoneCode.Trim()}'");
                        if (dr.Read())
                        {
                            ZoneId = Convert.ToInt64(dr["ZoneId"]);
                            ZoneDesc = dr["ZoneDesc"].ToString().Trim();
                            if (dr["ZoneType"].ToString().Trim() == "LOCAL")
                            {
                                MyWind.rdbDomesticZone.IsChecked = true;
                            }
                            else if (dr["ZoneType"].ToString().Trim() == "INT")
                            {
                                MyWind.rdbInternationalZone.IsChecked = true;
                            }

                            MyWind.txtZoneCode.IsReadOnly = true;
                        }
                        dr.Close();
                    }

                    AvailableDestDetails.Clear();
                    DataView view = new DataView(dtMain, "ZoneCode NOT LIKE '" + ZoneCode + "'", "", DataViewRowState.CurrentRows);
                    dt = new DataTable(); dt = view.ToTable();

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        using (view = new DataView(dt, "DestName LIKE '" + UnAssignedDestName + "%' AND DestCode LIKE '" + UnAssignedDestCode + "%'", "", DataViewRowState.CurrentRows))
                        {
                            dt = new DataTable(); dt = view.ToTable();
                            for (int n = 0; n < dt.Rows.Count; n++)
                            {
                                AvailableDestDetails.Add(new SelectedDestInfo { CountryName = dt.Rows[n]["Country"].ToString(), StateName = dt.Rows[n]["State"].ToString(), DestName = dt.Rows[n]["DestName"].ToString(), DestCode = dt.Rows[n]["DestCode"].ToString() });
                            }
                        }
                    }

                    SelectedDestDetails.Clear();
                    view = new DataView(dtMain, "ZoneCode LIKE '" + ZoneCode + "'", "", DataViewRowState.CurrentRows);
                    dt = new DataTable(); dt = view.ToTable();

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        using (view = new DataView(dt, "DestCode LIKE '" + AssignedDestCode + "%'", "", DataViewRowState.CurrentRows))
                        {
                            dt = new DataTable(); dt = view.ToTable();
                            for (int n = 0; n < dt.Rows.Count; n++)
                            {
                                SelectedDestDetails.Add(new SelectedDestInfo { CountryName = dt.Rows[n]["Country"].ToString(), StateName = dt.Rows[n]["State"].ToString(), DestName = dt.Rows[n]["DestName"].ToString(), DestCode = dt.Rows[n]["DestCode"].ToString() });
                            }
                        }
                    }
                }
                else
                {
                    AvailableDestDetails.Clear();
                    if (dtMain != null && dtMain.Rows.Count > 0)
                    {
                        using (DataView view = new DataView(dtMain, "DestName LIKE '" + UnAssignedDestName + "%' AND DestCode LIKE '" + UnAssignedDestCode + "%'", "", DataViewRowState.CurrentRows))
                        {
                            dt = new DataTable(); dt = view.ToTable();
                            for (int n = 0; n < dt.Rows.Count; n++)
                            {
                                AvailableDestDetails.Add(new SelectedDestInfo { CountryName = dt.Rows[n]["Country"].ToString(), StateName = dt.Rows[n]["State"].ToString(), DestName = dt.Rows[n]["DestName"].ToString(), DestCode = dt.Rows[n]["DestCode"].ToString() });
                            }
                        }
                    }
                }
            }
        }

        private void CheckWebData(string ZoneCode, string DestCode)
        {
            bool Status = false;
            List<clsZoneDet> data;
            clsGetZoneDet apiref;

            try
            {
                if (DestCode.Trim() != "")
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://skynetmy.ddns.net:8089");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            if (ZoneCode.Trim() != "")
                            {
                                data = new List<clsZoneDet>();
                                apiref = new clsGetZoneDet
                                {
                                    AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                                    FunctionStr = "GetZoneDet",
                                    ZoneCode = ZoneCode,
                                    zoneData = data
                                };
                                var response = client.PostAsJsonAsync("api/WebMasters/GetZone", apiref).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var res = response.Content.ReadAsStringAsync().Result;
                                    if (!res.Contains("Record not found"))
                                    {
                                        clsGetZoneDetRes item = JsonConvert.DeserializeObject<clsGetZoneDetRes>(res.ToString());
                                        if (item.status == "Success" && item.data.Count > 0)
                                        {
                                            Status = false;
                                            for (int i = 0; i < item.data.Count; i++)
                                            {
                                                if (item.data[i].DestCode == DestCode)
                                                {
                                                    Status = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        if (Status == false)
                        {
                            data = new List<clsZoneDet>();
                            data.Add(new clsZoneDet{
                            ZoneCode = ZoneCode,
                            DestCode = DestCode,
                            NetworkCode = "SKYNET",
                            StationCode = "",
                            SubName = "",
                            ShipmentType = ""});

                            apiref = new clsGetZoneDet
                            {
                                AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                                FunctionStr = "InsertZoneDet",
                                zoneData = data
                            };
                            var response = client.PostAsJsonAsync("api/WebMasters/InsertZoneDet", apiref).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var res = response.Content.ReadAsStringAsync().Result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog($"ZoneVM.CheckWebData({ZoneCode}, {DestCode}) : " + ex.ToString());
            }
        }

        public class SelectedDestInfo : AMGenFunction
        {
            public string CountryName { get { return GetValue(() => CountryName); } set { SetValue(() => CountryName, value); OnPropertyChanged("CountryName"); } }
            public string StateName { get { return GetValue(() => StateName); } set { SetValue(() => StateName, value); OnPropertyChanged("StateName"); } }
            public string DestName { get { return GetValue(() => DestName); } set { SetValue(() => DestName, value); OnPropertyChanged("DestName"); } }
            public string DestCode { get { return GetValue(() => DestCode); } set { SetValue(() => DestCode, value); OnPropertyChanged("DestCode"); } }
        }

        public class clsZoneDet
        {
            public int ZoneDetId { get; set; }
            public string ZoneCode { get; set; }
            public string DestCode { get; set; }
            public string NetworkCode { get; set; }
            public string StationCode { get; set; }
            public string SubName { get; set; }
            public string ReceivedTime { get; set; }
            public Nullable<int> ModifyBy { get; set; }
            public string ModifyDate { get; set; }
            public string ShipmentType { get; set; }
            public string TransmittedTime { get; set; }
            public Nullable<bool> IsDeleted { get; set; }
        }

        public class clsGetZoneDet
        {
            public string AccessToken { get; set; }
            public string FunctionStr { get; set; }
            public string ZoneCode { get; set; }
            public List<clsZoneDet> zoneData = new List<clsZoneDet>();
        }

        public class clsGetZoneDetRes
        {
            public string status { get; set; }
            public string code { get; set; }
            public string message { get; set; }
            public string referenceNumber { get; set; }
            public List<clsZoneDet> data = new List<clsZoneDet>();
        }
    }
}
