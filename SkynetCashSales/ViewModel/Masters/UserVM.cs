﻿using SkynetCashSales.General;
using SkynetCashSales.View.Help;
using SkynetCashSales.View.Masters;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class UserVM : AMGenFunction
    {
        public FrmUser MyWind;
        public UserVM(FrmUser Wind)
        {
            MyWind = Wind;
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        public long UserId { get { return GetValue(() => UserId); } set { SetValue(() => UserId, value); OnPropertyChanged("UserId"); } }
        public string UserName { get { return GetValue(() => UserName); } set { SetValue(() => UserName, value); OnPropertyChanged("UserName"); } }
        public SecureString Password { get { return GetValue(() => Password); } set { SetValue(() => Password, value); OnPropertyChanged("Password"); } }
        public string UserAuthentication { get { return GetValue(() => UserAuthentication); } set { SetValue(() => UserAuthentication, value); OnPropertyChanged("UserAuthentication"); } }
        public bool CmdMenu { get { return GetValue(() => CmdMenu); } set { SetValue(() => CmdMenu, value); OnPropertyChanged("CmdMenu"); } }
        public bool IsActivated { get { return GetValue(() => IsActivated); } set { SetValue(() => IsActivated, value); OnPropertyChanged("IsActivated"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            DataTable dt = new DataTable();
            var ChildWnd = new ListHelpGen();
            try
            {
                switch (Obj.ToString())
                {
                    case "User Name":
                        {
                            string condition1 = LoginUserAuthentication.Trim() == "User" ? " WHERE Status = 1" : "";
                            dt = GetDataTable($"SELECT UserId As ColOneId, UserName As ColOneText, Authentication As ColTwoText, (CASE Status WHEN 1 THEN 'ACTIVE' WHEN 0 THEN 'DEACTIVE' END) As ColThreeText FROM tblUserMaster {condition1} ORDER BY UserName");
                            ChildWnd.FrmListCol3_Closed += (r => { UserId = Convert.ToInt32(r.Id); UserName = r.ColOneText; LoadData(); });
                            ChildWnd.ThreeColWindShow(dt, "User Selection", "User", "Authentication", "Status");
                        }
                        break;
                    case "User Authentication":
                        {
                            dt = GetDataTable($"SELECT a.GenDetId As ColOneId, a.GDDesc As ColOneText FROM tblGeneralDet a" +
                                $" join tblGeneralMaster b on a.GenId = b.GenId" +
                                $" where b.GenDesc = 'USER AUTHENTICATION' order by a.GDDesc");

                            ChildWnd.FrmListSingleCol_Closed += (r => { UserAuthentication = r.ColOneText; });
                            ChildWnd.SingleColWindShow(dt, "Authentication Selection");
                        }
                        break;
                    case "Menu Selection":
                        {
                            if (UserId > 0)
                            {
                                FrmMenu Wnd = new FrmMenu();
                                Wnd.DataContext = new MenuVM(Wnd, UserId, UserName);
                                MyWind.ShowInTaskbar = false;
                                Wnd.Owner = Application.Current.MainWindow;
                                Wnd.ShowDialog();
                            }
                            else
                                MessageBox.Show("No User Selected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }

        private void LoadData()
        {
            if (UserId > 0)
            {
                SqlDataReader dr = GetDataReader($"SELECT a.UserId, a.UserName, a.Password, a.Authentication, a.Status FROM tblUserMaster a where a.UserId = {UserId}");
                if (dr.Read())
                {
                    UserId = Convert.ToInt64(dr["UserId"]);
                    UserName = dr["UserName"].ToString();
                    Password = ToSecureString(dr["Password"].ToString());
                    UserAuthentication = dr["Authentication"].ToString();
                    IsActivated = Convert.ToBoolean(dr["Status"]);
                }
                dr.Close();

                if (UserId > 0) CmdMenu = true;
            }
        }

        private void SaveData()
        {
            if (UserId == 0)
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Save?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    if (IsValidate)
                    {
                        if (LoginUserAuthentication.Trim() != "User")
                        {
                            ExecuteQuery($"INSERT INTO tblUserMaster(UserName, Password, Authentication, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                                $" VALUES('{UserName}', '{SecureStringToString(Password)}', '{UserAuthentication}', { LoginUserId}, GETDATE(), null, null, '{IsActivated}')");

                            MessageBox.Show("Successfully created", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                            ResetData();
                        }
                        else MessageBox.Show("You don't have rights to CREATE", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else if (UserId > 0)
            {
                MessageBoxResult MsgRes;
                if (LoginUserAuthentication.Trim() == "User" && UserId == LoginUserId && Password.ToString().Trim() != "")
                {
                    if (UserId == LoginUserId && Password.ToString().Trim() != "")
                    {
                        MsgRes = MessageBox.Show("Confirm Modify?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (MsgRes == MessageBoxResult.Yes)
                        {
                            ExecuteQuery($"UPDATE tblUserMaster SET UserName = '{UserName}', Password = '{SecureStringToString(Password)}', ModifyBy = {LoginUserId}, ModifyDate = GETDATE() Where UserId = {UserId}");
                            MessageBox.Show("Successfully updated", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("You can not modify the other's login details", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                }
                else if (LoginUserAuthentication.Trim() != "User" && IsValidate)
                {
                    MsgRes = MessageBox.Show("Confirm Modify?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblUserMaster SET UserName = '{UserName}', Password = '{SecureStringToString(Password)}', Authentication = '{UserAuthentication}', ModifyBy = { LoginUserId}, ModifyDate = GETDATE(), Status = '{IsActivated}' Where UserId = {UserId}");
                        MessageBox.Show("Successfully updated", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

                ResetData();
            }
        }

        static readonly string[] ValidatedProperties = { "UserName", "Password", "UserAuthentication", "LoginUserAuthentication" };
        public bool IsValidate
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null) return false; // there is an error
                } 
                return true;
            }
        }

        private string GetValidationError(string PropertyName)
        {
            string error = null;
            switch (PropertyName)
            {
                case "UserName": if (string.IsNullOrEmpty(UserName)) { error = "Invalid User name"; MyWind.txtUserName.Focus(); } break;
                case "Password": if (Password == null || Password.ToString() == "") { error = "Invalid Password"; MyWind.pwdLoginPassword.Focus(); } break;
                case "UserAuthentication": if (string.IsNullOrEmpty(UserAuthentication)) { error = "Invalid Authentication"; MyWind.txtAuthentication.Focus(); } break;
                case "LoginUserAuthentication": if (LoginUserAuthentication.Trim() != "Admin") { error = "Authentication Problem"; ResetData(); } break;
                default: error = null; throw new Exception("Unexpected property being validated on Service");
            }
            if (error != null) System.Windows.MessageBox.Show(error);
            return error;
        }

        private void ResetData()
        {
            UserId = 0; UserName = ""; Password = new SecureString();
            UserAuthentication = ""; CmdMenu = false; IsActivated = true;
            MyWind.txtUserName.Focus();
        }
    }
}
