﻿using SkynetCashSales.General;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SkynetCashSales.View.Masters;
using SkynetCashSales.Model;
using System.Data.SqlClient;

namespace SkynetCashSales.ViewModel.Masters
{
    public class UserAuthenticationVM : AMGenFunction
    {
        public FrmUserAuthentication MyWind;
        public UserAuthenticationVM(FrmUserAuthentication Wind)
        {
            MyWind = Wind;
            UserRightsDetails = new ObservableCollection<UserRightsInfo>();
            ResetData();
            if (LoginUserAuthentication == "User") { MyWind.btnSave.Visibility = Visibility.Hidden; }
            else { MyWind.btnSave.Visibility = Visibility.Visible; }
        }

        private long _UserAuthenticationId;
        private string _UserAuthentication;

        public long UserAuthenticationId { get { return _UserAuthenticationId; } set { _UserAuthenticationId = value; OnPropertyChanged("UserAuthenticationId"); } }
        public string UserAuthentication { get { return _UserAuthentication; } set { _UserAuthentication = value; OnPropertyChanged("UserAuthentication"); } }

        private ObservableCollection<UserRightsInfo> _UserRightsDetails = new ObservableCollection<UserRightsInfo>();
        public ObservableCollection<UserRightsInfo> UserRightsDetails { get { return _UserRightsDetails; } set { _UserRightsDetails = value; } }

        private UserRightsInfo _SelectedItem = new UserRightsInfo();
        public UserRightsInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen, _CommandHelp;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }
        public ICommand CommandHelp { get { if (_CommandHelp == null) _CommandHelp = new RelayCommand(Parameter => ExecuteCommandHelp(Parameter)); return _CommandHelp; } }

        private void ExecuteCommandGen(object Obj)
        {
            if (Obj.ToString() == "Save")
            {
                SaveData();
            }
            else if (Obj.ToString() == "Clear")
            {
                ResetData();
            }
            else if (Obj.ToString() == "Close")
            {
                CloseWind();
            }
        }

        private void ExecuteCommandHelp(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            if (Obj.ToString() == "User Authentication")
            {
                dt = GetDataTable($"SELECT a.GenDetId As ColOneId, a.GDDesc As ColOneText, b.GenDesc As ColTwoText FROM tblGeneralDet a" +
                    $" join tblGeneralMaster b on a.GenId = b.GenId" +
                    $" where b.GenDesc = 'USER AUTHENTICATION' order by a.GDDesc");
                ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt32(r.Id)); });
                ChildWnd.TwoColWindShow(dt, "Authentication Selection", "Description", "Group");
            }
        }

        private void DispSelectedItem(string Ref, int RefId)
        {
            try
            {
                UserRightsDetails.Clear();

                DataTable dt = new DataTable();
                dt = GetDataTable($"SELECT a.GenDetId, a.GDDesc, b.GenDesc FROM tblGeneralDet a" +
                    $" join tblGeneralMaster b on a.GenId = b.GenId" +
                    $" where b.GenDesc = 'USER RIGHTS' order by a.GDDesc");

                foreach (DataRow row in dt.Rows)
                {
                    UserRightsDetails.Add(new UserRightsInfo { UserRightsId = Convert.ToInt32(row["GenDetId"]), UserRights = row["GDDesc"].ToString(), Status = false });
                }

                if (Ref == "User Authentication")
                {
                    if (RefId > 0)
                    {
                        SqlDataReader dr = GetDataReader($"SELECT a.GenDetId, a.GDDesc FROM tblGeneralDet a" +
                            $" join tblGeneralMaster b on a.GenId = b.GenId" +
                            $" where a.GenDetId = {RefId}");

                        if (dr.Read())
                        {
                            UserAuthenticationId = Convert.ToInt64(dr["GenDetId"]);
                            UserAuthentication = dr["GDDesc"].ToString();
                        }
                        dr.Close();
                    }
                }

                dt = new DataTable();
                dt = GetDataTable($"SELECT a.Status, a.RightsId FROM tblUserAuthentication a where a.AuthenticationId = {UserAuthenticationId}");

                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < UserRightsDetails.Count; i++)
                    {
                        if (Convert.ToInt32(row["RightsId"]) == UserRightsDetails[i].UserRightsId)
                        {
                            UserRightsDetails[i].Status = Convert.ToBoolean(row["Status"]);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }

        private void SaveData()
        {
            int ID = 0;
            if (UserAuthenticationId > 0 && UserAuthentication != "")
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Save?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    if (UserRightsDetails.Count > 0)
                    {
                        for (int i = 0; i < UserRightsDetails.Count; i++)
                        {
                            ID = UserRightsDetails[i].UserRightsId;
                            
                            SqlDataReader dr = GetDataReader($"SELECT Id FROM tblUserAuthentication a where AuthenticationId = {UserAuthenticationId} AND RightsId = {ID}");
                            if (dr.HasRows)
                            {
                                ExecuteQuery($"UPDATE tblUserAuthentication SET Rights = '{UserRightsDetails[i].UserRights}', Status = {UserRightsDetails[i].Status} where AuthenticationId = {UserAuthenticationId} AND RightsId = {ID}");
                            }
                            else
                            {
                                ExecuteQuery($"INSERT INTO tblUserAuthentication(AuthenticationId, Authentication, RightsId, Rights, Status)" +
                                    $" VALUES ({Convert.ToInt32(UserAuthenticationId)}, '{UserAuthentication}', {UserRightsDetails[i].UserRightsId}, , '{UserRightsDetails[i].UserRights}', {UserRightsDetails[i].Status}");
                            }
                            MessageBox.Show("Saved Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
            }
            ResetData();
        }

        private void ResetData()
        {
            UserAuthenticationId = 0; UserAuthentication = "";
            UserRightsDetails.Clear();

            DataTable dt = new DataTable();
            dt = GetDataTable($"SELECT a.GenDetId, a.GDDesc, b.GenDesc FROM tblGeneralDet a" +
                $" join tblGeneralMaster b on a.GenId = b.GenId" +
                $" where b.GenDesc = 'USER RIGHTS' order by a.GDDesc");

            foreach (DataRow row in dt.Rows)
            {
                UserRightsDetails.Add(new UserRightsInfo { UserRightsId = Convert.ToInt32(row["GenDetId"]), UserRights = row["GDDesc"].ToString(), Status = false });
            }

            MyWind.txtUserAuthentication.Focus();
        }

        public class UserRightsInfo : AMGenFunction
        {
            private int _UserRightsId;
            private string _UserRights;
            private bool _Status;

            public int UserRightsId { get { return _UserRightsId; } set { _UserRightsId = value; OnPropertyChanged("UserRightsId"); } }
            public string UserRights { get { return _UserRights; } set { _UserRights = value; OnPropertyChanged("UserRights"); } }
            public bool Status { get { return _Status; } set { _Status = value; OnPropertyChanged("Status"); } }
        }

    }
}
