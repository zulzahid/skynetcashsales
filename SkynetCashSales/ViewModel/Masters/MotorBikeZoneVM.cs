﻿using SkynetCashSales.General;
using SkynetCashSales.View.Masters;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Masters
{
    public class MotorBikeZoneVM : AMGenFunction
    {
        public FrmMotorBikeZone MyWind;
        public MotorBikeZoneVM(FrmMotorBikeZone Wind)
        {
            MyWind = Wind;
            BMZoneDetails = new ObservableCollection<BMZoneInfo>();
            ResetData();

            MyWind.btnSave.Visibility = LoginUserAuthentication == "User" ? Visibility.Hidden : MyWind.btnSave.Visibility = Visibility.Visible; 
        }

        public long BMZoneId { get { return GetValue(() => BMZoneId); } set { SetValue(() => BMZoneId, value); OnPropertyChanged("BMZoneId"); } }
        public string ZoneCode { get { return GetValue(() => ZoneCode); } set { SetValue(() => ZoneCode, value); OnPropertyChanged("ZoneCode"); } }
        public bool IsMotorBike { get { return GetValue(() => IsMotorBike); } set { SetValue(() => IsMotorBike, value); OnPropertyChanged("IsMotorBike"); } }
        public bool IsBicycle { get { return GetValue(() => IsBicycle); } set { SetValue(() => IsBicycle, value); OnPropertyChanged("IsBicycle"); } }

        private ObservableCollection<BMZoneInfo> _BMZoneDetails = new ObservableCollection<BMZoneInfo>();
        public ObservableCollection<BMZoneInfo> BMZoneDetails { get { return _BMZoneDetails; } set { _BMZoneDetails = value; } }

        private BMZoneInfo _SelectedItem = new BMZoneInfo();
        public BMZoneInfo SelectedItem { get { return _SelectedItem; } set { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        private void ExecuteCommandGen(object Obj)
        {
            var ChildWnd = new ListHelpGen();
            DataTable dt = new DataTable();

            try
            {
                switch (Obj.ToString())
                {
                    case "Zone Code":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.ZoneId, ColOneText = a.ZoneCode, ColTwoText = a.ZoneDesc FROM tblZone a");
                            ChildWnd.FrmListCol2_Closed += (r => { DispSelectedItem(Obj.ToString(), Convert.ToInt32(r.Id)); });
                            ChildWnd.TwoColWindShow(dt, "Zone code Selection", "Zone Code", "Description");
                        }
                        break;
                    case "MBZone":
                        {
                            if(SelectedItem != null) DispSelectedItem(Obj.ToString(), SelectedItem.BMZoneId);
                        }
                        break;
                    case "Save": SaveData(); break;
                    case "Delete": DeleteData(SelectedItem.BMZoneId); break;
                    case "Clear": ResetData(); break;
                    case "Close": CloseWind(); break;
                    default: break;
                }
            }
            catch (Exception Ex) { error_log.errorlog(Ex.Message); }
        }
            
        private void DispSelectedItem(string Ref, int RefId)
        {
            SqlDataReader dr; DataTable dt;
            try
            {
                if (Ref == "Zone Code")
                {
                    if (RefId > 0)
                    {
                        dr = GetDataReader($"SELECT ZoneCode FROM tblZone Where ZoneId = {RefId}");
                        ZoneCode = dr.Read() ? dr["ZoneCode"].ToString() : ""; dr.Close();

                        BMZoneDetails.Clear();

                        dt = new DataTable();
                        dt = GetDataTable($"SELECT a.BMZoneId, a.ZoneCode, a.ShipmentType, a.IsActive FROM tblMotorBikeZone a where RTRIM(a.ZoneCode) = '{ZoneCode.Trim()}' order by a.ZoneCode");
                        foreach (DataRow row in dt.Rows)
                        {
                            BMZoneDetails.Add(new BMZoneInfo { Item = BMZoneDetails.Count + 1, BMZoneId = Convert.ToInt32(row["BMZoneId"]), ZoneCode = row["ZoneCode"].ToString(), ShipmentType = row["ShipmentType"].ToString(), IsActive = Convert.ToBoolean(row["IsActive"].ToString()) });
                        }
                        MyWind.txtZoneCode.Focus();
                    }
                }
                else if (Ref == "MBZone")
                {
                    if (RefId > 0)
                    {
                        dr = GetDataReader($"SELECT a.BMZoneId, a.ZoneCode, a.ShipmentType FROM tblMotorBikeZone a Where a.BMZoneId = {RefId}");
                        if (dr.Read())
                        {
                            BMZoneId = Convert.ToInt64(dr["BMZoneId"]);
                            ZoneCode = dr["ZoneCode"].ToString();
                            IsMotorBike = dr["ShipmentType"].ToString() == "Motor Bikes" ? true : false;                            
                        }
                        dr.Close();

                        dt = new DataTable();
                        dt = GetDataTable($"SELECT a.BMZoneId, a.ZoneCode, a.ShipmentType, a.IsActive FROM tblMotorBikeZone a where RTRIM(a.ZoneCode) = '{ZoneCode.Trim()}' order by a.ZoneCode");

                        BMZoneDetails.Clear();
                        foreach (DataRow row in dt.Rows)
                        {
                            BMZoneDetails.Add(new BMZoneInfo { Item = BMZoneDetails.Count + 1, BMZoneId = Convert.ToInt32(row["BMZoneId"]), ZoneCode = row["ZoneCode"].ToString(), ShipmentType = row["ShipmentType"].ToString(), IsActive = Convert.ToBoolean(row["IsActive"].ToString()) });
                        }
                        MyWind.txtZoneCode.Focus();
                    }
                }
                IsBicycle = !IsMotorBike;
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }      
        private void ResetData()
        {
            BMZoneId = 0; ZoneCode = ""; IsMotorBike = true; 
            BMZoneDetails.Clear();

            DataTable dt = new DataTable();
            dt = GetDataTable($"SELECT a.BMZoneId, a.ZoneCode, a.ShipmentType, a.IsActive FROM tblMotorBikeZone a order by a.ZoneCode");
            foreach (DataRow row in dt.Rows)
            {
                BMZoneDetails.Add(new BMZoneInfo { Item = BMZoneDetails.Count + 1, BMZoneId = Convert.ToInt32(row["BMZoneId"]), ZoneCode = row["ZoneCode"].ToString(), ShipmentType = row["ShipmentType"].ToString(), IsActive = Convert.ToBoolean(row["IsActive"].ToString()) });
            }

            MyWind.txtZoneCode.Focus();
        }

        private void SaveData()
        {
            if (ZoneCode.Trim() != "")
            {
                string shipment = IsMotorBike == true ? "MOTOR BIKES" : "BICYCLES";

                SqlDataReader dr = GetDataReader($"SELECT * FROM tblMotorBikeZone where BMZoneId = {BMZoneId}");
                if (!dr.HasRows)
                {
                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Save this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"INSERT INTO tblMotorBikeZone(ZoneCode, ShipmentType, CreateBy, CreateDate, ModifyBy, ModifyDate, IsActive)" +
                            $" VALUES('{ZoneCode}', '{shipment}', {LoginUserId}, GETDATE(), 0, null, 1)");

                        QuotationDetInsert();
                        MessageBox.Show("Saved Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult MsgRes = MessageBox.Show("Confirm Modify this data?", "Confirmation", MessageBoxButton.YesNo);
                    if (MsgRes == MessageBoxResult.Yes)
                    {
                        ExecuteQuery($"UPDATE tblMotorBikeZone SET ZoneCode = '{ZoneCode}', ShipmentType = '{shipment}', ModifyBy = {LoginUserId}, GETDATE(), 1 where BMZoneId = {BMZoneId}");
                        QuotationDetInsert();
                        MessageBox.Show("Data Modified Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else
                MessageBox.Show("Invalid Data to Save", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

            ResetData();
        }

        private void QuotationDetInsert()
        {
            string shipment = IsMotorBike == true ? "MOTOR BIKES" : "BICYCLES";

            SqlDataReader dr = GetDataReader($"SELECT * FROM tblQuotationDet where ToZone = '{ZoneCode}'");
            if(dr.HasRows)
            {
                dr.Read();
                string qry1 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({Convert.ToInt32(dr["QuotationId"])}, 5, '{dr["FromZone"].ToString()}', '{dr["ToZone"].ToString()}', '{dr["shipment"].ToString()}', 0.01, 239.00, 50.00, 0.00, 0.00, GETDATE(), null, 0, null)";
                
                string qry2 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({Convert.ToInt32(dr["QuotationId"])}, 6, '{dr["FromZone"].ToString()}', '{dr["ToZone"].ToString()}', '{dr["shipment"].ToString()}', 50.10, 380.00, 99.00, 0.00, 0.00, GETDATE(), null, 0, null)";

                string qry3 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({Convert.ToInt32(dr["QuotationId"])}, 7, '{dr["FromZone"].ToString()}', '{dr["ToZone"].ToString()}', '{dr["shipment"].ToString()}', 0.01, 120.00, 99.00, 0.00, 0.00, GETDATE(), null, 0, null)";
                dr.Close();

                dr = GetDataReader($"SELECT * FROM tblQuotationDet where ToZone = '{ZoneCode}' AND ShipmentType = '{shipment}'");
                if (!dr.HasRows)
                {
                    if (shipment == "MOTOR BIKES")
                    {
                        ExecuteQuery(qry1);
                        ExecuteQuery(qry2);
                    }
                    else if (shipment == "BICYCLES")
                    {
                        ExecuteQuery(qry3);
                    }
                }
            }
            else
            {
                long quotationid = 0;
                if (ZoneCode.Contains("ZONE")) 
                {
                    dr = GetDataReader($"SELECT QuotationId FROM tblQuotation WHERE QuotationType = 'Standard'");
                    quotationid = dr.Read() ? Convert.ToInt32(dr["QuotationId"]) : 0; dr.Close();
                }
                else 
                {
                    dr = GetDataReader($"SELECT QuotationId FROM tblQuotation WHERE QuotationType = 'International'");
                    quotationid = dr.Read() ? Convert.ToInt32(dr["QuotationId"]) : 0; dr.Close();
                }

                string qry1 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({quotationid}, 5, 'LOCAL', '{ZoneCode}', '{shipment}', 0.01, 239.00, 50.00, 0.00, 0.00, GETDATE(), null, 0, null)";

                string qry2 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({quotationid}, 6, 'LOCAL', '{ZoneCode}', '{shipment}', 50.10, 380.00, 99.00, 0.00, 0.00, GETDATE(), null, 0, null)";

                string qry3 = $"INSERT INTO tblQuotationDet(QuotationId, SlNo, FromZone, ToZone, ShipmentType, FirstWeight, FirstRate, AddWeight, AddRate, SurCharge, FromDate, ToDate, IsExported, ExportedDate" +
                    $" VALUES({quotationid}, 7, 'LOCAL', '{ZoneCode}', '{shipment}', 0.01, 120.00, 99.00, 0.00, 0.00, GETDATE(), null, 0, null)";

                if (shipment == "MOTOR BIKES")
                {
                    ExecuteQuery(qry1);
                    ExecuteQuery(qry2);
                }
                else if (shipment == "BICYCLES")
                {
                    ExecuteQuery(qry3);
                }
            }
        }

        private void QuotationDetDelete()
        {
            string shipment = IsMotorBike == true ? "MOTOR BIKES" : "BICYCLES";
            long quotationid = 0;

            SqlDataReader dr = GetDataReader($"SELECT * FROM tblQuotationDet where ToZone = '{ZoneCode}'");
            if (dr.HasRows)
            {
                quotationid = dr.Read() ? Convert.ToInt32(dr["QuotationId"]) : 0; dr.Close();
                ExecuteQuery($"DELETE FROM tblQuotationDet WHERE QuotationId = {quotationid} AND ShipmentType = '{shipment}'");
            }
        }

        private void DeleteData(int bmzoneid)
        {
            if (bmzoneid > 0)
            {
                MessageBoxResult MsgRes = MessageBox.Show("Confirm Delete this data?", "Confirmation", MessageBoxButton.YesNo);
                if (MsgRes == MessageBoxResult.Yes)
                {
                    ExecuteQuery($"DELETE FROM tblMotorBikeZone WHERE BMZoneId = {bmzoneid}");
                    QuotationDetDelete();
                }
                ResetData();
            }
        }
        public class BMZoneInfo : AMGenFunction
        {
            public int Item { get { return GetValue(() => Item); } set { SetValue(() => Item, value); OnPropertyChanged("Item"); } }
            public int BMZoneId { get { return GetValue(() => BMZoneId); } set { SetValue(() => BMZoneId, value); OnPropertyChanged("BMZoneId"); } }
            public string ZoneCode { get { return GetValue(() => ZoneCode); } set { SetValue(() => ZoneCode, value); OnPropertyChanged("ZoneCode"); } }
            public string ShipmentType { get { return GetValue(() => ShipmentType); } set { SetValue(() => ShipmentType, value); OnPropertyChanged("ShipmentType"); } }
            public bool IsActive { get { return GetValue(() => IsActive); } set { SetValue(() => IsActive, value); OnPropertyChanged("IsActive"); } }
        }
    }
}
