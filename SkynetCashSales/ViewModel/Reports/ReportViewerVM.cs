﻿using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Windows;
using System.Drawing.Printing;
using SkynetCashSales.General;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;

namespace SkynetCashSales.ViewModel.Reports
{
    public class ReportViewerVM : AMGenFunction
    {
        public ReportViewerVM(string DSCnAddress, DataTable DTCnAddress, string DSGenAddress, DataTable DTGenAddress, string DS1, DataTable DT1, string DS2, DataTable DT2, string RptFile, string HeaderString, ReportViewer RptViewer, bool Porttrait, bool Landscape)
        {
            ReportDataSource RptDS1 = new ReportDataSource();
            ReportDataSource RptDS2 = new ReportDataSource();
            ReportDataSource RptDS3 = new ReportDataSource();
            ReportDataSource RptDS4 = new ReportDataSource();
            ReportParameter RptPar = new ReportParameter("RptHeader", HeaderString);

            RptDS1.Name = DS1;
            RptDS1.Value = DT1;
            RptViewer.LocalReport.DataSources.Add(RptDS1);

            RptDS2.Name = DS2;
            RptDS2.Value = DT2;
            RptViewer.LocalReport.DataSources.Add(RptDS2);

            RptDS3.Name = DSCnAddress;
            RptDS3.Value = DTCnAddress;
            RptViewer.LocalReport.DataSources.Add(RptDS3);

            RptDS4.Name = DSGenAddress;
            RptDS4.Value = DTGenAddress;
            RptViewer.LocalReport.DataSources.Add(RptDS4);

            RptViewer.LocalReport.ReportEmbeddedResource = "SkynetCashSales.Reports." + RptFile;
            if (HeaderString != "")
                RptViewer.LocalReport.SetParameters(new ReportParameter[] { RptPar });

            //RptViewer.LocalReport.ReportPath = RptFile;
            //PageSettings pg = new PageSettings();
            //pg.Margins.Top = 0;
            //pg.Margins.Bottom = 0;
            //pg.Margins.Left = 0;
            //pg.Margins.Right = 0;
            //PaperSize size = new PaperSize();
            //size.RawKind = (int)PaperKind.A5;
            //pg.PaperSize = size;
            //RptViewer.SetPageSettings(pg);

            RptViewer.ZoomMode = ZoomMode.FullPage;

            if (Porttrait == true)
            {
                WHeight = 800; WWidth = 660;
            }
            else if (Landscape == true)
            {
                WHeight = 640; WWidth = 960;
            }            
            RptViewer.SetDisplayMode(DisplayMode.PrintLayout);

            RenderingExtension extension1 = RptViewer.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals("Word", StringComparison.CurrentCultureIgnoreCase));
            if (extension1 != null)
            {
                System.Reflection.FieldInfo fieldInfo = extension1.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                fieldInfo.SetValue(extension1, (LoginUserAuthentication == "User" ? false : true));
            }

            RenderingExtension extension2 = RptViewer.LocalReport.ListRenderingExtensions().ToList().Find(x => x.Name.Equals("Excel", StringComparison.CurrentCultureIgnoreCase));
            if (extension2 != null)
            {
                System.Reflection.FieldInfo fieldInfo = extension2.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                fieldInfo.SetValue(extension2, (LoginUserAuthentication == "User" ? false : true));
            }

            RptViewer.RefreshReport();
        }

        public ReportViewerVM(string DSCnAddress, DataTable DTCnAddress, string DSGenAddress, DataTable DTGenAddress, string DS1, DataTable DT1, string DS2, DataTable DT2, string RptFile, string HeaderString, ReportViewer RptViewer, bool Porttrait, bool Landscape, bool PrintDirectly, string PrntrName)
        {
            ReportDataSource RptDS1 = new ReportDataSource();
            ReportDataSource RptDS2 = new ReportDataSource();
            ReportDataSource RptDS3 = new ReportDataSource();
            ReportDataSource RptDS4 = new ReportDataSource();
            ReportParameter RptPar = new ReportParameter("RptHeader", HeaderString);

            RptDS1.Name = DS1;
            RptDS1.Value = DT1;
            RptViewer.LocalReport.DataSources.Add(RptDS1);

            RptDS2.Name = DS2;
            RptDS2.Value = DT2;
            RptViewer.LocalReport.DataSources.Add(RptDS2);

            RptDS3.Name = DSCnAddress;
            RptDS3.Value = DTCnAddress;
            RptViewer.LocalReport.DataSources.Add(RptDS3);

            RptDS4.Name = DSGenAddress;
            RptDS4.Value = DTGenAddress;
            RptViewer.LocalReport.DataSources.Add(RptDS4);

            RptViewer.LocalReport.ReportEmbeddedResource = "SkynetCashSales.Reports." + RptFile;
            if (HeaderString != "") RptViewer.LocalReport.SetParameters(new ReportParameter[] { RptPar });
            
            if (PrintDirectly)
            {
                Export(RptViewer.LocalReport);
                Print(PrntrName);
            }
            else
            {
                RptViewer.Refresh();
            }
        }

        private int _WHeight, _WWidth;
        public int WHeight { get { return _WHeight; } set { _WHeight = value; OnPropertyChanged("WHeight"); } }
        public int WWidth { get { return _WWidth; } set { _WWidth = value; OnPropertyChanged("WWidth"); } }
        
        private int m_currentPageIndex;
        private IList<Stream> m_streams;

        // Routine to provide to the report renderer, in order to
        //    save an image for each page of the report.
        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.5in</PageWidth>
                <PageHeight>11in</PageHeight>
                <MarginTop>0.25in</MarginTop>
                <MarginLeft>0.25in</MarginLeft>
                <MarginRight>0.25in</MarginRight>
                <MarginBottom>0.25in</MarginBottom>
            </DeviceInfo>";

            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }

        private void Print(string PintrName)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print");
            PrintDocument printDoc = new PrintDocument();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer");
            }
            else
            {
                if (PintrName.Trim() != "") printDoc.PrinterSettings.PrinterName = PintrName;
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }

        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

    }
}
