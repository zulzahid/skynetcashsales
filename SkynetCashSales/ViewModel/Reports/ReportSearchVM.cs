﻿using SkynetCashSales.General;
using SkynetCashSales.View.Reports;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.ViewModel.Reports
{
    public class ReportSearchVM : AMGenFunction
    {
        private FrmReportSearch Wndow;
        public ReportSearchVM(FrmReportSearch frm, string tabname)
        {
            Wndow = frm; ClearAll();
            CashSales = true;

            DateTime fdate = DateTime.Now.Date;
            DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

            SqlDataReader dr = GetDataReader($"SELECT a.CreatedDate FROM tblDailyLog a where a.LogTitle = 'Closed' and a.CreatedDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CreatedDate <= '{tdate.ToString(sqlDateLongFormat)}'");
            ClosedTitle = dr.Read() ? "Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToLongTimeString() : "Close Today Sales Now"; dr.Close();
        }

        public ReportSearchVM(FrmReportSearch frm)
        {
            Wndow = frm; ClearAll();
            //EntityCon();
            CashSales = true;
            getTaxDetail();

            DateTime fdate = DateTime.Now.Date;
            DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

            SqlDataReader dr = GetDataReader($"SELECT a.CreatedDate FROM tblDailyLog a where a.LogTitle = 'Closed' and a.CreatedDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CreatedDate <= '{tdate.ToString(sqlDateLongFormat)}'");
            ClosedTitle = dr.Read() ? "Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToLongTimeString() : "Close Today Sales Now"; dr.Close();
        }
        private string _ClosedTitle;
        private DateTime ClosedDateTime;
        public string ClosedTitle { get { return _ClosedTitle; } set { _ClosedTitle = value; OnPropertyChanged("ClosedTitle"); } }

        //CashSales Summary Customer User
        private bool _CashSales, _Summary, _Customer, _User, _TaxInvoice;
        public bool CashSales { get { return _CashSales; } set { _CashSales = value; OnPropertyChanged("CashSales"); } }
        public bool Summary { get { return _Summary; } set { _Summary = value; OnPropertyChanged("Summary"); } }
        public bool Customer { get { return _Customer; } set { _Customer = value; OnPropertyChanged("Customer"); } }
        public bool User { get { return _User; } set { _User = value; OnPropertyChanged("User"); } }
        public bool TaxInvoice { get { return _TaxInvoice; } set { _TaxInvoice = value; OnPropertyChanged("TaxInvoice"); } }

        private string _FromDate, _ToDate;
        public string FromDate { get { return _FromDate; } set { _FromDate = value; OnPropertyChanged("FromDate"); } }
        public string ToDate { get { return _ToDate; } set { _ToDate = value; OnPropertyChanged("ToDate"); } }

        private string _FromCSNoCode, _ToCSNoCode, _FromCSNoName, _ToCSNoName;
        public string FromCSNoCode { get { return _FromCSNoCode; } set { _FromCSNoCode = value; OnPropertyChanged("FromCSNoCode"); } }
        public string ToCSNoCode { get { return _ToCSNoCode; } set { _ToCSNoCode = value; OnPropertyChanged("ToCSNoCode"); } }
        public string FromCSNoName { get { return _FromCSNoName; } set { _FromCSNoName = value; OnPropertyChanged("FromCSNoName"); } }
        public string ToCSNoName { get { return _ToCSNoName; } set { _ToCSNoName = value; OnPropertyChanged("ToCSNoName"); } }

        private string _FromCustCode, _ToCustCode, _FromCustName, _ToCustName;
        public string FromCustCode { get { return _FromCustCode; } set { _FromCustCode = value; OnPropertyChanged("FromCustCode"); } }
        public string ToCustCode { get { return _ToCustCode; } set { _ToCustCode = value; OnPropertyChanged("ToCustCode"); } }
        public string FromCustName { get { return _FromCustName; } set { _FromCustName = value; OnPropertyChanged("FromCustName"); } }
        public string ToCustName { get { return _ToCustName; } set { _ToCustName = value; OnPropertyChanged("ToCustName"); } }

        private int _UserID;
        public int UserID { get { return _UserID; } set { _UserID = value; OnPropertyChanged("UserID"); } }
        private string _UserCode, _UserName;
        public string UserCode { get { return _UserCode; } set { _UserCode = value; OnPropertyChanged("UserCode"); } }
        public string UserName { get { return _UserName; } set { _UserName = value; OnPropertyChanged("UserName"); } }

        private string _CSNo, _CSDate, _CustCode, _CustName, _CustGSTNo, _Address1, _Address2, _City, _State, _Country, _PostalCode, _Mobile, _Email;
        public string CSNo { get { return _CSNo; } set { _CSNo = value; OnPropertyChanged("CSNo"); } }
        public string CSDate { get { return _CSDate; } set { _CSDate = value; OnPropertyChanged("CSDate"); } }
        public string CustCode { get { return _CustCode; } set { _CustCode = value; OnPropertyChanged("CustCode"); } }
        public string CustName { get { return _CustName; } set { _CustName = value; OnPropertyChanged("CustName"); } }
        public string CustGSTNo { get { return _CustGSTNo; } set { _CustGSTNo = value; OnPropertyChanged("CustGSTNo"); } }
        public string Address1 { get { return _Address1; } set { _Address1 = value; OnPropertyChanged("Address1"); } }
        public string Address2 { get { return _Address2; } set { _Address2 = value; OnPropertyChanged("Address2"); } }
        public string City { get { return _City; } set { _City = value; OnPropertyChanged("City"); } }
        public string State { get { return _State; } set { _State = value; OnPropertyChanged("State"); } }
        public string Country { get { return _Country; } set { _Country = value; OnPropertyChanged("Country"); } }
        public string PostalCode { get { return _PostalCode; } set { _PostalCode = value; OnPropertyChanged("PostalCode"); } }
        public string Mobile { get { return _Mobile; } set { _Mobile = value; OnPropertyChanged("Mobile"); } }
        public string Email { get { return _Email; } set { _Email = value; OnPropertyChanged("Email"); } }

        public string AWBNumber { get { return GetValue(() => AWBNumber); } set { SetValue(() => AWBNumber, value); OnPropertyChanged("AWBNumber"); } }

        private string _StnryBatchNo, _StnryBatchDate, _StnryCustCode, _StnryCustName, _StnryCustGSTNo, _StnryAddress1, _StnryAddress2, _StnryCity, _StnryState, _StnryCountry, _StnryPostalCode, _StnryMobile, _StnryEmail;
        public string StnryBatchNo { get { return _StnryBatchNo; } set { _StnryBatchNo = value; OnPropertyChanged("StnryBatchNo"); } }
        public string StnryBatchDate { get { return _StnryBatchDate; } set { _StnryBatchDate = value; OnPropertyChanged("StnryBatchDate"); } }
        public string StnryCustCode { get { return _StnryCustCode; } set { _StnryCustCode = value; OnPropertyChanged("StnryCustCode"); } }
        public string StnryCustName { get { return _StnryCustName; } set { _StnryCustName = value; OnPropertyChanged("StnryCustName"); } }
        public string StnryCustGSTNo { get { return _StnryCustGSTNo; } set { _StnryCustGSTNo = value; OnPropertyChanged("StnryCustGSTNo"); } }
        public string StnryAddress1 { get { return _StnryAddress1; } set { _StnryAddress1 = value; OnPropertyChanged("StnryAddress1"); } }
        public string StnryAddress2 { get { return _StnryAddress2; } set { _StnryAddress2 = value; OnPropertyChanged("StnryAddress2"); } }
        public string StnryCity { get { return _StnryCity; } set { _StnryCity = value; OnPropertyChanged("StnryCity"); } }
        public string StnryState { get { return _StnryState; } set { _StnryState = value; OnPropertyChanged("StnryState"); } }
        public string StnryCountry { get { return _StnryCountry; } set { _StnryCountry = value; OnPropertyChanged("StnryCountry"); } }
        public string StnryPostalCode { get { return _StnryPostalCode; } set { _StnryPostalCode = value; OnPropertyChanged("StnryPostalCode"); } }
        public string StnryMobile { get { return _StnryMobile; } set { _StnryMobile = value; OnPropertyChanged("StnryMobile"); } }
        public string StnryEmail { get { return _StnryEmail; } set { _StnryEmail = value; OnPropertyChanged("StnryEmail"); } }

        private bool _AllCSNoChecked, _FromCSNoChecked;
        public bool AllCSNoChecked { get { return _AllCSNoChecked; } set { _AllCSNoChecked = value; OnPropertyChanged("AllCSNoChecked"); } }
        public bool FromCSNoChecked { get { return _FromCSNoChecked; } set { _FromCSNoChecked = value; OnPropertyChanged("FromCSNoChecked"); } }

        private bool _CSChecked, _StnryChecked, _CSStnryChecked, _CSStnryBeforeChecked, _CSStnryAfterChecked;
        public bool CSChecked { get { return _CSChecked; } set { _CSChecked = value; OnPropertyChanged("CSChecked"); } }
        public bool StnryChecked { get { return _StnryChecked; } set { _StnryChecked = value; OnPropertyChanged("StnryChecked"); } }
        public bool CSStnryChecked { get { return _CSStnryChecked; } set { _CSStnryChecked = value; OnPropertyChanged("CSStnryChecked"); } }
        public bool CSStnryBeforeChecked { get { return _CSStnryBeforeChecked; } set { _CSStnryBeforeChecked = value; OnPropertyChanged("CSStnryBeforeChecked"); } }
        public bool CSStnryAfterChecked { get { return _CSStnryAfterChecked; } set { _CSStnryAfterChecked = value; OnPropertyChanged("CSStnryAfterChecked"); } }

        private ICommand _CommandGen;
        public ICommand CommandGen { get { if (_CommandGen == null) _CommandGen = new RelayCommand(Parameter => ExecuteCommandGen(Parameter)); return _CommandGen; } }

        public void ExecuteCommandGen(object ObjCommand)
        {
            DataTable dt = new DataTable();

            try
            {
                string CaseName = "", ControlName = "";
                string[] StrParameter = ObjCommand.ToString().Split('-');
                if (StrParameter.Length > 1) { CaseName = StrParameter[0]; ControlName = StrParameter[1]; } else { CaseName = StrParameter[0]; }
                var ChildWnd = new ListHelpGen();
                switch (CaseName)
                {
                    case "CSNo":
                        {
                            //dt = GetDataTable($"SELECT ColOneId = a.CSalesID, ColOneText = a.CSalesNo, ColTwoText = a.CSalesDate, ColThreeText = b.CNNum, ColFourText = b.ConsignorCode FROM tblCashSales a" +
                            //    $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            //    $" where a.Status = 1 order by a.CSalesDate");
                            dt = GetDataTable($"SELECT distinct ColOneId = a.CSalesID, ColOneText = a.CSalesNo, ColTwoText = a.CSalesDate FROM tblCashSales a join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo where a.Status = 1 order by a.CSalesDate");
                            ChildWnd.FrmListCol2_Closed += (r =>
                            {
                                if (ControlName == "From") { FromCSNoCode = r.ColOneText; FromCSNoName = r.ColTwoText; }
                                else if (ControlName == "To") { ToCSNoCode = r.ColOneText; ToCSNoName = r.ColTwoText; }
                                else { CSNo = r.ColOneText; CSDate = r.ColTwoText; LoadConsignorbasedCSNo(CSNo); }
                            });
                            ChildWnd.TwoColWindShow(dt, "Cash Sales No. Selection", "Cash Sales No", "Cash Sales Date");
                            //ChildWnd.FourColWindShow(dt, "Cash Sales No. Selection", "Cash Sales No", "Cash Sales Date", "AWB Number", "Consignor");
                            //ChildWnd.ThreeColWindShow(dt, "Cash Sales No. Selection", "Cash Sales No", "Cash Sales Date", "AWB Number");
                        }
                        break;
                    case "StnryBatchNo":
                        {
                            dt = GetDataTable($"SELECT DISTINCT ColOneId = 1, ColOneText = a.CSalesNo, ColTwoText = b.CDate FROM tblCSStationary a " +
                                $" join tblCashSales b on a.CSalesNo = b.CSalesNo" +
                                $" where a.Status = 1 order by b.CDate");
                            ChildWnd.FrmListCol2_Closed += (r =>
                            {
                                StnryBatchNo = r.ColOneText; StnryBatchDate = r.ColTwoText; LoadConsignorbasedBatchNo(StnryBatchNo);
                            });
                            ChildWnd.TwoColWindShow(dt, "Stationay sales batch no. Selection", "Batch No", "Invoice Date");
                        }
                        break;
                    case "User":
                        {
                            dt = GetDataTable($"SELECT ColOneId = a.UserId, ColOneText = a.UserName, ColTwoText = a.Authentication FROM tblUserMaster a order by a.UserName");
                            ChildWnd.FrmListCol2_Closed += (r => { UserID = Convert.ToInt32(r.Id); UserCode = r.ColOneText; UserName = r.ColTwoText; });
                            ChildWnd.TwoColWindShow(dt, "User Selection", "User Name", "Authentication");
                        }
                        break;
                    case "Customer":
                        {
                            dt = GetDataTable($"SELECT distinct ColOneId = a.ConsignorID, ColOneText = a.ConsignorCode, ColTwoText = a.Name, ColThreeText = a.City, ColFourText = a.Mobile FROM tblConsignor a order by a.Name");
                            ChildWnd.FrmListCol4_Closed += (r =>
                            {
                                if (ControlName == "From") { FromCustCode = r.ColOneText; FromCustName = r.ColTwoText; }
                                else if (ControlName == "To") { ToCustCode = r.ColOneText; ToCustName = r.ColTwoText; }
                                else { CustCode = r.ColOneText; CustName = r.ColTwoText; LoadConsignorbasedCODE(CustCode); }
                            });
                            ChildWnd.FourColWindShow(dt, "Customer Selection", "Code", "Name", "City", "Mobile");
                        }
                        break;
                    case "Stationary Customer":
                        {
                            dt = GetDataTable($"SELECT distinct ColOneId = a.ConsignorID, ColOneText = a.ConsignorCode, ColTwoText = a.Name, ColThreeText = a.City, ColFourText = a.Mobile FROM tblConsignor a order by a.Name");
                            ChildWnd.FrmListCol4_Closed += (r =>
                            {
                                StnryCustCode = r.ColOneText; StnryCustName = r.ColTwoText; LoadConsignorbasedStnryCODE(StnryCustCode);
                            });
                            ChildWnd.FourColWindShow(dt, "Customer Selection", "Code", "Name", "City", "Mobile");
                        }
                        break;
                    case "SearchPreview": using (new WaitCursor()) { SearchPreview(ControlName); } break;
                    case "Clear": ClearData(ControlName); break;
                    case "Print": break;
                    case "Close":
                        {
                            CloseWind();
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.Message);
            }
        }
        private static bool canPreview = false;
        private static bool portrait = true, landscape = false;
        private static string RPTName = "", RPTHeaderName = "";
        private static string strDTMas = "", strDTTran = "", strDTHelp = "";
        private static DataTable DTCompany = new DataTable();
        private static DataTable DTMas = new DataTable();
        private static DataTable DTTran = new DataTable();
        private static DataTable DTHelp = new DataTable();

        private void SearchPreview(string Condition)
        {
            try
            {
                canPreview = false;
                RPTName = ""; RPTHeaderName = "";
                strDTMas = ""; strDTTran = ""; strDTHelp = "";
                DTCompany.Rows.Clear(); DTCompany.Columns.Clear(); DTCompany.Clear();
                DTMas.Rows.Clear(); DTMas.Columns.Clear(); DTMas.Clear();
                DTTran.Rows.Clear(); DTTran.Columns.Clear(); DTTran.Clear();
                DTHelp.Rows.Clear(); DTHelp.Columns.Clear(); DTHelp.Clear();

                switch (Condition)
                {
                    case "StockStatus": StockStatusFunc(); break;
                    case "CashSales": CashSalesRpt(); break;
                    case "Summary":
                        {
                            if (CSChecked == true)
                                CSalesSummaryRpt();
                            else if (StnryChecked == true)
                                StationaryRpt();
                            else if (CSStnryChecked == true)
                                CSalesStationaryDetailedRpt();
                            else if (CSStnryBeforeChecked == true)
                                CSalesStationaryBeforeDetailedRpt();
                            else if (CSStnryAfterChecked == true)
                                CSalesStationaryAfterDetailedRpt();
                            else
                                CSalesStationaryDetailedRpt();
                        }
                        break;
                    case "StationarySummary": StationaryRpt(); break;
                    case "CSalesSummary": CSalesSummaryRpt(); break;
                    case "CancelledCSales": CSalesCancelRpt(); break;
                    case "Customer": CustomerRpt(); break;
                    case "User": CSaleUserRpt(); break;
                    case "TaxInvoice": TaxInvoices(); break;
                    case "Stationary": StationaryRpt(); break;
                    case "StationaryInvoice": StationaryInvoiceRpt(); break;
                    case "LodgeInAWB": LodgeInAWB(); break;
                    case "SummaryClose": { canPreview = false; SummaryClose(); } break;
                    default: canPreview = false; break;
                }

                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
                if (canPreview == true)
                {
                    if (DTCompany.Rows.Count <= 0)
                    {
                        DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.Status FROM tblStationProfile a where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' and a.Status = 1 and a.ToDate is null");
                    }

                    FrmReportViewer RptViewerGen = new FrmReportViewer();
                    RptViewerGen.Owner = Application.Current.MainWindow;
                    ReportViewerVM VM = new ReportViewerVM("CompanyMas", DTCompany, strDTMas, DTMas, strDTTran, DTTran, strDTHelp, DTHelp, RPTName, RPTHeaderName, RptViewerGen.RptViewer, portrait, landscape);
                    RptViewerGen.DataContext = VM;
                    RptViewerGen.Show();
                }
                else if(Condition != "SummaryClose" && canPreview == false) { MessageBox.Show("No data to preview", "Information", MessageBoxButton.OK, MessageBoxImage.Information); }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search and Preview Function : " + Ex.ToString()); }
        }

        private void ClearData(string Condition)
        {
            try
            {
                string strEmpty = string.Empty;
                FromDate = ToDate = strEmpty;
                if (Condition == "CashSales")
                {
                    FromCSNoCode = ToCSNoCode = "";
                    FromCSNoName = ToCSNoName = "";
                    FromDate = ToDate = "";
                }
                else if (Condition == "CSalesSummary")
                {
                    FromDate = ToDate = "";
                }
                else if (Condition == "Summary")
                {
                    FromDate = ToDate = "";
                }
                else if (Condition == "StationarySummary")
                {
                    FromDate = ToDate = "";
                }
                else if (Condition == "CancelledCSales")
                {
                    FromDate = ToDate = "";
                }
                else if (Condition == "Customer")
                {
                    FromCustCode = ToCustCode = "";
                    FromCustName = ToCustName = "";
                    FromDate = ToDate = "";
                }
                else if (Condition == "User")
                {
                    UserID = 0;
                    UserCode = UserName = "";
                    FromDate = ToDate = "";
                }
                else if (Condition == "LodgeInAWB")
                {
                    FromDate = ToDate = "";
                }
                else if (Condition == "TaxInvoice")
                {
                    CSNo = CSDate = "";
                    CustCode = CustName = CustGSTNo = "";
                    Address1 = Address2 = City = State = Country = PostalCode = "";
                    Mobile = Email = ""; AWBNumber = "";
                }
                else if (Condition == "Stationary")
                {
                    StnryBatchNo = StnryBatchDate = "";
                    StnryCustCode = StnryCustName = StnryCustGSTNo = "";
                    StnryAddress1 = StnryAddress2 = StnryCity = StnryState = StnryCountry = StnryPostalCode = "";
                    StnryMobile = StnryEmail = "";
                }
                else
                {
                    canPreview = false;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Clear Data Function : " + Ex.ToString()); }
        }
        private void StockStatusFunc()
        {
            try
            {
                RPTName = "RPTStockStatus.rdlc";
                strDTMas = "StockStatus"; strDTTran = ""; strDTHelp = "";

                DTMas = GetDataTable($"SELECT ItemCode = c.ItemCode, ItemDesc = c.ItemDesc, ItemType = d.GDDesc, Unit1 = c.Unit1, Unit2 = c.Unit2, UnitFactor = c.UnitFactor, Opening = a.Opening, StockAdjustment = a.StockAdj, Purchase = a.Purchase, PurchaseRet = a.PurchaseRet, Issue = a.Issue, Sales = a.Sales, SalesRet = a.SalesRet, Short = a.Short, Excess = a.Excess, Closing = a.Closing, Unit1Cost = c.Unit1Cost, CompanyName = '{MyCompanyName}', StnCode = '{MyCompanyCode}', SubName = '{MyCompanySubName}' FROM tblItemMaster c" +
                    $" join tblStock a on c.ItemId = a.ItemId" +
                    $" join tblGeneralDet d on c.ItemType = d.GenDetId" +
                    $" where c.Status = 1 order by c.ItemDesc");
            }
            catch (Exception Ex) { error_log.errorlog("Report Search StockStatus Function : " + Ex.ToString()); }
        }

        private void LodgeInAWB()
        {
            try
            {
                RPTName = "RPTLodgeInAWB.rdlc";
                strDTMas = "LodgeInAWB"; strDTTran = ""; strDTHelp = "";
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Lodge in AWB list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    DTMas = GetDataTable($"SELECT a.LodgeinID, Date = a.Date, a.AWBNum, a.Weight, a.ShipmentType, a.NoofPcs, a.RecFromName, a.RecFromMobile, a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status, a.IsExported, a.ExportedDate, FromDate = '{fdate}', ToDate = '{tdate}', CurrentUser = '{LoginUserName}' FROM tblLodginAWB a" +
                        $" where a.Status = 1 and a.Date >= '{fdate.ToString(sqlDateLongFormat)}' and a.Date <= '{tdate.ToString(sqlDateLongFormat)}' order by a.Date desc");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");

                    RPTHeaderName = "Lodge in AWB list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Lodge in AWB list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    DTMas = GetDataTable($"SELECT a.LodgeinID, Date = a.Date, a.AWBNum, a.Weight, a.ShipmentType, a.NoofPcs, a.RecFromName, a.RecFromMobile, a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status, a.IsExported, a.ExportedDate, FromDate = fdate, ToDate = tdate, CurrentUser = LoginUserName FROM tblLodginAWB a" +
                        $" where a.Status = 1 and a.Date >= '{fdate.ToString(sqlDateLongFormat)}' and a.Date <= '{tdate.ToString(sqlDateLongFormat)}' order by a.Date desc");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(ToDate), tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Lodge in AWB list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                    
                    DTMas = GetDataTable($"SELECT a.LodgeinID, Date = a.Date, a.AWBNum, a.Weight, a.ShipmentType, a.NoofPcs, a.RecFromName, a.RecFromMobile, a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status, a.IsExported, a.ExportedDate, FromDate = fdate, ToDate = tdate, CurrentUser = LoginUserName FROM tblLodginAWB a" +
                        $" where a.Status = 1 and a.Date >= '{fdate.ToString(sqlDateLongFormat)}' and a.Date <= '{tdate.ToString(sqlDateLongFormat)}' order by a.Date desc");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");

                    RPTHeaderName = "Lodge in AWB list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else { RPTHeaderName = "   "; }
                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Lodge In AWB Function : " + Ex.ToString()); }
        }
        private void SummaryClose()
        {
            SqlDataReader dr;
            try
            {
                string pcname = AMGeneral.GetPCName();
                System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("Do you want to close sales now?", "Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo);
                if (dialogResult1 == System.Windows.Forms.DialogResult.Yes)
                {
                    dr = GetDataReader($"SELECT LogID, CreatedDate FROM tblDailyLog WHERE (CreatedDate BETWEEN '{DateTime.Now.Date.ToString(sqlDateLongFormat)}' AND '{DateTime.Now.Date.AddHours(23.99).ToString(sqlDateLongFormat)}') AND LogTitle = 'Closed'");
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ClosedTitle = "Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToString("dd-MM-yyyy hh:mm:ss tt");
                        MessageBox.Show("Sorry already Closed at " + Convert.ToDateTime(dr["CreatedDate"]).ToString("dd-MM-yyyy hh:mm:ss tt"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        dr.Close();
                    }
                    else
                    {                        
                        ExecuteQuery($"INSERT INTO tblDailyLog(StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported)" +
                            $" VALUES('{MyCompanyCode.Trim()}', 'Closed', '{("Sales closed at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt"))}', '{pcname}', '{LoginUserName}', '{LoginUserId}', datetime(CURRENT_TIMESTAMP, 'localtime'), 0, 0)");
                        
                        ClosedTitle = "Closed at " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");
                    }
                }
                else
                {
                    ExecuteQuery($"INSERT INTO tblDailyLog(StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported)" +
                        $" VALUES('{MyCompanyCode.Trim()}', 'ClickedCls', '{("Clicked to close at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt"))}', '{pcname}', '{LoginUserName}', '{LoginUserId}', datetime(CURRENT_TIMESTAMP, 'localtime'), 0, 0)");
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Close for the Day Function : " + Ex.ToString()); }
        }
        private void CashSalesRpt()
        {
            try
            {
                FromCSNoChecked = true;
                RPTName = "RPTCashSales.rdlc";
                strDTMas = "CashSales"; strDTTran = ""; strDTHelp = "";
                if (!string.IsNullOrEmpty(FromCSNoCode) && !string.IsNullOrEmpty(ToCSNoCode))//!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && 
                {
                    string between = "";
                    long fid = 0, tid = 0;

                    long frid = 0, trid = 0;
                    DateTime fdate = DateTime.Now.Date, tdate = DateTime.Now;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesID, CSalesDate FROM tblCashSales Where CSalesNo = '{FromCSNoCode}'");
                    if (dr.Read())
                    {
                        frid = Convert.ToInt64(dr["CSalesID"]);
                        fdate = Convert.ToDateTime(dr["CSalesDate"]);
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT CSalesID, CSalesDate FROM tblCashSales Where CSalesNo = '{ToCSNoCode}'");
                    if (dr.Read())
                    {
                        trid = Convert.ToInt64(dr["CSalesID"]);
                        tdate = Convert.ToDateTime(dr["CSalesDate"]);
                    }
                    dr.Close();

                    if (frid > trid) { fid = trid; tid = frid; between = ToCSNoCode + " to " + FromCSNoCode; }
                    else { fid = frid; tid = trid; between = FromCSNoCode + " to " + ToCSNoCode; }

                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen" +
                            $" join tblCashSales b on gen.CSalesNo = b.CSalesNo" +
                            $" where gen.Status = 1 and b.CSalesID >= {fid} and b.CSalesID <= {tid}");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen" +
                            $" join tblCashSales b on gen.CSalesNo = b.CSalesNo" +
                            $" where gen.Status = 1 and b.CSalesID >= {fid} and b.CSalesID <= {tid}");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesID >= {fid} and gen.CSalesID <= {tid}");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesID >= {fid} and a.CSalesID <= {tid} and a.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");
                        
                        RPTHeaderName = "Cash Sales list from " + between + " and Date from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                    }
                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (!string.IsNullOrEmpty(FromCSNoCode) && string.IsNullOrEmpty(ToCSNoCode))//!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) &&
                {
                    DateTime fdate = DateTime.Now.Date;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{FromCSNoCode}'");
                    if (dr.Read())
                    {
                        fdate = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    }
                    dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesNo = '{FromCSNoCode}' order by a.CSalesDate desc");
                        RPTHeaderName = "Cash Sales list of " + FromCSNoCode;
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromCSNoCode) && !string.IsNullOrEmpty(ToCSNoCode))//!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && 
                {
                    DateTime fdate = DateTime.Now.Date;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{ToCSNoCode}'");
                    if (dr.Read())
                    {
                        fdate = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    }
                    dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesNo = '{ToCSNoCode}' order by a.CSalesDate desc");

                        RPTHeaderName = "Cash Sales list of " + ToCSNoCode;
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCSNoCode) && string.IsNullOrEmpty(ToCSNoCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate), tdate = Convert.ToDateTime(ToDate);

                    DTMas = GetDataTable($"SELECT a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                        $" JOIN tblCSaleCNNum b ON a.CSalesNo = b.CSalesNo" +
                        $" WHERE (a.CSalesDate BETWEEN '{Convert.ToDateTime(FromDate).Date.ToString("yyyy-MM-dd HH:mm")}' AND '{Convert.ToDateTime(ToDate).Date.AddHours(23.9999).ToString(sqlDateLongFormat)}') AND a.Status = 1");

                    RPTHeaderName = "Cash Sales list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCSNoCode) && string.IsNullOrEmpty(ToCSNoCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate), tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen" +
                            $" join tblCashSales mas on gen.CSalesNo = mas.CSalesNo" +
                            $" where gen.Status = 1 and mas.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and mas.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen" +
                            $" join tblCashSales mas on gen.CSalesNo = mas.CSalesNo" +
                            $" where gen.Status = 1 and mas.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and mas.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and gen.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesDate = '{fdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

                        RPTHeaderName = "Cash Sales list on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCSNoCode) && string.IsNullOrEmpty(ToCSNoCode))
                {
                    DateTime fdate = Convert.ToDateTime(ToDate), tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen" +
                            $" join tblCashSales mas on gen.CSalesNo = mas.CSalesNo" +
                            $" where gen.Status = 1 and mas.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and mas.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen" +
                            $" join tblCashSales mas on gen.CSalesNo = mas.CSalesNo" +
                            $" where gen.Status = 1 and mas.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and mas.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and gen.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesDate = '{fdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

                        RPTHeaderName = "Cash Sales list on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(FromCSNoCode) && string.IsNullOrEmpty(ToCSNoCode))
                {
                    DateTime fdate = DateTime.Now.Date;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{FromCSNoCode}'");
                    if (dr.Read())
                    {
                        fdate = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    }
                    dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesNo = '{FromCSNoCode}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesNo = '{FromCSNoCode}' order by a.CSalesDate desc");

                        RPTHeaderName = "Cash Sales list of " + FromCSNoCode;
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCSNoCode) && !string.IsNullOrEmpty(ToCSNoCode))
                {
                    DateTime fdate = DateTime.Now.Date;
                    SqlDataReader dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{ToCSNoCode}'");
                    if (dr.Read())
                    {
                        fdate = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    }
                    dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1 and gen.CSalesNo = '{ToCSNoCode}'");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, a.DestCode, a.Pieces, a.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 and a.CSalesNo = '{ToCSNoCode}' order by a.CSalesDate desc");

                        RPTHeaderName = "Cash Sales list of " + ToCSNoCode;
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else
                {
                    if (FromCSNoChecked == true)
                    {
                        RPTName = "RPTCashSalesDet.rdlc";
                        strDTMas = "CashSalesDet"; strDTTran = "CashSaleAWBs"; strDTHelp = "CashSaleStationary";

                        DTTran = GetDataTable($"SELECT gen.* FROM tblCSaleCNNum gen where gen.Status = 1");

                        DTHelp = GetDataTable($"SELECT gen.* FROM tblCSStationary gen where gen.Status = 1");

                        DTMas = GetDataTable($"SELECT b.CNNum, gen.CSalesNo, gen.CSalesDate, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight, gen.Price, gen.ShipmentGSTValue, gen.ShipmentTotal, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, b.InsShipmentDesc, gen.InsuranceTotal, gen.InsGSTValue, gen.CDate, gen.CSalesTotal, gen.StaffCode, gen.StaffName, StaffDisAmt = gen.StaffDisAmt, gen.IsManual, gen.OtherCharges, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount FROM tblCashSales gen" +
                            $" join tblCSaleCNNum b on gen.CSalesNo = b.CSalesNo" +
                            $" join tblUserMaster u on gen.CUserID = u.UserId" +
                            $" where gen.Status = 1");
                    }
                    else
                    {
                        DTMas = GetDataTable($"SELECT distinct a.CSalesNo, a.CSalesDate, a.CSalesTotal, b.DestCode, b.Pieces, b.Weight, a.IsManual, a.OtherCharges, b.CNNum, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                            $" join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                            $" where a.Status = 1 order by a.CSalesDate desc");

                        RPTHeaderName = "All Cash Sales list";
                    }

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1 and a.ToDate IS NULL))");
                }
                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales Function : " + Ex.ToString()); }
        }
        private void CSalesSummaryRpt()
        {
            try
            {
                RPTName = "RPTCSSummary.rdlc";
                strDTMas = "CSSummary"; strDTTran = ""; strDTHelp = "";
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Cash Sales Summary from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                    tdate = tdate.Date.AddDays(1);

                    DTMas = GetDataTable($"Select b.CSCNNumID as ID, a.CSalesDate as PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], Count(b.CNNum) as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount FROM tblCashSales a" +
                        " join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                        " join tblCSPrintDate c on b.CNNum = c.AWBNum" +
                        " join tblDestination d on trim(a.DestCode) = trim(d.DestCode)" +
                        " WHERE a.Status = '1' and CAST(strftime('%s', c.CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', c.CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) GROUP BY b.CSCNNumID, a.CSalesDate, b.CNNum, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VolWeight, a.ShipmentGSTValue, a.Price, a.ShipmentTotal, a.StationaryTotal, a.InsuranceTotal, a.CSalesTotal, a.StaffCode, a.StaffName, a.StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount UNION Select LogID as ID, CreatedDate as PrintDate, 'SalesClosed' as AWBNo, '' as ShipmentType, '' as DestCode, 0 as Pieces, 0 as Weight, 0 as VolWeight, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount FROM tblDailyLog WHERE IsDeleted = '0' AND LogTitle = 'Closed' and CAST(strftime('%s', CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) Order By PrintDate");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date;
                    RPTHeaderName = "Cash Sales Summary on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
                    DateTime tdate = fdate.Date.AddDays(1);

                    DTMas = GetDataTable($"Select b.CSCNNumID as ID, a.CSalesDate PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], Count(b.CNNum) as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount  FROM tblCashSales a" +
                        " join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                        " join tblCSPrintDate c on b.CNNum = c.AWBNum" +
                        " join tblDestination d on trim(a.DestCode) = trim(d.DestCode)" +
                        " WHERE a.Status = '1' and CAST(strftime('%s', c.CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', c.CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) GROUP BY b.CSCNNumID, a.CSalesDate, b.CNNum, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VolWeight, a.ShipmentGSTValue, a.Price, a.ShipmentTotal, a.StationaryTotal, a.InsuranceTotal, a.CSalesTotal, a.StaffCode, a.StaffName, a.StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount UNION Select LogID as ID, CreatedDate as PrintDate, 'SalesClosed' as AWBNo, '' as ShipmentType, '' as DestCode, 0 as Pieces, 0 as Weight, 0 as VolWeight, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount FROM tblDailyLog WHERE IsDeleted = '0' AND LogTitle = 'Closed' and CAST(strftime('%s', CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) Order By PrintDate");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(ToDate).Date;
                    RPTHeaderName = "Cash Sales Summary on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
                    DateTime tdate = fdate.Date.AddDays(1); ;

                    DTMas = GetDataTable($"Select b.CSCNNumID as ID, a.CSalesDate PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], Count(b.CNNum) as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount  FROM tblCashSales a" +
                        " join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                        " join tblCSPrintDate c on b.CNNum = c.AWBNum" +
                        " join tblDestination d on trim(a.DestCode) = trim(d.DestCode)" +
                        " WHERE a.Status = '1' and CAST(strftime('%s', c.CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', c.CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) GROUP BY b.CSCNNumID, a.CSalesDate, b.CNNum, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VolWeight, a.ShipmentGSTValue, a.Price, a.ShipmentTotal, a.StationaryTotal, a.InsuranceTotal, a.CSalesTotal, a.StaffCode, a.StaffName, a.StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount  UNION Select LogID as ID, CreatedDate as PrintDate, 'SalesClosed' as AWBNo, '' as ShipmentType, '' as DestCode, 0 as Pieces, 0 as Weight, 0 as VolWeight, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount FROM tblDailyLog WHERE IsDeleted = '0' AND LogTitle = 'Closed' and CAST(strftime('%s', CreatedDate)  AS  integer) >=CAST(strftime('%s', '" + fdate.Year.ToString() + "-" + fdate.Month.ToString("00") + "-" + fdate.Day.ToString("00") + "')  AS  integer) and CAST(strftime('%s', CreatedDate)  AS  integer) < CAST(strftime('%s', '" + tdate.Year.ToString() + "-" + tdate.Month.ToString("00") + "-" + tdate.Day.ToString("00") + "')  AS  integer) Order By PrintDate");

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else
                {
                    DTMas = GetDataTable($"Select b.CSCNNumID as ID, a.CSalesDate PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], Count(b.CNNum) as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount  FROM tblCashSales a" +
                        " join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                        " join tblCSPrintDate c on b.CNNum = c.AWBNum" +
                        " join tblDestination d on trim(a.DestCode) = trim(d.DestCode)" +
                        " WHERE a.Status = '1' GROUP BY b.CSCNNumID, a.CSalesDate, b.CNNum, b.ShipmentType, b.DestCode, b.Pieces, b.Weight, b.VolWeight, a.ShipmentGSTValue, a.Price, a.ShipmentTotal, a.StationaryTotal, a.InsuranceTotal, a.CSalesTotal, a.StaffCode, a.StaffName, a.StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount  UNION Select LogID as ID, CreatedDate as PrintDate, 'SalesClosed' as AWBNo, '' as ShipmentType, '' as DestCode, 0 as Pieces, 0 as Weight, 0 as VolWeight, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST,  '" + MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : "") + "' as Station, '" + LoginUserName + "' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount FROM tblDailyLog WHERE IsDeleted = '0' AND LogTitle = 'Closed' Order By PrintDate");
                    
                    RPTHeaderName = "Cash Sales Summary";

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND a.ToDate IS NULL))");
                }
                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales Summary Function : " + Ex.ToString()); }
        }
        private void CSalesCancelRpt()
        {
            try
            {
                RPTName = "RPTCSCancel.rdlc";
                strDTMas = "CSCancel"; strDTTran = ""; strDTHelp = "";
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT distinct a.CSalesID, a.AWBNum, a.DestCode, a.ShipmentType, a.Pieces, a.Weight, a.CSSaleTotal, a.CancelReason, b.CSalesDate, CancelledBy = a.CUserName, Station = MyCompanyCode.Trim() FROM tblCancelCashSale a" +
                        $" join tblCashSales b on a.CSalesID = b.CSalesID" +
                        $" where a.Status = 1 and b.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and b.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate");

                    RPTHeaderName = "Cancelled Cash Sales from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date;
                    DateTime tdate = fdate.Date.AddHours(23).AddMinutes(59).AddSeconds(59); ;

                    DTMas = GetDataTable($"SELECT distinct a.CSalesID, a.AWBNum, a.DestCode, a.ShipmentType, a.Pieces, a.Weight, a.CSSaleTotal, a.CancelReason, b.CSalesDate, CancelledBy = a.CUserName, Station = MyCompanyCode.Trim() FROM tblCancelCashSale a" +
                        $" join tblCashSales b on gen.CSalesNo = b.CSalesNo" +
                        $" where a.Status = 1 and b.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and b.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate");

                    RPTHeaderName = "Cancelled Cash Sales on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date;
                    DateTime tdate = fdate.Date.AddHours(23).AddMinutes(59).AddSeconds(59); ;
                    
                    DTMas = GetDataTable($"SELECT distinct a.CSalesID, a.AWBNum, a.DestCode, a.ShipmentType, a.Pieces, a.Weight, a.CSSaleTotal, a.CancelReason, b.CSalesDate, CancelledBy = a.CUserName, Station = MyCompanyCode.Trim() FROM tblCancelCashSale a" +
                        $" join tblCashSales b on gen.CSalesNo = b.CSalesNo" +
                        $" where a.Status = 1 and b.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and b.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate");

                    RPTHeaderName = "Cancelled Cash Sales on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else
                {
                    DTMas = GetDataTable($"SELECT distinct a.CSalesID, a.AWBNum, a.DestCode, a.ShipmentType, a.Pieces, a.Weight, a.CSSaleTotal, a.CancelReason, b.CSalesDate, CancelledBy = a.CUserName, Station = MyCompanyCode.Trim() FROM tblCancelCashSale a" +
                        $" join tblCashSales b on gen.CSalesNo = b.CSalesNo" +
                        $" where a.Status = 1 order by a.CSalesDate");

                    RPTHeaderName = "Cancelled Cash Sales ";

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND a.ToDate IS NULL");
                }
                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales Summary Function : " + Ex.ToString()); }
        }
        private void CustomerRpt()
        {
            try
            {
                RPTName = "RPTCustomers.rdlc";
                strDTMas = "Customer"; strDTTran = ""; strDTHelp = "";
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(FromCustCode) && !string.IsNullOrEmpty(ToCustCode))
                {
                    string between = "";
                    long fid = 0, tid = 0;

                    long frid = 0, trid = 0;
                    SqlDataReader dr = GetDataReader($"SELECT ConsignorID FROM tblConsignor Where ConsignorCode = '{FromCustCode}'");
                    if (dr.Read())
                    {
                        frid = Convert.ToInt64(dr["ConsignorID"]);
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT ConsignorID FROM tblConsignor Where ConsignorCode = '{ToCustCode}'");
                    if (dr.Read())
                    {
                        trid = Convert.ToInt64(dr["CSalesID"]);
                    }
                    dr.Close();

                    if (frid > trid) { fid = trid; tid = frid; between = ToCustCode + " to " + FromCustCode; }
                    else { fid = frid; tid = trid; between = FromCustCode + " to " + ToCustCode; }
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.ConsignorID >= {fid} and a.ConsignorID <= {tid} order by a.CDate desc");

                    RPTHeaderName = "Customers list from " + between;
                }
                else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(FromCustCode) && string.IsNullOrEmpty(ToCustCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate), tdate = Convert.ToDateTime(ToDate);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.ConsignorCode = '{FromCustCode}' and a.CDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CDate desc");

                    RPTHeaderName = "Customers list of " + FromCustCode;
                }
                else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCustCode) && !string.IsNullOrEmpty(ToCustCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate), tdate = Convert.ToDateTime(ToDate);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.ConsignorCode = '{ToCustCode}' and a.CDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CDate desc");
                    
                    RPTHeaderName = "Customers list of " + ToCustCode;
                }
                else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCustCode) && string.IsNullOrEmpty(ToCustCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate), tdate = Convert.ToDateTime(ToDate);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.CDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CDate desc");

                    RPTHeaderName = "Customers list from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCustCode) && string.IsNullOrEmpty(ToCustCode))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate);
                    DateTime ffdate = Convert.ToDateTime(FromDate).Date;
                    DateTime ttdate = ffdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.CDate = '{fdate.ToString(sqlDateLongFormat)}' order by a.CDate desc");

                    RPTHeaderName = "Customers list on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCustCode) && string.IsNullOrEmpty(ToCustCode))
                {
                    DateTime ffdate = Convert.ToDateTime(ToDate).Date;
                    DateTime ttdate = ffdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    DateTime tdate = Convert.ToDateTime(ToDate);

                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.CDate = '{tdate.ToString(sqlDateLongFormat)}' order by a.CDate desc");

                    RPTHeaderName = "Customers list on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(FromCustCode) && string.IsNullOrEmpty(ToCustCode))
                {
                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.ConsignorCode = '{FromCustCode}' order by a.CDate desc");

                    RPTHeaderName = "Customers Details";
                }
                else if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(FromCustCode) && !string.IsNullOrEmpty(ToCustCode))
                {
                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 and a.ConsignorCode = '{ToCustCode}' order by a.CDate desc");

                    RPTHeaderName = "Customers Details";
                }
                else
                {
                    DTMas = GetDataTable($"SELECT distinct a.ConsignorCode, a.Name, Address = a.Add1 + a.Add2, a.City, a.State, a.Mobile, a.Phone, a.Fax, a.Email FROM tblConsignor a" +
                        $" where a.Status = 1 order by a.CDate desc");

                    RPTHeaderName = "All Customers List";
                }
                if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Customer Function : " + Ex.ToString()); }
        }

        private void CSaleUserRpt()
        {
            //try
            //{
            //    RPTName = "RPTCSaleUser.rdlc";
            //    strDTMas = "CSaleUser"; strDTTran = ""; strDTHelp = "";
            //    if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(UserCode))
            //    {
            //        long frid = UserID;
            //        DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and b.UserId = {frid} and a.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales created by " + UserName + " from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
            //    }
            //    else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(UserCode))
            //    {
            //        long frid = UserID;
            //        DateTime fdate = Convert.ToDateTime(FromDate).Date;

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and b.UserId = {frid} and a.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales created by " + UserName + " on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
            //    }
            //    else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(UserCode))
            //    {
            //        long frid = UserID;
            //        DateTime tdate = Convert.ToDateTime(ToDate).Date.AddHours(23.9999);

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and b.UserId = {frid} and a.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales created by " + UserName + " on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
            //    }
            //    else if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && !string.IsNullOrEmpty(UserCode))
            //    {
            //        long frid = UserID;

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and b.UserId = {frid} order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales created by " + UserName;
            //    }
            //    else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(UserCode))
            //    {
            //        DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23.9999);

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and a.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' and a.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + ToDate;
            //    }
            //    else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(UserCode))
            //    {
            //        DateTime fdate = Convert.ToDateTime(FromDate).Date;

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and a.CSalesDate >= '{fdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
            //    }
            //    else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate) && string.IsNullOrEmpty(UserCode))
            //    {
            //        DateTime tdate = Convert.ToDateTime(ToDate).Date.AddHours(23.9999);

            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 and a.CSalesDate <= '{tdate.ToString(sqlDateLongFormat)}' order by a.CSalesDate desc");

            //        RPTHeaderName = "Cash Sales on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
            //    }
            //    else
            //    {
            //        DTMas = GetDataTable($"SELECT distinct b.UserName, a.CSalesNo, a.CSalesDate, a.CSalesTotal, DestCode = ab.DestCode, ab.Pieces, ab.Weight, a.Price, Stationary = a.StationaryTotal, Insurance = a.InsuranceTotal, CSTotal = a.CSalesTotal, CSAWBNum = ab.CNNum, ShipmentType = ab.ShipmentType, a.OtherCharges, a.IsManual FROM tblCashSales a" +
            //            $" join tblCSaleCNNum ab on a.CSalesNo = ab.CSalesNo" +
            //            $" left join tblUserMaster b on a.CUserID = b.UserId" +
            //            $" where a.Status = 1 order by a.CSalesDate desc");

            //        RPTHeaderName = "All Cash Sales list ";
            //    }
            //    if (DTMas.Rows.Count > 0) { canPreview = true; } else { canPreview = false; }
            //}
            //catch (Exception Ex) { error_log.errorlog("Report Search Csales Report Function : " + Ex.ToString()); }

            //sini
            try
            {
                RPTName = "RPTCSSummary.rdlc";
                strDTMas = "CSSummary"; strDTTran = ""; strDTHelp = "";
                DTMas = new DataTable();
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    long frid = UserID;
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    //RPTHeaderName = "Cash Sales & Stationery Summary from " + fdate.ToString("dd/MM/yyyy") + " to " + tdate.ToString("dd/MM/yyyy") + " (Overall)";  
                    RPTHeaderName = "Cash Sales created by " + UserCode + " from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();

                    string strCSStnryquery = $"Select distinct '{RPTHeaderName}' As Header, 0 as ID, a.CSalesNo, a.CSalesDate as PrintDate, '' as AWBNo, '' as ShipmentType, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and a.CSalesNo LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country " +
                        $"FROM tblCashSales a left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        $"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        $"left join tblDestination d on b.destcode = d.DestCode " +
                        $"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $"left join tblUserMaster f on a.CUserID = f.UserId " +
                        $"WHERE f.UserId = {frid} and (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.ClosedSalesTime IS NOT NULL AND a.Status = 1 UNION " +
                        $"Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesNo, a.CSalesDate as PrintDate, iif(b.shipmenttype = 'STATIONERY', f.itemdesc, b.CNNum)  as AWBNo, b.ShipmentType as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], c.PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, b.DestCode as AWBDestCode, b.Pieces as AWBPieces, b.HighestWeight as AWBWeight, b.VolWeight as AWBVolWeight, IIF(b.ShipmentType = 'STATIONERY', b.Price, 0) as StationaryPrice, b.InsValue as AWBInsurans, IIF(b.ShipmentType != 'STATIONERY', b.Price, 0) as AWBPrice, b.OtherCharges as AWBOtherCharges, (b.TotalAmt + b.InsValue - b.StaffDiscAmt - b.PromoDiscAmt) as AWBTotalAmt, b.StaffDiscAmt as AWBStaffDiscAmt, b.PromoDiscAmt as AWBPromoDiscAmt, IIF(b.TaxCode = 'Surcharge Tax', b.destcode, 'MALAYSIA') as Country " +
                        $"FROM tblCashSales a left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        $"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        $"left join tblDestination d on b.destcode = d.DestCode " +
                        $"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $"LEFT JOIN tblItemMaster f on b.CNNum = f.ItemCode " +
                        $"left join tblUserMaster g on a.CUserID = g.UserId " +
                        $"WHERE g.UserId = {frid} and (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.ClosedSalesTime IS NOT NULL AND a.Status = 1 UNION " +
                        $"Select '{RPTHeaderName}' As Header, 0 as ID, LogDescription AS CSalesNo, CreatedDate as PrintDate, LogTitle  as AWBNo, '' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0  as DiscPercentage, 0 as DiscAmount, 0 as RoundingAmt, '' as PaymentMode, '' as PaymentRefNo, 0 AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country  FROM tblDailyLog WHERE (CreatedDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed' Order By PrintDate";

                    DTMas = GetDataTable(strCSStnryquery);
                    DTMas.AsEnumerable().Where(rec => rec["AWBNo"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => { rec["AWBNo"] = "Stationery"; });
                    DTMas = DataTableSort(DTMas, "PrintDate", "ASC");
                    RPTHeaderName = "";
                    canPreview = DTMas.Rows.Count > 0 ? true : false;
                    //    }
                    //}
                }
                else
                {
                    MessageBox.Show("Please select the from and to-date", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales and Stationery Summary Function : " + Ex.ToString()); }

        }
        private static string errormsg = "";
        static readonly string[] ValidatedProperties = { "CSNo", "CustName", "CustGSTNo", "Address1", "Mobile" };
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null) // there is an error
                        return false;
                }
                return true;
            }
        }
        static readonly string[] ValidatedPropertiesStnry = { "StnryBatchNo", "StnryCustName", "StnryCustGSTNo", "StnryAddress1", "StnryMobile" };
        public bool IsValidStnry
        {
            get
            {
                foreach (string property in ValidatedPropertiesStnry)
                {
                    errormsg = GetValidationError(property);
                    if (errormsg != null) // there is an error
                        return false;
                }
                return true;
            }
        }
        private string GetValidationError(string propertyName)
        {
            string error = null;
            switch (propertyName)//
            {
                case "CSNo": if (string.IsNullOrEmpty(CSNo)) error = "Cash Sales number is required"; Wndow.txtCSNo.Focus(); break;
                case "CustName": if (string.IsNullOrEmpty(CustName)) error = "Customer name is required"; Wndow.txtCustName.Focus(); break;
                case "CustGSTNo": if (string.IsNullOrEmpty(CustGSTNo)) error = "Customer GST number is required"; Wndow.txtCustGST.Focus(); break;
                case "Address1": if (string.IsNullOrEmpty(Address1)) error = "Customer address is required"; Wndow.txtCustAdd1.Focus(); break;
                case "Mobile": if (string.IsNullOrEmpty(Mobile)) error = "Mobile is required"; Wndow.txtCustMobile.Focus(); break;
                case "StnryBatchNo": if (string.IsNullOrEmpty(StnryBatchNo)) error = "Cash Sales number is required"; Wndow.txtStnryCSNo.Focus(); break;
                case "StnryCustName": if (string.IsNullOrEmpty(StnryCustName)) error = "Customer name is required"; Wndow.txtStnryCustName.Focus(); break;
                case "StnryCustGSTNo": if (string.IsNullOrEmpty(StnryCustGSTNo)) error = "Customer GST number is required"; Wndow.txtStnryCustGST.Focus(); break;
                case "StnryAddress1": if (string.IsNullOrEmpty(StnryAddress1)) error = "Customer address is required"; Wndow.txtStnryCustAdd1.Focus(); break;
                case "StnryMobile": if (string.IsNullOrEmpty(StnryMobile)) error = "Mobile is required"; Wndow.txtStnryCustMobile.Focus(); break;
                default:
                    error = null;
                    throw new Exception("Unexpected property being validated on Service");
            }
            return error;
        }
        private void LoadConsignorbasedCSNo(string numcsales)
        {
            try
            {
                if (!string.IsNullOrEmpty(numcsales))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.ConsignorCode, a.GSTNum, a.Name, a.Add1, a.Add2, a.City, a.State, a.Country, a.PostalCode, a.Mobile, a.Email FROM tblConsignor a" +
                        $" join tblCSaleCNNum b on a.ConsignorCode = b.ConsignorCode" +
                        $" join tblCashSales c on b.CSalesNo = c.CSalesNo" +
                        $" where c.CSalesNo = '{numcsales}'");
                    if (dr.Read())
                    {
                        CustCode = dr["ConsignorCode"].ToString();
                        CustGSTNo = dr["GSTNum"].ToString();
                        CustName = dr["Name"].ToString();
                        Address1 = dr["Add1"].ToString();
                        Address2 = dr["Add2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        PostalCode = dr["PostalCode"].ToString();
                        Mobile = dr["Mobile"].ToString();
                        Email = dr["Email"].ToString();
                    }
                    dr.Close();
                }
                else { }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Consignor Function : " + Ex.ToString()); }
        }
        private void LoadConsignorbasedBatchNo(string numcsales)
        {
            try
            {
                if (!string.IsNullOrEmpty(numcsales))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.ConsignorCode, a.GSTNum, a.Name, a.Add1, a.Add2, a.City, a.State, a.Country, a.PostalCode, a.Mobile, a.Email FROM tblConsignor a" +
                        $" join tblStationaryInvoice b on a.ConsignorCode = b.ConsignorCode" +
                        $" where b.BatchNum = '{numcsales}'");

                    if (dr.Read())
                    {
                        CustCode = dr["ConsignorCode"].ToString();
                        CustGSTNo = dr["GSTNum"].ToString();
                        CustName = dr["Name"].ToString();
                        Address1 = dr["Add1"].ToString();
                        Address2 = dr["Add2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        PostalCode = dr["PostalCode"].ToString();
                        Mobile = dr["Mobile"].ToString();
                        Email = dr["Email"].ToString();
                    }
                    dr.Close();
                }
                else { }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Consignor Function : " + Ex.ToString()); }
        }
        private void LoadConsignorbasedCODE(string code)
        {
            try
            {
                if (!string.IsNullOrEmpty(code))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.ConsignorCode, a.GSTNum, a.Name, a.Add1, a.Add2, a.City, a.State, a.Country, a.PostalCode, a.Mobile, a.Email FROM tblConsignor a where a.ConsignorCode = '{code}'");
                    if (dr.Read())
                    {
                        CustCode = dr["ConsignorCode"].ToString();
                        CustGSTNo = dr["GSTNum"].ToString();
                        CustName = dr["Name"].ToString();
                        Address1 = dr["Add1"].ToString();
                        Address2 = dr["Add2"].ToString();
                        City = dr["City"].ToString();
                        State = dr["State"].ToString();
                        Country = dr["Country"].ToString();
                        PostalCode = dr["PostalCode"].ToString();
                        Mobile = dr["Mobile"].ToString();
                        Email = dr["Email"].ToString();
                    }
                    dr.Close();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Consignor Function : " + Ex.ToString()); }
        }
        private void LoadConsignorbasedStnryCODE(string code)
        {
            try
            {
                if (!string.IsNullOrEmpty(code))
                {
                    SqlDataReader dr = GetDataReader($"SELECT a.ConsignorCode, a.GSTNum, a.Name, a.Add1, a.Add2, a.City, a.State, a.Country, a.PostalCode, a.Mobile, a.Email FROM tblConsignor a where a.ConsignorCode = '{code}'");
                    if (dr.Read())
                    {
                        StnryCustCode = dr["ConsignorCode"].ToString();
                        StnryCustGSTNo = dr["GSTNum"].ToString();
                        StnryCustName = dr["Name"].ToString();
                        StnryAddress1 = dr["Add1"].ToString();
                        StnryAddress2 = dr["Add2"].ToString();
                        StnryCity = dr["City"].ToString();
                        StnryState = dr["State"].ToString();
                        StnryCountry = dr["Country"].ToString();
                        StnryPostalCode = dr["PostalCode"].ToString();
                        StnryMobile = dr["Mobile"].ToString();
                        StnryEmail = dr["Email"].ToString();
                    }
                    dr.Close();
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Stationery Invoice Consignor Function : " + Ex.ToString()); }
        }
        private void TaxInvoices()
        {
            SqlDataReader dr;
            try
            {
                if (!string.IsNullOrEmpty(CSNo))
                {
                    string CodeConsignor = "";
                    if (IsValid)
                    {
                        if (string.IsNullOrEmpty(CustCode))
                        {
                            dr = GetDataReader($"SELECT a.ConsignorCode FROM tblConsignor a where a.Name = '{CustName}' and a.Mobile = '{Mobile}'");
                            if (dr.Read())
                            {
                                CodeConsignor = dr["ConsignorCode"].ToString();
                                dr.Close();

                                ExecuteQuery($"UPDATE tblConsignor SET Add1 = '{Address1}', Add2 = '{Address2}', City = '{City}', State = '{State}', Country = '{Country}', PostalCode = '{PostalCode}', Email = '{Email}', GSTNum = '{CustGSTNo}' Where RTRIM((ConsignorCode) = '{CodeConsignor.Trim()}'");
                            }
                            else
                            {
                                dr = GetDataReader($"SELECT COUNT(ConsignorCode) AS CntConsignorCode FROM tblConsignor");
                                
                                int counts = dr.Read() ? Convert.ToInt32(dr["CntConsignorCode"]) : 0; dr.Close();

                                if (counts > 0)
                                {
                                    dr = GetDataReader($"SELECT MAX(ConsignorCode) AS MaxConsignorCode FROM tblConsignor");

                                    int nos = dr.Read() ? Convert.ToInt32(dr["MaxConsignorCode"]) : 0; dr.Close();

                                    CodeConsignor = (nos + 1).ToString("000000");
                                }
                                else
                                {
                                    CodeConsignor = "000001";
                                }

                                ExecuteQuery($"INSERT INTO tblConsignor (ConsignorCode, Name, GSTNum, Add1, Add2, City, State, Country, PostalCode, Mobile, Email, Phone, Fax, CUserID, CDate, Status)" +
                                    $" VALUES('{CodeConsignor}', '{CustName}', '{CustGSTNo}', '{Address1}', '{Address2}', '{City}', '{State}', '{Country}', '{PostalCode}', '{Mobile}', '{Email}', '', '', {LoginUserId}, GETDATE(), 1)");
                            }
                            dr.Close();
                        }
                        else
                        {
                            CodeConsignor = CustCode;

                            ExecuteQuery($"UPDATE tblConsignor SET Name = '{CustName}', Add1 = '{Address1}', Add2 = '{Address2}', City = '{City}', State = '{State}', Country = '{Country}', PostalCode = '{PostalCode}', Email = '{Email}', GSTNum = '{CustGSTNo}' Where RTRIM((ConsignorCode) = '{CodeConsignor.Trim()}'");
                        }

                        if (CodeConsignor.Trim() != "")
                        {
                            ExecuteQuery($"UPDATE tblCSaleCNNum SET ConsignorCode = '{CodeConsignor}' Where CSalesNo = '{CSNo}'");

                            DTTran = GetDataTable($"SELECT gen.* FROM tblConsignor gen WHERE gen.ConsignorCode = '{CodeConsignor}' AND gen.Status = 1");
                        }
                    }

                    decimal limitkg = Convert.ToDecimal(30.009);
                    RPTName = "RPTCSTaxInvoice.rdlc";
                    strDTMas = "CashSalesDetails"; strDTTran = "Consignor"; strDTHelp = "";

                    //DTMas = GetDataTable($"SELECT DISTINCT num.CNNum, gen.CSalesNo, gen.CSalesDate, num.ShipmentType, num.DestCode, num.Pieces, num.Weight, num.VLength, num.VBreadth, num.VHeight, num.VolWeight, gen.Price, gen.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, num.InsShipmentDesc, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, gen.StaffDisAmt, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount, gen.ShipmentTotal, gen.TaxTitle, gen.TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate FROM tblCashSales gen" +
                    //    $" JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo" +
                    //    $" WHERE gen.CSalesNo = '{CSNo}' AND gen.Status = 1");
                    
                    DTMas = GetDataTable($"SELECT iif(num.shipmenttype = 'STATIONERY', item.itemdesc, num.CNNum)  as CNNum, gen.CSalesNo, gen.CSalesDate, num.ShipmentType, num.DestCode, num.Pieces, num.Weight, num.VLength, num.VBreadth, num.VHeight, num.VolWeight, num.Price, num.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, num.InsShipmentDesc, num.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, num.StaffDiscAmt as 'StaffDisAmt', gen.DiscCode, gen.DiscPercentage, num.PromoDiscAmt as 'DiscAmount', gen.ShipmentTotal, num.TaxCode as TaxTitle, num.TaxRate as TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate, (num.TotalAmt + num.InsValue - num.StaffDiscAmt - num.PromoDiscAmt) as AmountperRow, ((num.TotalAmt + num.InsValue - num.StaffDiscAmt - num.PromoDiscAmt) * num.TaxRate / 100) as AmountperRowAfterTax " +
                        $"FROM tblCashSales gen LEFT JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo " +
                        $"LEFT JOIN tblItemMaster item on num.CNNum = item.ItemCode " +
                        $"WHERE num.CSalesNo = '{CSNo}' AND gen.Status = 1");

                    DTTran = GetDataTable($"Select A.CSalesNo, B.TaxCode, B.TotalAmt AS TotalAmt1, (B.TotalAmt * B.TaxRate / 100) AS TaxAmt1, C.TaxCode, C.TotalAmt AS TotalAmt2, (C.TotalAmt * C.TaxRate / 100) AS TaxAmt2, (A.ShipmentTOTAL + A.STATIONARYTOTAL + A.InsValue) as TotalBeforeTax, A.ShipmentGSTValue AS TotalTaxAmt, (A.DiscAmount + A.StaffDisAmt) as TotalDiscount, A.CSalesTotal from tblCashSales a LEFT JOIN(SELECT CSalesNo, TaxCode, TaxRate, SUM(TotalAmt + InsValue - StaffDiscAmt - PromoDiscAmt) AS TotalAmt FROM tblCSaleCNNum WHERE TAXCODE = '{ StandardTaxCode }' GROUP BY CSalesNo, TaxCode, TaxRate) B ON A.CSalesNo = B.CSalesNo LEFT JOIN(SELECT CSalesNo, TaxCode, TaxRate, SUM(TotalAmt + InsValue - StaffDiscAmt - PromoDiscAmt) AS TotalAmt FROM tblCSaleCNNum WHERE TAXCODE = '{ InternationalTaxCode }' GROUP BY CSalesNo, TaxCode, TaxRate) C ON A.CSalesNo = C.CSalesNo WHERE A.CSalesNo = '{CSNo}'");

                    strDTTran = "InvoiceSummary";

                    DateTime fdate = DateTime.Now.Date;
                    dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{CSNo}'");
                    if (dr.Read())
                    {
                        fdate = Convert.ToDateTime(dr["CSalesDate"]).Date;
                    }
                    dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    
                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");
                }
                else { MessageBox.Show(errormsg, "Warning"); }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Function : " + Ex.ToString()); }
        }

        private void StationaryRpt()
        {
            try
            {
                RPTName = "RPTStationary.rdlc";
                strDTMas = "Stationary"; strDTTran = ""; strDTHelp = "";
                //string TaxTitle = "SST";
                //decimal TaxPercentage = 6;


                //SqlDataReader dr = GetDataReader($"SELECT TaxTitle, TaxPercentage FROM tblQuotation WHERE QuotationType = 'Standard' AND IsDeleted = 0 AND ToDate IS NULL");
                //if(dr.Read())
                //{
                //    TaxTitle = dr["TaxTitle"].ToString().Trim();
                //    TaxPercentage = Convert.ToDecimal(dr["TaxPercentage"]);
                //}
                //dr.Close();

                DTMas = new DataTable();
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    //SqlDataReader dr2 = GetDataReader($"SELECT LogID, CreatedDate FROM tblDailyLog WHERE (CreatedDate BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{DateTime.Now.Date.AddHours(23.99).ToString("yyyy-MM-dd HH:mm:ss")}') AND LogTitle = 'Closed'");
                    //if (dr2.HasRows)
                    //{
                    //    if (dr2.Read())
                    //    {
                            DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                            //DTMas = GetDataTable($" select '{ fdate.ToString("dd/MM/yyyy") }' as 'FDate', '{tdate.ToString("dd/MM/yyyy")}' as 'TDate', b.CSStnryID as 'StnryID', c.DestCode as 'StnCode',a.CSalesNo as 'BatchNum',b.SlNo, b.ItemCode, " +
                            //    $" b.ItemName, b.Quantity, b.UOM, b.Rate, b.Amount, a.CUserID, a.CDate, a.MUserID, a.MDate, a.Status, b.IsExported, b.ExportedDate, a.PaymentMode, a.PaymentRefNo,'{TaxTitle}' As TaxTitle, {TaxPercentage} As TaxPercentage  from tblCashSales a" +
                            //    $" JOIN tblCSStationary b ON a.CSalesNo = b.CSalesNo" +
                            //    $" LEFT JOIN tblCSaleCNNum c ON a.CSalesNo = c.CSalesNo" +
                            //    $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND C.CSalesNo IS NULL AND a.Status = 1");

                            DTMas = GetDataTable($"select '{ fdate.ToString("dd/MM/yyyy") }' as 'FDate', '{tdate.ToString("dd/MM/yyyy")}' as 'TDate', 'TBD' as 'StnryID', b.DestCode as 'StnCode',a.CSalesNo as 'BatchNum', '' as SlNo, c.ItemCode as ItemCode, c.itemdesc as ItemName, b.Pieces as Quantity, c.Unit1 as UOM, b.Price as Rate, b.TotalAmt as Amount, a.CUserID, a.CDate, a.MUserID, a.MDate, a.Status, b.IsExported, b.ExportedDate, a.PaymentMode, a.PaymentRefNo, '{StandardTaxCode}' As TaxTitle, {StandardTaxRate} As TaxPercentage from tblCashSales a" +
                                $" LEFT JOIN tblCSaleCNNum b ON a.CSalesNo = b.CSalesNo " +
                                $" LEFT JOIN tblItemMaster c on b.CNNum = c.ItemCode " +
                                $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') and (b.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1 and b.ShipmentType = 'STATIONERY'");

                            RPTHeaderName = "";
                            DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                                $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND (a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}' OR a.ToDate IS NULL))");

                            DTCompany.AsEnumerable().Where(rec => rec["Address1"].ToString() != "").ToList().ForEach(rec => { rec["Address1"] = rec["Address1"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["Address2"].ToString() != "").ToList().ForEach(rec => { rec["Address2"] = rec["Address2"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["Address3"].ToString() != "").ToList().ForEach(rec => { rec["Address3"] = rec["Address3"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["City"].ToString() != "").ToList().ForEach(rec => { rec["City"] = rec["City"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["State"].ToString() != "").ToList().ForEach(rec => { rec["State"] = rec["State"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["Country"].ToString() != "").ToList().ForEach(rec => { rec["Country"] = rec["Country"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["PostalCode"].ToString() != "").ToList().ForEach(rec => { rec["PostalCode"] = rec["PostalCode"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["PhoneNo"].ToString() != "").ToList().ForEach(rec => { rec["PhoneNo"] = rec["PhoneNo"] + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["FaxNo"].ToString() != "").ToList().ForEach(rec => { rec["FaxNo"] = rec["FaxNo"].ToString().Trim() + ", "; });
                            DTCompany.AsEnumerable().Where(rec => rec["EMail"].ToString() != "").ToList().ForEach(rec => { rec["EMail"] = rec["EMail"].ToString().Trim(); });

                            canPreview = DTMas.Rows.Count > 0 ? true : false;
                    //    }
                    //}
                }
                else
                {
                    MessageBox.Show("Please select the from and to-date", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales Summary Function : " + Ex.ToString()); }
        }

        private void CSalesStationaryDetailedRpt()
        {
            try
            {
                RPTName = "RPTCSSummary.rdlc";
                strDTMas = "CSSummary"; strDTTran = ""; strDTHelp = "";
                DTMas = new DataTable();
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    //SqlDataReader dr2 = GetDataReader($"SELECT LogID, CreatedDate FROM tblDailyLog WHERE (CreatedDate BETWEEN '{DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}' AND '{DateTime.Now.Date.AddHours(23.99).ToString("yyyy-MM-dd HH:mm:ss")}') AND LogTitle = 'Closed'");
                    //if (dr2.HasRows)
                    //{
                    //    if (dr2.Read())
                    //    {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Cash Sales & Stationery Summary from " + fdate.ToString("dd/MM/yyyy") + " to " + tdate.ToString("dd/MM/yyyy") + " (Overall)";

                    //string strCSStnryquery = $"Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesDate as PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], c.PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo," +
                    //    $" Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff FROM tblCashSales a" +
                    //    $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo" +
                    //    $" left join (SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum" +
                    //    $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                    //    $" UNION" +
                    //    $" Select '' As Header, b.StnryInvID as ID, b.CDate as PrintDate, 'Stationery' as AWBNo, 'STNRY' as ShipmentType, '' as DestCode, Sum(a.Quantity) as Pieces, 0 as Weight, 0 as VolWeight, b.GST as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, Sum(a.Amount) as StationaryAmt, 0 as InsPremium, b.GrandTotal as WOTotalGST, '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount, 0.00 As RoundingAmt, '' As PaymentMode, '' As PaymentRefNo, 0.00 as EHQDiff FROM tblStationary a" +
                    //    $" JOIN tblStationaryInvoice b ON a.BatchNum = b.BatchNum" +
                    //    $" WHERE (b.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                    //    $" GROUP BY b.StnryInvID,b.CDate,b.GST,b.GrandTotal" +
                    //    $" UNION" +
                    //    $" Select '' As Header, LogID as ID, CreatedDate as PrintDate, 'SalesClosed' as AWBNo, '' as ShipmentType, '' as DestCode, 0 as Pieces, 0 as Weight, 0 as VolWeight, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST,  '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount, 0.00 As RoundingAmt, '' As PaymentMode, '' As PaymentRefNo, 0.00 as EHQDiff FROM tblDailyLog" +
                    //    $" WHERE (CreatedDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed' Order By PrintDate";

                    string strCSStnryquery = $"Select distinct '{RPTHeaderName}' As Header, 0 as ID, a.CSalesNo, a.CSalesDate as PrintDate, '' as AWBNo, '' as ShipmentType, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and a.CSalesNo LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge   FROM tblCashSales a " +
                        //$"left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        //$"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        //$"left join tblDestination d on b.destcode = d.DestCode " +
                        //$"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1 " +
                        $" UNION " +
                        $" Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesNo, a.CSalesDate as PrintDate, iif(b.shipmenttype = 'STATIONERY', ISNULL(f.itemdesc, b.CNNum), b.CNNum) as AWBNo, b.ShipmentType as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], ISNULL(c.PrintCount, 0) AS PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, b.DestCode as AWBDestCode, b.Pieces as AWBPieces, b.HighestWeight as AWBWeight, b.VolWeight as AWBVolWeight, IIF(b.ShipmentType = 'STATIONERY', b.Price, 0) as StationaryPrice, b.InsValue as AWBInsurans, IIF(b.ShipmentType != 'STATIONERY', b.Price, 0) as AWBPrice, b.OtherCharges as AWBOtherCharges, (b.TotalAmt + b.InsValue - b.StaffDiscAmt - b.PromoDiscAmt) as AWBTotalAmt, b.StaffDiscAmt as AWBStaffDiscAmt, b.PromoDiscAmt as AWBPromoDiscAmt, IIF(b.TaxCode = 'Surcharge Tax', b.destcode, 'MALAYSIA') as Country, ISNULL(b.RASurcharge, 0) FROM tblCashSales a" +
                        $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        $" left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        $" left join tblDestination d on b.destcode = d.DestCode " +
                        $" LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $" LEFT JOIN tblItemMaster f on b.CNNum = f.ItemCode " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (b.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND b.CSCNNumID is not null AND a.Status = 1 " +
                        $" UNION " +
                        $" Select '{RPTHeaderName}' As Header, 0 as ID, 'Sales Close on ' + Convert(varchar ,CreatedDate) AS CSalesNo, CreatedDate as PrintDate, LogTitle  as AWBNo, '' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0  as DiscPercentage, 0 as DiscAmount, 0 as RoundingAmt, '' as PaymentMode, '' as PaymentRefNo, 0 AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge    " +
                        $" FROM tblDailyLog WHERE (CreatedDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed' " +
                        $" UNION " +
                        $" Select '{RPTHeaderName}' As Header, b.CSStnryID as ID, a.CSalesNo, a.CSalesDate as PrintDate, b.itemname  as AWBNo, 'STATIONERY' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, 0.00 AS EHQDiff, '' as AWBDestCode, b.Quantity as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, b.Rate as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, b.Rate as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, 'MALAYSIA' as Country, 0 as RASurcharge   " +
                        $" FROM tblCashSales a left join tblCSStationary b on a.CSalesNo = b.CSalesNo " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1 and b.CSStnryID is not null Order By PrintDate, AWBNo";

                    DTMas = GetDataTable(strCSStnryquery);

                    DTMas.AsEnumerable().Where(rec => rec["AWBNo"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => { rec["AWBNo"] = "Stationery"; });
                    DTMas = DataTableSort(DTMas, "PrintDate", "ASC");
                    RPTHeaderName = "";
                    canPreview = DTMas.Rows.Count > 0 ? true : false;
                    //    }
                    //}
                }
                else
                {
                    MessageBox.Show("Please select the from and to-date", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales and Stationery Summary Function : " + Ex.ToString()); }
        }

        private void CSalesStationaryBeforeDetailedRpt()
        {
            try
            {
                RPTName = "RPTCSSummary.rdlc";
                strDTMas = "CSSummary"; strDTTran = ""; strDTHelp = ""; canPreview = false;
                DTMas = new DataTable();
                SqlDataReader dr;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Cash Sales & Stationery Summary from " + fdate.ToString("dd/MM/yyyy") + " to " + tdate.ToString("dd/MM/yyyy") + " (Before Close)";

                    int daycount = (tdate.Date - fdate.Date).Days;
                    daycount += 1;

                    DateTime ffdate = Convert.ToDateTime(FromDate).Date.AddDays(-1), ttdate = Convert.ToDateTime(ToDate).Date.AddDays(-1);
                    for (int i = 0; i < daycount; i++)
                    {
                        ffdate = fdate.AddDays(i); ttdate = ffdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                        dr = GetDataReader($"SELECT TOP 1 CreatedDate FROM tblDailyLog" +
                            $" WHERE (CreatedDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed'" +
                            $" ORDER BY CreatedDate");
                        if (dr.HasRows)
                        {
                            dr.Read();
                            ttdate = Convert.ToDateTime(dr["CreatedDate"]);
                            dr.Close();
                        }

                        //DTMas = GetDataTable($"Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesDate as PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], c.PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo," +
                        //    $" Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff FROM tblCashSales a" +
                        //    $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        //    $" left join (SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum" +
                        //    $" WHERE (a.CDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        //    $" UNION" +
                        //    $" Select '' As Header, b.StnryInvID as ID, b.CDate as PrintDate, 'Stationery' as AWBNo, 'STNRY' as ShipmentType, '' as DestCode, Sum(a.Quantity) as Pieces, 0 as Weight, 0 as VolWeight, b.GST as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, Sum(a.Amount) as StationaryAmt, 0 as InsPremium, b.GrandTotal as WOTotalGST,  '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount, 0.00 As RoundingAmt, '' As PaymentMode, '' As PaymentRefNo, 0.00 as EHQDiff FROM tblStationary a" +
                        //    $" JOIN tblStationaryInvoice b ON a.BatchNum = b.BatchNum" +
                        //    $" WHERE (b.CDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        //    $" GROUP BY b.StnryInvID,b.CDate,b.GST,b.GrandTotal");


                        DTMas = GetDataTable($"Select distinct '{RPTHeaderName}' As Header, 0 as ID, a.CSalesNo, a.CSalesDate as PrintDate, '' as AWBNo, '' as ShipmentType, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and a.CSalesNo LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge FROM tblCashSales a " +
                        //$"left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        //$"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        //$"left join tblDestination d on b.destcode = d.DestCode " +
                        //$"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') = FORMAT(a.CDate, 'yyyy-MM-dd') AND a.Status = 1 " +
                        $" union " +
                        $" Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesNo, a.CSalesDate as PrintDate, iif(b.shipmenttype = 'STATIONERY', ISNULL(f.itemdesc, b.CNNum), b.CNNum)  as AWBNo, b.ShipmentType as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], ISNULL(c.PrintCount, 0) AS PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, b.DestCode as AWBDestCode, b.Pieces as AWBPieces, b.HighestWeight as AWBWeight, b.VolWeight as AWBVolWeight, IIF(b.ShipmentType = 'STATIONERY', b.Price, 0) as StationaryPrice, b.InsValue as AWBInsurans, IIF(b.ShipmentType != 'STATIONERY', b.Price, 0) as AWBPrice, b.OtherCharges as AWBOtherCharges, (b.TotalAmt + b.InsValue - b.StaffDiscAmt - b.PromoDiscAmt) as AWBTotalAmt, b.StaffDiscAmt as AWBStaffDiscAmt, b.PromoDiscAmt as AWBPromoDiscAmt, IIF(b.TaxCode = 'Surcharge Tax', b.destcode, 'MALAYSIA') as Country, ISNULL(b.RASurcharge, 0) FROM tblCashSales a" +
                        $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        $" left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                        $" left join tblDestination d on b.destcode = d.DestCode " +
                        $" LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                        $" LEFT JOIN tblItemMaster f on b.CNNum = f.ItemCode " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (b.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') = FORMAT(a.CDate, 'yyyy-MM-dd') AND b.CSCNNumID IS NOT NULL AND a.Status = 1 " +
                        $" UNION " +
                        $" Select '{RPTHeaderName}' As Header, 0 as ID, 'Sales Close on ' + Convert(varchar ,CreatedDate) AS CSalesNo, CreatedDate as PrintDate, LogTitle  as AWBNo, '' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0  as DiscPercentage, 0 as DiscAmount, 0 as RoundingAmt, '' as PaymentMode, '' as PaymentRefNo, 0 AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge  FROM tblDailyLog WHERE (CreatedDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed' " +
                        $" UNION " +
                        $" Select '{RPTHeaderName}' As Header, b.CSStnryID as ID, a.CSalesNo, a.CSalesDate as PrintDate, b.itemname  as AWBNo, 'STATIONERY' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, 0.00 AS EHQDiff, '' as AWBDestCode, b.Quantity as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, b.Rate as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, b.Rate as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, 'MALAYSIA' as Country, 0 as RASurcharge " +
                        $" FROM tblCashSales a left join tblCSStationary b on a.CSalesNo = b.CSalesNo " +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') = FORMAT(a.CDate, 'yyyy-MM-dd') AND a.Status = 1 and b.CSStnryID is not null Order By PrintDate, AWBNo");
                    }

                    if (DTMas.Rows.Count > 0)
                    {
                        DTMas.AsEnumerable().Where(rec => rec["AWBNo"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => { rec["AWBNo"] = "Stationery"; });
                        RPTHeaderName = "";
                        DTMas = DataTableSort(DTMas, "PrintDate", "ASC");
                        canPreview = true;
                    }
                }
                else
                {
                    MessageBox.Show("Please select the from and to-date", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales and Stationery Summary Function : " + Ex.ToString()); }
        }

        private void CSalesStationaryAfterDetailedRpt()
        {
            try
            {
                RPTName = "RPTCSSummary.rdlc";
                strDTMas = "CSSummary"; strDTTran = ""; strDTHelp = ""; canPreview = false;
                DTMas = new DataTable();
                SqlDataReader dr;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    RPTHeaderName = "Cash Sales & Stationery Summary from " + fdate.ToString("dd/MM/yyyy") + " to " + tdate.ToString("dd/MM/yyyy") + " (After Close)";

                    int daycount = (tdate.Date - fdate.Date).Days;
                    daycount += 1;

                    DateTime ffdate = Convert.ToDateTime(FromDate).Date.AddDays(-1), ttdate = Convert.ToDateTime(ToDate).Date.AddDays(-1);
                    for (int i = 0; i < daycount; i++)
                    {
                        ffdate = fdate.AddDays(i); ttdate = ffdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                        dr = GetDataReader($"SELECT TOP 1 CreatedDate FROM tblDailyLog" +
                            $" WHERE (CreatedDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed'" +
                            $" ORDER BY CreatedDate");
                        if (dr.HasRows)
                        {
                            dr.Read();
                            ffdate = Convert.ToDateTime(dr["CreatedDate"]).AddSeconds(1);
                            dr.Close();
                        }

                        //DTMas = GetDataTable($"Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesDate as PrintDate, b.CNNum as AWBNo, b.ShipmentType as ShipmentType, b.DestCode as DestCode, b.Pieces as Pieces, b.Weight as Weight, b.VolWeight as VolWeight, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST,  '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], c.PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo," +
                        //    $" Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff FROM tblCashSales a" +
                        //    $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                        //    $" left join (SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum" +
                        //    $" WHERE (a.CDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        //    $" UNION" +
                        //    $" Select '' As Header, b.StnryInvID as ID, b.CDate as PrintDate, 'Stationery' as AWBNo, 'STNRY' as ShipmentType, '' as DestCode, Sum(a.Quantity) as Pieces, 0 as Weight, 0 as VolWeight, b.GST as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, Sum(a.Amount) as StationaryAmt, 0 as InsPremium, b.GrandTotal as WOTotalGST,  '{(MyCompanyCode + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))}' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0 as DiscPercentage, 0 as DiscAmount, 0.00 As RoundingAmt, '' As PaymentMode, '' As PaymentRefNo, 0.00 AS EHQDiff FROM tblStationary a" +
                        //    $" JOIN tblStationaryInvoice b ON a.BatchNum = b.BatchNum" +
                        //    $" WHERE (b.CDate BETWEEN '{ffdate.ToString(sqlDateLongFormat)}' AND '{ttdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        //    $" GROUP BY b.StnryInvID,b.CDate,b.GST,b.GrandTotal");

                        DTMas = GetDataTable($"Select distinct '{RPTHeaderName}' As Header, 0 as ID, a.CSalesNo, a.CSalesDate as PrintDate, '' as AWBNo, '' as ShipmentType, a.ShipmentGSTValue as GSTAmt, a.Price as WOGSTAmt, a.ShipmentTotal as WGSTAmt, a.StationaryTotal as StationaryAmt, a.InsuranceTotal as InsPremium, a.CSalesTotal as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, a.RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and a.CSalesNo LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge FROM tblCashSales a " +
                            //$"left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                            //$"left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                            //$"left join tblDestination d on b.destcode = d.DestCode " +
                            //$"LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                            $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') != FORMAT(a.CDate, 'yyyy-MM-dd') OR a.ClosedSalesTime IS NULL) AND a.Status = 1 " +
                            $" union " +
                            $" Select '{RPTHeaderName}' As Header, b.CSCNNumID as ID, a.CSalesNo, a.CSalesDate as PrintDate, iif(b.shipmenttype = 'STATIONERY', ISNULL(f.itemdesc, b.CNNum), b.CNNum) as AWBNo, b.ShipmentType as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], ISNULL(c.PrintCount, 0) AS PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, Case When a.PaymentMode = 'CASH' and b.CNNum LIKE 'EHQ%D' Then a.CSalesTotal Else 0.00 END AS EHQDiff, b.DestCode as AWBDestCode, b.Pieces as AWBPieces, b.HighestWeight as AWBWeight, b.VolWeight as AWBVolWeight, IIF(b.ShipmentType = 'STATIONERY', b.Price, 0) as StationaryPrice, b.InsValue as AWBInsurans, IIF(b.ShipmentType != 'STATIONERY', b.Price, 0) as AWBPrice, b.OtherCharges as AWBOtherCharges, (b.TotalAmt + b.InsValue - b.StaffDiscAmt - b.PromoDiscAmt) as AWBTotalAmt, b.StaffDiscAmt as AWBStaffDiscAmt, b.PromoDiscAmt as AWBPromoDiscAmt, IIF(b.TaxCode = 'Surcharge Tax', b.destcode, 'MALAYSIA') as Country, ISNULL(b.RASurcharge, 0) FROM tblCashSales a" +
                            $" left join tblCSaleCNNum b on a.CSalesNo = b.CSalesNo " +
                            $" left join(SELECT AWBNum, COUNT(AWBNum) As PrintCount FROM tblCSPrintDate GROUP BY AWBNum) c on b.CNNum = c.AWBNum " +
                            $" left join tblDestination d on b.destcode = d.DestCode LEFT JOIN tblZoneDetail e ON RTRIM(d.DestCode) = RTRIM(e.DestCode) " +
                            $" LEFT JOIN tblItemMaster f on b.CNNum = f.ItemCode " +
                            $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (b.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') != FORMAT(a.CDate, 'yyyy-MM-dd') OR a.ClosedSalesTime IS NULL) AND a.Status = 1 and b.CSCNNumID is not null " +
                            $" UNION " +
                            $" Select '{RPTHeaderName}' As Header, 0 as ID, 'Sales Close on ' + Convert(varchar ,CreatedDate) AS CSalesNo, CreatedDate as PrintDate, LogTitle  as AWBNo, '' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, '' as StaffCode, '' as StaffName, 0 as StaffDisAmt, 0 as OtherCharges, '' as DiscCode, 0  as DiscPercentage, 0 as DiscAmount, 0 as RoundingAmt, '' as PaymentMode, '' as PaymentRefNo, 0 AS EHQDiff, '' as AWBDestCode, 0 as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, 0 as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, 0 as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, '' as Country, 0 as RASurcharge  " +
                            $" FROM tblDailyLog " +
                            $" WHERE (CreatedDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND IsDeleted = 0 AND LogTitle = 'Closed' " +
                            $" UNION " +
                            $" Select '{RPTHeaderName}' As Header, b.CSStnryID as ID, a.CSalesNo, a.CSalesDate as PrintDate, b.itemname  as AWBNo, 'STATIONERY' as ShipmentType, 0 as GSTAmt, 0 as WOGSTAmt, 0 as WGSTAmt, 0 as StationaryAmt, 0 as InsPremium, 0 as WOTotalGST, '{(MyCompanyCode + "" + (!string.IsNullOrEmpty(MyCompanySubName) ? "-" + MyCompanySubName : ""))} ' as Station, '{LoginUserName}' as [User], 0 as PrintCount, a.StaffCode as StaffCode, a.StaffName as StaffName, a.StaffDisAmt as StaffDisAmt, a.OtherCharges, a.DiscCode, a.DiscPercentage, a.DiscAmount, 0 as RoundingAmt, a.PaymentMode, a.PaymentRefNo, 0.00 AS EHQDiff, '' as AWBDestCode, b.Quantity as AWBPieces, 0 as AWBWeight, 0 as AWBVolWeight, b.Rate as StationaryPrice, 0 as AWBInsurans, 0 as AWBPrice, 0 as AWBOtherCharges, b.Rate as AWBTotalAmt, 0 as AWBStaffDiscAmt, 0 as AWBPromoDiscAmt, 'MALAYSIA' as Country, 0 as RASurcharge FROM tblCashSales a left join tblCSStationary b on a.CSalesNo = b.CSalesNo " +
                            $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND (FORMAT(a.ClosedSalesTime, 'yyyy-MM-dd') != FORMAT(a.CDate, 'yyyy-MM-dd') OR a.ClosedSalesTime IS NULL) AND a.Status = 1 and b.CSStnryID is not null Order By PrintDate, AWBNo");


                    }

                    if (DTMas.Rows.Count > 0)
                    {
                        DTMas.AsEnumerable().Where(rec => rec["AWBNo"] == DBNull.Value && Convert.ToDecimal(rec["StationaryAmt"]) > 0).ToList().ForEach(rec => { rec["AWBNo"] = "Stationery"; });
                        RPTHeaderName = "";
                        DTMas = DataTableSort(DTMas, "PrintDate", "ASC");
                        canPreview = true;
                    }
                }
                else
                {
                    MessageBox.Show("Please select the from and to-date", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales and Stationery Summary Function : " + Ex.ToString()); }
        }
        private void CSalesStationaryRpt()
        {
            try
            {
                RPTName = "RPTCSSTNRYSummary.rdlc";
                strDTMas = "CSalesStationarySummary"; strDTTran = ""; strDTHelp = ""; decimal zeroval = 0;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date, tdate = Convert.ToDateTime(ToDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT Date,  CSAmount = Sum(CSAmount), StnryAmount = Sum(StnryAmount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM" +
                        $" (SELECT Date = format(a.CSalesDate, 'dd-MMM-yyyy'),  CSAmount = Sum(a.CSalesTotal), StnryAmount = zeroval, StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblCashSales a" +
                        $" WHERE (a.CSalesDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CSalesDate, 'dd-MMM-yyyy')" +
                        $" UNION" +
                        $" SELECT Date = format(a.CDate, 'dd-MMM-yyyy'), CSAmount = zeroval, StnryAmount = Sum(a.Amount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblStationary a" +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CDate, 'dd-MMM-yyyy')) as gen" +
                        $" GROUP BY Date");

                    RPTHeaderName = "Cash Sales & Stationery Summary from " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString() + " to " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date;
                    DateTime tdate = fdate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT Date,  CSAmount = Sum(CSAmount), StnryAmount = Sum(StnryAmount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM" +
                        $" (SELECT Date = format(a.CSalesDate, 'dd-MMM-yyyy'),  CSAmount = Sum(a.CSalesTotal), StnryAmount = zeroval, StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblCashSales a" +
                        $" WHERE (a.CSalesDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CSalesDate, 'dd-MMM-yyyy')" +
                        $" UNION" +
                        $" SELECT Date = format(a.CDate, 'dd-MMM-yyyy'), CSAmount = zeroval, StnryAmount = Sum(a.Amount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblStationary a" +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CDate, 'dd-MMM-yyyy')) as gen" +
                        $" GROUP BY Date");

                    RPTHeaderName = "Cash Sales & Stationery Summary on " + fdate.Day.ToString("00") + "/" + fdate.Month.ToString("00") + "/" + fdate.Year.ToString();
                }
                else if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime fdate = Convert.ToDateTime(FromDate).Date;
                    DateTime tdate = fdate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTMas = GetDataTable($"SELECT Date,  CSAmount = Sum(CSAmount), StnryAmount = Sum(StnryAmount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM" +
                        $" (SELECT Date = format(a.CSalesDate, 'dd-MMM-yyyy'),  CSAmount = Sum(a.CSalesTotal), StnryAmount = zeroval, StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblCashSales a" +
                        $" WHERE (a.CSalesDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CSalesDate, 'dd-MMM-yyyy')" +
                        $" UNION" +
                        $" SELECT Date = format(a.CDate, 'dd-MMM-yyyy'), CSAmount = zeroval, StnryAmount = Sum(a.Amount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblStationary a" +
                        $" WHERE (a.CDate BETWEEN '{fdate.ToString(sqlDateLongFormat)}' AND '{tdate.ToString(sqlDateLongFormat)}') AND a.Status = 1" +
                        $" GROUP BY format(a.CDate, 'dd-MMM-yyyy')) as gen" +
                        $" GROUP BY Date");

                    RPTHeaderName = "Cash Sales & Stationery Summary on " + tdate.Day.ToString("00") + "/" + tdate.Month.ToString("00") + "/" + tdate.Year.ToString();
                }
                else
                {
                    DTMas = GetDataTable($"SELECT Date,  CSAmount = Sum(CSAmount), StnryAmount = Sum(StnryAmount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM" +
                        $" (SELECT Date = format(a.CSalesDate, 'dd-MMM-yyyy'),  CSAmount = Sum(a.CSalesTotal), StnryAmount = zeroval, StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblCashSales a" +
                        $" WHERE a.Status = 1 GROUP BY format(a.CSalesDate, 'dd-MMM-yyyy')" +
                        $" UNION" +
                        $" SELECT Date = format(a.CDate, 'dd-MMM-yyyy'), CSAmount = zeroval, StnryAmount = Sum(a.Amount), StnCode = MyCompanyCode, SubName = MyCompanySubName FROM tblStationary a" +
                        $" WHERE a.Status = {true} GROUP BY format(a.CDate, 'dd-MMM-yyyy')) as gen" +
                        $" GROUP BY Date");

                    RPTHeaderName = "Cash Sales & Stationery Summary";
                }
                if (DTMas.Rows.Count > 0)
                {
                    canPreview = true;
                }
                else
                {
                    canPreview = false;
                }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Cash Sales and Stationery Summary Function : " + Ex.ToString()); }
        }

        private void StationaryInvoiceRpt()
        {
            SqlDataReader dr;
            try
            {
                if (StnryBatchNo.Trim() != "")
                {
                    RPTName = "RPTCSTaxInvoice.rdlc"; strDTMas = "CashSalesDetails"; strDTTran = "Consignor";
                    string CodeConsignor = "";

                    decimal limitkg = Convert.ToDecimal(30.009);

                    DTMas = GetDataTable($"SELECT num.CNNum, gen.CSalesNo, gen.CSalesDate, num.ShipmentType, num.DestCode, num.Pieces, num.Weight, num.VLength, num.VBreadth, num.VHeight, num.VolWeight, gen.Price, gen.OtherCharges, gen.StationaryTotal, gen.IsInsuranced, gen.InsShipmentValue, num.InsShipmentDesc, gen.InsValue, gen.InsuranceTotal, gen.InsGSTValue, gen.StaffCode, gen.StaffName, gen.StaffDisAmt, gen.DiscCode, gen.DiscPercentage, gen.DiscAmount, gen.ShipmentTotal, gen.TaxTitle, gen.TaxPercentage, gen.ShipmentGSTValue, gen.RoundingAmt, gen.CSalesTotal, gen.CDate FROM tblCashSales gen" +
                        $" LEFT JOIN tblCSaleCNNum num ON gen.CSalesNo = num.CSalesNo" +
                        $" WHERE gen.CSalesNo = '{StnryBatchNo}' AND gen.Status = 1");

                    DateTime fdate;
                    dr = GetDataReader($"SELECT CSalesDate FROM tblCashSales Where CSalesNo = '{StnryBatchNo}'");
                    dr.Read(); fdate = Convert.ToDateTime(dr["CSalesDate"]).Date; dr.Close();

                    DateTime tdate = fdate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                        $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}' AND a.Status = 1  AND ((a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND a.ToDate >= '{tdate.ToString(sqlDateLongFormat)}') OR (a.FromDate <= '{fdate.ToString(sqlDateLongFormat)}' AND a.ToDate IS NULL))");
                    if (DTCompany.Rows.Count <= 0)
                    {
                        DTCompany = GetDataTable($"SELECT a.CompId, a.CompCode, Name = a.CompName, SName = a.CompType, a.CashAccNum, a.TaxNo, a.TaxTitle, a.RegNum, a.IATACode, a.HQCode, Address1 = ISNULL(a.Address1, ''), Address2 = ISNULL(a.Address2, ''), Address3 = ISNULL(a.Address3, ''), City = ISNULL(a.City, ''), State = ISNULL(a.State, ''), Country = ISNULL(a.Country, ''), PostalCode = ISNULL(a.ZipCode, ''), PhoneNo = ISNULL(a.PhoneNum, ''), FaxNo = ISNULL(a.Fax, ''), EMail = ISNULL(a.EMail, ''), a.CreateBy, a.CreateDate, a.ModifyBy, a.ModifyDate, a.Status FROM tblStationProfile a" +
                            $" Where RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}'");
                    }

                    DTCompany.AsEnumerable().Where(rec => rec["Address1"].ToString() != "").ToList().ForEach(rec => { rec["Address1"] = rec["Address1"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Address2"].ToString() != "").ToList().ForEach(rec => { rec["Address2"] = rec["Address2"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Address3"].ToString() != "").ToList().ForEach(rec => { rec["Address3"] = rec["Address3"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["City"].ToString() != "").ToList().ForEach(rec => { rec["City"] = rec["City"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["State"].ToString() != "").ToList().ForEach(rec => { rec["State"] = rec["State"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["Country"].ToString() != "").ToList().ForEach(rec => { rec["Country"] = rec["Country"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["PostalCode"].ToString() != "").ToList().ForEach(rec => { rec["PostalCode"] = rec["PostalCode"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["PhoneNo"].ToString() != "").ToList().ForEach(rec => { rec["PhoneNo"] = rec["PhoneNo"] + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["FaxNo"].ToString() != "").ToList().ForEach(rec => { rec["FaxNo"] = rec["FaxNo"].ToString().Trim() + ", "; });
                    DTCompany.AsEnumerable().Where(rec => rec["EMail"].ToString() != "").ToList().ForEach(rec => { rec["EMail"] = rec["EMail"].ToString().Trim(); });
                }
                else { MessageBox.Show("Invalid Cash sales number", "Warning"); }
            }
            catch (Exception Ex) { error_log.errorlog("Report Search Tax Invoice Function : " + Ex.ToString()); }
        }

        private void ClearAll()
        {
            string emptyStr = string.Empty;
            FromDate = ToDate = DateTime.Now.ToShortDateString();
            UserCode = UserName = emptyStr;
            UserID = 0;
            FromCSNoCode = ToCSNoCode = emptyStr;
            FromCSNoName = ToCSNoName = emptyStr;
            FromCustCode = ToCustCode = emptyStr;
            FromCustName = ToCustName = emptyStr;

            CSNo = CSDate = CustCode = CustName = CustGSTNo = Address1 = Address2 = City = State = Country = PostalCode = Mobile = Email = "";
            StnryBatchNo = StnryBatchDate = ""; StnryCustCode = StnryCustName = StnryCustGSTNo = ""; StnryAddress1 = StnryAddress2 = StnryCity = StnryState = StnryCountry = StnryPostalCode = ""; StnryMobile = StnryEmail = "";
        }
    }
    public class CSSTNRYModel
    {
        public DateTime Date { get; set; }
        public decimal CSAmount { get; set; }
        public decimal StnryAmount { get; set; }
        public string StnCode { get; set; }
        public string SubName { get; set; }
    }
}
