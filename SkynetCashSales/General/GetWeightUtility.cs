﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Management;
using System.Threading;

namespace SkynetCashSales.General
{
    public class GetWeightUtility : AMGenFunction, IDisposable
    {
        string StartupPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        private static int DataBits, SleepTime, Division;
        private static string Machine, PortNumber, BaudRate, Pos;
        public bool IsEnabled;
        WeightUtility ab;
        
        public GetWeightUtility()
        {
            SqlDataReader dr = GetDataReader($"SELECT ISNULL(WTMachineName, 'OTHER') AS WTMachineName, ISNULL(SerialPort, '') AS SerialPort, ISNULL(BaudRate, 0) AS BaudRate, ISNULL(DataBits, 0) AS DataBits, ISNULL(WTLocation, 0) AS WTLocation, ISNULL(WTDividend, 0) AS WTDividend, ISNULL(SleepTime, 0) AS SleepTime FROM tblWPConfig WHERE PCNAME = HOST_NAME() AND Status = 1");
            if (dr.Read())
            {
                IsEnabled = true;
                Machine = dr["WTMachineName"].ToString();
                PortNumber = dr["SerialPort"].ToString();
                BaudRate = dr["BaudRate"].ToString();
                DataBits = Convert.ToInt32(dr["DataBits"]);
                Pos = dr["WTLocation"].ToString();
                Division = Convert.ToInt32(dr["WTDividend"]);
                SleepTime = Convert.ToInt32(dr["SleepTime"]);
            }            
            else
            {
                Machine = "";
                IsEnabled = false;
            }
            dr.Close();
        }

        public string GetWeight()
        {
            string weight = "0", Commweight = "";
            if (Machine == "CZNEWTON EWA")
            {
                weight = ab.WeightDetails(PortNumber, Convert.ToInt32(BaudRate), Convert.ToInt32(Pos), Division, SleepTime).ToString();
                error_log.errorlog("CZNEWTON EWA:" + weight);
                return weight;
            }
            else if (Machine == "METTLER TOLEDO")
            {
                SerialPort comPort = new SerialPort();
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        if (comPort != null && comPort.IsOpen == false)
                        {
                            if (BaudRate != "")
                                comPort.BaudRate = Convert.ToInt32(BaudRate);
                            else
                                comPort.BaudRate = 9600;
                            if (PortNumber != "")
                                comPort.PortName = PortNumber;
                            else
                                comPort.PortName = "COM1";
                            comPort.Open();
                        }
                        Thread.Sleep(SleepTime);
                        try
                        {
                            if (comPort != null && comPort.IsOpen)
                                Commweight = comPort.ReadExisting();

                        }
                        finally
                        {
                            comPort.Close();
                            comPort.Dispose();
                        }
                        if (Commweight != "")
                        {
                            try
                            {
                                string[] strar = Commweight.Split(null);
                                double Weighresult = 0;
                                try
                                {
                                    Weighresult = Convert.ToDouble(strar[Convert.ToInt16(Pos)]);
                                    if (Weighresult > 0)
                                    {
                                        weight = Math.Round(Weighresult / Division, 3).ToString("N3");
                                        return weight;
                                    }

                                }
                                catch (Exception c)
                                {
                                    for (i = 0; i < strar.Length; i++)
                                    {
                                        try
                                        {
                                            weight = strar[i];
                                            Weighresult = Convert.ToDouble(weight != "" && weight != null ? weight : "0");
                                            if (Weighresult > 0)
                                            {
                                                weight = Math.Round(Weighresult / Division, 3).ToString("N3");
                                                //error_log.errorlog("METTLER TOLEDO:" + weight);
                                                return weight;
                                            }
                                        }
                                        catch (Exception d)
                                        {

                                        }
                                    }
                                }
                            }
                            catch (Exception c)
                            {
                                try
                                {
                                    comPort.Close();
                                }
                                catch (Exception d)
                                {
                                    // do nothing
                                }
                                comPort.Dispose();
                                comPort = new SerialPort();
                            }
                        }
                    }
                    catch (System.IO.IOException c)
                    {
                        try
                        {
                            comPort.Close();
                        }
                        catch (Exception d)
                        {
                            // do nothing
                        }
                        comPort.Dispose();
                        comPort = new SerialPort();
                    }
                    catch (Exception c)
                    {
                        //comPort.Close();
                        comPort.Dispose();
                        comPort = new SerialPort();
                    }
                }
                //MessageBox.Show("Failed to read from weighing scale. Please try again");
                comPort.Dispose();
                error_log.errorlog("METTLER TOLEDO failed:" + weight);
                return "1";
            }
            else
            {
                SerialPort comPort = new SerialPort(PortNumber);
                comPort.Open();
                if (comPort.IsOpen == true)
                {
                    if (comPort.IsOpen) { comPort.Close(); }
                    if (!(comPort.IsOpen == true))
                    {
                        comPort.PortName = PortNumber;
                        comPort.BaudRate = Convert.ToInt32(BaudRate);
                        comPort.Open();
                    }
                    Thread.Sleep(SleepTime);

                    comPort.Handshake = Handshake.RequestToSendXOnXOff;
                    comPort.BreakState = false;
                    Commweight = comPort.ReadExisting();
                    if (Commweight == "")
                    {
                        comPort.Close();
                        if (!(comPort.IsOpen == true))
                        {
                            comPort.PortName = PortNumber;
                            comPort.BaudRate = Convert.ToInt32(BaudRate);
                            comPort.Open();
                        }
                        Commweight = comPort.ReadExisting();
                        string[] strar = Commweight.Split(null);
                        weight = strar[Convert.ToInt16(Pos)];
                    }
                    else
                    {
                        string[] strar = Commweight.Split(null);
                        weight = strar[Convert.ToInt16(Pos)];
                    }
                    comPort.Close();
                    return weight;
                }
            }
            return "1";
        }

        public void Dispose()
        {
            //    if (comPort != null)
            //    {
            //        comPort.Close();
            //        comPort.Dispose();
            //    }
        }

    }
}