﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace SkynetCashSales.General
{
    public class RelayCommand : ICommand
    {
        public static string CurrentUserID;
        public static string CurrentUserIP;
        public static string CurrentUserMachine;
        public static string CurrentOriginCode;
        #region Fields
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;
        #endregion // Fields

        #region Constructors
        public RelayCommand() { }
        public RelayCommand(Action<object> execute) : this(execute, null) { }
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }
        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
        #endregion // ICommand Members

        //For setting the Focus
        RelayCommand _FocusNext;
        public RelayCommand FocusNext
        {
            get { _FocusNext = new RelayCommand(param => FocusNextControl(), param => CanFocusNext()); return _FocusNext; }
        }
        public virtual void FocusNextControl()
        {
            UIElement focusedElement = Keyboard.FocusedElement as UIElement;

            if (focusedElement != null)
            {
                focusedElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }
        public virtual bool CanFocusNext()
        {
            return true;
        }        
    }    
}