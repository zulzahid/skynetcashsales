﻿using Microsoft.Win32;
using Newtonsoft.Json;
using SkynetCashSales.ViewModel.Transactions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;


namespace SkynetCashSales.General
{
    public class AMGenFunction : PropertyChangedNotification
    {
        public AMGenFunction() { }
        public static MainWindow MainWind;
        public static bool MainWindStat = false;
        public CommunicateWebDb WebDb;
        public static bool NoRange;

        public static int IDAutoUpdate;
        public static string FTPHost, FTPFilePath, FTPUser, FTPPassword, FTPFinalFileDirectory, FTPMainFileName;
        public static string sqlDateShorFormat = "MM/dd/yyyy", sqlDateLongFormat = "MM/dd/yyyy HH:mm:ss";

        public static int LoginUserId;
        public static string LoginUserName, LoginUserAuthentication;
        public static int MyCompanyId, MyBranchId;
        public static string CodeChoosen, CNPrinter, MyCompanyCode, MyCompanyName, MyCompanySubName, MyBranchCode, MyStateName, CSalesNoPrefix;
        public static string CSInvenOBPrefix, CSInvenPRPrefix, CSInvenPOPrefix, CSInvenGRNPrefix, CSInvenSAPrefix;
        public static DataTable DTCompAddress = new DataTable();
        public static DataTable DTProdCode = new DataTable();
        public bool IsAuthenticated;
        public static string CurrentVersion = "V4.1.00", CSSUpgradeToken = "86d321823d134762aa9685f8034a174d";
        public static string APIMainURI, APIToken, APICheckAWB;
        public static string APIMobileReferenceBase, APIMobileReferenceToken, APIMobileReferenceGet, APIMobileReferenceUpdate;
        public static string APICreateTracking, APITrackingBase;
        public static string APICreatetblRcvCashSale, APICreatetblRcvCashSaleBase;
        public static string _totalAWB, _totalStationary, _totalSales, _totallodgein, _totalCollectedAmount;

        public MessageBoxResult MsgRes;
        public static string ConStr, sqlServer, sqlInstance, sqlDbName;

        public static SqlConnection SqlCon;
        public static SqlCommand SqlCmd;
        private SqlDataAdapter SqlAdapter;

        public static SqlConnection mSqlCon;

        public static tblCSSUpgrade cssupgrade = new tblCSSUpgrade();

        private int _selectedshipmentindex, _selectedstationaryindex;
        public int SelectedShipmentIndex { get { return _selectedshipmentindex; } set { _selectedshipmentindex = value; OnPropertyChanged("SelectedShipmentIndex"); } }
        public int SelectedStationaryIndex { get { return _selectedstationaryindex; } set { _selectedstationaryindex = value; OnPropertyChanged("SelectedStationaryIndex"); } }

        public string MMMasterVisisbility { get { return GetValue(() => MMMasterVisisbility); } set { SetValue(() => MMMasterVisisbility, value); OnPropertyChanged("MMMasterVisisbility"); } }

        public string totalAWB { get { return _totalAWB; } set { _totalAWB = value; OnPropertyChanged("totalAWB"); } }
        public string totalStationary { get { return _totalStationary; } set { _totalStationary = value; OnPropertyChanged("totalStationary"); } }
        public string totalCollectedAmount { get { return _totalCollectedAmount; } set { _totalCollectedAmount = value; OnPropertyChanged("totalCollectedAmount"); } }
        public string totalSales { get { return _totalSales; } set { _totalSales = value; OnPropertyChanged("totalSales"); } }
        public string totallodgein { get { return _totallodgein; } set { _totallodgein = value; OnPropertyChanged("totallodgein"); } }
        public bool isCNPrinter { get { return GetValue(() => isCNPrinter); } set { SetValue(() => isCNPrinter, value); OnPropertyChanged("isCNPrinter"); } }
        public bool isStickerPrinter { get { return GetValue(() => isStickerPrinter); } set { SetValue(() => isStickerPrinter, value); OnPropertyChanged("isStickerPrinter"); } }
        public string StandardQuotationType { get { return GetValue(() => StandardQuotationType); } set { SetValue(() => StandardQuotationType, value); OnPropertyChanged("StandardQuotationType"); } }
        public string InternationalQuotationType { get { return GetValue(() => InternationalQuotationType); } set { SetValue(() => InternationalQuotationType, value); OnPropertyChanged("InternationalQuotationType"); } }
        public string StandardTaxCode { get { return GetValue(() => StandardTaxCode); } set { SetValue(() => StandardTaxCode, value); OnPropertyChanged("StandardTaxCode"); } }
        public string InternationalTaxCode { get { return GetValue(() => InternationalTaxCode); } set { SetValue(() => InternationalTaxCode, value); OnPropertyChanged("InternationalTaxCode"); } }
        public decimal StandardTaxRate { get { return GetValue(() => StandardTaxRate); } set { SetValue(() => StandardTaxRate, value); OnPropertyChanged("StandardTaxRate"); } }
        public decimal InternationalTaxRate { get { return GetValue(() => InternationalTaxRate); } set { SetValue(() => InternationalTaxRate, value); OnPropertyChanged("InternationalTaxRate"); } }

        private UpdateDatabase UpdtDb;
        public void SqlDatabseConection()
        {
            try
            {
                string tsqlserver = "";
                tsqlserver = sqlServer.Contains(".") ? sqlServer : sqlServer + "\\" + sqlInstance;
                //tsqlserver = sqlServer.Contains(".") ? sqlServer : sqlServer + (sqlInstance.Trim() != "" ? "\\" + sqlInstance : "");

                if (MyCompanyCode.Trim() == "CR2" || MyCompanyCode.Trim() == "CR3")
                    ConStr = @"data source=" + tsqlserver + ";initial catalog=" + sqlDbName + ";User Id=SCSHQ2020; Password= #2020@HQ!SCS; MultipleActiveResultSets = True;pooling=true;Max Pool Size=1024;";
                else
                    ConStr = @"data source=" + tsqlserver + ";initial catalog=" + sqlDbName + ";User Id=sa; Password=skynet12#; MultipleActiveResultSets = True;pooling=true;Max Pool Size=1024;";
                SqlCon = new SqlConnection(ConStr);
                SqlCon.Open();
            }
            catch (SqlException ex)
            {
                error_log.errorlog("SqlException : " + ex.ToString());
                if (ex.ToString().Contains("The login failed"))
                {
                    ConStr = @"data source=" + sqlServer + ";initial catalog=" + sqlDbName + "; Integrated Security=SSPI; persist security info=True; MultipleActiveResultSets = True;pooling=true;Max Pool Size=1024;";
                    SqlCon = new SqlConnection(ConStr);
                    SqlCon.Open();
                }
            }

            if (SqlCon.State == ConnectionState.Open)
            {
                UpdtDb = new UpdateDatabase();
                UpdtDb.CheckDatabaseVersion();
            }
        }

        public void InitializedDashboard()
        {
            try
            {
                totalAWB = "0";
                totalStationary = " 0";
                totallodgein = "0";
                totalSales = "0";
                totalCollectedAmount = "0";

                using (SqlDataReader dr = GetDataReader($"SELECT count(B.CNNum) as TotalCount from tblCashSales A" +
                    $" LEFT JOIN tblCSaleCNNum B ON A.CSalesNo = B.CSalesNo" +
                    $" where FORMAT(A.CSalesDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and FORMAT(B.CDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') AND B.CSalesNo IS NOT NULL and A.Status = 1 and B.ShipmentType != 'STATIONERY' "))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            totalAWB = dr["TotalCount"].ToString();
                            MainWind.txttotalAWB.Text = totalAWB;
                        }
                    }
                    dr.Close();
                }

                using (SqlDataReader dr = GetDataReader($"SELECT count(B.CNNum) as TotalCount from tblCashSales A" +
                    $" LEFT JOIN tblCSaleCNNum B ON A.CSalesNo = B.CSalesNo" +
                    $" where FORMAT(A.CSalesDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and FORMAT(B.CDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') AND B.CSalesNo IS NOT NULL and A.Status = 1 and B.ShipmentType = 'STATIONERY' "))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            totalStationary = dr["TotalCount"].ToString();
                            MainWind.txttotalStationary.Text = totalStationary;
                        }
                    }
                    dr.Close();
                }

                using (SqlDataReader dr = GetDataReader($"select sum(CSalesTotal) as TotalAmount from tblCashSales where FORMAT(CSalesDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and Status = 1"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            totalSales = string.Format("{0:0.00}", GetRoundingVal((dr["TotalAmount"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["TotalAmount"])), 2));
                            MainWind.txttotalSales.Text = totalSales;
                            //MainWind.txttotalSales.Text = string.Format("{0:0.00}", Convert.ToDecimal(totalSales));
                        }
                    }
                    dr.Close();
                }

                using (SqlDataReader dr = GetDataReader($"select sum(CSalesTotal) as TotalAmount from tblCashSales where FORMAT(CSalesDate, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and PaymentMode = 'CASH' and Status = 1"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            totalCollectedAmount = string.Format("{0:0.00}", GetRoundingVal((dr["TotalAmount"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["TotalAmount"])), 2));
                            MainWind.totalCollectedAmount.Text = totalCollectedAmount;
                        }
                    }
                    dr.Close();
                }

                using (SqlDataReader dr = GetDataReader($"SELECT count(*) as TotalCountLodge FROM tblLodginAWB where FORMAT(Date, 'dd-MMM-yyyy') = FORMAT(GETDATE(), 'dd-MMM-yyyy') and Status = 1"))
                {
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            totallodgein = dr["TotalCountLodge"].ToString();
                            MainWind.txttotallodgein.Text = totallodgein;
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog(ex.ToString());
            }
        }

        public void MenuAccessSetup()
        {
            try
            {
                if (LoginUserAuthentication == "User")
                {
                    if (MMMasterVisisbility == "Visible" || MMMasterVisisbility == null)
                    {
                        MainWind.MasMenu.Visibility = Visibility.Collapsed;

                        MainWind.SysCompProfile.Visibility = Visibility.Collapsed;
                        MainWind.SysPostCode.Visibility = Visibility.Collapsed;
                        MainWind.SysQuotation.Visibility = Visibility.Collapsed;
                        MainWind.SysAWBAlloc.Visibility = Visibility.Collapsed;
                        MainWind.SysSystemSetting.Visibility = Visibility.Collapsed;
                        MainWind.SysGenMaster.Visibility = Visibility.Collapsed;
                        MainWind.SysSQLQueryWin.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    if (MMMasterVisisbility == "Collapsed" || MMMasterVisisbility == null)
                    {
                        MainWind.MasMenu.Visibility = Visibility.Visible;

                        MainWind.SysCompProfile.Visibility = Visibility.Visible;
                        MainWind.SysPostCode.Visibility = Visibility.Visible;
                        MainWind.SysQuotation.Visibility = Visibility.Visible;
                        MainWind.SysAWBAlloc.Visibility = Visibility.Visible;
                        MainWind.SysSystemSetting.Visibility = Visibility.Visible;
                        MainWind.SysGenMaster.Visibility = Visibility.Visible;
                        MainWind.SysSQLQueryWin.Visibility = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("Menu Access Setup" + ex.ToString());
            }
        }

        public static List<CSSUpgradeDto> GetCSSUpgradebyAPI()
        {
            List<CSSUpgradeDto> list = TrackingHTTPClient.Program.GetProductAsync(CSSUpgradeToken);

            return list;
        }

        public string GetValueasSTRING(string query, string columnname)
        {
            string res = "";
            using (SqlDataReader dr = GetDataReader(query))
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        res = dr[columnname] != DBNull.Value ? dr[columnname].ToString().Trim() : "";
                    }
                }
                dr.Close();
            }

            return res;
        }

        public int GetValueasInt(string query, string columnname)
        {
            int res = 0;

            using (SqlDataReader dr = GetDataReader(query))
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        string temp = dr[columnname] != DBNull.Value ? dr[columnname].ToString().Trim() : "";
                        res = Convert.ToInt32(!string.IsNullOrEmpty(temp) ? temp : "0");
                    }
                }
                dr.Close();
            }

            return res;
        }

        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";

            //var temp = UTF8Encoding.UTF8.GetBytes(cipherText);
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public bool IsNewVersion(tblCSSUpgrade cssupgrade)
        {
            string NewVersion = cssupgrade.VersionNo;
            string NewFeatures = cssupgrade.Features;

            //Version = "V4.0.0";
            //NewFeatures = "Changes at V4.0.0 :\n" +
            //    "1. ReleasedTo : CR3,CR4,CR6\n" +
            //    "2. Setting for CSS Version to ALL/EAST MALAYSIA/WEST MALAYSIA/Selected stations";

            StringReader strReader = new StringReader(NewFeatures);
            string ReleasesTo = strReader.ReadLine(); // First line (Changes at <version> :)
            ReleasesTo = strReader.ReadLine(); // Second line (1. ReleasedTo : <ALL/CR3,CR4,CR6>)

            if (ReleasesTo.Contains("ReleasedTo"))
                ReleasesTo = ReleasesTo.Split(':')[1].ToString().Trim();
            else
                ReleasesTo = "ALL";

            if (MyCompanyCode.Trim() == "TEST" || ReleasesTo == "ALL" || ReleasesTo.Contains(MyCompanyCode.Trim()) || (ReleasesTo == "EAST MALAYSIA" && (MyStateName == "SABAH" || MyStateName == "SARAWAK")) || (ReleasesTo == "WEST MALAYSIA" && (MyStateName != "SABAH" && MyStateName != "SARAWAK")))
            {
                var NewVer = Regex.Replace(NewVersion, @"\s", "");
                var OldVer = Regex.Replace(CurrentVersion, @"\s", "");

                var NewVerAry = NewVer.Split('.');
                var OldVerAry = OldVer.Split('.');

                int OldFirstNum = Convert.ToInt32(Regex.Replace(OldVerAry[0], @"[^\d]", ""));
                int NewFirstNum = Convert.ToInt32(Regex.Replace(NewVerAry[0], @"[^\d]", ""));
                int NewSeconddNum = (NewVerAry.ElementAtOrDefault(1) == null) ? 0 : Convert.ToInt32(NewVerAry[1]);
                int OldSecondNum = (OldVerAry.ElementAtOrDefault(1) == null) ? 0 : Convert.ToInt32(OldVerAry[1]);
                int NewThirdNum = (NewVerAry.ElementAtOrDefault(2) == null) ? 0 : Convert.ToInt32(NewVerAry[2]);
                int OldThirdNum = (OldVerAry.ElementAtOrDefault(2) == null) ? 0 : Convert.ToInt32(OldVerAry[2]);

                if (NewVerAry.Count() > 1 && NewFirstNum >= 4)
                {
                    if (OldFirstNum > 100)
                        return true;
                    else if (NewFirstNum > OldFirstNum)
                        return true;
                    else if ((NewFirstNum == OldFirstNum) && (NewSeconddNum > OldSecondNum))
                        return true;
                    else if ((OldSecondNum == NewSeconddNum) && (NewThirdNum > OldThirdNum))
                        return true;
                    else
                        return false;
                }
                else if (NewFirstNum > OldFirstNum)
                    return true;
                else return false;
            }
            else return false;
        }

        public int ExecuteQuery(string Query)
        {
            int res = 0;
            if (SqlCon.State == ConnectionState.Closed) SqlCon.Open();
            using (SqlCmd = new SqlCommand(Query, SqlCon))
            {
                res = SqlCmd.ExecuteNonQuery();
            }
            return res;
        }

        public int ExecuteScalarQuery(string Query)
        {
            int res = 0;
            if (SqlCon.State == ConnectionState.Closed) SqlCon.Open();
            using (SqlCmd = new SqlCommand(Query + "; SELECT SCOPE_IDENTITY();", SqlCon))
            {
                res = Convert.ToInt32(SqlCmd.ExecuteScalar());
            }
            return res;
        }

        public SqlDataReader GetDataReader(string Query)
        {
            if (SqlCon.State == ConnectionState.Closed) SqlCon.Open();
            SqlCmd = new SqlCommand(Query, SqlCon);
            return SqlCmd.ExecuteReader();
        }

        public DataTable GetDataTable(string Query)
        {
            if (SqlCon.State == ConnectionState.Closed) SqlCon.Open();
            using (SqlAdapter = new SqlDataAdapter(Query, SqlCon))
            {
                DataTable dt = new DataTable();
                SqlAdapter.Fill(dt);
                return dt;
            }
        }

        public static DataTable DataTableSort(DataTable dt, string colName, string direction)
        {
            dt.DefaultView.Sort = colName + " " + direction;
            dt = dt.DefaultView.ToTable();
            return dt;
        }

        public static string GetMacAddress()
        {
            string result = "NULL";
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    // Only consider Ethernet network interfaces
                    if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet && nic.OperationalStatus == OperationalStatus.Up)
                    {
                        result = nic.GetPhysicalAddress().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("GetMACAddress function:" + ex.ToString());
            }
            return result;
        }

        public static string GetIPAddress()
        {
            string result = "";
            try
            {
                string hostName = Dns.GetHostName();
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
                result = myIP;
            }
            catch (Exception ex)
            {
                error_log.errorlog("GetIPAddress function:" + ex.ToString());
            }
            return result;
        }

        public static string GetTeamviewerID()
        {
            string result = "";
            try
            {
                string regPath = Environment.Is64BitOperatingSystem ? @"SOFTWARE\Wow6432Node\TeamViewer" : @"SOFTWARE\TeamViewer";
                RegistryKey key = Registry.LocalMachine.OpenSubKey(regPath);
                if (key == null)
                    result = "";
                object clientId = key.GetValue("ClientID");
                if (clientId != null) //ver. 10
                    result = Convert.ToString(clientId);
                foreach (string subKeyName in key.GetSubKeyNames().Reverse()) //older versions
                {
                    clientId = key.OpenSubKey(subKeyName).GetValue("ClientID");
                    if (clientId != null)
                        result = Convert.ToString(clientId);
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("GetTeamviewerID function:" + ex.ToString());
            }
            return result;
        }

        public string StrAuthenticateType()
        {
            string result = "";
            try
            {
                SqlDataReader dr = GetDataReader($"SELECT Authentication FROM tblUserMaster WHERE UserId = {LoginUserId}");
                if (dr.Read())
                {
                    result = dr["Authentication"].ToString();
                }
                dr.Close();
                return result;
            }
            catch (Exception Ex) { error_log.errorlog("Authentication Type Check Function:" + Ex.ToString()); return result; }
        }

        public bool CheckValidDestStation(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                SqlDataReader dr = GetDataReader($"SELECT DestCode FROM tblDestination WHERE RTRIM(DestCode) = '{code.Trim()}' AND (IsDeleted IS NULL OR IsDeleted = 0)");
                if (dr.HasRows)
                {
                    dr.Close();
                    return true;
                }
            }
            return false;
        }

        public void CheckPrinter()
        {
            using (SqlDataReader dr = GetDataReader($"SELECT CNPrinterIsActive, StickerPrinterIsActive FROM tblWPConfig where PCNAME = HOST_NAME() AND Status = 1 order by CDate desc"))
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        //isCNPrinter = dr["CNPrinterIsActive"] == DBNull.Value ? false : Convert.ToBoolean(dr["CNPrinterIsActive"]);
                        isStickerPrinter = dr["StickerPrinterIsActive"] == DBNull.Value ? false : Convert.ToBoolean(dr["StickerPrinterIsActive"]);
                    }
                }
                dr.Close();
            }
        }

        public decimal GetRoundingVal(decimal Val, int NoOfDecimal)
        {
            decimal res = Val;
            try
            {
                using (DataTable dt = GetDataTable($"SELECT cast(round({Val}, {NoOfDecimal}) as numeric(36,{NoOfDecimal})) AS RetVal"))
                {
                    res = Convert.ToDecimal(dt.Rows[0]["RetVal"]);
                }
            }
            catch
            {
                res = Math.Round(Val, NoOfDecimal);
            }
            return res;
        }

        public void getTaxDetail()
        {
            SqlDataReader drStandard;
            drStandard = GetDataReader($"Select QuotationType, TaxTitle, TaxPercentage FROM tblquotation where QuotationType = 'Standard' and isdeleted = 0");
            if (drStandard.Read())
            {
                StandardQuotationType = drStandard["QuotationType"].ToString();
                StandardTaxCode = drStandard["TaxTitle"].ToString();
                StandardTaxRate = Convert.ToDecimal(drStandard["TaxPercentage"]);
            }

            SqlDataReader drIntl;
            drIntl = GetDataReader($"Select QuotationType, TaxTitle, SurCharge FROM tblquotation where QuotationType = 'International' and isdeleted = 0");
            if (drIntl.Read())
            {
                InternationalQuotationType = drIntl["QuotationType"].ToString();
                InternationalTaxCode = drIntl["TaxTitle"].ToString();
                InternationalTaxRate = Convert.ToDecimal(drIntl["SurCharge"]);
            }
        }

        public void CompAddress()
        {
            DTCompAddress = new DataTable();

            DTCompAddress.Columns.Add(new DataColumn("Name", typeof(string)));
            DTCompAddress.Columns.Add(new DataColumn("Address1", typeof(string)));
            DTCompAddress.Columns.Add(new DataColumn("Address2", typeof(string)));
            DTCompAddress.Columns.Add(new DataColumn("Address3", typeof(string)));
            DTCompAddress.Columns.Add(new DataColumn("Address4", typeof(string)));
            DTCompAddress.Columns.Add(new DataColumn("Address5", typeof(string)));

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id", typeof(string)));
            dt.Columns.Add(new DataColumn("Address", typeof(string)));

            int l1 = 0, l2 = 0, l = 0;
            string str1 = "", str2 = "";

            SqlDataReader sqldr = GetDataReader($"SELECT a.CompId, a.CompName, a.CompCode, a.CompType, a.RegNum, a.IATACode, a.HQCode, a.Address1, a.Address2, a.Address3, a.City, a.State, a.Country, a.ZipCode, a.PhoneNum, a.Fax, a.EMail FROM tblStationProfile a WHERE RTRIM(a.CompCode) = '{MyCompanyCode.Trim()}'");
            if (sqldr.HasRows)
            {
                sqldr.Read();
                DataRow dr = dt.NewRow(); l = 0; dr["Id"] = l; dr["Address"] = sqldr["CompName"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["Address1"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["Address2"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["Address3"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["City"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["State"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["Country"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["ZipCode"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["PhoneNum"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["Fax"].ToString(); dt.Rows.Add(dr);
                dr = dt.NewRow(); l += 1; dr["Id"] = l; dr["Address"] = sqldr["EMail"].ToString(); dt.Rows.Add(dr);
                sqldr.Close();

                DataRow DRCompAddress = DTCompAddress.NewRow();
                foreach (DataRow row in dt.Rows)
                {
                    if (row["Address"].ToString().Trim() != "")
                    {
                        if (l1 == 0 && Convert.ToInt32(row["Id"]) == 0)
                        {
                            DRCompAddress["Name"] = row["Address"].ToString();
                            l1 = +1;
                        }
                        else if (l1 == 1 && Convert.ToInt32(row["Id"]) > 0)
                        {
                            if (row["Address"].ToString() != " " && l2 == 0)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str1 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str1 = "Fax : " + row["Address"].ToString();
                                else str1 = row["Address"].ToString();
                                l2 = 1;
                            }
                            else if (row["Address"].ToString() != " " && l2 == 1)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str2 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str2 = "Fax : " + row["Address"].ToString();
                                else str2 = row["Address"].ToString();
                                DRCompAddress["Address1"] = str1 + ", " + str2;
                                l1 = 2; l2 = 0;
                            }
                        }
                        else if (l1 == 2 && Convert.ToInt32(row["Id"]) > 0)
                        {
                            if (row["Address"].ToString() != " " && l2 == 0)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str1 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str1 = "Fax : " + row["Address"].ToString();
                                else str1 = row["Address"].ToString();
                                l2 = 1;
                            }
                            else if (row["Address"].ToString() != " " && l2 == 1)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str2 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str2 = "Fax : " + row["Address"].ToString();
                                else str2 = row["Address"].ToString();
                                DRCompAddress["Address2"] = str1 + ", " + str2;
                                l1 = 3; l2 = 0;
                            }
                        }

                        else if (l1 == 3 && Convert.ToInt32(row["Id"]) > 0)
                        {
                            if (row["Address"].ToString() != " " && l2 == 0)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str1 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str1 = "Fax : " + row["Address"].ToString();
                                else str1 = row["Address"].ToString();
                                l2 = 1;
                            }
                            else if (row["Address"].ToString() != " " && l2 == 1)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str2 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str2 = "Fax : " + row["Address"].ToString();
                                else str2 = row["Address"].ToString();
                                DRCompAddress["Address3"] = str1 + ", " + str2;
                                l1 = 4; l2 = 0;
                            }
                        }
                        else if (l1 == 4 && Convert.ToInt32(row["Id"]) > 0)
                        {
                            if (row["Address"].ToString() != " " && l2 == 0)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str1 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str1 = "Fax : " + row["Address"].ToString();
                                else str1 = row["Address"].ToString();
                                l2 = 1;
                            }
                            else if (row["Address"].ToString() != " " && l2 == 1)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str2 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str2 = "Fax : " + row["Address"].ToString();
                                else str2 = row["Address"].ToString();
                                DRCompAddress["Address4"] = str1 + ", " + str2;
                                l1 = 5; l2 = 0;
                            }
                        }
                        else if (l1 == 5 && Convert.ToInt32(row["Id"]) > 0)
                        {
                            if (row["Address"].ToString() != " " && l2 == 0)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str1 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str1 = "Fax : " + row["Address"].ToString();
                                str1 = row["Address"].ToString();
                                l2 = 1;
                            }
                            else if (row["Address"].ToString() != " " && l2 == 1)
                            {
                                if (Convert.ToInt32(row["Id"]) == 8) str2 = "Phone : " + row["Address"].ToString();
                                else if (Convert.ToInt32(row["Id"]) == 9) str2 = "Fax : " + row["Address"].ToString();
                                else str2 = row["Address"].ToString();

                                DRCompAddress["Address5"] = str1 + ", " + str2;
                                l1 = 6; l2 = 0;
                            }
                        }
                    }
                }
                DTCompAddress.Rows.Add(DRCompAddress);
            }
            sqldr.Close();
        }

        private int _SelectedIndex;
        public int SelectedIndex { get { return _SelectedIndex; } set { _SelectedIndex = value; OnPropertyChanged("SelectedIndex"); } }

        private Object _CurrentItem;
        public Object CurrentItem { get { return _CurrentItem; } set { _CurrentItem = value; OnPropertyChanged("CurrentItem"); } }

        private DataGridCellInfo _CurrentCell;
        public DataGridCellInfo CurrentCell { get { return _CurrentCell; } set { _CurrentCell = value; OnPropertyChanged("CurrentCell"); } }

        private DataGridColumn _CurrentColumn;
        public DataGridColumn CurrentColumn { get { return _CurrentColumn; } set { _CurrentColumn = value; OnPropertyChanged("CurrentColumn"); } }

        public void FocusMyGrid(DataGrid FocusDGrid, int FocusRow, int FocusCol)
        {
            SelectedIndex = FocusRow; CurrentItem = FocusDGrid.Items[FocusRow]; CurrentColumn = FocusDGrid.Columns[FocusCol];
            FocusDGrid.Focus(); FocusDGrid.BeginEdit();
        }

        private ICommand _FocusNext, _EscapeCommand, _FocusDataGrid;
        public ICommand FocusNext { get { _FocusNext = new RelayCommand(param => FocusNextControl()); return _FocusNext; } }
        public ICommand EscapeCommand { get { if (_EscapeCommand == null) _EscapeCommand = new RelayCommand(Parameter => CloseWind()); return _EscapeCommand; } }
        public ICommand FocusDataGrid { get { if (_FocusDataGrid == null) _FocusDataGrid = new RelayCommand(Parameter => DataGridFocus(Parameter)); return _FocusDataGrid; } }

        public void DataGridFocus(object Obj)
        {
            var MyObj = Obj as DataGrid;

            SelectedIndex = MyObj.Items.Count - 1; CurrentItem = MyObj.Items[MyObj.Items.Count - 1]; CurrentColumn = MyObj.Columns[0];
            MyObj.Focus(); MyObj.BeginEdit();
        }

        public virtual void FocusNextControl()
        {
            UIElement focusedElement = Keyboard.FocusedElement as UIElement;
            if (focusedElement != null)
            {
                focusedElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));

                UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;

                var tb = elementWithFocus as TextBox;
                if (tb != null)
                {
                    tb.SelectAll();
                }
            }
        }

        public String SecureStringToString(SecureString value)
        {
            IntPtr bstr = Marshal.SecureStringToBSTR(value);
            try { return Marshal.PtrToStringBSTR(bstr); }
            finally { Marshal.FreeBSTR(bstr); }
        }

        public SecureString ToSecureString(string Source)
        {
            if (string.IsNullOrWhiteSpace(Source)) return null;
            else
            {
                SecureString Result = new SecureString();
                foreach (char c in Source.ToCharArray())
                    Result.AppendChar(c);
                return Result;
            }
        }

        public static string[] GetFileListFTP(string ftphost, string ftpfilepath, string ftpuser, string ftppassword)
        {
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            WebResponse response = null;
            StreamReader reader = null;
            try
            {
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftphost + "/" + ftpfilepath + "/"));
                reqFTP.Proxy = null;
                reqFTP.Credentials = new NetworkCredential(ftpuser, ftppassword);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                reqFTP.UseBinary = true;
                reqFTP.UsePassive = true;
                reqFTP.KeepAlive = false;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                error_log.errorlog("ReadLine:" + line);

                while (line != null)
                {
                    string[] ListDetails = line.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string ListDetail in ListDetails)
                    {
                        if (ListDetail.StartsWith("d") && (!ListDetail.EndsWith(".")))
                        {
                            string FtpDirName = ListDetail.Substring(ListDetail.IndexOf(':') + 3).TrimStart();
                            result.Append(FtpDirName); result.Append("\n");
                        }
                        else if (ListDetail.StartsWith("-"))
                        {
                            string FtpDirName = ListDetail.Substring(ListDetail.IndexOf(':') + 3).TrimStart();
                            result.Append(FtpDirName); result.Append("\n");
                        }
                    }
                    line = reader.ReadLine();
                }

                //while (line != null)
                //{
                //    result.Append(line);
                //    result.Append("\n");
                //    line = reader.ReadLine();
                //}
                // to remove the trailing '\n'
                int index = result.ToString().LastIndexOf('\n');
                if (index >= 0)
                {
                    result.Remove(index, 1);
                    return result.ToString().Split('\n');
                }
                else { return result.ToString().Split('\n'); }
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                downloadFiles = null;
                error_log.errorlog("FTP file list error:" + ex.ToString());
                return downloadFiles;
            }
        }

        public static void DownloadFilefromFTP(string[] List, string localpath, string ftppath)
        {
            try
            {
                error_log.errorlog("Autoupdate inside:ftppath =" + ftppath + ", localpath:" + localpath);
                if (List.Length > 1)
                {
                    string Directories = "";
                    for (int i = 0; i < List.Length; i++)
                    {
                        if (List[i] == "" || List[i] == "." || List[i] == ".." || List[i] == "..." || List[i] == "....") { }
                        else
                        {
                            string a = List[i];
                            if (a.Split('.').Length > 1)
                            {
                                DownloadFTP(FTPHost, ftppath, FTPUser, FTPPassword, localpath, a);
                            }
                            else
                            {
                                string Paths = ftppath + @"/" + a;
                                string[] ftpavailablelist = GetFileListFTP(FTPHost, Paths, FTPUser, FTPPassword);
                                if (ftpavailablelist.Length > 0)
                                {
                                    for (int j = 0; j < ftpavailablelist.Length; j++)
                                    {
                                        if (ftpavailablelist[j] == "" || ftpavailablelist[j] == "." || ftpavailablelist[j] == ".." || ftpavailablelist[j] == "..." || ftpavailablelist[j] == "....") { }
                                        else
                                        {
                                            string a1 = ftpavailablelist[j];
                                            if (a1.Split('.').Length > 1)
                                            {
                                                DownloadFTP(FTPHost, Paths, FTPUser, FTPPassword, localpath + @"\\" + a, a1);
                                            }
                                            else
                                            {
                                                Directories = Directories + @"%/" + a + @"/" + a1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    DownloadFilefromFTP(Directories.Split('%'), localpath, ftppath);
                }
            }
            catch (Exception Ex) { SkynetCashSales.General.error_log.errorlog("Auto Update DownloadFromFile : " + Ex.ToString()); }
        }

        public static bool DownloadFTP(string ftphost, string ftpfilepath, string ftpuser, string ftppassword, string localpath, string file)
        {
            try
            {
                if (!string.IsNullOrEmpty(ftphost) && !string.IsNullOrEmpty(ftpfilepath) && !string.IsNullOrEmpty(ftpuser) && !string.IsNullOrEmpty(ftppassword) && !string.IsNullOrEmpty(localpath) && !string.IsNullOrEmpty(file))
                {
                    if (!Directory.Exists(localpath)) { SkynetCashSales.General.error_log.CreateDirectory(localpath); }
                    string uri = "ftp://" + ftphost + "/" + ftpfilepath + "/" + file;
                    Uri serverUri = new Uri(uri);
                    if (serverUri.Scheme != Uri.UriSchemeFtp)
                    {
                        return false;
                    }
                    FtpWebRequest reqFTP;
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftphost + "/" + ftpfilepath + "/" + file));
                    reqFTP.Credentials = new NetworkCredential(ftpuser, ftppassword);
                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.Proxy = null;
                    reqFTP.UsePassive = false;
                    FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    FileStream writeStream = new FileStream(localpath + "\\" + file, FileMode.Create);
                    int Length = 2048;
                    Byte[] buffer = new Byte[Length];
                    int bytesRead = responseStream.Read(buffer, 0, Length);
                    while (bytesRead > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                        bytesRead = responseStream.Read(buffer, 0, Length);
                    }
                    writeStream.Close();
                    response.Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (WebException wEx)
            {
                SkynetCashSales.General.error_log.errorlog("Autoupdate Download Error : " + wEx.Message); return false;
            }
            catch (Exception ex)
            {
                SkynetCashSales.General.error_log.errorlog("Autoupdate Download Error : " + ex.Message); return false;
            }
        }

        public static bool FTPFileExistence(string ftphost, string ftpfilepath, string ftpuser, string ftppassword, string filename)
        {
            string ftpfullpathoffile = "ftp://" + ftphost + "/" + ftpfilepath + filename;
            var request = (FtpWebRequest)WebRequest.Create(ftpfullpathoffile);
            request.Credentials = new NetworkCredential(ftpuser, ftppassword);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false;
                }
                return false;
            }
        }

        public static string VerifyLatestFTP(string ftpbasicpath)
        {
            try
            {
                int l = 0;
                string[] files = GetFileListFTP(FTPHost, ftpbasicpath, FTPUser, FTPPassword);
                if (files != null)
                {
                    if (files.Length > 0)
                    {
                        foreach (string file in files)
                        {
                            if (file.IsInteger())
                            {
                                if (l == 0) { l = Convert.ToInt32(file); }
                                else { l = l >= Convert.ToInt32(file) ? l : Convert.ToInt32(file); }
                            }
                        }
                        FTPFinalFileDirectory = l.ToString();
                    }
                }
                return l == 0 ? "" : l.ToString();
            }
            catch (Exception Ex) { SkynetCashSales.General.error_log.errorlog("Auto Update verify latest : " + Ex.ToString()); return ""; }
        }

        public static string VerifyLatestFTPByStation()
        {
            try
            {
                int l = 0;
                string substr = MyCompanySubName == null || MyCompanySubName == "" ? "" : "/" + MyCompanySubName;
                string filename = MyCompanyCode.Trim() + substr;
                string[] files = GetFileListFTP(FTPHost, FTPFilePath + "/DotNet40/" + filename.Trim(), FTPUser, FTPPassword);
                if (files != null)
                {
                    if (files.Length > 0)
                    {
                        foreach (string file in files)
                        {
                            if (file.IsInteger())
                            {
                                if (l == 0) { l = Convert.ToInt32(file); }
                                else { l = l >= Convert.ToInt32(file) ? l : Convert.ToInt32(file); }
                            }
                        }
                        FTPFinalFileDirectory = l.ToString();
                    }
                }
                return l == 0 ? "" : l.ToString();
            }
            catch (Exception Ex) { SkynetCashSales.General.error_log.errorlog("Auto Update verify latest : " + Ex.ToString()); return ""; }
        }

        public void CloseWind()
        {
            foreach (System.Windows.Window window in System.Windows.Application.Current.Windows)
            {
                if (window.DataContext == this) window.Close();
            }
        }

        public string NumberToWords(string rawnumber)
        {
            int inputNum = 0;
            int dig1, dig2, dig3, level = 0, lasttwo, threeDigits;
            string dollars, cents;
            try
            {
                string[] Splits = new string[2];
                Splits = rawnumber.Split('.');   //notice that it is ' and not "
                inputNum = Convert.ToInt32(Splits[0]);

                //get inputNum as an int

                //dollars = Convert.ToString(inputNum);
                dollars = "";
                cents = Splits[1];
                if (cents.Length == 1)
                {
                    cents += "0";   // 12.5 is twelve and 50/100, not twelve and 5/100
                }
            }
            catch
            {
                cents = "00";
                inputNum = Convert.ToInt32(rawnumber);
                dollars = "";
                //dollars = Convert.ToString(rawnumber);
            }

            string x = "";

            //they had zero for ones and tens but that gave ninety zero for 90
            string[] ones = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] tens = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
            string[] thou = { "", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion" };

            bool isNegative = false;
            if (inputNum < 0)
            {
                isNegative = true;
                inputNum *= -1;
            }
            if (inputNum == 0)
            {
                return "zero and " + cents + "/100";
            }

            string s = inputNum.ToString();

            //for (int t = 0; t < 5; t++)
            while (s.Length > 0)
            {
                if (s.Length > 0)
                {
                    //Get the three rightmost characters
                    x = (s.Length < 3) ? s : s.Substring(s.Length - 3, 3);

                    // Separate the three digits
                    threeDigits = int.Parse(x);
                    lasttwo = threeDigits % 100;
                    dig1 = threeDigits / 100;
                    dig2 = lasttwo / 10;
                    dig3 = (threeDigits % 10);


                    // append a "thousand" where appropriate
                    if (level > 0 && dig1 + dig2 + dig3 > 0)
                    {
                        dollars = thou[level] + " " + dollars;
                        dollars = dollars.Trim();
                    }

                    // check that the last two digits is not a zero
                    if (lasttwo > 0)
                    {
                        if (lasttwo < 20)
                        {
                            // if less than 20, use "ones" only
                            dollars = ones[lasttwo] + " " + dollars;
                        }
                        else
                        {
                            // otherwise, use both "tens" and "ones" array
                            dollars = tens[dig2] + " " + ones[dig3] + " " + dollars;
                        }
                        if (s.Length < 3)
                        {
                            if (isNegative) { dollars = "negative " + dollars; }
                            return dollars + " and " + cents + "/100";
                        }
                    }

                    // if a hundreds part is there, translate it
                    if (dig1 > 0)
                    {
                        dollars = ones[dig1] + " hundred " + dollars;
                        s = (s.Length - 3) > 0 ? s.Substring(0, s.Length - 3) : "";
                        level++;
                    }
                }
            }
            if (isNegative) { dollars = "negative " + dollars; }

            if (cents.Trim() != "")
            {
                if (cents.Length > 1)
                {
                    cents = tens[Convert.ToInt32(cents.Substring(0, 1))] + " " + ones[Convert.ToInt32(cents.Substring(1, 1))];
                }
                else
                {
                    cents = ones[Convert.ToInt32(cents.Substring(1, 1))];
                }
            }

            return dollars + (cents.Trim() != "" ? " and " + cents + " cents" : "");
        }

        public void UerAccess()
        {
            MessageBoxResult MsgConfirmation = System.Windows.MessageBox.Show("You don't have permission to Access.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public DataTable IntlCountries()
        {
            DataTable RetDt = new DataTable();
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);

                if (reply.Status == IPStatus.Success)
                {
                    HttpClient client = new HttpClient();
                    var uri = "http://web-api.skynet.com.my/api/country/document/countries";
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMmUzMzcwNzNiMmQ2NjExZjZjNWM0YjNkMDkwNzI1YWIxMWVlZmFlMTJhOTJjZjE4MzlkNDhkYzE1MTY0NTFmY2I5YTY3NTY1ZWYxYTUwZWMiLCJpYXQiOjE2MTAzMjgxNTYsIm5iZiI6MTYxMDMyODE1NiwiZXhwIjoxNjQxODY0MTU2LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.pQWFb-B7xG91A5spmkPTHst6AZJpnds95ClwopdreCSPyhCJuNESOPrum3WKN3gN6fs2YGUsXCCHdU4upxzUx0tG03cUxCYEMCVmRjGR8eyqnqyGl55j_Wop-Y739bNLw_MMKb14N-C7jWZVNsiFlSUjjqdLUT1lbXhcJ-nw-xlyE2Av6_Zu6bfeMwyMaqe5mn588tD-Ov4VHp-GTZY_M92uJdxXXJx9JcLO4TJHcd1-lTPWfq_EXOEJzM0WovGTT0N-INTC2MhWJZhG0aKMgO36tZ5zMMELnL25rO48oZVXQodpcajfLS4Q_VmpahxNkjBv6Ya7PFe9vTR0oWZh0Rx0Cdqu3e0NFpYRe_LPokBiYYGhCGh5r3WrWK_DFyMxoeo80v4dIuywMx57Axh9IHPSDoIgnctrgH7PisdQxHTF9H6nZRGyCpfZBf8HGaTRoTMvTpjb6hjS2b7QuTT9hBSDSAlkD4CzAemoalVtEIAv3xuzScd90e07g82-ffOBRi_oO8rfq4zdB70HZkti7DahcHt5QTA5NHP-VRMzsyUo6uQ7hapDLZXaXYx6snt99ppUa8W5lHsGHQfI39O8G1-7tukQDXHn60EQYqjqWMludzpr_O_AflQoZDnvf5iBaDN4eUcaHotRNiltaS2hp0Ls97JRqalSAX9l4KXz9-I");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(uri).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var res = response.Content.ReadAsStringAsync().Result;
                        if (!res.Contains("Parameter is missing") && !res.Contains("Missing param or value ()") && !res.Contains("No country found"))
                        {
                            RetDt = JsonConvert.DeserializeObject<DataTable>(res);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Rate system is offline. Please check your internet connection or contact SCS Support", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception Ex) { }
            return RetDt;
        }

    }

    public class MoveFocusEventArgs : EventArgs
    {
        public MoveFocusEventArgs(string focusedProperty)
        {
            this.FocusedProperty = focusedProperty;
        }

        public string FocusedProperty { get; private set; }
    }

    public class DecimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return ((decimal)value).ToString("#,##0;(#,##0)");

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }


    //26.09.2014 for visibility and in-visibility start
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        public bool IsReversed { get; set; }
        public bool UseHidden { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = System.Convert.ToBoolean(value, CultureInfo.InvariantCulture);
            if (this.IsReversed)
            {
                val = !val;
            }
            if (val)
            {
                return Visibility.Visible;
            }
            return this.UseHidden ? Visibility.Hidden : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    //26.09.2014 for visibility and in-visibility end

    //Integer Check method start 
    public static class Check
    {
        public static bool IsInteger(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return false;

            int i;
            return Int32.TryParse(s, out i);
        }
        public static bool IsLong(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return false;

            long i;
            return Int64.TryParse(s, out i);
        }
    }
    //Integer Check method end 

    //Digit textbox method start on 02102014
    public class DigitBox : System.Windows.Controls.TextBox
    {
        #region Constructors
        /// <summary>
        /// The default constructor
        /// </summary>
        public DigitBox()
        {
            TextChanged += new TextChangedEventHandler(OnTextChanged);
            KeyDown += new System.Windows.Input.KeyEventHandler(OnKeyDown);
        }
        #endregion

        #region Properties
        new public String Text
        {
            get { return base.Text; }
            set
            {
                base.Text = LeaveOnlyNumbers(value);
            }
        }

        #endregion

        #region Functions
        private bool IsNumberKey(Key inKey)
        {
            if (inKey < Key.D0 || inKey > Key.D9)
            {
                if (inKey < Key.NumPad0 || inKey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsActionKey(Key inKey)
        {
            return inKey == Key.Delete || inKey == Key.Back || inKey == Key.Tab || inKey == Key.Return || Keyboard.Modifiers.HasFlag(ModifierKeys.Alt);
        }

        private string LeaveOnlyNumbers(String inString)
        {
            String tmp = inString;
            foreach (char c in inString.ToCharArray())
            {
                if (!IsDigit(c))
                {
                    tmp = tmp.Replace(c.ToString(), "");
                }
            }
            return tmp;
        }

        public bool IsDigit(char c)
        {
            return (c >= '0' && c <= '9');
        }
        #endregion

        #region Event Functions
        protected void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key) && !IsActionKey(e.Key);
        }

        protected void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            base.Text = LeaveOnlyNumbers(Text);
        }
        #endregion
    }
    //Digit textbox method end

    //Decimal textbox method start on 02102014
    public class DecimalTextBox : System.Windows.Controls.TextBox
    {
        #region Constructors
        /// <summary>
        /// The default constructor
        /// </summary>
        private static Key keyinput;
        private static char ch;
        public DecimalTextBox()
        {
            TextChanged += new TextChangedEventHandler(OnTextChanged);
            KeyDown += new System.Windows.Input.KeyEventHandler(OnKeyDown);
        }
        #endregion

        #region Properties
        new public String Text
        {
            get { return base.Text; }
            set
            {
                base.Text = LeaveOnlyNumbers(value);
            }
        }

        #endregion

        #region Functions
        private bool IsNumberKey(Key inKey)
        {
            if ((inKey < Key.D0 || inKey > Key.D9) && inKey != Key.OemPeriod)
            {
                if (inKey < Key.NumPad0 || inKey > Key.NumPad9)
                {
                    if (inKey != Key.Decimal)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool IsActionKey(Key inKey)
        {
            return inKey == Key.Delete || inKey == Key.Back || inKey == Key.Tab || inKey == Key.Return || Keyboard.Modifiers.HasFlag(ModifierKeys.Alt);
        }

        private string LeaveOnlyNumbers(String inString)
        {
            decimal i; bool Result;
            String tmp = inString;
            Result = decimal.TryParse(tmp, out i);
            if (Result == true)
            {
                foreach (char c in inString.ToCharArray())
                {
                    if (!IsDigit(c))
                    {
                        if (c != '.')
                        {
                            tmp = tmp.Replace(c.ToString(), "");
                        }
                    }
                }
            }
            else
            {
                int check = 0;
                for (int j = 0; j < inString.Length; j++)
                {
                    char ch = inString[j];
                    if (!IsDigit(ch))
                    {
                        if (ch == '.' && check == 0)
                        {
                            check = check + 1;
                        }
                        else
                        {
                            tmp = tmp.Remove(j);
                        }
                    }
                }
            }
            return tmp;
        }

        public bool IsDigit(char c)
        {
            return (c >= '0' && c <= '9');
        }
        #endregion

        #region Event Functions
        protected void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key) && !IsActionKey(e.Key);
            keyinput = e.Key;

            int ScanCode = (int)e.Key;
            ch = Convert.ToChar(ScanCode);
        }
        protected void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            base.Text = LeaveOnlyNumbers(Text);
            base.SelectionStart = base.Text.Length;
        }
        #endregion
    }
    //Decimal textbox method end

    //Numeric text method start
    public class NumericTextBox : System.Windows.Controls.TextBox
    {
        static NumericTextBox()
        {
            EventManager.RegisterClassHandler(
                typeof(NumericTextBox),
                System.Windows.DataObject.PastingEvent,
                (DataObjectPastingEventHandler)((sender, e) =>
                {
                    if (!IsDataValid(e.DataObject))
                    {
                        System.Windows.DataObject data = new System.Windows.DataObject();
                        data.SetText(String.Empty);
                        e.DataObject = data;
                        e.Handled = false;
                    }
                }));
        }

        protected override void OnDrop(System.Windows.DragEventArgs e)
        {
            e.Handled = !IsDataValid(e.Data);
            base.OnDrop(e);
        }

        protected override void OnDragOver(System.Windows.DragEventArgs e)
        {
            if (!IsDataValid(e.Data))
            {
                e.Handled = true;
                e.Effects = System.Windows.DragDropEffects.None;
            }

            base.OnDragEnter(e);
        }

        private static Boolean IsDataValid(System.Windows.IDataObject data)
        {
            Boolean isValid = false;
            if (data != null)
            {
                String text = data.GetData(System.Windows.DataFormats.Text) as String;
                if (!String.IsNullOrEmpty(text == null ? null : text.Trim()))
                {
                    decimal result = -1;
                    if (decimal.TryParse(text, out result))
                    {
                        if (result > 0)
                        {
                            isValid = true;
                        }
                    }
                }
            }

            return isValid;
        }
        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                if (e.Key != Key.Decimal)
                {
                    if (e.Key < Key.NumPad0 || e.Key > Key.NumPad9)
                    {
                        if (e.Key != Key.Back)
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
        }
    }
    //Numeric text method end   

    public class AMGeneral : AMGenFunction
    {
        public AMGeneral()
        {

        }
        public static string GetPCName()
        {
            return Dns.GetHostName();
        }
        public static string GetMachineIP()
        {
            string hostName = Dns.GetHostName();
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
        }
        public static string GetMacAddress()
        {
            string macAddresses = string.Empty;
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return macAddresses;
        }

        public static string GetTeamviewerID()
        {
            foreach (var path in new[] { "SOFTWARE\\TeamViewer", "SOFTWARE\\Wow6432Node\\TeamViewer" })
            {
                if (Registry.LocalMachine.OpenSubKey(path) != null)
                {
                    var subKey = path;// string.Format("{0}\\Version{1}", path, version);
                    if (Registry.LocalMachine.OpenSubKey(subKey) != null)
                    {
                        var clientID = Registry.LocalMachine.OpenSubKey(subKey).GetValue("ClientID");
                        if (clientID != null) //found it?
                        {
                            return Convert.ToInt32(clientID).ToString();
                        }
                    }
                }
            }
            //Not found, return an empty string
            return string.Empty;
        }

        public static string GetTeamviewerVersion()
        {
            foreach (var path in new[] { "SOFTWARE\\TeamViewer", "SOFTWARE\\Wow6432Node\\TeamViewer" })
            {
                if (Registry.LocalMachine.OpenSubKey(path) != null)
                {
                    var subKey = path;// string.Format("{0}\\Version{1}", path, version);
                    if (Registry.LocalMachine.OpenSubKey(subKey) != null)
                    {
                        var Version = Registry.LocalMachine.OpenSubKey(subKey).GetValue("Version");
                        if (Version != null) //found it?
                        {
                            return Version.ToString();
                        }
                    }
                }
            }
            //Not found, return an empty string
            return string.Empty;
        }

        public static bool ExecutingSQLFile(string FilewithPath, string server, string DBName, string loginId, string password)
        {
            string conStr = "Data Source=" + server + ";Initial Catalog=" + DBName + ";";
            if (!string.IsNullOrEmpty(loginId) && !string.IsNullOrEmpty(password))
            {
                conStr = "Data Source=" + server + ";Initial Catalog=" + DBName + ";User ID=" + loginId + ";Password=" + password + ";";
            }
            try
            {
                var fileContent = File.ReadAllText(FilewithPath);
                var sqlqueries = fileContent.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries);

                var con = new SqlConnection(conStr);
                var cmd = new SqlCommand("query", con);
                con.Open();
                foreach (var query in sqlqueries)
                {
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                con.Close();
                return true;
            }
            catch (SqlException er)
            {
                error_log.errorlog("SQL Query file execution Failed : " + er.Message); return false;
            }
            finally { }
        }

        private static string SQLFileExecute(string credentials, string scriptDir, string scriptFilename)
        {
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WorkingDirectory = scriptDir;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.FileName = "sqlplus";
            process.StartInfo.Arguments = string.Format("{0} @{1}", credentials, scriptFilename);
            process.StartInfo.CreateNoWindow = true;

            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return output;
        }
    }

    public class UserActivities : AMGenFunction
    {
        public UserActivities()
        {

        }
        public static void UserEvents(string Title, string Description)
        {
            try
            {
                string pcname = AMGeneral.GetPCName();
                using (SqlConnection Con = new SqlConnection(ConStr))
                {
                    Con.Open();
                    using (SqlCommand Cmd = new SqlCommand($"INSERT INTO tblDailyLog (StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported)" +
                    $" VALUES('{MyCompanyCode.Trim()}', '{Title}', '{Description}', '{pcname}', '{LoginUserName}', {LoginUserId}, GETDATE(), 0, 0)", Con))
                    {
                        Cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                error_log.errorlog("UserEvents function:" + ex.ToString());
            }
        }

        public static void UserWindowEvent(string category)
        {
            try
            {
                string queryCMD = "";
                string pcname = AMGeneral.GetPCName(), ip = AMGeneral.GetMachineIP(), mac = AMGeneral.GetMacAddress(), tvid = AMGeneral.GetTeamviewerID(), tvversion = AMGeneral.GetTeamviewerVersion();


                using (SqlConnection Con = new SqlConnection(ConStr))
                {
                    Con.Open();

                    using (SqlCommand Cmd = new SqlCommand($"SELECT * FROM tblConnectedPCLog WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'", Con))
                    {
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                            if (dr.HasRows)
                            {
                                if (IDAutoUpdate == 1)
                                    queryCMD = $"UPDATE tblConnectedPCLog SET IsActive = 0, IsAutoUpdate = 1, IsExported = 0, lastUpdatedDate = UpdatedDate, UpdatedDate = GETDATE() WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'";
                                else
                                    queryCMD = $"UPDATE tblConnectedPCLog SET IsActive = 0 WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'";
                            }
                            else
                            {
                                if (IDAutoUpdate == 1)
                                    queryCMD = $"INSERT INTO tblConnectedPCLog (StnCode, PCName, IPAddress, MacAddress, TeamviewerID, TeamviewerVersion, IsActive, CreatedDate, IsAutoUpdate, UpdatedDate, lastUpdatedDate, IsDeleted, IsExported)" +
                                        $" VALUES('{MyCompanyCode.Trim()}', '{pcname}', '{ip}', '{mac}', '{tvid}', '{tvversion}', 1, GETDATE(), 1, GETDATE(), GETDATE(), 0, 0)";
                                else
                                    queryCMD = $"INSERT INTO tblConnectedPCLog (StnCode, PCName, IPAddress, MacAddress, TeamviewerID, TeamviewerVersion, IsActive, CreatedDate, IsAutoUpdate, UpdatedDate, lastUpdatedDate, IsDeleted, IsExported)" +
                                        $" VALUES('{MyCompanyCode.Trim()}', '{pcname}', '{ip}', '{mac}', '{tvid}', '{tvversion}', 1, GETDATE(), 0, NULL, NULL, 0, 0)";
                            }
                    }

                    using (SqlCommand Cmd = new SqlCommand(queryCMD, Con))
                    {
                        Cmd.ExecuteNonQuery();
                    }
                }
                GC.Collect();
            }
            catch (Exception ex)
            {
                error_log.errorlog("Exit function:" + ex.ToString());
            }
        }

        public static void UserExitEvent()
        {
            try
            {
                string queryCMD = "";

                string pcname = AMGeneral.GetPCName(), ip = AMGeneral.GetMachineIP(), mac = AMGeneral.GetMacAddress(), tvid = AMGeneral.GetTeamviewerID(), tvversion = AMGeneral.GetTeamviewerVersion();

                using (SqlConnection Con = new SqlConnection(ConStr))
                {
                    Con.Open();

                    using (SqlCommand Cmd = new SqlCommand($"SELECT * FROM tblConnectedPCLog WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'", Con))
                    {
                        using (SqlDataReader dr = Cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                if (IDAutoUpdate == 1)
                                    queryCMD = $"UPDATE tblConnectedPCLog SET IsActive = 0, IsAutoUpdate = 1, IsExported = 0, lastUpdatedDate = UpdatedDate, UpdatedDate = GETDATE() WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'";
                                else
                                    queryCMD = $"UPDATE tblConnectedPCLog SET IsActive = 0 WHERE PCName = '{pcname}' AND IPAddress = '{ip}' AND TeamviewerID = '{tvid}'";
                            }
                            else
                            {
                                if (IDAutoUpdate == 1)
                                    queryCMD = $"INSERT INTO tblConnectedPCLog (StnCode, PCName, IPAddress, MacAddress, TeamviewerID, TeamviewerVersion, IsActive, CreatedDate, IsAutoUpdate, UpdatedDate, lastUpdatedDate, IsDeleted, IsExported)" +
                                        $" VALUES('{MyCompanyCode.Trim()}', '{pcname}', '{ip}', '{mac}', '{tvid}', '{tvversion}', 1, GETDATE(), 1, GETDATE(), GETDATE(), 0, 0)";
                                else
                                    queryCMD = $"INSERT INTO tblConnectedPCLog (StnCode, PCName, IPAddress, MacAddress, TeamviewerID, TeamviewerVersion, IsActive, CreatedDate, IsAutoUpdate, UpdatedDate, lastUpdatedDate, IsDeleted, IsExported)" +
                                        $" VALUES('{MyCompanyCode.Trim()}', '{pcname}', '{ip}', '{mac}', '{tvid}', '{tvversion}', 1, GETDATE(), 0, NULL, NULL, 0, 0)";
                            }
                        }

                    }
                    using (SqlCommand Cmd = new SqlCommand(queryCMD, Con))
                    {
                        Cmd.ExecuteNonQuery();
                    }
                }

                UserEvents("EXIT", "Exit from the Cash Sales System");
            }
            catch (Exception ex)
            { error_log.errorlog("Exit function:" + ex.ToString()); }
        }

    }

    public partial class tblCSSUpgrade
    {
        public int UGID { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public string VersionNo { get; set; }
        public string FileName { get; set; }
        public bool IsCumpolsory { get; set; }
        public bool IsUpdated { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string Features { get; set; }
        public string Ftp_URL { get; set; }
        public string Ftp_UserName { get; set; }
        public string Ftp_Password { get; set; }
    }

    public class PaymentModeUpdtModel : PropertyChangedNotification
    {
        private int _paymentid;
        public int PaymentId
        {
            get { return _paymentid; }
            set { _paymentid = value; OnPropertyChanged("PaymentId"); }
        }
        private string _paymentcode;
        public string PaymentCode
        {
            get { return _paymentcode; }
            set { _paymentcode = value; OnPropertyChanged("PaymentCode"); }
        }
        private string _paymentdesc;
        public string PaymentDesc
        {
            get { return _paymentdesc; }
            set { _paymentdesc = value; OnPropertyChanged("PaymentDesc"); }
        }
        private string _isActive;
        public string IsActive
        {
            get { return _isActive; }
            set { _isActive = value; OnPropertyChanged("PaymentDesc"); }
        }

    }
}