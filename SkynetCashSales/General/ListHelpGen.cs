﻿using SkynetCashSales.View.Help;
using SkynetCashSales.View.Transactions;
using SkynetCashSales.ViewModel.Help;
using SkynetCashSales.ViewModel.Transactions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;

namespace SkynetCashSales.General
{
    public class ListHelpGen : AMGenFunction
    {
        public event Action<GetCommentVM.CommentInfo> FrmGetComment_Closed;
        
        public void SingleColWindShow(DataTable DT, string WinHdr)
        {
            FrmListHelpSingleCol ChildWnd = new FrmListHelpSingleCol();
            SkynetCashSales.ViewModel.Help.ListHelpSingleColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpSingleColVM(DT, ChildWnd, WinHdr);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public void TwoColWindShow(DataTable DT, string WinHdr, string ColOneTxt, string ColTwoTxt)
        {
            FrmListHelpTwoCol ChildWnd = new FrmListHelpTwoCol();
            SkynetCashSales.ViewModel.Help.ListHelpTwoColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpTwoColVM(DT, ChildWnd, WinHdr, ColOneTxt, ColTwoTxt);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public void ThreeColWindShow(DataTable DT, string WinHdr, string ColOneTxt, string ColTwoTxt, string ColThreeTxt)
        {
            FrmListHelpThreeCol ChildWnd = new FrmListHelpThreeCol();
            SkynetCashSales.ViewModel.Help.ListHelpThreeColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpThreeColVM(DT, ChildWnd, WinHdr, ColOneTxt, ColTwoTxt, ColThreeTxt);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public void FourColWindShow(DataTable DT, string WinHdr, string ColOneTxt, string ColTwoTxt, string ColThreeTxt, string ColFourTxt)
        {
            FrmListHelpFourCol ChildWnd = new FrmListHelpFourCol();
            SkynetCashSales.ViewModel.Help.ListHelpFourColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpFourColVM(DT, ChildWnd, WinHdr, ColOneTxt, ColTwoTxt, ColThreeTxt, ColFourTxt);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public void FiveColWindShow(DataTable DT, string WinHdr, string ColOneTxt, string ColTwoTxt, string ColThreeTxt, string ColFourTxt, string ColFiveTxt)
        {
            FrmListHelpFiveCol ChildWnd = new FrmListHelpFiveCol();
            SkynetCashSales.ViewModel.Help.ListHelpFiveColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpFiveColVM(DT, ChildWnd, WinHdr, ColOneTxt, ColTwoTxt, ColThreeTxt, ColFourTxt, ColFiveTxt);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public void SixColWindShow(DataTable DT, string WinHdr, string ColOneTxt, string ColTwoTxt, string ColThreeTxt, string ColFourTxt, string ColFiveTxt, string ColSixTxt)
        {
            FrmListHelpSixCol ChildWnd = new FrmListHelpSixCol();
            SkynetCashSales.ViewModel.Help.ListHelpSixColVM vm = new SkynetCashSales.ViewModel.Help.ListHelpSixColVM(DT, ChildWnd, WinHdr, ColOneTxt, ColTwoTxt, ColThreeTxt, ColFourTxt, ColFiveTxt, ColSixTxt);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.ShowDialog();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpSingleColVM.ListInfo> FrmListSingleCol_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpSingleColVM.ListInfo SingleCol)
        {
            if (FrmListSingleCol_Closed != null)
                FrmListSingleCol_Closed(SingleCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpTwoColVM.ListInfo> FrmListCol2_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpTwoColVM.ListInfo TwoCol)
        {
            if (FrmListCol2_Closed != null)
                FrmListCol2_Closed(TwoCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpThreeColVM.ListInfo> FrmListCol3_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpThreeColVM.ListInfo ThreeCol)
        {
            if (FrmListCol3_Closed != null)
                FrmListCol3_Closed(ThreeCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpFourColVM.ListInfo> FrmListCol4_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpFourColVM.ListInfo FourCol)
        {
            if (FrmListCol4_Closed != null)
                FrmListCol4_Closed(FourCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpFiveColVM.ListInfo> FrmListCol5_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpFiveColVM.ListInfo FiveCol)
        {
            if (FrmListCol5_Closed != null)
                FrmListCol5_Closed(FiveCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public event Action<SkynetCashSales.ViewModel.Help.ListHelpSixColVM.ListInfo> FrmListCol6_Closed;
        public void ChildWindow_Closed(SkynetCashSales.ViewModel.Help.ListHelpSixColVM.ListInfo SixCol)
        {
            if (FrmListCol6_Closed != null)
                FrmListCol6_Closed(SixCol);
            ChildWindowManager.Instance.CloseChildWindow();
        }


        public void CommentWindShow()
        {
            FrmGetComment ChildWnd = new FrmGetComment();
            GetCommentVM vm = new GetCommentVM(ChildWnd);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public void ChildWindow_Closed(GetCommentVM.CommentInfo Comment)
        {
            if (FrmGetComment_Closed != null)
                FrmGetComment_Closed(Comment);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        public void PostcodeFinderShow()
        {
            FrmPostcodeFinder ChildWnd = new FrmPostcodeFinder();
            PostcodeFinderVM vm = new PostcodeFinderVM(ChildWnd, true);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public void PostcodeFinderShowODACheck(string PostCode)
        {
            FrmPostcodeFinder ChildWnd = new FrmPostcodeFinder();
            PostcodeFinderVM vm = new PostcodeFinderVM(ChildWnd, true, PostCode);
            vm.Closed += ChildWindow_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public event Action<PostcodeFinderVM.PostcodeInfo> FrmPostcodeFinder_Closed;
        public void ChildWindow_Closed(PostcodeFinderVM.PostcodeInfo PostCode)
        {
            if (FrmPostcodeFinder_Closed != null)
                FrmPostcodeFinder_Closed(PostCode);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        //PromoCode
        public void PromoCodeScreen(MobileRefCashSalesVM cashSalesVM)
        {
            FrmPromoCode ChildWnd = new FrmPromoCode();
            PromoCodeVM vm = new PromoCodeVM(ChildWnd, cashSalesVM);
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }


        //StaffCode
        public void StaffCodeScreen(MobileRefCashSalesVM cashSalesVM)
        {
            FrmStaffCode ChildWnd = new FrmStaffCode();

            StaffCodeVM vm = new StaffCodeVM(ChildWnd, cashSalesVM);
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        //AWBDetail
        public void AWBDetailScreen(MobileRefCashSalesVM cashSalesVM)
        {
            FrmAWBDetail ChildWnd = new FrmAWBDetail();

            AWBDetailVM vm = new AWBDetailVM(ChildWnd, cashSalesVM);
            vm.Closed += ChildWindowAWBDetail_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public event Action<AWBDetailVM.AWBDetailInfo> FrmAWBDetail_Closed;
        public void ChildWindowAWBDetail_Closed(AWBDetailVM.AWBDetailInfo AWBClosed)
        {
            if (FrmAWBDetail_Closed != null)
                FrmAWBDetail_Closed(AWBClosed);
            ChildWindowManager.Instance.CloseChildWindow();
        }

        //View or Edit AWBDetail
        public void AWBDetailScreen2(MobileRefCashSalesVM cashSalesVM, string[] awbdetail/*, string[] refdetail*/)
        {
            FrmAWBDetail ChildWnd = new FrmAWBDetail();

            AWBDetailVM vm = new AWBDetailVM(ChildWnd, cashSalesVM, awbdetail/*, refdetail*/);
            vm.Closed += ChildWindowAWBDetail_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        //View Payment Screen
        public void PaymentScreen(MobileRefCashSalesVM cashSalesVM)
        {
            FrmPayment ChildWnd = new FrmPayment();

            PaymentVM vm = new PaymentVM(ChildWnd, cashSalesVM);
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        //View Stationery Quantity Screen
        public void QuantityScreen(MobileRefCashSalesVM cashSalesVM, string StationeryName, int CurrentQuantity)
        {
            FrmQuantity ChildWnd = new FrmQuantity();
            QuantityVM vm = new QuantityVM(ChildWnd, cashSalesVM, StationeryName, CurrentQuantity);
            vm.Closed += ChildWindowQuantity_Closed;
            ChildWnd.DataContext = vm;
            ChildWnd.Owner = Application.Current.MainWindow;
            ChildWnd.ShowDialog();
        }

        public event Action<QuantityVM.ListInfo> FrmQuantity_Closed;
        public void ChildWindowQuantity_Closed(QuantityVM.ListInfo QuantityClosed)
        {
            if (FrmQuantity_Closed != null)
                FrmQuantity_Closed(QuantityClosed);
            ChildWindowManager.Instance.CloseChildWindow();
        }
    }
}
