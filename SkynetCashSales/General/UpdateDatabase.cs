﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace SkynetCashSales.General
{
    public class UpdateDatabase : AMGenFunction
    {
        private static int NewDbVersion = 20210316, CurDbVersion = 0;
        private SqlDataReader dr;

        public void CheckDatabaseVersion()
        {
            try
            {
                WebDb = new CommunicateWebDb();
                WebDb.GetPromotionJSON();

                SqlSequences();
                NewTable();
                AlterTable();
                CheckStoredProcedure();

                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'DatabaseVersion'");
                if (!dr.HasRows)
                {
                    dr.Close(); 
                    ExecuteQuery($"Insert into SystemParams (paramname,ParamValue) values ('DatabaseVersion','{CurDbVersion}')");
                }
                else if (dr.Read())
                {
                    CurDbVersion = Convert.ToInt32(dr["ParamValue"]);
                }
                dr.Close();

                if (CurDbVersion < NewDbVersion)
                {
                    if (UpdatedateVersion())
                    {
                        ExecuteQuery($"update SystemParams set ParamValue ={NewDbVersion} WHERE paramname = 'DatabaseVersion'");
                    }
                }

                WebDb.GetUpdatedPostCode();
            }
            catch (Exception Ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.CheckDatabaseVersion() : " + Ex.Message);
            }
        }

        private bool UpdatedateVersion()
        {
            bool res = false;
            DataTable dt = new DataTable();
            try
            {
                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'StaffDiscount' and ParamValue = 'StaffDiscount_20201203'");
                if (!dr.HasRows)
                {
                    dt = GetDataTable($"SELECT ZoneCode FROM tblZone WHERE ZoneType = 'LOCAL'");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SqlDataReader dr = GetDataReader($"SELECT Code FROM tblStaffDiscount WHERE Code = '{dt.Rows[i]["ZoneCode"].ToString().Trim()}' AND IsZone = 1");
                        if (dr.HasRows)
                        {
                            ExecuteQuery($"UPDATE tblStaffDiscount SET ToDate = NULL, Status = 1 WHERE Code = '{dt.Rows[i]["ZoneCode"].ToString().Trim()}' AND IsZone = 1");
                        }
                        else
                        {
                            ExecuteQuery($"INSERT INTO tblStaffDiscount(IsZone, Code, IsPercentage, DiscountValue, MonthlyValid, FromDate, ToDate, CreatedBy, CreatedDate, IsExported, ExportedDate, Status)" +
                                $" VALUES('{true}', '{dt.Rows[i]["ZoneCode"].ToString().Trim()}', '{true}', 20, 3, '2019-11-01 00:00:00.000', NULL, 'ICT', GETDATE(), 0, null, 1)");
                        }
                    }
                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('StaffDiscount','StaffDiscount_20201203')");
                }

                #region Add DestCode
                // (SPL & BLP) - EFFECTIVE 07th Dec 2020
                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'DestCode' and ParamValue = 'SPL, BLP'");
                if (!dr.HasRows)
                {
                    ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'SPL') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('SPL', 'SIMPANG PULAI', 'SPL', 'PERAK', 'MALAYSIA', 'WM', 0, 0, 0, NULL)");
                    ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'BLP') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('BLP', 'BALIK PULAU', 'BLP', 'PENANG', 'MALAYSIA', 'WM', 0, 0, 0, NULL)");
                    ExecuteQuery($"UPDATE tblPostcode2020 SET StnCode = 'SPL' WHERE StnCode = 'IPH' AND postcode IN ('30250', '31650', '31600', '31350', '31300', '31000')");
                    ExecuteQuery($"UPDATE tblPostcode2020 SET StnCode = 'BLP' WHERE StnCode = 'PEN' AND postcode IN ('11000', '11010', '11020', '11060', '11700', '11800', '11900', '11910', '11920', '11950', '11960')");
                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('DestCode','SPL, BLP')");
                }

                ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'MRG') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('MRG', 'MARANG', 'MRG', 'TERENGGANU', 'MALAYSIA', 'WM', 0, 0, 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'MRG ODA') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('MRG ODA', 'MARANG ODA', 'MRG', 'TERENGGANU', 'MALAYSIA', 'WM', 1, 0, 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'SGK') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('SGK', 'SUNGKAI', 'SGK', 'PERAK', 'MALAYSIA', 'WM', 0, 0, 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT DestCode FROM tblDestination WHERE DestCode LIKE 'SGK') INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('SGK ODA', 'SUNGKAI ODA', 'SGK', 'PERAK', 'MALAYSIA', 'WM', 1, 0, 0, NULL)");
                #endregion

                #region Assign DestCode under Zone
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'SPL') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 1', 'SPL', 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'BLP') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 1', 'BLP', 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'MRG') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 1', 'MRG', 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'MRG ODA') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 2', 'MRG ODA', 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'MRG SGK') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 1', 'SGK', 0, NULL)");
                ExecuteQuery($"IF NOT EXISTS (SELECT ZoneCode FROM tblZoneDetail WHERE DestCode LIKE 'MRG SGK') INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 2', 'SGK ODA', 0, NULL)");
                #endregion 

                ExecuteQuery("INSERT INTO tblDestination(DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate)" +
                    " SELECT A.DestCode AS DestCode, A.DestCode AS DestName, LEFT(A.DestCode, 3) AS StationCode, '' AS State, IIF(A.ZoneCode LIKE 'ZONE%', 'MALAYSIA', '') AS Country, '' AS Region, 0 AS IsODA, 0 AS IsDeleted, 1 AS IsExported, GETDATE() AS ExportedDate FROM tblZoneDetail A" +
                    " LEFT JOIN tblDestination B ON A.DestCode = B.DestCode" +
                    " WHERE B.DestCode IS NULL");

                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'DestCode' and ParamValue = 'CheckLatest_20200911'");
                if (!dr.HasRows)
                {
                    string[] DestCode = { "STU", "STU ODA", "RAU", "RAU ODA", "PAM", "PAM ODA", "PGH", "PGH ODA", "BGB", "SBL", "MWT" };
                    string[] DestName = { "SETIU", "SETIU ODA", "RAUB", "RAUB ODA", "PUNCAK ALAM", "PUNCAK ALAM ODA", "PAGOH", "PAGOH ODA", "LEDANG", "SUNGAI BULOH", "MELAWATI" };
                    string[] StnCode = { "STU", "STU", "RAU", "RAU", "PAM", "PAM", "PGH", "PGH", "BGB", "SBL", "MWT" };
                    string[] State = { "TERENGGANU", "TERENGGANU", "PAHANG", "PAHANG", "SELANGOR", "SELANGOR", "JOHOR", "JOHOR", "JOHOR", "SELANGOR", "SELANGOR" };
                    bool[] IsODA = { false, true, false, true, false, true, false, true, false, false, false };
                    string[] ZoneCode = { "ZONE 1", "ZONE 2", "ZONE 1", "ZONE 2", "ZONE 1", "ZONE 2", "ZONE 1", "ZONE 2", "ZONE 1", "ZONE 1", "ZONE 1" };

                    for (int i = 0; i < DestCode.Length; i++)
                    {
                        dr = GetDataReader($"SELECT * FROM tblDestination WHERE RTRIM(DestCode) = '{DestCode[i]}'");
                        if (dr.HasRows)
                        {
                            ExecuteQuery($"UPDATE tblDestination SET DestCode = '{DestCode[i]}', DestName = '{DestName[i]}', StationCode = '{StnCode[i]}', State = '{State[i]}', Country = 'MALAYSIA', Region = '', IsODA = '{IsODA[i]}', IsDeleted = 0 WHERE RTRIM(DestCode) = '{DestCode[i]}'");
                        }
                        else
                        {
                            ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted)" +
                                $" VALUES('{DestCode[i]}', '{DestName[i]}', '{StnCode[i]}', '{State[i]}', 'MALAYSIA', '', '{IsODA[i]}', 0)");
                        }

                        dr = GetDataReader($"SELECT * FROM tblZoneDetail WHERE RTRIM(ZoneCode) = '{ZoneCode[i]}' AND RTRIM(DestCode) = '{DestCode[i]}'");
                        if (!dr.HasRows)
                        {
                            ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('{ZoneCode[i]}', '{DestCode[i]}', 0, GETDATE())");
                        }
                    }

                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('DestCode','CheckLatest_20200911')");
                }
                dr.Close();

                // PaymentMode : SHP [ShopeePay]
                dr = GetDataReader($"SELECT a.* FROM tblGeneraldet a" +
                    $" LEFT JOIN tblGeneralMaster b ON a.GenId = b.GenId" +
                    $" WHERE b.GenDesc = 'PAYMENT MODE' AND A.GDDesc = 'ShopeePay'");
                if (!dr.HasRows)
                {
                    int GenId = 14;
                    dr = GetDataReader($"SELECT GenId FROM tblGeneralMaster WHERE GenDesc = 'PAYMENT MODE'");
                    if (dr.Read()) GenId = Convert.ToInt32(dr["GenId"]); dr.Close();

                    ExecuteQuery($"INSERT INTO tblGeneralDet (GenId, GDDesc,GDSDesc, CreateBy, CreateDate, ModifyBy, ModifyDate, Status, IsExported, ExportedDate )" +
                        $" VALUES ( {GenId}, 'ShopeePay', 'SHP', '1', GETDATE(), null, null, 1 , 0, null)");
                }

                dr = GetDataReader($"SELECT * FROM tblItemMaster where ItemDesc = 'Face Mask'");
                if (!dr.HasRows)
                {
                    int ItemId = ExecuteScalarQuery($"INSERT INTO tblItemMaster(ItemCode, ItemDesc, ItemType, SerialNo, Brand, Manufacturer, Unit1Id, Unit1, Unit1Cost, Unit1SalRateToStation, Unit1SalRateToBranch, Unit1SalRateToDept, Unit2Id, Unit2, UnitFactor, Unit2SalRateToStation, Unit2SalRateToBranch, Unit2SalRateToDept, OpeningStk, MinStk, MaxStk, ReOrder, CreateBy, CreateDate, ModifyBy, ModifyDate, Status)" +
                        $" VALUES('I0011', 'Face Mask', 28, 11, '', '', 30, 'PC', 7, 7, 7, 7, 30, 'PC', 1, 7, 7, 7, 1000, 100, 1000, 500, 1, GETDATE(), 0, null, 1)");

                    ExecuteQuery($"INSERT INTO tblStock(ItemId, Opening, StockAdj, Purchase, PurchaseRet, Receive, Issue, Sales, SalesRet, Excess, Short, Closing)" +
                        $" VALUES({ItemId}, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1000)");
                }

                dr = GetDataReader($"SELECT * FROM SystemParams WHERE paramname = 'Missing ODA' and ParamValue = 'DestCode'");
                if (!dr.HasRows) // 2020-SEP-01
                {
                    dt = GetDataTable($"SELECT DISTINCT StnCode, State FROM tblPostcode2020 WHERE Remarks LIKE 'ODA%' AND State NOT IN('SABAH', 'SARAWAK')");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dr = GetDataReader($"SELECT * FROM tblDestination WHERE StationCode = '{dt.Rows[i]["StnCode"].ToString().Trim()}' AND IsODA = 1");
                        if (!dr.HasRows)
                        {
                            ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted)" +
                                $" VALUES('{dt.Rows[i]["StnCode"].ToString().Trim()} ODA', '{dt.Rows[i]["StnCode"].ToString().Trim()} ODA', '{dt.Rows[i]["StnCode"].ToString().Trim()}', '{dt.Rows[i]["State"].ToString().Trim()}', 'MALAYSIA', 'W', 1, 0)");
                        }
                        dr.Close();

                        dr = GetDataReader($"SELECT * FROM tblZoneDetail WHERE RTRIM(ZoneCode) = 'ZONE 2' AND RTRIM(DestCode) = '{dt.Rows[i]["StnCode"].ToString().Trim()} ODA'");
                        if (!dr.HasRows)
                        {
                            ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 2', '{dt.Rows[i]["StnCode"].ToString().Trim()} ODA', 0, NULL)");
                        }
                    }
                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('Missing ODA','DestCode')");
                }

                dr = GetDataReader($"SELECT * FROM SystemParams WHERE paramname = 'NewStation' and ParamValue = 'STU'");
                if (!dr.HasRows) // 2020-SEP-01
                {
                    dr.Close();
                    ExecuteQuery($"UPDATE tblPostcode2020 SET StnCode = 'STU' WHERE StnCode = 'TGG' AND postcode IN ('22100', '22110', '22120')");

                    dr = GetDataReader($"SELECT * FROM tblDestination WHERE DestCode LIKE 'STU%'");
                    if (!dr.HasRows)
                    {
                        dr.Close();
                        ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('STU', 'SETIU', 'STU', 'TERENGGANU', 'MALAYSIA', 'WM', 0, 0, 0, NULL)");
                        ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('STU ODA', 'SETIU ODA', 'STU', 'Selangor', 'MALAYSIA', 'WM', 1, 0, 0, NULL)");
                    }

                    dr = GetDataReader($"SELECT * FROM tblZoneDetail WHERE DestCode LIKE 'STU%'");
                    if (!dr.HasRows)
                    {
                        dr.Close();
                        ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 1', 'STU', 0, NULL)");
                        ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('ZONE 2', 'STU ODA', 0, NULL)");
                    }
                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('NewStation','STU')");
                }
                dr.Close();

                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'CheckIndex' and ParamValue = 'Idx200513'");
                if (!dr.HasRows)
                {
                    CheckIndex("Idx200505");
                }
                dr.Close();

                dr = GetDataReader("SELECT * FROM tblDestination WHERE DestCode = 'LKW' AND IsDeleted = 0");
                if (!dr.HasRows)
                {
                    ExecuteQuery($"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('LKW', 'LANGKAWI', 'LKW', 'KEDAH', 'MALAYSIA', 'W', 0, 0, 0, NULL)");
                }
                dr.Close();

                dr = GetDataReader("SELECT * FROM tblPostcode2020 WHERE POSTCODEID IN ('10619', '10620', '10621') AND remarks = 'ODA- 48HOURS'");
                if (!dr.HasRows)
                {
                    ExecuteQuery($"UPDATE tblPostcode2020 set remarks = 'ODA- 48HOURS' WHERE POSTCODEID IN ('10619', '10620', '10621')");
                }
                dr.Close();

                dr = GetDataReader("SELECT * FROM tblPostcode2020 WHERE timeReceived is not null");
                if (dr.HasRows)
                {
                    ExecuteQuery($"UPDATE tblPostcode2020 set timeReceived  = null");
                }
                dr.Close();

                res = true;
            }
            catch (Exception ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.UpdatedateVersion() :" + ex.ToString());
            }
            return res;
        }

        private void SqlSequences()
        {
            try
            {
                ExecuteQuery(@"
                IF NOT EXISTS(SELECT name FROM sys.sequences WHERE name = 'ITranRefNumGRN')
                    CREATE SEQUENCE ITranRefNumGRN AS INT START WITH 1 INCREMENT BY 1

                IF NOT EXISTS(SELECT name FROM sys.sequences WHERE name = 'ITranRefNumOB')
                    CREATE SEQUENCE ITranRefNumOB AS INT START WITH 1 INCREMENT BY 1

                IF NOT EXISTS(SELECT name FROM sys.sequences WHERE name = 'ITranRefNumSL')
                    CREATE SEQUENCE ITranRefNumSL AS INT START WITH 1 INCREMENT BY 1

                IF NOT EXISTS(SELECT name FROM sys.sequences WHERE name = 'ITranRefNumPR')
                    CREATE SEQUENCE ITranRefNumPR AS INT START WITH 1 INCREMENT BY 1

                IF NOT EXISTS(SELECT name FROM sys.sequences WHERE name = 'ITranRefNumSA')
                    CREATE SEQUENCE ITranRefNumSA AS INT START WITH 1 INCREMENT BY 1");
            }
            catch (Exception Ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.SqlSequences() : " + Ex.Message);
            }
        }

        private void NewTable()
        {
            try
            {
                ExecuteQuery("IF NOT EXISTS(SELECT NAME FROM sys.tables WHERE NAME = 'SystemParams')" +
                    " CREATE TABLE [dbo].[SystemParams](" +
                    " [SystemParamsID] [int] IDENTITY(1,1) NOT NULL," +
                    " [ParamName] [nvarchar](30) NOT NULL," +
                    " [ParamValue] [nvarchar](50) NULL," +
                    " [TimeTransmitted] [datetime] NULL," +
                    " CONSTRAINT [PK_SystemParams] PRIMARY KEY CLUSTERED ([SystemParamsID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]");

                ExecuteQuery("IF NOT EXISTS(SELECT NAME FROM sys.tables WHERE NAME = 'tblCSSUpgrade')" +
                    " CREATE TABLE [tblCSSUpgrade] (" +
                    " [GUID] [int] IDENTITY(1,1) NOT NULL," +
                    " [ReleaseDate]   TEXT NOT NULL, [VersionNo] TEXT, [FileName]  TEXT, [IsCumpolsory]  REAL, [UpdatedDate]   TEXT, " +
                    " [Features]  TEXT, " +
                    " [Ftp_URL]   TEXT, " +
                    " [Ftp_UserName]  TEXT, " +
                    " [Ftp_Password]  TEXT," +
                    " CONSTRAINT [PK_tblCSSUpgrade] PRIMARY KEY CLUSTERED ([GUID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]");

                ExecuteQuery("IF NOT EXISTS(SELECT NAME FROM sys.tables WHERE NAME = 'tblPODShipment')" +
                    " CREATE TABLE [dbo].[tblPODShipment](" +
                    " [AWBNumber] [varchar](30) NOT NULL," +
                    " [CheckinDate] [datetime] NOT NULL," +
                    " [CheckinBy] [varchar](50) NOT NULL," +
                    " [PODDate] [datetime] NULL," +
                    " [Status] [varchar](30) NOT NULL," +
                    " [DeliveredTo] [varchar](50) NOT NULL," +
                    " [IDProof] [varchar](20) NOT NULL," +
                    " [IDProofType] [varchar](30) NOT NULL," +
                    " [PODBy] [varchar](50) NOT NULL," +
                    " [StnCode] [varchar](10) NOT NULL," +
                    " [TransmittedOn] [datetime] NULL," +
                    " CONSTRAINT [PK_tblPODShipment] PRIMARY KEY CLUSTERED ([AWBNumber] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]");

                ExecuteQuery(@"IF NOT EXISTS(SELECT NAME FROM sys.tables WHERE NAME = 'tblStockTran')
                CREATE TABLE [dbo].[tblStockTran](
	            [StkTranID] [int] IDENTITY(1,1) NOT NULL,
	            [StkTranType] [varchar](10) NOT NULL,
	            [StkTranRefNum] [varchar](20) NOT NULL,
	            [StkTranDate] [datetime] NOT NULL,
	            [ItemId] [int] NOT NULL,
	            [ItemCode] [varchar](15) NOT NULL,
	            [Unit1] [varchar](50) NOT NULL,
	            [Unit2] [varchar](50) NOT NULL,
	            [StkTranQty] [decimal](18, 2) NOT NULL,
	            [Rate] [decimal](18, 2) NOT NULL,
	            [DiscountPer] [decimal](18, 2) NOT NULL,
	            [DiscountAmount] [decimal](18, 2) NOT NULL,
	            [TaxPer] [decimal](18, 2) NOT NULL,
	            [TaxAmount] [decimal](18, 2) NOT NULL,
	            [TranAmount] [decimal](18, 2) NOT NULL,
	            [CancelQty] [decimal](18, 2) NOT NULL,
	            [StkTranPendingQty] [decimal](18, 2) NOT NULL,
	            [TranStatus] [varchar](10) NOT NULL,
	            [StkTranDesc] [varchar](50) NOT NULL,
	            [StkTranRemarks] [varchar](100) NULL,
	            [CreateBy] [int] NOT NULL,
	            [ModifyBy] [int] NULL,
	            [ModifyDate] [datetime] NULL,
	            [EMailBy] [int] NULL,
	            [EMailDate] [datetime] NULL,
	            [IsExported] [bit] NULL,
	            [ExportedDate] [datetime] NULL
                CONSTRAINT [PK_tblStockTran] PRIMARY KEY CLUSTERED ([StkTranID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]");

            }
            catch (Exception ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.NewTable() :" + ex.ToString());
            }
        }

        private void ItemMaster()
        {
            ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM SystemParams WHERE paramname = 'tblItemMaster' and ParamValue = 'PKey_ItemCode')
            BEGIN
	            DECLARE @MytblItemMaster TABLE(
		            [ItemId] [int] IDENTITY(1,1) NOT NULL,
		            [ItemCode] [varchar](15) NOT NULL,
		            [ItemDesc] [varchar](50) NOT NULL,
		            [ItemType] [int] NOT NULL,
		            [SerialNo] [varchar](15) NULL,
		            [Brand] [varchar](50) NULL,
		            [Manufacturer] [varchar](50) NULL,
		            [Unit1Id] [int] NULL,
		            [Unit1] [varchar](50) NULL,
		            [Unit1Cost] [decimal](18, 2) NULL,
		            [Unit1SalRateToStation] [decimal](18, 2) NULL,
		            [Unit1SalRateToBranch] [decimal](18, 2) NULL,
		            [Unit1SalRateToDept] [decimal](18, 2) NULL,
		            [Unit2Id] [int] NULL,
		            [Unit2] [varchar](50) NULL,
		            [UnitFactor] [int] NULL,
		            [Unit2SalRateToStation] [decimal](18, 2) NULL,
		            [Unit2SalRateToBranch] [decimal](18, 2) NULL,
		            [Unit2SalRateToDept] [decimal](18, 2) NULL,
		            [OpeningStk] [decimal](18, 2) NULL,
		            [MinStk] [decimal](18, 2) NULL,
		            [MaxStk] [decimal](18, 2) NULL,
		            [ReOrder] [decimal](18, 2) NULL,
		            [CreateBy] [int] NOT NULL,
		            [CreateDate] [datetime] NOT NULL,
		            [ModifyBy] [int] NULL,
		            [ModifyDate] [datetime] NULL,
		            [Status] [bit] NULL,
		            [IsExported] [bit] NULL,
		            [ExportedDate] [datetime] NULL,
		            [ItemImage] [varbinary](max) NULL)

	            INSERT @MytblItemMaster ([ItemCode], [ItemDesc], [ItemType], [SerialNo], [Brand], [Manufacturer], [Unit1Id], [Unit1], [Unit1Cost], [Unit1SalRateToStation], [Unit1SalRateToBranch], [Unit1SalRateToDept], [Unit2Id], [Unit2], [UnitFactor], [Unit2SalRateToStation], [Unit2SalRateToBranch], [Unit2SalRateToDept], [OpeningStk], [MinStk], [MaxStk], [ReOrder], [CreateBy], [CreateDate], [ModifyBy], [ModifyDate], [Status], [IsExported], [ExportedDate], [ItemImage]) VALUES 
	            (N'I0001', N'FLYER A', 28, N'1', N'.', N'.', 30, N'PC', CAST(2.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 30, N'PC', 1, CAST(1.30 AS Decimal(18, 2)), CAST(1.50 AS Decimal(18, 2)), CAST(1.20 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), 0, CAST(0x0000A5F700000000 AS DateTime), 2, CAST(0x0000AC870148B2F3 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0002', N'FLYER B', 28, N'2', N'.', N'.', 30, N'PC', CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 30, N'PC', 1, CAST(2.00 AS Decimal(18, 2)), CAST(3.50 AS Decimal(18, 2)), CAST(2.80 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), 0, CAST(0x0000A5F700000000 AS DateTime), 2, CAST(0x0000AC87014C554B AS DateTime), 1, 0, NULL, NULL),
	            (N'I0003', N'FLYER C', 28, N'3', N'.', N'.', 30, N'PC', CAST(0.50 AS Decimal(18, 2)), CAST(0.50 AS Decimal(18, 2)), CAST(0.50 AS Decimal(18, 2)), CAST(0.50 AS Decimal(18, 2)), 30, N'PC', 1, CAST(5.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), 0, CAST(0x0000A5F700000000 AS DateTime), 2, CAST(0x0000AC87014C6395 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0004', N'WRAPPING SERVICE', 28, N'4', N'.', N'.', 37, N'SET', CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 37, N'SET', 1, CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(75.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), 0, CAST(0x0000A5F700000000 AS DateTime), 2, CAST(0x0000AC87014D47C3 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0005', N'CARTON BOX - BIG', 28, N'5', N'-', N'.', 30, N'PC', CAST(3.50 AS Decimal(18, 2)), CAST(3.50 AS Decimal(18, 2)), CAST(3.50 AS Decimal(18, 2)), CAST(3.50 AS Decimal(18, 2)), 30, N'PC', 1, CAST(6.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(8.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A5F700000000 AS DateTime), 2, CAST(0x0000AC87014B9EE0 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0006', N'CARTON BOX - SMALL', 28, N'', N'', N'ABC', 30, N'PC', CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), 30, N'PC', 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A69D00BBE144 AS DateTime), 2, CAST(0x0000AC8701807BF1 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0007', N'BUBBLE WRAP', 28, N'', N'', N'', 30, N'PC', CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), CAST(2.50 AS Decimal(18, 2)), 30, N'PC', 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 2, CAST(0x0000A69D00BBE144 AS DateTime), 2, CAST(0x0000AC8701807BF1 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0008', N'MIC-PAC', 27, N'', N'', N'', 29, N'PACK', CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 29, N'PACK', 1, CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 2, CAST(0x0000AAF800000000 AS DateTime), 2, CAST(0x0000AC870180461C AS DateTime), 1, 0, NULL, NULL),
	            (N'I0009', N'POUCH', 27, N'', N'', N'', 29, N'PACK', CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 29, N'PACK', 1, CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 2, CAST(0x0000AAF800000000 AS DateTime), 2, CAST(0x0000AC870180461C AS DateTime), 1, 0, NULL, NULL),
	            (N'I0010', N'PRIHATIN', 27, N'7', N'', N'', 30, N'PC', CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), 30, N'PC', 1, CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(50000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 2, CAST(0x0000AB9E00E702AC AS DateTime), 2, CAST(0x0000AC87017A6AA3 AS DateTime), 1, 0, NULL, NULL),
	            (N'I0011', N'FACE MASK', 28, N'11', N'', N'', 30, N'PC', CAST(7.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), 30, N'PC', 1, CAST(7.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), 1, CAST(0x0000AC2F00C2E08A AS DateTime), 2, CAST(0x0000AC870144D3EE AS DateTime), 1, 0, NULL, NULL)

	            UPDATE @MytblItemMaster SET SerialNo = ItemId

	            UPDATE @MytblItemMaster SET ItemType = B.ItemType, Brand = B.Brand, Manufacturer = B.Manufacturer, Unit1Id = B.Unit1Id, Unit1 = B.Unit1, Unit1Cost = B.Unit1Cost, Unit1SalRateToStation = B.Unit1SalRateToStation, Unit1SalRateToBranch = B.Unit1SalRateToBranch, Unit1SalRateToDept = B.Unit1SalRateToDept, 
	            Unit2Id = B.Unit2Id, Unit2 = B.Unit2, UnitFactor = B.UnitFactor, Unit2SalRateToStation = B.Unit2SalRateToStation, Unit2SalRateToBranch = B.Unit2SalRateToBranch, Unit2SalRateToDept = B.Unit2SalRateToDept, 
	            OpeningStk = B.OpeningStk, MinStk = B.MinStk, MaxStk = B.MaxStk, ReOrder = B.ReOrder, CreateBy = B.CreateBy, CreateDate = B.CreateDate FROM @MytblItemMaster A
	            LEFT JOIN tblItemMaster B ON A.ItemDesc = B.ItemDesc
	            WHERE B.ItemDesc IS NOT NULL

	            DROP TABLE tblItemMaster
	            SELECT * INTO tblItemMaster FROM @MytblItemMaster

	            DECLARE @Constraint_Name VARCHAR(100), @CmdStr VARCHAR(200);

	            SELECT @Constraint_Name = Constraint_Name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE Table_Name = 'tblItemMaster'
	            IF NOT EXISTS (SELECT Constraint_Name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE Table_Name = 'tblItemMaster' AND (Constraint_Name != 'PK_tblItemMaster' OR COLUMN_NAME != 'ItemCode'))
	            BEGIN
		            IF @Constraint_Name IS NOT NULL AND RTRIM(@Constraint_Name) != ''
		            BEGIN
			            SELECT @CmdStr = CONCAT('ALTER TABLE tblItemMaster DROP CONSTRAINT ', @Constraint_Name)
			            EXEC(@CmdStr)
		            END
		            ALTER TABLE tblItemMaster ADD CONSTRAINT PK_tblItemMaster PRIMARY KEY (ItemCode);
	            END

	            Insert into SystemParams (paramname, ParamValue) values ('tblItemMaster', 'PKey_ItemCode')
            END");

            dr = GetDataReader("SELECT paramname FROM SystemParams WHERE paramname = 'tblItemMaster' and ParamValue = 'Update ItemImage'");
            if (!dr.HasRows)
            {
                dr.Close();
                DataTable dt = new DataTable();
                string path = "SkynetCashSales.Images.";
                string image_FlyersA = path + "FlyersA.png", image_FlyersB = path + "FlyersB.png", image_FlyersC = path + "FlyersC.png", image_Wrapping = path + "wrapping.png", image_boxBig = path + "boxBig.png", image_boxSmall = path + "boxSmall.png", image_Handling = path + "handling.png", image_micpac = path + "micpac.png", image_Pouch = path + "Pouch.jpeg", image_bubblewrap = path + "bubblewrap.png", image_facemask = path + "facemask.png";
                string image = "";

                dt = GetDataTable($"SELECT ItemCode, ItemDesc from tblItemMaster");
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        image = "";
                        switch (dt.Rows[i]["ItemDesc"].ToString().ToUpper())
                        {

                            case "FLYER A": image = image_FlyersA; break;
                            case "FLYER B": image = image_FlyersB; break;
                            case "FLYER C": image = image_FlyersC; break;
                            case "WRAPPING SERVICE": image = image_Wrapping; break;
                            case "CARTON BOX - BIG": image = image_boxBig; break;
                            case "CARTON BOX - SMALL": image = image_boxSmall; break;
                            case "HANDLING CHARGES": image = image_Handling; break;
                            case "MIC-PAC": image = image_micpac; break;
                            case "POUCH": image = image_Pouch; break;
                            case "BUBBLE WRAP": image = image_bubblewrap; break;
                            case "FACE MASK": image = image_facemask; break;
                            default: break;
                        }

                        if (dt.Rows[i]["ItemDesc"].ToString().ToUpper() != "PRIHATIN" && dt.Rows[i]["ItemDesc"].ToString().ToUpper() != "HANDLING CHARGES")
                        {
                            System.Reflection.Assembly assem = System.Reflection.Assembly.GetExecutingAssembly();
                            using (Stream resFilestream = assem.GetManifestResourceStream(image))
                            {
                                byte[] imgBytes = new byte[resFilestream.Length];
                                resFilestream.Read(imgBytes, 0, imgBytes.Length);

                                if (imgBytes != null)
                                {
                                    SqlCmd = new SqlCommand("UPDATE tblItemMaster SET ItemImage = @ItemImage WHERE ItemCode = @ItemCode", SqlCon);
                                    SqlCmd.Parameters.AddWithValue("@ItemImage", imgBytes);
                                    SqlCmd.Parameters.AddWithValue("@ItemCode", dt.Rows[i]["ItemCode"].ToString());
                                    SqlCmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
                ExecuteQuery("Insert into SystemParams (paramname, ParamValue) values ('tblItemMaster', 'Update ItemImage')");

                dr = GetDataReader("SELECT ParamValue FROM SystemParams WHERE paramname = 'PO EMAIL' and ParamValue = 'PO EMAIL INSERT'");
                if (!dr.HasRows)
                {
                    dr.Close();
                    ExecuteQuery("DELETE FROM SystemParams WHERE paramname IN ('PurOrdEmailIdToPIC', 'PurOrdCcEmailId')");
                    ExecuteQuery("Insert into SystemParams (paramname, ParamValue) values ('PurOrdEmailIdToPIC', 'itsally@skynet.com.my')");
                    ExecuteQuery("Insert into SystemParams (paramname, ParamValue) values ('PurOrdCcEmailId', 'ictilangovan@skynet.com.my')");
                    ExecuteQuery("Insert into SystemParams (paramname, ParamValue) values ('PO EMAIL', 'PO EMAIL INSERT')");
                }

            }
            dr.Close();
        }

        private void AlterTable()
        {
            try
            {
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblItemMaster') AND name = 'ItemImage')
                BEGIN                
                    EXEC('ALTER TABLE tblItemMaster ADD [ItemImage] [varbinary](max) NULL')
                END");
                
                ItemMaster();

                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'Remarks')
                BEGIN                
                    ALTER TABLE tblStock ADD [Remarks] [varchar](100) NULL;
                END");

                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'CreateBy')
                BEGIN                
                    ALTER TABLE tblStock ADD [CreateBy] [int] NULL;
                END");
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'CreateDate')
                BEGIN                
                    ALTER TABLE tblStock ADD [CreateDate] [datetime] NULL;
                END");
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'ModifyBy')
                BEGIN                
                    ALTER TABLE tblStock ADD [ModifyBy] [int] NULL;
                END");
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'ModifyDate')
                BEGIN                
                    ALTER TABLE tblStock ADD [ModifyDate] [datetime] NULL;
                END");
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'Status')
                BEGIN                
                    ALTER TABLE tblStock ADD [Status] [bit] NULL;                    
                END");
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblStock') AND name = 'ItemCode')
                BEGIN                
                    ALTER TABLE tblStock ADD [ItemCode] [varchar](15) NULL;
                END");


                // 2020-DEC-04
                ExecuteQuery($@"IF NOT EXISTS (SELECT * FROM SystemParams WHERE paramname = 'tblStock' and ParamValue = 'Initiate Stock')" +
                "BEGIN " +
                    "UPDATE tblStock set ItemCode = b.ItemCode from tblStock a " +
                    "JOIN tblItemMaster b ON a.ItemId = b.ItemId; " +

                     "UPDATE tblStock SET Purchase = 0, PurchaseRet = 0, Receive = 0, Issue = 0, Excess = 0, Short = 0, Status = 0, Remarks = '', CreateBy = 0, CreateDate = GETDATE() " +

                    "UPDATE tblStock set Status = 1 from tblStock a " +
                    "JOIN (SELECT ItemId, Max(StockId) as StockId FROM tblStock GROUP BY ItemId) b ON a.ItemId = b.ItemId " +
                    "WHERE a.StockId = b.StockId; " +

                    "INSERT[dbo].[tblStockTran]([StkTranType], [StkTranRefNum], [StkTranDate], [ItemId], [ItemCode], [Unit1], [Unit2], [StkTranQty], [Rate], [DiscountPer], [DiscountAmount], [TaxPer], [TaxAmount], [TranAmount], [CancelQty], [StkTranPendingQty], [TranStatus], [StkTranDesc], [StkTranRemarks], [CreateBy], [ModifyBy], [ModifyDate], [EMailBy], [EMailDate], [IsExported], [ExportedDate]) " +
                    $"SELECT 'OB', FORMAT(NEXT VALUE FOR ITranRefNumOB, 'OB{MyCompanyCode}{(MyCompanySubName ?? "").ToString()}000000'), GETDATE(), A.ItemId, B.ItemCode, B.Unit1, B.Unit2, A.Opening, B.Unit1SalRateToStation, 0, 0, 0, 0, 0, 0, 0, 'C', 'OPENING STOCK', NULL, 1, 0, NULL, 0, Null, 0, NULL FROM tblStock A " +
                    "JOIN tblItemMaster B ON A.ItemId = B.ItemId WHERE A.Status = 1; " +

                    "Insert into SystemParams(paramname, ParamValue) values('tblStock', 'Initiate Stock'); " +
                "END");

                //// 2020 - NOV - 25
                //// decimal(18, 0) => decimal(18, 2)
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM SystemParams WHERE paramname = 'tblCashSales' and ParamValue = 'ALTER COLUMN OtherCharges')
                BEGIN
                    ALTER TABLE tblCashSales ALTER COLUMN OtherCharges decimal(18, 2)
                    Insert into SystemParams (paramname, ParamValue) values ('tblCashSales', 'ALTER COLUMN OtherCharges')
                END");

                //dr = GetDataReader("SELECT paramname FROM SystemParams WHERE paramname = 'tblCashSales' and ParamValue = 'ALTER COLUMN OtherCharges'");
                //if (!dr.HasRows)
                //{
                //    dr.Close();
                //    ExecuteQuery("ALTER TABLE tblCashSales ALTER COLUMN OtherCharges decimal(18, 2)");
                //    ExecuteQuery("Insert into SystemParams (paramname, ParamValue) values ('tblCashSales', 'ALTER COLUMN OtherCharges')");
                //}
                //dr.Close();

                //#region Primary Key is changed in tblCSaleCNNum tabe to control the duplicate CNNumber. Because when use multi user functionality the duplicate CNNumber are inserted within micro second
                //dr = GetDataReader("SELECT Constraint_Name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE Table_Name = 'tblCSaleCNNum' AND Constraint_Name != 'PK_tblCSaleCNNum'");
                //if (dr.Read())
                //{
                //    string Constraint_Name = dr["Constraint_Name"].ToString();
                //    dr.Close();
                //    ExecuteQuery($"ALTER TABLE tblCSaleCNNum DROP CONSTRAINT {Constraint_Name}");
                //    ExecuteQuery($"ALTER TABLE tblCSaleCNNum ADD CONSTRAINT PK_tblCSaleCNNum PRIMARY KEY (CNNum)");
                //}
                //dr.Close();
                //#endregion 

                // 2020-AUG-12
                ExecuteQuery(@"IF EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblCashSales') AND name = 'ShipmentType')
                BEGIN
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN PostCodeId')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN DestCode')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN DestStation')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN ShipmentType')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN Pieces')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN Weight')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN VLength')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN VBreadth')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN VHeight')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN VolWeight')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN InsShipmentDesc')
                    EXEC('ALTER TABLE tblCashSales DROP COLUMN RefNum')
                END");

                try
                {
                    string[] Columns = { "StnCode", "SubName", "WTMachineName", "SerialPort", "BaudRate", "DataBits", "WTLocation", "WTDividend", "SleepTime", "CNPrinter", "RptPrinter", "IsGenXML", "PCName", "CUserID", "CDate", "Status", "IsExported", "ExportedDate", "StickerPrinter", "CNPrinterIsActive", "StickerPrinterIsActive" };
                    string DataType = "";
                    for (int i = 0; i < Columns.Length; i++)
                    {
                        DataType = "";
                        dr = GetDataReader($"SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblWPConfig') AND name = '{Columns[i].ToString()}'");
                        if (!dr.HasRows)
                        {
                            switch (Columns[i].ToString())
                            {
                                case "StnCode": DataType = $"varchar (10) NOT NULL DEFAULT '{MyCompanyCode}'"; break;
                                case "SubName": DataType = "varchar (10) NOT NULL DEFAULT ''"; break;
                                case "WTMachineName": DataType = "varchar (120) NOT NULL DEFAULT ''"; break;
                                case "SerialPort": DataType = "varchar (10) NOT NULL DEFAULT ''"; break;
                                case "BaudRate": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "DataBits": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "WTLocation": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "WTDividend": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "SleepTime": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "CNPrinter": DataType = "varchar (120) NOT NULL DEFAULT ''"; break;
                                case "RptPrinter": DataType = "varchar (120) NOT NULL DEFAULT ''"; break;
                                case "IsGenXML": DataType = "bit NOT NULL DEFAULT 0"; break;
                                case "PCName": DataType = "varchar (50) NOT NULL DEFAULT ''"; break;
                                case "CUserID": DataType = "int NOT NULL DEFAULT 0"; break;
                                case "CDate": DataType = "datetime NOT NULL DEFAULT GETDATE()"; break;
                                case "Status": DataType = "bit NOT NULL DEFAULT 0"; break;
                                case "IsExported": DataType = "bit NOT NULL DEFAULT 0"; break;
                                case "ExportedDate": DataType = "datetime NULL"; break;
                                case "StickerPrinter": DataType = "varchar (120) NULL"; break;
                                case "CNPrinterIsActive": DataType = "bit NULL"; break;
                                case "StickerPrinterIsActive": DataType = "bit NULL"; break;
                            }

                            if (Columns[i].ToString().Trim() != "" && DataType.Trim() != "")
                            {
                                ExecuteQuery($"ALTER TABLE tblWPConfig ADD {Columns[i].ToString()} {DataType}");
                            }
                        }
                    }
                }
                catch { }

                // 2020-OCT-13
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblConsignor') AND name = 'AWBNum')
                BEGIN                
                    EXEC('ALTER TABLE tblConsignor ADD AWBNum varchar(20) NULL')
                END");

                // 2020-OCT-13
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblConsignee') AND name = 'AWBNum')
                BEGIN      
                    EXEC('ALTER TABLE tblConsignee ADD AWBNum varchar(20) NULL')
                    EXEC('ALTER TABLE tblConsignee DROP COLUMN ConsignorCode')
                    EXEC('ALTER TABLE tblConsignee ADD CompanyName varchar(50) NULL')
                END");

                // 2021-JAN-29
                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblCSaleCNNum') AND name = 'RASurcharge')
                BEGIN      
                    EXEC('ALTER TABLE tblCSaleCNNum ADD RASurcharge decimal(18,2) NULL')
                END");

                ExecuteQuery(@"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('tblCashSales') AND name = 'RASurcharge')
                BEGIN      
                    EXEC('ALTER TABLE tblCashSales ADD RASurcharge decimal(18,2) NULL')
                END");
            }
            catch (Exception ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.AlterTable() :" + ex.ToString());
            }
        }
        
        private void CheckStoredProcedure()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = GetDataTable("SELECT NAME FROM sys.procedures WHERE NAME = 'SP_AddCnNumber'");
                string str1 = dt.Rows.Count > 0 ? "ALTER " : "CREATE ";
                ExecuteQuery(str1 + @" PROCEDURE [dbo].[SP_AddCnNumber]
	            @CSalesNo [varchar](20),
	            @MobRefNum [varchar](50),
	            @OriginCode [varchar](10),
	            @ConsignorCode [varchar](10),
	            @PostCodeId [int],
	            @DestCode [varchar](10),
	            @ConsigneeCode [varchar](10),
	            @ShipmentType [varchar](20),
	            @Pieces [int],
	            @Weight [decimal](18, 2),
	            @VLength [decimal](18, 2),
	            @VBreadth [decimal](18, 2),
	            @VHeight [decimal](18, 2),
	            @VolWeight [decimal](18, 2),
	            @HighestWeight [decimal](18, 2),
	            @Price [decimal](18, 2),
	            @OtherCharges [decimal](18, 2),
	            @TotalAmt [decimal](18, 2),
	            @InsShipmentValue [decimal](18, 2),
	            @InsShipmentDesc [varchar](150),
	            @InsRate [decimal](18, 2),
	            @InsValue [decimal](18, 2),
	            @StaffDiscRate [decimal](18, 2),
	            @StaffDiscAmt [decimal](18, 2),
	            @PromoCode [varchar](20),
	            @PromoRate [decimal](18, 2),
	            @PromoDiscAmt [decimal](18, 2),
	            @TaxCode [varchar](20),
	            @TaxRate [decimal](18, 2),
                @RASurcharges [decimal](18, 2),
	            @CUserID [int]
                AS
                BEGIN
	                SET NOCOUNT ON;
	
	                DECLARE @FromRunningNo VARCHAR(11), @ToRunningNo VARCHAR(11), @RunningNo BIGINT, @CheckDigit INT, @AwbNumber VARCHAR(30), @CmdStr VARCHAR(500), @ValidCnt smallint
	                SELECT @AwbNumber = CNNum FROM tblLastCNNum WHERE CNNum IS NOT NULL AND CNNum != ''
	                IF(@AwbNumber IS NULL OR @AwbNumber = '') 
				    BEGIN				
					    SELECT TOP 1 @FromRunningNo = LEFT(FromAWB, 11), @ToRunningNo = LEFT(ToAWB, 11) FROM tblAWBAllocation WHERE Status = 'ACTIVE' AND IsDelete = 0 ORDER BY FromAWB
					    IF(@FromRunningNo IS NULL OR @FromRunningNo = '') 
					    BEGIN
			                UPDATE tblAWBAllocation SET Status = 'USED', ModifyBy = @CUserID, ModifyDate = GETDATE() WHERE Status = 'ACTIVE'
			                UPDATE tblAWBAllocation SET Status = 'ACTIVE', ModifyBy = @CUserID, ModifyDate = GETDATE() WHERE AllocateId = (SELECT MIN(AllocateId) FROM tblAWBAllocation WHERE Status = 'NEXT')
						    SELECT TOP 1 @FromRunningNo = LEFT(FromAWB, 11), @ToRunningNo = LEFT(ToAWB, 11) FROM tblAWBAllocation WHERE Status = 'ACTIVE' AND IsDelete = 0 ORDER BY FromAWB
						    IF EXISTS (SELECT name FROM sys.sequences WHERE name = 'AwbNumber')
						    BEGIN
							    DROP SEQUENCE AwbNumber
						    END
					    END
					    SELECT @RunningNo = MAX(LEFT(CNNum, 11)) from tblCSaleCNNum WHERE left(CNNum, 11) BETWEEN @FromRunningNo AND @ToRunningNo
						
					    IF(@RunningNo IS NULL OR @RunningNo = 0) SELECT @RunningNo = @FromRunningNo

			            SELECT @CheckDigit = Convert(INT, @RunningNo % 7)
			            SELECT @AwbNumber = Concat(@RunningNo, @CheckDigit)
				    END
	            
				    IF(RTRIM(@AwbNumber) != '' AND LEN(@AwbNumber) = 12)
	                BEGIN					
					    SET @ValidCnt = 0

					    WHILE (@ValidCnt <= 5)
					    BEGIN
						    SELECT @RunningNo = LEFT(@AwbNumber, 11)

						    IF NOT EXISTS (SELECT name FROM sys.sequences WHERE name = 'AwbNumber')
						    BEGIN
							    SELECT @CmdStr = CONCAT('CREATE SEQUENCE AwbNumber AS BIGINT START WITH ', @RunningNo, ' INCREMENT BY 1')
							    EXEC(@CmdStr)
						    END

						    SELECT @RunningNo = NEXT VALUE FOR AwbNumber
						    SELECT @CheckDigit = Convert(INT, @RunningNo % 7)
						    SELECT @AwbNumber = Concat(@RunningNo, @CheckDigit)

						    IF EXISTS(SELECT CNNum FROM tblCSaleCNNum WHERE CNNum = @AwbNumber)
							    SET @ValidCnt = @ValidCnt + 1
						    ELSE
							    SET @ValidCnt = 6
					    END

		                IF NOT EXISTS(SELECT AllocateId FROM tblAWBAllocation WHERE (LEFT(@AwbNumber, 11) BETWEEN LEFT(FromAWBRunNum, 11) AND LEFT(ToAWBRunNum, 11)) AND Status = 'ACTIVE')
		                BEGIN
			                UPDATE tblAWBAllocation SET Status = 'USED', ModifyBy = @CUserID, ModifyDate = GETDATE() WHERE Status = 'ACTIVE'
			                UPDATE tblAWBAllocation SET Status = 'ACTIVE', ModifyBy = @CUserID, ModifyDate = GETDATE() WHERE AllocateId = (SELECT MIN(AllocateId) FROM tblAWBAllocation WHERE Status = 'NEXT')
			            
						    SELECT TOP 1 @FromRunningNo = LEFT(FromAWB, 11), @ToRunningNo = LEFT(ToAWB, 11) FROM tblAWBAllocation WHERE Status = 'ACTIVE' AND IsDelete = 0 ORDER BY FromAWB
						    SELECT @RunningNo = MAX(LEFT(CNNum, 11)) from tblCSaleCNNum WHERE left(CNNum, 11) BETWEEN @FromRunningNo AND @ToRunningNo
						
						    IF(@RunningNo IS NULL OR @RunningNo = 0) SELECT @RunningNo = @FromRunningNo

			                SELECT @CmdStr = CONCAT('ALTER SEQUENCE AwbNumber RESTART WITH ', @RunningNo, ' INCREMENT BY 1')
			                EXEC(@CmdStr)

			                SELECT @CheckDigit = Convert(INT, @RunningNo % 7)
			                SELECT @AwbNumber = Concat(@RunningNo, @CheckDigit)
		                END
	                END
	                ELSE SET @AwbNumber = ''

	                IF(RTRIM(@AwbNumber) != '')
	                BEGIN
		                INSERT INTO tblCSaleCNNum (CSalesNo, Num, CNNum, MobRefNum, OriginCode, ConsignorCode, PostCodeId, DestCode, ConsigneeCode, ShipmentType, Pieces, Weight, VLength, VBreadth, VHeight, VolWeight, HighestWeight, Price, OtherCharges, TotalAmt, InsShipmentValue, InsShipmentDesc, InsRate, InsValue, StaffDiscRate, StaffDiscAmt, PromoCode, PromoRate, PromoDiscAmt, TaxCode, TaxRate, CUserID, CDate, Status, IsExported, ExportedDate, RASurcharge) 
			                VALUES (@CSalesNo, @AwbNumber, @AwbNumber, @MobRefNum, @OriginCode, @ConsignorCode, @PostCodeId, @DestCode, @ConsigneeCode, @ShipmentType, @Pieces, @Weight, @VLength, @VBreadth, @VHeight, @VolWeight, @HighestWeight, @Price, @OtherCharges, @TotalAmt, @InsShipmentValue, @InsShipmentDesc, @InsRate, @InsValue, @StaffDiscRate, @StaffDiscAmt, @PromoCode, @PromoRate, @PromoDiscAmt, @TaxCode, @TaxRate, @CUserID, GETDATE(), 1, 0, NULL, @RASurcharges)
	
		                IF (SELECT COUNT(*) FROM tblLastCNNum) > 0
			                UPDATE tblLastCNNum SET CNRunningNum = @AwbNumber, CNNum = @AwbNumber, CSNo = @CSalesNo, CSDate = GETDATE()
		                ELSE
			                INSERT INTO tblLastCNNum(CNPrefix, CNSuffix, CNRunningNum, CNNum, CSNo, CSDate, DoneBy, IsDeleted) VALUES('', '', @AwbNumber, @AwbNumber, @CSalesNo, GETDATE(), @CUserID, 0)

		                IF(RTRIM(@MobRefNum) != '')
		                BEGIN
			                INSERT INTO tblRefMobileAppUpdate (RefNum, AWBNum, OriginStn, DestStn, CUserID, CDate, Status, IsExported, ExportedDate) 
				                VALUES (@MobRefNum, @AwbNumber, @OriginCode, @DestCode, @CUserID, GETDATE(), 1, 0, null)
		                END

		                SELECT @AwbNumber AS CNNum, SCOPE_IDENTITY() AS InsertedId
	                END
	                ELSE SELECT @AwbNumber AS CNNum, 0 AS InsertedId
                END");
            }
            catch (Exception ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.CheckStoredProcedure() :" + ex.ToString());
            }
        }
        
        private void CheckIndex(string refstr)
        {
            try
            {
                if (refstr == "Idx200513")
                {
                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblCashSales' and name = 'idx_tblCashSales_CSalesNo'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblCashSales_CSalesNo ON tblCashSales(CSalesNo)");
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblCashSales' and name = 'idx_tblCashSales_CSalesDate'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblCashSales_CSalesDate ON tblCashSales(CSalesDate)");
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblCashSales' and name = 'idx_tblCashSales_PaymentMode'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblCashSales_PaymentMode ON tblCashSales(PaymentMode)");
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblCSaleCNNum' and name = 'idx_tblCSaleCNNum_CSalesNo'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblCSaleCNNum_CSalesNo ON tblCSaleCNNum(CSalesNo)");
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblCSaleCNNum' and name = 'idx_tblCSaleCNNum_CNNum'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblCSaleCNNum_CNNum ON tblCSaleCNNum(CNNum)");
                    }
                    dr.Close();

                    dr = GetDataReader($"SELECT type, name, tbl_name, sql FROM sqlite_master WHERE type= 'index' and tbl_name = 'tblLodginAWB' and name = 'idx_tblLodginAWB_Date'");
                    if (!dr.HasRows)
                    {
                        ExecuteQuery($"CREATE INDEX idx_tblLodginAWB_Date ON tblLodginAWB(Date)");
                    }
                    dr.Close();

                    ExecuteQuery($"Insert into SystemParams (paramname, ParamValue) values ('CheckIndex', 'Idx200513')");
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("SkynetCashSales.General.UpdateDatabase.CheckIndex():" + ex.ToString());
            }
        }


    }
}

