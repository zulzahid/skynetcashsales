﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkynetCashSales.General
{
    public class SendEmail : AMGenFunction
    {

        public SendEmail(string strEmailTo, string strEmailCC, string strSubject, string strBodyMeyssage, string strAttachmentpath)
        {
            //ResetData();
            SendEMail(strEmailTo, strEmailCC, strSubject, strBodyMeyssage, strAttachmentpath);
        }


        public SendEmail(string MailMsg)
        {
            SendEMail("scssupport@skynet.com.my", "ictinnasi@skynet.com.my", "Cash Sales System - " + MyCompanyCode.Trim().ToUpper(), "Dear Support Team,", MailMsg, $"Thank you\nCash Sales System - {MyCompanyCode.Trim().ToUpper()}", "", "");
        }

        private System.Net.Mail.SmtpClient SmtpServer;
        private System.Net.Mail.MailMessage mail;
        private string[] EmailToAddress, EmailCCAddress;
        private System.Net.Mail.Attachment attachment;

        public bool SendEMail(string strEmailTo, string strEmailCC, string strSubject, string strBodyTopline, string strBodyMessage, string strBodySignature, string FooterStr, string strAttachmentpath)
        {
            bool res = false;

            try
            {
                using (SmtpServer = new System.Net.Mail.SmtpClient("mail.skynet.com.my"))
                {
                    SmtpServer.Port = 587;
                    SmtpServer.EnableSsl = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("scsinvoice@skynet.com.my", "vR$727R6Ya&GgvwV");

                    using (mail = new System.Net.Mail.MailMessage())
                    {
                        //From Mail Id.
                        mail.From = new System.Net.Mail.MailAddress("donotreply@skynet.com.my");

                        //To Mail Ids
                        EmailToAddress = strEmailTo.Split(new char[] { ';' });
                        for (int i = 0; i < EmailToAddress.Count(); i++)
                        {
                            mail.To.Add(EmailToAddress[i].ToString());
                        }

                        //CC Mail Ids
                        if (!string.IsNullOrEmpty(strEmailCC))
                        {
                            EmailCCAddress = strEmailCC.Split(new char[] { ';' });
                            for (int i = 0; i < EmailCCAddress.Count(); i++)
                            {
                                mail.CC.Add(EmailCCAddress[i].ToString());
                            }
                        }

                        mail.Subject = strSubject;

                        mail.IsBodyHtml = true;
                        mail.Body = strBodyTopline.Replace("\n", "<br>") +
                            ("\n" + strBodyMessage).Replace("\n", "<br>") +
                            ("\n\n" + strBodySignature).Replace("\n", "<br>") +
                            ("<p ​border:ridge><font size=1>" + FooterStr.Replace("\n", "<br>") + "</font></p>");

                        //Attachment path
                        if (!string.IsNullOrEmpty(strAttachmentpath))
                        {
                            attachment = new System.Net.Mail.Attachment(strAttachmentpath);
                            mail.Attachments.Add(attachment);
                        }

                        SmtpServer.Send(mail);
                        res = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.ToString());
            }
            return res;
        }

        public bool SendEMail(string strEmailTo, string strEmailCC, string strSubject, string strBodyMessage, string strAttachmentpath)
        {
            bool res = false;

            try
            {
                using (SmtpServer = new System.Net.Mail.SmtpClient("mail.skynet.com.my"))
                {
                    SmtpServer.Port = 587;
                    SmtpServer.EnableSsl = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("scsinvoice@skynet.com.my", "vR$727R6Ya&GgvwV");

                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                    using (mail = new System.Net.Mail.MailMessage())
                    {
                        //From Mail Id.
                        mail.From = new System.Net.Mail.MailAddress("donotreply@skynet.com.my");

                        //To Mail Ids
                        EmailToAddress = strEmailTo.Split(new char[] { ';' });
                        for (int i = 0; i < EmailToAddress.Count(); i++)
                        {
                            mail.To.Add(EmailToAddress[i].ToString());
                        }

                        //CC Mail Ids
                        if (!string.IsNullOrEmpty(strEmailCC))
                        {
                            EmailCCAddress = strEmailCC.Split(new char[] { ';' });
                            for (int i = 0; i < EmailCCAddress.Count(); i++)
                            {
                                mail.CC.Add(EmailCCAddress[i].ToString());
                            }
                        }


                        mail.Subject = strSubject;

                        mail.IsBodyHtml = true;
                        mail.Body = strBodyMessage;

                        //Attachment path
                        if (!string.IsNullOrEmpty(strAttachmentpath))
                        {
                            attachment = new System.Net.Mail.Attachment(strAttachmentpath);
                            mail.Attachments.Add(attachment);
                        }

                        SmtpServer.Send(mail);
                        res = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                error_log.errorlog(Ex.ToString());
            }
            return res;
        }

    }
}

