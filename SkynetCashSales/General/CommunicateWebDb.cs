﻿using Newtonsoft.Json;
using SkynetCashSales.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SkynetCashSales.General
{
    public class CommunicateWebDb : AMGenFunction
    {
        public CommunicateWebDb()
        {
            APIMainURI = "http://skynetmy.ddns.net:9091/";
            APIToken = "";
            APICheckAWB = "api/RcvCSaleCNNum/GetRcvCSaleCNNumDetails";
            APIMobileReferenceBase = "http://mapi.skynet.com.my/";
            APIMobileReferenceToken = "A@MA6BA9F5D57DDBAEA6158474EC4F7AES%12";
            APIMobileReferenceGet = "api/BookShipment/getshipment";
            APIMobileReferenceUpdate = "api/BookShipment/updateshipment";
            APITrackingBase = "http://api.skynet.com.my";
            APICreateTracking = "api/sn/createTrackingstatus";
            APICreatetblRcvCashSaleBase = "http://skynetmy.ddns.net:8100/";
            APICreatetblRcvCashSale = "api/Skynet/insertTblRcvCashSale";
        }

        public void StartProcess()
        {
            try
            {
                if (!string.IsNullOrEmpty(APIMainURI))
                {
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                    {
                        GetUpdatedPostCode();
                        GetPromotionJSON();
                        //CheckAWBJSON("");
                        UpdateMobileRef();
                        CreateProductAsync(); // Tracking
                        insertTblRcvCashSale(); // Sales data to web database
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("CommunicateWebDb.StartProcess : " + ex.ToString());
            }
        }

        public void GetDestination(string DestCode)
        {
            string sqlStr = "";
            List<clsDestination> data;
            clsGetDestination apiref;

            try
            {
                if (DestCode.Trim() != "")
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://skynetmy.ddns.net:8089");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        data = new List<clsDestination>();
                        apiref = new clsGetDestination
                        {
                            AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                            FunctionStr = "GetDest",
                            DestCode = DestCode,
                            destData = data
                        };
                        var response = client.PostAsJsonAsync("api/WebMasters/GetDest", apiref).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            if (!res.Contains("data not found"))
                            {
                                clsGetDestinationRes item = JsonConvert.DeserializeObject<clsGetDestinationRes>(res.ToString());
                                if (item.status == "Success" && item.data.Count > 0 && item.data[0].DestCode != null)
                                {
                                    SqlDataReader dr = GetDataReader($"SELECT DestCode FROM tblDestination WHERE DestCode = '{DestCode}'");
                                    if (dr.HasRows)
                                    {
                                        sqlStr = $"UPDATE tblDestination SET DestName = '{item.data[0].DestName.Replace("'", "''")}', StationCode = '{item.data[0].StationCode}', State = '{item.data[0].State.Replace("'", "''")}', Country = '{item.data[0].Country.Replace("'", "''")}', Region = '{item.data[0].Region}', IsODA = '{item.data[0].IsODA}', IsDeleted = '{item.data[0].IsDeleted}', IsExported = 1, ExportedDate = GETDATE() WHERE DestCode = '{DestCode}'";
                                    }
                                    else
                                    {
                                        sqlStr = $"INSERT INTO tblDestination (DestCode, DestName, StationCode, State, Country, Region, IsODA, IsDeleted, IsExported, ExportedDate) VALUES('{item.data[0].DestCode}', '{item.data[0].DestName.Replace("'", "''")}', '{item.data[0].StationCode}', '{item.data[0].State.Replace("'", "''")}', '{item.data[0].Country.Replace("'", "''")}', '{item.data[0].Region}', '{item.data[0].IsODA}', '{item.data[0].IsDeleted}', 1, GETDATE())";
                                    }
                                    if (sqlStr.Trim() != "") ExecuteQuery(sqlStr);
                                }
                            }
                        }
                    }

                    GetZoneDet(DestCode);
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog($"CommunicateWebDb.GetDestination() : {ex.ToString()} \n\n{sqlStr}");
            }
        }

        public void GetZoneDet(string DestCode)
        {
            string ZoneCode = "";
            List<clsZoneDet> data;
            clsGetZoneDet apiref;

            try
            {
                if (DestCode.Trim() != "")
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://skynetmy.ddns.net:8089");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        DataTable dt = GetDataTable($"SELECT ISNULL(ZoneCode, '') AS ZoneCode FROM tblZone");
                        for (int n = 0; n < dt.Rows.Count; n++)
                        {
                            ZoneCode = dt.Rows[n]["ZoneCode"].ToString();
                            if (ZoneCode.Trim() != "")
                            {
                                data = new List<clsZoneDet>();
                                apiref = new clsGetZoneDet
                                {
                                    AccessToken = "sgsfdsjnfkjdnfkjdnfkjn",
                                    FunctionStr = "GetZoneDet",
                                    ZoneCode = ZoneCode,
                                    zoneData = data
                                };
                                var response = client.PostAsJsonAsync("api/WebMasters/GetZone", apiref).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var res = response.Content.ReadAsStringAsync().Result;
                                    if (!res.Contains("Record not found"))
                                    {
                                        clsGetZoneDetRes item = JsonConvert.DeserializeObject<clsGetZoneDetRes>(res.ToString());
                                        if (item.status == "Success" && item.data.Count > 0)
                                        {
                                            SqlDataReader dr;
                                            for (int i = 0; i < item.data.Count; i++)
                                            {
                                                if (item.data[i].DestCode == DestCode)
                                                {
                                                    dr = GetDataReader($"SELECT DestCode FROM tblZoneDetail WHERE ZoneCode = '{item.data[i].ZoneCode}' AND DestCode = '{item.data[i].DestCode}'");
                                                    if (!dr.HasRows)
                                                    {
                                                        ExecuteQuery($"INSERT INTO tblZoneDetail (ZoneCode, DestCode, IsExported, ExportedDate) VALUES('{item.data[i].ZoneCode}', '{item.data[i].DestCode}', 0, GETDATE())");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog($"CommunicateWebDb.GetZoneDet() : " + ex.ToString());
            }
        }

        public void GetUpdatedPostCode()
        {
            try
            {
                string FromDate = "2020-09-01";
                SqlDataReader dr = GetDataReader("SELECT TOP 1 timeReceived FROM tblPostcode2020 ORDER BY timeReceived DESC");
                if (dr.Read()) FromDate = Convert.ToDateTime(dr["timeReceived"]).ToString("yyyy-MM-dd"); dr.Close();

                if (FromDate.Trim() != "")
                {
                    GetPostCodeAPIParams apiref = new GetPostCodeAPIParams();
                    apiref.access_token = "fc7017130c796ce23f43a0adc89f382d";
                    apiref.functionstr = "getpostcode";
                    apiref.GetType = "fromdate";
                    apiref.GetVal = "all";
                    apiref.FromDate = FromDate;

                    using (HttpClient client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://api.skynet.com.my");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        var response = client.PostAsJsonAsync("api/sn/getAnyData", apiref).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            if (!res.Contains("data not found"))
                            {
                                var postcodes = response.Content.ReadAsAsync<IEnumerable<Postcode2020>>().Result;
                                var query = (from a in postcodes where a.postcode.Trim() != "" select a).ToList();
                                if (query.Count() > 0)
                                {
                                    foreach (var row in query)
                                    {
                                        dr = GetDataReader($"SELECT postcode FROM tblPostcode2020 WHERE POSTCODEID = {row.postcodeid}");
                                        if (dr.HasRows)
                                            ExecuteQuery($"UPDATE tblPostcode2020 SET postcode = '{row.postcode}', town = '{row.town.Replace("'", "''")}', city = '{row.city.Replace("'", "''")}', state = '{row.state.Replace("'", "''")}', stncode = '{row.stncode}', deliveryFrequency = '{row.deliveryFrequency.Replace("'", "''")}', remarks = '{row.remarks.Replace("'", "''")}', isdelete = '{row.isdelete}', timeReceived = GETDATE() WHERE POSTCODEID = {row.postcodeid}");
                                        else
                                            ExecuteQuery($"INSERT INTO tblPostcode2020 (POSTCODEID, postcode, town, city, state, stncode, deliveryFrequency, remarks, isdelete, timeTransmitted, timeReceived)" +
                                                    $" VALUES({row.postcodeid}, '{row.postcode}', '{row.town.Replace("'", "''")}', '{row.city.Replace("'", "''")}', '{row.state.Replace("'", "''")}', '{row.stncode}', '{row.deliveryFrequency.Replace("'", "''")}', '{row.remarks.Replace("'", "''")}', '{row.isdelete}', '{row.timeReceived}', GETDATE())");
                                        dr.Close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("CommunicateWebDb.GetUpdatedPostCode() : " + ex.ToString());
            }
        }

        public void GetPromotionJSON()
        {
            try
            {
                string promocode = GetValueasSTRING("SELECT top 1 PromoCode FROM tblAllPromotion WHERE Status = '1' ORDER BY ToDate", "PromoCode");
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(APIMainURI);
                //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string promopath = "api/AllPromotion/GetLatest?Code=" + MyCompanyCode.Trim() + "&PromoCode=" + promocode;
                HttpResponseMessage response = client.GetAsync(promopath).Result;

                if (response.IsSuccessStatusCode)
                {
                    //var res = response.Content.ReadAsStringAsync().Result;
                    var promotions = response.Content.ReadAsAsync<IEnumerable<tblAllPromotion>>().Result;
                    var promoquery = (from a in promotions where a.StnCode.Trim() == MyCompanyCode.Trim() || a.StnCode.Trim() == "All" select a).ToList();
                    if (promoquery.Count() > 0)
                    {
                        foreach (var row in promoquery)
                        {
                            try
                            {
                                long check = GetValueasInt("SELECT PromoID FROM tblAllPromotion WHERE PromoCode = '" + row.PromoCode + "' AND ShipmentType = '" + row.ShipmentType + "' ", "PromoID");
                                if (check > 0)
                                {
                                    string SQLQuery = $"UPDATE tblAllPromotion SET AgentCode = '{row.AgentCode}', StnCode = '{row.StnCode}', SubName = '{row.SubName}', PromoCode = '{row.PromoCode}', PromoTitle = '{row.PromoTitle}', PromoDesc = '{row.PromoDesc}', Percentage = '{row.Percentage}', IsPercentage = '{row.IsPercentage}', ShipmentType = '{(row.ShipmentType == null ? "All" : row.ShipmentType)}', FromDate = '{Convert.ToDateTime(row.FromDate).ToString(sqlDateLongFormat)}', ToDate = '{Convert.ToDateTime(row.ToDate).ToString(sqlDateLongFormat)}', Status = 1, IsExported = 0 WHERE PromoID = {check}";
                                    ExecuteQuery(SQLQuery);
                                }
                                else
                                {
                                    string SQLQuery = "INSERT INTO tblAllPromotion (AgentCode, StnCode, SubName, PromoCode, PromoTitle, PromoDesc, Percentage, IsPercentage, ShipmentType, FromDate, ToDate, CUserID, CDate, Status, IsExported) VALUES ('" + row.AgentCode + "', '" + row.StnCode + "', '" + row.SubName + "', '" + row.PromoCode + "', '" + row.PromoTitle.Replace("'", "''") + "', '" + row.PromoDesc.Replace("'", "''") + "', '" + row.Percentage + "', '" + row.IsPercentage + "', '" + (row.ShipmentType == null ? "All" : row.ShipmentType) + "', '" + Convert.ToDateTime(row.FromDate).ToString(sqlDateLongFormat) + "', '" + Convert.ToDateTime(row.ToDate).ToString(sqlDateLongFormat) + "', '" + row.CUserID + "', '" + Convert.ToDateTime(row.CDate).ToString(sqlDateLongFormat) + "', '" + row.Status + "', 0)";
                                    ExecuteQuery(SQLQuery);
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    error_log.errorlog("[AllPromotion] Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception Ex) { error_log.errorlog("API in AllPromotion: " + Ex.ToString()); }
        }

        private void GetAnnouncementsJSON()
        {
            try
            {
                DateTime CreatedDate = DateTime.Now;
                SqlDataReader dr = GetDataReader($"SELECT CreatedDate FROM tblAnnouncements WHERE Status = 1 ORDER BY CreatedDate DESC");
                if (dr.Read())
                {
                    CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                }
                dr.Close();

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(APIMainURI);
                //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string promopath = "api/Announcements/GetLatest?Code=" + MyCompanyCode.Trim() + "&CDate=" + CreatedDate;
                HttpResponseMessage response = client.GetAsync(promopath).Result;

                if (response.IsSuccessStatusCode)
                {
                    var announcements = response.Content.ReadAsAsync<IEnumerable<Model.tblAnnouncement>>().Result;
                    var announcementquery = (from a in announcements select a).ToList();
                    if (announcementquery.Count() > 0)
                    {
                        foreach (var row in announcementquery)
                        {
                            dr = GetDataReader($"SELECT AncID FROM tblAnnouncements WHERE Title = '{row.Title}' AND Description = '{row.Description}'");
                            if(dr.HasRows)
                            {
                                ExecuteQuery($"UPDATE tblAnnouncements Title = '{row.Title}', Description = '{row.Description}', CreatedBy = '{row.CreatedBy}', CreatedDate = '{row.CreatedDate}', Status = '{row.Status}', IsRead = 0, IsExported = 0, ExportedDate = null WHERE Title = '{row.Title}' AND Description = '{row.Description}'");
                            }
                            else
                            {
                                ExecuteQuery($"INSERT INTO tblAnnouncements (Title, Description, CreatedBy, CreatedDate, Status, IsRead, IsExported, ExportedDate)" +
                                    $" VALUES('{row.Title}', '{row.Description}', '{row.CreatedBy}', '{row.CreatedDate}', '{row.Status}', 0, 0, ExportedDate = null)");
                            }
                        }
                    }
                }
                else
                {
                    General.error_log.errorlog("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception Ex) { General.error_log.errorlog("API in MainWindowVM: " + Ex.ToString()); }
        }

        private void CheckAWBJSON(string AWBNum)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(APIMainURI);
                //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                String apipath = APICheckAWB + "?AWBNum=" + AWBNum;
                HttpResponseMessage response = client.GetAsync(apipath).Result;

                HttpResponseMessage response1 = new HttpResponseMessage();

                tblCSaleCNNum obj = new tblCSaleCNNum();
                // HTTP POST  https://www.c-sharpcorner.com/article/wpf-rest-web-api-consumption/
                response = client.PostAsJsonAsync(apipath, obj).Result;//.ConfigureAwait(false);


                if (response.IsSuccessStatusCode)
                {
                    var employees = response.Content.ReadAsAsync<IEnumerable<tblCSaleCNNum>>().Result;
                    string cnnumcheck = (from a in employees where a.CNNum == AWBNum select a.CNNum).FirstOrDefault();
                    if (!string.IsNullOrEmpty(cnnumcheck))
                    {
                        long check = GetValueasInt("SELECT DISTINCT BlockAWBId FROM tblBlockAWB WHERE StnCode = '" + MyCompanyCode + "' ", "BlockAWBId");
                        if (check > 0)
                        {
                            string SQLQuery = "UPDATE tblBlockAWB SET IsBlockAWB = '1', Remarks = 'AWBNum already used.Contact IT department', Status = '1', IsExported='0' WHERE BlockAWBId = " + check + " ";
                            ExecuteQuery(SQLQuery);
                        }
                        else
                        {
                            string SQLQuery = "INSERT INTO tblBlockAWB (IsBlockAWB, StnCodeRemarks, CreateDate, Status, IsExported ExportedDate) VALUES ('1', '" + MyCompanyCode + "', 'AWBNum already used. Contact IT department', GetDate(), '1', '0', null)";
                            ExecuteQuery(SQLQuery);
                        }
                    }
                    else
                    {
                        long check = GetValueasInt("SELECT DISTINCT BlockAWBId FROM tblBlockAWB WHERE StnCode = '" + MyCompanyCode + "' ", "BlockAWBId");
                        if (check > 0)
                        {
                            string SQLQuery = "UPDATE tblBlockAWB SET IsBlockAWB = '0', Remarks = '-', Status = '1', IsExported='0' WHERE BlockAWBId = " + check + " ";
                            ExecuteQuery(SQLQuery);
                        }
                        else
                        {
                            string SQLQuery = "INSERT INTO tblBlockAWB (IsBlockAWB, StnCodeRemarks, CreateDate, Status, IsExported ExportedDate) VALUES ('0', '" + MyCompanyCode + "', '-', GetDate(), '1', '0', null)";
                            ExecuteQuery(SQLQuery);
                        }
                    }
                }
                else
                {
                    error_log.errorlog("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception Ex) { error_log.errorlog("API in RunAPIVM2: " + Ex.ToString()); }
        }

        static MobileRefUpdateAuth obtmref = new MobileRefUpdateAuth();
        private void UpdateMobileRef()
        {
            try
            {
                DataTable dtRef = GetDataTable("SELECT DISTINCT RefNum, AWBNum, OriginStn, DestStn, CUserID, CDate, Status, IsExported FROM tblRefMobileAppUpdate WHERE IsExported = '0'");
                if (dtRef.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtRef.Rows)
                    {
                        string refnum = dr["RefNum"].ToString();
                        string awbnum = dr["AWBNum"].ToString();
                        string origin = dr["OriginStn"].ToString();
                        string destination = dr["DestStn"].ToString();
                        string status = dr["Status"].ToString();

                        obtmref = new MobileRefUpdateAuth();
                        obtmref.RefNum = refnum;
                        obtmref.Awbnumber = awbnum;
                        obtmref.OriginStn = origin;
                        obtmref.DestStn = destination;
                        obtmref.Token = APIMobileReferenceToken.Replace("A@MA", "").Replace("S%12", "");
                        HttpClient client = new HttpClient();
                        client = new HttpClient();
                        client.BaseAddress = new Uri(APIMobileReferenceBase);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.PostAsJsonAsync(APIMobileReferenceUpdate, obtmref).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var query = response.Content.ReadAsStringAsync().Result;
                            if (query.ToString().Contains("Success"))
                            {
                                ExecuteQuery("UPDATE tblRefMobileAppUpdate SET IsExported = '1', ExportedDate = GETDATE() WHERE RefNum = '" + refnum + "' and AWBNum = '" + awbnum + "' and IsExported = '0'");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("Update Mobile Reference : " + ex.ToString());
            }
        }

        public void CreateProductAsync()
        {
            try
            {
                //ResultAWBTrackingDto result = new ResultAWBTrackingDto();
                //var json = Newtonsoft.Json.JsonConvert.SerializeObject(tracking);
                List<Tracking> trac = new List<Tracking>();

                SqlDataReader dataReader = GetDataReader($"select a.CSalesNo,b.CNNum,b.CDate, b.Weight, b.VLength, b.VBreadth, b.VHeight, b.VolWeight,a.CUserID,b.DestCode,b.ShipmentType from tblCashSales a" +
                    $" JOIN tblCSaleCNNum b ON a.CSalesNo = b.CSalesNo" +
                    $" WHERE a.IsExported = '0' ");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        trac.Add(new Tracking
                        {
                            MAWBNumber = dataReader["CNNum"].ToString(),
                            AWBNumber = dataReader["CNNum"].ToString(),
                            AWBDate = Convert.ToDateTime(dataReader["CDate"]),
                            ShipmentType = dataReader["ShipmentType"].ToString(),
                            DestStn = dataReader["DestCode"].ToString(),
                            CreatedBy = dataReader["CUserId"].ToString(),
                            EventType = "CSSPRINT",
                            Reference = "W:" + dataReader["Weight"].ToString() + ",L:" + dataReader["VLength"].ToString() + ",B: " + dataReader["VBreadth"].ToString() + ",H:" + dataReader["VHeight"].ToString() + ",VW:" + dataReader["VolWeight"].ToString() + "",
                            CreatedByStn = MyCompanyCode.Trim()
                        });

                    }
                }dataReader.Close();

                TrackingModel track = new TrackingModel
                {
                    access_token = "C4447448SND5F0S484DSNB3FASN5B2E07A83094",
                    functionstr = "createEvents",
                    AWBTracking = trac
                };

                HttpClient client = new HttpClient();
                client = new HttpClient();
                client.BaseAddress = new Uri(APITrackingBase);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsJsonAsync(APICreateTracking, track).Result;
                if (response.IsSuccessStatusCode)
                {
                    var query = response.Content.ReadAsStringAsync().Result;
                    if (query.ToString().Contains("Success"))
                    {
                        foreach (var row in trac)
                        {
                            ExecuteQuery("UPDATE tblCashSales SET IsExported = '1', ExportedDate = 'GETDATE()'  WHERE CSalesNo = (SELECT t1.CSalesNo FROM tblCSaleCNNum t1 WHERE t1.CNNum = '" + row.MAWBNumber + "' ) and IsExported = '0'");
                        }

                    }
                    ExecuteQuery("Insert into tblDailyLog(StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported) VALUES('" + MyCompanyCode.Trim() + "', 'Tracking Ref', '" + query.ToString() + "', '" + System.Environment.MachineName + "', 'CSSCommunicator', '0', '" + DateTime.Now + "','0','0')");
                }

                var responseJson = response.Content.ReadAsStringAsync().Result;

                responseJson = responseJson.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });


                //error_log.errorlog(responseJson.ToString());

            }
            catch (Exception ex)
            {
                ResultAWBTrackingDto result = new ResultAWBTrackingDto();

                result.code = 500;
                result.message = "Failed to Contact TrackingAPI";
                result.status = "FAILED";

                error_log.errorlog("CreateProductAsync: " + ex.ToString() + System.Environment.NewLine + result);

            }
        }

        public void insertTblRcvCashSale()
        {
            try
            {
                DataTable table = GetDataTable($"SELECT DISTINCT A.CSalesNo, CAST(A.CSalesDate as datetime) as CSalesDate , B.DestCode, B.DestCode AS DestStation, B.ShipmentType, B.Pieces, B.Weight, B.VLength, B.VBreadth, B.VHeight, B.VolWeight, A.Price, A.ShipmentTotal, A.ShipmentGSTValue, A.StationaryTotal, A.IsInsuranced, A.InsShipmentValue, B.InsShipmentDesc, A.InsValue, A.InsGSTValue, A.InsuranceTotal, A.CSalesTotal, A.StaffCode, A.StaffName, A.StaffDisAmt, A.CUserID, A.CDate, A.MUserID, A.MDate, A.OtherCharges, A.IsManual, A.DiscCode, A.DiscPercentage, A.DiscAmount, A.TaxTitle, A.TaxPercentage, A.RoundingAmt, B.MobRefNum AS RefNum, A.Status, A.PaymentMode, A.PaymentRefNo, A.PaymentRemarks, B.Num, B.CNNum, B.ConsignorCode, B.ConsigneeCode, B.CDate, B.CUserID, B.Status FROM tblCashSales A" +
                    $" LEFT JOIN tblCSaleCNNum B ON A.CSalesNo = B.CSalesNo" +
                    $" WHERE B.IsExported = 0");

                if (table.Rows.Count > 0)
                {
                    HttpClient client = new HttpClient();
                    client = new HttpClient();
                    client.BaseAddress = new Uri(APICreatetblRcvCashSaleBase);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    TblRcvCashSale tbl;
                    foreach (DataRow dr in table.Rows)
                    {
                        tbl = new TblRcvCashSale();
                        try
                        {
                            tbl.access_token = "C4447448SND5F0S484DSNB3FASN5B2E07A83094";
                            tbl.StnCode = MyCompanyCode;
                            tbl.SubName = "";
                            tbl.CSalesNo = dr["CSalesNo"].ToString();
                            tbl.CSalesDate = Convert.ToDateTime(dr["CSalesDate"]);
                            tbl.CSalesTotal = Convert.ToDecimal(dr["CSalesTotal"]);
                            tbl.DestCode = dr["DestCode"].ToString();
                            tbl.DestStation = dr["DestStation"].ToString();
                            tbl.ShipmentType = dr["ShipmentType"].ToString();
                            tbl.Pieces = Convert.ToInt32(dr["Pieces"]);
                            tbl.Weight = Convert.ToDecimal(dr["Weight"]);
                            tbl.VLength = Convert.ToDecimal(dr["VLength"]);
                            tbl.VBreadth = Convert.ToDecimal(dr["VBreadth"]);
                            tbl.VHeight = Convert.ToDecimal(dr["VHeight"]);
                            tbl.VolWeight = Convert.ToDecimal(dr["VolWeight"]);
                            tbl.Price = Convert.ToDecimal(dr["Price"]);
                            tbl.ShipmentTotal = Convert.ToDecimal(dr["Price"]);
                            tbl.ShipmentGSTValue = Convert.ToDecimal(dr["ShipmentGSTValue"]);
                            tbl.StationaryTotal = Convert.ToDecimal(dr["StationaryTotal"]);
                            tbl.IsInsuranced = Convert.ToInt32(dr["IsInsuranced"]);
                            tbl.InsShipmentValue = Convert.ToDecimal(dr["InsShipmentValue"]);
                            tbl.InsShipmentDesc = dr["InsShipmentDesc"].ToString();
                            tbl.InsValue = Convert.ToDecimal(dr["InsValue"]);
                            tbl.InsGSTValue = Convert.ToDecimal(dr["InsGSTValue"]);
                            tbl.InsuranceTotal = Convert.ToDecimal(dr["InsuranceTotal"]);
                            tbl.StaffCode = dr["StaffCode"].ToString();
                            tbl.StaffName = dr["StaffName"].ToString();
                            tbl.StaffDisAmt = Convert.ToDecimal(dr["StaffDisAmt"]);
                            tbl.CUserID = Convert.ToInt32(dr["CUserID"]);
                            tbl.CDate = Convert.ToDateTime(dr["CDate"]);
                            tbl.MUserID = Convert.ToInt32(dr["MUserID"]);
                            tbl.MDate = dr["MDate"] != DBNull.Value ? Convert.ToDateTime(dr["MDate"]) : DateTime.MinValue;
                            tbl.Status = Convert.ToInt32(dr["Status"]);
                            tbl.IsExported = 0;
                            tbl.ExportedDate = DateTime.Now;
                            tbl.IsTracked = 0;
                            tbl.UserName = "";

                            tbl.Num = dr["Num"].ToString();
                            tbl.CNNum = dr["CNNum"].ToString();
                            tbl.ConsignorCode = dr["ConsignorCode"].ToString();
                            tbl.ConsigneeCode = dr["ConsigneeCode"].ToString();

                            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(tbl);
                            //error_log.errorlog(json.ToString());

                            var response = client.PostAsJsonAsync(APICreatetblRcvCashSale, tbl).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var query = response.Content.ReadAsStringAsync().Result;
                                if (query.ToString().Contains("Success"))
                                {
                                    ExecuteQuery($"UPDATE tblCSaleCNNum SET IsExported = 1, ExportedDate = GETDATE()  WHERE CSalesNo = '{tbl.CSalesNo}' and CNNum = '{tbl.CNNum}' and IsExported = '0'");
                                }
                                ExecuteQuery($"Insert into tblDailyLog(StnCode, LogTitle, LogDescription, PCName, UserName, CreatedBy, CreatedDate, IsDeleted, IsExported) VALUES('{MyCompanyCode.Trim()}', 'Tracking Ref', '{query.ToString()}', HOST_NAME(), 'CSSCommunicator', '0', GETDATE(),'0','0')");
                            }

                            //var responseJson = response.Content.ReadAsStringAsync().Result;
                            //responseJson = responseJson.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });
                        }
                        catch (Exception ex)
                        {
                            error_log.errorlog("insertTblRcvCashSale 1 :" + ex.ToString());
                        }
                    }                      
                }
            }
            catch (Exception ex)
            {
                error_log.errorlog("insertTblRcvCashSale 2 :" + ex.ToString());
            }
        }

        public class GetPostCodeAPIParams
        {
            public string access_token { get; set; }
            public string functionstr { get; set; }
            public string GetType { get; set; }
            public string GetVal { get; set; }
            public string FromDate { get; set; }
        }

        public class Postcode2020
        {
            public int postcodeid { get; set; }
            public string postcode { get; set; }
            public string town { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string stncode { get; set; }
            public string deliveryFrequency { get; set; }
            public string remarks { get; set; }
            public Nullable<bool> isdelete { get; set; }
            public Nullable<bool> isODA { get; set; }
            public string timeReceived { get; set; }
        }

        public class ResultAWBTrackingDto
        {
            public string status { get; set; }
            public int code { get; set; }
            public string message { get; set; }
        }
        public class Tracking
        {

            public string MAWBNumber { get; set; }
            public string AWBNumber { get; set; }
            public System.DateTime AWBDate { get; set; }
            public string ShipmentType { get; set; }
            public string DestStn { get; set; }
            public string CreatedBy { get; set; }
            public string EventType { get; set; }
            public string Reference { get; set; }
            public string CreatedByStn { get; set; }
        }

        public class TrackingModel
        {
            public string access_token { get; set; }
            public string functionstr { get; set; }
            public List<Tracking> AWBTracking = new List<Tracking>();
        }

        public class MobileRefUpdateAuth
        {
            public string Token { get; set; }
            public string RefNum { get; set; }
            public string Awbnumber { get; set; }
            public string OriginStn { get; set; }
            public string DestStn { get; set; }
        }

        public class TblRcvCashSale
        {
            public string access_token { get; set; }
            public string StnCode { get; set; }
            public string SubName { get; set; }
            public string CSalesNo { get; set; }
            public DateTime CSalesDate { get; set; }
            public decimal CSalesTotal { get; set; }
            public string DestCode { get; set; }
            public string DestStation { get; set; }
            public string ShipmentType { get; set; }
            public int Pieces { get; set; }
            public decimal Weight { get; set; }
            public decimal VLength { get; set; }
            public decimal VBreadth { get; set; }
            public decimal VHeight { get; set; }
            public decimal VolWeight { get; set; }
            public decimal Price { get; set; }
            public decimal ShipmentTotal { get; set; }
            public decimal ShipmentGSTValue { get; set; }
            public decimal StationaryTotal { get; set; }
            public int IsInsuranced { get; set; }
            public decimal InsShipmentValue { get; set; }
            public string InsShipmentDesc { get; set; }
            public decimal InsValue { get; set; }
            public decimal InsGSTValue { get; set; }
            public decimal InsuranceTotal { get; set; }
            public string StaffCode { get; set; }
            public string StaffName { get; set; }
            public decimal StaffDisAmt { get; set; }
            public int CUserID { get; set; }
            public DateTime CDate { get; set; }
            public int MUserID { get; set; }
            public DateTime MDate { get; set; }
            public int Status { get; set; }
            public int? IsExported { get; set; }
            public DateTime ExportedDate { get; set; }
            public int IsTracked { get; set; }
            public string UserName { get; set; }

            public string Num { get; set; }
            public string CNNum { get; set; }
            public string ConsignorCode { get; set; }
            public string ConsigneeCode { get; set; }
        }

        public class clsDestination
        {
            public int DestId { get; set; }
            public string DestCode { get; set; }
            public string DestName { get; set; }
            public string StationCode { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Region { get; set; }
            public bool IsODA { get; set; }
            public bool IsDeleted { get; set; }
            public string TransmittedTime { get; set; }
            public string ReceivedTime { get; set; }
        }

        public class clsGetDestination
        {
            public string AccessToken { get; set; }
            public string FunctionStr { get; set; }
            public string DestCode { get; set; }
            public List<clsDestination> destData = new List<clsDestination>();
        }

        public class clsGetDestinationRes
        {
            public string status { get; set; }
            public string code { get; set; }
            public string message { get; set; }
            public string referenceNumber { get; set; }
            public List<clsDestination> data = new List<clsDestination>();
        }

        public class clsZoneDet
        {
            public int ZoneDetId { get; set; }
            public string ZoneCode { get; set; }
            public string DestCode { get; set; }
            public string NetworkCode { get; set; }
            public string StationCode { get; set; }
            public string SubName { get; set; }
            public string ReceivedTime { get; set; }
            public string ModifyBy { get; set; }
            public string ModifyDate { get; set; }
            public string ShipmentType { get; set; }
            public string TransmittedTime { get; set; }
            public Nullable<bool> IsDeleted { get; set; }
        }

        public class clsGetZoneDet
        {
            public string AccessToken { get; set; }
            public string FunctionStr { get; set; }
            public string ZoneCode { get; set; }
            public List<clsZoneDet> zoneData = new List<clsZoneDet>();
        }

        public class clsGetZoneDetRes
        {
            public string status { get; set; }
            public string code { get; set; }
            public string message { get; set; }
            public string referenceNumber { get; set; }
            public List<clsZoneDet> data = new List<clsZoneDet>();
        }

    }
}
