﻿using System;
using System.IO;
using System.Security.AccessControl;

namespace SkynetCashSales.General
{
    public class error_log
    {
        public static void errorlog(string StrEx)
        {
            try
            {
                string DirPath=@"C:\SkynetICT";
                string FilePath = @"C:\SkynetICT\CasSalesERRORLOG.txt";
                CreateDirectory(DirPath);
                CreateFile(FilePath);
                FileStream fread = new FileStream(FilePath, FileMode.Append, FileAccess.Write);
                StreamWriter swrt = new StreamWriter(fread);
                swrt.WriteLine(DateTime.Now.ToString() + ":" + StrEx);
                swrt.Dispose();
                fread.Dispose();
            }
            catch (Exception Ex)
            {
                errorlog(Ex.ToString());
            }
        }
        public static void CreateDirectory(string directoryname)
        {
            try
            {
                if (!Directory.Exists(directoryname))
                {
                    Directory.CreateDirectory(directoryname);
                   System.Security.Principal.NTAccount identity = new System.Security.Principal.NTAccount("MyOwn");
                   DirectorySecurity ds = new DirectorySecurity();
                   DirectoryInfo dr = new DirectoryInfo(directoryname);
                   dr.GetAccessControl(AccessControlSections.Access);
                   dr.SetAccessControl(ds);
                   ds.AddAccessRule(new FileSystemAccessRule(identity, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow));
                }
            }
            catch (Exception Ex)
            {
                errorlog(Ex.ToString());
            }
        }
        public static void CreateFile(string filename)
        {
            try
            {
                if (!File.Exists(filename))
                {
                    FileStream fcreate = new FileStream(filename, FileMode.Create);
                   fcreate.Close();
                }
            }
            catch (Exception Ex)
            {
                errorlog(Ex.ToString());
            }
        }
    }
}
