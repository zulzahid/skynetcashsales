﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Windows;

namespace SkynetCashSales.General
{
    public class GetIntlPostCode
    {
        public GetIntlPostCode()
        {

        }

        public DataTable IntlPostCode(string country)
        {
            DataTable RetDt= new DataTable();
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);

                if (reply.Status == IPStatus.Success)
                {
                    HttpClient client = new HttpClient();
                    var uri = $"http://web-api.skynet.com.my/api/surcharge/country/{country}";
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMmUzMzcwNzNiMmQ2NjExZjZjNWM0YjNkMDkwNzI1YWIxMWVlZmFlMTJhOTJjZjE4MzlkNDhkYzE1MTY0NTFmY2I5YTY3NTY1ZWYxYTUwZWMiLCJpYXQiOjE2MTAzMjgxNTYsIm5iZiI6MTYxMDMyODE1NiwiZXhwIjoxNjQxODY0MTU2LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.pQWFb-B7xG91A5spmkPTHst6AZJpnds95ClwopdreCSPyhCJuNESOPrum3WKN3gN6fs2YGUsXCCHdU4upxzUx0tG03cUxCYEMCVmRjGR8eyqnqyGl55j_Wop-Y739bNLw_MMKb14N-C7jWZVNsiFlSUjjqdLUT1lbXhcJ-nw-xlyE2Av6_Zu6bfeMwyMaqe5mn588tD-Ov4VHp-GTZY_M92uJdxXXJx9JcLO4TJHcd1-lTPWfq_EXOEJzM0WovGTT0N-INTC2MhWJZhG0aKMgO36tZ5zMMELnL25rO48oZVXQodpcajfLS4Q_VmpahxNkjBv6Ya7PFe9vTR0oWZh0Rx0Cdqu3e0NFpYRe_LPokBiYYGhCGh5r3WrWK_DFyMxoeo80v4dIuywMx57Axh9IHPSDoIgnctrgH7PisdQxHTF9H6nZRGyCpfZBf8HGaTRoTMvTpjb6hjS2b7QuTT9hBSDSAlkD4CzAemoalVtEIAv3xuzScd90e07g82-ffOBRi_oO8rfq4zdB70HZkti7DahcHt5QTA5NHP-VRMzsyUo6uQ7hapDLZXaXYx6snt99ppUa8W5lHsGHQfI39O8G1-7tukQDXHn60EQYqjqWMludzpr_O_AflQoZDnvf5iBaDN4eUcaHotRNiltaS2hp0Ls97JRqalSAX9l4KXz9-I");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(uri).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var res = response.Content.ReadAsStringAsync().Result;
                        if (!res.Contains("Parameter is missing") && !res.Contains("Missing param or value ()") && !res.Contains("No country found"))
                        {
                            RetDt = JsonConvert.DeserializeObject<DataTable>(res);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Rate system is offline. Please check your internet connection or contact SCS Support", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                //if (CheckConnection("https://www.google.com/") == true)
                //{
                //    HttpClient client = new HttpClient();
                //    var uri = $"http://web-api.skynet.com.my/api/surcharge/country/{country}";
                //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMmUzMzcwNzNiMmQ2NjExZjZjNWM0YjNkMDkwNzI1YWIxMWVlZmFlMTJhOTJjZjE4MzlkNDhkYzE1MTY0NTFmY2I5YTY3NTY1ZWYxYTUwZWMiLCJpYXQiOjE2MTAzMjgxNTYsIm5iZiI6MTYxMDMyODE1NiwiZXhwIjoxNjQxODY0MTU2LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.pQWFb-B7xG91A5spmkPTHst6AZJpnds95ClwopdreCSPyhCJuNESOPrum3WKN3gN6fs2YGUsXCCHdU4upxzUx0tG03cUxCYEMCVmRjGR8eyqnqyGl55j_Wop-Y739bNLw_MMKb14N-C7jWZVNsiFlSUjjqdLUT1lbXhcJ-nw-xlyE2Av6_Zu6bfeMwyMaqe5mn588tD-Ov4VHp-GTZY_M92uJdxXXJx9JcLO4TJHcd1-lTPWfq_EXOEJzM0WovGTT0N-INTC2MhWJZhG0aKMgO36tZ5zMMELnL25rO48oZVXQodpcajfLS4Q_VmpahxNkjBv6Ya7PFe9vTR0oWZh0Rx0Cdqu3e0NFpYRe_LPokBiYYGhCGh5r3WrWK_DFyMxoeo80v4dIuywMx57Axh9IHPSDoIgnctrgH7PisdQxHTF9H6nZRGyCpfZBf8HGaTRoTMvTpjb6hjS2b7QuTT9hBSDSAlkD4CzAemoalVtEIAv3xuzScd90e07g82-ffOBRi_oO8rfq4zdB70HZkti7DahcHt5QTA5NHP-VRMzsyUo6uQ7hapDLZXaXYx6snt99ppUa8W5lHsGHQfI39O8G1-7tukQDXHn60EQYqjqWMludzpr_O_AflQoZDnvf5iBaDN4eUcaHotRNiltaS2hp0Ls97JRqalSAX9l4KXz9-I");
                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //    var response = client.GetAsync(uri).Result;
                //    if (response.IsSuccessStatusCode)
                //    {
                //        var res = response.Content.ReadAsStringAsync().Result;
                //        if (!res.Contains("Parameter is missing") && !res.Contains("Missing param or value ()") && !res.Contains("No country found"))
                //        {
                //            RetDt = JsonConvert.DeserializeObject<DataTable>(res);
                //        }
                //    }
                //}
                //else
                //{
                //    MessageBox.Show("Rate system is offline. Please check your internet connection or contact SCS Support", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                //}
            }
            catch (Exception Ex) { }
            return RetDt;
        }

        private bool CheckConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

    }
}
