﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
namespace SkynetCashSales.General
{
    public class WeightUtility
    {
        public string Portweight, weight;
        public string WeightAvailablePorts()
        {
            SerialPort spComPort = new SerialPort();
            string Result = "";
            try
            {
                string[] s = SerialPort.GetPortNames();
                string ports = "";
                for (int i = 0; i < s.Length; i++)
                {
                    ports = ports + s[i] + ",";
                }
                Result = "Available Ports are " + ports;
            }
            catch (Exception Ex) { spComPort.Close(); String s = Ex.Message; }
            return Result;
        }
        public string WeightPortRawData(string WTSerialPort, int WTBaudRate)
        {
            SerialPort spComPort = new SerialPort();
            string Result = "";
            try
            {
                var portExists = SerialPort.GetPortNames().Any(x => x == WTSerialPort);
                if (portExists == true)
                {
                    if (spComPort.IsOpen) { spComPort.Close(); }
                    if (!(spComPort.IsOpen == true))
                    {
                        spComPort.PortName = WTSerialPort;
                        spComPort.BaudRate = WTBaudRate;
                        spComPort.Open();
                    }
                    Thread.Sleep(2500);

                    spComPort.Handshake = Handshake.RequestToSendXOnXOff;
                    spComPort.BreakState = false;
                    Portweight = spComPort.ReadExisting();
                    spComPort.Close();
                    Result = "Port received data is " + Portweight + ". After Splitups : " + errorlogforweighingmachine(Portweight.Split(null));
                }
            }
            catch (Exception Ex) { spComPort.Close(); string s = Ex.Message; }
            return Result;
        }
        public decimal WeightDetails(string WTSerialPort, int WTBaudRate, int WTLocation, int WTDividend, int SleepTime)
        {
            SerialPort spComPort = new SerialPort();
            decimal Result = 0;
            decimal Weighresult;
            try
            {
                var portExists = SerialPort.GetPortNames().Any(x => x == WTSerialPort);
                if (portExists == true)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        try
                        {
                            if (spComPort != null && spComPort.IsOpen == false)
                            {
                                spComPort.PortName = WTSerialPort;
                                spComPort.BaudRate = WTBaudRate;
                                spComPort.Open();
                            }
                            Thread.Sleep(SleepTime);
                            spComPort.Handshake = Handshake.RequestToSendXOnXOff;
                            spComPort.BreakState = false;
                            Portweight = spComPort.ReadExisting();
                            if (Portweight != "")
                            {
                                try
                                {
                                    string[] strar = Portweight.Split(null);
                                    weight = strar[WTLocation];
                                    weight = weight.Replace("ST", "").Replace("GS", "").Replace(",", "").Replace("kg", "").Replace("at", "").Replace("+", "");
                                    Weighresult = Convert.ToDecimal(weight != "" && weight != null ? weight : "0");
                                    WTDividend = WTDividend == 0 ? 1000 : WTDividend;
                                    Result = Weighresult / WTDividend;
                                    if (Result != 0)
                                    {
                                        spComPort.Dispose();
                                        return Result;
                                    }
                                }
                                catch (Exception c)
                                {
                                    try
                                    {
                                        spComPort.Close();
                                    }
                                    catch (Exception d)
                                    {
                                        // do nothing
                                    }
                                    spComPort.Dispose();
                                    spComPort = new SerialPort();
                                }
                            }
                        }
                        catch (System.IO.IOException c)
                        {
                            try
                            {
                                spComPort.Close();
                            }
                            catch (Exception d)
                            {
                                // do nothing
                            }
                            spComPort.Dispose();
                            spComPort = new SerialPort();
                        }
                        catch (Exception c)
                        {
                            //comPort.Close();
                            spComPort.Dispose();
                            spComPort = new SerialPort();
                        }
                    }
                    //string s = "Failed to read from weighing scale. Please try again.";
                    spComPort.Dispose();
                    return 0;
                }
                else
                    return 0;
            }
            catch (Exception Ex)
            {
                //spComPort.Close();
                string s = Ex.Message;
                return 0;
            }
            //return Result;
        }
        public string errorlogforweighingmachine(string[] strar)
        {
            string result = "";
            if (strar.Length >= 9)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6] + "at 7:" + strar[7] + "at 8:" + strar[8]; }
            else if (strar.Length >= 8)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6] + "at 7:" + strar[7]; }
            else if (strar.Length >= 7)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5] + "at 6:" + strar[6]; }
            else if (strar.Length >= 6)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4] + "at 5:" + strar[5]; }
            else if (strar.Length >= 5)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3] + "at 4:" + strar[4]; }
            else if (strar.Length >= 4)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2] + "at 3:" + strar[3]; }
            else if (strar.Length >= 3)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1] + "at 2:" + strar[2]; }
            else if (strar.Length >= 2)
            { result = "at 0:" + strar[0] + "at 1:" + strar[1]; }
            else if (strar.Length >= 1)
            { result = "at 0:" + strar[0]; }
            else { result = "length:" + strar.Length; }
            return result;
        }

    }
}
