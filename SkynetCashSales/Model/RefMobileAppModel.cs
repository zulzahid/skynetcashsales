using System.ComponentModel;
using SkynetCashSales.General;
using System;

namespace SkynetCashSales.Model
{
    public class RefMobileAppModel
    {
        public string RefNum { get; set; }
        public string ShipperName { get; set; }
        public string ShipperAdd1 { get; set; }
        public string ShipperAdd2 { get; set; }
        public string ShipperAdd3 { get; set; }
        public string ShipperAdd4 { get; set; }
        public string ShipperTelNo { get; set; }
        public string SendTo { get; set; }
        public string CompName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
    }
}
