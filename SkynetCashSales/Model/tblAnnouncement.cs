﻿using System;

namespace SkynetCashSales.Model
{
    public partial class tblAnnouncement
    {
        public long AncID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool Status { get; set; }
        public bool IsRead { get; set; }
        public Nullable<bool> IsExported { get; set; }
        public Nullable<System.DateTime> ExportedDate { get; set; }
    }
}
