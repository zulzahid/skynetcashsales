﻿using System;

namespace SkynetCashSales.Model
{
    public partial class tblAllPromotion
    {
        public long PromoID { get; set; }
        public string AgentCode { get; set; }
        public string StnCode { get; set; }
        public string SubName { get; set; }
        public string PromoCode { get; set; }
        public string PromoTitle { get; set; }
        public string PromoDesc { get; set; }
        public decimal Percentage { get; set; }
        public Nullable<bool> IsPercentage { get; set; }
        public string ShipmentType { get; set; }
        public System.DateTime FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public int CUserID { get; set; }
        public System.DateTime CDate { get; set; }
        public bool Status { get; set; }
        public Nullable<bool> IsExported { get; set; }
        public Nullable<System.DateTime> ExportedDate { get; set; }
    }
}
