﻿using System;

namespace SkynetCashSales.Model
{
    public partial class tblCSaleCNNum
    {
        public long CSCNNumID { get; set; }
        public string CSalesNo { get; set; }
        public long Num { get; set; }
        public string CNNum { get; set; }
        public string ConsignorCode { get; set; }
        public string ConsigneeCode { get; set; }
        public int CUserID { get; set; }
        public System.DateTime CDate { get; set; }
        public bool Status { get; set; }
        public Nullable<bool> IsExported { get; set; }
        public Nullable<System.DateTime> ExportedDate { get; set; }
    }
}
