﻿using System.ComponentModel;
using SkynetCashSales.General;
using System;

namespace SkynetCashSales.Model
{
    public class AMAllClassModel
    {
    }
     //public CourierManagementEntities(string connection)
     //       : base(connection)
     //       {

     //       }
    public class AMSearchModel : AMGenFunction
    {
        private string _code;
        public string Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        private string _desc1;
        public string Description1
        {
            get { return _desc1; }
            set { _desc1 = value; OnPropertyChanged("Description1"); }
        }
        private string _desc2;
        public string Description2
        {
            get { return _desc2; }
            set { _desc2 = value; OnPropertyChanged("Description2"); }
        }
        private string _etc1;
        public string Etc1
        {
            get { return _etc1; }
            set { _etc1 = value; OnPropertyChanged("Etc1"); }
        }
        private string _etc2;
        public string Etc2
        {
            get { return _etc2; }
            set { _etc2 = value; OnPropertyChanged("Etc2"); }
        }
        private string _etc3;
        public string Etc3
        {
            get { return _etc3; }
            set { _etc3 = value; OnPropertyChanged("Etc3"); }
        }
        private string _etc4;
        public string Etc4
        {
            get { return _etc4; }
            set { _etc4 = value; OnPropertyChanged("Etc4"); }
        }
    }
    public class SupplierItemModel : AMGenFunction
    {
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
    }
    public class AMGeneralModel : AMGenFunction
    {
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
        private string _code;
        public string Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        private string _description1;
        public string Description1
        {
            get { return _description1; }
            set { _description1 = value; OnPropertyChanged("Description1"); }
        }
        private string _description2;
        public string Description2
        {
            get { return _description2; }
            set { _description2 = value; OnPropertyChanged("Description2"); }
        }
        private string _etc1;
        public string Etc1
        {
            get { return _etc1; }
            set { _etc1 = value; OnPropertyChanged("Etc1"); }
        }
        private string _etc2;
        public string Etc2
        {
            get { return _etc2; }
            set { _etc2 = value; OnPropertyChanged("Etc2"); }
        }
        private string _etc3;
        public string Etc3
        {
            get { return _etc3; }
            set { _etc3 = value; OnPropertyChanged("Etc3"); }
        }
        private string _etc4;
        public string Etc4
        {
            get { return _etc4; }
            set { _etc4 = value; OnPropertyChanged("Etc4"); }
        }
    }
    public class IssueItemModel : AMGenFunction
    {
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged("Quantity"); }
        }
        private string _uom;
        public string UOM
        {
            get { return _uom; }
            set { _uom = value; OnPropertyChanged("UOM"); }
        }
        private string _price;
        public string Price
        {
            get { return _price; }
            set { _price = value; OnPropertyChanged("Price"); }
        }
        private string _issuetotal;
        public string Total_Amt
        {
            get { return _issuetotal; }
            set { _issuetotal = value; OnPropertyChanged("Total_Amt"); }
        }
    }
    public class SOItemModel : AMGenFunction
    {
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged("Quantity"); }
        }
        private string _uom;
        public string UOM
        {
            get { return _uom; }
            set { _uom = value; OnPropertyChanged("UOM"); }
        }
        private string _price;
        public string Price
        {
            get { return _price; }
            set { _price = value; OnPropertyChanged("Price"); }
        }
        private string _discper;
        public string Disc_Per
        {
            get { return _discper; }
            set { _discper = value; OnPropertyChanged("Disc_Per"); }
        }
        private string _discamt;
        public string Disc_Amt
        {
            get { return _discamt; }
            set { _discamt = value; OnPropertyChanged("Disc_Amt"); }
        }
        private string _amountafterdisc;
        public string Amount_AfterDisc
        {
            get { return _amountafterdisc; }
            set { _amountafterdisc = value; OnPropertyChanged("Amount_AfterDisc"); }
        }
        private string _taxper;
        public string Tax_Per
        {
            get { return _taxper; }
            set { _taxper = value; OnPropertyChanged("Tax_Per"); }
        }
        private string _taxamt;
        public string Tax_Amt
        {
            get { return _taxamt; }
            set { _taxamt = value; OnPropertyChanged("Tax_Amt"); }
        }
        private string _amountaftertax;
        public string Amount_AfterTax
        {
            get { return _amountaftertax; }
            set { _amountaftertax = value; OnPropertyChanged("Amount_AfterTax"); }
        }
        private string _issuetotal;
        public string Total_Amt
        {
            get { return _issuetotal; }
            set { _issuetotal = value; OnPropertyChanged("Total_Amt"); }
        }
    }
    public class InvoiceItemModel : AMGenFunction
    {
        private string _soid;
        public string SO_ID
        {
            get { return _soid; }
            set { _soid = value; OnPropertyChanged("SO_ID"); }
        }
        private string _sono;
        public string SO_No
        {
            get { return _sono; }
            set { _sono = value; OnPropertyChanged("SO_No"); }
        }
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
        private string _soquantity;
        public string SO_Quantity
        {
            get { return _soquantity; }
            set { _soquantity = value; OnPropertyChanged("SO_Quantity"); }
        }
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged("Quantity"); }
        }
        private string _uom;
        public string UOM
        {
            get { return _uom; }
            set { _uom = value; OnPropertyChanged("UOM"); }
        }
        private string _price;
        public string Price
        {
            get { return _price; }
            set { _price = value; OnPropertyChanged("Price"); }
        }
        private string _discper;
        public string Disc_Per
        {
            get { return _discper; }
            set { _discper = value; OnPropertyChanged("Disc_Per"); }
        }
        private string _discamt;
        public string Disc_Amt
        {
            get { return _discamt; }
            set { _discamt = value; OnPropertyChanged("Disc_Amt"); }
        }
        private string _amountafterdisc;
        public string Amount_AfterDisc
        {
            get { return _amountafterdisc; }
            set { _amountafterdisc = value; OnPropertyChanged("Amount_AfterDisc"); }
        }
        private string _taxper;
        public string Tax_Per
        {
            get { return _taxper; }
            set { _taxper = value; OnPropertyChanged("Tax_Per"); }
        }
        private string _taxamt;
        public string Tax_Amt
        {
            get { return _taxamt; }
            set { _taxamt = value; OnPropertyChanged("Tax_Amt"); }
        }
        private string _amountaftertax;
        public string Amount_AfterTax
        {
            get { return _amountaftertax; }
            set { _amountaftertax = value; OnPropertyChanged("Amount_AfterTax"); }
        }
        private string _issuetotal;
        public string Total_Amt
        {
            get { return _issuetotal; }
            set { _issuetotal = value; OnPropertyChanged("Total_Amt"); }
        }
    }
    public class AddChequesModel : AMGenFunction
    {
        private string _slno;
        public string Sl_No
        {
            get { return _slno; }
            set { _slno = value; OnPropertyChanged("Sl_No"); }
        }
        private string _number;
        public string Number
        {
            get { return _number; }
            set { _number = value; OnPropertyChanged("Number"); }
        }
        private string _date;
        public string Date
        {
            get { return _date; }
            set { _date = value; OnPropertyChanged("Date"); }
        }
        private string _amount;
        public string Amount
        {
            get { return _amount; }
            set { _amount = value; OnPropertyChanged("Amount"); }
        }
        private string _dueamount;
        public string Due_Amount
        {
            get { return _dueamount; }
            set { _dueamount = value; OnPropertyChanged("Due_Amount"); }
        }
        private string _chequeamount;
        public string Cheque_Amount
        {
            get { return _chequeamount; }
            set { _chequeamount = value; OnPropertyChanged("Cheque_Amount"); }
        }
        private string _select;
        public string Select
        {
            get { return _select; }
            set { _select = value; OnPropertyChanged("Select"); }
        }
    }
    public class SalesReturnItemModel : AMGenFunction
    {
        private string _soinvid;
        public string SOINV_ID
        {
            get { return _soinvid; }
            set { _soinvid = value; OnPropertyChanged("SOINV_ID"); }
        }
        private string _soinvno;
        public string SOINV_No
        {
            get { return _soinvno; }
            set { _soinvno = value; OnPropertyChanged("SOINV_No"); }
        }
        private string _itemcode;
        public string Item_Code
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("Item_Code"); }
        }
        private string _itemdesc;
        public string Item_Desc
        {
            get { return _itemdesc; }
            set { _itemdesc = value; OnPropertyChanged("Item_Desc"); }
        }
        private string _soinvquantity;
        public string SOINV_Quantity
        {
            get { return _soinvquantity; }
            set { _soinvquantity = value; OnPropertyChanged("SOINV_Quantity"); }
        }
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; OnPropertyChanged("Quantity"); }
        }
        private string _uom;
        public string UOM
        {
            get { return _uom; }
            set { _uom = value; OnPropertyChanged("UOM"); }
        }
        private string _price;
        public string Price
        {
            get { return _price; }
            set { _price = value; OnPropertyChanged("Price"); }
        }
        private string _discper;
        public string Disc_Per
        {
            get { return _discper; }
            set { _discper = value; OnPropertyChanged("Disc_Per"); }
        }
        private string _discamt;
        public string Disc_Amt
        {
            get { return _discamt; }
            set { _discamt = value; OnPropertyChanged("Disc_Amt"); }
        }
        private string _amountafterdisc;
        public string Amount_AfterDisc
        {
            get { return _amountafterdisc; }
            set { _amountafterdisc = value; OnPropertyChanged("Amount_AfterDisc"); }
        }
        private string _taxper;
        public string Tax_Per
        {
            get { return _taxper; }
            set { _taxper = value; OnPropertyChanged("Tax_Per"); }
        }
        private string _taxamt;
        public string Tax_Amt
        {
            get { return _taxamt; }
            set { _taxamt = value; OnPropertyChanged("Tax_Amt"); }
        }
        private string _amountaftertax;
        public string Amount_AfterTax
        {
            get { return _amountaftertax; }
            set { _amountaftertax = value; OnPropertyChanged("Amount_AfterTax"); }
        }
        private string _issuetotal;
        public string Total_Amt
        {
            get { return _issuetotal; }
            set { _issuetotal = value; OnPropertyChanged("Total_Amt"); }
        }
    }
    public class RemarksModel : AMGenFunction
    {
        private string _remarks;
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; OnPropertyChanged("Remarks"); }
        }
    }
    public class AMReportModel : AMGenFunction
    {
        private string _code;
        public string Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
    }
    public class AMHistoryModel : AMGenFunction
    {
        private string _slno;
        public string SlNo
        {
            get { return _slno; }
            set { _slno = value; OnPropertyChanged("SlNo"); }
        }
        private string _doneby;
        public string Done_By
        {
            get { return _doneby; }
            set { _doneby = value; OnPropertyChanged("Done_By"); }
        }
        private string _donedate;
        public string Done_Date
        {
            get { return _donedate; }
            set { _donedate = value; OnPropertyChanged("Done_Date"); }
        }
        private string _historytitle;
        public string History_Title
        {
            get { return _historytitle; }
            set { _historytitle = value; OnPropertyChanged("History_Title"); }
        }
        private string _history;
        public string History
        {
            get { return _history; }
            set { _history = value; OnPropertyChanged("History"); }
        }
    }
    public class PrepaidOrderModel : AMGenFunction
    {
        //private string _prepaidorderid;
        //public string PrepaidOrder_Id
        //{
        //    get { return _prepaidorderid; }
        //    set { _prepaidorderid = value; OnPropertyChanged("PrepaidOrder_Id"); }
        //}
        //private string _prepaidorderno;
        //public string PrepaidOrder_No
        //{
        //    get { return _prepaidorderno; }
        //    set { _prepaidorderno = value; OnPropertyChanged("PrepaidOrder_No"); }
        //}
        private string _prepaidOrderdate;
        public string PrepaidOrder_Date
        {
            get { return _prepaidOrderdate; }
            set { _prepaidOrderdate = value; OnPropertyChanged("PrepaidOrder_Date"); }
        }
        private string _accountno;
        public string Account_No
        {
            get { return _accountno; }
            set { _accountno= value; OnPropertyChanged("Account_No"); }
        }
        private string _deptcode;
        public string Dept_Code
        {
            get { return _deptcode; }
            set { _deptcode = value; OnPropertyChanged("Dept_Code"); }
        }
        private string _companyname;
        public string Company_Name
        {
            get { return _companyname; }
            set { _companyname = value; OnPropertyChanged("Company_Name"); }
        }
        private string _skycode;
        public string Sky_Code
        {
            get { return _skycode; }
            set { _skycode = value; OnPropertyChanged("Sky_Code"); }
        }
        private string _contactperson;
        public string Contact_Person
        {
            get { return _contactperson; }
            set { _contactperson = value; OnPropertyChanged("Contact_Person"); }
        }
        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; OnPropertyChanged("Phone"); }
        }
        private double _netpack;
        public double NetPack
        {
            get { return _netpack; }
            set { _netpack= value; OnPropertyChanged("NetPack"); }
        }
        private double _sinpack;
        public double SinPack
        {
            get { return _sinpack; }
            set { _sinpack= value; OnPropertyChanged("SinPack"); }
        }
        private double _prepaid500;
        public double Prepaid500
        {
            get { return _prepaid500; }
            set { _prepaid500 = value; OnPropertyChanged("Prepaid500"); }
        }
        private double _prepaid1000;
        public double Prepaid1000
        {
            get { return _prepaid1000; }
            set { _prepaid1000 = value; OnPropertyChanged("Prepaid1000"); }
        }
        private double _prepaid2000;
        public double Prepaid2000
        {
            get { return _prepaid2000; }
            set {_prepaid2000 = value; OnPropertyChanged("Prepaid2000"); }
        }
        private double _prepaid5000;
        public double Prepaid5000
        {
            get { return _prepaid5000; }
            set { _prepaid5000 = value; OnPropertyChanged("Prepaid5000"); }
        }
        private string _paymentconfirmation;
        public string Payment_Confirmation
        {
            get { return _paymentconfirmation; }
            set { _paymentconfirmation = value; OnPropertyChanged("Payment_Confirmation"); }
        }
        private string _paymentenabled;
        public string Payment_Enabled
        {
            get { return _paymentenabled; }
            set { _paymentenabled = value; OnPropertyChanged("Payment_Enabled"); }
        }
    }
    public class ShipmentTypeModel : AMGenFunction
    {
        private string _shipmenttypeid;
        public string ShipmentTypeId
        {
            get { return _shipmenttypeid; }
            set { _shipmenttypeid = value; OnPropertyChanged("ShipmentTypeId"); }
        }
        private string _shipmenttypename;
        public string ShipmentTypeName
        {
            get { return _shipmenttypename; }
            set { _shipmenttypename = value; OnPropertyChanged("ShipmentTypeName"); }
        }
    }
    public class SearchItemsModel : AMGenFunction
    {
        private string _searchitemsid;
        public string SearchItemsId
        {
            get { return _searchitemsid; }
            set { _searchitemsid = value; OnPropertyChanged("SearchItemsId"); }
        }
        private string _searchitemsname;
        public string SearchItemsName
        {
            get { return _searchitemsname; }
            set { _searchitemsname = value; OnPropertyChanged("SearchItemsName"); }
        }
    }
    public class CSSummary
    {
        public int ID { get; set; }
        public DateTime PrintDate { get; set; }
        public string AWBNo { get; set; }
        public string ShipmentType { get; set; }
        public string DestCode { get; set; }
        public int Pieces { get; set; }
        public decimal Weight { get; set; }
        public decimal VolWeight { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal WOGSTAmt { get; set; }
        public decimal WGSTAmt { get; set; }
        public decimal StationaryAmt { get; set; }
        public decimal InsPremium { get; set; }
        public decimal WOTotalGST { get; set; }
        public string Station { get; set; }
        public string User { get; set; }
        public int PrintCount { get; set; }
        public string StaffCode { get; set; }
        public string StaffName { get; set; }
        public decimal StaffDisAmt { get; set; }
        public decimal OtherCharges { get; set; }
        public string DiscCode { get; set; }
        public decimal DiscPercentage { get; set; }
        public decimal DiscAmount { get; set; }
    }

    public class MobileRefAuth
    {
        public string Token { get; set; }
        public string RefNum { get; set; }
    }

    public class MobileRefUpdateAuth
    {
        public string Token { get; set; }
        public string RefNum { get; set; }
        public string Awbnumber { get; set; }
        public string OriginStn { get; set; }
        public string DestStn { get; set; }
    }
}
