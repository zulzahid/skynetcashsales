using System.ComponentModel;
using SkynetCashSales.General;
using System;

namespace SkynetCashSales.Model
{
    public class RefMobileAppUpdateModel
    {
        public string RefNum { get; set; }
        public string AWBNum { get; set; }
        public string OriginStn { get; set; }
        public string DestStn { get; set; }
    }
}
